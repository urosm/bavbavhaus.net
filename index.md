---
title: "bavbavhaus.net"
...

> Odkrivanje zatona arhitektov kot aktivnih ideologov, spoznanje o ogromnih tehnoloških zmožnostih racionalizacije mesta in ozemlja skupaj z vsakodnevnimi ugotovitvami o njihovi brezkoristnosti ter zastaranje specifičnih metod projektiranja, še preden so njihove hipoteze lahko preverjene v realnosti, za arhitekte tvorijo tesnobno vzdušje, ki na obzorju ponuja vpogled na zelo konkretno ozadje, zastrašujoče kot največje zlo ... [@tafuri1969peruna, 77]

*bavbavhaus.net* je poskus hekanja prokrastinacije in sploščitve nekaterih idej o ["sanjskem datotečnem formatu"](neusmerjeno_pisanje.md) v enostaven sistem povezanih in vzporednih zapiskov okoli teme [arhitekturnega dela in kapitalističnega razvoja](arhitekturno_delo_in_kapitalisticni_razvoj.md).

---
lang: sl
references:
- type: article-journal
  id: tafuri1969peruna
  author:
  - family: Tafuri
    given: Manfredo
  title: "Per una critica dell'ideologia archittetonica"
  title-short: "Per una critica dell'ideologia archittetonica"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1969
  page: 31-79
  language: it
...
