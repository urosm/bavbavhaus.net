Zapiski in skripte za pretvarjanje zapiskov v različne formate in generiranje <https://bavbavhaus.net>.
Glavni repozitorij kode je <https://git.kompot.si/urosm/bavbavhaus.net>.

Za delovanje potrebuje [`pandoc`](https://pandoc.org/) in `make` (gl. [`makefile`](makefile)).

V [`pandoc/filters/`](pandoc/filters) hranimo filtre, ki med generiranjem dodatno prirejajo vsebino in formo zapiskov.
Primerjaj [izvorno kodo tega zapisa](https://git.kompot.si/urosm/bavbavhaus.net) z generiranim na <https://bavbavhaus.net/README.html>, ki gre, med drugim, čez [`pandoc/filters/inline_lua.lua`](pandoc/filters/inline_lua.lua) filter.

```lua {.inline}
local f = io.open("pandoc/filters/inline_lua.lua")
local data = f:read("a")
f:close()
return pandoc.CodeBlock(data)
```
