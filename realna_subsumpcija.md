---
title: "Realna subsumpcija dela pod kapitalom"
...

<!-- Formal and Real Subsumption of Labour under Capital. Transitional Forms [@marx1994economic, 93-121] -->

Dvema oblikama presežne vrednosti -- absolutni in relativni -- ustrezata dve obliki *subsumpcije dela pod kapitalom*, oziroma dve obliki *kapitalistične produkcije*, pri čemer je prva vedno podlaga drugi, čeprav lahko druga predstavlja tudi podlago za uvedbo prve v novih panogah.
[@marx1994economic, 95]

## Formalna subsumpcija

Prvo obliko, ki sloni na absolutni presežni vrednosti, Marx imenuje *formalna subsumpcija dela pod kapitalom* saj se od drugih produkcijskih načinov -- kjer producenti zase ali za drugega proizvajajo presežek -- razlikuje zgolj *formalno*.
Razlika je v vrsti prisile, v metodi s katero je presežna vrednost priklicana v obstoj.

Bistvene točke *formalne subsumpcije dela pod kapitalom*:

1) *delavec* se nasproti kapitalistu, posestniku denarja, postavlja kot posestnik lastne delovne zmožnosti in kot prodajalec njene začasne rabe.
Oba se srečata kot lastnika blag, kot prodajalec in kupec in zatorej kot formalno svobodni osebi med katerima ni drugega, politično ali družbeno določenega, razmerja gospostva ali podreditve.
[@marx1994economic, 95]
V kolikor pride do podreditve, do te pride iz posebne vsebine prodaje ne pa iz prodaji vnaprej postavljene podreditve.
[@marx1994economic, 430]

2) *objektivni pogoji dela* (surovine, orodja -- produkcijska sredstva) ter *subjektivni pogoji dela* (življenjska sredstva) v celoti ali vsaj delno pripadajo kupcu in konsumentu njegovega dela, torej delavcu stojijo nasproti kot *kapital*.
Bolj kot se mu pogoji dela postavljajo nasproti kot lastnost drugega, bolj so razmerje med kapitalom in mezdnim delom formalno prisotno, torej bolj je formalna subsumpcija dela pod kapital dokončna.
(Prva točka implicira drugo, saj drugače delavecu ne bi bilo potrebno prodajati svoje delovne zmožnosti.)

Tu še ne gre za razliko v samem produkcijskem načinu.
Delovni proces je iz tehnološkega vidika nespremenjen in je zgolj kot delovni proces podrejen kapitalu.

## Realna subsumpcija

> The real subsumption of labour under capital is developed in all the forms which produce relative, as opposed to absolute, surplus value, though, as we have seen, this definitely does not exclude the possibility that they might increase the latter while increasing the former.
[@marx1994economic, 105-106]

> With the real subsumption of labour under capital, all the changes we have discussed take place in the technological process, the labour process, and at the same time there are changes in the relation of the worker to his own production and to capital -- and finally, the development of the productive power of labour takes place, in that the productive forces of social labour are developed, and only at that point does the application of natural forces on a large scale, of science and of machinery, to direct production become possible.
> Here, therefore, there is a change not only in the formal relation but in the labour process itself.
> On the one hand the capitalist mode of production -- which now first appears as a mode of production *sui generis* -- creates a change in the shape of material production.
[@marx1994economic, 106]

Realna subsumpcija dela pod kapitalom se razvije v oblikah, ki producirajo relativno

---
lang: sl
references:
- type: book
  id: marx1994economic
  author:
  - family: Marx
    given: Karl
  title: "Economic manuscript of 1861--63 (conclusion)"
  container-title: "Collected works"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Frederick
  volume: 34
  publisher-place: London
  publisher: Lawrence & Wishart
  issued: 1994
  language: en
...
