---
title: "Mladi Lukács"
...

[Esej je bil pozneje objavljen v A. Asor Rosa, Intelektualci in delavski razred.
Saggi sulle forme di uno storico conflitto e di una possibile alleanza, La Nuova
Italia, Firence 1973, z naslovom L'anima, le forme, iz katerega je povzeto
besedilo. Ker tega nismo raziskovali, ne vemo, ali se ta različica ujema z
različico iz revije ali pa je bila v kakšnem delu spremenjena].

## 1. "Novi časi".

"Blagoslovljeni časi": takšni, v katerih je nebesna plošča tista, ki začrta
poti, ki jih je mogoče prehoditi in premagati, in katerih poti osvetljuje
svetloba zvezd. Za te čase je vse novo in hkrati znano, pustolovsko in hkrati
poznano. Svet je širok in vendar podoben svojemu domu, saj je ogenj, ki gori v
duši, iz iste snovi kot zvezde; svet in jaz, svetloba in ogenj, sta jasno ločena
drug od drugega, vendar si nikoli nista tuja: ogenj je duša vse svetlobe, vsak
ogenj je odet v svetlobo. Tako dobi vsako dejanje duše v tej dvojnosti smisel in
polnost: totalnost v smislu in za čute; polnost, ker duša v sebi počiva v poteku
dejanja; polnost spet zato, ker se njeno početje od nje loči in, avtonomizirano,
najde svoje lastno središče ter zariše sklenjen krog okoli sebe. "1 (TR, str.
55).

To je prva stran Teorije romana. Toda že prej je Lukács v enem najpomembnejših
esejev v zbirki Duša in oblike zapisal: "Bil je čas, -- mislimo, da je bil, -- ko
to, čemur zdaj pravimo oblika, kar mrzlično iščemo in se potapljamo v hladne
ekstaze, da bi ga destilirali kot edini ostanek iz spremenljivega kaosa, bil je
čas, ko vse to ni bilo nič drugega kot naravni jezik razodetja, krik, ki
nenadzorovano izbruhne, neposredna energija tresočih se gibanj. Kajti takrat se
še nismo spraševali, kaj to je, niti tega še nismo ločevali od materije in
življenja, ko ni bilo nič drugega kot najpreprostejši način, najkrajša pot do
medsebojnega razumevanja med dvema dušama iste vrste, dušo pesnika in dušo
občinstva. Danes je tudi to postalo problem" (AF, str. 232-33). Tu je prva
značilnost, ki jo je treba poudariti. Nešteto drugih primerov bi lahko pričalo o
tem, da stališča mladega Lukácsa ni mogoče niti razumeti niti prodreti vanj, ne
da bi izhajali iz njegovega globokega občutka specifičnosti eksistencialnega in
estetskega meščanskega problema, - občutka specifičnosti, ki lahko dobi tudi
drugo plat, ki je že prisotna v teh dveh citatih, veliko neizpolnjeno nostalgijo
po nedotaknjeni virtualnosti preteklosti. Res je: pri njem je prisotno tudi
iskanje arhetipskih vzorcev, figur in del, v katerih se napoveduje tipična
lastnost modernega esejista: to so tisti misleci in filozofi, ki jim je
tragično-ironični glas duha že skrivnostno spregovoril: Montaigne, Pascal in
predvsem Platon, ki ga Lukács opredeli kot največjega esejista vseh časov. Toda
te v času izolirane osebnosti najdejo plodna tla, da postanejo referenčne točke
celotne kulture šele takrat, ko se moderna doba predstavi s svojo zelo posebno
in nezamenljivo duhovno konfiguracijo. Pred tem trenutkom jih je mogoče
obravnavati kot preroke ali predhodnike, nikoli pa kot glasnike ali razlagalce,
povezane s pomenom in pričakovanji svojega časa. Veliki lik Sokrata, ki se v
prvem od esejev v zbirki Duša in oblike pogosto vrača narisan z nostalgičnimi
potezami, je v disonanci s klasičnim vzdušjem, s katerim je obdan: šele danes, v
moderni dobi, lahko v celoti dojamemo njegov eksistencialni pomen in popolnoma
razumemo njegovo sporočilo.

Da bi ga bolje razumeli: po Lukácsu obstaja več stičnih in sorodnih točk med
klasično-pogansko in srednjeveško-transcendentalno umetnostjo kot med njima in
moderno-buržoazno umetnostjo. Klasična in srednjeveška umetnost namreč obe
temeljita na predpostavki dejanske človeške celovitosti (v obeh obravnavanih
primerih seveda drugačne vrste), do katere vodita po pogosto nasprotujočih si in
težavnih, a vedno možnih poteh. Za moderno umetnost je značilna prav izguba te
naravne celovitosti. Njeno ponovno iskanje je zanjo problem: osrednji in
specifični problem moderne umetnosti. Lukácsevo celotno razmišljanje od Duše in
oblik do Teorije romana se opira na ta problem.

Ta samoidentifikacija lastne pozicije, ki je hkrati natančna identifikacija
pozicije meščanske ali moderne umetnosti, je že sama po sebi razlog za zelo
močno zanimanje za zgodnjo produkcijo tega misleca. Pravzaprav nam daje splošni
občutek o njegovem poskusu. Mladi Lukács nedvomno sodi med tiste, ki jih ni
veliko, ki poskušajo mimo vseh teoretičnih in tematskih težav priti natančno do
bistva obravnavanega vprašanja. Gledano s tega vidika, se že od samega začetka
uvršča v veliko miselno, estetsko in filozofsko tradicijo. In sklicevanje na
njegov globok občutek za specifičnost je le še en način izražanja istega
koncepta. Ta občutek specifičnosti, to samozavedanje lastne razločnosti in
drugačnosti, to dobro zavedanje, da predstavlja nekaj avtonomnega in
nezamenljivega glede na preteklost, so pravzaprav drže, ki jih zelo pogosto
najdemo na začetku in koncu velikih kulturnih izkušenj, - še posebej velikih
meščanskih kulturnih izkušenj. V teh odločilnih trenutkih se usoda duhovne ali
ideološke izbire izpolni, pozitivno ali negativno, zaradi globoko občutene
potrebe po njeni uveljavitvi proti različno nasprotujočim si zunanjim silam.
Mejnik razkriva najgloblje in najbolj skrivne vsebine civilizacije. Na poseben
način, kot je bilo rečeno, to velja za kulturno civilizacijo, kakršna je
meščanska civilizacija, ki ima avtonomijo duha za svojo temeljno predpostavko.
Čut za razlikovanje ji je zato prirojen in ne moremo se čuditi, da so njene
najvišje manifestacije zaznamovane z njim. Po našem mnenju ni naključje, da
moramo za iskanje dela, v katerem je problem moderne umetnosti obravnavan s tako
živim in zamerljivim občutkom za razlikovanje, v odnosu do preteklosti - do
celotne preteklosti -, morda poseči vse do Schillerjevih Pisma o estetski
vzgoji: dela, katerega vsebina je nedvomno zelo drugačna od vsebine Duše in
oblik, a se ga kljub temu spontano spomnimo zaradi te tesne, globoke sorodnosti
s predmetom naše obravnave.

## 2. Rojstvo "problematičnega posameznika".

Spremembe v času povzročajo spremembe v oblikah. Vendar ta odnos ni niti
neposreden niti mehaničen. Med časi in spreminjajočimi se oblikami je pravzaprav
tisto, kar je Lukács imenoval "transcendentalna topografija duha" (v Duši in
oblikah), ali pozneje (v Teoriji romana) pojasnjevalni in posredovalni poseg
filozofije zgodovine.

To pomeni, da po Lukácsu med čistim empirizmom in poezijo ni nobenega razmerja.
Namesto tega obstaja razmerje med poezijo in dušo, med privilegiranimi trenutki
duha in oblikami, ki jim ab aeterno ustrezajo.

To je tako res, da same eksistencialne analize zgodnjega Lukácsa nikoli ali
skoraj nikoli ne zadevajo vsakdanje empirične resničnosti, temveč po njegovem
mnenju najbolj resnično resničnost duše in idej: odnos, to je odnos človeka ne
do stvari, temveč do samega sebe. Empirija je čisti kaos ali, kot pravi,
  "surovina", s katero ima padla duša pogosto opraviti, vendar ne da bi v tem,
  kar nas kot samovoljnost, naključnost ali nedoločenost obdaja, prepoznala
  svojo lastno zgodovino. Vendar pa duša, ne glede na to, kako zavestno je
  postavljena nasproti kaosu empirije, za Lukácsa ni "kraj", kjer se absolutno
  brez zadržkov opira na samo sebe in kljub ločenosti od sveta ali morda prav
  zaradi nje najde moč, da se postavi v tiho avtonomijo. Na tej točki svoja
  stališča jasno loči od vsakršne možnosti nesporazuma ali zamenjave z
  mističnimi ali abstraktno idealističnimi teorijami. Zanj preusmeritev analize
  eksistence v njena transcendentalna načela ne pomeni osvojiti točko prihoda
  iskanja in v njej kot v sklenjenem in zadovoljnem krogu enkrat za vselej najti
  tiste končne odgovore, ki potem ne ostanejo nič drugega kot velika nerešena
  vprašanja o svetu in človeškem življenju. Predhodna opredelitev
  transcendentalne topografije sodobne duše mu služi le za nakazovanje
  izhodišča, ki po njegovem mnenju ni negotovo, ni nestabilno, predvsem pa ni
  obremenjeno s preprostim empirizmom. Seveda je to že njegova temeljna izbira
  (implicira na primer presojo razmerja med vednostjo in bitjo: pravzaprav si
  sam ustvari interpretativni vidni kot, izhajajoč iz implicitnega poveličevanja
  ideje-v-sebi v nasprotju s svetom). Za zdaj se bomo omejili na to, da bomo
  opozorili na posledice, ki iz tega izhajajo, da bi najprej pojasnili celoten
  obris Lukacijevega diskurza.

Prvo lahko uganemo. Svet, kolikor je empiričen, nima moči nad dušo: "Resnična
eksistenca je vedno neresnična, zaradi empirične narave eksistence ni nikoli
mogoča." Ali z navidezno obrnjeno, a precej podobnega pomena izjavo: "Da bi
živel, se mora človek vrniti v temo, zanikati mora eksistenco" (AF, str. 307).
Če pa svet kot empirija nima nobene moči nad dušo, je, nasprotno, dejstvo, ki je
duši lastno - to je dejstvo, ki se je tesno dotika -, da ne poseduje več
spontano sveta v nobeni obliki totalnosti. Izguba naravne posesti sveta torej ni
brez pomena za stanje duše, - ki je prav iz območja gotovosti padla v območje
večnega spraševanja in iskanja. To novo stanje duše, neznano vsej naši
preteklosti, najde pri Lukácsu izraz v figuri "problematičnega posameznika",
subjekta in hkrati privilegiranega objekta celotne eksistencialne in estetske
teme modernega sveta. "Problematični posameznik" je tisti, za katerega - če
hočemo prevzeti, vendar obrnjeno, formulo, ki jo je Lukács uporabil za
opredelitev klasične umetnosti - je tisti, za katerega "bivanje in usoda,
pustolovščina in uspeh, življenje in bistvo niso več identični pojmi" (TR, str.
57) (kot so bili prej v primeru klasicizma, kot so bili v drugačni obliki
kasneje za srednjeveško transcendenco).

Toda kaj se je zgodilo, da je prišlo do tega preloma v sami "sestavi" biti in
med njenimi vzajemnimi in nujnimi sestavinami? Tu začne diskurz prodirati v
globino, - v dvojnem smislu, da se Lukácsevo stališče jasneje konfigurira in
jasneje pokaže svoje meje, - prav zato, ker Lukács v skladu s svojimi
predpostavkami za ta pojav ne išče zunanjih razlag, ki bi bile z njegovega
stališča vedno obrobne, temveč skuša v sami konfiguraciji duha prepoznati
razloge za opisani fenomen. Kasneje bomo videli, kaj si je treba misliti o tej
"koherenci" (in s kakšno vsebino jo konkretno napolni sam Lukács, tako da ne
ostane, kot bi se tu morda zdelo, zgolj abstraktna koherenca). Zdaj nas bolj
zanima, kako izpostaviti notranjo "igro", ki jo ta stroga koherenca igra za
namene stališča, ki ga zagovarja Lukács. Rekli bomo torej, da je za Lukácsa duh
sam sebi odgovoren za svojo lastno zgodovino. Njegove možnosti, tako kot njegov
šah, imajo povsem enak izvor. Lukács piše: "Odkrili smo produktivnost duha: zato
so arhetipi v naših očeh enkrat za vselej izgubili svoj objektivni dokaz, naša
misel pa bije neskončno pot približka, ki ni nikoli dokončan. In malo naprej: "V
nas samih smo odkrili edino pravo bistvo: razlog, zakaj smo morali izkopati
nepremostljiva brezna med vednostjo in delovanjem, med dušo in podobami, med jaz
in svetom ter refleksivno odtrgati vsako substancialnost, postavljeno onkraj
brezna; razlog, zakaj se je moralo naše bistvo spet dvigniti v status postulata
za nas in izkopati še globlje in bolj grozeče brezno med nami in nami" (TR, str.
61). Tej Lukacsevi analizi bi lahko ugovarjali, da je tavtološka: na ta način se
dejansko vzrok in posledica pojava na koncu izkažeta za skoraj identična. Res pa
je, da Lukács s to relativno tavtologijo in v mejah, ki so lastne njegovi
poziciji, pride do popolnega opisa pojava, ki bi ga lahko povzeli takole:
produktivnost duha je temelj modernega problematizma in tako izraža in hkrati
odraža eksistencialno in umetniško krizo našega časa. Problematični posameznik
je torej otrok produktivnosti duha; je zgodovinski in teoretski proizvod razvoja
moderne filozofije od njenih začetkov do danes.

## 3. "Produktivnost duha" in "beda sveta".

Ta sklep lahko vsaj za zdaj štejemo za zadovoljivega v teh dveh različnih
pomenih ali zornih kotih diskurza. V Lukácsevih očeh bi se zdel zadovoljiv, saj
do njega konkretno pride s ponovnim preučevanjem in kritičnim filtriranjem
  celotne miselne tradicije, na katero je najtesneje navezan. Zadovoljiva je
  tudi za nas, saj se nam zdi teorija oblik mladega Lukácsa ravno skrajni
  produkt vrste buržoaznih filozofskih stališč, ki so med seboj tesno povezana
  in se hkrati presegajo. V luči zgornjih citatov bi lahko mladega Lukácsa
  opredelili kot kantovca, ki predpostavke svojih filozofskih stališč podvrže
  strogi samokritiki, pri čemer se ne brani uporabiti instrumentov, ki jih je že
  izdelala antiidealistična meditacija Schopenhauerja, Kierkegaarda in
  Nietzscheja. Za nas je novost njegovega stališča v tem, da svet idej sam po
  sebi (abstraktna produktivnost duha) ponovno postavi v komunikacijo s svetom
  eksistence in s tem povzroči eksplozijo zaprtega univerzuma klasične nemške
  filozofije. Svet idej, spuščen v živo substanco duše, sicer dobi konkretno
  obliko, ki mu jo lahko zagotovi le eksistenca, vendarle pa se pokaže obrat
  njegove izjemne ustvarjalne moči - njegova nepremostljiva meja in hkrati
  začetek njegovega dramatičnega boja, da ga ne bi premagale sovražne sile
  zunanje resničnosti. O tej zunanji resničnosti Lukács ne poda in ne more
  podati nobenega koherentnega opisa: lahko reče le, da je to antiduh, kaos,
  nered. Vendar intuitivno sluti in to intuicijo postavi za temelj svojega
  diskurza, da moderna umetnost izhaja iz procesa odtujitve meščanskega
  intelektualca od okoliškega sveta, ki je sam po sebi nesrečen.

Tu je, morda nehote, prišel do stališča, ki ima skoraj splošno veljavnost.
Zgodovino meščanske kulture ali vsaj njenega velikega dela pravzaprav v celoti
obvladuje znamenje tega razmerja in tega strmoglavljenja (kot ga je zajel v
pravkar citiranih odlomkih). Med konceptom duha, ki vase vsrka vse bivanje, in
procesom razcepa med subjektom in svetom, ki ga sproži, ni protislovja, temveč
vzročna zveza. Med ogromno kulturno produkcijo, ki jo goji buržoazija, in
postopnim ločevanjem buržoazne duše od sveta, to je, v našem smislu, od prakse
in od samih možnosti človeškega delovanja v družbi in zgodovini, ni protislovja,
temveč vzročna zveza. Internalizacija ustvarjalnih procesov, ki je morda najbolj
značilna za ta širok sektor sodobne meščanske kulture, je prav temelj in hkrati
posledica te izjemne moči, ki sovpada s popolno nemočjo. Le popolna odtujenost
od sveta lahko povzroči neomejen razcvet duha. A obratno: zaradi razcveta duha
je človekova odtujenost od sveta močnejša in povečuje njegovo nemoč proti njemu.
Na tem področju torej največje bogastvo vodi v skrajno revščino. Vendar je prav
tako smiselno reči: le skrajna revščina (in njeno razočarano sprejemanje) lahko
na tem področju vodi v bogastvo.

O tem pojavu je Lukácseva prva knjiga hkrati zavestno pričevanje in objektiven
dokument. Pravzaprav je težko reči, kako zavestno je mladi mislec v svojih
stališčih s svojo diagnozo odseval znake "bolezni", ki jo je obsojal. Zdaj
moramo kritično pregledati nekatere izjave, ki so bile izrečene prej in namenoma
sprejete za dobro kot nujne stopničke v razvoju diskurza. Koherentnost, s katero
vsako dejstvo duha razlaga z duhom samim, ne izključuje, da se v konkretnih
analizah, ki so jim podvrženi pogoji sodobne eksistencialne krize, pojavi sum,
da šele velika beda sveta in v svetu sooča dušo z zgoraj opisanimi krutimi in
nerešljivimi antitezami. Seveda pa mu nezmožnost teoretskega prodiranja v
realnost preprečuje, da bi v zvezi s tem prišel do jasnih sklepov. Da pa ta
problem ni povsem odsoten z obzorja njegovih interpretativnih skrbi, kaže
skrivnostna dialektika, ki poteka skozi Dušo in oblike, med vznemirljivim
občutkom, da lahko vse zreducira na obliko, in tesnobnim spoznanjem, da je vsaka
oblika spričo življenja in zgodovine neuspešna. Ne glede na to, kakšno raven
subjektivne zavesti je Lukács morda dosegel na tej točki, je gotovo, da se v
naših očeh Duša in oblike kot dokončano delo in zato zaznamovano z lastnim
objektivnim pomenom popolnoma umešča v krog tega problema. Okoli nje čutimo
praznino delovanja in prakse, iz katere lahko izvira le visoka meščanska
kultura. Le iz sveta, ki je brez smisla in zaprt tako za preoblikovalno
dejavnost kot tudi za poskuse spoznanja in prodora, lahko nastane eksperiment,
kot je ta.

K temu vidiku problema se bomo še vrnili. Vendar je treba na tem mestu omeniti
eno od nemajhnih zaslug zgodnjega Lukácsa, namreč da se je kar se da približal,
čeprav od znotraj, celovitemu, tj. resnično celovitemu opisu pojava. Proces
posredovanja med nasprotujočimi si vidiki stanja moderne ali meščanske duše
(bogastvo - revščina, ustvarjalna moč - praktična nemoč, vrhunska avtonomija -
bedna izolacija) dejansko predstavlja velik del vsebinske tematike te knjige. In
čeprav ponujene rešitve pogosto ne pomenijo nič drugega kot ponovno postavitev
izhodiščnih podatkov o problemu, je to storjeno tako, da se na poti od enega
pola razmišljanja do drugega vsakič znova pridobijo dragoceni novi podatki za
poznavanje obravnavanih problemov.

Prav to opažanje je povod za (zelo zahtevno) metodo branja, ki je bila
uporabljena v tem primeru. "Kritika" te knjige, usmerjena predvsem v teoretske
in ideološke temelje, na katerih temelji, pravzaprav ne more zanemariti zbiranja
in poudarjanja tistega, kar ima v njej vrednost jasnega samozavedanja o aporijah
meščanske misli in antinomičnih praks, na katere so cepljene. V tem smislu (to
smo že omenili) lahko skladnost z lastnimi načeli za Lukácsa (in v naših očeh)
predstavlja način, kako jasneje in pogumneje izpostaviti meje (tj. omejitve),
znotraj katerih se je njegov diskurz prisiljen gibati. Posledično je prav
bralčevo dojemanje teh meja-omejitev danes pravzaprav najpravilnejši način, da
znotraj Lukácsevega diskurza dojamemo tisto konkretno razmerje med bogastvom in
revščino, o katerem smo prej govorili v zvezi s splošnimi problemi sodobne
meščanske kulture, - kar z drugimi besedami pomeni možnost in našo sposobnost,
da v tem diskurzu prepoznamo bogastvo in revščino, ki sta hkrati in neločljivo
povezana, prepletena in prekrivajoča se.

## 4. Oblika eseja.

Govorili smo o transcendentalni topografiji duha, ki jo Lukács postavlja v
temelj svoje estetske meditacije predvsem v Duši in oblikah. Na tej točki bi
lahko opazili, da so oblike ugotavljanja in prepoznavanja transcendentalne
topografije problematičnega človeka raznovrstne. Niti za dosežene rezultate niti
za metodološke indikacije, ki iz njih izhajajo, ni naključje, da Lukács izbere
tisto, ki jo je izbral, namreč esej. Zakaj esej in ne filozofija ali poezija?
Zakaj ta oblika izražanja in sporočanja, ki je nekje med obema, saj tako kot
filozofija išče resnico, vendar jo tako kot poezija išče tako, da se ukvarja z
oblikami in prihaja do oblik? Zakaj ne, na primer, bolj neposredna in odkrita
filozofska razlaga problema? Zakaj ne filozofija eksistencializma? Opis "eseja o
obliki", kot ga najdemo v uvodnem pismu knjige Duša in oblike, je tako
briljanten in prepričljiv, da bom dodal le informativne elemente k tistemu, kar
je mogoče zelo preprosto razbrati ob branju tega eseja. Namesto tega bi rad
poskusil interpretacijo te točke, ki povzema Lukácseve namere in hkrati uspe
odkriti tisto, kar pri njem ostaja implicitno, kot skrita, čeprav prisotna
motivacija. Na zastavljeno vprašanje: zakaj esej? bi odgovoril: zato, ker lahko
problematični človek resnico vidi le skozi igro zrcal in torej le z refleksijo.
Filozof, na primer, mora gledati življenje, če ga hoče sploh prepisati v
sublimirano obliko. Zato je v nekem smislu bližje empirični resničnosti obstoja
kot esejist, ki namesto tega raziskuje umetnost. Prav zato je esejist bližje
realnosti duše kot filozof, saj je bližje umetnosti, ki je že sublimiran in
posredovan izraz eksistence. Gledati v umetnost torej pomeni, v lukacovskem
jeziku prvih knjig, gledati natančno v bistvo stvari, četudi ali prav zato, ker
stvari niso brutalno zgoščene v umetnosti, temveč za umetnostjo ali ob njej. V
Duši in oblikah Lukács v zelo kratkem stavku zajame smisel tega, kar sem skušal
povzeti: "Modrec si prizadeva za resnico, točno, toda kot Savel, ki se je
odpravil iskat očetove osle in našel kraljestvo, tako bo modrec, ki zna resnično
iskati resnico, na koncu svoje poti dosegel neosvojljiv cilj, življenje" (AF,
str. 36). O tej vrnitvi modreca v življenje bomo obširneje razpravljali pozneje.
Vendar je na tem mestu pomembno, da najprej poudarimo naslednje: oblike znanja,
ki jih predlaga modrec, so zelo posredne oblike znanja. Da bi se poglobil v
resnico stvari, se pravzaprav ozre na mesto, kjer so te stvari že predstavljene
v sublimirani obliki. S to izjavo se že nahajamo znotraj natančno določenega
svetovnega nazora. Toda da bi še bolj pojasnili razloge za izbiro eseja kot
privilegirane oblike izražanja problematičnega posameznika, moramo prejšnjim
argumentom dodati še nekaj, kar Lukácsa jasno razlikuje od vseh filozofov
eksistence, ki so mu sledili, od tistega, kar bi lahko imenovali traktatni pisci
tesnobe in obupa, Heidegger, Jaspers, Sartre; razlikuje se od vseh teh, tudi
kadar se zdi, da so si njihove teme podobne, ali kadar je celo dokazljivo, da so
ti poznejši nadaljevalci diskurza o eksistenci prevzeli teme, oblike, stališča
mladega Lukácsa. Kaj je to nekaj več in nekaj posebnega, kar Lukácsa loči od
piscev traktatov o tesnobi? Po mojem mnenju je to njegova podtalna, morda ne
eksplicitna, a vendarle popolna zavest o nemožnosti redukcije stanja
problematičnega človeka na filozofski sistem, o tem, da bi spremenljivo,
negotovo, a bogato in mobilno življenje človeške eksistence ujeli v okvir
abstraktno totalizirajoče refleksije.

Lukács to občutje "eksistencialnega ekstremizma", do katerega je prišel čas in
ki ga ni mogoče pogojevati z nobenimi omejitvami, niti s teoretsko omejitvijo,
ki je lastna procesu sistematizacije podatkov eksistence, zelo dobro izrazi s to
izjavo: "Če je nekaj na določeni točki postalo problematično, je odrešitev
mogoča le tako, da se problematičnost do skrajnosti radikalizira" (AF, str. 43).
V tem smislu je Lukács veliko bližje osebnostim, kot so Schopenhauer,
Kierkegaard in Nietzsche, katerih filozofska refleksija se ne po naključju
pogosto nagiba k artikulaciji v obliki eseja, ki ga včasih pripeljejo do meja
aforizma in maksime; rekel bi, da je bližje svojim predhodnikom kot svojim
neposrednim in takojšnjim sledilcem, ki so ne po naključju pogosto tudi
obnovitelji tistega "filozofskega razuma", ki so ga Schopenhauer, Kierkegaard,
Nietzsche in ob njih mladi Lukács bolj ali manj prispevali h kritiki ali celo
zanikanju. Da ne bi predlog tega razmerja ostal na ravni čistega vtisa, je
dovolj, če spomnimo na pomen, ki ga v eseju, ki odpira Dušo in oblike, prevzame
lik Schopenhauerja, ki se ga spomnimo kot avtorja Parerga, dela, ki je po
Lukácsu povsem ločeno in avtonomno od Sveta kot volje in predstavitve in ki mu
avtor dolguje predvsem svoj naboj ter kvalifikacijo preroka in predhodnika. Prav
v njem se je Schopenhauer spustil (ali povzpel) iz oblike sistema v obliko
eseja. Če problematičnega posameznika ne more prepoznati in prikazati
filozofija, ga lahko prepozna in prikaže poezija? Nedvomno da, vsaj v teoriji.
Vsaj teoretično, saj so eseji v njegovi zbirki Duša in oblike le galerija
načinov, na katere je sodobna poezija poskušala doseči ta rezultat (kar ga jasno
kaže kot možnega), ne da bi vendarle kdaj presegla čim več sijajnih približkov
(kar prav tako jasno vzbuja dvom v to možnost, ki sem jo prej omenil).

Lukács ni šel nikoli tako daleč, da bi to rekel, toda ali ne moremo spričo
sklepov njegove analize reči, da problematični individuum, ki ga izrisuje, nima
večje možnosti izražanja kot tista oblika umetnosti, ki izhaja prav iz svoje
bistvene notranje problematičnosti in se je zato drži z vsem seboj, s samo svojo
naravo? Ali se ne zdi, da želi Lukács s celotnim delom Duša in oblike nakazati
identifikacijo med formo eseja in bistvom moderne duše, ki je odcepljena od sebe
in ločena od sveta? Zdi se, da tudi tu citat osvetljuje te interpretativne
namige. Lukács namreč zapiše: "Esej vedno govori o nečem, kar je že oblikovano,
ali vsaj o nečem, kar je že enkrat obstajalo"; njegovemu bistvu je lastno, da ne
"izpeljuje novosti iz nič", temveč "daje nov red obstoječim stvarem" (AF, str.
34). To dajanje nove oblike oblikam, to odrekanje odgovornosti za stvaritev, ki
se začne iz nič, omejevanje in hkrati zgoščevanje realnih možnosti vednosti,
izvajanje operacije posredovane rekonstrukcije realnosti, je morda formula, ki
nam najbolje pomaga razumeti ne le Lukačev diskurz o eseju, temveč tudi
splošneje določeno držo raziskovanja, ki jo je poznala in na široko prakticirala
meščanska kultura 20. stoletja.

V odsotnosti tistih srečnih neposrednih vrednot, ki jih imanentno najdemo ali
ugotovimo, da delujejo v življenju in zgodovini, bo potemtakem poseglo tisto,
kar lahko imenujemo posredne vrednote eksistence, ki nam bodo omogočile zamah
svobode in ustvarjalnosti. Ker v stvareh ni več vrednot, bomo morali drugje
poiskati druge, ki pa so že posredovane s procesom formalizacije eksistence.

Esej je tako po Lukácsu tipična izrazna oblika dobe, v kateri neposrednih
vrednot v stvareh ni več. Le s pogledom na druge vrednote, ki so zdaj na tej
strani ali onkraj stvari, lahko ironični divjak ponovno vstopi v življenje, ne
da bi se bal, da bo izgubil samega sebe.

Umetniške oblike, ki jih Lukács obravnava v svoji knjigi, sodijo med te posredne
vrednote sodobnega sveta.

## 5. Totalnost in absolut ter njune aporije.

Rekli smo že: problematična duša modernega človeka ni in ne more biti mirna in
samozadovoljna duša. Nasprotno: njeno najbolj primerno stanje je stanje
napetosti. Elementi, ki jo sestavljajo, so podvrženi nenehnemu prizadevanju,
hrepenenju - Sehnsucht, kot to imenuje Lukács -, da bi se razlikovali od tega,
kar so. Duša, ki je ločena od sebe in sveta, še ni nehala iskati svoje
izgubljene celovitosti. Pravzaprav jo išče prav zato, ker je nima več.

To izjavo, ki se zdi očitna, je treba razumeti v mnoštvu različnih pomenov, ki
jih vsebuje, saj Lukács prav v medsebojni odvisnosti med njimi postavlja problem
ponovne pridobitve totalnosti, vendar ga ne razreši. Izguba totalnosti in
ponovna osvojitev totalnosti dejansko nista in ne moreta biti antitetična in
tako rekoč kronološko zaporedna momenta duha. V Lukacovem teoretiziranju sta dve
strani istega kovanca: če izguba totalnosti postavlja problem njene ponovne
osvojitve, ni ponovne osvojitve (buržoazne) totalnosti, ne da bi jo izgubili in
se nenehno vračali k njeni izgubi.

Na tej točki se spomnimo, kaj smo rekli o razmerju med produktivnostjo duha in
bedo sveta kot prevladujočim znakom meščanske kulture. Podobno bi lahko že zdaj
- anticipirajoč nekatere poznejše analize - razmišljali o razmerju med konceptom
Sehnsucht in konceptom totalnosti (vsaj v Duši in oblikah).

Koncept meščanske totalnosti temelji na konceptu izgube in je od njega
neločljivo povezan. Tudi totalnost torej v meščanskem intelektualnem univerzumu
zgolj reproducira pogoje, zaradi katerih je njena uresničitev materialno
nemogoča. Pravzaprav to od bede sveta ne predstavlja dejanske negacije, temveč
zrcalni odsev, izdelek, ki ohranja vse znake in vse začetne meje matrice. To
bomo bolje videli, ko se bomo ukvarjali s temo tragedije. Za zdaj pa lahko
rečemo, da bomo v končnem cilju, po tolikih spraševanjih in iskanju, odkrili, da
totalnost ni nič drugega kot vsota združenih elementov problematičnosti, ali pa
je v najvišjih in najbolj zavestnih primerih le globalna vizija krize, nemočna,
da bi se učinkovito rešila, in prav zato, čeprav je totalnost, morda bolj obupna
in mučna kot vsak njen nasprotni del, sama po sebi upoštevana. V tem smislu je
torej buržoazna totaliteta res točka prihoda, vendar takšna, ki za vedno
potrjuje in sankcionira neskladja izhodišča.

Kaj pravzaprav v esejih in avtorjih Duše in oblike lukacsianski Sehnsucht v
bistvu predlaga kot cilj samemu sebi? Ob natančnejšem pregledu ugotovimo, da
Sehnsucht predlaga samega sebe kot edini bistveni cilj, - niti ne bi mogel
drugače, saj tudi zanj ni nobenega drugega možnega temelja in utemeljitve zunaj
duha. Na koncu dolgotrajnega iskanja ostaja le še hrepeneče stremljenje, le še
nostalgično hrepenenje: torej isti izhodiščni podatki v obliki, ki se razlikuje
le v tem, da so z napredovanjem postopoma izgubili velik del svoje prvotne vere,
da so se poslabšali in izčrpali vitalni impulz, iz katerega so bili motivirani.

Do tega prepričanja lahko pridemo, če temo absolutnega, kot se pojavlja v vseh
esejih v zbirki Duša in oblike, podrobneje preučimo.

Kot se po Lukácsu moderna duša bistveno razlikuje od klasične ali srednjeveške,
tako se tudi njen pojem absolutnega razlikuje od tistega, s katerim so se v
imanentni ali transcendentni obliki identificirale prejšnje estetske in kulturne
izkušnje. Za meščansko ali moderno dušo absolut ni nič drugega kot popolna
uresničitev same sebe: torej ne gre za resnično in pravilno preseganje
razkoraka, ki obstaja med njo in svetom, še manj pa med njo in transcendentno
realnostjo fiksnih in nespremenljivih vrednot, temveč, nasprotno, za preseganje
njene lastne bistvene avtonomije glede na vso drugo realnost (z vsem, kar iz
tega izhaja in kar zdaj vemo).

Problem avtentičnosti, ki je tako pomemben v meditaciji o Duši in oblikah,
sovpada s tem stališčem. Avtentičnost duše je izpolnitev njenega absolutnega.
Absolut duše je njena pristnost. Toda ta je za razliko od klasične ali
srednjeveške duše lahko zares avtentična le spričo same sebe.

## 6. Avtentičnost in oblika.

Toda ali je meščanska ali moderna duša sploh lahko resnično avtentična? Tu se
začne Lukácsev konkretno zgodovinski diskurz o različnih poskusih, različnih
oblikah, v katerih so ta problem zastavljali in poskušali reševati moderni
pisatelji in misleci. In tu se pokaže še ena značilnost specifičnosti Lukácseve
argumentacije. Kajti le če je zunanja, vsakdanja, občutljiva resničnost
predstavljena v podobi čistega Kaosa; le če je tej fragmentarni, razdrobljeni
podobi sveta v nasprotju (ne da bi se z njo sploh poskušali poistovetiti)
problematična narava ustvarjalne duše, razpete med razpadom v nedoločenost in
nenehnim prizadevanjem za povrnitev bistvenosti in pristnosti; le če so podani
ti pogoji, le če je problem postavljen natanko v teh pogojih; le če je mogoče
razumeti povsem specifično in neponovljivo pojmovanje forme, kot je
konfigurirano že v prvem Lukacovem razmisleku. S tem je mišljeno, da je božja
forma, na katero se Lukacs sklicuje in ji podreja pesniško ustvarjanje, v zelo
tesnem razmerju, pravzaprav v enosti z doslej prikazanim eksistencialnim
pojmovanjem.

Strogo gledano, ko obstaja "ustreznost dejanj notranjim potrebam duše"
(eksistencialni položaj klasične in srednjeveške umetnosti), poezija nikoli ni
forma v pravem in izključnem pomenu besede. Pojem poezije kot oblike se v
zahodni kulturi pojavi prav takrat, ko so dejanja neprimerna notranjim potrebam
duše. Ločevanje teh dveh vidikov problema, kot bi radi storili nekateri, da bi z
njegovo izolacijo obnovili pojem forme, ni mogoče, ne da bi pri tem prezrli celo
tisto bridko zavest, ki jo je o njunem neločljivem razmerju imel že Lukács.

Bog-forma ima smisel le, če je postavljeno razmerje med umetnostjo in
življenjem, kot ga je pravilno postavil Lukács v svoji analizi umetnikov
meščanske dobe. Da bi forma kot vodilo koncepta obstala, je treba še naprej
verjeti v prevlado duha nad resničnostjo in idej nad prakso, samo prakso pa
obravnavati kot domeno razpadajočega in nebistvenega empirizma. Če to
prepričanje izgine, nujno izgine tudi bog oblike.

O povezanosti različnih sestavnih delov stališča ter o njihovi vzajemnosti in
enotnosti bi lahko našteli številne, pravzaprav neskončne citate. Izbrali smo
najbolj zgovorne. O konceptu avtonomije umetnosti: "Vizionarska resnica sveta,
ki se ujema z nami, umetnost, je [...] postala avtonomna: prenehala je biti
kopija, saj so vsi prototipi propadli; in je ustvarjalna celota, ki ji je
naravna enotnost metafizičnih področij za vedno razbita" (TR, str. 65).

O razmerju med poezijo in živim življenjem (kar pomeni: o razmerju med obliko in
kaosom): "... Življenje je za pesnika le surovina; le naravna moč njegovih rok
lahko da obliko - to je mejo in pomen -, lahko iz kaosa izpelje enovitost, lahko
temperira simbole iz breztelesnih pojavnosti, lahko da obliko - to je mejo in
pomen - razdrobljenim in nihajočim mnogoterostim" (AF, str. 91).

O odnosu med problematičnostjo in umetnostjo (kaj to pomeni: o odnosu med
nasprotujočimi si silami duše in njihovo možno enotnostjo): "Pravo razrešitev
lahko da le oblika. Samo v obliki [...] vsaka antiteza, vsaka težnja postane
glasba in nujnost. Ker pot vsakega problematičnega človeka vodi k formi, to je k
tisti enotnosti, ki lahko v sebi poveže največje število razhajajočih se sil, je
na koncu te poti človek, ki zna oblikovati, umetnik, v obliki katerega sta si
pesnik in platonik enakovredna" (AF, str. 57).

O zmožnostih in izraznih možnostih poezije (kaj to v bistvu pomeni: o razmerju
med oblikami in absolutom duše): "Vsako literarno delo je zgrajeno okoli
problemov in trasira pot, ki se lahko nenadoma, nepričakovano in vendar z
neizprosno silo zalomi na robu soteske. Vsaka njegova pot - tudi če se vije po
cvetočih palmovih nasadih ali skozi bujna polja belih lilij - vodi samo tja, na
rob velike soteske, in se ne more ustaviti pred ali drugje kot na samem robu te
soteske. Najgloblji pomen oblik je naslednji: voditi k velikemu trenutku nenadne
tišine in dajati smer mnogoteremu življenju, ki se brezciljno valja, kot da je
usmerjeno samo v te trenutke" (AF, str. 231).

Ali lahko torej za konec rečemo, da za mladega Lukácsa absolut moderne duše
sovpada s formo? Vsekakor je to mogoče reči, če le vedno upoštevamo, da tudi
različna zgoraj obravnavana razmerja niso plod neproblematične in nesporne poti,
temveč tistega Sehnsucht, ki se nenehno postavlja v igro in pod vprašaj in se le
zelo redko (fulminantni trenutki enega od citatov) konča s pogojem izpolnitve
(ki potemtakem tudi ne sloni na nezlomljivi osnovi trdnosti in tako naprej).

## 7. Od estetike k etiki, skozi kaos.

Ali je torej v knjigi Duša in oblike predlog formalistične estetike, ali je Duša
in oblike estetizirajoča knjiga? Nič ne bi moglo biti dlje od Lukácsevih
namenov. S tega vidika je poučen en esej: tisti o Beer-Hoffmannu. Samo vprašati
se moramo, zakaj Beer-Hoffmann in ne na primer Hofmanstahl, da bi dobili odgovor
na naše vprašanje.

Beer-Hoffmann pripada svetu dunajskih estetov; to, kar pripoveduje, je tragedija
dunajskega esteta. Vendar je v njem nekaj drugačnega v primerjavi z deli
njegovih "bratov" po prepričanju in okusu.

"Tudi Beer-Hoffmannova poezija se je razvijala na tej podlagi; toda pri njem so
vse strune napete bolj kot pri katerem koli drugem in pošiljajo globlji in
mehkejši zvok, kjer bi se pri drugih že zdavnaj pretrgale. Pri njegovih estetih
ni sence 'literature'; svet, ki obstaja samo pri njih, ni nastal zaradi estetske
izoliranosti njihove lastne umetnosti ali umetnosti drugih, temveč zaradi
vseobsegajočega bogastva velikega življenja in zlate teže vsakega trenutka, ki
se kopiči iz časa v čas; prav tako v njem ni nobenega odrekanja ali resignacije.
Tudi v njihovem nadvse rafiniranem življenju je veliko naivne svežine, energije
in globokega hrepenenja po bistvu stvari, čeprav vse to pogosto spremljajo
sterilne igre in mazohistični skepticizem" (AF, str. 225). "Beer-Hoffmannovi
esteti so tako psihični, da je dovolj majhna stvar, naključna nesreča, da jih
vse vznemiri; vendar so dovolj močni, da preprečijo, da bi neuspeh njihovih
eksistencialnih vsebin v brezno potegnil tudi njihov obstoj" (AF, str. 227).

Kako se torej Beer-Hoffmannov esteticizem v resnici razlikuje od
Hofmansthalovega in Schnitzlerjevega, čeprav ima s slednjima nedvomno veliko
skupnega? Sodeč po besedah, ki jih navaja Lukács, bi lahko rekli, da je drugačen
prav v raznolikosti razmerja, ki se v njem vzpostavlja med eksistenco in formo,
med življenjem in umetnostjo. Pri Beer-Hoffmannu - ne glede na dosežene
rezultate - je skratka več življenja, več življenjske moči, polnejši in trdnejši
(celo religioznejši) občutek obstoja. Na tej točki je vpeljan nov element
vrednotenja, ki je v kritičnih analizah Duše in oblike najpogosteje spregledan,
vendar je za razumevanje tega dela nujno potreben. Lukács namreč tu pride do
potrditve, da vitalnost eksistencialnega občutja, moč duše nista ravnodušni do
procesa formalizacije, ki ju izraža; nasprotno, v formalni pesniški rezultat
vstopata kot kvalificirajoča elementa. In to je zelo pomembno dejstvo, pravi
preskok v razvoju diskurza.

Že na začetku smo povedali, da po Lukácsu obstaja razmerje med življenjem duše
in poezijo. Zdaj pa se moramo vprašati: kje duša najde moč, da sprosti in
predela svoje konflikte in napetosti, ki jih bo potem poezija zbrala in
uskladila v absolutni obliki? Kakšno je razmerje med poezijo in Življenjem (ki
ga tu razumemo kot življenje zgodovinskih, kolektivnih eksistenc in njihove
občutljive manifestacije v svetu)? Ali v bistvu res drži, da je kaos empirije za
pesniško ustvarjanje tuji? Ali pa ne bi raje rekli, da je ta kaos, na videz
povsem nepomemben, nasprotno bistven element ustvarjanja, prav zato, ker
predstavlja njegovo surovo, a nenadomestljivo materijo in nejasnega prejemnika,
skritega Boga, ki leži pod vsemi stvarmi in s tem tudi pod dušo in pod oblikami?

V eseju o tragediji je odlomek, v katerem je ta koncept napovedan: "Vendar je v
tem svetu red, sestava, v zmedenih volutah njegovih črt. Zdi se, da je nemogoče
ugotoviti njegovo intencionalnost, še manj pa, da bi jo bilo mogoče najti; zdi
se, kot da celotna razmršenost črt negibno čaka na eno samo besedo, da postane
jasna, nedvoumna in razumljiva, kot da bi imel nekdo to besedo vedno na koncu
jezika - a je še nihče ni izrekel. Zdi se, da je zgodovina globok simbol usode:
njene naključnosti v skladu z zakonom, njene na koncu vedno pravične volje in
tiranije. Boj, ki ga tragedija bije za zgodovino, je njena velika osvajalska
vojna z življenjem; poskus, da bi v njej našli - njeno intencionalnost -
nepovratno oddaljeno od intencionalnosti običajnega življenja -, da bi jo lahko
dešifrirali prav kot njeno resnično skrito intencionalnost" (AF, str. 334).

Zapisal sem: zasenčen; in s tem izrazom sem želel izraziti prav dvoumnost, v
kateri ta pojem in to razmerje pušča Lukács. Lukács nikoli ne more iti tako
daleč, da bi priznal, da med empirijo in poezijo obstaja tok korelacij. V
resnici torej njegovih interpretacij ni mogoče razumeti, ne da bi upoštevali
tudi ta element. Navidezno dualistična napetost med dušo in oblikami je pri
Lukácsu v resnici nekaj kompleksnejšega, prepletanje danosti, ki bi mu težko
rekli dialektično v pravem pomenu besede in v katerem se poleg eksistence in
oblik, poleg duše in poezije kot tretji, ne nepomemben sogovornik ponovno
pojavljajo življenje samo, empiričnost, arbitrarnost, naključno, surovo
potrebno.

V Lukácsevi misli gre torej za dvojno gibanje - od življenja k umetnosti in od
umetnosti k življenju - dvojno gibanje, ki pa se nikoli ne izraža dialektično v
sintezi in ne poteka po ciklično ponavljajočih se fazah, temveč je vedno sočasno
s samim seboj, se pravi, vedno poteka hkrati v obe smeri.

Na eni strani je namreč gibanje navzgor od vsakdanjega kaosa k čudežu oblik, od
nereda k redu, od empirične eksistence k bistvu; na drugi strani pa je posesivno
gibanje oblik nad kaosom, reda nad neredom, bistva nad eksistenco. Seveda brez
tega, da bi se vse to ujemalo z rednim diagramom prehodov in razvoja, kajti vsak
prehod se nato zabriše v živo problematiko sodobne duše.

Tako pozoren bralec dojame trenutek, ko se Lukács povzpne na stopnico, ki loči
estetiko od etike. Hočem reči, da tako kot je bilo mogoče iz natančne
eksistencialne vizije izpeljati teorijo oblik, je zdaj za Lukácsa mogoče iz iste
teorije oblik izpeljati novo eksistencialno vizijo, ki, ne da bi nasprotovala
transcendentalnim izhodiščem, jih obogati z obsežno in živo eksistencialno
izkušnjo, ki se, če se izrazim na kratko, postavlja tudi kot predlog etične
narave.

## 8. Življenje in smrt v iskanju absolutnega.

Med ta tri pola - dušo, kaos in oblike - bi lahko umestili branje vseh esejev,
ki jih vsebuje Lukácsevo prvo delo. Na dveh skrajnih mejah (skrajnih v vsakem
smislu) Lukácsevega diskurza sta eseja o Novalisu in Kierkegaardu na eni strani
ter esej o Metafiziki tragedije na drugi strani, ki, kar ni presenetljivo, spet
odpirata in zapirata knjigo. K Metafiziki tragedije se bomo vrnili pozneje, za
zdaj pa lahko rečemo, da ima z drugima dvema omenjenima esejema skupno vsaj to
lastnost, in sicer to, da predstavljajo in opisujejo najbolj absolutne stopnje
napetosti duše, oblike, ki jim ustrezajo - bodisi eksistencialne bodisi
umetniške -, pa so posledično dosegle izrazno raven neprimerljive čistosti. Zato
povsem ali skoraj povsem izključujejo vsakršno razmerje z življenjem, in sicer
ravno v tolikšni meri, da vse, tudi življenje, postane poezija in estetsko
izkustvo. Novalis, kot nas spomni Lukács v svojem eseju, bi lahko izjavil:
"Poezija je tipičen način produkcije človeškega duha"; Lukács pa komentira: "Ne
umetnost zaradi umetnosti, ampak panpoetizem" (AF, str. 107).

Toda kaj pomeni vse zreducirati na poezijo? Za romantike in zlasti za Novalisa
to pomeni, da se ne odrečemo ničemur, kar predstavlja eksistencialno napetost
duha. Od tod njihova sorodnost z Goethejem, a tudi globoka različnost od njega.
"Tu je točka," piše Lukács, "kjer se poti Goetheja in romantikov razhajajo. Oba
iščeta ravnovesje istih nasprotujočih si sil, toda romantiki hočejo nekaj, v
čemer harmonija ne pomeni oslabitve energij" (AF, str. 105). To pomeni, da se,
kot sem že dejal, nočejo odpovedati nobenemu delu napetosti, ki jih žene v
poezijo. Zato celo nekaj nekoliko bolj skrajnega od njihove prvotne vizije, celo
zavračanje tragedije: "Težnje romantikov, da z zavestno nepopustljivostjo vedno
zanikajo, da je tragedija oblika življenja (seveda kot čista oblika življenja,
ne kot oblika poezije), so se pri njem [Novalisu] do skrajnosti izostrile:
njihovo največje prizadevanje je bilo vedno odpraviti tragedijo, najti
netragično rešitev tragičnih situacij" (AF, str. 114). Zakaj torej to zavračanje
tragedije? Ker tragedija s tem, ko svoje like v določenem smislu sooča z
nerešljivostjo njihovih prizadevanj, povzroči, da se ti zavedajo meja sveta, v
katerem se tudi herojsko gibljejo z vrhunskim naporom napetosti. Pri Novalisu,
tako kot pri romantikih, se zavrača prav občutek meje: poezija ne pozna ovir,
svet zanjo nima, ne sme imeti ovir.

Da bi razumeli Lukácsevo oceno tega skrajnega romantičnega poskusa, je treba na
tem mestu spomniti na že izrečene govore o konceptu totalitete in absoluta ter o
mejah, aporijah, protislovjih in dramatičnih ovirah, na katere je ta koncept
naletel v Lukácsevem lastnem teoretiziranju. Izjemno pomembno je namreč, da
Lukács uvidi, kako je naravni in nujni zaključek te brezmejne napetosti duha
ravno nasprotje tistega, za kar si je prizadeval in se boril, in kako je skratka
med celoto in ničem tesnejše sorodstvo, kot se zdi, in kako je na koncu
brezmejna volja do življenja nič drugega kot klicanje smrti. Nihče ne bi mogel
biti bolj jasen, kot je uspelo Lukácsu v tej sklepni sodbi o Novalisu: "Med
vsemi temi iskalci oblasti nad življenjem je edini praktični umetnik obstoja.
Toda tudi on ni dobil odgovora na svoje vprašanje: posvetoval se je z življenjem
in smrt mu je dala odgovor" (AF, str. 118). In še: "Tragedija romantikov je bila
v tem, da je le Novalisovo življenje lahko postalo poezija; njegova zmaga je
smrtna obsodba za celotno šolo. Vsa sredstva, s katerimi so hoteli osvojiti
življenje, so preprosto zadostovala za lepo smrt; njihova filozofija življenja
ni bila nič drugega kot filozofija smrti, njihova umetnost življenja je bila
umetnost umiranja" (ibid.). Kierkegaarda pa Lukács predstavlja v drugačni,
pravzaprav nasprotni drži kot Novalisa in romantike. Dejansko je ujet - in ne v
jedru svoje filozofije - v poskusu, da bi svoj obstoj prilagodil bistvu, da bi
svet upognil svoji volji, da bi zakon absolutnega prenesel v samo resničnost.
Toda tudi zanj, tako kot za Novalisa, je bilo to mogoče le na podlagi strašnega
procesa redukcije. Njegova želja po absolutnem in avtentičnosti je bila
zreducirana - in tega ni mogel preseči - na izpolnitev ene same geste, na
zavestno in namerno ločitev od Regine Olsen, ki jo je nežno ljubil. Samo v
gesti, pravzaprav v njeni trenutnosti in negotovosti, je mogoče absolutno
prevesti v eksistencialne izraze: "Z eno besedo, gesta je tisti skok, s katerim
se v življenju absolutno spremeni v možno. Gesta je veliki paradoks življenja,
saj se vsak bežni trenutek življenja umakne v njegovo negibno večnost in v njej
postane resnična resničnost" (AF, str. 71). Toda tako kot je pri Novalisu
iskanje pesniškega absoluta vodilo vse dlje od življenja in nato skorajda nujno
k poveličevanju in sprejemanju smrti kot edine možne resničnosti, kot edine
resnično absolutne resničnosti, tako tudi pri Kierkegaardu junaška zahteva po
vnosu absolutnega v življenje to življenje le podvrže nenehni nesreči, ki se
prav tako nima možnosti končati drugače kot v pristnem niču smrti.

Toda citat, ki ga navajamo, nam omogoča tudi drugo vrsto razmisleka. Pravzaprav
nas pripelje do razumevanja, kako nastanejo in kakšne so te "eksistencialne"
oblike, ki so postavljene ob bok umetniškim oblikam in reproducirajo nekatere
njihove značilnosti: ti "privilegirani trenutki" eksistence, v katerih se, morda
le za trenutek, uresniči strašni in žgoči zakon absoluta. Lukačev diskurz se
spusti za eno stopnjo: zdaj gre preverit vsebino eksistence in v ta namen na tej
stopnji opusti skrajne ravni argumentacije, redke in skoraj neprehodne poti
romantičnega junaštva. Na prizorišče stopi pojem "meščanske umetnosti", v kateri
junaštvo in življenje nista več nezdružljiva pojma, temveč se zbližujeta.

## 9. Meščanska zgodovinska morala in apriornost oblik.

Med skrajnima izkušnjama leži lok možnih rešitev: Kassner, Storm, George,
Philippe, Beer-Hoffmann.

Z možnimi rešitvami tu mislim predvsem tiste, ki se sicer uresničujejo v celoti,
vendar ne povzročajo niti na eksistencialni niti na umetniški ravni pretirano
nasilnih napetosti in posledično pretirano drastičnih prelomov. Skratka, eseji,
ki sestavljajo osrednji del knjige Duša in oblike, poskušajo razumeti, ali je v
življenju in umetnosti mogoče iskati absolutno, ki ne vodi takoj v smrt. Da bi
to storil, se mora Lukács spustiti iz hiperurana eksistencialne napetosti in se
nasloniti na že dano ozadje, torej, kot bomo videli, že zgodovinsko izkušeno.
Samega koncepta tragičnega pri Lukácsu ni mogoče izostriti, ne da bi ga povezali
s to bistveno podlago izraznega in življenjskega posredovanja, v katerem so že
prisotni vsi pogoji tragičnosti, vendar kot utišani in ublaženi z eksistencialno
držo, ki na splošno ni tragična.

V tem pogledu so razsvetljujoči čudoviti razmisleki o razmerju med tragedijo in
idilo pri avtorjih, kot sta Storm in Philippe.

Po Lukácsevi oceni je idila zgolj tragedija v svoji moči. Vsebuje namreč vse
elemente nerazrešljivosti, ki so temelj tragedije. Toda drža njenih likov je v
bistvu v tem, da sprejmejo pogoje svojega eksistencialnega neuspeha, ne pa da
jih zavračajo ali zavračajo; kot to stori tragični junak, ki jih prav zato
pripelje do končne stopnje napetosti in s tem nujno do zloma.

Opredelitev te eksistencialne drže, natančno opredeljene in zamejene, lahko naš
diskurz popelje še dlje. V teh esejih namreč problematika duše in forme tako
rekoč najde svojo zgodovinsko korenino, pa tudi, kot smo videli doslej, svojo
teoretsko in transcendentalno korenino. Na tej podlagi opredelitev Lukácsa kot
teoretika meščanske umetnosti dobi svojo polno karakteristično vrednost. Tu
namreč odkrijemo, kakšno objektivno podlago ima teorija oblik in s kakšno
vsebino naj bi bile napolnjene t. i. transcendentalne povezave duha.

Topografski zemljevid" moderne notranjosti, katerega meje smo doslej začrtali
navzven, proti življenju, empiričnosti, Kaosu ali proti absolutnemu in
totalnosti, zdaj razkriva svojo intimno, specifično konfiguracijo. V tej skupini
esejev so namreč v Lukácsu v razmislek vključene vrednote, ki niso posredne ali
posredovane, ampak so neposredno vrednote, ki so prepoznane kot pripadajoče
meščanski kulturni in duhovni civilizaciji na določeni stopnji njenega razvoja.
To nam omogoča, da bolje razumemo ne le vrsto razmerja, ki Lukácsa veže s
kulturnim in intelektualnim okoljem, temveč tudi in predvsem vrsto splošne
vizije - če hočete: Weltanschauung -, ki vlada njegovi estetski misli, in še
enkrat, tesno povezavo in vzajemnost, ki pri mladem Lukácsu poteka med konceptom
eksistence in njenimi formalnimi rešitvami.

V teh esejih se pokaže, kako se lahko človek - tudi navaden človek - včasih
odreši zgolj vsakdanji empiričnosti, če je sposoben dojeti in v držo prevesti
usodni element, ki je v eksistenci vsakega človeka. Ponavljam: človek, celo
navaden človek. Ni treba biti Kierkegaardovec, da bi se človek zavedel svoje
usode: tudi Kierkegaard je z izjemno absolutnostjo svoje geste pokazal pot, po
kateri se gibljejo liki Storma, Beer-Hoffmanna itd. Toda Kierkegaard si je to
želel, svojo lastno usodo. Ti drugi liki pa so jo na splošno le prenašali. Toda
prav v tem se razkriva prav posebna in v tem smislu izjemna lastnost njihovega
bitja.

V tej viziji namreč usoda ni nič drugega kot globoko in nujno spoznanje tega,
kar smo. Spoznati samega sebe torej pomeni spoznati svojo usodo. Umetnost,
oblike pa so le eksplikacija in izraz tega spoznanja o usodi, ki je tudi v
stvareh, predvsem pa v človeku, na najvišji ravni duha.

Od Schopenhauerja do Nietzscheja in Manna iz Malega gospoda Friedemanna,
Buddenbrookovih in Smrti v Benetkah ima identifikacija tega motiva že svojo
zgodovino, ki jo Lukács prevzame in povzame v zelo učinkovitih formulah. Ko
pravi: "Vsako napisano delo predstavlja svet pod simbolom razmerja usod; problem
usode povsod določa problem forme" (AF, str. 29), se sprašujemo, ali je mlademu
meščanskemu mislecu bilo pomembno le izražanje teoretske formulacije problema,
ali pa teoretske formulacije ni raje modeliral na nekaterih temeljnih
značilnostih Mannovih Buddenbrookov, ki jih je inteligentno dojel in definiral.
V tem pogledu je torej etična vsebina Lukačevega eksistencialnega predloga
povezana s tisto strujo meščanske misli (ki jo radi imenujemo "negativna
misel"), v kateri je jasno potrjena prevlada biti nad voljo in problem etike ni
nič drugega kot problem popolnega izraza lastnega ustvarjalnega potenciala.
Nietzschejevski "postani to, kar si" je v tej drži mladega Lukácsa na prvem
mestu, ne da bi jo izčrpal.

Dejansko liki Storma, Philippa ali Beer-Hoffmanna vedo, da do tega samospoznanja
ni mogoče priti, ne da bi izvajali ravnanje, ki ga naredi nujnega in ki ga, ko
ga doseže, postavi v pravo razmerje z duhom in umetnostjo.

Po drugi strani pa je torej Lukács s poudarjanjem koncepta odgovornosti kot
vodila svojih avtorjev in njihovih likov tesno povezan s problematiko sodobnega
nemškega kulturnega sveta (Windelband, Dilthey, Weber).

Na tej operaciji sinteze, ki v Nemčiji na začetku 20. stoletja ni edinstvena
(mladi Mann izvede izjemno podobno operacijo), a je kljub temu izvedena z
izjemno prefinjenostjo in občutljivostjo, temelji jedro Lukácsevega
etično-eksistencialnega predloga.

Na tej podlagi gre Lukács pravzaprav še dlje od svojih abstraktnejših teoretskih
predlogov, da bi prišel do izrisa junaškega lika meščanskega intelektualca, pri
katerem najprej spoznanje in nato sprejetje lastne usode ne pomenita tragične
odtujenosti od okoliške stvarnosti, temveč nasprotno, dostojanstveno moško
postavitev pred njo.

Poleg tega je bil že pri Kierkegaardu zarisan človeški "model", na katerega se
tu natančno sklicujemo. Pod masko zapeljivca je Lukács v resnici odkril asketa,
"ki se je v tej gesti prostovoljno okamenil iz askeze" (AF, str. 74). V
navidezno idiličnem Philippu je Lukács dojel to izjemno tragično in filozofsko
dejstvo, da ima velika ljubezen, ko se "hrepenenje razteza onkraj sebe, v sebi
vedno nekaj asketskega" (AF, str. 194), in da ljubezen, ko postane asketska,
daje moč, ki lahko seže vse do surovosti in hudobije2.

"Beer-Hoffmannovi esteti [ta citat smo že navedli, ne po naključju začenjamo
diskurz, ki tu doseže svoj zaključek] so tako psihični, da je dovolj majhnost,
naključna nesreča, da jih vse vznemiri, vendar so dovolj močni, da preprečijo,
da bi propad njihove eksistenčne vsebine potegnil v brezno tudi njihov obstoj"
(AF, str. 221). jorge je vsekakor estet in njegova poezija je poezija modernega
intelektualizma, vendar to ne zmanjšuje pomena dejstva, da v njegovi poeziji
"skoraj ni žalovanja: mirno gleda življenju v obraz, morda resignirano, vendar
vedno neustrašno, vedno z dvignjeno glavo" (AF, str. 185-86).

Askeza, moško sprejemanje lastne usode, zavračanje sentimentalnosti, neustrašna
resignacija: ali niso to že sami po sebi sestavni deli etike med stoiško in
protestantsko-krščansko, v kateri se zdi, da lahko prepoznamo odmev Webrovih
takrat še zelo svežih teorij?

To je več kot le vtis. Esej o Burzi - katere pomen je v naših očeh resnično
osrednji in ki, da bi že na začetku odpravili morebitne nesporazume in
nejasnosti, nosi naslov La bourgeoisie et l'art pour l' art - ponovno potrjuje
enega za drugim vse svoje temelje. Pravzaprav je to le dolg, veličasten prikaz
načina, kako meščansko življenje, pošteno prakticirano v vseh svojih pomenih in
obveznostih, tudi posvetnih, ne izključuje, temveč v določenem smislu olajšuje
in daje moč umetniški poklicanosti, ki se konča prav v zelo visokem, skoraj
svetem pojmovanju oblike. Meščanstvo ali umetnost za umetnost: ali ni to morda
največji paradoks? Vendar, piše Lukács, "nekoč [...] se ni predstavljal kot
paradoks. Kako bi si namreč, meščan po rodu, lahko mislil, da lahko obstaja še
kakšen drug obstoj kot meščanski? To, da je bila umetnost sama po sebi dovršena
stvar in se je ravnala po svojih zakonih, ni bila posledica nasilnega izmikanja
življenju, ampak je veljala za naravno dejstvo, kolikor je bilo vsako delo,
opravljeno z resnostjo, samo po sebi upravičeno" (AF, str. 121). To tudi pomeni,
da je za ta tip umetnika, tako kot za antične umetnike-obrtnike, "umetnost
manifestacija življenja, tako kot vse drugo, in zato življenje, posvečeno
umetnosti, zavezujejo enake pravice in dolžnosti kot vsako drugo človeško in
meščansko dejavnost" (AF, str. 133). Iz te natančne umestitve razmerja med
umetnostjo in življenjem izhajajo nekatere maksime neposrednega in splošno
etičnega značaja: "Meščanski poklic kot oblika obstoja pomeni predvsem primat
etike v življenju, pomeni, da v življenju prevladuje tisto, kar se sistematično,
redno ponavlja, kar je treba obvezno ponavljati, kar je treba storiti ne glede
na ugodje ali neugodje. Z drugimi besedami: prevlada reda nad razpoloženji,
trajnega nad trenutnim, mirnega dela nad genialnostjo, ki jo poganjajo občutki"
(AF, str. 124). Res je, da je v okviru tega razmišljanja težko razločiti,
"katero od dveh načel življenja je primarno": ali "preprost, urejen, meščanski
red življenja ali enako mirna in trdna varnost, ki jo to življenje vzbuja v
duši" (AF, str. 130). Vendar ni toliko pomembno odgovoriti na vprašanje, temveč
preprečiti, da bi se že samo s tem, da je postavljeno, oddaljilo od prave poti,
ki še vedno ostaja samo ena: "Opravljanje dolžnosti: to je edina varna pot v
življenju" (AF, str. 130). 143). Za to trmasto, moško resignacijo pred zakonom
usode gotovo ni velike nagrade. Vendar pa je "v tem življenjskem ravnanju nekaj
trmastega in močnega, nekaj zanesljivega in togega ritma, kotna energija" (AF,
str. 128), v kateri se zrcali "moč odpovedi, moč resignacije, moč stare
buržoazije pred novim življenjem" (AF, str. 137).

In skratka, vse to pomeni, da sta "vedenje meščanskega življenja, njegovo
razumevanje v strogo meščanskem vrednostnem sistemu le sredstvi za približevanje
tej popolnosti. Gre za askezo, za odpoved vsemu sijaju v življenju, da bi se ves
sijaj lahko povrnil drugje, v drugih oblikah, v delu" (AF, str. 122).

Temeljno načelo, ki je že v eseju o Kassnerju ikastično izraženo s formulo
"življenje ni nič, delo je vse" (AF, str. 56), ki dokončno pojasni koncept
junaške sublimacije dela, značilne za meščansko etiko, in ga ponovno poveže
oziroma postavi v temelj veličine in avtonomije umetnosti in oblik, pri čemer je
slednja nedvomno privilegirano načelo duha, vendar ni izčrpna ali opravičljiva
izključno na estetskem področju. Zato je Štorm povezan s Flaubertom, vendar se z
njim nikakor ne ujema. Pri Flaubertu namreč "tehtnica življenja in dela visi na
strani dela, tu na strani življenja" (AF, str. 132). Kot smo že napovedali na
drugem mestu, Lukácseva eksistencialna vizija, ki se iz poezije spusti v
življenje, ostane bistveno nespremenjena, vendar se spremeni v etično držo; sama
forma pa iz urejevalnega načela umetnosti postane vrhovno urejevalno načelo
življenja. Izbira forme je torej vedno hkrati merilo presoje in vrednotenja,
bistveno in globoko moralna izbira: "Forma je najvišji sodnik eksistence" (AF,
str. 344). Toda vsebina te izbire, a priori presoje, pa naj bo še tako
sublimirana, zagotovo ne vznikne s spontanim čudežem samonastajanja iz
transcendentalne topografije duha. Še vedno so to tiste, ki jih sodobna
meščanska kulturna civilizacija zagotavlja na podlagi zgodovinskega procesa,
sestavljenega iz neskončnih ideoloških in intelektualnih posredovanj. V tem
smislu ima forma morda prvič opraviti z lastno in nenadomestljivo vsebino:
meščansko etiko dolžnosti.

In prav ta nenadomestljiva vsebina je tista, ki daje teoriji forme zadnji in
odločilni obrat. Etika dolžnosti je pravzaprav še eden od tistih nepremostljivih
grebenov, med katere se postavi in nujno izčrpa demonstrativna in utemeljitvena
moč mladega Lukácsa. Pravzaprav ni nič manj kot izhodiščna točka in izhodiščni
postulat. Tisto, kar ga podpira, je buržujska zgodovinska vera v samega sebe.
Uničite to vero in dolžnost ne bo več zadosten in zadovoljiv odgovor. Etika
dolžnosti, videna v svoji dokončni in dovršeni obliki, dejansko nima in ne more
imeti razlag ali razlogov, ki ne spadajo vsi in v celoti vanjo. S tega vidika
predstavlja navidezno nesporno koherentnost. Toda to je natanko ista železna
koherentnost, ki predseduje vsakemu dejanju buržoaznega duha, zaprtemu vase. In
Lukács tu pravzaprav ponovno predstavi model logične rešitve obravnavanega
problema, katerega nesmiselnost oziroma krhkost smo že pokazali. Dolžnost prav
tako opravičuje samo sebe. Herojska sublimacija meščanskega dela ni usmerjena k
nobenemu drugemu cilju, ne pozna drugih impulzov kot tiste, ki jih vsebuje
imperativ dela samega. Kajti takoj ko meščan postavi glavo zunaj sebe, ne najde
nič drugega kot bedo, obup in kaos. Svet okoli njega ne predstavlja niti ne
poseduje nobenega cilja, ki bi lahko upravičil boj. Ne pozabimo, da v tem jeziku
dolžnost pomeni biti. In v tem jeziku, kot smo rekli ob drugih priložnostih, je
človek le to, kar je. Pot te etike torej ne poteka od življenja do življenja,
temveč od a priori do a priori.

Življenje je v tem pogledu potrebno le zato, da se v celoti izrazi kakovost
odpora a priori. Sam poskus prenosa eksistencialne propozicije v svet v obliki
etične drže ima zaradi tega negotov in prehoden značaj. Glede na naravo svoje
vsebine se Lukácseva eksistencialna etika ne more obdržati v življenju - in
postati človekovo učinkovito vodilo -, ne da bi prav zato in takoj zahtevala od
subjekta, da se ponovno odtrga od življenja, k višjim in vsaj namerno
prepričljivejšim ciljem.

Ta način bivanja v življenju - ki ga imajo liki Storma, Philippa, Beer-Hoffmanna
- je torej tudi način bivanja proti življenju.

Sam po sebi pravzaprav nima ne smisla ne cilja. Imelo bi ga le, če bi Lukácsu
uspelo pokazati, da je onkraj ali nad tem načinom bivanja še totalnejši in
absolutnejši, takšen, ki je zmožen kvalificirati in upravičiti celo ravnanje
tistih, ki se gibljejo v vsakdanjosti, v empiričnem itd. To poskuša storiti v
svojem diskurzu o tragičnem.

## 10. Oblika kot utopija meščanske biti.

Ko se je morda že zdelo, da je diskurz zaključen, je vse postavljeno pod
vprašaj. In to nujno. Če je pravzaprav res, da koncept forme ne more brez svoje
ukoreninjenosti v meščanskem duhovnem univerzumu, - ki smo ga opisali na zadnjih
straneh, - je prav tako res, da ne bi mogel razsvetljevati tega obsežnega
območja človeške izkušnje, če se ne bi znal znova dvigniti v najvišje višave
esencialnosti in čistosti.

Koncept tragedije torej ni v nasprotju s tem, kar smo povedali doslej, temveč
predstavlja njegovo sublimacijo, potisnjeno tako daleč, kot lahko gre človekovo
najbolj popolno, brezpogojno poznavanje samega sebe, tj. svoje usode.

"Gole duše se osamljeno pogovarjajo z golimi usodami. Obema je bil odvzet ves
odpadek in ostalo jima je njuno intimno bistvo; vsak eksistencialni odnos je bil
izbrisan, da bi se vzpostavil usodni odnos; vsak atmosferski element, ki ovija
ljudi in stvari, je izginil in ostala je le ostra, kristalna gorska avra, ki
riše ostre obrise njihovih vprašanj in odgovorov" (AF, str. 311). "Tragedija ima
samo eno dimenzijo: višino" (ibid.). "Dramska tragedija je oblika višin bivanja,
njegovih končnih ciljev in skrajnih meja" (AF, str. 319). S temi izjavami se
celotna pozicija ponovno zažene v stanje skrajne napetosti, v katerem so pravice
posameznika, - izoliranega in junaškega predstavnika biti, - potisnjene do točke
recitala čistega egoizma.

Celotno stališče je ponovno sproženo. Toda v kakšnem smislu? V povsem drugačnem
smislu kot doslej. Ali je esej o metafiziki tragedije po naravi podoben drugim,
ki smo jih prej analizirali? Navkljub videzu nikakor ni. Tu je priložnost eseja
- stilizirane in klasične tragedije Paula Ernsta - bolj priložnost kot v katerem
koli drugem primeru.

Narava eseja o metafiziki tragedije ni analitična in interpretativna: je
hipotetična in propozicijska. Esej namreč, izhajajoč iz šibke priložnosti,
predstavlja predlog za razvoj moderne umetnosti, ki tokrat šteje prav zato, ker
še ni našel svoje uresničitve ("Doslej to še nikomur ni uspelo - vendar to ne
pomeni nič v zvezi z možnostjo rešitve problema") (AF, str. 343).

Na ta način se lukacjanski diskurz, oropan demonstrativne opore, razkrije veliko
bolj kot v preostalem delu knjige. In prav z izhajanjem iz te teme tragičnega
lahko morda pridemo do nekaterih sklepov, na podlagi katerih bomo lahko
presojali stališče mladega Lukácsa v celoti.

Lukács je prepričan, da je sodobna zgodovinska situacija oziroma
transcendentalna topografija moderne duše ugodna za preporod tragedije, -
tragedije, naj bo jasno, ki je ne moremo enačiti niti z meščansko dramo 19. in
20. stoletja niti s tisto mešanico klasičnega in modernega, ki sta jo izvajala
že Goethe in Schiller, saj si prizadeva za bistveno bolj, celo bolj togo in
surovo izražanje duše: "Danes lahko ponovno upamo na prihod tragedije, saj še
nikoli prej narava in usoda nista bili tako strašno brezdušni, še nikoli prej
človeške duše niso potovale po svojih zapuščenih poteh v takšni osamljenosti;
upati je mogoče na vrnitev tragedije, ko bodo popolnoma izginili negotovi
fantomi ugodnega reda, ki jih je strahopetnost naših sanj projicirala na naravo,
da bi si ustvarila iluzijo varnosti" (AF, str. 309). Še pozneje, v Teoriji
romana, ko primerja različne "možnosti" bivanja žanrov v antični Grčiji in
moderni dobi, Lukács ugotavlja, da ima tragedija v sebi še vedno pogoje, da
ponovno pridobi (kar tu pomeni: ohrani) svojo prvotno (tj.: večno) avtentičnost:
"Toda medtem ko je v najmanjših delčkih korelativnosti vitalna imanenca smisla
neizogibno obsojena na izginotje, se lahko bistvo, ki je daleč od življenja in
tuje življenju, tako okrona s svojim lastnim obstojem, da bo ta posvečenost tudi
od največjih pretresov kvečjemu zakrita, nikoli povsem izbrisana. Zato je
tragedija, čeprav se je preoblikovala, ohranila svoje bistvo nedotaknjeno tudi v
našem času, v katerem je moral ep izginiti in narediti prostor povsem novi
obliki, romanu" (TR, str. 70).

Toda kje je moderna tragedija, ki bi jo Lukácseva
formalistično-eksistencialistična hipoteza ne le lahko, ampak jo je morala imeti
za možno in potrebno, če naj sistem oblik ne ostane nedokončan, če naj ne najde
svoje odločilne kronske točke, tiste točke prihoda, ki vdihne vso prehojeno pot
in jo upraviči?

Zdaj ne more biti naključje, da je osnovna hipoteza Lukacijevega diskurza tudi
tista, ki ne najde izpolnitve ali, rekel bi, približka. Zakaj tragedija, čeprav
je ohranila svoje bistvo, ni dobila svoje moderne oblike?

Odgovorili bomo z drugim vprašanjem: morda prav zato, ker je ohranila svoje
bistvo?

Obrnimo vprašanje in se ga lotimo z drugega zornega kota.

Iz tega, kar so o mladem Lukácsu zapisali nekateri celo inteligentni razlagalci,
bi iz njega izpeljali lik katastrofičnega preroka apokalipse, ki je ves nagnjen
k uničenju in smrti. Verjamemo, da smo tak vtis med našo raziskavo točko za
točko razbili; zdaj pa je treba še jasneje povedati, da je splošna in
kvalifikacijska oznaka knjige z naslovom Duša in oblike pozitivna, da je ta
knjiga pozitivna knjiga.

Globok in včasih moteč preplet argumentacije ne izhaja le iz kompleksnosti
obravnavanih problemov, temveč tudi iz same narave diskurza, v katerem stopa v
ospredje in se zapleta v vozle bistvena dialektika med bitjo in postajanjem, med
absolutnim in relativnim, med možnostmi spoznanja in skepso, med izgubljeno vero
v zgodovinske vrednote in iskanjem vere v splošnejše, abstraktnejše,
absolutnejše vrednote, vendar ne brez življenjskega človeškega naboja.

To je po mojem mnenju dialektika, kot se zastavlja v velikem sektorju nemške
kulture zgodnjega 20. stoletja, med v določenem smislu pridobljenimi rezultati
negativne teoretske spekulacije in potrebo po utemeljitvi novih vrednot. Ponovno
moramo omeniti imeni Diltheya in Webra, na literarni ravni pa Thomasa Manna.

Vendar to ne pomeni, da Lukács oblikuje stališče relativnega kompromisa med
nasprotujočima si zahtevama. Na tej stopnji še ni dialektik (v pravem pomenu
besede). Krizi in negativnosti nasproti postavi ponovno potrditev absolutnega:
absolutnost oblik, predvsem kot zadnji obrambni greben meščanske umetnosti, pa
tudi kot zadnje sredstvo za razrešitev eksistencialnih nasprotij. Lukács je res
zapisal: "Forma je edini način, kako v življenju doseči absolut" (AF, str. 69);
vendar ni pozabil dodati: "Vsaka forma je sestava substancialne disonance biti"
(TR, str. 96).

Tako odkrijemo še zadnjo implikacijo Lukacijevega diskurza: Forma kot utopija
meščanske biti, - kot ena od mnogih utopičnih inkarnacij meščanske biti, morda
temeljna. Forma kot pojmovna meja tega, kar naj bi bilo; kot, to je treba reči,
popolna platonska ideja, postavljena onkraj negativnosti, krize, raztrganosti in
ločenosti, kot svetilnik, ki osvetljuje razburkano morje, čeprav nima moči, da
bi ga umiril; Forma kot izraz absoluta, ki bistveno leži onkraj vseh stvari in
je zato (glej primer tragedije) sama po sebi popolna, dovršena in enotna.

Ni naključje, da se knjiga z naslovom Duša in oblike, ki je, kot vemo, v celoti
posvečena analizi razmerja med problematiko eksistence in sublimacijo oblik,
konča s hipotezo, ki je hkrati tudi prerokba, in kot vse prerokbe je tudi
utopija, v kateri se živo prepletanje polov, ki smo ga analizirali med našim
razlaganjem, razpusti in onemogoči, da bi ostal nepoškodovan le eden, najvišji,
a tudi najbolj oddaljen in neizvedljiv. Celo pojem problematičnosti je v tem
pogledu zastarel: "Ernst postavlja ta zaprti, končni, višji svet kot opomin in
opomin, kot svetlečo referenčno točko za pot človeštva, ne da bi ga zanimala
njegova dejanska uresničitev. Veljavnost in moč etike sta neodvisni od njenega
spoštovanja. Zato lahko le oblika, ki je očiščena do stopnje etičnosti - ne da
bi zaradi tega postala slepa in revna -, pozabi na obstoj vsake problematičnosti
in jo za vedno izžene iz svojega kraljestva" (AF, str. 347). To so zadnje besede
Duše in oblike.

## 11. Konec problematičnosti in razpustitev meščanskega junaka.

Toda ali ni konec problematičnosti - kot smo skušali pokazati v tem eseju - sam
konec buržoaznega junaka? Tu smo resnično predani razumevanju paradoksa
paradoksov v Lukacijevi argumentaciji. Po eni strani je bilo namreč rečeno, da
je Forma absolut meščanske biti in hkrati njena utopija. Po drugi strani pa je
bilo opozorjeno, da je bistvo moderne forme tragedija. Iz tega bi lahko
sklepali, da je forma-utopija meščanske biti tragedija. Toda ta zaključek je v
trku z nekaterimi transcendentalnimi predpostavkami Lukacijevega diskurza in,
dodali bi, z nekaterimi njegovimi dejanskimi zgodovinskimi in ideološkimi
vsebinami. Utopija se dejansko pojavlja le v podobi nenehnega ponovnega
napredovanja meščanske biti in navsezadnje kot njena končna ideološka varovalka.
Toda, kot pokaže sam Lukács, ta proces ni in ne more biti ad infinitum, temveč
mora imeti izhod. Toda ali ima meščanski junak (kot, če navedemo enega od
neskončno možnih primerov, opisuje Lukács njegovo delovanje v delih Storma)
lastnosti, da postane tragični junak? Ali pa ga teža njegove "človeškosti"
(tega, da je popolnoma meščanski) vodi navzdol, v ravno nasprotno smer od tiste,
v katero bi moral iti, da bi postal tragičen?

Na tej točki ostaja le še eden od dveh skrajnih polov Lukacijevega diskurza:
tisti, ki zadeva Novalisa in Kierkegaarda. Tam je bil diskurz v svoji
drastičnosti resnično jasen. Zaključek je bil lahko samo en: smrt ali, bolje
rečeno, samouničenje. Lukács se je premaknil prav s te točke, v iskanju
nasprotnega ekstremizma, ki bi bil zmožen uskladiti življenje in napetost. Toda
na tej drugi strani - na strani razrešitve in propozicije - je diskurz ostal
nedokončan, to pomeni, da je razkril svojo nezmožnost uresničitve v predlogu za
pozitivno rast moderne umetnosti.

Vrnimo se k trditvi, ki smo jo izrekli na neki točki našega diskurza: tragedija
ni imela moderne oblike morda prav zato, ker je ohranila svoje bistvo. Kaj to
pomeni? Pomeni, da so tiste oblike, ki niso bile podvržene procesu degradacije,
ostale čiste, a neuresničene. Skratka, ni dovolj, da obstajajo abstraktni pogoji
tragedije, da se tragedije lahko zgodijo: najti je treba tudi like, ki so jih
sposobni živeti. Lukácsu se je zdelo, da je svet naokoli pripravljen sprejeti
tragično "ponudbo". V resnici pa se je meščanstvo nagibalo k nečemu drugemu,
skladno s predpostavkami svoje ideološke in duhovne formacije. Ali so se borili
za svoje materialno preživetje in se tako pripravljali na to, da postanejo
intelektualci, organski za kapitalistični razvoj, ali pa so se prepuščali sami
sebi in s tem dosegali zadnje, velike spoznavne rezultate, ki so jim bili dani.
Edino, česar niso znali in zmogli, je bilo to, da so se postavili v tragično
napetost proti svojemu svetu: etika dolžnosti je svetovala moško resignacijo, ne
upora.

Toda mladi Lukács tega ni povsem razumel. Še tako občutljiv za negativnost
empiričnega bivanja, vendar ni mogel videti, v kolikšni meri vdira tudi v
življenje absolutnega. Njegov platonizem se na koncu zaključi s ponovno
potrditvijo načela. Tudi problematičnost lahko v njegovi misli brez razpok najde
formalno mesto.

Tisto, česar Lukács ni dojel, je bila prav hipoteza o postopni, naraščajoči
degradaciji absolutnega v njegovi dvojni eksistencialni in formalni podobi.

Stališče, ki ga je Lukács izrazil v Duši in oblikah, nam namreč le deloma
omogoča boljše razumevanje loka moderne umetniške izkušnje, ki se razteza od
začetka 20. stoletja do danes in vključuje morda nekatere največje osebnosti
našega časa. Najbolj pretresljive in življenjske izkušnje moderne umetnosti se
začnejo takrat, ko je en bloc s celotnim problemom človeškega obstoja postavljen
pod vprašaj celo zadnji preživeli absolut: absolut oblike.

Toda oblike, ki so, da bi preživele, privolile v proces degradacije, so se v
njem polastile. Namesto da bi se korak za korakom vzpenjale do vzvišenosti
tragičnega, so se prek vseh stopenj groteske, parodije, pastiša spustile
navzdol, da bi se v mnogih primerih preprosto odrekle absolutnosti in se kot
oblike ponovno predstavile kot epizodične, naključne, priložnostne.

Ta pojav, ki je, če pomislimo, le obrnjena izvedba Lukácseve utopične hipoteze,
je mladi Lukács prezrl, ni ga predvidel in ga morda dejansko ni mogel vedeti ali
predvideti. Lukács v zrelih letih in starosti jo je poznal le zato, da bi jo
zavrnil in obsodil. Ker je idealno prišel pred in po zgodovinski priložnosti za
razmislek, ki jo je ponudil veliki pojav meščanske umetnosti, ki gre z vstopom v
krizo tako daleč, da zanika predpostavke, na katerih temelji, se Lukács v tem
delu kvalificira kot teoretik meščanske umetnosti v najbolj čistem in najvišjem
pomenu besede. Dejansko se predstavlja kot vztrajen zagovornik vrednot.

Je na pragu krize, vidi vse njene predpostavke in vse njene simptome, a še vedno
verjame, da se ji je mogoče zoperstaviti z zakonom in vedenjem, ki v svoji
absolutnosti nadomestita vse rane in razpoke eksistence.

V tem smislu je resnično na liniji - v morda bolj pristni obliki kot pozneje -
osebnosti, kot sta Goethe in Schiller.

Skratka, njegovo pojmovanje oblik še vedno ohranja klasični pomen tega izraza
(in tu imamo seveda opraviti z meščanskim klasicizmom): čeprav ga Lukács, kot je
postalo jasno iz našega celotnega razglabljanja, domiselno izpostavlja vsem
tveganjem eksistence in ga s tem napolni s prav posebno, neponovljivo izrazno in
življenjsko intenzivnostjo. Njegova metoda gledanja na stvari skozi umetnost,
njegov predlog "posrednega spoznavanja" sodobne stvarnosti, ni mogla iti tako
daleč, da bi z dejanskim odkritjem nove resnice ovrgla začetni skepticizem. Sama
"forma" je bila aluzija na drugo-od-sebe, ki je ostala nenehno nepremagljiva,
impregnirana.

V igri ogledal, ki jo je sprožil, se je na koncu vedno zrcalil isti obraz, ista
strastna gesta, vendar razlomljena na pol. Vendar se je iz teh zunajempiričnih
in transcendentalnih soočenj, če ne nova, pa stara resnica, resnica stare
meščanske duše v krizi, izluščila. Vsekakor pa to ni vsa resnica, ki bi jo bilo
mogoče povedati o njej. Je pa pomemben del resnice, ki bi jo meščan lahko
odkril, ne da bi se odpovedal temu, da je meščan. Za tem se začne proces
razkrajanja, ki je bil omenjen. Ali pa nujno proces reintegracije, še bolj
izrazit in odločilen od tistega, ki je bil napovedan že v Anima e le forme.
Teorija romana je že izbira med tema dvema nasprotujočima si alternativama.

## 12. Roman kot pozitivna in "integrirana" zvrst.

V Teoriji romana problem zgodovinske rekuperacije nekaterih meščanskih
umetniških oblik naredi še en korak k sklepom Duše in oblike. Ne bi rekel, da se
vsebina stališč, ki jih zagovarja Teorija romana, razlikuje od tiste, ki jo
obravnavata Duša in oblike. Pojmi specifičnosti, problematičnosti in
avtentičnosti se tudi tu vračajo kot bistvena podpora in ozadje Lukácsevih
analiz moderne umetnosti. Vendar jim Lukács nameni drugačno teoretsko in
metodološko ureditev, ki transcendentalna načela, ki ostajajo analogna ali
sorodna, pripelje do temeljito drugačnih sklepov.

Da bi izrazil smisel tega razmerja-diferenciacije med svojima zgodnjima deloma,
je Lukács sam zapisal, da je v Teoriji romana operiral "kierkegaardovsko
interpretacijo heglovske zgodovinske dialektike "4. Če bi morali v podobno
formulo ubesediti vtis, ki ga je v nas sprožila analiza razmerja med načelnimi
stališči in njihovo teoretsko ureditvijo v Teoriji romana - tudi ob upoštevanju
ideološke poti, ki jo je Lukács dejansko prehodil od svojega prvega do drugega
dela -, bi rekli, da Lukács zdaj izvaja "hegeljanizacijo kierkegaardovskega
eksistencialističnega stališča". Sodba o "položaju časa" in o "problematičnem
posamezniku" ostaja pravzaprav skoraj povsem nespremenjena (pravzaprav je še
dodatno poglobljena z nekaterimi lepimi analizami problematičnosti moderne
duše); drugačni so odgovori, s katerimi Lukács tu poskuša bolj realistično in
bolj pozitivno povezati opisani eksistencialni položaj z nekaterimi dejanskimi
možnostmi moderne umetnosti.

Tragična perspektiva torej pade kot najvišja točka in s tem kot končni in
splošni preobrat celotnega sistema oblik.

V ospredje stopi roman kot literarna vrsta, za katero je značilna notranja in
organska strukturna problematika in ki zato najbolj ustreza notranji
problematičnosti moderne duše ter je, strogo gledano, v bistvu rezultat procesa
njenega izraznega samozavedanja. "Roman," piše Lukács, "je epopeja dobe, za
katero obsežna totalnost življenja ni več čutno dana, za katero je vitalna
imanenca smisla postala problematična in ki kljub temu hrepeni po totalnosti"
(TR, str. 89); "... je oblika pustolovščine vrednote, lastne notranjosti; njena
vsebina je zgodba duše, ki se tu začne prepoznavati, ki išče pustolovščine, da
bi v njih našla lastno bistvenost" (TR, str. 132). Zato so "romaneskni junaki
[...] iskalci" (TR, str. 95); zato je biografija eminentno romaneskna oblika
(TR, str. 117); zato je ironija, "najvišja svoboda, ki je mogoča v brezbožnem
svetu" (TR, str. 137), prav tisti pogled, s katerim romanopisec opazuje in
predstavlja svet.

Ko je tako vzpostavil to popolno enačbo med literarno zvrstjo in duhom - enačbo,
ki je bila na splošno napovedana tudi v Ani ma e le forme, vendar na veliko bolj
posreden in posreden način -, Lukács izpelje posledico, da med romanom in
moderno dušo obstaja nujna korespondenca, ki jo lahko posledično spremeni le
modifikacija časa (k tej točki se bomo vrnili pozneje).

Na ta način Lukács veliko bolj kot v Duši in oblikah poudari bistveno pozitivno
funkcijo (kot tudi bistveno pozitivno razmerje med umetnostjo in resničnostjo)
umetnosti v odnosu do resničnosti. Ni naključje, da Lukács v Teoriji romana
izreče formulo, ki smo jo citirali že prej, po kateri je "vsaka forma sestava
substancialne disonance biti" (TR, str. 96); v istem delu jo potrjuje še
eksplicitnejša trditev: "Vsaka forma mora biti na nekem mestu pozitivna, da bi
kot forma pridobila substanco" (TR, str. 172). Skratka, roman s tem, ko vase kot
žanr vključi objektivne probleme sodobnega sveta, postane tudi njegov razlagalec
in sodnik, pa tudi njegov odsev, in se zato izkaže za sposobnega, čeprav s
svojimi posebnimi instrumenti, ki jih Lukács zelo dobro opisuje, sprožiti proces
samospoznanja in samozavedanja, katerega končni rezultat je ponovna vključitev
subjekta (predvsem romanopisca, nato pa tudi bralca) v odnos do sodobne
stvarnosti.

Da bi dosegel ta rezultat, mora Lukács raziskovanje "transcendentalne
topografije" duha, kot je to storil v Duši in oblikah, nagniti do te mere, da ga
spremeni v "filozofijo zgodovine" literarnih zvrsti, osredotočeno zlasti na eno
od njih, roman, vendar bogato z navezavami, v tem smislu tudi na problematiko
epa in tragedije.

Pri tem je treba razumeti globoko razliko med teorijo oblik in teorijo
literarnih zvrsti. V prvem primeru oblike izražajo odnos med ustvarjalnim
navdihom in mnoštvom krajev duha, od katerih ima vsak v resnici le en način,
kako priti do svoje avtentične in potrebne oblike, vendar se prav zato od časa
do časa predstavi s svojim avtonomnim značajem in položajem, ki ga je težko
  posnemati in ponoviti; v drugem primeru pa literarni žanri že predstavljajo
  subsumpcijo v zgodovinsko-filozofski ključ (domnevno) homogenih elementov
  duha, s katerimi se ustvarjalni navdih zato sooča kot z že samimi po sebi
  opremljenimi objektivnimi notranjimi zakoni delovanja, katerih bistvena
  značilnost je tipičnost in posledično ponovljivost. Ko je enkrat podana
  najbolj funkcionalna shema ali model interpretacije-reprezentacije sodobne
  stvarnosti, nič ne preprečuje, da bi jo prevzeli in prakticirali s predhodnimi
  možnostmi uspeha, spričo česar isti eksistencialni problem sodobnega človeka
  izgubi svoj dramatični naboj, to je globok občutek lastne negotovosti in
  nestabilnosti. Roman je tako hkrati privilegirana izrazna oblika in rešilna
  bilka sodobne meščanske umetnosti.

Lukács je do takšnega uvida lahko prišel le tako, da je v svoje razmišljanje
vpeljal pojem, za katerega tudi prej ni bilo mogoče reči, da je povsem odsoten,
vendar šele zdaj dobi teoretično opredeljeno obliko in postane popolnoma
operativen: pojem totalnosti. Že v Duši in oblikah je, kot smo videli, deloval
določen koncept totalnosti: vendar je tam deloval prav kot koncept-omejitev,
cilj, ki ga je nenehno zasledoval in nikoli dejansko dosegel ali pa ga dosegel
in kmalu izgubil: torej neuresničljiva popolnost, ki je Sehnsucht vendarle
spodbujala in ga zavezovala k ustvarjanju.

Po drugi strani pa v Teoriji romana pojem totalnosti nastopa kot dejansko
(objektivno) prisotno razmerje med nasprotujočimi si elementi resničnosti, ki ga
je zato mogoče z jasno zavestjo dojeti in opredeliti, in je torej cilj, ki ni
hipotetičen, temveč dosegljiv, če ustvarjalcu uspe najti (in ima možnost)
notranje povezave, specifična razmerja med predmeti svojega navdiha. Roman se
kvalificira kot moderni ep prav zato, ker je iskanje (in na koncu tudi ponovna
osvojitev) izgubljene celote: "Ep prikazuje vitalno celoto, ki je zaprta vase;
roman skuša s svojimi upodobitvami odkriti in osvetliti skrito celoto življenja"
(TR, str. 94). Teorija romana predstavlja vzorčni primer, kako objektivni
idealizem ni vedno avtomatično bližje realnosti meščanskega bivanja kot t. i.
subjektivni idealizem.

Prvo dejstvo, ki ga je pri tem treba opaziti, je, da "sistematizacija" podatkov
življenjskega izkustva in estetske refleksije pomeni zaostritev - in ne
obogatitev - problematike, obravnavane v Duši in oblikah (vsaj na ravni možne
izdelave teorije meščanske umetnosti). In togost ali shematizem podatkov
neizogibno vodi v k oddaljevanju od dejanske realnosti meščanske umetnosti in k
poskus univerzalne formulacije problematike zvrsti, pri katerem se prepogosto
pojavi vtis, da so primerjalni pojmi preveč oddaljeni drug od drugega in odkrito
nekomunikativni.

Neksus Cervantes-Flaubert-Goethe, na katerem sloni celotna Lukacsejeva
demonstracija, ni učinkovit, če pogledamo vsebino del treh preučevanih avtorjev:
smiseln je le kot demonstracijski temelj abstraktne, a nič manj toge tipologije.
Nič ne bi preprečevalo, da se ob natančnejšem pregledu kvantitativno obogatijo
tu preučevani tipi. Le če bi se to zgodilo, bi to zmanjšalo moč predloga: Lukács
si tu ne prizadeva za popolno identifikacijo oblik meščanske biti; tu si
prizadeva identificirati le tiste, ki so dejansko izvedljive v pozitivni
perspektivi. Kar se ne prilega shemi, celo izgubi možnost, da bi bilo avtonomno
in na druge zanj posebne načine smiselno in reprezentativno: tak je primer s
prenagljeno likvidacijo Gogolja. Ali pa, ko sheme ni mogoče uporabiti, v
Lukačevem argumentu ostane praznina. Kam se v tukaj razviti teoriji romana
umeščajo Buddenbrookovi (in z Buddenbrookovimi vsi romani, ki temeljijo na
razmerju usod)? Ali pa naj zaradi dejstva, da jih Lukács ne omenja, sklepamo, da
so od problematike meščanske duše oddaljeni bolj kot vsi tipi, ki jih obravnava
Lukacsov shematizem?

Druga stvar, ki se jo je treba vprašati, je, ali je koncept totalnosti, kot ga
tu uporablja Lukács, treba šteti tudi za kvalitativno višjega, celovitejšega ali
zmožnega podajanja višjih stopenj izraznosti, kot bolj ali manj zavestno
netotalen odnos, tj. sam po sebi razcepljen in ločen. S tega vidika je
primerjava med Novalisom in Goethejem poučna. Ta primerjava je bila opravljena
že v Duši in oblikah in že tam se je tehtnica veličine nagnila na Goethejevo
stran. Toda predmet analize, "izbrani avtor", je bil še vedno Novalis, kar ni
ostalo brez odločilnih posledic za razvoj argumenta, tako v tem posameznem eseju
kot v knjigi kot celoti. Goethe je v njem odigral vlogo, ki mu je v nemški
kulturi devetnajstega in dvajsetega stoletja pogosto pripadala, vlogo
olimpijskega boga, s katerim se vse primerja, ki pa ostaja nekako v ozadju ali v
zakulisju. Lukács je priznal, ni si mogel kaj, da ne bi priznal, da je Goetheju
uspelo v sebi uskladiti tiste sile, ki so pri romantikih eksplodirale in
proizvajale le anarhijo in smrt. Vendar ga je veliko bolj zanimalo razumeti,
zakaj in kako je Novalis poskušal preseči Goetheja in v kolikšni meri mu je to
uspelo.

Pri tem še ni imel vnaprej določene hierarhije rezultatov: vsakemu, tudi
Novalisu, njegove možnosti, začenši z odločnim odnosom duha: potem se bodo
pokazali rezultati.

V Teoriji romana je merilo ravno obratno: Lukácsa zdaj zanima predvsem
razumevanje, zakaj Novalis nikoli ni mogel doseči rezultatov, kot jih je dosegel
Goethe. Zgodovinsko-filozofski a priori, skratka, pogojuje poezijo: "... ker je
Goethejev način, usmerjen v odkrivanje ravnotežja, ki je tu ironično nestabilno,
ki se vzpostavlja iz subjekta in, kolikor je mogoče, ne prejudicira podob,
Novalis zavrnil, se Novalisu samemu ne odpira nobena druga pot, kot da podobe
lirično poetizira v njihovi objektivni biti, in s tem ustvariti svet, ki je lep
in harmoničen, vendar ne izhaja iz samega sebe, ki je brez povezav in ki je
povezan tako s transcendenco, ki je končno postala resnična, kot s problematično
notranjostjo le refleksivno, le atmosferično, ne pa epsko, in zato ne more
postati prava totalnost" (TR, str. 201). 201).

## 13. Zgodovinska utopija in kulturna rekuperacija.

Toda ali tako postavljena primerjava Novalis-Goethe ne izraža apriornega
ocenjevalnega kriterija, po katerem se sposobnost videti več stvari v
medsebojnem razmerju že predstavlja kot spoznavna drža, superiorna tisti, ki
vidi samo eno stvar hkrati ali pa vse vidi enostransko? "5

In ali se ne pojavi že hipoteza, na kateri bo Lukács zgradil velik del svojega
zrelega stališča, da tej kognitivni superiornosti naravno ustreza superiorna
izrazna zmožnost? Lukács v Duši in oblikah je že postavil in na svoj način rešil
ta problem, ko je trdil, da je estetski rezultat bistveno pogojen s stopnjo
napetosti, ki jo je ustvarjalčeva duša sposobna v sebi vzbuditi. Vsak formalni
rezultat, vsaka oblika, je bil torej absolut, ki ga je bilo težko primerjati z
drugimi izkušnjami različnega eksistencialnega izvora, predvsem pa ga je bilo
težko meriti z zunanjimi parametri. Perspektiva se povsem spremeni, ko v
oblikovanje sodbe, bolj ali manj posredovano s filozofijo zgodovine, posežejo
prav ti zunanji parametri. Tudi tu je treba razumeti, da med poezijo in realnost
ni povsem enako postaviti transcendentalno topografijo duha ali filozofijo
zgodovine. V slednjem primeru namreč razmerje med stanjem časa in estetskim
izrazom postane veliko tesnejše in bolj pogojeno kot v prvem. Kot zapiše Lukács:
"Kontingentni svet in problematični posameznik sta vzajemno pogojujoči
realnosti" (TR, str. 117).

Posledično, kot se pogosto zgodi, ko analizo konkretnih zgodovinskih pojavov,
kakršne koli narave, podredimo okviru splošnih idej, izpeljanih iz filozofskega
apriorija, se v okviru metafizike duha na koncu ponovno odkrije predlog
sociologije problema: v tem primeru sociologije literature. Ni naključje, da
Lukács za cezuro med prvim in drugim delom knjige postavi naslednje izjave:
"Ironija kot samoprevara subjektivnosti, ki se konča, je najvišja svoboda, ki je
mogoča v brezbožnem svetu. Zato ni le edini možni apriorni pogoj konkretne
objektivnosti, tvorca totalnosti, temveč to totalnost, roman, povzdigne v
reprezentativno obliko epohe, kolikor so konstitutivne kategorije samega romana
konstitutivno utemeljene na stanju sveta" (TR;str. 137). Lukács je z
vzpostavitvijo tovrstnega pogojujočega razmerja odprl pot problematiki kulturne
rekuperacije, ki pa je temeljila na angažirani, progresivni viziji zgodovinskega
razvoja. Utopičnost Duše in oblik na tej stopnji pravzaprav še ni bila opuščena,
vendar se je tudi ona zdaj predstavila v historizirani in dialektični obliki
(kot razmerje med mesijansko vizijo in dejanskimi možnostmi preoblikovanja
sveta).

S to grozečo novostjo, ki je bila še vedno negotova in negotova, vendar ne
povsem odsotna, je bila tako povezana prihodnost moderne umetnosti in njeno
preseganje ali preobrat. Še enkrat je izjemno pomenljiv zaključek knjige, kjer
se zdi, da se diskurz, izhajajoč iz sodbe, ki jo je izoblikoval Dostojevski,
odpira v brezmejna obzorja zgodovinsko-politične prerokbe: "Ali je on
[Dostojevski] že Homer ali Dante tega sveta, ali pa tisti, ki zgolj daje
kantose, ki jih bodo kasnejši pesniki, tudi po drugih predhodnikih, sestavili v
veliko enotnost: ali je šele začetek ali že izpolnitev: tukaj je nekaj, kar bo
lahko razkrila šele formalna analiza njegovih del. In šele potem bo lahko naloga
zgodovinsko-filozofske razlage nebeških znamenj, da pove, ali smo res na tem, da
opustimo stališče absolutne grešnosti, ali pa gre le za navadno upanje, ki
napoveduje prihod novega: znamenja prihoda, ki so še tako šibka, da jih lahko po
mili volji, za zabavo, zdrobi neuspešna moč tega, kar preprosto obstaja" (TR,
str. 127).

Tu je še en preskok v diskurzu, tokrat odločilen. Se spomnite uvodne izjave?
Sprememba časa je povzročila spremembo oblik. Toda zdaj, ob zaključku diskurza,
ni šlo več za to, da bi upoštevali, kaj se je zgodilo v velikih posvetnih
mutacijah in cikličnem gibanju epoh. Zdaj je bilo treba najti konkretno,
neposredno stično točko, na katero bi lahko obesili razjedeno vrv moderne
umetnosti. Usmerjenost v prihodnost je tokrat pomenila diskvalifikacijo intimnih
potencialov duha in zanašanje na zunanje sile. Spremembo oblik je lahko
povzročila le sprememba časa. Ker pa se sprememba časov temu naraščajočemu kvasu
palingenetičnih in utopičnih upov zdaj ni zdela nemogoča, se je sprememba oblik
manifestirala tudi v redu človeških možnosti. Prihodnost naj bi razrešila
pričakovanja tako, da bi objektivno premagala pogoje krize. Toda prenos upanja z
ravni oblik na raven zgodovine je zdaj spremenil perspektive lukačevske presoje
in jih usmeril v takšno držo, ki jo je lahko pozneje izpolnilo le praktično
revolucionarno prizadevanje. Kriza buržoazne kulturne civilizacije potem ne bi
bila več vidna od znotraj, temveč od zunaj. Vendar pa ne v tem ni meja Lukacseve
argumentacije v Teoriji romana. Tu je meja v identitetni zamenjavi med ravnijo
formalne utopije in zgodovinske utopije. V samem dejanju, v katerem je Lukács
postavil nujno razmerje med tema momentoma, je dejansko podelil buržoazni
umetnosti zadnjo možnost, zadnjo (do tedaj nepredstavljivo) možnost okrevanja. S
tem ko je umetnost zasidral v usodo zgodovine, je moderni poeziji pokazal tisto
pot odrešitve, ki je tragična perspektiva ne le ni zagotovila, temveč jo je še
bolj odmevno onemogočila s patentiranim dokazom lastne neizvedljivosti. V
resnici je Lukács s tem izgubil globok pomen, najbolj pretresljiv in zrel
rezultat, dosežen v Duši in oblikah: prepričanje in hkrati dokaz, da med
meščansko umetnostjo in svetom, med estetskim ustvarjanjem in zgodovinsko
resničnostjo ni ne sporazuma ne miru, temveč obstaja globoko nasprotje,
nepopravljiva odtujenost.

Naš sklep o teoriji romana je torej analogen tistemu, ki smo ga oblikovali o
duši in oblikah. Vendar s to pomembno razliko. V Duši in oblikah je bila nemoč
Lukácsevega diskurza, da bi se "zaprl", tudi znak odlične diagnoze, ki jo je v
tej knjigi postavil o značajih in usodi moderne umetnosti: v trenutku, ko je
Lukács sodil druge, je sodil samega sebe, razlagalca in sodnika, predvsem pa
pričevalca svojega časa. V Teoriji romana se Lukácsev diskurz ne "zapre" prav
zato, ker ga hoče za vsako ceno ohraniti odprtega in si prizadeva za
"prijateljski dogovor" (čeprav problematičen) z zgodovino. V Teoriji romana -
kljub globokim razlikam - že kroži vzdušje zrelega Lukácsa. Umetnost zanj
ponovno postane instrument družbenega posredovanja, čeprav na zelo visoki ravni.
"Progresivna" perspektiva bo pomen tega procesa le še izpopolnila in izpolnila.

[1968]

1 Večina naših citatov je vzeta iz naslednjih italijanskih izdaj del G. Lukácsa:
L'anima e le forme, prevedel S. Bologna, Milano 1963; Teoria del romanzo,
prevedel F. Saba Sardi, Milano 1962. V besedilu ju bomo imenovali AF in TR.

2 Ko govori o enem od protagonistov Marie Donadien, Philippe zapiše: "Hrepenenje
ga je utrdilo in okrepilo. Pustil jo je nemo in jokajočo, razbito, tresočo se od
bolečine, zdaj pa ima zanesljivo moč, da se ji odpove. Moč, da je zloben in trd.
Kajti uničil je njeno življenje" (AF, str. 202).

3 Delo Maxa Webra Protestantska etika in duh kapitalizma je bilo prvič
objavljeno v letih 1904-05.

4 V predgovoru k Die Theorie des Romans, Neuwied, Luchterhand, 1962.

5 Spomnimo se, da je Lukács v Duši in oblikah zapisal: "Ena od Kassnerjevih
prednosti je v tem, da ne vidi toliko stvari" (str. 53).


---
lang: sl
...
