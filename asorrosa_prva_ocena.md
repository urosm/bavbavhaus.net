---
title: "Aberto Asor Rosa in Massimo Cacciari, Prva ocena"
...

<!-- Quando, all'incirca un anno fa, questa rivista fu pensata e voluta, una
precisa previsione di classe ne ispirava la genesi e le motivazioni.
Certo, nessuno avrebbe potuto allora presagire nei particolari ciò che
sarebbe accaduto in Europa e nel mondo nei mesi successivi:
dall'esplosione della violenza studentesca alla Francia alla
Cecoslovacchia. Tuttavia, qualcosa c'era che c'indusse a credere
possibile e necessario un organo di riflessione teorica come questo. La
catena di lotte operaie, che ininterrottamente avevano scosso l'Europa
dal 1960 in poi, costringendo il capitale ad un affannoso inseguimento e
a paurose convulsioni, -- mai un momento di stasi, mai un periodo di
effettivo controllo ed equilibrio, -- rendeva facile e fondato il nostro
convincimento che la situazione si sarebbe sviluppata in nuovi, più
avanzati livelli di scontro e che la stessa maturazione delle forze
politiche soggettivamente impegnate nella lotta avrebbe conosciuto un
processo di rapida accelerazione. Il tentativo di dare un contributo di
orientamento e di ripensamento a quanti fossero spinti dentro questo
processo dai movimenti materiali della lotta di classe ha dato origine a
*Contropiano*. Una certa dose di distacco, l'aspirazione a guardar
lontano, il convincimento che anche la pausa di riflessione giova alla
lotta, -- ma insieme il senso preciso delle situazioni reali in
movimento, il desiderio di seguire da vicino il tracciato tortuoso e
difficile dello scontro di classe, la volontà di battere la risposta
capitalistica anche al livello della polemica più contingente ed
immediata, -- tutto ciò costituiva fin dall'inizio il programma di
*Contropiano*. -->

> Ko je bila, pred približno enim letom, ta revija zamišljena in smo se za
> njo odločili, je njen nastanek in nagibe navdahnila natančna razredna
> napoved. Seveda nihče ni mogel natančno predvideti, kaj se bo v
> naslednjih mesecih zgodilo v Evropi: izbruh študentskega nasilja od
> Francije do Čehoslovaške. Vseeno pa nas je nekaj napeljevalo verjeti, da
> je organ teoretske refleksije, kot je ta, mogoč in nujno potreben. Niz
> delavskih bojev, ki so od leta 1960 neprestano pretresali Evropo in
> silili kapital v besno gonjo in strahovite krče -- nikoli trenutka miru,
> nikoli obdobja učinkovitega nadzora in ravnovesja -- je olajšala in
> utemeljila naša prepričanja, da se bo situacija razvila v nove,
> naprednejše ravni spopada in da se bo samo dozorevanje političnih sil,
> ki so subjektivno vpete v boj, hitro pospešilo. Poskus, da bi tistim, ki
> so jih v ta proces potisnila materialna gibanja razrednega boja,
> prispevali orientacijo in premislek, je botroval nastanku *Contropiana*.
> Določena mera distance, prizadevanja k daljnovidnosti, prepričanje, da
> je tudi premor za refleksijo koristen za boj -- toda hkrati natančen
> smisel za dejanske razmere v gibanju, želja natančno slediti zaviti in
> težavni poti razrednega spopada, volja premagati kapitalistični odgovor
> tudi na ravni najbolj nenujnih in neposrednih polemik --, vse to je od
> začetka predstavljalo program *Contropiana*.

<!-- Oggi l'accumulo enorme delle esperienze, la necessità di una critica
radicale dei programmi, l'esigenza di uno sconvolgimento fecondo nelle
situazioni organizzative finora dominanti, la volontà di saldare
finalmente insieme lotta e teoria, soluzioni tattiche e quadro
strategico, fanno di questi nostri propositi iniziali un punto di
partenza, che noi consideriamo validissimo e già in parte verificato, ma
che va precisato, approfondito ed allargato. Perciò ci sembra necessario
fissare in questo momento lo "stato" delle questioni, che ci stanno
davanti, per non saltare al di là di tappe necessarie, come tutti
talvolta siamo tentati di fare, ma per non restare allo stesso tempo
ancorati (o per non ritornare) a vecchie forme di discorso e di lotta,
-- come taluni fanno. Non c'è dubbio, intanto, che la "dimensione",
dentro la quale ci muoviamo, ha subito *ormai* un profondo cambiamento.
Una crosta s'è rotta nella situazione politica della classe operaia
dell'Europa occidentale. Francia e Cecoslovacchia, -- nella
straordinaria diversità ed anzi contraddittorietà delle rispettive
esperienze, -- concorrono però a formare il piano unitario, su cui si
collocano le nuove proposte: rottura del sistema sovietico e massiccia
ricomparsa della classe operaia europea sul fronte rivoluzionario sono
fatti per l'appunto di diversa natura e diversamente valutabili, ma
ambedue operanti infine all'allargamento e alla radicalizzazione sul
piano internazionale dello scontro tra capitale e lavoro. Fuor da ogni
precipitoso ottimismo, c'è da dire però che *un'epoca storica volge al
termine ed un'altra, faticosamente e dolorosa mente, se ne apre*. Ciò
che sta emergendo in questi ultimi mesi (ma sulla base di una lunga
stratificazione di lotte, che già negli anni passati noi avevamo cercato
d'individuare e di ricostruire teoricamente) è un nuovo livello politico
dello scontro di classe, è una più ampia, profonda, oggettivamente
verificabile possibilità di generalizzazione del potenziale
rivoluzionario esistente. Il senso di questo mutamento ci trova
preparati, ma nello stesso tempo ci esalta. Al nodo fondamentale di
problemi che questo passaggio comporta noi pensiamo si debbano dedicare
da questo momento in poi le forze di ciascuno di noi. Quella crosta
infatti si è rotta, ma al di sotto di essa non troviamo l'ordine d'un
sistema chiuso, di un'"istituzione" bell'e fatta e solo da raccogliere,
per sostituirla a quelle esistenti. Troviamo una situazione di classe
molteplice e contraddittoria, ribollente di possibilità e di forza, che
aspetta d'essere conosciuta teoricamente e ricostruita
organizzativamente, perché ne nasca ciò che essa già oggi sembra
indicarci e proporci, -- il processo di costruzione dell'alternativa
rivoluzionaria. *Perciò* rifiutiamo, consapevolmente rifiutiamo di
bloccare il nostro discorso intorno ad un'ipotesi esclusiva.
L'esplosione di classe si è manifestata a diversi livelli, ed ha
investito diversi livelli. Le forme dell'esperienza sono, se non
contraddittorie fra di loro, però estremamente complesse, e spesso il
loro giusto uso politico è ancora subordinato ad un corretto processo di
analisi, che è da fare. Proprio perché il compito strategico
fondamentale consiste essenzialmente per noi nella capacità di far
convergere *tutti* i livelli dell'esperienza di classe verso un *unico*
punto di rottura, proprio per questo oggi ci sembra necessario non
partire dal presupposto che tale convergenza sia già scontata e che il
problema sia solo quello molto semplice di praticarla politicamente. Lo
stesso discorso vale per le forze della sinistra operaia organizzata.
Qui, fuori ma anche all'interno dei vecchi confini ufficiali del
movimento operaio, è fenomeno innegabile e inarrestabile la crescita dei
settori disponibili al corretto recupero di una prospettiva di classe.
Senza dubbio non va sottovalutata l'altra faccia del fenomeno, -- quella
che negativamente si manifesta con la difficoltà di dare un asse comune
e un orientamento unitario a quanti troppo spesso, e dalle più diverse
direzioni, presumono di avere per sé e da sé già scoperto la chiave
della vicenda rivoluzionaria in Europa. La verità, -- verità che tutti
dovremmo onestamente riconoscere, -- è che ancora oggi la situazione
reale di classe è assai più avanzata di quanto le istituzioni del
movimento operaio riescano ad esprimere (e con i partiti e i sindacati
ci permettiamo di mettere in questo caso, per un senso di rispetto verso
la situazione com'è, anche i gruppi minoritari). Come per un'esplosione,
-- ma non certo senza fondati motivi, -- la contestazione
anticapitalistica esprime prepotentemente da sé una serie infinita di
forze soprattutto giovani e poi subito dopo le separa e le contrappone.
In questo non c'è tanto, o non soltanto, l'espressione di una
preesistente carenza d'indicazioni teoriche e politiche: c'è anche
quella specifica, quasi spontanea (in realtà consapevolissima) forma di
"rigetto", con cui il sistema riesce a difendersi, *dividendo i suoi
avversari*. Ebbene, noi cerchiamo di muoverci in direzione esattamente
contraria a questa. L'ipotesi, su cui si fonda *Contropiano*, è che la
molteplicità di forze anticapitalistiche e rivoluzionarie (forze
direttamente di classe, innanzi tutto), oggi disperse, possano essere
avvicinate e indirizzate verso un *blocco* futuro su basi *comuni*, che
probabilmente non potrà coincidere con nessuno degli organismi oggi
esistenti, così come sono, pur non potendo ignorare, nel suo sviluppo
teorico e nel suo pratico farsi, né le realtà della classe in movimento
né le realtà organizzative già date. Quest'intento (od ipotesi) non
deriva del resto da un calcolo astratto delle forze oggi in campo e
neanche da un improbabile tentativo di metterle semplicemente in
equilibrio fra loro. La stessa complessità della situazione di classe
dovrebbe secondo noi aiutare ad uscire chiunque da una visione
eccessivamente unilaterale del lavoro teorico e politico in questo
momento. Guardando nei movimenti della classe, non in noi stessi o nella
nostra innata predisposizione a far gruppo, possiamo individuare linee
di comportamento valide; e in questo momento è la spinta di classe a
suggerirci che non può esservi iniziativa che funzioni senza una
basilare preoccupazione di *unità*, senza lo sforzo di dare persino alla
ricerca teorica un compito di avvicinamento, di omogeneizzazione e di
fusione. -->

> Ogromna kopica izkušenj, nujnost radikalne kritike programov, potreba po
> plodnem preobratu organizacijskih razmer, ki so prevladovale doslej,
> želja, da bi končno združili boj in teorijo, taktične rešitve in
> strateški okvir so danes izhodišča našim začetnim predlogom, ki jih
> smatramo za ustrezne in delno že preverjene, vendar jih je treba še
> natančneje določiti, poglobiti in razširiti. Zato se nam v tem trenutku
> zdi nujno določiti "stanje" vprašanj, ki so pred nami, ne da bi
> preskočili nujno potrebnih korakov, kot nas včasih zamika, a da hkrati
> ne bi ostali zasidrani (ali se k njim vrnili) v starih oblikah razprav
> in boja -- kot to nekateri počnejo. Obenem ni dvoma, da se je
> "dimenzija" znotraj katere se *sedaj* gibljemo, temeljito spremenila.
> Skorja politične situacije delavskega razreda zahodne Evrope je počila.
> Francija in Čehoslovaška -- v izredni raznolikosti in celo
> protislovnosti njunih izkušenj -- tekmujeta v oblikovanju enotnega plana
> znotraj katerega se umeščajo novi predlogi: zlom sovjetskega sistema in
> množični ponovni pojav evropskega delavskega razreda na revolucionarni
> fronti sta dejstvi vsekakor različne narave, ki jih je mogoče različno
> vrednotiti, vendar oba na koncu delujeta na razširitev in radikalizacijo
> spopada med kapitalom in delom na mednarodni ravni. Brez kakršnega koli
> prenagljenega optimizma je treba povedati, da *se eno zgodovinsko
> obdobje izteka, drugo pa se naporno in boleče odpira*. To, kar se je
> pojavilo v zadnjih mesecih (vendar na podlagi dolge stratifikacije
> bojev, ki smo jo že v minulih letih poskušali opredeliti in teoretsko
> rekonstruirati), je nova politična raven razrednega spopada, je širša,
> globlja in objektivno preverljiva možnost posplošitve obstoječega
> revolucionarnega potenciala. Pred občutkom te spremembe smo se znašli
> pripravljeni, toda istočasno nas povzdiguje. Od tega trenutka dalje mora
> vsak od nas posvetiti vse svoje sile temeljnemu vozlišču problemov, ki
> ga ta prehod prinaša. Skorja dejansko je počila, vendar pod njo ne
> najdemo reda zaprtega sistema, predpripravljene "institucije", ki jo je
> potrebno zgolj prevzeti, da bi nadomestila te obstoječe. Najdemo
> večplastno in protislovno razredno situacijo, ki prekipeva od možnosti
> in sile, ki čaka, da jo teoretično spoznamo in organizacijsko
> rekonstruiramo, da bi nastalo to, kar se zdi, da se že danes nakazuje in
> predlaga -- proces konstrukcije revolucionarne alternative. *Zato*
> zavračamo, zavestno zavračamo, našo razpravo zavreti znotraj ene
> izključne hipoteze. Razredna eksplozija se je pojavila na različnih
> ravneh in zadeva različne ravni. Oblike izkustva so, če že ne
> protislovne med sabo, pa skrajno kompleksne, in pogosto je njihovo
> pravilna politična raba še vedno podrejena pravilnemu analitičnemu
> procesu, ki ga je potrebno opraviti. Prav zato, ker je temeljna
> strateška naloga za nas v bistvu v tem, da smo sposobni *vse* ravni
> razredne izkušnje zbližati k *eni* točki zloma, se nam danes zdi nujno,
> da ne izhajamo iz predpostavke, da je to zbliževanje že samoumevno in da
> je težava zgolj v tem, da ga politično prakticiramo. Enako velja za sile
> organizirane delavske levice. Pri tem je, zunaj in tudi znotraj starih
> uradnih meja delavskega gibanja, porast sektorjev, ki so naklonjeni
> pravilni obnovi razredne perspektive, neizpodbiten in neustavljiv pojav.
> Nedvomno ne smemo podcenjevati druge plati tega pojava -- ta, ki se
> negativno kaže v tem, da stežka določimo skupno os in enotno usmeritev
> tistih, ki prepogosto in iz najrazličnejših smeri domnevajo, da so že
> sami odkrili ključ revolucionarne zgodbe v Evropi. Resnica -- resnica,
> ki bi jo vsi morali odkrito priznati -- je, da je že danes dejanske
> razredna situacija precej bolj napredna kot jo institucije delavskega
> gibanja uspejo izraziti (in k strankam in sindikatom si v tem primeru,
> iz spoštovanja do situacije kot je ta, dovolimo vključiti tudi
> manjšinske skupine). Kot pri eksploziji, tudi antikapitalistični protest
> iz sebe pretirano izrazi neskončen niz predvsem mladih sil in jih nato
> takoj loči ter zoperstavi med sabo. Pri tem ne gre toliko, ali ne gre
> zgolj, za izraz predobstoječega pomanjkanja teoretskih in političnih
> indikacij: gre tudi za tisto specifično, skoraj spontano (v resnici zelo
> zavestno) obliko "zavračanja" s katero se sistem uspe ubraniti z
> razdvajanjem svojih nasprotnikov. No, poskušamo se gibati v ravno
> nasprotno smer od te. Hipoteza, na kateri temelji *Contropiano*, je, da
> je mnogotere antikapitalistične in revolucionarne sile (neposredno
> razredne sile, predvsem), ki so danes razpršene, zbližati in usmeriti v
> prihodnji *blok* na *skupnih* osnovah, ki verjetno ne bo mogel sovpadati
> z nobenim od obstoječih organizmov, kakršni so danes, pri čemer ne mogel
> zanemariti, v svojem teoretskem in praktičnem razvoju, niti realnosti
> razreda v gibanju niti že dane organizacijske realnosti. Ta namera (ali
> hipoteza) ne izhaja iz abstraktnega izračuna sil, ki so danes v igri,
> niti iz nezanesljivega poskusa, da bi jih preprosto enačili med seboj.
> Sama kompleksnost razredne situacije bi morala po našem mnenju vsakomur
> pomagati, da se izogne preveč enostranskem pogledu na teoretsko in
> politično delo v tem trenutku. S pogledom v razredna gibanja, ne v nas
> same ali v našo prirojeno nagnjenost k grupiranju, lahko prepoznamo
> veljavne usmeritve vedenja; in v tem trenutku nam razredni nagon nalaga,
> da ne more biti nobene pobude, ki bi delovala brez osnovne skrbi za
> *enotnost*, brez prizadevanja celo teoretskim raziskavam naložiti nalogo
> združevanja, homogenizacije in zlitja.

<!-- Tutto ciò, del resto, -- questo modo di procedere, intendiamo dire, --
non dovrebbe stupire. Dal quadro brevemente tracciato non esce infatti
niente di più che la riconferma di un punto fondamentale del nostro
programma teorico. Il cammino che noi seguiamo e intendiamo ancora
seguire è quello che va dalla comprensione e valutazione dei movimenti
di classe alla comprensione e valutazione della società capitalistica
nel suo complesso. Questo è ciò che chiamiamo "scienza operaia". Ad onta
degli strilli d'indignazione, che ci hanno dedicato i recensori borghesi
e quelli comunisti ufficiali, -- i quali evidentemente almeno in questo
concordano, nel ritenere che esista una disposizione neutrale alla
conoscenza, di cui le parti avverse possono ciascuna per suo conto far
l'uso che vogliono, -- noi continueremo a pensare che il punto di vista
della classe operaia sulla società capitalistica comporti già in sé,
ogni qual volta viene formulato correttamente, un chiaro, totalmente
demistificato processo di descrizione dell'oggetto da conoscere e
un'altrettanto chiara indicazione a trasformare quella conoscenza in
azione, -- in *rovesciamento* della realtà esistente, in *prassi di
organizzazione rivoluzionaria*. Che non si può conoscere, dunque, senza
pensare la conoscenza per l'azione; e che la conoscenza per l'azione non
può riproporsi come strumento d'individuazione corretta dell'oggetto,
senza mettersi dal punto di vista della classe *totalmente alienata*, --
la classe operaia. Di tutto ciò ovviamente non c'è stata data delega da
nessuno, per il semplice motivo che in questo campo meno che in ogni
altro vale il principio della delega. La delega, se di delega appunto si
può parlare, ce la siamo presi da soli mettendoci *in* un campo *contro*
l'altro. Se c'è un termine, con cui potremmo definire questo
atteggiamento, esso è: responsabilità. Responsabilità di militanti e di
ricercatori verso la classe, che abbiamo scelto come la nostra, *e che
dunque è la nostra*. -->

> Vse to, konec koncev, -- ta način ravnanja, želimo reči --  ne bi smelo
> presenečati. Iz tega kratko orisanega okvirja pravzaprav ne izhaja nič
> drugega kot ponovna potrditev temeljne točke našega teoretskega
> programa. Pot, ki ji sledimo in ji nameravamo še vedno slediti je ta, ki
> vodi od razumevanja in vrednotenja razrednih gibanj k razumevanju in
> vrednotenju kapitalistične družbe v svoji celoti. To je čemur pravimo
> "delavska znanost". Kljub ogorčenim vzklikom, ki jih namenjajo buržoazni
> in uradni komunistični recenzenti -- ki se očitno strinjajo vsaj v tem,
> da obstaja neka nevtralna dovzetnost do spoznanja, ki jo lahko nasprotni
> strani uporabljata po lastni presoji, kakor želita --, bomo še naprej
> menili, da stališče delavskega razreda do kapitalistične družbe že samo
> po sebi nosi jasen, popolnoma demistificiran proces opisa predmeta
> spoznanja in prav tako jasno nakazuje na preoblikovanje tega spoznanja v
> dejanje -- s *prevratom* obstoječe realnosti, s *prakso revolucionarne
> organizacije*. Da torej ni mogoče vedeti, brez da bi mislili na znanje
> za delovanje; in da se znanje za delovanje ne more ponuditi kot
> instrument pravilne opredelitve predmeta, ne da bi se postavilo na
> stališče *popolnoma odtujenega* razreda -- delavskega razreda. Povsem
> očitno je, da nas za to nihče ni pooblastil, in sicer iz preprostega
> razloga, ker na tem polju princip pooblaščanja velja manj kot na katerem
> koli drugem. Pooblastilo, če o pooblastilu sploh lahko govorimo, smo
> prevzeli mi sami in se postavili *v* en tabor *proti* drugemu. Če
> obstaja izraz s katerim lahko opredelimo to držo, je to: odgovornost.
> Odgovornost militantov in raziskovalcev do razreda, ki smo ga izbrali za
> našega *in ki zato je naš*.

<!-- È persino ovvio che, pur partendo da queste salde premesse, da esse non
scaturisca né una totale omogeneità dei contributi presentati né una
meccanica applicazione dell'impostazione teorica né un'automatica
corrispondenza dei risultati alle intenzioni. Tutto ciò non può esserci,
e forse è bene, è opportuno che non ci sia. Ci richiamiamo al senso di
alcune nostre frasi precedenti. Di fronte a noi sì apre un campo immenso
di possibilità pratiche e teoriche, d'iniziative politiche, di lotte e
di valutazioni delle lotte. Anche per noi si pone il problema, che è di
tutti, di tenere il passo con la realtà in rapido movimento. Noi
abbiamo, com'è noto, un'enorme fiducia nella nostra fedeltà ad un
metodo, che affonda direttamente le sue radici nelle lezione marxiana.
Ma, come pochi altri, -- ed anche questo è ben noto, ed anche questo ci
viene rimproverato come l'altro ed opposto difetto, -- abbiamo sempre
curato che la riflessione costantemente anticipasse l'esperienza, sempre
tenendo l'occhio e *da lontano* al continuo manifestarsi del nuovo:
forse persino colpevoli qualche volta di aver spiccato il salto troppo
al di la della situazione presente (ma non senza poi aver ricevute
qualche confortante conferma della validità delle nostre ipotesi). Oggi
ci sembra che il compito nostro si esprima nella capacità di raccogliere
il maggior numero possibile d'indicazioni e d'ipotesi serie,
correttamente fondate, senza preoccuparci tanto della loro presente
contraddittorietà (laddove questa si manifesti), quanto della loro
convergenza futura. È, ripetiamo, la stessa mossa situazione di classe a
livello europeo a suggerirci questo atteggiamento. All'interno di quelle
coordinate teoriche, che abbiamo indicato e cui non sapremmo certo
rinunciare, si apre un campo *problematico* di ricerche, nel quale il
confronto e la relativa discordanza dei risultati non sono da
respingere, bensì da favorire e da ricercare come l'espressione di una
realtà politica e di classe in continua trasformazione, ricca di
possibilità diverse, *tutte da scoprire*. Sarà un'impresa difficile, lo
riconosciamo, ma l'unica da tentare, perché in nessun modo possiamo
accettare che *Contropiano* torni ad essere la "rivista di gruppo" di
tipo tradizionale, i cui esiti appaiono scontati in partenza. Chi crede
di poter ridurre oggi, soprattutto al livello della ricerca
teorico-politica, l'intera iniziativa e l'intero discorso entro gli
schemi ristretti di una soluzione precostituita, commette un errore già
commesso. Il problema se mai, -- e non ce ne nascondiamo gli ostacoli,
-- è quello di far quadrare l'irrinunciabile rigore dell'impostazione
teorica con la ricchezza e la varietà dei contributi presentati. Ma è,
per l'appunto, un problema di tipo tecnico-politico, che va
immediatamente ricollegato a quello sui contenuti e i temi della
rivista. -->

> Povsem očitno je, da, čeprav izhajamo iz trdnih izhodišč, iz njih ne
> sledi niti popolna homogenost predstavljenih prispevkov, niti mehanska
> uporaba teoretskega pristopa, niti avtomatsko ujemanje med rezultati in
> nameni. Vsega tega ne more biti in morda je dobro in pravilno, da ne.
> Prikličimo pomene nekaterih naših prejšnjih stavkov. Pred nami se odpira
> ogromno polje praktičnih in teoretskih možnosti, političnih pobud, bojev
> in vrednotenj bojev. Tudi mi imamo težavo, ki jo imajo vsi, biti v
> koraku s hitro premikajočo realnostjo. Znano je, da smo prepričani v
> našo zvestobo metodi, ki izhaja neposredno iz marksovskega nauka. Toda,
> kot malokdo drugi, -- tudi to je dobro znano in tudi to se očita kot
> druga in nasprotna napaka -- smo vedno skrbeli, da je refleksija nenehno
> predvidela izkušnjo, vedno smo *z razdalje* opazovali nenehno
> pojavljanje novega: morda smo včasih bili celo krivi, da smo naredili
> prevelik skok onkraj sedanje situacije (vendar ne brez, da ne bi po tem
> prejeli kakšne tolažilne potrditve o veljavnosti naših hipotez). Danes
> se zdi, da se naša naloga izraža v sposobnosti zbrati čim večje število
> resnih, pravilno utemeljenih indikacij in hipotez, ne da bi se toliko
> obremenjevali z njihovo sedanjo protislovnostjo (kjer se pojavlja), kot
> ne z njihovim prihodnjim zbliževanjem. Ponavljamo, da tako držo narekuje
> sam premik v razredni situaciji na evropski ravni. Znotraj teh
> teoretskih koordinat, ki smo jih nakazali in ki se jim ne bi znali jasno
> odreči, se odpira *problematično* raziskovalno polje, na katerem
> soočenje in relativno nesoglasje rezultatov ni treba zavračati, prej
> favorizirati in raziskovati kot izraz politične in razredne realnosti v
> nenehnem preoblikovanju, bogate z različnimi možnostmi, ki jih je *vse
> treba odkriti*. Zavedamo se, da bo to težak podvig, toda edini za
> poskusiti, saj nikakor ne moremo sprejeti, da bi *Contropiano* obrnili k
> tradicionalnem tipu "skupinske revije" v kateri se rezultati zdijo že z
> začetka predvidljivi. Kdor verjame, da lahko danes, zlasti na ravni
> teoretsko-političnih raziskav, vso pobudo in vso razpravo omeji znotraj
> ozkih shem predpripravljenih rešitev, dela že storjeno napako. Problem,
> če sploh -- in ne slepimo se pred ovirami --, je uskladiti nepogrešljivo
> strogost teoretskega pristopa z bogastvom in raznolikostjo
> predstavljenih prispevkov. Vendar gre pravzaprav za tehnično-politični
> problem, ki ga je treba neposredno ponovno povezati s tem v vsebinah in
> temah revije.

<!-- Due parole, anche se ai nostri occhi perfino ovvie, diventano dunque
necessarie per comprendere il tipo d'impostazione tematica scelto per
*Contropiano*. La nostra rivista affronta infatti un arco vastissimo
d'interessi, come risulta evidente dal suo primo numero. Ciò avviene
perché noi pensiamo che la risposta del punto di vista operaio debba
rivelarsi efficace di fronte a qualunque livello delle "istituzioni"
capitalistiche. Naturalmente è ben chiaro che in questo settore la
rivista non può dare indicazioni positive, ma soltanto incentivi al
rifiuto e alla negazione. Non intendiamo insomma ricostruire una
sistematica cultura di classe operaia, -- operazione in cui non
crediamo, e che ci sembrerebbe comunque offensiva, qualora la
tentassimo, per la maturità politica e pratica della classe operaia in
questione, -- bensì distruggere sistematicamente e fin dalle più lontane
fondamenta (spesso, in verità, più significative e probanti dei suoi
epigonici rappresentanti presenti) *la cultura di classe dell'avversario
borghese*, che ci appare ancora tutt'altro che sconfitta ed inefficiente
(soprattutto se l'intendiamo, come noi l'intendiamo, in quanto "sistema"
di dominio intellettuale e di coercizione ideologica di una classe
sull'altra). -->

> Par besed, četudi so nam očitne, je torej potrebnih za razumevanje
> tipa tematskega pristopa, ki je bil izbran za *Contropiano*. Naša revija
> dejansko naslavlja zelo širok razpon interesov, kar je razvidno že iz
> njene prve številke. To je zato, ker menimo, da se mora odziv delavskega
> stališča izkazati za učinkovitega ob soočenju na katerikoli ravni
> kapitalističnih "institucij". Seveda je povsem jasno, da na tem področju
> revija ne more dajati pozitivnih napotkov, temveč samo spodbude za
> zavračanje in negacijo. Skratka, ne nameravamo rekonstruirati
> sistematične kulture delavskega razreda -- operacija v katero ne
> verjamemo in ki bi politični in praktični zrelosti zadevnega delavskega
> razreda vsekakor bila žaljiva, če bi jo poskusili --, prej sistematično
> in do najglobljih temeljev (v resnici pogosto pomembnejši kot bolj
> dokazni kot današnji epigonski predstavniki) uničiti *razredno kulturo
> buržoaznega nasprotnika*, ki se še vedno zdi vse prej kot poražena in
> neučinkovita (še posebej, če jo razumemo kot "sistem" intelektualnega
> gospostva in ideološke prisile enega razreda nad drugim).

<!-- C'è da chiedersi ora cosa va conservato e cosa mutato in questa
impostazione. Anche in questo caso va conservato secondo noi lo spirito
di fondo della scelta e precisato il modulo della sua applicazione. Non
solo nel senso abbastanza ovvio e scontato (anche se di certo non
facilmente realizzabile) di assicurare un più giusto equilibrio tra le
parti o, se si vuole, una più corretta "gerarchia" tra gli interessi,
che *Contropiano* affronta e mette in gioco. Ma soprattutto nel senso di
riuscire a "rivelare" dall'interno la funzionalità di questa parte del
discorso ai fini della formazione di un corretto punto di vista operaio
sulla società capitalistica del suo complesso, -- *sulla società
capitalistica come sistema di potere globalmente considerato* (economia,
politica, ma anche organizzazione del consenso, controllo ideologico,
mitologia culturale, ecc.). Chi abbia seguito con attenzione il revival
ideologico, su cui s'è fondata buona parte della fortuna della sinistra
non operaia negli ultimi anni, non può non comprendere l'effetto
demistificante e chiarificatore, che questa parte del discorso è
destinata ad avere; proprio per chi si proponga poi di sviluppare una
corretta impostazione teorica e politica di classe. Si tratta in molti
casi di non molto di più che d'un compito di pulizia e di risanamento
(qualche volta, soltanto di sgombero delle fogne). Utile, però, se lo si
considera nella prospettiva *di ciò a cui serve*. -->

> Zdaj se postavlja vprašanje, kaj je pri tem pristopu treba ohraniti in
> kaj spremeniti. Tudi v tem primeru menimo, da je treba ohraniti osnovni
> duh odločitve in natančno določiti obliko njene uporabe. Ne samo v
> precej očitnem in vidnem smislu zagotavljanja ravnovesja med deležniki
> ali, če želite, pravilnejše "hierarhije" interesov, ki jih naslavlja in
> uvaja *Contropiano*. Ampak predvsem v smislu iz znotraj "razkriti"
> funkcionalnost tega dela razprave za formacijo pravilnega delavskega
> stališča do kapitalistične družbe v njeni celoti -- *do kapitalistične
> družbe kot globalno upoštevanega sistema moči* (ekonomija, politika,
> ampak tudi organizacija konsenza, ideološki nadzor, kulturna mitologija
> itd.). Kdor je pozorno spremljal ideološki revival na katerem temelji
> dobršen del bogastva nedelavske levice, ne more ne razumeti
> demistifikacijskega in pojasnjevalnega učinka, ki ga ta del razprave
> zasleduje; prav za tiste, ki nato predlagajo razviti pravilen teoretski
> in politični razredni pristop. V mnogih primerih ne gre za nič več kot
> čiščenje in saniranje (včasih samo za čiščenje kanalizacije). Vendar je
> uporaben, če ga upoštevamo iz vidika *čemur služi*.

<!-- Una cosa è certa, comunque: *la rivista va fatta marciare verso una
sempre più corretta e predominante impostazione storico-politica e
teorico-politica*. Dagli impegni di lavoro avremo dunque, quando siano
stati realizzati, una risposta più precisa a quanto in questo bilancio
per ora è solo ripensato, abbozzato e infine riproposto all'attenzione
dei lettori. Non abbiamo intanto difficoltà a riconoscere che questo
secondo numero è, quanto alla tematica affrontata, solo lievemente
modificato rispetto al primo. Il terzo rappresenterà invece una svolta
nella direzione sopra indicata, e quella svolta la considereremo come
definitiva per i compiti e i caratteri, che vogliamo attribuire a
*Contropiano*. -->

> Ena stvar pa vsekakor je gotova: *revijo je treba napotiti k vedno
> pravilnejši in bolj prevladujočem zgodovinsko-političnem in
> teoretsko-političnem pristopu*. Na podlagi delovnih obveznosti, ko bodo
> realizirane, bomo torej imeli bolj natančen odgovor na to kar je v tej
> oceni za zdaj zgolj premišljeno, skicirano in na koncu ponovno
> predlagano v obravnavo našim bralcem. Medtem pa nimamo težav prepoznati,
> da se ta, druga številka, glede na naslovljeno tematiko, le rahlo
> razlikuje od prve. Tretja pa bo nasprotno predstavljala obrat v zgoraj
> navedeno smer in ta obrat smatramo kot dokončen za naloge in značaj, ki
> jih želimo pripisati *Contropianu*.
