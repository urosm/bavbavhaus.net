---
title: "Produktivnost kapitala, produktivno in neproduktivno delo"
...

## Produktivnost kapitala

Z menjavo med delavcem in kapitalom je živo delo inkorporirano v kapital.
Z začetkom delovnega procesa se živo delo že prikazuje kot dejavnost, ki pripada kapitalu, in tako se vse produktivne moči in posebne forme družbenega dela predstavljajo kot produktivne moči in posebne forme kapitala.
Lastnosti živega dela se kažejo kot lastnosti objektiviranega dela oz. objektivnih pogojev dela, ki jih pooseblja kapitalist in se postavljajo nasproti živemu delu.
[@marx1994economic, 121-122]
Tu gre za razmerje, ki ga je Marx pri obravnavi blaga že označil za *fetišizem*.
Obrnjeno razmerje, kjer se lastnosti družbenega dela kažejo kot lastnosti kapitala, lahko označimo za [*Fetiš kapitala*](fetis_kapitala.md).

Produktivnost kapitala sestoji, prvič, iz *prisile opravljati presežno delo*.
Tj. iz prisile delati več kot je potrebno za zadovoljitev neposrednih individualnih potreb.
V tem smislu se kapitalistični produkcijski proces ne razlikuje od prejšnjih razen, da to prisilo izvaja na način, ki bolje odgovarja produkciji.

Sredstva produkcije, objektivni pogoji produkcije se ne kažejo kot podrejeni delavcu.
Delavec se zdi podrejen njim.
Sredstva produkcije niso sredstva s katerimi bi produciral produkte s katerimi bi si ali neposredno ali z menjavo zagotovil življenjska sredstva.
Delavec je sredstvo za uvrednotenje (ohranjanje in povečanje vrednosti ...).

Tudi tu gre za inverzijo razmerja in razliko kapitalističnega produkcijskega načina od prejšnjih.
Kapitalist ne vlada nad delom kot oseba, zgolj kot kapital, objektivirano delo vlada nad živim delom.

To razmerje postane še kompleksnejše.
Ne samo neposredno materialne stvari, tudi forme družbeno razvitega dela se z razvojem specifično kapitalistišnega načina produkcije kažejo kot forme razvoja kapitala.
Kooperacija, manufaktura, tovarna se kažejo kot forme razvoja kapitala.

::: {lang=en}

> And this assumes a form which is the more real the more, on the one hand, their labour capacity is itself modified by these forms, so that it becomes powerless when it stands alone, i.e. *outside* this context of capitalism, and its capacity for independent production is destroyed, while on the other hand the development of machinery causes the conditions of labour to appear as ruling labout technologically too, and at the same time to replace it, suppress it, and render it superfluous in its independent forms.
[@marx1994economic, 123-124]

:::

Naravne sile, stroji, znanost so sredstva, ki jih kapital uporablja za izkoriščanje dela.
Da jih lahko uporablja za izkoriščanje dela, jih mora uporabiti v produkciji, mora jih razvijati.
Razvoj družbenih produktivnih sil in pogoji za ta razvoj se kažejo kot delo kapitala.
Delavec ima do tega pasiven odnos in ta razvoj se vrši v antagonističnem odnosu do njega.

Buržoazna znanost zamenjuje vprašanji kaj je produktivno delo s stališča kapitala in kaj je produktivno delo nasploh.
Slednje lahko privede zgolj do splošnega odgovora, da je vso delo, ki rezultira v nečem, produktivno.


## Produktivno in neproduktivno delo

::: alts

> Delo se v dejanskem produkcijskem procesu *realiter* spreminja v kapital, toda ta sprememba [je] pogojena s prvotno menjavo med denarjem in delovno zmožnostjo [Arbeitsvermögen].
> Šele s to *neposredno* spremembo dela v *upredmeteno* delo, ki ne pripada delavcu marveč kapitalistu, se denar spremeni v kapital, tudi tisti del [denarja], ki je dobil formo produkcijskih sredstev, delovnih pogojev.
> Pred tem je denar samo *na sebi* kapital, pa najsi eksistira v svoji lastni formi ali pa v formi blag (produktov), ki imajo neko podobo, v kateri lahko služijo kot produkcijska sredstva novih blag.
>
> To določeno *razmerje* do dela šele spremeni denar ali blago v kapital, delo pa, ki s tem svojim razmerjem do produkcijskih pogojev, razmerjem, ki mu ustreza določeno obnašanje v dejanskem produkcijskem procesu, spreminja denar ali blago v kapital, tj. delo, ki nasproti delovni zmožnosti osamosvojeno *upredmeteno* delo ohranja in veča v njegovi vrednosti, je *produktivno* delo.
> Produktivno delo je samo neki skrajšan izraz za celotno razmerje in za način, v katerem delovna zmožnost figurira v kapitalističnem produkcijskem procesu.
> Razlikovanje od *drugih* vrst dela pa je izredno pomembno, ker izraža prav tisto oblikovno določenost dela, na kateri sloni celoten kapitalistični produkcijski način in kapital sam.
>
> *Produktivno delo* je torej takšno delo -- v sistemu kapitalistične produkcije -- ki producira presežno vrednost za svojega employer [delodajalca] ali, je delo, ki spreminja objektivne delovne pogoje v kapital, njihovega posestnika pa v kapitalista, torej delo, ki producira svoj lasten produkt kot kapital.
[@marx1994economic, 130-131; slovenski prevod odlomka iz @marx1977historicni, 133-134]

::: {lang=en}

> In the real production process labour is converted *in reality* into capital, but this conversion is conditioned by the original exchange between money and labour capacity.
> It is only through this *direct* conversion of labour into *objectified* labour which belongs not to the worker but to the capitalist that the money is converted into capital, including the part which has taken on the form of the means of production, the conditions of labour.
> Previously money was only capital in *itself*, whether it existed in its own form or in the form of commodities (products) which possessed a shape enabling them to serve as the means of production for new commodities.
> 
> It is only this particular relation to labour which converts money or commodity into capital, and that labour is *productive labour* which -- by means of this relation it has to the conditions of production, to which there corresponds a particular position in the real production process -- converts money or commodity into capital, i.e. preserves and increases the value of the *objective labour* which has attained an independent position vis-à-vis labour capacity.
> Productive labour is only an abbreviation for the whole relation in which, and the manner in which, labour capacity figures in the capitalist production process.
> It is however of the highest importance to distinguish between this and *other* kinds of labour, since this distinction brings out precisely the determinate form of labour on which there depends the whole capitalist mode of production, and capital itself.
> 
> *Productive labour*, therefore, is labour which -- in the system of capitalist production -- produces surplus value for its EMPLOYER or which converts the objective conditions of labour into capital, and their owners into capitalists, hence labour which produces its own product as capital.

:::

:::

<!-- MEW 26/1, 371-372 -->

> Kapitalistični produkcijski način zato tudi ni le produkcija blag.
> Je proces, ki absorbira neplačano delo in ki material in delovna sredstva -- produkcijska sredstva -- naredi za sredstva absorpcije neplačanega dela.
> 
> Iz dosedaj povedanega izhaja, da označitev dela kot *produktivnega* nima najprej absolutno nič opraviti z določeno *vsebino dela*, z njegovo posebno koristnostjo ali s svojevrstno uporabno vrednostjo, v kateri se delo prikazuje.
> 
> *Ista* vrsta dela je lahko *produktivna* ali *neproduktivna*.
> 
> Npr. Milton, who did the "Paradise Lost" for 5 £ [ki je napisal "Izgubljeni raj" za 5 funtov], je bil *neproduktiven delavec*.
> Nasprotno je pisatelj, ki opravlja konkretno delo za svojega knjigotržca, *produktiven delavec*.
> Milton je produciral "Izgubljeni raj" iz istega vzroka kot producira sviloprejka svilo.
> To je bilo udejstvovanje *njegove* narave.
> Kasneje je prodal produkt za 5 £.
> Toda leipziški literarni proletarec, ki pod upravo svojega knjigotržca producira knjige (npr. ekonomske priročnike), je *produktiven delavec*; kajti njegov produkt je že vnaprej subsumiran v kapital in gre samo še za uvrednotenje le-tega.
> Pevka, ki na lastno pest prodaja svoje petje, je *neproduktiven delavec*.
> Toda ista pevka, ki jo angažira neki entrepreneur [podjetnik] ter jo pusti peti, da bi si pridobival denar, je *produktiven delavec*; kajti ona producira kapital.
[@marx1994economic, 136; slovenski prevod iz @marx1977historicni, 202-203]

<!-- MEW 26/1, 376-377 -->

> Pri nematerialni produkciji, celo če se opravlja izključno za menjavo, torej če reproducira *blaga*, je možno dvoje:
> 
> 1. Produkcija rezultira v *blagih*, uporabnih vrednotah, ki posedujejo neko od producentov in konsumentov različno samostojno podobo [Gestalt], ki torej lahko obstajajo v nekem intervalu med produkcijo in konzumpcijo, ki lahko cirkulirajo v tem intervalu kot *kupljiva blaga*, kot je to pri knjigah, slikah, skratka pri vseh umetniških produktih, ki so različni od umetniške storitve izvajajočega umetnika.
> Tu je kapitalistična produkcija uporabljiva le v zelo omejeni meri, kolikor npr. neki pisatelj za neko skupno delo -- npr. enciklopedijo -- izkorišča celo vrsto drugih kot pomožne delavce.
> Tu večinoma ostaja pri *prehodni formi* h kapitalistični produkciji, [pri formi], da delajo različni znanstveni ali umetniški producenti, obrtniki ali profesionalci, za skupni trgovski kapital založnikov, razmerje, ki nima ničesar opraviti s pravim kapitalističnim produkcijskim načinom in ki le-temu celo formalno še ni podrejeno.
> Da je v teh prehodnih formah eksploatacija dela ravno največja, ničesar ne spremeni na stvari.
>
> 2. Produkcije ni mogoče ločiti od akta produciranja, kot pri vseh eksekutivnih umetnikih, govornikih, igralcih, učiteljih, zdravnikih, duhovnikih itd.
> Tudi tu teče kapitalistični produkcijski način samo v skromnem obsegu in se lahko glede narave stvari dogaja samo v nekaterih sferah.
> Na primer, v učnih zavodih so lahko učitelji goli mezdni delavci za podjetnika učnega zavoda, tako kot obstajajo v Angliji številne takšne učne tovarne.
> Čeprav nasproti učencem niso *produktivni delavci*, so to nasproti svojemu podjetniku.
> Le-ta menja svoj kapital za njihovo delovno zmožnost in s tem procesom postaja bogatejši.
> Enako je pri gledaliških, zabaviščnih podjetjih itd.
> Umetnik se tu do občinstva obnaša kot umetnik, nasproti svojemu podjetniku pa je *produktiven delavec*.
> Vsi ti pojavi kapitalistične produkcije na tem področju so v primerjavi s celoto produkcije tako nepomembni, da jih lahko popolnoma zanemarimo.
[@marx1994economic, 143-144; slovenski prevod iz @marx1977historicni, 203-204]

<!-- MEW 26/1, 385-386 -->

> Z razvojem specifično kapitalističnega produkcijskega načina, kjer sodeluje veliko delavcev pri produkciji istega blaga, mora biti razmerje, v katerem je neposredno njihovo delo do predmeta produkcije, seveda zelo različno.
> Npr. prej omenjeni pomožni delavci v neki tovarni nimajo nobenega direktnega opravka z obdelovanjem surovine.
> Delavci, ki so nadzorniki tistih, ki neposredno delajo pri tem obdelovanju, so za korak bolj odmaknjeni, inženir ima spet drugačen odnos in dela v glavnem le s svojo glavo etc.
> Toda *celota teh delavcev*, ki imajo delovne zmožnosti različne vrednosti, čeprav kaže uporabljena količina precej enako višino, producira rezultat, ki se -- če opazujemo celoten produkcijski proces -- izrazi v *blagu* ali nekem *materialnem produktu*; vi skupaj pa, kot delavnica, so živi produkcijski pogoji teh *produktov*, ker menjajo, če opazujemo celoten produkcijski proces, svoje delo za kapital in reproducirajo denar kapitalistov kot kapital, tj. kot ovrednotujočo se vrednost, večajočo se vrednost.
> 
> Saj to je prav tista svojevrstnost kapitalističnega produkcijskega načina, da ločuje različna dela, torej tudi umska in ročna dela -- ali dela, v katerih prevladuje ena ali druga plat -- in da jih razdeli na različne osebe, kar pa ne preprečuje, da je materialni produkt *skupni produkt* teh oseb ali njihov skupni produkt upredmeten v materialnem bogastvu; kar na drugi strani prav tako ne preprečuje oz. ničesar ne spremeni na tem, da je razmerje vsake posamezne osebe razmerje mezdnega delavca do kapitala in v tem eminentnem pomenu razmerje *produktivnega delavca*.
> Vse te osebe niso samo *neposredno* zaposlene v produkciji materialnega bogastva, marveč menjajo svoje delo *neposredno* za denar kot kapital in tako poleg svoje mezde neposredno reproducirajo presežno vrednost za kapitalista.
> Njihovo delo obstaja iz plačanega dela plus neplačanega presežnega dela.
[@marx1994economic, 144-145; slovenski prevod iz @marx1977historicni, 134-135]

<!-- MEW 26/1, 386-387 -->

> *Division of labour (delitev dela)* v določenem pomenu ni nič drugega kot *coexisting labour*, to se pravi koeskistenca *različnih* delovnih načinov, ki se prikazujejo v *different kinds* of produce or rather commodities (*različnih vrstah* produktov ali bolje blag).
> Division of labour v kapitalističnem pomenu, kot analiza posebnega dela, ki producira neko določeno blago, na vsoto enostavnih, med različne delavce porazdeljenih in součinkujočih operacij, predpostavlja delitev dela znotraj družbe, zunaj delavnice, kot separation of occupation (ločitev opravil).
> 
> Na drugi strani jih ta delitev množi.
> Produkt je lahko toliko bolj eminentno produciran kot blago, njegova menjalna vrednost postaja toliko bolj neodvisna od njegovega neposrednega bivanja, ali njegova produkcija postaja toliko bolj neodvisna od njegove konzumpcije s strani producentov, od njegovega bivanja kot uporabne vrednosti za producente, kolikor bolj enostranski je produkt sam in kolikor večja je raznovrstnost blag, za katero se zamenjuje, kolikor večja je vrsta uporabnih vrednot, v katerih se izraža njegova menjalna vrednost, kolikor večji je zanj trg.
> Čim bolj se to dogaja, tem bolj je lahko produkt produciran kot blago.
> Torej tudi tem bolj *masovno*.
> Enakovrednost (ravnodušnost -- Gleichgültigkeit) njegove uporabne vrednosti za producente se kvantitativno izrazi v masi, v kateri je produciran in ki ni v nobenem razmerju do konzumpcijskih potreb producenta, celo takrat ne, če je producent hkrati konzument svojega produkta.
> Ena od metod za produktion en masse in tako za produkcijo produkta pa je delitev dela znotraj delavnice.
> Tako sloni delitev dela v notranjosti delavnice na delitvi occupation (opravil) znotraj družbe.
[@marx1990economic, 402; @marx1977historicni, 108-109].

<!-- MEW 26/3, 264 -->

## Ali so delavci v trgovini produktivni delavci?

V Marxovih ekonomskih rokopisih iz let 1861--1863 se med razpravo o delitvi dela
pojavi satirična "Digresija o produktivnem delu":

::: alts

> Filozof producira ideje, pesnik pesmi, pastor pridige, profesor kompendije itn.
> Zločinec producira zločine.
> Če si pobliže ogledamo povezanost te zadnje produkcijske panoge z mejami družbe, bomo opustili veliko predsodkov.
> Zločinec ne producira le zločinov, temveč tudi kazensko pravo in s tem tudi profesorja, ki predava o kazenskem pravu, poleg tega pa še neogibni kompendij, v katerem ta profesor vrže svoja predavanja kot "blago" na splošni trg.
> S tem se veča narodno bogastvo, ne upoštevajoč zasebnega užitka, ki ga daje rokopis kompendija njegovemu tvorcu, kot nam [pove] kompetentna priča prof. Roscher.
> Zločinec producira nadalje vso policijo in kazensko pravosodje, biriče, sodnike, rablje, porotnike itn. in vse te različne poklicne panoge, ki predstavljajo ravno toliko kategorij družbene delitve dela, razvijajo različne zmožnosti človeškega duha, ustvarjajo nove potrebe in nove načine za njihovo zadovoljitev.
> Že mučenje je spodbudilo za najumetelnejša mehanična odkritja in je v produkciji svojih orodij zaposlovalo množico častitih rokodelcev.
> Zločinec producira vtis, deloma moralen, deloma tragičen, pač odvisno, in s to "storitvijo" vzburja moralna in estetska občutja občinstva.
> Ne producira le kompendijev o kazenskem pravu, ne le kazenskih zakonikov in s tem kazenskih zakonodajalcev, temveč tudi umetnost, lepo literaturo, romane in celo tragedije, kot pričata ne le Müllnerjeva "Krivda" in Schillerjevi "Razbojniki", temveč celo "Ojdip" in "Richard III.".
> Zločinec pretrga monotonijo in vsakdanjo gotovost meščanskega življenja.
> S tem ga obvaruje pred stagniranjem in zbudi tisto nemirno napetost in razgibanost, brez katerih bi otopela tudi ost konkurence.
> S tem spodbuja produktivne sile.
> Medtem ko zločin odtegne del presežnega prebivalstva trgu dela in s tem zmanjša konkurenco med delavci, ko do neke določene točke prepreči, da bi mezda padla pod minimum, absorbira boj proti zločinu drugi del istega prebivalstva.
> Zločinec torej nastopi kot ena izmed tistih naravnih "izravnav", ki vzpostavi pravo raven in odpre pravcato perspektivo "koristnih" zaposlitev.
> Delovanje zločina na razvoj produktivne sile je mogoče do podrobnosti dokazati.
> Ali bi se ključavnice kdajkoli razvile do zdajšnje popolnosti, če ne bi bilo tatov?
> Ali bi izdelava bankovcev dosegla svojo zdajšnjo dovršenost, če ne bi bilo ponarejevalcev denarja?
> Ali bi mikroskop našel pot v navadne trgovske sfere, če ne bi prav v trgovanju goljufali?
> Mar nima za praktično kemijo ravno toliko zaslug ponarejanje blaga in prizadevanje odkriti ga kakor poštena produkcijska prizadevnost?
> Zločin z vedno novimi načini napadov na lastnino prikliče v življenje vedno nova obrambna sredstva in učinkuje s tem enako produktivno kakor strikes na izume strojev.
> Pa pustimo področje zasebnega zločina.
> Ali bi brez nacionalnega zločina sploh kdaj nastal svetovni trg?
> In celo narodi?
> In mar ni od Adamovih časov naprej drevo greha hkrati tudi drevo spoznanja?
> Mandville je v "Fable of the Bees" (1705) že dokazal produktivnost vseh mogočih poklicev itn.
> in sploh tendenco tega argumenta: ["That what we call evil in this world, moral as well as natural, is the grand principle that makes us sociable creatures, the solid basis, the *life and support of all trades and employments* without exception; there we must look for the true origin of all arts and sciences; and the moment evil ceases the society must be spoiled, if not totally destroyed."]{lang=en}
> Le da je bil Mandeville seveda neskončno bolj drzen in pošten kakor filistrski apologeti meščanske družbe.
[@marx1989kritika, 246-247]

::: {lang=en}

> A philosopher produces ideas, a poet poems, a clergyman sermons, a professor compendia and so on.
> A criminal produces crimes.
> If we take a closer look at the connection between this latter branch of production and society as a whole, we shall rid ourselves of many prejudices.
> The criminal produces not only crimes but also criminal law, and with this also the professor who gives lectures on criminal law and in addition to this the inevitable compendium in which this same professor throws his lectures onto the general market as "commodities".
> This brings with it augmentation of national wealth, quite apart from the personal enjoyment which -- as a competent witness, Professor Roscher, [tells] us -- the manuscript of the compendium brings to its originator himself.
> The criminal moreover produces the whole of the police and of criminal justice, constables, judges, hangmen, juries, etc.; and all these different lines of business, which form just as many categories of the social division of labour, develop different capacities of the human mind, create new needs and new ways of satisfying them.
> Torture alone has given rise to the most ingenious mechanical inventions, and employed many honourable craftsmen in the production of its instruments.
> The criminal produces an impression, partly moral and partly tragic, as the case may be, and in this way renders a "service" by arousing the moral and aesthetic feelings of the public.
> He produces not only compendia on Criminal Law, not only penal codes and along with them legislators in this field, but also art, belles-lettres, novels, and even tragedies, as not only Müllner's *Schuld* and Schiller's *Räuber* show, but *Oedipus* and *Richard the Third*.
> The criminal breaks the monotony and everyday security of bourgeois life.
> In this way he keeps it from stagnation, and gives rise to that uneasy tension and agility without which even the spur of competition would get blunted.
> Thus he gives a stimulus to the productive forces.
> While crime takes a part of the redundant population off the labour market and thus reduces competition among the labourers -- up to a certain point preventing wages from falling below the minimum -- the struggle against crime absorbs another part of this population.
> Thus the criminal comes in as one of those natural "counterweights" which bring about a correct balance and open up a whole perspective of "useful" occupations.
> The effects of the criminal on the development of productive power can be shown in detail.
> Would locks ever have reached their present degree of excellence had there been no thieves?
> Would the making of bank-notes have reached its present perfection had there been no forgers?
> Would the microscope have found its way into the sphere of ordinary commerce but for trading frauds?
> Does not practical chemistry owe just as much to the adulteration of commodities and the efforts to show it up as to the honest zeal for production?
> Crime, through its ever new methods of attack on property, constantly calls into being new methods of defence, and so is as productive as *strikes* for the invention of machines.
> And if one leaves the sphere of private crime: would the world market ever have come into being but for national crime?
> Indeed, would even the nations have arisen?
> And has not the Tree of Sin been at the same time the Tree of Knowledge ever since the time of Adam?
>
> In his *Fable of the Bees* (1705) Mandeville had already shown that every possible kind of occupation is productive, and had given expression to the tendency of this whole line of argument:
>
> > That what we call Evil in this World, Moral as well as Natural, is the grand Principle that makes us Sociable Creatures, the solid Basis, the *Life and Support of all Trades and Employments* without exception; there we must look for the true origin of all Arts and Sciences; and the moment Evil ceases, the Society must be spoiled if not totally destroyed.
>
> Only Mandeville was of course infinitely bolder and more honest than the philistine apologists of bourgeois society.
[@marx1987economic, 306-310]

:::

:::

- Ironija, satira usmerjena v vulgarne ekonomiste.
- Kaže, da zadeva ni enostavna, enoznačna. To je verjetno stvar tega, da
  gledamo kapital v deloti (družba).
- Relevantno še danes, ker se levičarji motijo o tem. Relevantno je
  vztrajat pri pravilni interpretaciji, ker so politične posledice tega.

na katero je v levičarskih razpravah, kakor se te pojavljajo v
"postindustrijskih" družbah, vredno večkrat opomniti. Govorimo o
arbitrarnem razlikovanju nekaterih marksistov in marksistk med
proizvodnjo in storitvenim sektorjem na podlagi tega ali sektorja
"ustvarjata vrednost". Razlikovanje, kakor nam je znano, nima prave
podlage, je pa teoretsko in politično sporno saj zamegljuje predmet
kritike politične ekonomije v družbah, ko delavci ne udarjajo več z
macolami v istem obsegu kot so to domnevno počeli v Manchestru leta 1863
(tudi ta statistika bi presenetila).

***

V tako imenovanih postindustrijskih družbah, katerih večji del posameznih nacionalnih ekonomij tvori storitveni sektor in tako večji del delavskega razreda storitveni delavci, marksovsko kritiko, zaradi posebne pozornosti, ki jo tradicionalno namenja *produktivnemu* in *industrijskemu* kapitalu, spremljajo teoretske in politične zadrege.
Teorija, ki iz odkritja presežne vrednosti izpeljuje poseben epistemološki in revolucionarni pomen proletariata, ki ga zaposluje produktivni kapital, se z dejstvom, da na čelu kapitalističnega sveta takšnega proletariata ni, sooča na različne načine.
Če omenimo samo enega, takšnega, ki želi ohraniti videz teoretske ortodoksije:
vso pravo delovanje kapitalizma in ves revolucionarni potencial se lahko na primer geografsko prestavi na globalni jug ali periferijo.
^[Ali polperiferijo, četrtperiferijo itd.]
Toda 


Il pagamento di ogni prezzo del lavoro in termini di salario si presenta come negazione assoluta del profitto capitalistico, in quanto assoluta eliminazione del pluslavoro operaio. Il capitale, che scompone e ricompone il processo lavorativo secondo i bisogni crescenti del proprio processo di valorizzazione, si presenta ormai come oggettiva potenza spontanea della società che si autorganizza e cosi si sviluppa. fabricca 22

Plačilo vseh cen dela v obliki plač je predstavljeno kot absolutno zanikanje kapitalističnega dobička, kot absolutna odprava presežnega dela delavcev. Kapital, ki razgrajuje in ponovno sestavlja delovni proces v skladu z naraščajočimi potrebami lastnega procesa valorizacije, se zdaj predstavlja kot objektivna spontana moč družbe, ki se samoorganizira in tako razvija.



 
 periferijo, polperiferijo
Teoretizira na primer

industrijskemu proletariatu, ki ga zaposluje produktivni kapital, pripisuje posebno epistemološko in revolucionarno vrednost 

 torej med delavci storitveni delavci

  kjer prevladuje storitveni sektor 
 Glede na to, da so trgovinske verige eden največjih zaposlovalcev v nacionalnih ekonomijah tim. postindustrijskih družb

V osnovi trditev: vprašanje  med produktivnim in neproduktivnim delu se ne tiče vsebine dela. Torej, delavci katerih konkretno delo obsega skeniranje in bla bla bla *so lahko produktivni delavci*.

Kar razločuje produktivno in neproduktivno je zgolj to ali ga zaposluje kapital, oz. natančneje ali ga zaposluje produktivni kapital.

Vprašanje je torej, ali je npr. Mercator produktivni kapital(ist)?

## Ali so intelektualci produktivni delavci?

> Neproduktivnost intelektualnega dela je tista *krivda*, ki bremeni kulturo 19. stoletja in ki jo napredne ideologije *morajo* preseči.
[@tafuri1970lavoro, 241]


---
lang: sl
references:
- type: book
  id: marx1989kritika
  author:
  - family: Marx
    given: Karl
  title: "Kritika politične ekonomije 1861--1863, I. del"
  container-title: "Temeljna izdaja I"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: "10.1"
  publisher-place: Ljubljana
  publisher: Marksistični center CK ZKS
  issued: 1989
  language: sl
- type: book
  id: marx1987economic
  author:
  - family: Marx
    given: Karl
  title: "Economic manuscript of 1861--63"
  container-title: "Collected works"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Frederick
  volume: 30
  publisher-place: London
  publisher: Lawrence & Wishart
  issued: 1987
  language: en
- type: book
  id: marx1990economic
  author:
  - family: Marx
    given: Karl
  title: "Economic manuscript of 1861--63 (continuation)"
  container-title: "Collected works"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Frederick
  volume: 32
  publisher-place: London
  publisher: Lawrence & Wishart
  issued: 1990
  language: en
- type: book
  id: marx1994economic
  author:
  - family: Marx
    given: Karl
  title: "Economic manuscript of 1861--63 (conclusion)"
  container-title: "Collected works"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Frederick
  volume: 34
  publisher-place: London
  publisher: Lawrence & Wishart
  issued: 1994
  language: en
- type: book
  id: marx1977historicni
  author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  title: "Historični materializem: izbor odlomkov iz del Karla Marxa in Friedricha Engelsa"
  title-short: "Historični materializem"
  translator:
  - family: Riha
    given: Rado
  publisher-place: Ljubljana
  publisher: Mladinska knjiga
  issued: 1977
  language: sl
...
