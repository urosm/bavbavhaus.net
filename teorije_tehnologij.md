---
title: "Teorije tehnologij"
description: |
  Zapiski s predavanj *Teorija tehnologije* v poletnem semestru 2023/2024.
...

Osnovno raziskovalno vprašanje: ali se pri sodobnem tehnološkem razvoju dogaja
nekaj, česar koncept avtomatizacije kot se razvije in uporablja v 20. stoletju,
ne zajame. Ter ali se to, kar pride po avtomatizaciji, že dogaja.

*Razumevanja avtomatizacije*: (1) samodejnost strojev; (2) prenos človekovih
dejavnosti na stroje; (3) programiranost strojev.

1. Razmeroma nesporna opredelitev. Sicer niso dobesedno samodejni, saj z njimi
upravlja človek.

2. Ponavadi v sklopu humanističnega ali kritičnoteoretskega očitka, da
avtomatizacija predstavlja odtujitev. 

3. Vnaprejšnja določenost strojnih dejanj. Popolnoma nesporna opredelitev, a se
danes postavlja vprašanje, če se to spreminja: Vprašanje inteligence strojev.

## Leroi-Gourhan, *Gib in beseda*

Leroi-Gourhan ponudi teorijo o osnovnem razmerju med človekom in tehnologijo in
sicer, da tehnologija nadaljevanje evolucije človeka.

Kar človekove prednike prvič razloči od živali je pokončna hoja. To sprosti
roke. Nespecializirane roke, zato pračlovek vedno posega po zunanjih predmetih.
Praorodja se razvijajo enako kot telesni deli pri živalih. Orodja so zunanji
organi. Pri živalih je kontinuiteta med mišicami in "orodjem", pri človeku je
diskontinuiteta med telesom in orodjem. Orodja ne presegajo živalskega
obnašanja, edina posebnost je, da so zunanja.

Novost *Homo sapiens* je, da se pozunanji tudi gib sam. Primer lok in puščica.
Gib se prenese na orodje. Tu je zanimivo, da lok ne oponaša človeških ali
živalskih organov.

Tretja faza je pozunanjenje spomina. Zaporedja zveznih urejenih gibov.

Zadnja stopnja, pozunanjenje programa. Več urejenih zaporedij gibov, torej več
spominov. Tu lahko govorimo o avtomatizaciji v modernem pomenu, po
Leroi-Gourhan lahko torej dobi avtomatizacije rečemo doba pozunanjenih
programov.

Pri Leroi-Gourhan odtujitev začne antropogenezo, človeška vrsta se začne z
alienacijo. Inteligenca človeške vrste narašča v meri pozunanjenja, notranja
inteligenca človeka je ista že od začetka *Homo sapiensa*.

Tehnika

: bolj ali manj specializirane (učinkovite) oblike stika z okolico.


*Vrsta in etnija*. Postopna razlika med živalmi in ljudmi, naravo in kulturo,
zoološkim in sociološkim.

> [D]ružbi mravelj in človeški družbi [je] skupno le to, da pri obeh obstajajo
> izročila, ki zagotavljajo, da se od generacije do generacije prenašajo
> operativne verige, ki omogočajo preživetje in razvoj družbene skupine. [...
> S]kupina preživi tako, da uporablja pravi spomin, v katerega se vključujejo
> obnašanja; pri živalih se ta spomin, ki je značilen za sleherno vrsto, opira
> na zelo kompleksen aparat nagona, pri antropih pa se spomin, ki je značilen
> za sleherno etnijo, opira na nič manj kompleksen aparat govorice
> [@leroigourhan1990gib2, 11].

*Spomin*. Razumljen v razširjenem pomenu.

> Ni lastnost inteligence, ampak se ozira na nosilec, na katerega se vpisujejo
> verige dejanj. Zatorej lahko govorimo o "spominu vrste", ko opredeljujemo
> fiksiranje obnašanja živalskih vrst, o "etičnem" spominu, ki zagotavlja
> reproduciranje obnašanj v človeških družbah, in enako upravičeno o "umetnem"
> spominu, ki je v svoji najnovejši obliki elektronski in ki brez zatekanja k
> nagonu ali k refleksiji zagotavlja reproduciranje med seboj povezanih
> mehaničnih dejanj [@leroigourhan1990gib2, 211n14].

*Družbeni spomin*. 

> Paleoontologi so pogosto vztrajali pri tem, da je človek usmeril svojo
> specializacijo k ohranjanju zelo splošnih zmožnosti. To daleč presega telesni
> okvir; res je, da tečemo manj hitro kakor konj, da ne prebavljamo celuloze
> kakor krava, da slabše plezamo kakor veverica in da je naposled naš
> kostno-mišični stroj superspecializiran le za to, da je zmožen početi vse,
> najpomembnejše pa je to, da so človeški možgani evoluirali tako, da so ostali
> zmožni misliti vse in da se rodijo praktično prazni [@leroigourhan1990gib2,
> 20].

[Neurejeni izpiski]

> Človeški možgani so tako zgrajeni, da odtujijo del svoje razpoložljivosti, s
> tem da izdelajo elementarne programe, ki zagotavljajo, da je izjemno
> obnašanje prosto. Prav te elementarne prakse, katerih verige se vzpostavijo
> že ob rojstvu, posameznika najmočneje zaznamujejo z etničnim odtisom. Gibi,
> drže, način, kako se obnaša v vsakdanjem življenju in v vsakdanjosti,
> sestavljajo del vezi z družbeno skupino, iz katere je, in kadar se posameznik
> premesti v drug družbeni razred ali v drugo etnijo, se jih nikoli povsem ne
> otrese [@leroigourhan1990gib2, 25].

> Vsa človeška evolucija prispeva k temu, da se zunaj človeka znajde tisto, kar
> v siceršnjem živalskem svetu ustreza prilagoditvi vrste.  Najbolj
> presenetljivo materialno dejstvo je zagotovo "sprostitev" orodja, vendar je v
> resnici temeljno dejstvo sprostitev besede in tista enkratna človekova
> lastnost, da postavlja svoj spomin ven iz samega sebe, v družbeni organizem
> [@leroigourhan1990gib2, 28].

> [V]pletenost orodja in giba v organe, ki so zunaj človeka, ima vse
> značilnosti biološke evolucije, saj se enako kakor možganska evolucija
> razgrinja skozi čar z dodajanjem elementov, ki izpopolnjujejo operativni
> proces, ne da bi ti drug drugega odpravljali. [...] Obstoj in delovanje
> samodejnega stroja s kompleksnim programom prav tako zahteva, da na
> posameznih nadstropjih izdelave, reguliranja in popravljanja v mraku
> sodelujejo vse kategorije tehničnega giba od obdelovanja kovine do uporabe
> pile in navijanja električnih žic na tuljave, do bolj ali manj ročnega ali
> mehaničnega povezovanja kosov [@leroigourhan1990gib2, 34].

> To, da je mogoče v stroj potisniti les, ne da bi nam bilo mar za žile in
> grče, in da iz njega pride avtomatično zapakirana standardna parketna
> deščica, je brez slehernega dvoma zelo pomembna družbena pridobitev, toda to
> pušča človeku zgolj možnost, da se odpove temu, da bi ostal *sapiens*, in da
> postane nekaj, kar je morebiti boljše, a je zagotovo drugačno. Toda
> razmisliti moramo o zoološkem človeku, ki se ne bo spremenil v enem stoletju,
> in poiskati izhode, ki so mu na voljo, če hoče imeti drugačen življenjski
> občutek kakor to, da je razosebljena celica v organizmu, pa čeprav je ta
> čudovito planetariziran [@leroigourhan1990gib2, 48].

- za človeško vrsto je značilna zunanja tehničnost, naša orodja so
  odstranljiva

- za zunanjo tehničnost je značilna hitrejša evolucija od biološke
  (vzporedna in hitrejša evolucija), ne poteka kot simulacija biološke
  evolucije ampak začne razvijati lastne vzorce (avtonomna evolucija)

- omejenost človeške intelektualnosti proti teoretični neomejenosti
  strojne inteligence

- 1. eksternalizacija orodja
- 2. eksternalizacija giba
- 3. eksternalizacija spomina
- 4. eksternalizacija programa

- 5. eksternalizacija programiranja (?)


## Marx, *Kapital*

Kapitalistični stroji in industrija.

- pojem ročne virtuoznosti

- družbena delitev dela -> tehnična delitev dela

  Prejšnja delitev dela ne preživi, se spremeni. Stroji zdaj nastopajo kot
nadrejeni. Delitev dela je funkcija funkcij strojev. Delitev dela sledi
tehnološkim imperativom. Ni socialnega pogajanja s strojem.

  preddelavci, delavci s stroji, "feeders", tehniki

- posebnost kapitalizma je, da tehnološki razvoj poteka hitreje in bolj
  kontinuirano (tu je neka povezava s [produktivnim delom](produktivno_delo.md)).

- Marx je dober, ker brzda svoje konzervativne impulze. Njegov refleks ni, da
  se vrača v zlato dobo obrti.

- str. 347: Stroji delujejo samodejno ampak nimajo zavesti. Marxov obrat:
  stroji si podredijo ljudi kot njihova čutila (obrat od L-G, stroji niso
zunanja čutila ljudi, ampak so ljudje zunanja čutila strojev).

- Sprememba družbene delitve dela. Ne več samo literati ipd. ampak nastanek
  tehnične inteligence.

- Ad humanistični zadržek. Ne gre za prehajanje sposobnosti od ljudi do
  strojev. Res skozi generacije je manj ene virtuoznosti, ampak zato ker so te
sposobnosti "podružbljene" in zato individualno virtuoznost postane družbeno
objektivno irelevantna.

  In ne gre za prehajanje točno teh veščin, ampak stroji razvijejo lastno
virtuoznost. Ne imitira biološkega telesa.

- Po Marxu: ključno pri tovarnah je, da gre za zaprte in kontrolirane,
  izolirane sisteme.

- vpr: kdaj stroji postanejo prijazni? (ko se začnejo vpeljevat v
  gospodinjstva)


## Heinrich, *Kritika politične ekonomije*

*Konstantni in variabilni kapital*.

Podobno z delom (v kapitalizmu?) ima tudi *kapitalistični produkcijski proces*
dvojni značaj: je enotnost konkretnega *delovnega procesa* in *procesa
uvrednotevanja* -- producira izdelke *in* presežno vrednost
[@heinrich2013kritika, 99].

Konstantni kapital:

$$c$$

Presežna vrednost:

$$m$$

Variabilni kapital:

$$v$$

Vrednost množine blag, ki je sproducirana v določenem obdobju, lahko zapišemo
kot:

$$c + v + m$$

*Stopnja presežne vrednosti*.

Mera uvrednotenja ali stopnja presežne vrednosti:

$$m / v$$

*Profitna stopnja*.

$$m / (c + v)$$

[na kratko kam to pelje v tretjem zvezku]

*Delovni dan*.

*Razlika med kapitalističnimi in predkapitalističnimi družbami*. Pri obeh je
mogoče razlikovati "nujni delovni čas" in "presežni delovni čas", toda v
družbah (ekonomskih družbenih formacijah) kjer prevladuje uporabna vrednost
produkta, je presežno delo omejeno z ožjim ali širšim krogom potreb vladajočega
razreda. Za kapitalistično družbo je značilna neomejena potreba po presežnem
delu [@heinrich2013kritika, 103].

Ta neomejena potreba ni individualna moralna napaka posameznega kapitalista,
temveč posledica *logike* kapitalistične blagovne produkcije. Posamezni
kapitalist skuša maksimalno izkoristiti uporabno vrednost kupljenega blaga -- v
to ga sili tudi konkurenca drugih kapitalistov. Po drugi strani tudi delavec
skuša ohraniti svoje blago, da bo naslednji dan na razpolago za ponovno
prodajo. Oboji se sklicujejo na zakone blagovne menjave -- pravice prodajalcev
in kupcev. Meje delovnega dne se iz tega ne da izpeljati, ta se določa v
soočenju pravice proti pravici:

> Ko kapitalist poskuša karseda podaljšati delovni dan in, če je le mogoče, iz
> enega delovnega dne narediti dva, uveljavlja svojo pravico kot kupec. Na drugi
> strani specifična narava prodanega blaga vključuje mejo porabe tega blaga pri
> kupcu, in ko hoče delavec delovni dan omejiti na določeno normalno velikost,
> uveljavlja svojo pravico kot prodajalec. Tu imamo torej antinomijo, pravico
> proti pravici, obe enako zapečateni z zakonom blagovne menjave. Med enakima
> pravicama odloča sila. In tako se v zgodovini kapitalistične produkcije
> normiranje delovnega dne prikazuje kot boj za meje delovnega dne -- boj med
> vsemi kapitalisti, tj. razredom kapitalistov, in vsemi delavci oziroma
> delavskim razredom. [@marx2012kapital1, 193]

Poleg kritike meje politične ekonomije kot znanosti se nam tu zdi bistveno:

```
produkcija presežne vrednosti
  -> razredni boj
    -> produkcija relativne presežne vrednosti
      -> tehnološki napredek
```

*Absolutna in relativna presežna vrednost, prisilni zakon konkurence*. 

Predavanje v dveh delih:

1. razmerje med konkurenco in produktivnostjo (99-109)

   Dvojni značaj produkcijskega procesa. Dela konkretne stvari (ali storitve).
   Dela tudi presežno vrednost. To šele naredi kapitalistični produkcijski proces
   za *kapitalistični*.

   *variabilni in konstantni kapital*

   $$D - D'$$

   $$D - B - D'$$

   $$D - ? - B - D'$$

   $$D - PS, DS - B - D'$$

   *presežna vrednost*

   *delovni dan*

   *absolutna in relativna presežna vrednost*

   *strukturni dejavniki – individualna motivacija*

   ind. motiv. je povečanje presežne vrednosti lastnega obrata. Ekstra presežna
   vrednost je motivacija na ravni posameznega podjetja.
  
   *konkurenca*

[prejšnjič od konkurence do produktivnosti]
[danes od produktivnosti do tehnologije]

2. razmerje med produktivnostjo in tehnologijo (109-116, 121)

Vpr: zakaj so ravno stroji najzaneslivejši zviševalci produktivnosti?

Koncept: realne subsumpcije

Čeprav ne uporablja tega izraza, Marx govori o mehanizaciji (delo, ki je bilo
prej ročno se zdaj izvaja strojno/mehansko)

Družbena delitev dela je delitev, ki jo narekuje družba

Marx imenuje tehnična delitev dela tista, ki jo narekujejo kapitalistični stroji

- Bruneleschijeva kupola:
  
  V kolikor gre za prestiž in privilegije gre za vzpostavitev nove družbene
  deliteve dela v razmerju do starih obrtnikov.

  V kolikor gre za novo ekonomsko razmerje v razmerju do deklasiranih delavcev
  gre za vzpostavitev tehnične delitve dela.

Materializacija imperativov kapitala je v strojih. "Materialna eksistenca
ideologije" je ovinek do govora o diskurzih ...

## Morris-Suzuki, *Beyond computopia*


### Monopolni kapital

Morris-Suzuki na podlagi Barana in Sweezeya ter Bravermana glavne elemente
modela "monopolnega kapitala" povzema tako:

> 1. ekonomija kjer prevladujejo velike korporacije, katerih konkurenčne
>    instinkte omejuje močna potreba po vzajemni samoohranitvi;
> 2. vedno večji ekonomski presežek, ki se usmerja v neproduktivne rabe
>    (prodaja, militarizem, odpadki) in vedno znova grozi, da bo zajel sistem;
> 3. menedžerski vladajoči razred pri katerem je lastnina kapitala posplošeno
>    (tj. razred kot celota ima v lasti večino kapitala kot celote, posamezniki
>    pa običajno niso lastniki določenega fragmenta kapitala s katerim
>    upravljajo);
> 4. obsežen, dekvalificiran (razveščinjen) in splošno pasiven delavski razred;
>    ter
> 5. sloj tehničnih in profesionalnih delavcev ki, po Bravermanu, vzpostavljajo
>    "pravi srednji sloj", se pravi zasedajo ekonomski položaj med upravljalci
>    in upravljanimi. [@morrissuzuki1988beyond, 73-74]

### Produkcija informacij

Posledica fizične razdružitve delavca (živega dela) in stroja (nakopičenega
dela) je, da se ekonomski "center" premakne od produkcije dobrin k produkciji
inovacij (tj. produkciji novega znanja za izdelavo dobrin). Razloga sta predvsem
dva:

1. Avtomatizacija poveča hitrost in zmanjša strošek produkcije dobrin. Ob
   obstoječih mejah trga bi to vodilo v nasičenje trga in stagnacijo, če
   podjetja ne bi vse večjega deleža sredstev namenjala nenehnemu preoblikovanju
   in nadgrajevanju svojih produktov.
2. Zaradi vse manjše potrebe po delavcih za produkcijo samih dobrin podjetja vse
   težje ustvarijo profit na podlagi izkoriščanja delavcev v proizvodnji.
   [@morrissuzuki1988beyond, 76]

Iz slednjega bi lahko po tradicionalni socialistični analizi kapitalizma lahko
sklepali, da avtomatizacija vodi v spontan zlom kapitalizma. Toda nenehna
inovacija nudi izstop iz te pasti.

***

*avtomatizacija* (v 80. letih, na Japonskem)

*informacijski kapitalizem*

Beyond Computopia: kritika optimističnih narativov o razvoju računalnikov

*general intellect*, obči razum (splošni intelekt) (meti 1 08 str 506)

: skoncentrirana družbena vednost, ki (na eni točki) v kapitalizmu deluje kot
produktivna sila

## Ramtin, *Capitalism and automation*

- kapitalizem in avtomatizacija II

- Ramtinov point: avtomatizacija prinaša kvalitativen prelom v zgodovini
  kapitalizma. Diskuntiuteta je v tem, da se nadzor (funkcija nadzora) prenese
  na stroje. To naznanja tudi začetek aktivne vloge tehnologije znotraj
  produkcijskega načina.

- gl. Althusser "materialnost ideologije" iz vidika reprodukcije kapitalizma na
  podlagi tehnične realnosti strojev -- zgodovina softwarea in zgodovina
  kibernetike je tu pomembna.

  Po Althusserju mora obstajati nekaj zunanjega, kar reproducira kapitalizem.
  Vprašanje, če to še drži, da reprodukcija produkcijskih razmerij potrebuje
  nekaj zunanjega.

- *fetiš kapitala/kapitalski fetiš*
  
  Fetiš blaga: razumeti blago, kot da bi blaga sama po sebi imeli vrednost.

  Fetiš kapitala: razumeti kapital, kot da bi denar (kapital) sam po sebi imel
  nekatere lastnosti. Razumeti kapital, kot da ima sam po sebi moč svojega
  povečevanja. Marxov point: kapital (stroji) nima nobene moči, sam po sebi,
  ampak zgolj v svojem družbenem kontekstu.

- (M. Graziano o animizmu)

  Značilno za človeški um je, da bitjem in stvarem pripisujemo um. Gre še en
  korak naprej, da tudi ljudem pripisujemo um.

- Ramtin

  1. prenos moči
  2. transformacija gibanja
  3. nadzor

- "Osiromašenje" okolja je pogoj za delovanje (primitivnih) strojev.

  (Pri gradnji nimamo te možnosti, to je pomembno v luči "strukturne"
  zaostalosti avtomatizacije gradbišča.)

- Gl. Ramtin o Nobel, *Forces of Production*

  Pomanjkljivost Ramtina in Nobla: ni nemogoče, da kapitalizem ne bi spodbujal
  nepokorščino, torej nepokorščina je lahko tudi produktivna. Torej kapitalu ne
  gre za disciplino samo po sebi. "Clash" med produktivnostjo in našo
  nedesciplino je na ravni tega, da smo živa bitja. Naša telesa so dana.
  (možno dopolnit z Leroi-Gourhan)

- Ramtinov point: ločevanje softwarea od hardwarea, sprememba razrednih
  razmerij, ...

### Avtomatizacija in proces koncepcije

> At present the process is in its infancy, and the hardware and software
> components of this process appear as 'tools' and 'aids' to the conception
> workers rather than as their dispossessors. But the technology of, for
> instance, computer-aided design (CAD) -- developed on the basis of
> minicomputers, digitizing boards and visual display units -- contains, in
> embryonic form, the very elements which can lead to a greater objectiflcatlon
> of the process of conception. [@ramtin1991capitalism, 75]

## Dyer-Witheford, Kjosen in Steinhoff, *Inhuman power*

- kapitalizem in UI

- (gl. Good, Intelligence explosion)

- Človeška inteligenca nam ni transparentna, zato je ne moremo imeti za
  *benchmark* pri vrednotenju umetne inteligence. Kar pa nam je jasno, po čemer
  pa lahko sodimo, je *splošnost* človeške inteligence (prožnost, kreativnost,
  improvizacija). Zaenkrat UI tega še ne dosega, dobre so zgolj na svojem
  področju. "Narrow AI" vs. AGI.

- (gl. Virno -- mnoštvo ..., Vercelone)
- (gl. Pamela McCorduck, Sautoy, The Creativity Code)


## Parisi, "Instrumental reason"

- kapitalizem in UI II -- dinamična avtomatizacija

> Within the context of this *all-machine phase* transition of digital
> capitalism, it is no longer sufficient to side with the critical theory that
> accuses computation to be reducing human thought to mere mechanical
> operations. As information theorist Gregory Chaitin has demonstrated,
> incomputability and randomness are to be conceived as very condition of
> computation. If techno-capitalism is infected by computational randomness and
> chaos, the traditional critique of instrumental rationality therefore also has
> to be put into question: the incomputable cannot be simply understood as being
> opposed to reason. [@parisi2015instrumental, 125]


- Dinamična avtomatizacija, algoritemski kapitalizem
- zastarelost klasične kritične teorije v razmerju do sodobne tehnološke
  realnosti
- onkraj človeškega odzivnega časa, nova strojna ekologija

  - Po LG, ko človek postane živi fosil (prehiti ga tehnologija)
  - McKanzie trading at the speed of light (2021)

- Zanima nas strukturna povezanost UI (tehnologije) in kapitalizma. Ne različni
  načini (in z njimi povezani problemi) uporabe tehnologije v kapitalizmu.

- Kritika instrumentalnega uma?

## Hui, "ChatGPT, or the eschatology of machines"

Karl Löwith, *Meaning in history*: moderna filozofija zgodovine je
sekularizacija eshatologije.


"[N]ovost in pomen umetne inteligence je skrita [buried] za eshatološkim
imaginarijem, za sodobnimi [modern] stereotipi strojev in industrijske
propagande." [@hui2023chatgpt, 3]


Ne-mehanistični stroji.

Obravnava odziva: "nothing unusual, stroji as usual"

Dupuy, the mechanization of mind
Geogheigan


gl. Ducrout, slovenian lectures

## Bratton

O kritiki antropocentrizma pri obravnavi tehnologije.

Antropocentrizem: da človeška inteligenca nastopa kot merilo pri presojanju ali
obstaja umetna inteligenca.

Kopernikanska trauma: planet ni v središču vesolja. Po brattonu je zadnje
zatočišče tega napuha izrek, da še vedno je člov. inteligenca v središču.

gl. bergson kreativna revolucija


<!--

(346--361)

  Predavanje 3: Simondon on the mode of existence ... 129-159

  4: Heinrich, Uvod 99-116, 121.

  5: morris suzuki tessa beyond computopia, 5. poglavje

  6: ramtin ramin capitalism and automation 29-59

  7: Dyer Witheford: Inhuman power 110-144

  8: Parisi, Luciana Instrumental reason, v Matteo Pasquinelli Alleys of
  your mind, 125-137

  9: Hun Yuk ChatGPT v eflux

  10: Bratton Outing artificial intelligence, v Alleys of your mind,
  69-80

-->


---
lang: sl
references:
- type: book
  id: leroigourhan1990gib1
  author:
  - family: Leroi-Gourhan
    given: André
  title: "Gib in beseda"
  translator:
  - family: Rotar
    given: Braco
  volume: 1
  publisher-place: Ljubljana
  publisher: ŠKUC
  issued: 1988
  language: sl
- type: book
  id: leroigourhan1990gib2
  author:
  - family: Leroi-Gourhan
    given: André
  title: "Gib in beseda"
  translator:
  - family: Rotar
    given: Braco
  volume: 2
  publisher-place: Ljubljana
  publisher: ŠKUC
  issued: 1990
  language: sl
- type: book
  id: marx2012kapital1
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 1
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2012
  language: sl
- type: book
  id: heinrich2013kritika
  author:
  - family: Heinrich
    given: Michael
  title: "Kritika politične ekonomije: uvod"
  translator:
  - family: Dobnikar
    given: Mojca
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2013
  language: sl
- type: book
  id: morrissuzuki1988beyond
  author:
  - family: Morris-Suzuki
    given: Tessa
  title: "Beyond computopia: information, automation and democracy in Japan"
  title-short: "Beyond computopia"
  publisher-place: London
  publisher: Kegan Paul International
  issued: 1988
  language: en
- type: book
  id: ramtin1991capitalism
  author:
  - family: Ramtin
    given: Ramin
  title: "Capitalism and automation: revolution in technology and capitalist breakdown"
  title-short: "Capitalism and automation"
  publisher-place: London
  publisher: Pluto Press
  issued: 1991
  language: en
- type: book
  id: dyerwitheford2019inhuman
  author:
  - family: Dyer-Witheford
    given: Nick
  - family: Kjosen
    given: Atle Mikkola
  - family: Steinhoff
    given: James
  title: "Inhuman power: artificial intelligence and the future of capitalism"
  title-short: "Inhuman power"
  publisher-place: London
  publisher: Pluto Press
  issued: 2019
  language: en
- type: chapter
  id: parisi2015instrumental
  author:
  - family: Parisi
    given: Luciana
  title: "Instrumental reason, algorithmic capitalism, and the incomputable"
  title-short: "Instrumental reason"
  container-title: "Alleys of your mind: augmented intellligence and its traumas"
  editor:
  - family: Pasquinelli
    given: Matteo
  publisher-place: Lüneburg
  publisher: meson press
  issued: 2015
  page: 125-137
  language: en
- type: article-journal
  id: hui2023chatgpt
  author:
  - family: Hui
    given: Yuk
  title: "ChatGPT, or the eschatology of the machines"
  title-short: "ChatGPT"
  container-title: "e-flux"
  issue: 137
  issued:
    month: 6
    year: 2023
  page: 3-10
  language: en
- type: chapter
  id: bratton2015instrumental
  author:
  - family: Bratton
    given: Benjamin
  title: "Outing artificial intelligence: reckoning with turing tests"
  title-short: "Outing artificial intelligence"
  container-title: "Alleys of your mind: augmented intellligence and its traumas"
  editor:
  - family: Pasquinelli
    given: Matteo
  publisher-place: Lüneburg
  publisher: meson press
  issued: 2015
  page: 69-80
  language: en
# vim: spelllang=sl
...
