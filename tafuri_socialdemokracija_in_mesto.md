---
title: "Manfredo Tafuri, Socialdemokracija in mesto v Weimarski republiki"
...

K. Korsch, *Consigli di fabbrica e socializzazione*, Bari, Laterza,
1970 (1^a ed.: *Schrifter zur Sozialisierung. Arbeitsrecht für
Betriebsräte*, 1922).

Ossip K. Flechteim, *Il partito comunista tedesco (KPD) nel periodo
della Repubblica di Weimar*, con introduzione di Herman Weber, Milano,
Jaca Book, 1970 (1^a ed.: *Die KPD in der Weimarer Republik*, 1948).

E. Collotti, *La Germania dei Consigli*, "il manifesto" 5 1970, pp.
56--66.

E. Collotti, *Il Bauhaus nell'esperienza politico-sociale della
Repubblica di Weimar*, "Controspazio" 4-5 1970, pp. 8--15.

C. Aymonino, *Origini e sviluppo della città moderna*, Padova,
Marsilio, 1971^2.

C. Aymonino (a cura di), *L'abitazione razionale. Atti dei congressi
CIAM 1929-1930*, Padova, Marsilio, 1971.

B. Miller Lane, *Architecture and Politics in Germany, 1918--1945*,
Cambridge (Mass.), Harvard University Press, 1968.

K. Junghanns, *Die Beziehungen zwischen deutschen und sowjetischen
Architekten in den Jahren 1917 bis 1933*, "Wissenschaftliche Zeitschrift
der Humboldt Universität zu Berlin" 3 1967, p. 29 ss. 

*Die Form. Stimme des deutschen Werkbundes*, Berlin, Ullstein, 1969.

J. Elderfield, *Dissenting Ideologies and the German Revolution*,
"Studio International" 927 1970, pp. 180--187.

<!-- Ripercorrere le vicende delle realizzazioni urbanistiche promosse
dalla socialdemocrazia e dalle forze sindacali nel periodo della
repubblica di Weimar ha un risvolto estremamente attuale, in quanto in
quelle vicende è dato scorgere il primo apparire di una prassi
largamente assunta, in seguito, come obbiettivi specifici del movimento
operaio organizzato. I grandi temi della lotta alla rendita fondiaria,
della gestione cooperativistica dell'edilizia popolare, del rinnovamento
tecnologico del ciclo edilizio medesimo, appaiono infatti, nel periodo
considerato, come motivi connessi in modo talmente immediato con la
gestione socialdemocratica della città, da offrire con la massima
evidenza motivi di critica storicamente fondati e verificabili alla
tradizione delle rivendicazioni avanzate dai partiti ufficiali della
classe, nel campo della politica delle abitazioni e dell'amministrazione
urbanistica. -->

> Ponovno prehoditi zgodbo urbanističnih realizacij, ki so jih
> spodbijale socialdemokracija in sindikalne sile v obdobju Weimarske
> republike, ima izjemno aktualen vidik, saj je v teh zgodbah prvič
> podan vpogled v prakso, ki je pozneje na široko privzeta kot poseben
> cilj delavskega gibanja. Velike teme boja z zemljiško rento,
> zadružnega upravljanja socialne gradnje, tehnološke prenove samega
> gradbenega cikla se v obravnavanem obdobju dejansko pojavljajo kot
> motivi, ki so tako neposredno povezani s socialdemokratskim
> upravljanjem mesta, da z največjo očitnostjo ponujajo zgodovinsko
> utemeljene in preverjene motive za kritiko tradicije zahtev, ki jih
> predlagajo uradne razredne stranke na polju stanovanjske politike in
> urbanistične administracije.

<!-- Non è certo un caso che da un lato la "Germania dei Consigli",
dall'altro la Germania delle sperimentazioni urbanistiche di
avanguardia, siano oggi oggetto di una nuova attenzione. Né è un caso
che là dove le connessioni fra livello politico di intervento e ricerche
disciplinari in campo architettonico o urbanistico vengono criticamente
impostate -- vedi i saggi di Collotti, della Miller Lane o di Aymonino
-- il significato primario di quella stessa connessione risulti sfumato
o reso ampiamente equivoco. È infatti veramente difficile, oggi,
prescindere -- nell'analisi della socialdemocrazia tedesca nel suo
aspetto "classico" -- dalle acute interpretazioni offerte recentemente
da Mario Tronti nel suo "Poscritto" alla nuova edizione di *Operai e
capitale* (1971, pp. 267 e ss.). "Lukács aveva visto giusto -- egli
scrive (p. 279) -- nel mettere a nudo l'essenza della 'tattica
socialdemocratica', secondo cui il proletario deve fare dei compromessi
con la borghesia, perché la rivoluzione vera risulta ancora lontana e le
sue vere condizioni non sussistono ancora: 'Quanto più maturano i
presupposti soggettivi ed oggettivi della rivoluzione sociale, con tanta
maggiore *purezza* il proletariato può realizzare i propri fini di
classe. Sicché il compromesso nella prassi presenta, sul rovescio della
medaglia, un grande radicalismo, una volontà di assoluta purezza dei
principi con i fini ultimi'. Questa è la socialdemocrazia, quella vera,
quella classica e storica [...]. Sta di fatto che tra lotta operaia e
partito socialdemocratico il contatto è stato fin da principio diretto,
il rapporto talmente stretto da non permettere neppure la mediazione del
livello sindacale; il trade-unionismo risulta assente dalla tradizione
operaia tedesca". -->

> Zagotovo ni naključje, da sta "Nemčija svetov" na eni strani in
> Nemčija urbanističnih eksperimentov avantgard na drugi danes predmet
> nove pozornosti. Niti ni naključje, da je tam, kjer so kritično
> vzpostavljene povezave med ravnjo političnega poseganja in
> disciplinarnih raziskav na polju arhitekture ali urbanizma -- glej
> eseje Collottija, Miller Lane ali Ayomnina --, primarni pomen te iste
> povezave zabrisan ali v veliki meri napravljen v dvoumnega. Dejansko
> je danes res težko spregledati pronicljive interpretacije, ki jih je
> nedavno ponudil Mario Tronti v svojem "Pripisu" k novi izdaji *Operai
> e capitale* (1971, str. 267 in dalje). "Lukács je imel prav," piše
> Tronti (str. 279), "ko je razgalil bistvo 'socialdemokratske taktike'
> po kateri mora proletariat sklepati kompromise z buržoazijo, ker je
> prava revolucija še daleč in ker njeni pravi pogoji še ne obstajajo:
> 'Bolj ko zorijo subjektivne in objektivne predpostavke socialne
> revolucije, z večjo *čistostjo* lahko proletariat realizira lastne
> razredne cilje. Tako kompromis v praksi, na drugi strani medalje,
> predstavlja velik radikalizem, željo po popolni čistosti principov do
> končnega cilja'. To je socialdemokracija, prava, klasična in
> zgodovinska [...]. Ostaja dejstvo, da je bil stik med delavskim
> bojem in socialdemokracijo od samega začetka neposreden, odnos tako
> tesen, da ni dopuščal niti posredovanja sindikalne ravni; sindikalizma
> v nemški delavski tradiciji tako ni".

<!-- Ma per comprendere i fenomeni storici che seguono la "rivoluzione
di novembre" e che vedono cadere tale "soluzione di organizzazione"
basata sulla saldatura di una "pratica quotidiana di azioni mensceviche
e un'ideologia di puri principi sovversivi", bisogna considerare la
seconda faccia della medaglia rilevata da Tronti. Vale a dire mettere
l'accento su quel "livello medio di mediocrità intellettuale, di
approssimazione scientifica, di miseria teorica, che potevano solo
produrre il guasto che hanno prodotto: quella cura scolastica della
verità marxista, che da Lenin in poi ancora dobbiamo perdere tempo a
combattere" (Tronti, p. 280). -->

> Toda za razumevanje zgodovinskih pojavov, ki sledijo "novembrski
> revoluciji" in ki so priča padcu tej "organizacijski rešitvi", ki je
> temeljila na spoju "vsakdanje prakse menjševiških dejanj in ideologije
> čistih subverzivnih principov", je treba upoštevati drugo plat
> medalje, na katero je opozoril Tronti. To pomeni poudariti tisto
> "povprečno raven intelektualne povprečnosti, znanstvene aproksimacije,
> teoretske bede, ki je lahko proizvedla zgolj tisto kar je proizvedla:
> tisto sholastično obravnavo marksistične resnice, proti kateri moramo
> še vedno od Lenina dalje izgubljati čas boriti se" (Tronti, str.
> 280).

<!-- Di fronte al rinnovarsi e al crescere della scienza del capitale,
né la socialdemocrazia posteriore al momento "classico" -- quella
dell'SPD, per intenderci, pienamente immersa ormai nell'orizzonte
trade-unionista --, né il sorgere dell'opposizione spartachista, del KPD
o dell'USPD, hanno potuto e saputo rispondere con strumenti adeguati
alle premesse organizzative della socialdemocrazia "storica". Da un lato
la nuova e spregiudicata scienza capitalistica, l'avalutatività di un
Weber, le tecniche anticicliche di un Keynes; dall'altro il continuo
richiamo a visioni ottimistiche della storia e della sua graduale ascesa
verso un orizzonte "operaio", o a lotte "etiche", nel migliore dei casi,
non coscienti della loro collocazione nel ciclo economico complessivo.
-->

> Spričo obnove in rasti znanosti kapitala se niti socialna demokracija
> po "klasičnem" trenutku - jasno, socialdemokracija SPD, ki je bila do
> tedaj v celoti potopljena v sindikalistično obzorje - niti vzpon
> špartakovske opozicije, KPD ali USPD, nista mogli in nista mogli z
> ustreznimi instrumenti odzvati na organizacijske predpostavke
> "zgodovinske" socialne demokracije. Na eni strani nova in
> nepristranska kapitalistična znanost, Webrova avalutativnost,
> Keynesove anticiklične tehnike; na drugi strani nenehno sklicevanje na
> optimistične vizije zgodovine in njeno postopno približevanje
> "delavskemu" horizontu ali na "etične" boje, ki se v najboljšem
> primeru ne zavedajo svojega mesta v celotnem gospodarskem ciklu.

<!-- Il fenomeno già paradossale in tal senso è che la politica
"sociale" dei comuni socialdemocratici, dal '24 in poi, non faccia che
seguire l'esempio della tattica del grande capitale tedesco. I miliardi
affluiti in Germania sulla base del piano Dawes, dopo i! 24 appunto,
potenziano, certo, un sistema industriale tecnologicamente avanzato e ad
alta composizione organica di capitale, ma provocano,
contemporaneamente, un irrigidimento notevole dei cartelli industriali,
un artificioso rigonfiamento dei prezzi e del mercato interno, un
sistema convulso di esportazioni. (Si pensi alle vaste forniture
tedesche alla Russia sovietica e agli accordi internazionali dei grossi
cartelli dell'acciaio, dell'elettronica, del settore chimico, delle
automobili, ecc.). Da un lato, quindi, una perfetta politica doganale,
tesa ad una stabilizzazione dei prezzi interni al livello più alto;
dall'altro una politica di spese pubbliche attuata palesemente in
funzione di contenimento della pressione operaia sul mercato del lavoro
e di frantumazione dello scontro di classe, sempre più pericolosamente
profilato al di là delle politiche ufficiali dei partiti operai. -->

> Pri tem je paradoksalno že to, da "socialna" politika
> socialdemokratskih občin od leta 24 dalje zgolj sledi zgledu taktike
> nemškega velekapitala. Milijarde, ki so se po letu '24 stekale v
> Nemčijo na podlagi Dawesovega načrta, so zagotovo okrepile tehnološko
> napreden industrijski sistem z visoko organsko sestavo kapitala,
> vendar so hkrati povzročile znatno zaostritev industrijskih kartelov,
> umetno napihovanje cen in domačega trga ter krčevit sistem izvoza.
> (Spomnimo se na obsežne nemške dobave sovjetski Rusiji in mednarodne
> sporazume velikih kartelov na področju jekla, elektronike, kemikalij,
> avtomobilov itd.) Na eni strani torej popolna carinska politika,
> katere cilj je bil stabilizirati notranje cene na najvišji ravni, na
> drugi strani pa politika javne porabe, ki se je očitno izvajala z
> namenom zajeziti pritisk delavcev na trg dela in razbiti razredni
> spopad, ki se je vse bolj nevarno profiliral zunaj uradne politike
> delavskih strank.

<!-- Non legare strettamente la gestione socialdemocratica dei comuni
tedeschi fra il '24 e il °33 a tale duplice attacco ai movimenti della
classe operaia significa tagliarsi ogni possibilità di effettiva
comprensione della struttura reale delle operazioni di politica
urbanistica, esaltate dalla critica borghese come modelli -- anche se
imperfetti nella loto strumentazione disciplinare -- di un momento
"eroico" dell'architettura e della città del movimento moderno. -->

> Če socialdemokratskega upravljanja nemških občin med letoma '24 in '33
> ne povežemo strogo s takšnim dvojnim napadom na delavska gibanja,
> pomeni, da prekinemo vsako možnost učinkovitega razumevanja dejanske
> strukture delovanja mestne politike, ki jo meščanska kritika
> poveličuje kot model - četudi nepopolnega disciplinarnega
> instrumentarija - "junaškega" trenutka arhitekture in mesta modernega
> gibanja.

<!-- Il fallimento più completo della politica dei movimenti operai
ufficiali, pagato dalla classe operaia nel '19, dopo il putsch di Kapp,
dopo la liquidazione dei governi dei consigli di Sassonia e Turingia,
nel corso dei primi mesi della stabilizzazione, viene compensato con una
partecipazione delle stesse organizzazioni operaie alla folle corsa al
gonfiamento della spesa pubblica. E del resto, anche su questo piano,
l'oscillazione paurosa dell'esercito di riserva di disoccupazione --
700.000 operai disoccupati nel '24, 195.000 nel '25, 650.000 nel '28 --
dimostra l'inefficacia specifica di tali misure. -->

> Najbolj popoln neuspeh politike uradnih delavskih gibanj, ki ga je
> delavski razred plačal leta 19, po Kappovem puču, po likvidaciji vlad
> deželnih svetov Saške in Turingije, v prvih mesecih stabilizacije, je
> bil nadomeščen s sodelovanjem samih delavskih organizacij v nori
> naglici za napihovanje javne porabe. In tudi na tej ravni grozljivo
> nihanje rezervne vojske brezposelnih - 700.000 brezposelnih delavcev
> leta '24, 195.000 leta '25, 650.000 leta '28 - dokazuje specifično
> neučinkovitost teh ukrepov.

<!-- Ciò che va comunque sottolineato è l'indussolubile connessione fra
la precisa volontà capitalistica di spostare la propria corsa alla
massimizzazione dei profitti, dai benefici ottenuti nel corso del
periodo dell'inflazione a una più severa tattica doganale e di cartelli
internazionali, e la politica di spesa pubblica perseguita dalle
autorità locali. L'attacco alla politica indiscriminata delle spese,
sferrato dal presidente della Reichsbank, Schacht, ha senza dubbio un
risvolto palesemente di destra, teso a porre sotto accusa i comuni
socialdemocratici. Ma è incontestabile che le spese sociali effettuate
da quei comuni si ponevano nel quadro di un puro anticapitalismo
romantico, basato sulla pretesa di poter ignorare -- nel momento in cui
ne erano parte determinante -- le leggi dell'accumulazione
capitalistica. Dopo il '24 -- ha scritto Arthur Rosenberg -- "le
autorità tedesche credevano che il danaro non avesse alcuna importanza e
che in ogni momento se ne potesse avere quanto se ne voleva"; mentre il
cancelliere Stresemann esprimeva, sullo stesso piano, al borgomastro D.
Jarres, nel '27, le proprie preoccupazioni per l'incoerenza fra gli
sproporzionati capitali di spese sociali dei comuni di Berlino, Colonia
e Francoforte e le loro conseguenze sulla politica estera. -->

> Poudariti pa je treba neločljivo povezavo med točno določeno
> kapitalistično željo, da bi svojo tekmo za čim večji dobiček
> preusmeril iz koristi, pridobljenih v obdobju inflacije, v strožjo
> carinsko in mednarodno kartelno taktiko, ter politiko javne porabe, ki
> jo vodijo lokalne oblasti. Napad na nekritično politiko porabe, ki ga
> je sprožil predsednik Reichsbanke Schacht, ima nedvomno očitno
> desničarski prizvok, katerega cilj je obtožiti navadne socialne
> demokrate. Toda nesporno je, da je bila socialna poraba teh občin del
> čistega romantičnega antikapitalizma, ki je temeljil na pretvarjanju,
> da je mogoče zanemariti - ko je bila odločilen del - zakone
> kapitalistične akumulacije. Po letu '24," je zapisal Arthur Rosenberg,
> "so nemške oblasti verjele, da je denar nepomemben in da ga ima človek
> lahko kadar koli toliko, kolikor hoče." Kancler Stresemann pa je leta
> '27 na isti ravni burmistru D. Jarresu izrazil zaskrbljenost zaradi
> neskladja med nesorazmernimi socialnimi izdatki občin Berlin, Köln in
> Frankfurt ter njihovimi zunanjepolitičnimi posledicami.

<!-- Su questo sfondo ci si provi ora a valutare l'opera svolta dai
gestori della politica socialdemocratica in campo urbanistico. Il
principio primo che sembra emergere al proposito è il tentativo di
contrapporre a una "criminale anarchia della produzione" una
razionalizzazione della distribuzione, direttamente gestita, nei singoli
settori controllabili dalle organizzazioni sindacali dalla stessa classe
operaia, e -- cosa ancora più importante -- un'esemplificazione delle
autonome capacità organizzative dei sindacati nel campo della produzione
stessa, in quella sintesi mistificata per eccellenza di capitale e
lavoro che è la produzione cooperativistica. -->

> Glede na to poskušajmo oceniti delo, ki so ga opravili vodje
> socialdemokratske politike na področju urbanizma. Prvo načelo, ki se
> zdi, da se pri tem pojavlja, je poskus zoperstaviti "zločinsko
> anarhijo proizvodnje" z racionalizacijo distribucije, ki jo neposredno
> upravljajo sindikati, v posameznih sektorjih, ki jih nadzira sam
> delavski razred, in - kar je še pomembneje - s prikazom avtonomnih
> organizacijskih zmogljivosti sindikatov na samem področju proizvodnje,
> v tej mistificirani sintezi par excellence kapitala in dela, ki je
> kooperativna proizvodnja.

<!-- Le cooperative di produzione edilizia, fondate nel 1919 da Martin
Wagner, e l'*Allgemeiner Deutscher Gewerkschaftsbund* (ADGB), la lega
sindacale operante a fianco della socialdemocrazia, sono in prima linea
nel tradurre tutte le istanze *etiche* del Linkskommunismus espresse da
un Karl Korsch e dal movimento consigliare, in metodi di intervento nel
settore delle costruzioni, come freno contro la speculazione fondiaria e
come azione calmieratrice nei confronti del livello degli affitti. -->

> Gradbene proizvodne zadruge, ki jih je leta 1919 ustanovil Martin
> Wagner, in *Allgemeiner Deutscher Gewerkschaftsbund* (ADGB),
> sindikalna zveza, ki je delovala skupaj s socialno demokracijo, so
> bile v ospredju pri prenosu vseh *etičnih* zahtev Linkskommunismus, ki
> jih je izrazil Karl Korsch in gibanje svetov, v metode posredovanja v
> gradbenem sektorju kot zavoro proti špekulacijam z zemljišči in kot
> pomiritev višine najemnin.

<!-- Esattamente in coincidenza del nuovo afflusso di capitali
statunitensi del piano Dawes, l'ADGB affianca alle originarie
cooperative di produzione la Dewog, vera e propria struttura
capitalistica, strutturata come società per azioni promossa da sindacati
operai e di impiegati, dalla banca del sindacato, dalle cooperative
edili stesse. -->

> Natanko istočasno z novim prilivom ameriškega kapitala iz Dawesovega
> načrta je ADGB poleg prvotnih proizvodnih zadrug postavila Dewog,
> pravo kapitalistično strukturo, strukturirano kot delniško družbo, ki
> so jo spodbujali sindikati delavcev in uslužbencev, sindikalna banka
> in same gradbene zadruge.

<!-- Giustamente Collotti nota che "nel testo programmatico dell'ADGB
del 1928 è racchiusa la formulazione della 'democrazia economica'
destinata a fornire una ideologia alla SPD e alla stessa repubblica di
Weimar" (Collotti, *Il Bauhaus* cit., p. 14). -->

> Collotti pravilno ugotavlja, da je "v programskem besedilu ADGB iz
> leta 1928 vsebovana formulacija 'ekonomske demokracije', ki naj bi
> postala ideologija SPD in Weimarske republike" (Collotti, *The
> Bauhaus* cit., str. 14).

<!-- Attraverso le undici società figlie dislocate a Francoforte,
Altona, Berlino, Augusta, Lipsia, Monaco, Könisberg, Rostock e Schwerin,
la DEWOG intende porsi come un "germe di un'economia collettiva
(Gemeinwirtschaft) nel settore dell'edilizia e delle abitazioni
controllate dai sindacati [...]. Da una parte [essa] sostituisce come
organizzazione economica collettiva, amministratrice dell'edilizia e
degli alloggi, gli speculatori edili e i proprietari di case
capitalistici privati; dall'altra le aziende produttive costruttrici di
case organizzate nell'unione delle aziende edili a carattere sociale
soppiantano gli imprenditori edili, in quanto possessori dei mezzi di
produzione nel campo edilizio e dominatori dell'apparato produttivo"
(Fritz Naphtali, *Wirtschaftsdemokratie*, ecc., Frankfurt a. M. 1928;
1966^2, p. 107, cit. in Collotti, p. 14). -->

> DEWOG se je s svojimi enajstimi hčerinskimi družbami v Frankfurtu,
> Altoni, Berlinu, Augsburgu, Leipzigu, Münchnu, Könisbergu, Rostocku in
> Schwerinu želel predstaviti kot "seme kolektivnega gospodarstva
> (Gemeinwirtschaft) v gradbenem in stanovanjskem sektorju, ki ga
> nadzorujejo sindikati [...]. Po eni strani [nadomešča] gradbene
> in stanovanjske špekulante in zasebne kapitalistične lastnike
> stanovanj kot kolektivna gospodarska organizacija, ki upravlja z
> gradbeništvom in stanovanji; po drugi strani pa produktivna podjetja
> za gradnjo hiš, organizirana v sindikatu socialno usmerjenih gradbenih
> podjetij, izpodrivajo gradbene podjetnike kot lastnike proizvodnih
> sredstev v gradbenem sektorju in gospodarje proizvodnega aparata"
> (Fritz Naphtali, *Wirtschaftsdemokratie* itd, Frankfurt a. M. 1928;
> 1966^2, str. 107, navedeno v Collotti, str. 14).

<!-- Più che di "democrazia economica" dovremo quindi parlare, per la
politica dell'ADGB della DEWOG e della GEHAG -- la società a
partecipazione sindacale usata a Berlino da Martin Wagner, per la
realizzazione del programma edilizio concretato nelle famose Siedlungen
"razionaliste" -- di una ripresa, già di per sé snaturata e
settorializzata, del piano di socializzazione elaborato da Korsch e dal
movimento per 1 consigli operai sull'onda della "rivoluzione di
novembre". In sostanza, quindi, un'utopia fondata su un "capitalismo
democratico": a Korsch si somma Bernstein, -->

> Namesto o "ekonomski demokraciji" bi zato morali za politiko ADGB
> DEWOG-a in GEHAG-a - podjetja s sindikalno udeležbo, ki ga je v
> Berlinu uporabljal Martin Wagner, za uresničitev gradbenega programa,
> konkretiziranega v znamenitih "racionalističnih" Siedlungen-ih -
> govoriti o obnovitvi, že tako popačene in sektorizirane, načrta
> socializacije, ki sta ga po "novembrski revoluciji" izdelala Korsch in
> gibanje za 1 delavske svete. V bistvu torej utopija, ki temelji na
> "demokratičnem kapitalizmu": Korschu se je pridružil Bernstein,

<!-- L'essenza della "democrazia industriale" preconizzata dal
*Linkskommunismus* è del resto chiaramente esposta da Korsch nel '19 su
"Der Sozialist". "L'*inevitabile* conseguenza di ogni grande industria
meccanizzata -- egli scrive -- è la subordinazione e la mancanza di
libertà; anche su questo punto, in contrasto diretto con il 'socialismo
antiautoritario' degli 'anarcosindacalisti', gli assertori del
socialismo moderno si sono chiariti irrevocabilimente sino in fondo le
idee. Ogni grande industria meccanizzata è infatti lavoro organizzato, e
lavoro organizzato significa continua subordinazione di tutti i
partecipanti al lavoro della volontà unitaria della direzione [...].
Anche nella 'democrazia industriale' pienamente realizzata dell'epoca
socialista si deve quindi ancora applicare, anzi ancora più saldamente
un principio: *durante il lavoro* la massa dei lavoratori deve essere
passivamente subordinata a chi dirige il processo di produzione [...].
Ma chi deve svolgere questa funzione e per quanto tempo, lo decide la
democrazia operaia sovrana della società socialista [...]. In tal modo,
attraverso l'indispensabile *liberazione degli uomini attivi nella
produzione*, ci si assicura di non recare nessun danno alle leggi
economiche della forma di produzione più moderna e fruttuosa" (K.
Korsch, *Consigli di fabbrica* cit., pp. 60--61; corsivi nostri). -->

> Bistvo "industrijske demokracije", ki jo je predpostavljal
> *Linkskommunismus,* je poleg tega leta 19 v "Der Sozialist" jasno
> opredelil Korsch. "*Neizogibna* posledica vsake velike mehanizirane
> industrije," piše, "je podrejenost in pomanjkanje svobode; tudi v tej
> točki so zagovorniki modernega socializma v neposrednem nasprotju z
> 'antiavtoritarnim socializmom' 'anarhosindikalistov' nepreklicno
> pojasnili svoje ideje. Vsaka velika mehanizirana industrija je
> pravzaprav organizirano delo, organizirano delo pa pomeni stalno
> podrejanje vseh udeležencev pri delu enotni volji vodstva [...].
> Tudi v popolnoma uresničeni "industrijski demokraciji" socialistične
> dobe mora torej še vedno veljati eno načelo, celo še trdneje: *med
> delom* mora biti masa delavcev pasivno podrejena tistim, ki vodijo
> proizvodni proces [...]. Toda o tem, kdo bo to funkcijo opravljal
> in kako dolgo, odloča suverena delavska demokracija socialistične
> družbe [...]. Na ta način se z nujno *osvoboditvijo ljudi, ki so
> dejavni v proizvodnji,* zagotovi, da ekonomskim zakonitostim
> najsodobnejše in najplodnejše oblike proizvodnje ne bo storjena nobena
> škoda." (K. Korsch, *Tovarniški sveti,* cit. delo, str. 60-61; kurziva
> naša).

<!-- Ideologia sovietica + piano, dunque: la socialdemocrazia tedesca ne
erediterà tutti gli equivoci sul piano economico, eliminandone
accuratamente la sostanza specificamente politica. -->

> Sovjetska ideologija + načrt torej: nemška socialna demokracija bo
> podedovala vse njene nesporazume na ekonomski ravni in skrbno
> odpravila njeno specifično politično vsebino.

<!-- Tutte le difficoltà incontrate da Korsch e dal movimento dei
Consigli per conciliare a livello teorico le esigenze di una produzione
capitalistica autogestita con un ciclo di distribuzione e consumo
anch'essi autogestiti, e in cui fossero magicamente scomparsi
plusvalore, profitto, accumulazione e sviluppo, rientrano nel quadro del
bilancio fallimentare di quel socialismo come riappropriazione sociale
del "valore del lavoro", acutamente analizzato da Cacciari nella sua
tradizione storica (cfr. M. Cacciari, *Utopia e socialismo*,
"Contropiano" 3 1970, pp. 563--586). Nella riduzione di quegli equivoci
tecnici a prassi settoriale extraoperaia è la "miseria" storica della
prassi socialdemocratica; così come non possono essere sottaciute le
responsabilità, al proposito, del KPD, che controlla, fino al '23 80
comuni e 60 cartelli dell'ADGB a carattere locale (O. K. Flechteim, p.
190). -->

> Vse težave, s katerimi sta se soočila Korsch in gibanje svetov, da bi
> na teoretični ravni uskladila zahteve samoupravne kapitalistične
> proizvodnje s ciklom distribucije in potrošnje, ki je bil prav tako
> samoupraven in v katerem so presežna vrednost, dobiček, akumulacija in
> razvoj čudežno izginili, spadajo v okvir neuspešne bilance tega
> socializma kot družbene reapropriacije "vrednosti dela", ki jo je
> Cacciari v svoji zgodovinski tradiciji ostro analiziral (prim. M.
> Cacciari, *Utopia e socialismo*, "Contropiano" 3 1970, str. 563-586).
> V redukciji teh tehničnih nesporazumov na zunajdelavsko sektorsko
> prakso je zgodovinska "beda" socialdemokratske prakse; tako kot v tem
> pogledu ni mogoče spregledati odgovornosti KPD, ki je do leta 23
> obvladovala 80 občin in 60 lokalnih kartelov ADGB (O. K. Flechteim,
> str. 190).

<!-- L'autogestione del proprio sfruttamento e la mistificazione tutta
antimarxista del ciclo economico capitalista si ripresentano quindi,
nella politica sindacale e cooperativistica della socialdemocrazia
tedesca, ad un livello insostenibile di incoerenza sul piano della pura
concorrenza capitalistica, e ad un livello politico oggettivamente
antioperaio sul piano della tanto decantata "democrazia economica". -->

> Samoupravljanje lastnega izkoriščanja in povsem antimarksistična
> mistifikacija kapitalističnega gospodarskega cikla se tako v
> sindikalni in zadružni politiki nemške socialne demokracije ponovno
> pojavljata na nevzdržni ravni neskladnosti na ravni čiste
> kapitalistične konkurence in na objektivno protidelavski politični
> ravni na ravni tako hvaljene "ekonomske demokracije".

<!-- Spostando la competizione dal piano della lotta fra capitale e
lavoro a quello della lotta fra un settore gestito direttamente
dall'ADGB e dal SPD e il capitale privato, l'impasse diviene
inevitabile. La casa come "bene sociale" e la gestione della città
basata su un dualismo di forze economiche -- da un lato la gestione
capitalistica del territorio, dall'altro la gestione del solo settore
residenziale da parte del "capitale sociale" -- sfociano in una
inevitabile disfunzione economica complessiva. Il cui peso non può, a
sua volta, che ricadere su una classe operaia deviata su obiettivi di
lotta estranei ai propri diretti interessi, e responsabile -- rispetto
all'opinione pubblica sempre più antirepubblicana -- della crisi
montante dopo il '30. -->

> S tem ko se tekmovanje z ravni boja med kapitalom in delom prenese na
> raven boja med sektorjem, ki ga neposredno upravljata ADGB in SPD, ter
> zasebnim kapitalom, postane brezizhodna situacija neizogibna.
> Stanovanja kot "družbena dobrina" in upravljanje mesta na podlagi
> dualizma gospodarskih sil - na eni strani kapitalistično upravljanje
> zemljišč, na drugi strani upravljanje stanovanjskega sektorja samo s
> strani "družbenega kapitala" - povzročata neizogibno splošno
> gospodarsko disfunkcijo. Teža tega pa lahko pade le na delavski
> razred, ki je preusmerjen k ciljem boja, ki niso povezani z njegovimi
> neposrednimi interesi, in je odgovoren - glede na vse bolj
> protirepubliško javno mnenje - za krizo, ki se je stopnjevala po letu
> 1930.

<!-- In buona parte l'iniziativa cooperativistica e sindacale riesce --
nei centri a maggioranza socialdemocratica -- a fungere come calmiere
della rendita fondiaria e del livello dei fitti. Tralasciando il caso di
Magdeburgo, in cui Bruno Taut tenta un'impossibile traduzione a livello
urbano delle tematiche espressioniste e Dada nel pieno dell'inflazione
economica, dal '24 in poi inizia l'opera "esemplare" dell'edilizia
sociale guidata dagli architetti radicali della repubblica di Weimar. Ci
troviamo di fronte, anzitutto, all'accettazione di un nuovo ruolo per il
lavoro intellettuale. All'utopismo etico di Taut si sostituisce, con
l'opera tecnico-amministrativa degli architetti come Otto Haesler a
Celle, Ernst May a Francoforte, Fritz Schumacher ad Amburgo, Martin
Wagner a Berlino, un impegno diretto nella gestione concreta del ciclo
edilizio: dal livello politico, a quello meramente gestionale in seno
agli organi di controllo urbanistico, a quello produttivistico, a quello
architettonico. -->

> Zadružna in sindikalna pobuda je v središčih s socialdemokratsko
> večino v veliki meri uspela umiriti zemljiško rento in višino
> najemnine. Če pustimo ob strani primer Magdeburga, kjer je Bruno Taut
> sredi gospodarske inflacije poskušal nemogoče prenesti
> ekspresionistične in dadaistične teme na mestno raven, se je od leta
> '24 dalje začelo "zgledno" delo na področju socialnih stanovanj pod
> vodstvom radikalnih arhitektov Weimarske republike. Najprej se soočamo
> s sprejetjem nove vloge intelektualnega dela. Tautov etični utopizem
> je ob tehnično-administrativnem delu arhitektov, kot so Otto Haesler v
> Celju, Ernst May v Frankfurtu, Fritz Schumacher v Hamburgu in Martin
> Wagner v Berlinu, zamenjal neposreden angažma pri konkretnem
> upravljanju gradbenega cikla: od politične ravni prek povsem upravne
> ravni v organih za nadzor urbanizma do proizvodne ravni in
> arhitekturne ravni.

<!-- Il ruolo utopico, tradizionale per l'ideologia del movimento
moderno, sembra superato a favore di una reale e totale accettazione del
nuovo ruolo dell'architetto, come organizzatore di un ciclo economico in
cui la dislocazione del tema formale abbia perso tutte le sue antiche
valenze di variabile indipendente. -->

> Zdi se, da je bila utopična vloga, tradicionalna za ideologijo
> modernega gibanja, zamenjana za resnično in popolno sprejetje nove
> vloge arhitekta kot organizatorja gospodarskega cikla, v katerem je
> dislokacija formalne teme izgubila vso svojo nekdanjo vrednost kot
> neodvisna spremenljivka.

<!-- In luogo dell'ideologia del lavoro produttivo -- in cui si
impantanano i protagonisti delle avanguardie sovietiche dal '17 al '25
almeno -- sembra di poter leggere, nella Germania di Stresemann e della
"relativa stabilizzazione", l'istituzionalizzazione di un lavoro
intellettuale *direttamente* inserito nella produttività. Le stesse
relazioni presentate ai due congressi dei CIAM (Congressi internazionali
di architettura moderna) del '29 e del '30 dai tecnici tedeschi,
sembrano, al confronto con le proposizioni tutte utopiche (nel senso più
alto del termine; nell'ambito del tentativo borghese di un controllo
"rivoluzionario" del futuro) di un Le Corbusier, elaborati puramente
tecnici. -->

> Namesto ideologije produktivnega dela, v katero so bili zatopljeni
> vsaj protagonisti sovjetskih avantgard med letoma 17 in 25, se zdi, da
> je v Stresemannovi Nemčiji in "relativni stabilizaciji" mogoče
> prebrati institucionalizacijo intelektualnega dela, ki je *neposredno*
> vpeto v produktivnost. Ista poročila, ki so jih nemški tehniki
> predstavili na dveh kongresih CIAM (Mednarodni kongres moderne
> arhitekture) leta 1929 in 1930, se v primerjavi s povsem utopičnimi
> predlogi (v najvišjem pomenu besede; v kontekstu buržoaznega poskusa
> "revolucionarnega" obvladovanja prihodnosti) Le Corbusiera zdijo zgolj
> tehnični elaborati.

<!-- In realtà anche gli architetti-amministratori delle municipalità
socialdemocratiche andavano inseguendo un'utopia tutta immersa in un
clima densamente ideologico. I loro studi sulla standardizzazione delle
cellule residenziali e sulla definizione di relazioni ottimali fra
residenze e servizi, gli esperimenti nell'industrializzazione edilizia o
di alcuni suoi settori -- si pensi alla famosa Frankfurter Küche
standardizzata di May o alla Siedlung Rothenberg a Kassel di Haesler a
struttura in ferro e tamponamenti pre fabbricati per una sistematica
trasformazione del ciclo edilizio stesso, per un suo inserimento diretto
nel ciclo economico complessivo come industria finalmente al livello
delle imprese ad alta composizione organica di capitale, tutto tale
intenso lavoro di razionalizzazione tecnico-economica, cozza contro le
deficienze istituzionali del settore stesso. L'oscillazione del
dibattito fra l'indagine scientifica e la polemica ne è l'inevitabile
conseguenza. -->

> V resnici so si tudi arhitekti-upravitelji socialdemokratskih občin
> prizadevali za utopijo, potopljeno v gosto ideološko ozračje. Njihove
> študije o standardizaciji stanovanjskih celic in opredelitvi
> optimalnih razmerij med bivališči in storitvami, njihovi poskusi
> industrializacije gradnje stavb ali nekaterih njenih sektorjev -
> spomnimo se na znamenito Mayevo standardizirano Frankfurter Küche ali
> Haeslerjev Siedlung Rothenberg v Kasslu z železno konstrukcijo in
> montažnimi polnili za sistematično preoblikovanje samega gradbenega
> cikla, za njegovo neposredno vključitev v celoten gospodarski cikel
> kot panogo končno na ravni podjetij z visoko organsko sestavo
> kapitala, vse to intenzivno tehnično-ekonomsko racionalizacijsko delo
> trči ob institucionalne pomanjkljivosti samega sektorja. Neizogibna
> posledica tega je nihanje razprave med znanstveno raziskavo in
> polemiko.

<!-- Accanto alla DEWOG e alla GEHAG funziona nella repubblica di Weimar
la *Reichsforschungsgesellschaft*, un organismo ben distinto dal
Deutscher Werkbund, con compiti di coordinamento e sperimentazione nel
campo dell'edilizia a livello nazionale. In seno ad esso Paul Frank
sostiene che un "moderno studio di architettura diviene
obbligatoriamente, in certo modo, una grande impresa"; tuttavia lo
stesso "Ente per la ricerca del Reich" non riesce a trasformarsi in un
organismo esplicitamente impegnato in una gestione diretta del
rinnovamento strutturale del ciclo edilizio. La proposta avanzata nel
'29 da Ludwig Hilberseimer è di limitare l'attività del
*Reichsforschungsgesellschaft* ad un puro coordinamento dei settori di
ricerca sperimentale nel campo tecnologico e degli standards
residenziali: "non grandi complessi, come quelli progettati per
Haselhorst-Spandau, ma complessi sperimentali [come il Weissenhof] di
Stoccarda" (L. Hilberseimer, *Stadtebau und Wohnungsbau auf der
technischen Tagung der Reichsforschungsgesellschaft*, "Die Form" 11
1929. Ora in Die Form cit., p. 144). -->

> Poleg DEWOG in GEHAG je v Weimarski republiki delovalo tudi
> *Reichsforschungsgesellschaft,* organ, ki se je precej razlikoval od
> Deutscher Werkbund, z nalogo usklajevanja in eksperimentiranja na
> področju gradbeništva na nacionalni ravni. Paul Frank je trdil, da v
> njem "moderni arhitekturni biro v določenem smislu postane veliko
> podjetje"; vendar se Reichsforschungsgesellschaft ni razvila v organ,
> ki bi se izrecno ukvarjal z neposrednim upravljanjem strukturne
> prenove gradbenega cikla. Predlog Ludwiga Hilberseimerja iz leta '29
> je bil omejiti dejavnosti *Reichsforschungsgesellschaft* zgolj na
> usklajevanje eksperimentalnih raziskovalnih sektorjev na področju
> tehnologije in stanovanjskih standardov: "ne velikih kompleksov, kot
> so bili načrtovani za Haselhorst-Spandau, temveč eksperimentalnih
> kompleksov [kot je Weissenhof] v Stuttgartu" (L. Hilberseimer,
> *Stadtebau und Wohnungsbau auf der technischen Tagung der
> Reichsforschungsgesellschaft,* "Die Form" 11 1929. Sedaj v Die Form
> cit., str. 144).

<!-- Distinzione di compiti, quindi, fra progettazione sperimentale e
produzione di massa; con mutue e verificabili interazioni. Il
*Reichsforschungsgesellschaft* finanzia in effetti alcuni piccoli
complessi residenziali come quello di Térten Dessau, realizzato a più
riprese da Walter Gropius e Hannes Meyer. Ma la sua azione, limitata e
indecisa, non risponde all'obiettivo più urgente posto dalle esigenze di
un'integrazione del ciclo edilizio nel ciclo economico complessivo, che
richiedeva un'organizzazione statale e pianificata del settore. È
esattamente l'esigenza sentita nel 1928 da Walter Dexel, che, scrivendo
sulla crisi del Bauhaus in relazione alle dimissioni di Gropius da
direttore, preconizza, sulla "Frankfurter Zeitung" (17 marzo 1928, n.
209, ora in H. M. Wingler, *The Bauhaus*, Cambridge (Mass.), MIT Press,
1969, pp. 136--137), una nazionalizzazione delle industrie collegate al
settore edile e la programmazione di una serie di *Bauhäuser*
direttamente collegati, come laboratori di sperimentazione finalmente
concreta, a tale ristrutturazione pianificata e centralizzata in mano al
capitale di Stato. -->

> Razlikovanje nalog med eksperimentalnim oblikovanjem in množično
> proizvodnjo z vzajemnimi in preverljivimi interakcijami.
> *Reichsforschungsgesellschaft* dejansko financira nekaj manjših
> stanovanjskih kompleksov, kot je tisti v Tertenu Dessau, ki sta ga
> večkrat izvedla Walter Gropius in Hannes Meyer. Vendar pa se njeno
> omejeno in neodločno delovanje ni odzvalo na nujnejši cilj vključitve
> gradbenega cikla v splošni gospodarski cikel, ki je zahteval državno
> vodeno in načrtovano organizacijo tega sektorja. Prav to potrebo je
> leta 1928 začutil Walter Dexel, ki je v časopisu Frankfurter Zeitung
> (17. marec 1928, št. 209, zdaj v H. M. Wingler, *The Bauhaus,*
> Cambridge (Mass.), MIT Press, 1969, str. 136-137), nacionalizacijo
> panog, povezanih z gradbeništvom, in načrtovanje vrste *Bauhausov, ki*
> so kot laboratoriji za končno konkretno eksperimentiranje neposredno
> povezani s tem načrtnim in centraliziranim prestrukturiranjem v rokah
> državnega kapitala.

<!-- Ma negli anni '28--'30 la crisi del Bauhaus e la nascita delle
nuove esigenze di una nazionalizzazione dell'industria vanno misurate
sulla base dei risultati, già ampiamente verificabili, della gestione
cooperativistica dell'edilizia e della gestione urbanistica
socialdemocratica. -->

> Toda v 28. in 30. letih prejšnjega stoletja je treba krizo Bauhausa in
> pojav novih zahtev po nacionalizaciji industrije primerjati z že
> splošno preverljivimi rezultati zadružnega upravljanja stavb in
> socialdemokratskega načrtovanja mest.

<!-- Le Siedlungen realizzate fra il '24 e il '33 da Otto Haesler a
Celle e a Rathenow, quelle di Bruno Taut, di Martin Wagner o di Fred
Forbat progettate per la GEHAG a Berlino, quelle di Rigpahne Grod a
Colonia, quelle di Karl Schneider ad Amburgo, quelle di Ernst May, di
Mart Stam e di Schwagenscheidt a Francoforte, valgono -- infatti assai
più come "utopie realizzate" che come interventi qualificanti la nuova
dimensione economica delle città e dei territori metropolitani in
evoluzione. -->

> Siedlungen, ki jih je med letoma 1924 in 1933 zgradil Otto Haesler v
> Cellu in Rathenowu, Siedlungen Bruna Tauta, Martina Wagnerja ali Freda
> Forbata za GEHAG v Berlinu, Siedlungen Rigpahne Grod v Kölnu,
> Siedlungen Karla Schneiderja v Hamburgu, Siedlungen Ernsta Maya, Marta
> Stama in Schwagenscheidta v Frankfurtu so dejansko bolj vredni kot
> "uresničene utopije" kot posegi, ki opredeljujejo novo gospodarsko
> razsežnost mest in metropolitanskih območij v razvoju.

<!-- Localizzate secondo una tecnica di acquisizione dei terreni
largamente condizionata dalla pesante eredità della città speculativa
dell''800, indipendenti dalla localizzazione di centri produttivi già
tendenti a scegliere i propri insediamenti secondo criteri di economia
spaziale, indipendenti anche come unità produttive in sé dal ciclo
industriale complessivo, le Siedlungen "razionaliste" assumono un
patetico ruolo eminentemente ideologico. -->

> Racionalistični Siedlungen, locirani v skladu s tehniko pridobivanja
> zemljišč, ki je v veliki meri pogojena s težko dediščino
> špekulativnega mesta 19. stoletja, neodvisni od lokacije proizvodnih
> centrov, ki so že tako ali tako izbirali svoja naselja v skladu z
> merili prostorske ekonomije, in celo kot proizvodne enote sami po sebi
> neodvisni od celotnega industrijskega cikla, imajo patetično izrazito
> ideološko vlogo.

<!-- Le elementari organizzazioni delle cellule, accuratamente studiate
dal punto di vista dell'Existenz-minimum, costituiscono, certo, il
risultato estremo delle avanguardie artistiche radicali ed
elementariste. La "forma vuota" di Taut, di Haesler o di May vale come
esibizione della pura oggettività ideologica che caratterizza la "città
operaia" come *città etica*, città dell'igiene fisica e sociale, città,
principalmente, della *pace sociale*. La razionalità da loro evocata non
è quella delle leggi immanenti al ciclo capitalistico che investe ormai
in forma nuova i territori produttivi, ma quella del *lavoro liberato*.
La Siedlung operaia, costruita e gestita con capitali sindacali e
"sociali", diviene quindi dimostrazione dello scarto che esiste fra tali
"isole di razionalità" e il territorio capitalista. Essa deve e *vuole*
garantire la propria distanza dai grandi centri terziari come dagli
insediamenti industriali, mantenendo la propria "purezza" formale come
accusa nei confronti del "negativo" urbano: della Grossstadt e del
territorio produttivo; la Siedlung; operaia e cooperativa come immagine
della *città del lavoro*, dunque. È questa la dimensione "etica", tutta
ancora radicata nelle mistificazioni engelsiane e bernsteiniane, nel
mito del proletariato come alfiere di un "mondo nuovo" e di un
socialismo fondato su una società di *produttori coscienti*, che
nasconde -- e proprio con strumenti etici -- le clamorose sconfitte che
la classe operaia tedesca andava subendo sui fronti reali delle proprie
lotte. -->

> Elementarne celične organizacije, skrbno preučene z vidika
> eksistence-minimuma, so resda skrajni rezultat radikalne,
> elementaristične umetniške avantgarde. Prazna forma" Tauta, Haeslerja
> ali Maya služi kot prikaz čiste ideološke objektivnosti, ki označuje
> "mesto delavskega razreda" kot *etično mesto,* mesto fizične in
> socialne higiene, predvsem pa mesto *socialnega miru*. Racionalnost,
> ki jo prikličejo, ni racionalnost zakonov, imanentnih kapitalističnemu
> ciklu, ki zdaj v novi obliki investira v proizvodna območja, temveč
> racionalnost *osvobojenega dela*. Delavski Siedlung, zgrajen in
> upravljan s sindikalnim in "socialnim" kapitalom, tako postane
> demonstracija vrzeli, ki obstaja med temi "otoki racionalnosti" in
> kapitalističnim ozemljem. Zagotavljati mora in *želi* svojo
> oddaljenost od velikih terciarnih središč in tudi od industrijskih
> naselij, pri čemer ohranja svojo formalno "čistost" kot obtožbo proti
> urbani "negativnosti": Grossstadt in proizvodno ozemlje; delavski in
> zadružni Siedlung kot podoba *mesta dela* torej. Prav ta "etična"
> razsežnost, ki je vsa še vedno zakoreninjena v engelsovskih in
> bernsteinovskih mistifikacijah, v mitu o proletariatu kot praporščaku
> "novega sveta" in socializma, ki temelji na družbi *zavestnih
> proizvajalcev,* zakriva - in to prav z etičnimi instrumenti - odmevne
> poraze, ki jih je nemški delavski razred doživljal na dejanskih
> frontah svojih bojev.

<!-- La socializzazione dei terreni e dell'edilizia, per la quale si
erano battuti nel movimento per i consigli Alexander Gel'fand (Parvus) e
Martin Wagner nel 1918 e nel 1919, fallita sul piano politico, si
realizza così come "immagine" di una possibile *alternativa* alla città
capitalistica nel suo complesso. -->

> Socializacija zemljišč in gradnje, za katero sta se Alexander Gel'fand
> (Parvus) in Martin Wagner borila v svetniškem gibanju v letih 1918 in
> 1919 in ki je politično propadla, se tako uresničuje kot "podoba"
> možne *alternative* kapitalističnemu mestu kot celoti.

<!-- Ed è proprio tale *utopismo* etico che deve far leggere
l'esperienza delle Siedlungen "razionaliste" assai più connessa con i
movimenti populisti libertari delle avanguardie espressioniste -- si
pensi, solo come ad alcuni fra i tanti esempi, alla lirica urbana di
Georg Heym, all'"attivismo" dei gruppi di Kurt Hiller o di Ludwig
Rubiner, all'Arbeitsrat für Kunst o ai gruppi Dada di Berlino -- che al
contraddittorio "distacco" della *Neue Sachlichkeit*. -->

> In prav zaradi tega etičnega *utopizma* moramo izkušnje
> "racionalističnih" Siedlungen brati veliko bolj v povezavi z
> libertarnimi populističnimi gibanji ekspresionističnih avantgard -
> pomislimo samo na nekaj primerov mestne lirike Georga Heyma,
> "aktivizem" skupin Kurta Hillerja ali Ludwiga Rubinerja, Arbeitsrat
> für Kunst ali skupine berlinske dade - kot na protislovno "distanco"
> *Neue Sachlichkeit.*

<!-- Se questo è il terreno ideologico su cui si fonda il dibattito
architettonico della cultura radicale tedesca, è necessario
oltrepassarne i limiti più appariscenti per verificarne i reali
contenuti economici, al livello imposto dalla concreta situazione dello
sviluppo capitalistico all'interno della repubblica di Weimar. -->

> Če je to ideološki teren, na katerem temelji arhitekturna razprava o
> radikalni nemški kulturi, je treba preseči njene vidnejše meje in
> preveriti njeno resnično ekonomsko vsebino na ravni, ki jo je
> narekoval konkreten položaj kapitalističnega razvoja v Weimarski
> republiki.

<!-- È veramente incredibile che anche i più recenti critici
dell'urbanistica tedesca fra il '20 e il '30 non abbiano individuato
l'origine storica della politica residenziale concretata a Berlino e
Francoforte. Il tema della riforma urbanistica, in vista di una
soluzione umanitaria del problema gravissimo delle abitazioni operaie a
Berlino, viene impostato subito dopo le agitazioni del 1847 dal
conservatore Huber in collaborazione con l'architetto C. A. Hoffmann.
Sono esattamente Huber e Hoffmann a fondare la prima società edilizia
berlinese di pubblica utilità, con il compito di entrare in concorrenza
con la decaduta industria costruttiva; è Huber a combattere, dalla fine
degli anni '40 in poi la speculazione sui terreni di Berlino; è lo
stesso Huber a proporre un modello di insediamenti residenziali tipo
Siedlungen, disposti intorno alle grandi *città del lavoro* secondo
schemi radiali, la cui distanza dai centri produttivi possa essere
coperta "con moderni mezzi di locomozione entro un quarto d'ora" (cfr.
W. Hegemann, *Das steinerne Berlin*, Berlin 1930, 1963^2). La risposta
in senso riformatrice alle lotte operaie viene, alla metà dell''800, da
parte di conservatori "illuminati", dunque. Le loro iniziative non
trovano rispondenza immediata a causa delle resistenze della miope
burocrazia prussiana e dello stesso partito conservatore; ma gli stessi
strumenti da loro individuati -- eliminazione della rendita fondiaria,
iniziativa cooperativistica di consumo e produzione in concorrenza con
la iniziativa privata e basata su associazioni di pubblica utilità con
incentivi statali e comunali, modello urbanistico fondato sulle
Siedlungen -- si ritroveranno, non a caso, fra i caposaldi teorici della
politica socialdemocratica relativa alle città. -->

> Prav neverjetno je, da tudi najnovejšim kritikom nemškega urbanizma
> med dvajsetimi in tridesetimi leti prejšnjega stoletja ni uspelo
> ugotoviti zgodovinskega izvora stanovanjske politike, ki se je
> konkretizirala v Berlinu in Frankfurtu. Temo urbanistične reforme z
> namenom humanitarne rešitve izjemno resnega problema delavskih
> stanovanj v Berlinu je takoj po nemirih leta 1847 zastavil
> konservativec Huber v sodelovanju z arhitektom C. A. Hoffmannom. Prav
> Huber in Hoffmann sta ustanovila prvo berlinsko javno gradbeno
> društvo, katerega naloga je bila konkurirati dekadentni gradbeni
> industriji; Huber je bil tisti, ki se je od konca štiridesetih let 19.
> stoletja dalje boril proti špekulaciji z berlinskimi zemljišči; Huber
> sam je predlagal model stanovanjskih naselij tipa Siedlungen,
> razporejenih okoli velikih *delovnih mest po* radialnih vzorcih,
> katerih oddaljenost od proizvodnih središč bi lahko "s sodobnimi
> prevoznimi sredstvi premagali v četrt ure" (prim. W. Hegemann, *Das
> steinerne Berlin,* Berlin 1930, 1963^2). Reformistični odziv na
> delavske boje so sredi 19. stoletja prinesli "razsvetljeni"
> konservativci. Njihove pobude zaradi odpora kratkovidne pruske
> birokracije in same konservativne stranke niso našle takojšnjega
> odziva, vendar pa je bilo mogoče iste instrumente, ki so jih
> opredelili - odpravo zemljiške rente, zadružno pobudo za potrošnjo in
> proizvodnjo, ki bi konkurirala zasebni pobudi in bi temeljila na
> združenjih javnih služb z državnimi in občinskimi spodbudami, model
> urbanističnega načrtovanja na podlagi Siedlungen - ne po naključju
> najti med teoretičnimi temelji socialdemokratske politike do mest.

<!-- Può sembrare azzardato affermare che la tradizione della politica
urbanistica dell'ADGB si fondi su soluzioni elaborate in pieno '800
dalle avanguardie del partito conservatore. Eppure è questo un dato su
cui sarebbe opportuno riflettere anche in considerazione del peso che
quella tradizione esercita ancora oggi, all'interno dei partiti della
classe operaia. -->

> Morda se zdi pretirano trditi, da tradicija mestne politike ADGB
> temelji na rešitvah, ki so jih sredi 19. stoletja razvili voditelji
> konservativne stranke. Vendar je to dejstvo, o katerem bi bilo treba
> razmišljati tudi zaradi teže, ki jo ima tradicija še danes v strankah
> delavskega razreda.

<!-- Sta comunque di fatto che, al di là del forte richiamo
propagandistico esercitato a livello europeo dall'ascetico rigore dei
quartieri di May, Taut, Gropius o Haesler, e al di là delle loro
specifiche qualità architettoniche, l'impostazione socialdemocratica del
tema urbanistico risulta, all'inizio degli anni '30, altamente carente
agli stessi occhi dei tecnici e degli architetti che ad essa avevano
dato forma. -->

> Dejstvo pa je, da je bil socialdemokratski pristop k urbanističnemu
> načrtovanju v začetku tridesetih let prejšnjega stoletja v očeh
> tehnikov in arhitektov, ki so ga oblikovali, poleg močne propagandne
> privlačnosti, ki jo je na evropski ravni imela asketska strogost
> Mayevih, Tautovih, Gropiusovih ali Haeslerjevih sosesk, in poleg
> njihovih posebnih arhitekturnih kakovosti zelo pomanjkljiv.

<!-- È su questo terreno che gli intellettuali socialdemocratici più
lontani dall'utopia individuano come unica via di uscita da una
situazione fallimentare una pianificazione globale e intersettoriale,
capace di investire l'intero sistema economico nazionale. Il fallimento
della politica comunale del SPD a Berlino viene clamorosamente
denunciato da Martin Wagner, fino al 1931 Stadtbaurat socialdemocratico
imposto dai gruppi di pressione di avanguardia alla testa della gestione
urbanistica berlinese. -->

> Na tem področju so socialdemokratski intelektualci, ki so bili najbolj
> oddaljeni od utopije, kot edini izhod iz neuspešnega položaja
> prepoznali celovito medsektorsko načrtovanje, ki lahko investira v
> celoten nacionalni gospodarski sistem. Neuspeh občinske politike SPD v
> Berlinu je glasno obsodil Martin Wagner, ki je bil do leta 1931
> socialdemokratski Stadtbaurat, ki so ga na čelo berlinske mestne
> uprave postavile avantgardne skupine pritiska.

<!-- È proprio Wagner a individuare la fine della "città del lavoro"
ottocentesca provocata dai processi di razionalizzazione dell'industria
tedesca, dalla struttura della nuova composizione organica del capitale,
dalla riduzione del lavoro operaio a lavoro astratto puro, dalla
terziarizzazione delle città, dalla formazione di insediamenti
produttivi decentrati e dimensionati a una scala extra-urbana (M.
Wagner, *Sterbende Stadte? Oder Planwirtschaftlicher Stadtebau?*, "Die
Neue Stadt", luglio 1932, pp. 50--59). Ma su tali basi l'intera gestione
delle città socialdemocratiche è posta sotto accusa. Non è più il tema
della residenza per l'*uomo nuovo*, per il *produttore cosciente* che
"deve" riconoscersi nelle strutture architettoniche di avanguardia, ad
assumere ora la prevalenza, ma il calcolo economico della produttività
della città terziaria, l'ottimizzazione delJa rete dei trasporti in
relazione all'integrazione dei centri produttivi e alla pendolarità
della forza lavoro, la pianificazione globale e centralizzata. Il
calcolo economico della produttività della città: questa è l'esigenza
scoperta da Wagner, in serrata polemica contro la politica di cieca
spesa del SPD. In tal senso sono del massimo interesse i tre articoli
che lo stesso Wagner scrive nel 1931 per giustificare la sua uscita dal
partito socialdemocratico (M. Wagner, *Mein Austritt aus der SPD*, "Das
Tagebuch" 15 1931, pp. 568--570; 16, pp. 611--617). -->

> Prav Wagner je tisti, ki konec 19. stoletja opredeli "delavsko mesto",
> ki so ga povzročili procesi racionalizacije nemške industrije,
> struktura nove organske sestave kapitala, redukcija delavskega dela na
> čisto abstraktno delo, terciarizacija mest, oblikovanje
> decentraliziranih proizvodnih naselij v zunajmestnem merilu (M.
> Wagner, *Sterbende Stadte? Oder Planwirtschaftlicher Stadtebau?*, Die
> Neue Stadt, julij 1932, str. 50-59). Vendar je na tej podlagi obtoženo
> celotno upravljanje socialdemokratskih mest. Zdaj ne prevladuje več
> tema bivališča za *novega človeka,* za *zavednega proizvajalca,* ki se
> "mora" prepoznati v avantgardnih arhitekturnih strukturah, temveč
> ekonomski izračun produktivnosti terciarnega mesta, optimizacija
> prometnega omrežja glede na povezovanje proizvodnih centrov in vožnjo
> delovne sile na delo, globalno in centralizirano načrtovanje.
> Ekonomski izračun produktivnosti mesta: to je potreba, ki jo je odkril
> Wagner v tesni polemiki proti politiki slepe porabe SPD. V tem smislu
> so nadvse zanimivi trije članki, ki jih je Wagner sam napisal leta
> 1931, da bi upravičil svoj izstop iz socialdemokratske stranke (M.
> Wagner, *Mein Austritt aus der SPD,* "Das Tagebuch" 15 1931, str.
> 568-570; 16, str. 611-617).

<!-- In questi articoli, finora ignorati da tutti gli storici
dell'urbanistica tedesca fra le due guerre, l'accusa al SPD non è
politica, ma tutta basata sulla sua incosciente gestione economica.
Wagner giunge infatti a verificare in essi, cifre alla mano, come lo
stesso piano di esprorio dei terreni da parte del Comune di Berlino sia
avvenuto al di fuori di qualsiasi calcolo produttivo di localizzazione e
nella più completa anarchia. La "libera" disposizione delle Siedlungen,
tanto esaltata da alcuni critici attuali come esempio di "città per
parti" -- ma non a caso Ernst May e Hans Schmidt scriveranno che la
*città per parti* è la "città capitalistica", mentre quella compatta è
la "città socialista", mistificando a loro volta i termini del problema
-- si rivela così frutto di una politica assurda di acquisti, da cui è
escluso ogni ragionamento sul costo dei trasorti e sulla relazione
residenza-città terziaria-decentramento produttivo. "Dove approderà
necessariamente questo comportamento -- scrtive Wagner -- che vede il
partito più importante del Comune di Berlino tollerare per l'acquisto
dei terreni un sistema di inganno degli altri organi decisionali, si può
illustrare con l'esempio dell'acquisto del terreno di Asching
sull'Alexanderplatz. Tale terreno, che ha un valore tassabile di
2.725.000 DM è stato pagato, con l'aiuto del deputato regionale
Heilmann, dei consiglieri comunali Loewey e Zangemeister e con l'accordo
dei consiglieri Hahr e Reiter, 13,5 milioni di DM, cioè ad un prezzo di
6.000 DM al metro quadro. Il prezzo di acquisto, di oltre dieci milioni
di marchi più alto del valore tassabile del terreno, ha portato ad un
costo di affitto delle costruzioni 92 volte più alto di quello
dell'anteguerra" (M. Wagner, op. cit., II, p. 613). -->

> V teh člankih, ki so jih doslej zanemarjali vsi zgodovinarji nemškega
> urbanizma med obema vojnama, obtožbe proti SPD niso politične, temveč
> v celoti temeljijo na njenem nesprejemljivem gospodarskem upravljanju.
> Wagner gre namreč tako daleč, da v njih s številkami v roki preveri,
> kako je sam načrt razlastitve zemljišč berlinskega mestnega sveta
> potekal zunaj kakršne koli produktivne lokacijske kalkulacije in v
> popolni anarhiji. "Prosto" razpolaganje s Siedlungen, ki ga nekateri
> sedanji kritiki tako hvalijo kot primer "mesta po delih" - vendar ni
> naključje, da sta Ernst May in Hans Schmidt zapisala, da je *mesto po
> delih* "kapitalistično mesto", medtem ko je kompaktno mesto
> "socialistično mesto", s čimer sta po drugi strani mistificirala
> pogoje problema -, se tako izkaže za rezultat absurdne nakupne
> politike, iz katere je izključeno kakršno koli sklepanje o stroških
> prevoza in razmerju med prebivališčem-terciarnim
> mestom-decentralizacijo proizvodnje. "Kam takšno ravnanje nujno
> pripelje," piše Wagner, "ko najpomembnejša stranka v berlinski občini
> dopušča sistem zavajanja drugih organov odločanja pri nakupu zemljišč,
> lahko ponazorimo s primerom nakupa zemljišča Asching na
> Alexanderplatzu. Za to zemljišče z davčno vrednostjo 2.725.000 nemških
> mark je bilo s pomočjo deželnega poslanca Heilmanna, mestnih svetnikov
> Loeweya in Zangemeistra ter s soglasjem svetnikov Hahra in Reiterja
> plačanih 13,5 milijona nemških mark, tj. po ceni 6.000 nemških mark za
> kvadratni meter. Kupnina, ki je bila za več kot deset milijonov
> nemških mark višja od davčne vrednosti zemljišča, je pomenila 92-krat
> višji strošek najema stavbe kot pred vojno" (M. Wagner, op. cit., II,
> str. 613).

<!-- Gli esempi citati da Wagner proseguono in una catena continua. Ne
emerge chiaro il quadro di un conflitto fra la politica sociale posta al
centro dei programmi di "democrazia economica" dell'ADGB e la gestione
concreta di quei programmi: uno scarto significativo fra ideologia e
realtà economica, significativamente rimasta coperta fino ad oggi. -->

> Primeri, ki jih navaja Wagner, se nadaljujejo v neprekinjeni verigi.
> Jasno se izriše slika konflikta med socialno politiko, ki je bila v
> središču programov ADGB za "ekonomsko demokracijo", in konkretnim
> upravljanjem teh programov: velik razkorak med ideologijo in
> gospodarsko realnostjo, ki je vse do danes ostal precej zakrit.

<!-- "Va tenuto presente -- continua Martin Wagner -- cosa significa non
poter finanziare a lungo termine l'acquisto di terreni per 300 milioni
di marchi e la costruzione di mezzi di comunicazione per 350 milioni di
marchi, cosa significa essere messi continuamente alle strette da debiti
a breve scadenza per 650 milioni di marchi, e dover ricavatre queste
somme non dalla redditività dei capitali impiegati ma da mezzi ricavati
da tasse [...] Una simile politica comunale non mette in pericolo solo
il reddito delle nostre centrali elettriche in sé produttive, ma anche
il bilancio delle uscite per lavori pubblici di importanza vitale" M.
Wagner, *Mein Austritt* cit., III, p. 615). L'investimento di capitale
per la riorganizzazione della rete metropolitana, al di fuori di un
piano intersettoriale di investimenti, è quindi legato da Wagner al
sistema caotico di localizzazione residenziale, e tutte le
responsabilità vengono rovesciate sui dirigenti del SPD: "quando nella
primavera del '27 il gruppo Chapman offre al Comune di Berlino di
costruire su tutta la parte sud del Schöneberg -- egli osserva (ibidem,
pag. 616) -- allora si poteva dimostrare al mondo come si possa fare
urbanistica facendo economia. Il prolungamento della metropolitana,
specie se eseguito in trincea, avrebbe creato in un quartiere nuovo di
50.000 abitanti un aumento del traffico con pochi investimenti da parte
del comune. Il traffico, la rete elettrica, i costi dell'Amministrazione
comunale sarebbero stati molto più redditizi in una organizzazione
compatta della città". Le 110.000 abitazioni, create dal 1918 al 1930
con finanziamenti comunali, vengono così valutate come costi
improduttivi, all'insegna di uno spreco incapace, per di più, di
stimolare il livello degli investimenti. -->

> "Ne smemo pozabiti," nadaljuje Martin Wagner, "kaj pomeni, če ne
> moremo financirati dolgoročnega nakupa zemljišč v višini 300 milijonov
> nemških mark in gradnje komunikacijske opreme v višini 350 milijonov
> nemških mark, kaj pomeni, če smo nenehno obremenjeni s kratkoročnimi
> dolgovi v višini 650 milijonov nemških mark in če teh zneskov ne
> dobimo iz donosnosti uporabljenega kapitala, temveč iz davčnih virov
> [...] Taka občinska politika ne ogroža samo prihodkov naših
> proizvodnih elektrarn, temveč tudi proračunske izdatke za nujna javna
> dela. Wagner, *Mein Austritt* cit, III, str. 615). Kapitalsko naložbo
> za reorganizacijo podzemnega omrežja zunaj medsektorskega naložbenega
> načrta Wagner tako poveže s kaotičnim sistemom umeščanja stanovanj v
> prostor, vso odgovornost pa preloži na vodstvo SPD: "ko je spomladi
> leta '27 Chapmanova skupina berlinskemu mestnemu svetu ponudila
> pozidavo celotnega južnega dela Schöneberga," ugotavlja (ibid., str.
> 616), "potem bi lahko svetu pokazali, kako se lahko urbanistično
> načrtovanje izvaja z varčevanjem. Razširitev podzemne železnice,
> zlasti če bi bila izvedena v jarkih, bi v novem okrožju s 50.000
> prebivalci ustvarila povečan promet z majhno investicijo občine.
> Promet, električno omrežje in stroški občinske uprave bi bili veliko
> bolj donosni pri kompaktni organizaciji mesta. 110.000 stanovanj,
> nastalih v letih 1918-1930 z občinskimi sredstvi, tako ocenjujemo kot
> neproduktivne stroške, kot potratnost, ki poleg tega ne more
> spodbuditi ravni naložb.

<!-- È significativo che Wagner non attacchi mai i principi della
"democrazia economica", ma sempre e solo i metodi della sua
realizzazione. L'improduttività della spesa pubblica non è per lui
connaturata agli obiettivi specifici della socialdemocrazia, ma a
distorsioni evitabili, a inefficienze soggettive. Diviene quindi per lui
un passaggio quasi obbligato saltare a piè pari l'analisi reale della
funzione della città o degli stessi conflitti di classe nel quadro dello
sviluppo capitalistico, per affrontare direttamente, sul piano
disciplinare dell'analisi urbana, il tema della globalità della
pianificazione. -->

> Pomembno je, da Wagner nikoli ne napada načel "ekonomske demokracije",
> temveč le metode njenega uresničevanja. Po njegovem mnenju
> neproduktivnost javne porabe ni neločljivo povezana s posebnimi cilji
> socialne demokracije, temveč s popačenji, ki se jim je mogoče
> izogniti, s subjektivno neučinkovitostjo. Zato postane zanj skoraj
> obvezen korak, da preskoči dejansko analizo funkcije mesta ali samih
> razrednih konfliktov v okviru kapitalističnega razvoja, da bi se na
> disciplinarni ravni urbane analize neposredno lotil teme globalnosti
> načrtovanja.

<!-- Parlare quindi per la Berlino gestita dalla socialdemocrazia e
realizzata dagli architetti radicali fra il '20 e il '30 di "città
aperta", come fa Aymonino, in cui alla fluidità delle localizzazioni
residenziali unitarie costituite dalle Siedlungen operaie corrisponda
una riunificazione dell'organismo generale sulla base di una complessa
struttura metropolitana di trasporti, è non solo una lettura ideologica
di una ben diversa realtà economica e urbana, ma è anche
storiograficamente scorretto. -->

> Govoriti za Berlin, ki ga je upravljala socialna demokracija in so ga
> uresničili radikalni arhitekti med dvajsetimi in tridesetimi leti 20.
> stoletja, o "odprtem mestu", kot to počne Aymonino, v katerem
> fluidnost enotnih stanovanjskih lokacij, ki so jih predstavljali
> delavski Siedlungen, ustreza združitvi splošnega organizma na podlagi
> kompleksne metropolitanske prometne strukture, ni le ideološko branje
> zelo različne gospodarske in urbane stvarnosti, temveč je tudi
> zgodovinopisno napačno.

<!-- La politica di localizzazione delle Siedlungen, a Berlino ancor più
che a Francoforte, è parte di una gestione deficitaria e antieconomica
della città, sia per le oggettive incapacità dei funzionari del SPD di
porsi ad un livello capitalisticamente corretto -- almeno nel settore
limitato del loro intervento -- sia per la stessa settorialità con cui
il tema della residenza viene isolato dal tema della città come
organismo produttivo. (In tal senso nella politica urbanistica
socialdemocratica istanza anticapitalistica significa, paradossalmente,
tecnica fallimentare di gestione economica). -->

> Lokacijska politika Siedlungen, v Berlinu še bolj kot v Frankfurtu, je
> del pomanjkljivega in negospodarnega upravljanja mesta, tako zaradi
> objektivne nezmožnosti uradnikov SPD, da bi se postavili na
> kapitalistično pravilno raven - vsaj v omejenem sektorju svojega
> delovanja -, kot zaradi same sektorskosti, s katero je tema bivanja
> ločena od teme mesta kot proizvodnega organizma. (V tem smislu v
> socialdemokratski urbanistični politiki antikapitalistična instanca
> paradoksalno pomeni neuspešno tehniko gospodarskega upravljanja).

<!-- La gestione urbanistica dei comuni socialdemocratici tedeschi
sembra quindi verificare drammaticamente nella realtà quanto nel 1907
Max Weber aveva profetizzato intervenendo nella discussione "sulla
costituzione e organizzazione delle amministrazioni municipali",
avvenuta a Magdeburgo. Sostenendo l'impossibilità, per il socialismo
rivoluzionario di amministrare razionalmente il potere (leggi: secondo
una logica capitalistica complessiva}), senza rovinare le
amministrazioni stesse O senza mutare le proprie connotazioni di classe,
Weber scriveva: "niente si ritorcerebbe più pesantemente anche su di
noi, del tentativo di voler costruire sulla base dell'attuale nostro
ordine economico e sociale la futura politica socialista; i primi ad
abbandonare il Partito [...] sarebbero i suoi stessi partigiani, gli
operai" (Max Weber, *Gesammelte Aufsitze zur Soziologie und
Sozialpolitik*, Tübingen 1924, p. 411). Alle fumosità economiche del
*Linkskommunismus* e al dilettantismo della prassi socialdemocratica, la
lucida settarietà di parte capitalista di Weber sembra addirittura
ricordare i compiti specifici della lotta di classe *dentro e contro* lo
sviluppo dei rapporti di produzione. -->

> Tako se zdi, da mestna uprava nemških socialdemokratskih občin v
> resnici dramatično potrjuje tisto, kar je leta 1907 napovedal Max
> Weber, ko je v Magdeburgu govoril v razpravi "o ustanavljanju in
> organizaciji občinskih uprav". V utemeljitvi, da je za revolucionarni
> socializem nemogoče racionalno upravljati oblast (beri: v skladu s
> celovito kapitalistično logiko), ne da bi pri tem uničil same uprave
> ali spremenil njihove razredne konotacije, je Weber zapisal: "Nič se
> ne bi niti nam tako hudo maščevalo kot poskus, da bi hoteli graditi
> prihodnjo socialistično politiko na podlagi našega sedanjega
> gospodarskega in družbenega reda; prvi, ki bi partijo zapustili
> [...], bi bili njeni lastni partizani, delavci" (Max Weber,
> *Gesammelte Aufsitze zur Soziologie und Sozialpolitik*, Tübingen 1924,
> str. 411). Zdi se, da nas Webrovo lucidno sektaštvo na kapitalistični
> strani ob ekonomski *zadimljenosti Linkskommunismus* in amaterizmu
> socialdemokratske prakse celo opominja na specifične naloge razrednega
> boja *znotraj in proti* razvoju produkcijskih odnosov.

<!-- Esiste quindi un'enorme distanza fra la prassi urbanistica dei
comuni socialdemocratici e la lucidità con cui un teorico di modelli
urbani come Ludwig Hilberseimer riprende le tesi di Georg Simmel
(formulate nel 1903), sul significato della metropoli moderna. "La
grande città -- scrive Hilberseimer nel suo *Grossstadtarchitektur* del
1927 -- appare in primo luogo come creazione dell'onnipotente grande
capitale, improntato alla sua anonimità, ed inoltre come tipo di città
con proprie basi economico-sociali e psico-collettive, in cui si assiste
contemporaneamente al massimo isolamento e alla più fitta agglomerazione
degli abitanti. In essa un ritmo di vita enormemente intensificato
reprime ben presto ogni elemento locale e individuale". È difficile non
leggere, in tali osservazioni di Hilberseimer, la dialettica simmeliana
fra l'"intensificazione metropolitana della vita nervosa"
(*Nervenleben*) e il livello superiore di "conoscenza" da quest'ultima
provocata (*Verstand*). La disorganizzazione della città capitalistica è
quindi per Hilberseimer legata ad un primo momento di formazione ,
necessario solo in quanto fase di transizione, destinato ad un superiore
"conoscere" collettivo che è, in primo luogo, *piano globale* di
produttività urbana e territoriale, integrato in un piano di economia
nazionale e internazionale. Non solo, quindi "la progettazione della
città si amplia divenendo progettazione su scala nazionale [così che]
dalla edificazione delle città si giunge alla edificazione del Paese"
(L. Hilberseimer, Grossstadtarch., p. 20), ma il processo duplice e
integrato di concentrazione terziaria e decentramento industriale dovrà
sottostare a leggi -- ancora enunciate nel quadro di un puro modello
astratto -- di programmazione intersettoriale e di produttività
generale. "Ciò è intimamente legato alla progettazione su scala
nazionale, il cui sviluppo futuro sarà condizionato da quello dei grandi
complessi economici. Dalla fusione degli stati nazionali o
plurinazionali si giungerà alle unioni economiche: per noi soprattutto
la fusione del continente europeo, oggi politicamente lacerato, in una
sola entità economica costituirà la premessa per una politica
urbanistica di avanguardia in senso produttivo, che porterà finalmente a
soluzione il tema della grande città" (*Grossstadtarch*., p*. 21). -->

> Zato je med urbanistično prakso socialdemokratskih občin in jasnostjo,
> s katero teoretik urbanističnih modelov, kot je Ludwig Hilberseimer,
> povzema teze Georga Simmla (oblikovane leta 1903) o pomenu moderne
> metropole, ogromna razdalja. "Velemesto," piše Hilberseimer v svojem
> delu *Grossstadtarchitektur* iz leta 1927, "se v prvi vrsti kaže kot
> stvaritev vsemogočnega velikega kapitala, ki ga zaznamuje anonimnost,
> poleg tega pa kot tip mesta z lastnimi ekonomsko-socialnimi in
> psihološko-kolektivnimi temelji, v katerem sta hkrati največja
> izolacija in najgostejša aglomeracija prebivalcev. V njem izredno
> okrepljen tempo življenja kmalu zatre vsak lokalni in individualni
> element". V takšnih Hilberseimerjevih pripombah je težko ne prebrati
> Simmelijeve dialektike med "metropolitansko intenzifikacijo živčnega
> življenja" (*Nervenleben) in z* njo povezano višjo stopnjo "znanja"
> (*Verstand)*. Dezorganizacija kapitalističnega mesta je tako za
> Hilberseimerja povezana z začetnim trenutkom oblikovanja, ki je
> potreben le kot prehodna faza, namenjena višji kolektivni "vednosti",
> ki je predvsem *globalni načrt* urbane in teritorialne produktivnosti,
> vključen v načrt nacionalne in mednarodne ekonomije. Ne le, da se
> "načrtovanje mesta razširi tako, da postane načrtovanje na nacionalni
> ravni, [tako da] od gradnje mest pridemo do gradnje države" (L.
> Hilberseimer, Grossstadtarch., str. 20), ampak se bo moral dvojni in
> integrirani proces terciarne koncentracije in industrijske
> decentralizacije podrediti zakonom - še vedno izrečenim v okviru
> povsem abstraktnega modela - medsektorskega načrtovanja in splošne
> produktivnosti. "To je tesno povezano z načrtovanjem na nacionalni
> ravni, katerega prihodnji razvoj bo pogojen z razvojem velikih
> gospodarskih kompleksov. Iz združitve nacionalnih ali večnacionalnih
> držav bomo prišli do gospodarskih zvez: za nas bo predvsem združitev
> evropske celine, ki je danes politično razdvojena, v enotno
> gospodarsko enoto, predpostavka za avantgardno urbanistično politiko
> načrtovanja v produktivnem smislu, ki bo končno pripeljala do rešitve
> teme velikega mesta" (*Grossstadtarch*., str. 21).

<!-- Il problema, distaccato dalla prassi immediata e riportato
nell'ambito della modellistica pura, sposta dunque la scala delle
priorità e le dipendenze fra i fattori. La critica -- implicita ma
chiarissima -- al metodo settoriale di gestione economica delle
organizzazioni socialdemocratiche, è in Hilberseimer immediata. Solo
dalla programmazione di modelli sovranazionali di economia
intersettoriale il "molteplice", il "caos capitalistico" della
produzione, di cui la *Grossstadt* è immagine, potrà essere "plasmato",
dominato. (Non a caso nell'ultima pagina del suo volume Hilberseimer
cita, a proposito del caos "costretto" a divenire "forma", Nietzsche:
con buona pace di chi è ancora incapace di leggere correttamente i
legami fra "pensiero negativo" e avanguardie: cfr. L. Benevolo, *Le
Corbusier della parte degli uomini comuni*, "Settegiorni" 203 1971, pp.
24--25). -->

> Problem, ki je ločen od neposredne prakse in vrnjen na področje
> čistega modeliranja, tako spremeni lestvico prednostnih nalog in
> odvisnosti med dejavniki. Kritika - implicitna, vendar zelo jasna -
> sektorske metode gospodarskega upravljanja socialdemokratskih
> organizacij je pri Hilberseimerju neposredna. Le s programiranjem
> nadnacionalnih modelov medsektorske ekonomije je mogoče "mnogoterost",
> "kapitalistični kaos" proizvodnje, katerega podoba je *Grossstadt,*
> "oblikovati", obvladati. (Ni naključje, da Hilberseimer na zadnji
> strani svojega zvezka citira Nietzscheja na temo kaosa, ki je
> "prisiljen" postati "oblika": z dobro milostjo za tiste, ki še vedno
> ne znajo pravilno brati povezav med "negativno mislijo" in avantgardo:
> prim. L. Benevolo, *Le Corbusier della parte degli uomini comuni,*
> "Settegiorni" 203 1971, str. 24-25).

<!-- Ciò che è importante è che fra la lettura tutta teorica, da
laboratorio, di Hilberseimer, e le conclusioni tratte da Martin Wagner
dalla sua esperienza quotidiana di Stadtbaurat si viene ad ottenere una
collimazione di prospettive. Non più un "capitale democratico", ora, ma
un capitale pianificato è posto alla testa di un'*ideologia del piano*
cui vengono assegnati tutti gli attributi di una lotta al capitale
tout-court. -->

> Pomembno je, da med Hilberseimerjevim povsem teoretičnim,
> laboratorijskim branjem in sklepi, ki jih je Martin Wagner potegnil iz
> svojih vsakodnevnih izkušenj mestnega župana, pride do kolimatacije
> perspektiv. Na čelo *ideologije plana, ki* ji pripadajo vsi atributi
> boja proti kapitalu tout-court, ni več postavljen "demokratični
> kapital", temveč načrtovani kapital.

<!-- Il capitale come anarchia della produzione, dunque, e il *piano*
come legge da imporre con la forza al capitale stesso. Siamo ancora,
malgrado tutto, nel cielo della pura ideologia generale. Solo
parzialmente risultano superati i limiti più appariscenti della prassi
bernsteiniana, del SPD, del'ADGB. Di fronte ai salti teorici compiuti
dalle teorie economiche anticicliche e keynesiane, gli intellettuali
della repubblica di Weimar, soggettivamente trasformatisi in tecnici
dell'economia urbana, non possono che scoprire che l'unico paese dove
sembrano attuabili le loro prefigurazioni ideologiche di un capitalismo
pianificato è la Russia sovietica del primo Piano Quinquennale. -->

> Kapital je torej anarhija proizvodnje, *načrt* pa zakon, ki ga je
> treba s silo vsiliti samemu kapitalu. Kljub vsemu smo še vedno v
> nebesih čiste splošne ideologije. Bolj očitne omejitve bernsteinovske
> prakse, SPD, ADGB so le delno presežene. Spričo teoretičnih preskokov,
> ki jih naredijo proticiklične in keynesianske ekonomske teorije, lahko
> intelektualci Weimarske republike, subjektivno spremenjeni v tehnike
> urbane ekonomije, ugotovijo le, da je edina država, kjer se njihove
> ideološke predstave o načrtnem kapitalizmu zdijo izvedljive, sovjetska
> Rusija prvega petletnega načrta.

<!-- L'identificazione di socialismo e piano viene quindi compiutamente
avanzata sulla base della tradizione engelsiana, del dibattito
postleniniano in URSS e del dibattito ideologico degli intellettuali
tedeschi: del resto non è forse Berlino il centro di una programmata
confluenza, sin dai primi anni '20, fra pensiero radicale occidentale e
intellighencija sovietica? (Si pensi al ruolo svolto in tal senso da
Adolf Behne, da Ilya Ehrenburg, da Lisickij e da riviste come *Vesc* o
*Das neue Russland*. Cfr. K. Junghanns, *Beziebhungen* cit., e J.
Elderfielr, *Dissenting Ideologies* cit., specie per le connessioni fra
il clima politico berlinese e le concitate operazioni delle avanguardie
intellettuali). -->

> Identifikacija socializma in plana je tako v celoti izpeljana na
> podlagi engelsovske tradicije, postgermanske razprave v ZSSR in
> ideološke razprave nemških intelektualcev: navsezadnje, ali ni Berlin
> od začetka dvajsetih let 20. stoletja središče načrtnega stičišča med
> zahodno radikalno mislijo in sovjetsko inteligenco? (Pomislimo na
> vlogo, ki so jo pri tem imeli Adolf Behne, Ilja Ehrenburg, Lisickij in
> revije, kot sta *Vesc* ali *Das neue Russland*. Glej K. Junghanns,
> *Beziebhungen cit.* in J. Elderfielr, *Dissenting Ideologies* cit.,
> zlasti za povezave med političnim ozračjem v Berlinu in razburjenim
> delovanjem intelektualne avantgarde).

<!-- "Ironia del destino -- scrive Martin Wagner sul "Tagebuch" del 25
luglio 1931 (n. 30) -- lo stesso giorno in cui più di mille urbanisti,
dopo aver assistito per cinque giorni all'autopsia del cadavere
dell'organismo urbano europeo, nel loro incontro finale concordano sulla
loro impotenza a far qualcosa, l'assessore municipale all'urbanistica
Ernst May tiene la sua grande relazione sull'urbanistica russa, in una
cerchia di giovani architetti entusiasi e di costruttori interessati
[...]. I giovani sentono istitivamente che dalla Russia si sta
sprigionando una nuova vitalità, che là maturano e si avverano nuove
possibilità, che là la gioia creativa dell'urbanistica, liberata da
tutte le remore della proprietà e dell'utile privato, può pienamente
espandersi". E conclude il suo "inno socialdemocratico" al socialismo
realizzato, con una sintomatica regressione "umanistica", che mette in
prima linea i principi etici della pianificazione globale: nella città
sovietica "debbono essere contenuti i momenti più grandi e nobili di un
*Zeitgeist* socialista [...] quale cattedrale del popolo". -->

> "Ironija usode," je zapisal Martin Wagner v "Tagebuchu" 25. julija
> 1931 (št. 30), "prav na dan, ko se je več kot tisoč urbanistov po
> petih dneh opazovanja obdukcije trupla evropskega urbanega organizma
> na svojem zadnjem sestanku strinjalo, da ne morejo ničesar storiti, je
> občinski svetnik za urbanizem Ernst May krogu navdušenih mladih
> arhitektov in zainteresiranih gradbenikov [...] predstavil svoje
> veliko poročilo o ruskem urbanizmu. Mladi ljudje instinktivno čutijo,
> da iz Rusije prihaja nova vitalnost, da tam dozorevajo in se
> uresničujejo nove možnosti, da se lahko ustvarjalno veselje do
> urbanizma, osvobojeno vseh omejitev lastnine in zasebnega dobička, v
> Rusiji v celoti razvije". Svojo "socialdemokratsko himno" uresničenemu
> socializmu sklene s simptomatično "humanistično" regresijo, ki v
> ospredje postavlja etična načela globalnega načrtovanja: v sovjetskem
> mestu "morajo biti največji in najplemenitejši trenutki
> socialističnega *Zeitgeista [...]* vsebovani kot katedrala
> ljudstva".

<!-- Wagner, come May e Hannes Mevyer vedono quindi nell'URSS dei Piani
Quinquennali l'unico possibile spazio di verifica delle ipotesi
enunciate in Germania dal '24 in poi: nella globalità dell'esperienza
della pianificazione gli intellettuali della repubblica di Weimar
ritengono -- per paradosso -- di poter trovare quella collocazione
"esatta" del lavoro tecnico-operativo loro negata da una situazione
capitalistica in involuzione. (E sorvoliamo sul risultato di tale
appello tutto ideologico al "mondo nuovo" del socialismo in costruzione,
più volte analizzato nelle pagine di "Contropiano" e nel recente volume
collettivo *Socialismo, città, architettura*, Roma, Officina, 1971). -->

> Wagner, tako kot May in Hannes Mevyer, je zato v ZSSR petletnih
> načrtov videl edini možni prostor za preverjanje hipotez, izrečenih v
> Nemčiji od leta 24 dalje: v globalnosti izkušnje načrtovanja so
> intelektualci Weimarske republike verjeli - paradoksalno -, da lahko
> najdejo "natančno" uskladitev tehnično-operativnega dela, ki jim jo je
> kapitalistični položaj v involuciji odrekal. (In zamolčimo rezultat
> takšnega povsem ideološkega sklicevanja na "novi svet" socializma v
> gradnji, ki je bil večkrat analiziran na straneh "Contropiana" in v
> nedavnem kolektivnem zborniku *Socializem, mesto, arhitektura,* Rim,
> Officina, 1971).

<!-- Non è quindi con il martirologio dell'architettura di avanguardia
tedesca "uccisa dal nazismo", o con l'esaltazione del suo "impegno"
politico -- il volume della Miller Lane è in ciò tipico -- che si può
fare chiarezza su un momento fondamentale della gestione "sociale" della
città, come quello sperimentato nella repubblica di Weimar. Mettere a
nudo la sostanziale "miseria teorica" di quella gestione, parallela alla
"miseria teorica e alla cura scolastica della verità marxista" deprecata
da Tronti nella socialdemocrazia "classica", può risultare fecondo,
oggi, per evitare, almeno, il ripetersi di errori che prima di essere
"amministrativi" si misurano con lo stallo politico e le disastrose
deviazioni della lotta di classe da essi inevitabilmente provocati. -->

> Zato ni mogoče z mučeništvom nemške avantgardne arhitekture, ki jo je
> "ubil nacizem", ali s poveličevanjem njene politične "angažiranosti" -
> za to je značilna knjiga Miller Lane - osvetliti temeljnega trenutka
> "socialnega" upravljanja mesta, kakršnega je doživela Weimarska
> republika. Razkrivanje bistvene "teoretične bede" tega upravljanja,
> vzporedno s "teoretično bedo in sholastično skrbjo za marksistično
> resnico", ki jo je Tronti obsojal v "klasični" socialni demokraciji,
> je lahko danes plodno, da bi se izognili vsaj ponavljanju napak, ki se
> pred "administrativnimi" merijo s politično pat pozicijo in pogubnimi
> odkloni razrednega boja, ki jih neizogibno sprožijo.

---
references:
########################################################################
# vim: spelllang=sl,it
...
