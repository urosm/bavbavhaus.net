---
title: "Fragment o strojih"
...

> Narava ne gradi strojev, lokomotiv, železnic, electric telegraphs, selfacting
> mules etc. So produkti človeške industrije; naravni material, preobražen v
> organe človeške volje nad naravo ali njegovega udejstvovanja v naravi. *So s
> človeško roko ustvarjeni organi človeških možganov*; upredmetena moč znanja.
> Razvoj capital fixe nakazuje, na kateri stopnji je obče družbeno znanje,
> knowledge, postalo *neposredna produktivna sila*, tako da so sami pogoji
> družbenega življenjskega procesa prišli pod kontrolo general intelecta ter so
> po njemu primerno preusmerjeni. Do katere stopnje so proizvedene družbene
> produktivne sile, ne le v obliki znanja, ampak kot neposredni organi družbene
> prakse; realnega življenjskega procesa.
>
> *Razvoj capital fixe je še po neki drugi strani kazalec stopnje razvoja
> bogastva nasploh ali razvoja kapitala.* Predmet produkcije, usmerjene
> neposredno k uporabni vrednosti in prav tako neposredno k menjalni vrednosti,
> je produkt sam, ki je namenjen konsumpciji. Del produkcije, ki je usmerjen k
> produkciji capital fixe, ne producira neposrednih predmetov za užitek niti
> neposrednih menjalnih vrednosti; vsaj ne menjalnih vrednosti, ki jih je mogoče
> neposredno realizirati. *Že dosežena stopnja produktivnosti -- to, da del
> produkcijskega časa zadošča za neposredno produkcijo -- je torej vzrok temu,
> da se čedalje večji [del] uporablja za produkcijo produkcijskih sredstev.* K
> temu sodi, da lahko družba počaka; da lahko velik del že ustvarjenega bogastva
> odtegne tako neposrednemu užitku kakor tudi produkciji, namenjeni neposrednemu
> užitku, zato, da bi ta del uporabila za delo, ki *ni neposredno produktivno*
> (ravno v materialnem produkcijskem procesu samem). To zahteva visoko raven že
> dosežene produktivnosti in relativen prebitek, in sicer takšno raven, [ki je]
> neposredno sorazmerna s preobrazbo capital circulant v capital fixe. Kako je
> *velikost relativnega surplusnega dela odvisna od produktivnosti nujnega
> dela*, tako [je] *velikost delovnega časa, uporabljenega v produkciji capital
> fixe* -- živega kakor tudi upredmetenega -- [odvisna] od *produktivnosti
> delovnega časa, namenjenega neposredni produkciji produktov*. Pogoj za to sta
> *surplusno prebivalstvo* (s tega stališča) kakor tudi *surplusna produkcija*.
> To se pravi, rezultat časa, uporabljenega za neposredno produkcijo, mora biti
> relativno prevelik glede na neposredno potrebo po njem v reprodukciji
> kapitala, uporabljenega v tej industrijski panogi. *Čim manj fiksni kapital*
> neposredno prinaša sadove, čim manj posega v *neposredni produkcijski proces*,
> večja morata biti relativno *surplusno prebivalstvo* in *surplusna
> produkcija*; torej ga je več za gradnjo železnic, kanalov, vodovodnih
> napeljav, telegrafov etc. kakor za mašinerijo, neposredno dejavno v
> neposrednem produkcijskem procesu. Zato -- k čemur se bomo vrnili pozneje -- v
> nenehni nadprodukciji ali podprodukciji moderne industrije -- nenehna nihanja
> in krči zaradi nesorazmerja, v katerem se preobrazi zdaj premalo, zdaj preveč
> capital circulant v capital fixe. [@marx1985kritika, 506-507]

> Kakor z razvojem velike industrije baza, na kateri temelji prilaščanje tujega
> delovnega časa, ne tvori ali ne ustvarja več bogastva, tako neha s tem
> razvojem tudi *neposredno delo* biti takšna baza produkcije, kolikor se po eni
> strani spreminja v bolj nadzorno in regulirajočo dejavnost; nato pa tudi zato,
> ker produkt neha biti produkt oposameznjenega neposrednega dela, pač pa se,
> ravno narobe, *kombinacija* družbene dejavnosti prikazuje kot producent. "Brž
> ko se razvije delitev dela, je skoraj vsako delo posameznega individua del of
> a whole, *having no value or utility of itself. There is nothing on which the
> labourer can seize: this is my produce, this I will keep to myself.*" (*Labour
> defended*, 1, 2, XI.) V neposredni menjavi se oposameznjeno neposredno delo
> prikazuje kot realizirano v nekem posebnem produktu ali v delu produkta in
> njegov skupnostni družbeni značaj -- njegov značaj upredmetenega občega dela
> in zadovoljitve obče potrebe -- je postavljen zgolj z menjavo. Nasprotno pa je
> v produkcijskem procesu velike industrije po eni strani za produktivno silo
> delovnega sredstva, razvitega v avtomatičen proces, podvrženje naravnih sil
> družbenemu razumu predpostavka, *hkrati pa je po drugi strani delo posameznika
> v svojem neposrednem bivanju postavljeno kot odpravljeno posamezno delo, tj.
> kot družbeno delo. Tako odpade druga baza tega produkcijskega načina.*
> [@marx1985kritika, 508-509]


> Če bi stroj trajal večno, če bi ne bil sam iz minljivega materiala, ki ga je
> treba reproducirati (povsem abstrahiramo od iznajdbe bolj izpopolnjenih
> strojev, ki bi mu odvzeli značaj stroja), če bi bil perpetuum mobile, bi
> najpopolneje ustrezal svojemu pojmu. Njegove vrednosti ne bi bilo treba
> nadomeščati, ker bi trajno obstajala v neuničljivi materialnosti. Ker se
> capital fixe uporablja samo, kolikor je kot vrednost manjši, kakor je kot
> postavljalec vrednosti, bi surplusna vrednost, realizirana v capital
> circulant, čeprav ta sam ne bi nikdar stopil v cirkulacijo kot vrednost,
> vendarle kmalu nadomestila avances, tako da bi sam deloval kot postavljalec
> vrednosti, potem ko bi bili njegovi stroški za kapitalista, prav tako kot
> [stroški] surplusnega dela, = 0. Deloval bi naprej kot produktivna sila dela
> in bil obenem denar v 3\. pomenu, konstantna zase bivajoča vrednost.
> [@marx1985kritika, 553]

---
lang: sl
references:
- type: book
  id: marx1985kritika
  author:
  - family: Marx
    given: Karl
  title: "Kritika politične ekonomije 1857/1858"
  container-title: "Temeljna izdaja"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: "I/8"
  publisher-place: Ljubljana
  publisher: Marksistični center CK ZKS
  issued: 1985
  language: sl
# vim: spelllang=sl,en
...
