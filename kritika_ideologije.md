---
title: "Kritika ideologije"
...

## Strategija

::: {lang=en}

> Like any authentically revolutionary thought, Marx's is driven to destroy what
> already exists in order to build in its place something which does not yet
> prevail. So, Marx's thought has two sides which are distinct from one another
> yet also make up an organic whole. One is the 'ruthless criticism of all that
> exists: in Marx expressed as the discovery of the mystified procedure of
> bourgeois thought and thus as the theoretical demystification of capitalist
> ideologies. The other is the 'positive analysis of the present: which, with
> the maximum level of scientific understanding, brings the future alternative
> to our present. One is a *critique of bourgeois* ideology, the other is a
> scientific analysis of capitalism. These two moments in Marx's oeuvre can be
> understood as both logically divided and chronologically successive from the
> *Critique of Hegel's Philosophy of Right* to *Capital*. This does not at all
> mean that they always have to repeat this division and succession. When Marx
> himself looked at classical political economy and went back along the path
> which had already led him to discover certain general abstract relations
> through his analysis, he well knew that this path was not to be repeated.
> Rather, it was necessary to start out from these simple abstractions -- the
> division of labour, money, value -- in order again to reach the 'living
> whole': the population, the nation, the state, the world market. Thus, today,
> once we have reached the point of arrival of Marx's oeuvre -- that is, Capital
> -- we need to take it as our starting point; once we have arrived at the
> analysis of capitalism, it is this analysis from which we must build again.
> Now, research around certain determinate abstractions -- alienated labour, the
> modifications that have taken place in the organic composition of capital,
> value in oligopolistic capitalism -- should be the starting point for arriving
> at a new 'living whole': the people, democracy, the political state of
> neocapitalism, the international class struggle. Not by chance, this was also
> Lenin's path, from *The Development of Capitalism in Russia* to *The State and
> Revolution*. It is also not by chance that all bourgeois sociology and all
> reformist ideologies of the workers' movement follow the opposite path.
>
> But all this is still not enough: even if we grasp the specific character
> which *the analysis of capitalism* should today assume, we also simultaneously
> need to grasp the specific character that the *critique of ideology* should
> assume. And, here, it is useful to start out from a precise presupposition,
> deploying one of those tendentious exaggerations which are a positive
> characteristic of Marx's own *science*, stimulants to new thought and to
> active intervention in the practical struggle. This presupposition is that
> *any ideology is always bourgeois*, because it is always the *mystified
> reflection* of the class struggle on the terrain of capitalism.
>
> Marxism has been conceived as an "ideology" of the workers' movement. This is
> a fundamental error, since Marxism's starting point, its birth certificate,
> was always precisely the destruction of *all* ideology through the destructive
> critique of all *bourgeois* ideologies. A process of *ideological
> mystification* is only possible, indeed, on the basis of modern bourgeois
> society: it has always been and continues to be the *bourgeois* point of view
> regarding *bourgeois* society. And anyone who has looked at the opening pages
> of *Capital* even once can see that this is not a process of pure thought
> which the bourgeoisie consciously *chooses* in order to mask the *fact* of
> exploitation; rather, it is itself the real, objective process of
> exploitation. That is, it is itself the mechanism of capitalism's development,
> through all of its phases.
>
> For this reason, the working class does not need an 'ideology' of its own. For
> its existence *as a class* -- that is, its presence as a reality antagonistic
> to the entire system of capitalism, its *organisation* into a revolutionary
> class -- does not link it to the mechanism of this development but make it
> independent of and counterposed to it. Rather, the more that capitalist
> development advances, the more the working class can make itself *autonomous
> of* capitalism; the more accomplished the system becomes, the more *the
> working class must become the greatest contradiction within the system*, to
> the point of making this system's survival impossible and rendering *possible*
> and thus *necessary* the revolutionary rupture which liquidates and transcends
> it.
>
> Marx is not the *ideology* of the workers' movement but its *revolutionary
> theory*. This is a theory born as the critique of bourgeois ideologies and
> which must make this critique its daily bread -- it must continue to be the
> 'ruthless criticism of all that exists: A theory that came to constitute
> itself as the scientific analysis of capitalism and that must, at each moment,
> feed on this analysis, must at times identify with it when it needs to make up
> the lost ground and cover the gap, the distance, which has opened up between
> the development of things and the updating and verification of research and
> its tools. A theory which lives only in a function of the working class's
> revolutionary practice, one that provides weapons for its struggle, develops
> tools for its knowledge, and identifies and magnifies the objectives of its
> action. Marx has been and remains the *working-class* point of view regarding
> *bourgeois* society.
>
> But if Marx's thought is the working class's revolutionary theory, if Marx is
> the *science of the proletariat*, on what basis and by what paths has at least
> one part of *Marxism* become a populist ideology, an arsenal of banal
> commonplaces to justify all possible compromises in the course of the class
> struggle? Here, the historian's task becomes enormous. Yet it is obvious that,
> if ideology is a part, a specific, historically determinate articulation of
> the very mechanism of capitalism's development, then the acceptance of this
> 'ideological' dimension -- the construction of the ideology of the working
> class -- can only mean that the workers' movement has itself become, as such,
> a part, a *passive* articulation of capitalist development. That is, it has
> undergone a process of integration into the system. This integration process
> can have various phases and levels, but it nonetheless has one single
> consequence in provoking different phases and different levels -- that is,
> *different forms* -- of that *reformist* practice which ends up today seeming,
> *in appearance*, implicit in the very concept of the working class. If
> ideology in general is always *bourgeois*, an ideology of the working class is
> always *reformist*: that is, it is the *mystified* mode through which its
> revolutionary function is *expressed* and at the same time *inverted*.
> [@tronti2019workers, 5-7]

:::

::: {lang=en}

> Today's situation returns us continually to this attempt, in ai1 ever-harsher way.
> For now we face not the great abstract syntheses of bourgeois thought, but the cult of the most vulgar empirical trivia that has become capital's praxis.
> No longer the logical system of knowledge, the principles of science, but an orderless mass of historical facts, of fragmented experiences, of great *faits accomplis* that no one has ever thought about.
> Science and ideology again merge with and contradict one another, but no longer in a systematisation of ideas meant for eternity, but rather in the day-to-day happenings of the class struggle.
> And this struggle is now dominated by a new reality that would have been inconceivable in Marx's time.
> Capital has placed the whole functional apparatus of bourgeois ideology into the hands of the officially recognised workers' movement.
> Capital no longer manages its own ideology but has the workers' movement manage it in its stead.
> This 'workers' movement' thus functions as an ideological mediation internal to capital; through the historical exercises of this function, the entire mystified world of appearances that contradict reality is attached to the working class.
> That is why we say that today the critique of ideology is a task internal to the workingclass point of view, and has only in the second instance to do with capital.
> The political task of a working-class auto-critique must question the entire past historical course of the workers' class struggle and do so starting from the current state of organisation.
> In the present, the working class does not have to criticise anyone outside of itself, its own history, its own experiences and that corpus of ideas that has been gathered together by others around it.
[@tronti2019workers, 163-164]

:::

::: {lang=en}

> Capitalist power seeks to use the workers' antagonistic will-to-struggle as a motor for its own development.
> The working-class party must take this real working-class mediation of capital's interests and organise it in an antagonistic form, as the tactical terrain of struggle and as a strategic destructive potential.
[@tronti2019workers, 242]

:::


## Teorija

::: {lang=en}

> With [the concept of ideology] intellectual forms are drawn into the dynamic of society by relating them to the contexts that motivated them.
> In this way the concept of ideology critically penetrates their immutable semblance of existing in themselves, as well as their claims to truth.
> In the name of ideology, the autonomy of intellectual products, indeed the very conditions under which they themselves become autonomous, is thought together with the real historical movement of society.
> These intellectual products originate within this movement, and they perform their functions within it, too.
> They may stand in the service of particular interests, whether intentionally or not.
> Indeed, their very isolation, through the constitution of an intellectual sphere and its transcendence, is, at the same time, identified as a social consequence of the division of labor.
[@adorno2022contribution, 19]

:::

> Veste, da je eno izmed sredstev, ena izmed tehnik raziskovanja ideologij ali njihove kritike, da se to raziskovanje ukvarja s kakršnimikoli že proizvodi duha, jih analizira in iz teh proizvodov izvaja družbene zaključke.
> Seveda bi lahko rekli, da se mora kritika ideologij kratko malo ukvarjati z ljudmi, ki so, kot temu pravimo v sociološkem strokovnem jeziku, nosilci idej ali ideologij.
> Vendar pa vam že preprost razmislek pokaže, da ideologije, ki so neposredno ideologije ljudi samih, svojega družbenega izvora vendarle nimajo kar tako v teh ljudeh in njihovem konsenzu, temveč da jim pripadajo kolektivno, po tradiciji ali kako drugače, ali da jih -- in to je značilno za našo sedanjo družbo -- sploh šele proizvajajo skrajno koncentrirane in organizirane oblike tvorjenja javnega mnenja, kulturna industrija v najširšem smislu.
[@adorno2016uvod, 152-153]

Koncept *ideologije* intelektualne forme umesti v družbeno dinamiko tako, da jih poveže z motivacijami in interesi njihovih producentov.
Tako je avtonomijo intelektualnih produktov in pogoje pod katerimi postanejo avtonomni mogoče misliti skupaj z realnim zgodovinskim gibanjem družbe.

::: {lang=en}

> With the dynamization of the contents of the mind through the critique of ideology, one tends to forget that the theory of ideology is itself subject to the same historical movement; that, if not in substance, then nonetheless in function, the concept of ideology transforms through history, and the same dynamic governs this.
> What is called ideology -- and what ideology is -- can only be perceived insofar as one does justice to the movement of the concept; this movement is at the same time one of its objects.
[@adorno2022contribution, 20]

:::

> Hierarhijo in prisilo odseva celo deduktivna forma znanosti.
> Kot so prve kategorije reprezentirale organizirano pleme in njegovo moč nad posameznikom, temeljijo logični red, odvisnost, povezanost, zaobjemanje in sklenjenost pojmov v ustreznih razmerjih socialne dejanskosti, delitve dela.
> Seveda pa ta družbeni značaj miselnih form ni, kot uči Durkheim, izraz družbene solidarnosti, temveč je priča nepredirne enotnosti družbe in gospostva.
> Gospostvo podeljuje družbeni celoti, v kateri se utrjuje, zvišano konsistentost in moč.
> Delitev dela, v katero se gospostvo družbeno razvije, služi odvladani celoti za samoohranitev.
> S tem pa celota kot celota, kot potrjevanje njej imanentnega uma, nujno postaja izvrševanje partikularnega.
> Gospostvo stopa posamezniku nasproti kot obče, kot um v dejanskosti.
> Moč vseh članov družbe, ki jim kot takšnim ni na voljo noben drug izhod, se zaradi delitve dela, ki jim je naložena, vedno znova sumira ravno v realizaciji celote, katere racionalnost se tako zopet pomnožuje.
> To, kar se vsem dogaja od nekaterih, se zmerom izvršuje kot premaganje posameznikov s strani mnogih; vselej ima tlačenje družbe obenem poteze tlačenja nekega kolektiva.
> Ta enotnost kolektivnosti in gospostva, ne pa neposredna družbena občost, solidarnost, je tista, ki se odtiskuje v miselnih formah.
[@horkheimer2002dialektika, 35-36]

"Družbeno telo", očitna enotnost družbe, četudi ločeno po funkciji roke in glave, se ne izteče v harmoniji, temveč nakazuje pošastno združenje v zgodovini.
Posrednik med partikularnimi interesi roke in glave -- v zgodovini prispodob gre ponavadi za srce, sicer pa politika -- ne izniči ideologije v racionalizirajočem projektu.
Ni prostega trga idej kjer svobodni intelektualci ponujajo svoj inventar.
Sam medij izmenjave je že pod nadzorom najmočnejšega.
Kljub lastnim interesom so šibkejši primorani prilagoditi se "razumu" močnejšega, pa kakorkoli nerazumno je potem to stanje.


## Tehnologija

> Razvoj kapitalizma ne le pospešuje razvoj tehnologije, temveč tudi spreminja osnovno obliko razvoja tehnologije -- kapitalistične tehnologije niso več orodja, ki se umeščajo v družbena razmerja, temveč so stroji, ki družbena razmerja avtomatizirajo (proces realne subsumpcije).
> To ne velja le za ekonomsko področje (ki s tem postaja tehnonomsko), ampak tudi za področji ideologije in kulture.
> Ideologija v klasični, diskurzivni in na pisavi ali pisanju utemeljeni obliki je bila tako kot ročno orodje nekaj, s čimer se je kapitalizem sprva povezal in kar je sprva uporabljal, ne pa njegovo notranje določilo -- diskurzivna ideologija je bolj stvar *ancien régima* kot kapitalizma, saj je kapitalizem po revoluciji na področju kognitivnih tehnologij (računalniki) začel razvijati svoje oblike medijev in komunikacije, ki so avtomatizirane, zato niso več diskurzivne in ne temeljijo na pisavi (kot kognitivnem ročnem orodju).
[@krasovec2021tujost, 131]


---
lang: sl
references:
- type: book
  id: tronti2019workers
  author:
  - family: Tronti
    given: Mario
  title: "Workers and capital"
  translator:
  - family: Broder
    given: David
  publisher-place: London
  publisher: Verso
  issued: 2019
  language: en
- type: article-journal
  id: adorno2022contribution
  author:
  - family: Adorno
    given: Theodor
  title: "Contribution to the theory of ideology"
  translator:
  - family: Bard-Rosenberd
    given: Jacob
  container-title: "Selva: a journal of the history of art"
  issue: 4
  issued:
    season: 3
    year: 2024
  page: 19-33
  language: en
- type: book
  id: adorno2016uvod
  author:
  - family: Adorno
    given: Theodor
  title: "Uvod v sociologijo"
  translator:
  - family: Leskovec
    given: Alfred
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2016
  language: sl
- type: book
  id: horkheimer2002dialektika
  author:
  - family: Horkheimer
    given: Max
  - family: Adorno
    given: Theodor
  title: "Dialektika razsvetljenstva: filozofski fragmenti"
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2002
  language: sl
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
...
