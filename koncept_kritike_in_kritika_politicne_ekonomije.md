---
title: "Koncept kritike in kritika politične ekonomije"
...



Pri Rancièrjevem besedilu zasledujemo nit, ki jo izpostavi Primož
Krašovec v [*Tujost kapitala*](tujost_kapitala.md). In sicer
naj bi Rancière odločilno pokazal, da pri dilemi med teorijo fetišizma
in tezo o avtonomiji kapitala ne gre nujno za "igro ničelne vsote" ter
da je mogoče razviti antihumanistično teorijo fetišizma
[@krasovec2021tujost, 59-60]. Glede tega so relevantni zadnji deli
prispevka, ko Rancière obravnava *koncept fetišizma*.

Predhodne razdelke beremo nekoliko naivno, motivirano zgolj z osnovnim
zanimanjem za neko kanonično besedilo in osnovnim zanimanjem za
teoretske razlike in napovedi med zgodnjim Rancièrjem in njegovo
kasnejšo politično filozofijo oziroma estetiko.

<!--
(Morda zanimivo: za Althusserja naj bi teorija fetišizma bila
neznanstven ostanek pri Marxu. A zdi se, da gre za "znanstven" napredek
iz kritike ideologije, koncepta, ki se ga Althusser oklepa.
-->

## Kritika politične ekonomije v *Pariških rokopisih 1844*

*Uvod v koncept kritike*. Rancière skuša določiti "podobo kritične
teorije, ki deluje v *Pariških rokopisih 1844*". In sicer, pravi, gre za
*antropološko* kritiko, ki jo Marx izvaja na podlagi feuerbachovske
antropologije. Rancière tovrstno kritiko poskuša definirati skozi
odgovore na tri vprašanja in sicer: (1) kakšen je predmet kritike; (2)
kakšen je subjekt kritike oziroma kdo kritizira; ter (3) kakšna je
metoda kritike [@ranciere2018koncept, 69].

Odgovor naj bi bil strnjen v sklepnem delu Marxovega pisma Rugeju:

> Težnjo naše revije lahko zaobjamemo z enim samim obrazcem:
> samopojasnjevanje našega časa o njegovih bojih in težnjah. Ta naloga
> se postavlja svetu in nam. To delo je mogoče opraviti le z združenimi
> močmi: spoved je, nič več kot to. Da bi človeštvo doseglo odpuščanje
> grehov, jih mora samo razglasiti za to, kar so [Marx, navedeno po
> @ranciere2018koncept, 69; sicer pa gl. @marx1977pismi, 147].

Iz tu Rancière razbere: (1) predmet kot *izkušnjo*; (2) subjekt kot
človeštvo, ki je doseglo točko, ko lahko razume samo sebe, kritično
zavest; ter (3) metodo kot *razglaševanje* ali pojasnjevanje, to je
razglaševanje nečesa (človeške izkušnje) kot to kar je (grehi
človeštva). 



Iz tega:

> Naloga kritike je, da izreka ali bere protislovje, da ga razglaša za
> to, kar je [@ranciere2018koncept, 70].

Kritiko od navadnega izreka ločuje to, da za temi protislovji zaznava
globlje protislovje, "tisto, ki ga izraža koncept *odtujitve*"
[@ranciere2018koncept, 70].

*Koncept odtujitve*. 

*Koncept resnice*




<!-- 1. Raven politične ekonomije -->

<!-- 2. Kritična obdelava -->

<!-- 3. Amfibolija in njen temelj -->

<!-- 4. Razvoj protislovja: zgodovina in subjektivnost ali gibala in
vzgibi -->

<!-- 5. Kritični diskurz in znanstveni diskurz -->

## Kritika in znanost v *Kapitalu*



141: Fetišizem ni ne odtujitev kot antropološki proces, ne ideologija
kot predstava ekonomskih razmerij.

147: Fetišizem je ravno to skrivanje procesa v rezultatu. Konkretne
pojavne oblike kapitala so hkrati oblike njegovega samoskrivanja, in
bolj ...

147:

> Tu ne gre beseda o tem, da bi se v procesu potujevanja predikati
> subjekta povnanjali v tujem bitju, pač pa potujitev pomeni tisto, kar
> se zgodi s kapitalskim odnosom v najbolj posredovani obliki procesa.

... pri mladem Marxu subjekt postane objekt svojega objekta, kar je
odtujitev kot razmerje med osebo in stvarjo. V teoriji fetišizma ni več
subjekt tisti, ki je ločen od samega sebe in katerega predikati preidejo
v tujo zadevo ali reč, temveč postane sama forma kapitala tuja
kapitalskemu razmerju, ki ga izraža. ... Tisto, kar je pri tem
"popredmeteno" ali "porečovljeno", niso predikati subjekta, temveč
kapitalistična produkcijska razmerja sama. (Krašovec 62-63)

153: [kaj je določujoče razmerje ...]

## Opombe za sklep

---
lang: sl
references:
########################################################################
- type: chapter
  id: ranciere2018koncept
  author:
  - family: Rancière
    given: Jacques
  title: "Koncept kritike in kritika politične ekonomije od *Pariških rokopisov 1844* do *Kapitala*" 
  title-short: "Koncept kritike in kritika politične ekonomije"
  container-title: "Branje kapitala"
  container-author:
  - family: Althusser
    given: Louis
  - family: Balibar
    given: Étienne
  - family: Establet
    given: Roger
  - family: Macherey
    given: Pierre
  - family: Rancière
    given: Jacques
  translator:
  - family: Breznik
    given: Maja
  - family: Koncut
    given: Suzana
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2018
  page: 65-165
  language: sl
########################################################################
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
########################################################################
- type: chapter
  id: marx1977pismi
  author:
  - family: Marx
    given: Karl
  title: "Pismi Rugeju"
  container-title: "Izbrana dela v petih zvezkih"
  volume: 1
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1977
  page: 135-147
  language: sl
# vim: spelllang=sl
...
