.POSIX:
.SUFFIXES:
.PHONY: all clean

PANDOC ::= pandoc --data-dir pandoc
MDFILES != find *.md 2>/dev/null
DOCXFILES ::= $(MDFILES:.md=.docx)
HTMLFILES ::= $(MDFILES:.md=.html)
PDFFILES ::= $(MDFILES:.md=.pdf)

all: $(HTMLFILES)

$(HTMLFILES): pandoc/templates/bavbavhaus.net.html5

.SUFFIXES: .md .html
.md.html:
	$(PANDOC) -d html -o $@ $<

$(DOCXFILES):

.SUFFIXES: .md .docx
.md.docx:
	$(PANDOC) -d docx -o $@ $<


$(PDFFILES): pandoc/defaults/pdf.yaml

.SUFFIXES: .md .pdf
.md.pdf:
	$(PANDOC) -d pdf -o $@ $<


clean:
	rm -f $(HTMLFILES)
	rm -f $(DOCXFILES)
	rm -f $(PDFFILES)
