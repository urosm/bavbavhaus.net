---
title: "Franco Fortini, Dve avantgardi"
...

<!-- 1\. Riprendendo un motivo di Lukács si può dire che la cosiddetta
avanguardia storica è fondata sul ripudio della categoria della
mediazione. L'atteggiamento avanguardistico nasce, si sa, col
Romanticismo, da un senso molto vivo della contraddizione e del
conflitto. Contraddizione e conflitto che in alcuni intellettuali
europei diventarono acutissimi quando la rivoluzione rousseauiana e
giacobina si chiarì madre della borghesia filistea. I nodi
dell'avanguardia storica compaiono ad ogni stretta di sviluppo delle
società industrializzate, almeno a partire dall'età napoleonica. -->

> 1\. Če povzamemo Lukácsev motiv, lahko rečemo, da tako imenovana
> zgodovinska avantgarda temelji na zavračanju kategorije mediacije.
> Avantgardistična drža nastane, se ve, z Romantiko iz živahnega občutka
> protislovja in konflikta. Protislovje in konflikt, ki sta se pri
> nekaterih evropskih intelektualcih zelo zaostrila, ko se rousseaujevska
> in jakobinska revolucija razjasni kot mati filistrskega meščanstva.
> Vozlišča zgodovinske avantgarde se pokažejo ob vsaki stiski razvoja
> industrializiranih družb, vsaj od napoleonske dobe dalje.

<!-- Quella contraddizione ignora la dialettica: è giustapposizione o
alternanza polare fra soggettività assoluta e assoluta oggettività, fra
irrazionalità astratta – ossia rifiuto del momento discorsivo,
dialogico, comunicativo, a favore dell'associazione, della memoria
involontaria e del sogno – e astratta razionalità, ossia conoscibilità
per via discorsiva, nella particolare accezione naturalistica e
positivistica dell'idea di "ragione". L'avanguardia si rifugia in uno o
nell'altro estremo o li vive simultaneamente, in un modo ben conosciuto
da tutta la tradizione mistica. Proverbiali, in proposito, i passi del
*Primo manifesto surrealista*. -->

> To protislovje spregleda dialektiko: gre za sklop ali polarno izmenjavo
> med absolutno subjektivnostjo in absolutno objektivnostjo, med
> abstraktno iracionalnostjo in abstraktno racionalnostjo, torej
> spoznavnostjo na diskurziven način, v posebnem naturalističnem in
> pozitivističen smislu ideje "razuma". Avantgarda se zateka v eno ali
> drugo skrajnost ali pa ju istočasno živi, na dobro poznan način vsej
> mistični tradiciji. V tem pogledu so pregovorni odlomki iz *Prvega
> nadrealističnega manifesta*.

<!-- In un opuscolo polemico di R. Barthes (*Critique et vérité*, Parigi
1966) m'avviene di leggere definite "stupide" tutte "le opposizioni
estetico-morali fra un uomo organico, impulsivo, automatico, informe,
bruto, oscuro, ecc. e una letteratura volontaria, lucida, nobile,
gloriosa a forza di costrizioni espressive... perché l'uomo
psicanalitico non è geometricamente divisibile", eccetera. Anche senza
far intervenire la psicanalisi, si può ricordare che quella opposizione
"stupida" può essere superata solo per via dialettica; altrimenti non
capisco che cosa mai possano essere quei *avers* e quei *revers* mobili
dell'uomo (nozione che Barthes riprende da Lacan) "di cui il linguaggio
trasmuta continuamente le parti". L'antitesi razionalità-irrazionalità,
lo sappiamo tutti che è esaltata dalla cultura borghese ma non risolta
dall'avanguardia se non nella simultaneità dei contrari. Continuiamo
quindi ad impiegarla proprio perché allude ad una ben precisa eredità
storica; è uno dei punti nei quali l'avanguardia non ha certo superato
in nulla la cultura che credeva combattere. -->

> V polemičnem pamfletu R. Barthesa (*Critique et vérité*, Pariz 1966)
> sem slučajno prebral, da so vsa "estetsko-moralna nasprotja med
> organskim, impulzivnim, avtomatskim, brezobličnim, surovim, mračnim
> itd. človekom ter voljno, lucidno, plemenito, slavno literaturo, zaradi
> izraznih omejitev opredeljena kot "neumna" ... ker psihoanalitični
> človek ni geometrično deljiv" in tako dalje. Tudi brez vmešavanja
> psihoanalize se lahko spomnimo, da je to "neumno" nasprotje mogoče
> preseči le dialektično; sicer ne razumem, kaj sploh so lahko ta premična
> *avers* in *revers* človeka (pojma, ki ju Barthes prevzame od Lacana),
> "katerega dele jezik nenehno transmutira". Vsi vemo, da antitezi
> racionalnost-iracionalnost meščanska kultura povzdiguje, a avantgarda ne
> razrešuje, če ne v sočasnosti nasprotij. Nadaljujmo torej z njuno
> uporabo prav zato, ker se navezujeta na zelo natančno zgodovinsko
> dediščino; gre za eno od točk v kateri avantgarda zagotovo ni v ničemer
> presegla kulture, proti kateri je verjela, da se bori.

<!-- 2\. Quel movimento di fuga si voleva antagonistico ad una società e a
una cultura che nella maggior parte delle proprie manifestazioni
sociali, economiche e politiche voleva ancora credere possibili tutte le
mediazioni, soprattutto in nome della storia e del progresso e che
accettava la dilacerazione solo come "aroma spirituale", nell'ordine dei
sentimenti e del dover essere. La cultura egemone voleva credere
possibili le mediazioni reali; detto altrimenti, negava la divisione di
classe. In questo senso la prima e maggiore avanguardia, quella dei
solitari annunciatori ottocenteschi della tempesta, poté parere
profetica dei conflitti, delle rivoluzioni e dei massacri del nostro
secolo. Tuttavia la dilacerazione di tipo borghese-positivistico, fra
razionalismo scientifico e struggimento decadentistico, fra "prosa"
democratica e "sogno" aristocratico-simbolista, aveva ancora largo
margine di sopravvivenza, fra la fine dell'Ottocento e l'inizio del
Novecento e in questo senso (ma solo in questo) l'avanguardia storica
del primo decennio del secolo è davvero scherno e rottura, si pone
contro i termini di quel dissidio, li deride, smaschera la realtà di un
accordo profondo... e, in non pochi casi, si prepara a sostituirlo. Non
dimentichiamo che l'idea di progresso (riformista) e di trasformazione
(anche rivoluzionaria) doveva essere ripugnante alle avanguardie
storiche o almeno a gran parte di esse. Se non lo fu (come nel caso dei
cubofuturisti russi) si trattò di un vitale equivoco, che gli eventi
successivi si premurarono di chiarire, anche sanguinosamente; dovuto
all'apparente parallelismo fra il momento distruttivo della rivoluzione
politica e quello degli artisti e degli scrittori di avanguardia. -->

> 2\. To ubežno gibanje je želelo biti sovražno družbi in kulturi, ki je v
> večjem delu svojih družbenih, ekonomskih in političnih izrazov še vedno
> želela verjeti v možnost vseh mediacij, predvsem v imenu zgodovine in
> napredka, in ki je sprejemala raztrganje samo kot "spiritualno aromo" v
> redu občutij in najstva. Hegemona kultura je želela verjeti v možnost
> realnih mediacij; drugače rečeno, zanikala je razredno delitev. V tem
> smislu lahko prvo in največjo avantgardo, tisto od osamljenih
> napovedovalcev nevihte iz 19\. stoletja, označimo za preroško za
> spopade, revolucije in poboje našega stoletja. Kakor koli,
> meščansko-pozitivistično raztrganje, med znanstvenim racionalizmom in
> dekadentnim hrepenenjem, med demokratično "prozo" in
> aristokratsko-simbolističnimi "sanjami", je še vedno imelo široke
> možnosti preživetja, med koncem 18\. stoletja in začetkom 19\. in v tem
> smislu (a le v tem) je zgodovinska avantgarda prvega desetletja stoletja
> resnično zasmehovanje in prelom, postavlja se proti pogojem tega
> nesoglasja, se jim posmehuje, razkriva realnost globokega soglasja ...
> in se, nemalokrat, pripravlja, da ga nadomesti. Ne pozabimo, da sta
> morali biti ideji (reformističnega) napredka in (tudi revolucionarne)
> spremembe za zgodovinsko avantgardo, ali vsaj za njen večji del,
> odvratni. Če ni bilo (kot v primeru ruskih kubofuturistov), je šlo za
> bistven nesporazum, katerega so nadaljnji dogodki poskrbeli razjasniti,
> celo s krvjo; zaradi očitne vzporednice med destruktivnim momentom
> politične revolucije in umetnikov ter pisateljev avantgarde.

<!-- 3\. Ora accade che una delle caratteristiche elementari delle società di
nuovo capitalismo (quali si sono venute manifestando insieme allo
sviluppo tecnologico ed economico nel corso degli ultimi quindici anni
ma che, come in Usa, si rivelavano alla pupilla socioculturale assai
prima e cioè immediatamente alla vigilia e durante la seconda guerra)
sia *la tendenza ad attenuare o a far scomparire da gran parte delle
proprie espressioni ideologiche ogni possibilità di impiego della
categoria della mediazione* nonché la dimensione e il senso medesimo del
moto storico. (Non a caso una larga parte del pensiero cattolico sembra
essere divenuto, per sopravvivere, l'erede e il difensore del superstite
umanesimo). L'universo tecnico-scientifico, le codificazioni
cibernetiche, la razionalizzazione produttiva, le tecniche della
previsione, tutto quel che si ispira alla logica di tipo matematico, va
perfettamente d'accordo con l'etica dei consumi e della imprevidenza,
con la liberazione degli istinti, con l'assurdità permanente, eccetera.
La negazione anarchica è tornata, come puntualmente previsto, a fiorire
sul medesimo ceppo della potenza produttiva e dell'ordine (anche
militare) neocapitalistico. Non si tratta di un processo nuovo; è
l'esaltazione di un processo durato per tutto il nostro secolo. Il
silenzio ormai di rigore per ciò di cui "non si può parlare" –
l'incomunicabile e ineffabile della vita – avviva, anche nel ghigno e
nel sarcasmo, quel "senso del mistero" che il vecchio umanista
napoletano, all'inizio della seconda guerra mondiale, considerava fonte
di ogni sciagurato irrazionalismo. L'avanguardia impronunciabile è
l'altra faccia della chiacchiera di massa. La saldatura fra
neoavanguardia e ordine borghese-capitalistico diventa organica ed
esplicita dopo essere stata, per l'avanguardia storica, solo implicita e
indiretta. -->

> 3\. Zdaj sledi, da je ena od osnovnih značilnosti družb novega
> kapitalizma (ki se kažejo skupaj s tehnološkim in ekonomskim razvojem
> zadnjih petnajstih let, a so se, tako kot v ZDA, družbeno-kulturni
> zenici razkrile že veliko prej, tik pred drugo vojno in med njo) *težnja
> po utišanju ali zakrivanju iz večjega dela svojih ideoloških izrazov
> vsako možnost uporabe kategorije mediacije*, kaj šele razsežnost in sam
> smisel zgodovinskega gibanja. (Ne naključno se zdi, da je velik del
> katoliške misli, da bi preživel, postal dedič in zagovornik preživetega
> humanizma). Tehnično-znanstveni univerzum, kibernetične kodifikacije,
> produktivna racionalizacija, tehnike napovedovanja, vse kar navdihuje
> matematična logika se popolnoma ujema z etiko konsumpcije in
> nepredvidljivosti, z osvoboditvijo instinktov, s stalno absurdnostjo, in
> tako dalje. Anarhična negacija se je vrnila točno kot je bilo
> napovedano, da bi zacvetela na istem deblu produktivne moči in
> neokapitalističnega (tudi vojaškega) reda. Ne gre za nov proces; gre za
> poveličevanje procesa, ki traja celo naše stoletje. Zdaj stroga tišina o
> čemer "ni mogoče govoriti" –– poraja, celo v posmeh in sarkastično, ta
> "občutek skrivnosti", ki ga je stari neapeljski humanist, na začetku
> druge svetovne vojne, smatral za vir vsakega nesrečnega iracionalizma.
> Neizrekljiva avantgarda je druga plat množičnega klepetanja. Spajanje
> med neoavantgardo in meščansko-kapitalističnega reda postane organsko in
> nedvoumno potem, ko je bila za zgodovinsko avantgardo le implicitno in
> posredno.

<!-- 4\. Le forme fondamentali dell'avanguardia storica sono insomma
diventate, oggi, semplici strumenti espressivi, meri moduli al servizio
dell'espressione e della comunicazione odierna. Nelle forme letterarie
della nuova avanguardia non riesco a scorgere novità sostanzialmente
diverse da quelle elaborate dalle avanguardie europee nei primi
trent'anni del secolo. (Non direi certo la stessa cosa per le arti
plastiche e per la musica). O meglio, una novità ci sarebbe, c'è, ed è
fondamentale; e l'aveva programmaticamente intuita, una dozzina d'anni
fa, Sanguineti, quando – riprendendola da Cézanne – mi scriveva una
frase che avrebbe poi ripetuta più volte, anche se, direi, l'ha in
seguito contraddetta per necessità di propaganda fide: e cioè che si
dovesse fare dell'avanguardia un'arte durevole come quella dei musei.
Esattamente quel che allora capivo male e che ora vengo dicendo: la sola
diversità reale della neoavanguardia da quella storica consiste, quando
consiste, nell'uso quasi esclusivamente ironico e "classicheggiante" del
materiale iconografico, verbale e psichico che nella maggiore
avanguardia storica era spesso ancora "tragico" e di diretta discendenza
romantica. -->

> 4\. Temeljne forme zgodovinske avantgarde so, na kratko, danes postale
> preprosti izrazni instrumenti, zgolj moduli v službi današnjega izraza
> in komunikacije. V literarnih formah nove avantgarde ne morem zaznati
> novosti, ki bi se bistveno razlikovale od tistih, ki jih je evropska
> avantgarda razvila v prvih tridesetih letih stoletja. (Zagotovo ne bi
> rekel iste stvari za likovno umetnost ali glasbo). Ali bolje, novost, ki
> bi bila, je, in je temeljna; in bila je programsko zaznana, pred ducat
> leti, Sanguineti, ko mi je napisal frazo, ki jo je pozneje večkrat
> ponovil, čeprav, bi rekel, ji je dalje oporekal za potrebe razširjanja
> vere: in torej, da je treba narediti iz avantgarde trajno umetnost kot
> to iz muzejev. Prav to sem takrat slabo razumel in kar zdaj pravim:
> edina realna razlika med neoavantgardo in zgodovinsko avantgardo je,
> kadar je, v skoraj izključno ironični in "klasicistični" uporabi
> ikonografskega, verbalnega in psihičnega materiala, ki je v večini
> zgodovinskih avantgard bil še pogosto "tragičen" in neposredno
> romantičnega izvora.

<!-- Mi spiego, risparmiandomi di ripetere una dimostrazione troppe volte
svolta: se il potere di "denuncia" nell'espressione artistica
d'avanguardia è diminuito fino a sussistere solo per i pagi, per le zone
culturalmente sottosviluppate; se gli stessi apologeti dell'avanguardia
sono costretti a postulare una sorta di automatica rinascita del potere
d'opposizione per entro l'integrazione (ma non si tratta forse della
forza di contestazione implicita in *qualsiasi* opera d'arte?) questo
accade, appunto, perché la dimensione "tragica" dell'avanguardia (il suo
aspetto catastrofico-agonico) ha perduto e perde rilevanza a favore del
momento "ironico" (parodia, scherno, dissacrazione). Quest'ultimo è in
netta, per non dire assoluta, prevalenza: non solo in tutte le forme di
"poesia visiva" o di ritaglio ma nella grande maggioranza delle
composizioni in verso e in prosa che si fondano su l'impiego della
"frase fatta" o della microcitazione. Tutte le forme di letteratura
sarcastica, umoristica, patafisica, lunatica, di beffa, di epigramma, di
sconsacrazione del linguaggio comunicativo e persuasivo (accuratamente
ispirate alle antologie bretoniane dell'"humour nero") hanno avuto da
noi, e negli ultimi anni, uno sviluppo ignoto alle età precedenti; la
cabarettistica e la canzonetta polemica, questi generi che ancora venti
anni fa erano da noi quasi sconosciuti, sembrano divenute tranquille
istituzioni democratiche (come stanno per esserlo il controllo delle
nascite e il divorzio...) Quella sembra insomma essere la provincia,
voglio dire il *domaine*, della recente avanguardia. -->

> Naj pojasnim, brez zapravljanja ponavljanja že prevečkrat izvedene
> predstavitve: če se je moč "denunciacije" v umetniškem izrazu avantgarde
> zmanjšala do mere, da obstaja le za pagi, za kulturno nerazvita območja;
> če so isti apologeti avantgarde prisiljeni predpostavljati nekakšen
> avtomatski preporod moči opozicije zaradi integracije (toda ali ni to
> morda moč oporekanja implicitna v *kateremkoli* umetniškem delu?) se to
> zgodi prav zato ker "tragična" dimenzija avantgarde (njen
> katastrofično-agonijski vidik) je izgubila in izgublja pomembnost v prid
> "ironičnemu" momentu (parodije, posmeha, zaničevanja). Slednje je v
> očitni, da ne rečem v absolutni, prevladi: ne samo v vseh formah
> "vizualne poezije" ali izrezkov, temveč tudi v veliki večini kompozicij
> v verzu ali prozi, ki temeljijo na uporabi "besednih zvez" ali
> mikrocitatov. Vse forme sarkastične, humoristične, patafizične,
> lunatične, posmehljive, epigramske literature skrunjenja komunikativnega
> in prepričevalnega jezika (skrbno navdihnjene z bretonskimi antologijami
> "črnega humorja") so pri nas in v zadnjih letih imele razvoj neznan
> prejšnjim dobam; kabaretistika in polemična popevka, te žanri ki pred
> dvajsetimi leti so bila pri nas skoraj neznani, se zdijo da so postale
> mirne demokratične institucije (kot bosta nadzor rojstva in ločitev...)
> To se zdi, skratka, da je provinca, hočem reči *domena*, nedavne
> avantgarde.

<!-- In questa seconda metà del secolo le successive ondate delle
avanguardie, da quella espressionista, imagista, futurista e Esprit
Nouveau, a quella dadaista e surrealista, eccetera fino a quelle degli
anni più recenti, si sono sovrapposte, erodendo e svuotando molte delle
forme che hanno tuttavia accompagnato il nostro secolo, voglio dire la
narrativa del "realismo critico" e la poesia postsimbolista. Ma entro il
1930 tutto questo era già compiuto, con l'opera di Charlot, Picasso,
Klee, Mondrian, Kandinskij; di Proust, Joyce, Kafka, Musil; e con la
maggior parte dell'opera di Brecht, Breton, Eluard. Le opere e i
creatori che sono venuti dopo, ossia nell'ultimo trentennio, non hanno
probabilmente nulla da invidiare alle opere e ai creatori del primo
trentennio del secolo. Ma molto hanno da invidiare invece i
raggruppamenti. Quelli furono non soltanto strumenti di guerra
letteraria ma vere officine di forme. -->

> V tej drugi polovici stoletja zaporedni valovi avantgard, ta od
> ekspresionistov, imagistov, futuristorv in Espirit Nouveau, dadaistov in
> nadrealistov, in tako dalje do tistih iz zadnjih let, se prekrivajo,
> spodkopavajo in izpraznjujejo številne forme, ki so kakorkoli spremljale
> naše stoletje, hočem reči pripoved o "kritičnem realizmu" in
> postsimbolični poeziji. Toda do 1930 je bilo vse to že opravljeno, z
> deli Charlota, Picassa, Kleeja, Mondriana, Kandiskega; Prousta, Joyca,
> Kafka, Musila; in z večjim delom del Brechta, Bretona, Eluarda. Dela in
> ustvarjalci, ki so nastali po tem, torej v zadnjih tridesetih letih,
> verjetno nimajo kaj zavidati delom in ustvarjalcem prvih trideset let
> stoletja. Toda nasprotno pa imajo veliko za zavidati skupine. Te so bile
> ne samo instrumenti literarne vojne, ampak prave delavnice form.

<!-- 5\. Cases è tornato recentemente sul tema dell'avanguardia dicendone
l'esperienza "valida e significativa in quanto ha messo il dito sul
punto dolente, sulla inadeguatezza dell'eredità umanistica a comprendere
l'istanza del presente, sull'abisso che si scava sotto i piedi della
civiltà". Ma questo "gesto" dell'avanguardia – come d'altronde Cases
medesimo rammenta – va interpretato con la vecchia (anzi millenaria,
direbbe l'immancabile imbecille) voce di Lukács: "Quanto più genuine e
profonde queste esperienze, tanto più decisamente spezzano quell'unità
concreta e sensibile che è la premessa, il fondamento di ogni creazione
estetica". Vale a dire: l'esperienza valida e significativa
dell'avanguardia storica consiste nell'aver spezzato, insieme col
fondamento della creazione *estetica*, il velo che impediva di scorgere
l'abisso; consiste nell'aver chiesto imperiosamente la propria fine
medesima, il proprio superamento nella prassi sovversiva e
rivoluzionaria. Altrimenti, che senso può avere "mettere il dito sul
punto dolente"? Se l'eredità umanistica ci ha ingannati, illudendoci di
poterne entrare stabilmente in possesso quando, invece, quel possesso
ormai o è di tutti o non è di nessuno, la polemica avanguardistica
contro quella eredità in tanto è valida in quanto ratifica, e non
soltanto a parole, la fine della istituzione poetica e artistica (nella
società borghese-capitalistica, va da sé). -->

> 5\. Cases se je pred kratkim vrnil k temi avantgarde in dejal, da je
> bila njena izkušnja "veljavna in pomembna v kolikor je pokazala na
> bolečo točko, na neustreznost humanistične dediščine za razumevanje
> zahtev sedanjosti, na brezno, ki se koplje pod nogami civilizacije".
> Toda to "gesto" avantgarde – kot drugje opozarja tudi sam Cases – je
> treba interpretirati s starim (celo tisočletnimi, bi rekel neizogiben
> imbecil) besedami Lukácsa: "Bolj kot so te izkušnje pristne in globoke,
> bolj odločilno prekinjajo to konkretno in čutno enotnost ki je premisa,
> temelj vsake estetske stvaritve". To pomeni: veljavna in pomenljiva
> izkušnja zgodovinske avantgarde je v razbitju, skupaj s temeljem
> *estetske* stvaritve, tančico, ki je preprečevala uvideti brezno; je v
> ukazovalni zahtevi svojega konca, svojega preseganja v subverzivni in
> revolucionarni praksi. Kaj je sicer smisel "pritiskanja na bolečo
> točko"? Če humanistična dediščina nas je prevarala, nas preslepila da
> lahko stabilno posedujemo, medtem ko, to posestvo je vedno ali od vseh
> ali od nikogar, avantgardna polemika proti tej dediščini v celoti je
> veljavna v kolikor potrdi, in ne samo z besedo, konec pesniške in
> umetniške institucije (v meščansko-kapitalistični družbi, seveda).

<!-- Ma non questa la via tenuta dalla cosiddetta nuova avanguardia; che anzi
proprio in questo o solo in questo, ripeto, si differenzia realmente
dalla precedente. Come ho già detto, la novità è nella rinuncia alla
oltranza e alla trasgressione pratica. Nella misura in cui taluni
rappresentanti dell'avanguardia italiana ricalcano le orme di Breton,
finiscono con l'accettare inevitabilmente (ma in misura ovviamente
aggravata dal tempo intermesso) quel compromesso fra l'azione
surrealista e il poema che fu *felix culpa* dei migliori "fedeli
d'amore" dei primi gruppi e misera caduta di molti epigoni. Né vale gran
che il loro oltranzismo politico, inoffensivo nella misura in cui non
tocca, né in teoria né in pratica, i veri problemi di fondo del mondo di
oggi ma li usa come mero ornamento della propria parenesi. Molto più
coerenti, fra gli avanguardisti, quelli che istituzionalizzano lo
scherno e la fatuità e si collocano a "destra". Costoro hanno, mi
sembra, correttamente concluso (Cases lo nega e certo anch'io lo nego
per l'avanguardia storica *ma lo ritengo vero per gli ultimi giunti*)
che l'avanguardia è "un semplice assestamento dell'arte all'epoca della
tecnica"; hanno capito che le scoperte dei nonni vanno sistematicamente
applicate come un sistema di segni relativamente innovatore, ma senza
più pretendere di travalicare, di andare oltre la istituzione
letterario-artistica. Va aggiunto che, da questo punto di vista, le più
recenti tendenze del formalismo strutturalista (R. Barthes), proprio
accentuando l'idea che la letteratura (ma anche la critica e più in
genere le scienze umane) non sia che funzione del linguaggio, anzi
proprio restaurando i valori della "retorica" di tipo
classico-medievale, aiutano a smascherare il doppio giuoco
dell'estremismo neosurrealista e si costituiscono, come di fatto sono,
guardie del corpo di un formalismo che nella sua essenza nominalistica è
ben "tradizionale" e che può benissimo accogliere in sé tanto il
*collage* di puro spasso, quanto il racconto di logica paradossale,
quello di amplificazione barocca, la poesia neoorfica o elettronica,
ecc. -->

> Toda to ni pot, ki jo je nova avantgarda vzela v obzir; nasprotno prav v
> tem ali samo v tem, ponavljam, se realno razlikuje od prejšnje. Kot sem
> že rekel, novost je v odpovedi od skrajnosti in od praktičnega
> prestopka. V meri v kateri nekateri predstavniki italijanske avantgarde
> sledijo Bretonovim stopinjam, zaključijo z neizogibnim sprejetjem (toda
> v meri ) tistega kompromisa med nadrealističnim dejanjem in pesmijo, ki
> je bil *felix culpa* najboljših *zvestih ljubezni* prvih skupin in
> žalosten propad mnogih epigonov. Ni veliko veredno, da njihova politična
> skrajnost, neškodljiva v meri v kateri se ne dotika, ne v teoriji ne v
> praksi, pravih problemov v temelju današnjega sveta, ampak jo uporablja
> zgolj kot ornament lastni parenezi. Veliko bolj koherentni, med
> avantgardisti, ki institucionalizirajo posmeh in brezobzir in se
> postavljajo na "desno". Te ljudje, se mi zdi, so pravilno zaključili
> (Cases to zanika in jasno tudi jaz to zanikam za zgodovinsko avantgardo
> *toda to imam za resnično za zadnje prišleke*), da je avantgarda
> "preprosta prilagoditev umetnosti dobi tehnike"; so dojeli, da je treba
> odkritja starih staršev sistematično uveljaviti kot relativno inovativen
> sistem znakov, toda brez pretvarjanja, da presegajo literarno-umetniško
> institucijo. Treba je dodati da, iz tega stališča, bolj nedavne tendence
> strukturalističnega formalizma (R. Barthes), prav z poudarjanjem ideje,
> da literatura ni drugega kot funkcija jezika, 
> literarno-umetniško institucijo. Dodati je treba, da so s tega vidika
> najnovejše težnje strukturalističnega formalizma (R. Barthes) s
> poudarjanjem ideje, da je literatura (pa tudi kritika in nasploh
> humanistične vede) le funkcija jezika, in z obnavljanjem vrednot
> klasično-srednjeveške "retorike" pomagajo razkriti dvojno igro
> neosurrealističnega ekstremizma in se konstituirajo, kot dejansko so,
> telesni stražarji formalizma, ki je v svojem nominalističnem bistvu zelo
> "tradicionalen" in ki lahko v sebi zelo dobro sprejme tako *kolaž* čiste
> zabave kot zgodbo paradoksalne logike, zgodbo baročne amplifikacije,
> neorfično ali elektronsko poezijo itd.

<!-- Detto più sinteticamente: il salto storico fra l'avanguardia 1905-30 e
quella odierna (italiana) è – contrariamente ad un luogo del linguaggio
corrente – il salto da una fase "aperta" della espressione (e della
lotta sociale) ad una fase – in Europa – "chiusa". -->

Povedano bolj na kratko: zgodovinski preskok med avantgardo 1905-30 in
današnjo (italijansko) avantgardo je – v nasprotju z mestom v sedanjem
jeziku – preskok iz "odprte" faze izražanja (in družbenega boja) v – v
Evropi - "zaprto" fazo.

<!-- 6\. *E lo riportavo nella stalla silenziosa.* Questo noto verso di
Sanguineti, che si riferisce ad una *consistente coda! frigida frusta!
oh muscolo! oh pugno! penetrante* mi pare possa essere inteso, via dalla
sua metafora genitale, come allegorico della vicenda dell'avanguardia.
Il "pugno penetrante" è stato riportato nella stalla. Cioè nei termini
della istituzione. Personalmente, ritengo utilissima la stalla; e la
coscienza della stalla. E, ripeto, qui sta, quando stia, la differenza
sola fra le avanguardie d'una volta e le nuove. Ossia: *le nuove non
sono avanguardie*. Sono gruppi letterati che s'arrangiano come possono,
sono scrittori che ce la fanno e scrittori che non ce la fanno, geniali
o sciocchi, mediocri o bravi, ecc. Naturalmente, come lettore o come
critico, faccio conto dei vari autori che negli ultimi dieci anni hanno
scritto nei modi delle avanguardie; e per alcuni di quelli o per alcune
opere loro faccio anche gran conto. Anzi tutta l'operazione, penso, è
stata utile. Meglio: assolutamente benefica, un aggiornamento
tecnologico; una introduzione di nuove macchine che può aver colto di
sorpresa i sindacalisti letterari abituati al vecchio stile di
agitazioni liriche ma che fa sorgere nuovi problemi, ad un livello più
avanzato; i problemi che vengono dopo il benessere, quelli del *cafard
après la fête*. Dove invece penso non si debba né rispetto né
considerazione è, in genere, per le razionalizzazioni, le
ideologizzazioni, le attività pseudocritiche. È proprio il contrario di
quel che di solito si sente dire da chi pregia l'intelligenza ideologica
e critica delle avanguardie nostre ma guarda con sospetto i testi
"creativi": questi hanno talvolta interesse e spesso anche vita – che è
dir molto – mentre la chincaglieria critico-ideologica è quasi sempre di
un livello pauroso per rozzezza, eclettismo, letture di accatto e,
spesso, semplice imbonimento o ignoranza. La disinvoltura di non pochi
trattatisti delle neoavanguardie sarebbe una virtù se non fosse della
specie di quella di cui dispongono i cani, privi notoriamente di ogni
umano rispetto e d'ogni timidezza nell'attraversare una camera da letto
o ardente o una sala di concerto o anatomica: vale a dire, l'innocenza.
E certo fa un po' pietà rammentare che non pochi di costoro avevano già
denunciato, anni or sono, la manovra dei sozzi vecchi
dell'*establishment*, volta a circuirli e ucciderli con la dolcezza.
Doveva essere. Inghiottiti dalle sabbie mobili, levano ergotando i loro
titoli di successione e pensano a quando erano più giovani e un po' meno
editi. -->

6\. *Odpeljal sem ga nazaj v tihi hlev.* Ta znani Sanguinetijev verz, ki
govori o *doslednem repu! frigidnem biču! o mišici! o pesti!
prodirajoči, se* mi zdi, da ga je mogoče razumeti, če odmislimo njegovo
genitalno metaforo, kot alegorijo afere avantgarde. "Pronicljiva pest"
je bila vrnjena v hlev. To pomeni v okviru institucije. Osebno menim, da
je hlev zelo koristen; in zavest o hlevu. In, ponavljam, tu je, ko je,
edina razlika med starimi in novimi avantgardami. To pomeni, da *novi
niso avantgarda*. To so literarne skupine, ki se trudijo po svojih
najboljših močeh, to so pisatelji, ki jim uspe, in pisatelji, ki jim ne
uspe, ne glede na to, ali so odlični ali neumni, povprečni ali dobri in
tako naprej. Seveda kot bralec ali kritik upoštevam različne avtorje, ki
so v zadnjih desetih letih pisali v avantgardnem slogu, in za nekatere
od njih ali za nekatera njihova dela imam tudi velik pomen. Menim, da je
bila celotna operacija koristna. Boljše: absolutno koristno, tehnološka
posodobitev; uvedba novih strojev, ki so morda presenetili literarne
sindikaliste, vajene starega sloga lirske agitacije, a ki sprožajo nove
probleme na naprednejši ravni; probleme, ki pridejo po dobrem počutju,
probleme *cafard après la fête*. Po drugi strani pa menim, da nismo
dolžni ne spoštovanja ne upoštevanja, in sicer na splošno za
racionalizacije, ideologizacije, psevdokritične dejavnosti. To je ravno
nasprotno od tega, kar običajno slišimo od tistih, ki hvalijo ideološko
in kritiško inteligenco naših avantgard, na "ustvarjalna" besedila pa
gledajo z nezaupanjem: ta so včasih zanimiva in pogosto celo živa - kar
veliko pove -, medtem ko so kritično-ideološke smeti zaradi grobosti,
eklekticizma, poceni branja in pogosto preprostega šumenja ali neznanja
skoraj vedno na zastrašujoči ravni. Nonšalantnost kar nekaj
neoavantgardnih obdelovalcev bi bila vrlina, če ne bi bila tiste vrste,
ki jo imajo psi, ki jim manjka vsakršnega človeškega spoštovanja ali
sramežljivosti, ko prečkajo spalnico, kurilnico, koncertno dvorano ali
anatomsko sobo: to je nedolžnost. In seveda je malo škoda, če se
spomnimo, da jih je kar nekaj že pred leti obsodilo manever umazanih
starcev *establišmenta, ki jih je* želel obkrožiti in ubiti s
sladkostjo. Moralo je biti. V tekočem pesku dvignejo svoje nasledstvene
nazive in se spomnijo, ko so bili mlajši in malo manj objavljeni.

<!-- 7\. È bene rammentare che oggi non esiste nessuna differenza sostanziale
fra la cultura di massa e quella di élite. Non o non soltanto per la già
esistente omogeneità fra i produttori di questa e di quella, così come
fra i rispettivi destinatari; ma per la similarità delle strutture. -->

7\. Dobro se je zavedati, da danes ni bistvene razlike med množično in
elitno kulturo. Ne ali ne samo zaradi že obstoječe homogenosti med
proizvajalci tega in onega ter med njihovimi naslovniki, temveč tudi
zaradi podobnosti struktur.

<!-- In questo senso credo giusta la direzione di indagine proposta da
Barthes: che prevede omologhe, ad una analisi ravvicinata, le assise
profonde della cosiddetta "cattiva letteratura" o "letteratura di massa"
e quella della cosiddetta "buona letteratura"; a cominciare dalle
avanguardie. Questo non significa, naturalmente, che la distinzione
apparente possa sparire; essa è troppo necessaria all'industria. Né che
i passaggi, gli imprestiti linguistici dall'uno all'altro settore
abbiano particolare valore o debbano destare speciali paure o allarmi.
Si è in presenza di una ambiguità che solo la prassi, cioè l'uso, può
risolvere. Dire che, in Occidente almeno, la distinzione fra letteratura
e cultura di élite e quella di massa non esiste, può essere usato
infatti per affermare che le distinzioni di classe sono o in via di
scomparsa o in via di essere introiettate, sì che in ognuno di noi
conviverebbero ormai il padrone e il servo, il capitalista e lo
sfruttato, il produttore e il consumatore di subcultura; e questo uso
reazionario dell'indistinzione omette di fatto il vero argomento e cioè
che non sitratta affatto di separare o di riunire o di mescolare i
messaggi provenienti dalla cosiddetta cultura di massa da e con quelli
provenienti dall'altra e "alta", bensì di osservare in quale contesto,
in quale "cultura" preformata, in quale tessuto di valori vadano a
intervenire. Che equivale a ricordare la impossibilità, per l'unica
cultura esistente per noi (quella del capitalismo), di produrre una
pedagogia non incoerente e, al tempo stesso, a ribadire la identità –
già vissuta dalle rivoluzioni sovietica e cinese – fra rivoluzione
socialista e pedagogia generalizzata, di tutti mediante tutti; unica
"cultura di massa" autentica se mai una ve ne fu. -->

V tem smislu menim, da je smer raziskovanja, ki jo je predlagal Barthes,
pravilna: predvideva namreč, da so globoke osi tako imenovane "slabe
literature" ali "množične literature" in tako imenovane "dobre
literature" ob natančni analizi homologne; začenši z avantgardo. To
seveda ne pomeni, da lahko navidezno razlikovanje izgine; za industrijo
je preveč potrebno. To tudi ne pomeni, da imajo prehodi, jezikovni
prenosi iz enega sektorja v drugega kakšno posebno vrednost ali da bi
morali vzbujati poseben strah ali zaskrbljenost. Soočamo se z
dvoumnostjo, ki jo lahko razreši le praksa, torej uporaba. Če rečemo, da
vsaj na Zahodu razlika med elitno in množično literaturo in kulturo ne
obstaja, lahko dejansko trdimo, da razredne razlike izginjajo ali pa se
uvajajo, tako da v vsakem od nas zdaj sobivajo gospodar in hlapec,
kapitalist in izkoriščani, proizvajalec in potrošnik subkulture; in ta
reakcionarna uporaba razlikovanja pravzaprav izpušča pravi argument, ki
je, da sploh ne gre za to, da bi ločili, združili ali pomešali
sporočila, ki prihajajo iz tako imenovane množične kulture, od in s
tistimi, ki prihajajo iz druge, "visoke" kulture, temveč za to, da
opazujemo, v kakšnem kontekstu, v kateri vnaprej oblikovani "kulturi", v
kakšno tkivo vrednot posegajo. To pomeni, da se moramo spomniti, da za
edino kulturo, ki za nas obstaja (kultura kapitalizma), ni mogoče
ustvariti pedagogike, ki ne bi bila neskladna, in hkrati ponovno
potrditi identiteto – ki sta jo že izkusili sovjetska in kitajska
revolucija – med socialistično revolucijo in splošno pedagogiko vseh
skozi vse; edino pristno "množično kulturo", če je kdaj obstajala.

<!-- Una più attenta considerazione dello svolgimento delle forme e delle
espressioni letterarie nel resto del mondo avrebbe forse dovuto
suggerire una diversa politica ai promotori delle neoavanguardie
italiane. Si ha l'impressione che della neoavanguardia la mediazione
editoriale abbia offerto al pubblico una griglia o una chiave
interpretativa erronea o distorta. Fenomeno, questo, che non ha però
luogo soltanto in Italia, come testimoniano certe impressionanti, per
sostanziale vacuità ed eclettismo, tavole rotonde pubblicate da riviste
francesi o tedesche sui problemi dell'avanguardia o del romanzo o della
nuova poesia. Si ha l'impressione che la volontà di aggiornamento, di
adeguamento, di sprovincializzazione sia stata sbagliata non nella sua
meta ma nei suoi mezzi; mezzi, appunto, esclusivamente volontaristici.
Per forza – si diceva una volta a Firenze – non si fa nemmeno l'aceto.
Si è creduto di potersi adeguare ad una condizione – ossia al portato,
al repertorio delle avanguardie storiche – senza capire che questo
poteva essere fatto solo a condizione di cancellare, come nell'età di
mezzo dicevano facesse il leone, le proprie tracce con la coda, ossia
senza creare "movimenti". Le esigenze editorial-mercantili premevano, è
chiaro, perché si seguisse la via del rumore, del "movimento", del
"gruppo"; e allora si è stati costretti ad escogitare o riesumare tutte
le teorie (surrealiste e dadà) che mescolavano azione ed espressione,
gesto e parola, eccetera, aggirandole con la nozione di opera aperta, di
poesia visiva o tecnologica, con le esposizioni, le serate ed altre
manifestazioni di attivismo. Senza avvedersi che – ormai in tutto il
mondo – era avvenuta una capitale dislocazione: da una parte le
innovazioni formali delle avanguardie erano divenute di repertorio e
quindi le nuove espressioni potevano tranquillamente accettare prima di
nascere (invece di fingere di subirlo fra grida e gesticolazioni) quel
mercato-museo o museo-mercato che è il loro fin dai tempi di Omero;
dall'altro le azioni-opere avevano trovate altre forme, erano divenute
comportamenti di massa o almeno di larghi strati delle popolazioni
occidentali, nel costume *beatnik*, nel neoanarchismo politico,
relativamente irraggiungibili da parte di chi voglia, nonostante tutto,
continuare a considerarsi "scrittore" o "artista". Alcuni, certo, quel
ritardo e quell'errore lo avevano inteso; e, scostandosi dai più fatui e
"caldi", sono andati presto cercando una specialistica freddezza. Ma la
gradevole *image* della neoavanguardia locale non poteva essere buttata
via tanto facilmente. Era un marchio di fabbrica, un distintivo, era
costato buone lire. D'altronde, se nel nostro paese, certi equivoci sono
così a lungo intrattenuti ciò accade, è noto, per via dello sviluppo
ineguale, della scalarità della nostra economia. Non serve che libri e
riviste circolino per ogni parte della penisola, il periodico di
avanguardia stampato a Chieti o Treviso o il dibattito culturale
sull'avvenire della lingua letteraria tenuto al Circolo Salvemini di
Mazara del Vallo o alla Casa della Cultura di Rovereto si situano in un
contesto così diverso da quello delle città cosiddette maggiori,
adempiono a così diverse funzioni, da finire – ad opera di quelle forze
che, proprio nella provincia e nei centri minori, sono spesso più
intelligenti, preparate e generose delle altre – col ricevere una
giustificazione ed un significato che in sé non avrebbero; anche per
lacontinua indolente presenza di posizioni retrive e non soltanto
arretrate. Così, finché resterà da battere un circolo, una associazione
o una galleria d'arte, i rappresentanti di commercio della letteratura
tecnologica, neodadà, visiva, elettronica o come volete, avranno lavoro
da svolgere, giovani liceali da promuovere e conturbare, signore da
scandalizzare, degni sacerdoti da conquistare ("Siano riconosciute dalla
Chiesa le nuove tendenze artistiche adatte ai nostri tempi",
Costituzione pastorale *Gaudium et spes*, par. 62). -->

Natančnejši razmislek o razvoju literarnih oblik in izrazov v drugih
delih sveta bi moral zagovornikom italijanske neoavantgarde morda
predlagati drugačno politiko. Občutek je, da je uredniško posredovanje
neoavantgarde javnosti ponudilo napačno ali izkrivljeno mrežo ali
interpretacijski ključ. Ta pojav pa se ne dogaja samo v Italiji, kar
dokazujejo nekatere impresivne okrogle mize o problemih avantgarde,
romana ali nove poezije, ki jih objavljajo francoske ali nemške revije.
Človek ima občutek, da je bila volja po posodobitvi, prilagoditvi,
deprovincializaciji napačna ne pri cilju, temveč pri sredstvih; sredstva
so bila dejansko izključno voluntaristična. Seveda – tako so rekli v
Firencah – ne moreš narediti niti kisa. Verjeli smo, da se lahko
prilagodimo nekemu stanju – to je dediščini, repertoarju zgodovinske
avantgarde -, ne da bi razumeli, da je to mogoče le pod pogojem, da z
repom izbrišemo svoje sledi, kot je to v srednjem veku počel lev, to je
brez ustvarjanja "gibanj". Jasno je, da je uredniško-merkantilistična
potreba po tem, da bi šla po poti hrupa, "gibanja", "skupine", zato smo
bili prisiljeni izumiti ali ekshumirati vse teorije (nadrealistične in
dadà), ki so mešale akcijo in izraz, gesto in besedo itd., in jih
zaobiti s pojmom odprtega dela, vizualne ali tehnološke poezije, z
razstavami, večeri in drugimi manifestacijami aktivizma. Ne da bi se
zavedali, da se je po vsem svetu že zgodil kapitalski premik: po eni
strani so formalne novosti avantgard postale repertoar, zato so lahko
novi izrazi mirno sprejeli, še preden so se rodili (namesto da bi se med
kričanjem in gestikulacijami pretvarjali, da trpijo), tisti trg-muzej
ali muzej-trg, ki jim pripada že od Homerjevih časov; po drugi strani pa
so akcije-dela našle druge oblike, postale so množično vedenje ali vsaj
vedenje velikih slojev zahodnega prebivalstva, v *beatniški* preobleki,
v političnem neo-anarhizmu, relativno nedosegljive za tiste, ki se kljub
vsemu želijo še naprej imeti za "pisatelja" ali "umetnika". Nekateri so
seveda razumeli to zamudo in napako; in ker so se odvrnili od bolj
blaznih in "vročih", so kmalu začeli iskati specializirano hladnost.
Toda prijetne *podobe* lokalne neoavantgarde ni bilo mogoče tako zlahka
zavreči. To je bila blagovna znamka, oznaka, stala je veliko denarja. Po
drugi strani pa je znano, da so v naši državi nekateri nesporazumi tako
dolgo obstajali zaradi neenakomernega razvoja in neenakomernosti našega
gospodarstva. Knjige in revije ni treba razširjati po vseh delih
polotoka; avantgardna revija, ki jo natisnejo v Chietiju ali Trevisu,
ali kulturna razprava o prihodnosti knjižnega jezika v Circolo Salvemini
v Mazara del Vallo ali v Kulturnem domu v Roveretu se nahajajo v
kontekstu, ki je tako drugačen od konteksta tako imenovanih velikih
mest, in opravljajo tako različne funkcije, da na koncu – z delom tistih
sil, ki so prav v provincah in manjših centrih pogosto pametnejše,
pripravljene in velikodušnejše od drugih
- dobijo utemeljitev in pomen, ki ju same po sebi ne bi imele; tudi
  zaradi stalne indolentne prisotnosti nazadnjaških in ne le
nazadnjaških stališč. Dokler bo torej obstajal krožek, združenje ali
umetniška galerija, bodo komercialni predstavniki tehnološke literature,
neodadaistične, vizualne, elektronske ali kakršne koli že, imeli delo,
mladi srednješolci delo, ki ga bodo spodbujali in vznemirjali, gospe
delo, ki ga bodo vznemirjale, dostojni duhovniki delo, ki ga bodo
osvajali ("Naj Cerkev prepozna nove umetniške smeri, prilagojene našemu
času", Pastoralna konstitucija *Gaudium et spes,* par. 62).

<!-- 8\. Mi raccontava qualche anno fa Viktor Sklovskij che quando erano
tornati, in Unione Sovietica, l'interesse e gli studi per il formalismo
nella critica letteraria (di cui egli era stato, come tutti sanno, uno
dei maggiori promotori negli anni venti, fra Rivoluzione, Guerra Civile
e Nep), vari più di lui giovani studiosi avevano riscoperto, per così
dire, quei metodi di lavoro, non senza l'appoggio di tecniche recenti,
di apparecchiature elettroniche e di formule matematiche. Sì che, in
occasione – credo di rammentare – d'un pubblico dibattito a Mosca
affollato di studenti, dopo l'intervento d'uno di questi neoformalisti,
richiesto Sklovskij di parlare avrebbe detto: "Sono lieto di certe
conferme. I nostri antenati, gli Sciti, solevano sempre due volte
deliberare, se la materia era di grande momento: la prima da ebbri, la
seconda a mente sgombra. Ebbene, voi state ripetendo da sobri quel che
noi, venticinque anni fa, abbiamo detto, per la prima volta, da
ubriachi". -->

8\. Pred nekaj leti mi je Viktor Sklovskij povedal, da je, ko sta se v
Sovjetski zvezi obudila zanimanje za formalizem v literarni kritiki (za
katerega je bil, kot vsi vedo, eden glavnih promotorjev v dvajsetih
letih, med revolucijo, državljansko vojno in Nepom) in njegovo
preučevanje, nekaj mladih znanstvenikov, več kot on, tako rekoč na novo
odkrilo te delovne metode, ne brez podpore najnovejših tehnik,
elektronske opreme in matematičnih formul. Tako zelo, da je bil
Sklovskij ob neki javni razpravi v Moskvi, polni študentov, po
intervenciji enega od teh neoformalistov povabljen k besedi in je dejal:
"Zadovoljen sem z nekaterimi potrditvami. Naši predniki, Skiti, so vedno
razpravljali dvakrat, če je bila zadeva zelo pomembna: prvič, ko so bili
omamljeni, drugič, ko so imeli trezno glavo. Trezno ponavljate to, kar
smo pred petindvajsetimi leti prvič povedali, ko smo bili pijani.

<!-- Il gesto di questa battuta, insolente fino alla generosità, vale contro
i nipoti delle avanguardie storiche. Tuttavia la sobrietà, se davvero
praticata, ha i suoi meriti. Nel mondo formato dall'odierna industria,
l'anima del vino non canta più nelle bottiglie, come ai tempi del poeta
che per primo schernì l'uso della terminologia militare applicata alle
lettere e alla politica. E in anni a noi vicini, la morte di Dylan
Thomas per etilismo acuto parve ormai stranamente in ritardo sulla
realtà e fuori tempo. Le droghe newyorkesi o californiane sono un
fenomeno sociale prima che uno strumento di levitazione letteraria. *Vi
lascia sobri? Si può leggere al mattino?*: con queste parole Brecht –
che veniva, lui, dall'avanguardia storica ma per non restarci – ha
lasciato un consiglio prezioso alle avanguardie nuove ed a noi. -->

Gesta te šale, ki je drzna do velikodušnosti, se dobro drži proti vnukom
zgodovinske avantgarde. Kljub temu ima treznost, če jo resnično
izvajamo, svoje prednosti. V svetu, ki ga oblikuje današnja industrija,
duša vina ne poje več v steklenicah, kot je pela v času pesnika, ki se
je prvi posmehoval uporabi vojaške terminologije v pismih in politiki. V
letih, ki so nam bila blizu, se je smrt Dylana Thomasa zaradi akutnega
etilizma zdaj zdela nenavadno pozna in izven časa. Newyorške ali
kalifornijske droge so družbeni pojav, preden postanejo instrument
literarne levitacije. *Ali vas pusti trezne? Lahko berete zjutraj?* s
temi besedami je Brecht – ki je prišel, on, iz zgodovinske avantgarde, a
tam ni ostal – zapustil dragocen nasvet novi avantgardi in nam.

<!-- *Quattro postille.* -->

<!-- 1\. Dunque le due avanguardie sono una sola. Quella nuova sta alla
precedente come... (l'adulto al giovane...?) Ma perché cercare ancora?
Basta rammentare, come ho già detto, l'aumento dell'ironia, la
diminuzione della gara (il rapporto mercato-museo non è nato di colpo,
si è sviluppato lungo mezzo millennio almeno). Il delirio è diventato
delirio di immobilità. Ma qui ci verrà chiesto come mai le forme
dell'avanguardia storica abbiano avuta, in Italia, così scarsa fortuna.
È questo un argomento importante perché rivelatore di molti equivoci.
-->

1\. Torej sta obe avantgardi eno. Novi je za prejšnjega kot... (odrasli
za mladega...?) Toda zakaj bi iskali naprej? Dovolj je, da se spomnimo,
kot sem že omenil, povečanja ironije, zmanjšanja konkurence (odnos med
trgom in muzejem se ni rodil kar naenkrat, razvijal se je vsaj pol
tisočletja). Delirij je postal delirij negibnosti. Toda tu se bomo
vprašali, zakaj so imele oblike zgodovinske avantgarde v Italiji tako
malo sreče. To je pomemben argument, saj razkriva številne nesporazume.

<!-- Una risposta superficiale accuserebbe il ritardo dello sviluppo
industriale. Ma le avanguardie russe? Quelle spagnole? I surrealisti
bulgari, bosniaci, arabo sauditi, martinicani? Non c'è forza al mondo
che negli anni trenta impedisca ad un gruppo di intellettuali di
Bucarest o di Oviedo, di Cracovia o di Alessandria d'Egitto di godersi
una proiezione dello *Chien Andalou* o di pubblicare una rivista di
scritture automatiche e di saggi su Sade. La risposta è un'altra: le
avanguardie storiche, a loro modo, in Italia una fortuna l'hanno avuta.
Non sarà stata quella dissacrante e cinica o galante o perversa o
rivoltosa; ma il futurismo non era stato soltanto Marinetti, era stato
anche Palazzeschi e Ungaretti; e "Lacerba" è stata una rivista italiana.
E anche più tardi, in certe formule del Novecento letterario. -->

Površni odgovor bi krivdo za zamudo pripisal industrijskemu razvoju.
Toda ruska avantgarda? Španci? Bolgarski, bosanski, savdski, martiniški
nadrealisti? Na svetu ni sile, ki bi v tridesetih letih prejšnjega
stoletja skupini intelektualcev iz Bukarešte ali Ovieda, iz Krakova ali
Aleksandrije preprečila, da bi si ogledali film *Chien Andalou* ali
izdali revijo avtomatskega pisanja in esejev o Sadu. Odgovor je drug:
zgodovinske avantgarde so imele v Italiji na svoj način srečo. Morda ni
bila zaničevalna, cinična, galantna, perverzna ali uporniška, toda
futurizem ni bil le Marinetti, ampak tudi Palazzeschi in Ungaretti, in
"Lacerba" je bila italijanska revija. In še pozneje, v nekaterih
formulah literarnega Novecenta.

<!-- 2\. Uno dei sofismi consueti agli apologeti dell'avanguardia d'oggi è di
cominciare con scrupolose distinzioni fra avanguardie storiche e non
storiche, fra questo e quel gruppo e tendenza e sottotendenza; salvo
poi, nelle perorazioni, dimenticarsi di tutte queste belle cose e
difender se stessi difendendo i bisnonni (che ne fanno benissimo a meno)
anzi fruire del più stolto degli argomenti, quello della normale
incomprensione che accompagna il nuovo e della sua inevitabile gloria,
ecc. Stolto perché oblitera quanta parte di quel nuovo non è stato,
spesso giustamente, mai compreso; e ricatta i presenti con
inverificabili venturi. In questa materia anche i più intelligenti e
spregiudicati cadono frequentemente nell'uso dell'argomento della
novità: "ma come", si legge di frequente "questo è un argomento di
trenta, di cinquanta anni fa" oppure "così si scriveva prima della
guerra" e simili. Quale sia il pregiudizio di questo modo di parlare
(non dirò: di ragionare, che sarebbe troppo) lo dovrebbe vedere
chiunque. -->

2\. Ena od običajnih sofistik apologetov današnje avantgarde je, da
začnejo z natančnim razlikovanjem med zgodovinsko in nezgodovinsko
avantgardo, med to in ono skupino ter med trendom in podtrendom; potem
pa v svojih peroralnih razmišljanjih pozabijo na vse te lepe stvari in
se branijo z obrambo svojih pradedov (ki jim gre zelo dobro tudi brez
njih); nasprotno, uporabljajo najbolj neumne argumente, tiste o
običajnem nerazumevanju, ki spremlja novo in njegovo neizogibno slavo
itd. Neumno, ker zabriše, koliko tega novega ni bilo – pogosto
upravičeno – nikoli razumljenega; in izsiljuje sedanjost z nepreverljivo
prihodnostjo. Neumno je, ker ne upošteva, koliko novega, pogosto
upravičeno, ni bilo nikoli razumljeno, in ker izsiljuje sedanjost z
nepreverljivimi prihodnostmi. Pri tem tudi najbolj pametni in
brezobzirni pogosto uporabijo argument novosti: "ampak kako," pogosto
beremo, "to je argument izpred tridesetih, petdesetih let" ali "tako je
bilo napisano pred vojno" in podobno. Kaj je predsodek tega načina
govorjenja (ne bom rekel: utemeljevanja, to bi bilo preveč), bi moral
videti vsakdo.

<!-- 3\. L'idea che – non già l'avanguardia ma – l'arte in generale sia una
risposta alla trasformazione dei rapporti umani in cose (reificazione) e
quindi un modo di conservare o preservare l'immagine umana, sì che, come
voleva Schiller, dalla copia si possa un giorno ricostruire l'originale,
è, appunto, idea dell'età goethiana. E quel che sento chiamare talvolta
momenti costitutivi e coesistenti dell'avanguardia, il momento
eroico-patetico e il momento cinico, mi pare non siano altro che due
vecchi luoghi deputati della cultura romantica: la tragedia e l'ironia.
Certo trasformati, l'uno e l'altro, nel corso d'un secolo e mezzo ma non
alterati davvero per quanto è della loro essenza: che è appunto di
risposta ad una condizione fatta all'uomo in quanto merce. -->

3\. Ideja, da je – ne avantgarda, temveč umetnost na splošno – odziv na
preoblikovanje človeških odnosov v stvari (reifikacija) in s tem način
ohranjanja ali konserviranja človeške podobe, da bi, kot je želel
Schiller, iz kopije nekoč lahko rekonstruirali izvirnik, je prav ideja
Goetheve dobe. In to, kar včasih slišim imenovati konstitutivna in
soobstoječa momenta avantgarde, junaško-patetski moment in cinični
moment, se mi ne zdi nič drugega kot dva stara predstavnika romantične
kulture: tragedija in ironija. V stoletju in pol so se tako eni kot
drugi gotovo spremenili, vendar se v svojem bistvu niso zares
spremenili, kar je natanko odziv na stanje, ki je bilo postavljeno
človeku kot blagu.

<!-- 4\. In più d'una occasione, mi sembra, Sanguineti ha voluto stabilire un
rapporto fra mercificazione ed avanguardia. Quest'ultima sarebbe
relativa alle condizioni economiche in cui si sviluppa e giunge a
maturazione la storia dell'arte borghese e, più precisamente, alle
relazioni economico-sociali quali si riflettono nel mercato artistico e
nella pseudoclasse intellettuale. Ma proprio perché questa è una
pseudoclasse, ci vien detto, non è possibile inferire alcun rapporto
necessario fra classe e avanguardia, come sarebbe *provato* dalla
mancanza di correlazione fra avanguardia e *posizioni politiche* dei
suoi componenti. -->

4\. Zdi se mi, da je Sanguineti večkrat želel vzpostaviti razmerje med
komodifikacijo in avantgardo. Slednje bi bilo povezano z ekonomskimi
pogoji, v katerih se razvija in dozoreva zgodovina meščanske umetnosti,
natančneje z ekonomsko-socialnimi odnosi, ki se odražajo na trgu
umetnosti in v intelektualnem psevdo-razredu. Toda prav zato, ker gre za
psevdo razred, nam pravijo, ni mogoče sklepati o nujnem razmerju med
razredom in avantgardo, kar naj bi *dokazovalo* pomanjkanje korelacije
med avantgardo in *političnimi stališči* njenih sestavnih delov.

<!-- In questo modo, con questo giuoco delle tre tavolette, il carattere di
classe dell'avanguardia viene brillantemente dissolto proprio mentre con
questa vistosa accentuazione delle relazioni economico-sociali ci si va
dimostrando terribilmente "marxisti". -->

Na ta način, s to igro treh tablic, se razredni značaj avantgarde
briljantno razblini, ravno ko se s tem spektakularnim poudarjanjem
družbeno-ekonomskih odnosov izkaže, da smo strašno "marksistični".

<!-- Connotazione *di classe*, dovremmo saperlo, non corrisponde affatto a
connotazione *politica*. L'esempio corrente della divergenza "politica"
fra cubofuturismo russo e futurismo italiano significa soltanto che
l'applicazione politica della ideologia dei due gruppi fu divergente
(proprio secondo una equivocità tipica della *intelligencija*) ma non
significa affatto che non vi fosse un'affinità profonda (o almeno che le
affinità non vincessero sulle differenze) fra le ideologie artistiche e
letterarie che ispiravano i due movimenti, le quali non erano poi altro
che fiori dal ramo delle ideologie generali della borghesia europea,
italiana o russa. -->

*Razredna* konotacija, kot moramo vedeti, nikakor ne ustreza *politični*
konotaciji. Sedanji primer "političnega" razhajanja med ruskim
kubofuturizmom in italijanskim futurizmom pomeni le, da se je politična
uporaba ideologije obeh skupin razhajala (prav v skladu z dvoličnostjo,
značilno za *inteligenco),* nikakor pa ne pomeni, da ni bilo globoke
sorodnosti (ali da vsaj sorodnosti niso premagale razlik) med umetniško
in literarno ideologijo, ki je navdihovala obe gibanji, ki sta bili
zgolj cvetovi iz vej splošne ideologije evropskega, italijanskega ali
ruskega meščanstva.

<!-- Ma su questo punto converrebbe non insistere. Da una parte si vuol porre
l'avanguardia in rapporto con la mercificazione (altrimenti dove
finirebbe il "marxismo"?) per non cadere nella palude
dell'indifferenziata, astorica e cosmica "alienazione"; cara anch'essa
all'avanguardia ma a quella di "destra". D'altra parte non si vuol
rinunciare al gruzzolo di buona coscienza che ci viene dalle
"neoavanguardie" dei paesi cosiddetti "socialisti"; non si vuol
rinunciare al piacere di essere dalla parte della Rivoluzione e del
Progresso... Un bel pasticcio, comunque. -->

Vendar bi bilo bolje, če pri tem ne bi vztrajali. Po eni strani hočemo
avantgardo postaviti v razmerje s komodifikacijo (kam bi sicer prišel
"marksizem"?), da ne bi padli v močvirje nediferencirane, ahistorične in
kozmične "odtujitve", ki je prav tako draga avantgardi, vendar "pravi".
Po drugi strani pa se ne želimo odreči dobri vesti, ki prihaja iz
"neoavantgarde" tako imenovanih "socialističnih" držav; ne želimo se
odreči užitku, da smo na strani revolucije in napredka ... Vsekakor pa
je to lep nered.
