---
title: "Zadnji posnetki evropske inteligence"
...

## Leva inteligenca in tehnologija


Lack of plausable vision 


---
lang: sl
references:
- type: article-magazine
  id: tronti2022ondestituent
  author:
  - family: Tronti
    given: Mario
  title: "On destituent power"
  container-title: "Ill will"
  issued: 2022-05-22
  URL: https://illwill.com/on-destituent-power
  language: en
# vim: spelllang=sl
...
