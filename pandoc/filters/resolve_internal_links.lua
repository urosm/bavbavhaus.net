SCRIPT_NAME = "resolve_internal_links.lua"
os.setlocale("C")

local Inlines = pandoc.Inlines

return {
  { Link = function(l)
    local fp = l.target:match("^(.+%.md)#?.*$")
    if not fp then return end
    
    local f = io.open(fp)
    if f == nil then
      local input_file = PANDOC_STATE.input_files[1]
      print(("[WARNING] Broken link: %s -> %s"):format(input_file, l.target)) -- TODO: update for pandoc.info
      return l.content
    end
    f:close()

    l.target = l.target:gsub("^(.+)%.md(#?.*)$", "%1.html%2")

    return l
  end }
}
