---
title: "Pier Vittorio Aureli, Architecture and Abstraction"
...

Kaj ga zanima?

::: alts

> Cilj te knjige je brati vzpon in moč abstrakcije skozi materialne
> odnose, ki omogočajo *design* in gradnjo arhitekture.  Moj namen je
> prikazati, da arhitektura ni abstraktna, ker ji manjkajo ornamenti in
> je omejena na golo strukturo (kar je posledica abstrakcije, ne pa
> vzrok), temveč zato, ker je *vsaka* forma arhitekture, ki je zasnovana
> in zgrajena po *projektu*, sama po sebi abstrakcija, ki postane
> konkretna.  Konkretnost se pri tem ne nanaša le na materialnost
> stavbe, temveč tudi -- in predvsem -- na njen proizvodni proces
> [@aureli2023architecture, xx--xxi].

<!-- -->

> The aim of this book is to read the rise and the power of abstraction
> through the material relations that make possible the design and
> building of architecture.  My intention is to demonstrate how
> architecture is abstract not because it lacks ornamentation and is
> reduced to its bare structure (which is a consequence of abstraction,
> not the cause), but because *any* form of architecture that is
> designed and built according to a project is in itself an abstraction
> that becomes concrete.  Concreteness here refers not only to the
> materiality of a building but also -- and especially -- to its process
> of production [@aureli2023architecture, xx--xxi].

:::

V prvem poglavju, pravi, bo ponudil "predzgodovino" pojma *projekt* kot
tehnološke naprave, ki se je razvila iz izkušnje gradnje velikih
struktur.  Namerava preseči pojmovanje projekta kot zgolj arhitekturne
ideologije "volje po formi" in ga umestiti v dinamiko razmerij moči
znotraj družbene delitve dela, kot napravo nadzora in uvrednotenja.
Poleg tega predlaga še definiciji pojma plana in planiranja.

V nadaljnjih poglavij obravnava trope arhitekturne abstrakcije od risbe,
mreže, racionalizacije in forme.

---
lang: sl
references:
########################################################################
- type: book
  id: aureli2023architecture
  author:
  - family: Aureli
    given: Pier Vittorio
  title: "Architecture and abstraction"
  publisher-place: Cambridge, Mass.
  publisher: The MIT Press
  issued: 2023
  language: en
# vim: spelllang=sl,en
...
