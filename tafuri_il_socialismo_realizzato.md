--
title: Il socialismo realizzato e la crisi delle avanguardie
author: Manfredo Tafuri
date: 1971
--

Non esiste analisi seria delle vicende delle avanguardie artistiche
nella Russia sovietica che possa prescindere dal legare la storia di
quelle avanguardie a quella della «scuola formalista», nata dalla
convergenza dei due gruppi di ricerca linguistico-letteraria di
Pietroburgo e di Mosca.

Se esiste infatti una chiave di volta capace di spiegare le aporie
interne alla vicenda dell'avanguardia artistica nella Russia sovietica,
questa è esattamente l'analisi del dibattito che si accentra, ancor
prima della Rivoluzione di Ottobre, intorno alle tesi dell'*Opojaz*[^1].
Ciò che è in gioco, sin dall'apparire delle prime indagini critiche di
Šklovskij, Jakobson, Vinogradov o Eichenbaum, è infatti la
*funzionalità* dell'arte, in quanto costruzione dotata di leggi e
strutture affatto specifiche nel contesto istituzionale in cui il
linguaggio fonda la sua genesi. Se è vero – come ha scritto
Eichenbaum[^2], – che il gruppo iniziale dei formalisti tendeva a
«emancipare la parola poetica dalle tendenze filosofiche e religiose che
avvincevano sempre più i simbolisti», è anche vero che quel tanto
insistere sul carattere convenzionale, artificioso, sovrapersonale
dell'arte, non ha altro scopo che isolare l'oggetto poetico in un
laboratorio anatomico, per indagare, nell'autonomia assoluta della sua
interna struttura evolutiva, i modi di funzionamento delle istituzioni
linguistiche[^3].

E' a questo punto che l'innesto dell'analisi formale sul tronco delle
avanguardie storiche diviene problematico. La parola-oggetto di
Chlébnikov, la poesia urbana di Majakovskij, la tensione antiestetica di
Burljuk e Kručënyck, la disgregazione de i nessi sintattici e la
«riduzione al fonema» dei futuristi in genere, o al puro segno e al
materiale-segno, di un Malevič o di un Tatlin – si pensi all'elenco
degli «strumenti per la rigenerazione della lingua» con cui si apre il
manifesto del 1913[^4] – sono, contemporaneamente, conferma degli
strumenti analitici usati dai membri dell'Opojaz e contestazione della
loro universalità.

Il dissidio si rivela in tutta la sua ampiezza quando l'avanguardia
sceglie di legare il proprio destino al farsi della Rivoluzione
d'Ottobre. Quando essa, in altre parole, sceglie come propria la
dimensione della «progettazione», quando trasforma – in letteratura come
in poesia, in architettura, in pittura o nel cinema – la propria
negazione in proposta costruttiva, quando sceglie insomma di scendere
sul piano della storia.

E' tale scelta soggettiva, che vede il realizzarsi dell'avanguardia come
omologo del «realizzarsi del socialismo», che segna, sin dal '17, le
tappe di una vicenda storica su cui si è recentemente avuto modo di
costruire analisi critiche altamente mistificate. E' da questa stessa
scelta che dovremo ripartire per ripercorrere i momenti di una vicenda
che è ora di far uscire dal mito.

In quanto critica all'istituzione linguistica, le tesi dell'Opojaz non
possono che confermare, anzi storicizzare al livello più alto, la
struttura stessa delle discipline artistiche.  Proprio perché il suo
sforzo è tutto concentrato nel trovare le vie di una massima efficienza
nell'organizzazione del materiale linguistico, proprio perché il suo
mettere fra parentesi tutte le questioni relative alla *genesi* dei
linguaggi e ai significati da essi convogliati è funzionale a tale
ricerca di leggi assolutamente «specifiche», il lavoro svolto dai
seguaci del «metodo formale» ottiene due effetti complementari:

a) rovescia per intero le artificiose tesi circa la *politicità*
dell'avanguardia, omogeneizzando tecniche e articolazioni strutturali
futuriste a quelle dello stesso passato che i *budetljane* aggredivano
accanitamente[^5];

b) indica una nuova condizione del lavoro intellettuale, riducendo
quest'ultimo all'uso coerente ed efficiente di un materiale linguistico
in sé indifferente all'elaborazione che di esso dovrà fare il *tecnico
delle combinazioni formali*. Che è come dire consegnare – con un atto di
*realpolitik* culturale non sappiamo quanto cosciente – tutta la
responsabilità della *scelta dei materiali* (= la struttura dei
significati) al «committente politico». (Il che è già abbastanza
esplicito nelle risposte di Šklovskij alle accuse di parte marxista).

Tale scelta, dal nostro punto di vista, ha un chiaro significato. L'arte
– scrive Šklovskij[^6] – «non è una cosa, non è un materiale, ma un
rapporto di materiali: e, come ogni rapporto, anche questo è di grado
zero. E' quindi indifferente la scala di misurazione dell'opera, il
valore aritmetico del denominatore, importante è il loro rapporto. Le
opere giocose, tragiche, universali o da camera, le contrapposizioni di
un mondo a un altro o di un gatto a una pietra sono uguali tra loro».

Ma questa è esattamente la base teorica della riduzione del «materiale»
a se stesso operata dai controrilievi di Tatlin, o dai *Merzbilden* di
Schwitters. (L'accostamento non è casuale: si confrontino le immagini
fotografiche superstiti del Merzbau di Hannover con quelle relative alla
decorazione del *Café Pittoresque* eseguita nel 1917 da Georgiy Yakulov,
Rodčenko e Tatlin)[^7]. L'oggetto al suo grado zero è il materiale puro.
Lo scontro fra i materiali è già applicazione della legge dello
«straniamento» (il *ready-made* di Duchamp ne è d'altronde la
quint'essenza), e in un controrilievo o in un *Merz* l'artista è proprio
quell'«istigatore nella rivolta delle cose» di cui parla lo stesso
Šklovskij nel caratterizzare la tecnica dello «spostamento semantico» in
Tolstoj[^8].

Nasce già, a questo punto, una difficoltà. Fino a quando l'artista guida
la «rivolta delle cose» – la majakovskijana *rivolta degli oggetti* –
diviene almeno problematico passare dallo straniamento alla produzione:
lo «straniamento» specifico di una «cultura dei materiali», come quella
di Tatlin, è, oggettivamente, *straniamento dalla produzione*.

«La gente che viene dal mare – scriverà Šklovskij per spiegare il
significato dello straniamento semantico[^9] – s'abitua talmente al
rumore delle onde che non lo sente più. Allo stesso modo raramente
udiamo le parole che pronunciamo. Noi ci guardiamo ma non ci vediamo
più. La nostra percezione del mondo si è inaridita e dissolta ed è
rimasta soltanto un puro e semplice riconoscere».

Il comportamento quotidiano va quindi «corretto» dall'arte. Va, anzi,
negato nella sua opacità e nella sua opacità e nella sua afona
omogeneità. La «folla urbana» di Baudelaire o la Cura esistenziale di
Heidegger sono viste esplicitamente come i prodotti negativi della
«condizione» tecnologica. Ed è conseguente, a tale lettura idealizzata
della reificazione prodotta dal dominio crescente dei rapporti
capitalistici di produzione, il tentativo di rispondere con una astratta
tecnica di formalizzazione.

Lo «straniamento semantico» è così identificato da Eichenbaum come la
ragione stessa dell'esistenza dell'arte: questa non ha altro compito
specifico che «sottrarsi all'uso quotidiano».

«L'automatismo quotidiano nell'uso della parola – egli osserva[^10] –
lascia inutilizzate masse di *nuances* sonore, semantiche e sintattiche,
che trovano nell'arte letteraria il loro luogo specifico. La danza si
costruisce su movimenti che non hanno nulla a che fare con i gesti
abituali. *Se l'arte utilizza il quotidiano, è solo come un materiale in
un'interpretazione imprevista, o sotto una maschera accentuatamente
deformata* (il grottesco)»[^11].

Lo straniamento semantico, il divorzio fra segno e significato, la
creazione di sistemi di segni capaci di indurre significati inediti, si
rivelano quindi, alla fine, strumenti di lotta contro l'universo
reificato del «quotidiano».  L'appropriazione del mondo tecnologico o
dei mezzi di comunicazione di massa ha quindi una seconda faccia: poiché
quell'appropriazione avviene mediante una *distorsione* (il *priëm*,
l'artificio dei formalisti) e poiché l'obiettivo ultimo di essa è il
raggiungimento delle «verità» celate dall'universo mercificato, il
percorso labirintico attraverso la forma, proposto da avanguardie e
Opojaz, si pone come tentativo di recupero dell'autenticità perduta.

La *creazione di forma* come terra promessa della vittoria soggettiva
sull'alienazione, dunque: questo è ciò che i formalisti teorizzano,
questo è il *contenuto* latente delle loro analisi «scientifiche».

Ma è chiaro che un tale rovesciamento della negazione romantica
dell'universo tecnologico, nello stesso momento in cui dichiara
l'autonomia dell'arte dalla vita, in cui asserisce la necessità di
spezzare le associazioni abituali, di rendere *strano* l'abituale,
rivela anche la sua doppia faccia:

a) da un lato si pone come accettazione della realtà tecnologica e delle
sue leggi fondamentali;

b) d'altro lato assume quella realtà solo e unicamente come *materiale
da deformare*, da assoggettare, da restituire carico dei suoi *valori
perduti*.

Ma ciò pone problemi pressoché insolubili: il Costruttivismo sovietico
oscillerà perpetuamente fra un'architettura *estranea al quotidiano* –
le ricerche di Mel'nikov e dei Golosov – e un'architettura in dialettica
*positiva* con esso: le ricerche di Ginzburg, principalmente, o di
Burov.

D'altronde, lo stesso Šklovskij aveva già affermato che «la creatività,
anche quella artistico-rivoluzionaria, è tradizionale. Una infrazione al
canone è possibile solo quando il canone esiste, l'atto blasfemo
presuppone una religione non ancora morta. Esiste una 'Chiesa' dell'arte
nel senso di una congregazione di coloro che la sentono. Questa chiesa
ha il proprio canone, creato dalla progressiva stratificazione delle
eresie. Preoccuparsi di fondare un'arte collettiva è altrettanto inutile
quanto far pratiche perché il Volga si getti nel Caspio»[^12].

In un primo tempo, quindi, fra il 1919 e il '25, quando gli analisti
dell'Opojaz indagano sulle opere di Majakovskij, di Puni o di Tatlin, il
loro sforzo principale è nel dimostrare il legame del tutto arbitrario e
artificioso istituito dalle avanguardie fra arte e politica. Šklovskij
dice, della torre di Tatlin, che è «un monumento fatto di ferro, di
vetro e di rivoluzione»[^13], ma solo in quanto la «rivoluzione» si è
estraniata da se stessa, per divenire, come il ferro e il vetro,
*materiale* linguistico. Con un significativo mutamento di rotta, nel
'23 Osip Brik è costretto a definire cosa l'Opojaz offre alla
«costruzione culturale del proletariato», asserendo che al «poeta
tecnico del suo lavoro... creatore del linguaggio al servizio della sua
classe», è necessaria una conoscenza scientifica delle leggi della
produzione artistica «al posto di una rivelazione mistica dei misteri
della creazione», mentre contemporaneamente Arvàtov si incarica di
mediare Produttivismo e metodo formale, dando a quest'ultimo
contrassegni marxisti[^14].

Per il LEF, costituitosi appunto nel '23, il compito dell'artista non è
che una sperimentazione di «laboratorio», compiuto da «operai che
eseguono una ordinazione sociale»[^15] – almeno nell'accezione di
sinistra offerta ora al «lavoro sulla parola»: e tale «costruzione della
vita» (*ziznestroenie*), attuata per mandato sociale, sfocia nella più
assoluta conferma della stabilità e immutabilità del Linguaggio come
istituzione.

Tutta l'analisi dell'evoluzione morfologica alla scienza delle strutture
segniche, tutta l'analisi della loro genesi al partito: su questo
compromesso concorderanno sia Eichenbaum che Arvàtov e Lunačarskij. Ma
ciò significa dare per scontata, e da entrambi i versanti,
l'inattaccabilità delle Istituzioni formali.

Un dare per scontato che ha fini immediati opposti, ma che possono anche
convergere. Per i formalisti – siano essi i membri dell'Opojaz che più
tardi gli architetti dell'Asnova e dell'Aru – si tratta di salvaguardare
un'area minima di assoluto controllo sulla disciplina; per la critica di
parte «marxista» si tratta di finalizzare il permanere delle ideologie,
nelle loro diverse espressioni tecniche, alla generale restaurazione
ideologica operante nell'URSS dopo il '24[^16].

Non casualmente, quindi, Šklovskij può affermare che il Futurismo,
riempiendo lo spazio tra le rime con «chiazze di sonorità transazionali
o metanoiche» porta a consapevolezza «l'opera dei secoli»[^17].

L'isolamento della parola-oggetto, la distorsione e lo slittamento
semantico, la disgregazione dei nessi logico-sintattici, la distruzione
dei canoni ritmici e metrici, la tecnica dello «straniamento», non sono
– per i teorici del formalismo – strumenti specifici delle avanguardie,
ma criteri validi in generale per l'attività di configurazione
artistica. In tal modo essi distruggono spietatamente l'ntera base
teorica del Futurismo e delle avanguardie, facendo di queste ultime solo
movimenti il cui compito storico è riportare alle sue schematiche e nude
leggi il lavoro letterario, reso, in tale opera di riduzione alla sua
struttura primaria, trasparente e del tutto leggibile nella specificità
dei suoi attributi[^18].

L'alleanza tra scuola formale e Futurismo è quindi del tutto ambigua. In
realtà i formalisti avanzano la più radicale contestazione dei miti e
delle pretese «politiche» del Futurismo di sinistra. Fra Tolstoj e
Majakovskij non esiste opposizione: esiste per loro solo un diverso modo
di articolare – su differenti «materiali» – tecniche del tutto affini se
non coincidenti[^19].

Indirettamente il formalismo compie una demistificazione che – se
condotta fino in fondo e con mutati obiettivi – avrebbe potuto assumere
i connotati di una vera e propria «critica dell'ideologia» nel senso
rigorosamente marxista del termine.  E' inutile chiedersi perché ciò non
sia accaduto: la risposta è tutta nella falsa alternativa sorta fra
scuola formale ed «estetica marxista». Avanzando una teoria marxista
sull'arte, ignorando che da un punto di vista di classe non si dà altra
possibilità che di una critica marxista alle istituzioni linguistiche,
si aprirà la via a quella che sarà l'ambigua integrazione del formalismo
analitico nei metodi di produzione della forma[^20].

La poetica dell'oggetto, infatti, è nello stesso tempo una rinuncia e un
tentativo di dominio sul reale. Riconoscendo che l'universo delle
*cose*, nella sua brutale realtà, non è più sottoponibile ad
un'esperienza soggettiva che possa dominarlo con un progetto di
razionalità globale, le avanguardie europee avevano concluso il lungo
dibattito dell'epoca romantica sulla scoperta kantiana della
inconciliabilità fra l'*anima* e le *cose*.  Anche il disegno dialettico
hegeliano era stato mandato in frantumi dal *pensiero negativo*
dell'avanguardia: Futurismo, Espressionismo e Dada concordano nel
sottrarsi alla sintesi positiva delle contraddizioni esibendo queste
ultime come tali[^21].

Le avanguardie sovietiche in particolare – parallelamente alle
esperienze collaterali del Bauhaus – tendono a fondare un nuovo tipo di
dominio su un reale riconosciuto come contraddittorio, non
assoggettabile ad alcun *a priori* formale: ma per fare questo debbono
riconoscere che non è più il soggetto a fondare la realtà, ma è
quest'ultima a fondare il soggetto.  «Noi poeti futuristi – è scritto
nel manifesto *La parola come tale[^22] – ben più che alla Psiche diamo
valore alla Parola, bistrattata senza pietà dai nostri predecessori. Noi
vogliamo vivere della parola più che della nostra esperienza».  Il che
non è affatto in contrasto con il manifesto «rumorista» di Russolo
(1913), e giustamente le esperienze dei rumoristi italiani sono state
messe in relazione alle prime ricerche di Dziga Vertov[^23]. Le
*Kino-Pravda* di Vertov (cui collabora, dalla tredicesima in poi,
Rodčenko) assumono come *materiale* da riorganizzare attraverso la
manipolazione degli *intervalli* e del montaggio lo spettacolo della
città di massa, o avvenimenti distanti nello spazio, in cui l'azione
collettiva in quanto realtà nuova venga scoperta, nelle sue *verità*
nascoste, dalla legge dello straniamento[^24].

Anche qui l'esperienza soggettiva è fondata a partire dal riconoscimento
della sua impotenza di fronte agli oggetti che lo stesso soggetto ha
posto dinanzi a sé, che il soggetto ha creato, e che gli si ripresentano
come estranei. I controrilievi tatliniani, le «favole geometriche» di El
Lisickij, le *Kino-Pravda* di Vertov, sono tentativi di *gestire la
propria alienazione*; così come i planiti di Malevič erano tentativi di
sublimare l'alienazione cosmica. «La psicologia – scrive Vertov[^25] –
impedisce all'uomo di essere esatto come un cronometro, frustra la sua
ambizione di apparentarsi alle macchine. Da noi non c'è ragione perché
l'arte del movimento non consacri tutta la sua attenzione all'uomo
futuro piuttosto che all'uomo attuale. E' vergognoso che, contrariamente
alle macchine, gli uomini non si sappiano comportare. Ma cosa fare, se
il comportamento impeccabile dell'elettricità ci tocca più che il
disordine della gente attiva o l'ozio pedante di quella passiva... Noi
escludiamo per il momento l'uomo come oggetto di ripresa filmica, dato
che, questi è incapace di dirigere i propri movimenti. La nostra strada
è di partire dal sedicente cittadino per pervenire all'uomo elettrico
compiuto, attraverso la poesia delle macchine».

E' esattamente, la strada opposta – nonostante le apparenze superficiali
– a quella dell'*Esprit Nouveau*. L'uomo deve *farsi macchina* per
estinguere la propria colpa di fronte all'universo della produzione da
lui creato ma non dominato. Ma deve farsi macchina, principalmente, per
pagare cos1 il proprio scotto alla legge della produzione incessante,
della produzione per la produzione.

«Mettere a nudo l'anima della macchina, *far amare la macchina
all'operaio, far amare il trattore al contadino, far amare la locomotiva
al meccanico*» [^26].

Con questi assiomi Dziga Vertov rivela il fine ultimo dell'avanguàrdia
fattasi produttiva. E' il collettivo, la classe, ora, che è chiamata a
*farsi macchina*, a identificarsi con la produzione. Il Produttivismo è si
un progetto di avanguardia: ma è progetto di conciliazione fra Capitale
e Lavoro, operato con la riduzione della forza-lavoro a ingranaggio
obbediente e muto della macchina complessiva [^27].

Il progetto iniziale del Suprematismo o della scuola formale – il
recupero di un lavoro intellettuale opportunamente distanziato e
differenziato da quello produttivo – regge sempre meno dal '25 in poi:
lo dimostrano non solo le parole e i films di Vertov, ma anche, e
principalmente, le crisi di Šklovskij ed Eichenbaum, con le quali lo
Erlich fa iniziare, giustamente, il dissolvimento del formalismo[^28].

D'altra parte sarebbe errato dar fede per intero alle pretese
scientifiche del formalismo stesso nella sua fase più rigorosa. Anche
quell'ostinato e polemico scavare nelle strutture prime della forma,
senz'altro obiettivo apparente che la dimostrazione della fondamentale
tautologicità dell'arte, era a sua volta un «progetto» artistico. La
distanza dell'analista dall'opera non era che un pretesto: fissando un
perimetro rigoroso per il proprio scavo analitico, il formalismo
progettava in realtà il recupero di una *qualità* specifica per il lavoro
intellettuale, dando coraggiosamente per scontata l'inutilità di
quella stessa qualità di fronte alla realtà della produzione.

E' ciò che avviene anche nella vicenda culturale dei paesi occidentali.
Ma le difficoltà, nella Russia sovietica, vengono accentuate proprio dal
fatto che la dialettica interna al lavoro intellettuale e il suo
scontro con il lavoro tutto omogeneizzato, astratto, *senza qualità*,
direttamente produttivo, insistono sul *dato* insopprimibile della
Rivoluzione del '17.

Šklovskij, Eichenbaum, Tret'jakòv non riescono a convincere se stessi
della correttezza del loro «aureo» progetto di recupero di una qualità
intellettuale. Il metodo formale, dal '25 in poi, *vuole* inserirsi nella
produzione come per espiare un complesso di colpa. La letteratura viene
ora spinta a farsi *literatura fakta*, «fattografia», secondo l'espressione
dei formalisti: esattamente come per Rodčenko, la pittura deve farsi
produzione, o, come per Ginzburg, l'architettura deve farsi strumento
«sociale». Per tutta l'avanguardia sovietica, come per Dziga Vertov, il
campo da cui trarre i materiali primari della nuova arte produttiva è la
*banalità quotidiana*.

«Noi consideriamo elemento essenziale dell'arte oggi – scrive Tret'jakòv[^29] –
 il movimento dei fattografi. Respingiamo decisamente le affermazioni
sprezzanti di certi compagni del LEF: "Vorrete forse considerare *lefista*
ogni reportucolo di giornale, ogni ragazzino che sa scattare qualche
foto?" Ma è aristocraticismo estetico. La massa dei fotografi dilettanti
le migliaia di reporter e di corrispnndenti operai, nonostante la loro
bassa qualifica e il loro rigore, sono "fattografi" potenziali. Si deve
cercare di migliorare la loro qualifica, e per ogni autentica
socializzazione dell'arte costoro valgono più di qualsiasi pittore o
letterato qualificato, intento a lavorare in senso progressivo, una
volta pentito e dopo aver rinnegato il proprio passato».

La *qualità* sperimentata nel lavoro puro sulla forma deve ora entrare nei
processi di «socializzazione»: il rifiuto verbale dell'aristocrazia
culturale è tutto funzionale alla polemica ormai ingaggiata
dall'intellettuale con se stesso.

Il banale quotidiano ha la sua *forma* specifica nel giornalismo: non a
caso il progetto dei Vesnin per la «Pravda» o la grafica di El Lisickij
assumono dal *collage* cubista (con la mediazione della ricerca
tatliniana) la degradazione della forma e la sua identificazione con la
materia. «Quando diciamo che il romanzo sarà sostituito dal giornale –
scrive Šklovskij[^30] – non intendiamo dire che sarà sostituito da singoli
articoli. No, è la rivista stessa a rappresentare una determinata forma
letteraria, com'era ben chiaro all'epoca in cui nacque il giornalismo
inglese, quando si sentiva in modo cosi lampante il redattore come
autore». Ma subito dopo la «fattografia» è presentata come medium. di
«creazione collettiva», di funzione-collaborazione, secondo la
tradizione più ortodossa delle avanguardie storiche: «trovare il punto
di vista essenziale, capace di spostare il materiale e di offrire al
lettere la possibilità di ricostruirlo, ecco un procedimento molto più
organico di qualsiasi paragone, che ben raramente riesce a raggiungere
il proprio fine».

«Lo sviluppo della "letteratura del fatto" non deve seguire una
direttrice capace di avvicinarlo alla grande letteratura, bensì una
divergente, e una delle condizioni essenziali è la lotta contro
l'aneddoto tradizionale che contiene in sé, allo stato embrionale, tutte
le qualità e tutti i vizi del vecchio metodo estetico»[^31].

Non sappiamo fino a che punto Šklovskij o Tret'jakòv si accorgessero,
nel '29, che la loro scoperta della «fattografia» non era che la
riduzione aggiornata del programma Produttivista del '20, certo apparso
alle loro orecchie, a suo tempo, come vaniloquio dilettantistico[^32].

«L'arte collettiva del presente è la vita costruttiva»: nelle parole di
Rodčenko e della Stepanova, in tutto aderenti alle idee di Tatlin,
riecheggia l'utopia positiva delle avanguardie del primo dopoguerra,
dalla *Novembergruppe* al De Stijl. E del resto, lo strumento più
elementare di quelle avanguardie – la riduzione, del «materiale» formale
al puro segno, al puro oggetto, come risposta alla scoperta della
reificazione dell'oggetto in seno all'universo mercificato della
produzione – è anche lo strumento principe del Costruttivismo russo; in
tutta la gamma delle sue specificazioni.

La «*rivolta delle cose*», questo tema che, nella tradizione dell'angoscia
borghese è esperienza e prefigurazione di un mondo completamente
dominato da quel simbolo del *negativo* che è, per la falsa coscienza
intellettuale, la merce, si presenta ribaltato nelle ricerche delle
avanguardie sovietiche. L'Opojaz aveva ridotto scientificamente l'arte a
progetto di relazioni specifiche fra puri segni, fra elementari oggetti.
Il *Vešč* di Ehrenburg e di El Lisickij, l'astrazione geometrica di
Ladovskij e dei Vesnin, l'oggetto come prodotto, di Rodčenko o del
Metfak, le immagini virtuali di Dziga Vertov o di Ejzenstejn, le «cose
di Majakovskij, possono ora ripartire da quelle analisi, utilizzandole
positivamente, re inserendo le in un'ideologia della produzione che
rovesci la sospensione di giudizio compiuta dal formalismo circa la
propria collocazione politica.

La «riduzione all'oggetto» tuttavia, non è, per il Formalismo, una
conseguenza diretta della sua analisi scientifica. Essa è piuttosto la
conseguenza di un compromesso. Trasformare una tecnica di indagine in
un metodo costruttivo della forma, rovesciare nella produzione la
medesima teoria che aveva rivelato le aporie delle avanguardie
produttiviste, ribaltare il valore «negativo» dell'analisi formale in
teoria «positiva»: questo è l'equivoco cui il gruppo dell'Opojaz in gran
parte e l'ala «sinistra» dell'arte sovietica nella sua totalità non
riusciranno a sfuggire, dal '25 in poi. Si noti ancora una volta la
singolare omogeneità di funzioni storiche e di comportamenti che lega
fra loro avanguardie negative (Espressionismo e Dada) e formalismo
russo. La «riduzione all'oggetto» è il termine ultimo della loro
dissacrazione: anche se per le prime si tratta di un risultato ottenuto
tramite l'*autodegradazione* dell'attività artistica, e per il secondo di
un estremo tentativo di «salvarsi l'anima». Dada non «si traduce» in De
Stijl, la Negazione non si rovescia in Positivo attraverso un qualche
magico reagente. L'aporia dell'avanguardia è tutta qui: nel non saper
trovare altri strumenti per denunciare il proprio processo di
mercificazione, la propria incomunicabilità, che in un atto
comunicativo, anche se ridotto a pura testimonianza di se stesso, a puro
segnale. Negando la possibilità della comunicazione, l'avanguardia
rappresenta e rivela la «miseria» di una tipica utopia borghese: la
scoperta, cioè, che non è più dato altro Valore che il tendere verso
valori ineffettuali, irraggiungibili, privi di senso. Anche il
formalismo aveva distrutto – e scientificamente, per giunta – ogni
illusione circa il significato della comunicazione. Eichenbaum o
Šklovskij hanno un bell'attribuire agli «specialisti del compromesso» -
primo fra tutti Arvàtov – la responsabilità del dissolversi della teoria
scientifica della forma in banali artifici. La banalizzazione del
formalismo è già insita nel suo disperato tentativo di autorecuperarsi
alla *produzione*[^33].

«L'unità dell'opera letteraria – scrive Tynjanov nel '24[^34] – non consiste
nella perfetta simmetria delle componenti, bensì... nella loro
integrazione dinamica... La forma dell'opera letteraria va intesa
dinamicamente». La «somma degli artifici», in cui nel '21 Šklovskij
riconosceva il principio cardine del processo formale, si presenta ora
come un tutto, un sistema, una struttura. Intorno al '25 Eichenbaum
scopre che nella linea di confine tra fonetica e semantica esiste la,
sintassi: la legge dello straniamento dovrà ora insistere su un
recuperato concetto di totalità della forma. Solo che si tratta di una
forma instabile, di una struttura che ha sempre meno a che fare con uno
stabilizzato «principio di ragione»: di una forma, insomma, che è
piuttosto un tendere verso una razionalità progettante. E' esattamente
il medesimo processo che si rivela nella vicenda delle avanguardie
figurative: si pensi al passaggio dalla pura «cultura dei materiali»,
perpetuata da Ladovskij nel Vchutemas, alle prime esperienze dei Vesnin
nel '23, a quel «manifesto» di sintesi positiva che è Lo stile e
l'epoca, scritto da Ginzburg nel '24.

L 'uso di una tecnica di analisi come supporto di una metodologia di
progettazione (letteraria o architettonica che questa sia ) è già nel
Tatlin dei Controrilievi; ma è con la costituzione del LEF' che nella
Russia sovietica avviene qualcosa che solo molto più tardi avverrà
nell'Occidente capitalista. Facendo funzionare in senso produttivo
l'analisi formale, tutto il contenuto demistificante della sua analisi
viene annullato. La sua inquietante contestazione del Futurismo risulta
neutralizzata:. la scoperta della distanza ineliminabile dell'arte dalla
vita, della fatale estraneità dell'arte alla «bandiera che sventola
sopra la cittadella»[^35], è fatta funzionare come forza» trainante di
un'ideologia. L'analisi formale si cala tutta all'interno della
struttura, del processo di formazione, dei congegni di articolazione
delle poetiche che ne ereditano, quindi, il lascito «positivo».

L'arte costruzione della vita, preconizzata dal LEF, può cos1 assorbire
in sé tutti gli apporti di movimenti oggettivamente antitetici.

Il messaggio lanciato nel '23 a futuristi, costuttivisti, produttivisti
e Opojaz è estremamente significativo[^36].

L'artista «tecnico del suo Javoro» agganciato allo studio delle «leggi
scientifiche della produzione poetica» può ora recuperare il formalismo
come affossatore delle tradizioni della letteratura borghese e come
rivèlatore delle leggi della produzione poetica, mentre si profila. una
nuova utopia, quella dell'arte proletaria come creazione della società
proletaria[^37].

Il carattere tutto accessorio e sovtastrutturale delle tesi
formalistiche circa la riduzione della progettazione alla.pura
organizzazione si rivela compiutamente nel momento in cui non può più
essere sottaciuto il predicato specifico di quella generica
«organizzazione».

Rovesciare tutta la tematica della «morte dell'arte» nell'ideologia del
lavoro, funzionalizzare come stimòlo verso il rinnovamento dei mezzi e
delle tecniche di p roduzione la scoperta marxiana dei connotati di
classe delle Istituzioni e delle discipline, affermare declsamente un
possibile uso di cla.s se più insidioso quanto più raffinato l'ideologia
del «soçialismo realizzato» a quella dell'organizzazione (tettonica,
fattura, costruzione): questi i compiti che il 10 gruppo di Lavoro dei
Costruttivisti (1920) e il loro massimo teorico, A. Gan, si assumono in
proprio[^38].

Sarebbe però errato considerare chiusa, dal '20 in poi, la partita
dell'àvanguardia: questa ha solo spostato in là i propri obiettivi.
L'estraneità delle istituzioni borghesi al destinò del proletariato
rende legittima la dichiarazione di morte al feticismo dei Valori
artistici, dà un fondamento < politico» al tradizionale ruolo
distruttivo delle avanguardie storiche. La guerra alle «verità eterne e
incorruttibili»[^39] è solo il polo dialettico della ricostruzione, tutta
ideologica, del ruolo sovrastorico delle discipline: dove il cambiamento
di segno da queste assunto non trova. altra giustificazione che quella
etica.

In luogò della classe, il proleta.riato unico arbitro del proprio
destino; in luogo delle contraddizioni ineliminabili insite nella
gestione operaia del Capitale, l'esaltazione dell'organizza zione
socialista; in luogo della rivelazione dell'oggettivo funzionamento
della Russia sovietica come momento di crisi del Capitale
internazionale, abbiamo l'assunzione dell'ttobre proletario come momento
di palingenesi universale, di epifania etica. Attribuendo al
proletariato il compito storico di reintegrare l'Uomo a se stesso e al
suo ambiente sociale, il recupero di un l avoro risacralizzato in quanto
non più ' alienato si traduce direttamente nell ideologia
dell'organizzazione, nel Piano. Né conta qualcosa l'osservazione dei
«diplomatici sovietici della cultura» in Occidente – Ehrenburg ed El
Lisicki, non casualmente attaccati sia da Arvàtov che da Gan[^40] circa
la coincidenza delle linee gènerali di sviluppo capitalisti che e
sovietiche verso l'rganizzazione della produzione e la sua
pianificazione[^41].

Solo il socialismo realizzato ammette il Piano come espressione organica
del colloquio mistico fra le masse e il nuovo universo tecnologico: solo
in esso quel colloquio può significare la nuova verità della «raggiunta»
fine della divisione sociale del lavoro. La «città comunista» – Gan lo
dichiara esplicitamente, e dopo di lui lo ripeteranno fino
all'saurimento El Lisickij e tutti i tecnici europei impegnati nella
costruzione reale di quella città, da Hannes Meyer ad Hans Schmidt 4 2 -
è il luogo specifico della manifestazione sociale del Piano, come la
«città borghese» è il luogo delle contraddizioni soggettivistiche e
dell'anarchia della produzione».

L'Organizzazione è pronta ora a divenire il campo specifico su cui
l'intellettuale può fonaare le proprie speranze di riscatto e di
sopravvivenza; né manca chi, come Kusner, portando fino in fondo
l'deologia costruttivista, individua nel mito dell'rganizzazione la fine
del «feticismo della tecnica»[^43].

L'immagine della produzione come celebrazione della socialità del
lavoro, dunque. Produttivismo e Costruttivismo, ereditando, dal '23
aleno, tutte le aspirazioni troricate del Suprematismo, indicano le vie
del rovesciamento del momento col lettivo nell'organizzazione di un
lavoro produttivo in cui la classe operaia sia esplicitamente chiamata a
riconoscersi completamente. Il macchinismo celebrativo della torre di
Tatlin del '19, i garages e i clubs operai di Mel'ikov dal '25 al '30, i
progetti urbanistici di Lavinskij, di Varentsov e dell'RU – pensiamo
alla planimetria di Autostroi (1930), in cui Lavrov, Popo e Krutikov
sembrano tradurre in segno planimetrico reiterato l'equilibrio dinamico
di alcuni famosi Controrilievi tatliniani[^44], – sono le tappe di un
progressivo sganciarsi dalla realizzazione di quello stesso piano
sociale che l'rganizzarsi dellé fcirme si assume il compito di
comunicare metaforicamente. (L'avvertimento di Maj akovskij agli
studenti del Vchutemas è veramente sintomatico al riguardo, cosi come lo
è la narrazione autobiografica. della Semenova sul ruolo del
produttivismo «astratto» in seno alle scuole di architettura) 4'.

Non vorremmo che si interpretassero le nostre osservazioni come una
scontata critica rivolta al carattere puramente propositivo, simbolico,
«non realistico», delle correnti eredi dello spiritualismo suprematista
e dell'ccezione costruttivista offerta da Gabo e Pevsner. In realtà è
proprio il formalismo dei membri dell'ASNOVA, di Mel'ikov, dei Golosov,
di Leonidov, a segnare con maggiore chiarezza storica le tappe
successive in cui si realizza il «dover essere» dell'vanguardia.

Notiamo anzitutto un fenomeno solo in apparenza paradossale. In nome
dell'autonomia della costruzione formale Tatlin aveva duramente
criticato il programma di addobbo monumentale delle città promulgato da
Lenin nel 1918. Nel '19 Tatlin stesso elabora il proprio «monumento»,
dimostrando involontariamente quale fosse lo sbocco logico delle teorie
produttivistiche sui materiali; nello ste::ro tempo, le avanguardie
danno il meglio di sé nel teatro, nello spettacolo di massa, nella
propaganda urbana: il ribaltamento «produttivo» dell'«arte» sulla città
diviene la soluzione «naturale» della emblematica della tecnica e delle
implicazioni libertarie ad essa attribuite. L'ideologia dello sviluppo
si cala nell'p pello al lavoro sociale, all'integrazione della classe
nel piano di sviluppo, all'identificazione della classe nello sviluppo.
Lo stesso Lenin aveva confermato la necessità per la classe di
considerarsi estranea agli strumenti di valorizzazione del Capitale
fisso. La NEP aveva sancito implicitamente l'insopprimibilità
dell'opposizione dei diretti interessi di classe al pur \ necessario
sviluppo degli strumenti e dei metodi di produ-  zione.

Non cogliere la dialettica insita nella NEP e scegliere come proprio
campo di lavoro l'organizzazione dei modi di produzione – come ideologia
pura per i formalisti, come ambiguo compromesso fra ideologia e tecnica
di intervento settoriale per gli architetti dell'OSA – fa tutt'uno, per
gli intellettuali sovietici, con il travisamento del progetto leninista
rivolto a non annullare la classe  nel piano, a liberare, anzi, il
potenziale di lotta della classe stessa, nell'autonomia della propria
estraneità allo stesso capitale sociale dentro al quale essa funge come
elemento trainante.

La doppia faccia del lavoro produttivo – tutto dentro allo sviluppo e
contemporaneamente in lotta contro di esso viene annullata dall'deologia
che identifica classe e Piano. E' proprio questo, infatti, che
Ehrenburg, El Lisickij o i Vesnin indicano con il loro immediato
implicare il proletariato nel progetto di «liberazione collettiva» da
realizzare con lo sviluppo pianificato. Non a caso gli architetti
«radicali» tedeschi, olandesi o cecoslovacchi, potranno vedere nella
Russia sovietica il paese dell'utopia realizzata, il luogo eletto
dell'rganizzazione dello sviluppo, il campo di applicazione specifico in
cui è possibile attuare ' il sognato recupero di un lavoro intellettuale
riscattato nel suo ruolo di guida – etica e tecnica a un tempo – della
Civilisation machiniste[^48].

Il saggio di Vitezslav Prochazka, pubblicato in questo stesso volume,
documenta ampiamente la tensione ideale verso l'universo dell'umanesimo
socialista», tipico delle avanguardie progressiste cecoslovacche, da
Leva Fronta in poi. Ma forse, ancor più dell'opera. dei gruppi tedeschi
e ceki, è il pro getto elaborato d a Berlage per i l mausoleo di Lenin (
1 924 ) ad esibire uno spaccato concettuale di tale identificazione
dell'URSS, da parte della cultura radicale, come il luogo specifico
della fratellanza. universale, del riscatto miilenario dalla
oppressione. Il progetto. di Berlage non a caso si struttura intorno a
due alti fari, che ripetono il motivo del precedente progetto di
«Pantheon dell'manità» (1915), elaborato come accorato appello
umanitario contro la «barbarie» della prima guerra mondiale4 1

Lenin come incarnazione del recupero dell uomo totale e del riscatto
del, «lavoro», dunque: il corpo di fabbrica laterale, echeggiante con la
sua copertura a sheds ' un edificio industriale, è esplicito nella sua
allegoria del lavoro sublimato. L'intera attesa messianica di un futuro
liberatorio, che agita la cultura olandese della fine dell'800 e del
primo ventennio del '90 0, con tutto if suo bagaglio di teorie
teosofiche mistiche e anarchiche, sembra, con il progetto di Berlage,
aver trovato un oggetto concreto. capace di soddisfare quell'attesa. Fra
il mausoleo berlagiano, la Stadtkrone di Taut, la cattedrale del futuro
di Feininger e le «città della pace» di Kampffmeyer, C. Klein e
Schmidt-Rottluff, le differctPze soo – al di là delle forme – solo
marginali.

L'oscillare ambiguo tra l'autoconfinamento nell'deologia e il diretto
intervento nell'invenzione di forme socialiste di vita dal '28 in poi si
tratterà dell'invenzione della città socialista – caratterizza il
dibattito architettonico nel periodo della NEP.

Ma il continuo insistere sul ruolo della letteratura o del cinema come
contributi all'organizzazione e alla «costruzione delle coscienze», o su
quello dell'architettura e della città come immagini sociali di una
pianificazione possibile, rivela a ben vedere una profonda inquietudine.

Il ruolo dell'ideologia e dell'utopia, dal periodo della NEP in poi, non
può più essere mis tificato. Quanto più l'rchitettura salva se stessa
come disciplina, quanto più tenta la salvaguardia del proprio ruolo
istituzionale, tanto più essa gettualità del prodotto di massa e, il
destino del tecnico che ne guida il ciclo di produzione vengono legati
insieme, in modo non dissimile a quanto faranno Benjamin e Dorner ' 2.
Non solo rispetto alle condizioni reali dell'industria sovietica degli
anni '20, ma anche nei confronti della situazione storica più generale
dell'organizzazione produttiva, quell'invocazione, quella scelta
soggettiva di proletarizzazione, appare l'ultima prefigurazione
possibile del Geist borghese. Se ora la Totalità è il proletariato, se è
questa la classe che porta a compimento la filosofia dialettica tedesca
- è il tema costante, del resto, delle pagine più mistificanti di Engels
  e del giovane Lukàcs se è solo nel processo di costruzione
  dell'niverso socialista che sembra darsi il nuovo, unico Valore,
  allora non rimane che scavalcare i tempi, scongiurare il momento
  dell'estinzione reale, oggettiva, del lavoro intellettuale e della sua
  trasformazione in lavoro tecnico produttivo, anticipare con una scelta
  soggettiva esattamente questo momento: scegliere insomma la propria
  proletarizzazione per poterne gestire completamente i modi, la
  qualità, i fini generali.

Che è come dire, per il lavoro intellettuale, mantenere inalterato il
proprio ruolo di coscienza del mondo, di anticipatore profetico, di
gestore dei fini etici dell'mrianità ( ora, nell'URSS, dell'umanità
liberata dalla Rivoluzione d'Ottobre). E' difficile non vedere in tale
scelta, chiarissima in tutti i documenti delle avanguardie sovietiche,
il pieno esplicarsi del ruolo specifico dell'ideologia borghese nel
senso più pieno del termine. Né ci faremo ingannare, in tale
riconoscimento ( come tanta parte della critica occidentale e sovietica
recente ), dal fatto che quell'ideologia insista ora su uno spazio
storico cambiato di segno. Si può seguire agevolmente il processo
storico delle ideologie artistiche sovietiche nella duplice vicenda che
fra il '28 e il '30 segna l'estinzione dei ruoli soggettivi datisi dai
due gruppi dei Razionalisti raccolti nell'OSA e intorno alla rivista
«SA» e dei Formalisti – non solo i membri dell'ARU, l1' ''. anche, e
principalmente, Mel'nikov e Leonidov.

Di fronte al tema della riorganizzazione delle città, Ginzburg o
Sabsovic non possono rispondere – come, del resto, Ochitovic o Barse -
che con proposte d'invenzione soggettiva di «città socialiste»: sarà
Kaganovic, nella sua relazione al Plenum del c.c. del 15 giugn'o 1931 a
ricordare come non esista spazio colmabile mediante prefigurazioni
soggettivé, nella gestione dell'conotnia socialista[^53]. L'intervento
degli architetti ed urbanisti stranieri, dal '29 in poi, si rivela così
come frutto di una scelta politica conseguente. La gestione
socialdemocratica della città, sperimentata in Germania dà! '24 in -
poi, può essere ora utilizzata come utile supporto tecnico allo sviluppo
regionale fissato dal primo piano quinquennale. Gli sbocohi – che avrà
il lavoro di May, di Hannes e Kurt Meyer, di Hebebrand, di Stam, di
Schmidt, nell'Unione Sovietica', saranno studiati in modo
particolareggiato in questo stesso volume e pertanto possiamo per ora
tralasciarne l'analisi. Ai nostri fini è sufficiente sottolineare che è
proprio nel loro presentarsi come puri tecnici del Pi!tno, forniti del
mio, nimo ideologico ammissibile (o meglio, capaci di dare una veste
tutta. tecnica al loro minimo di ideologia ), che May o Schmidt
rivelano, storicamente, il reale volto sòvJ,:astrutturale del dibattito
interno ai gruppi di ricerca russi.

Sovrastrutturalità che potrà avere ancora, certo, un suo ruolo: come
propaganda. E non ' si è mai data propaganda che abbia. scelto
autonomamente gli oggetti di cui co.p.sigliare la fruizione. Alla luce
di tali considerazioni acquista 'un. nuovo significato quello che. è
stato acutaente chiamato il «recu7 pero del Suprematismo» da parte
-delle ' avanguardie russe tra il '27 e il '33 54

Nel '27 Aleksei Gari, il più accariitQ lassertore, nel '22, dell'
estinziqne dell'arte, scrive – e proprio su «SA» – le sue «Note su
azimir Malevié», in cui il consueto attacco ai formalisti e alla ASNOV
A, si, traduce. in un, progetto di recupero, delle «composizioni.
suprematiste astratte» come promotrici di «un nuovo atteggiamento
psicològico-percettivo di fronte alle mass.e – volumetrico-spaziaIi»[^55]. 

Nello stesso tempo – tra il '27 e il '28 Mel'ikov, e dal '27 al '33
Leonidov – i Formalisti portano al massimo compimento la poetica dello
«straniamento semantico» dell'ggetto, letto – in chiave neosuprematista
- non come oggetto-modulo di produzione o di serie, bensì come oggetto
  autonomo inserito in una serie definibile solo in termini ' dir
  linguaggio, di articolazione geometrica. Dal planita e dal Proun,
  piattaforme di passaggio all'rchitettura e alla produzione, produzione
  e architettura riapprodano, con loro, a planiti e Proun «realizzati»,
  ma non per questo meno programmatici.

Basterebbe intraprendere una compiuta analisi strutturale degli
assemblages geometrici melnikoviani per scoprire quanto la tecnica del
montaggio sia per essi non tanto e non solo metafora della catena di
produzione, quanto piuttosto proposta di recupero di una qualità
all'interno di questa. I modi di produzione riducono oggettivamente il
rapporto ' uomomacchina a relazione astratta fra «oggetti»: il club
operaio sarà il luogo dove tale relazione dovrà ricaricarsi di
significati. E' proprio la distanza dalla fabbrica del club operaio che
permette tale recupero: lo straniamento semantico insisterà quindi sulle
immagini della catena di produzione, astratta fino ad assumere i
connotati di un insieme di puri segni geometrici. Il lavoro del «tecnico
della forma» inizierà da qui: suo compite.. non sarà quello di rendere
significanti quei segni, bensì di fàrli «scontrare» fra loro, di
assumere il loro vuoto come materiale di una comunicazione volutamente
ambigua, perché sospesa fra la pura ed ermetica affermazione di se
stessa e l'indicazione – altrettanto ermetica – di una forma come
tensione verso un inattuale principio di Ragione.

Più che nel club Rusakov, club Svoboda (detto «il sigillo per la
fabbrica «Procellaria» '28-'29. Nel primo è notevole del '28, ciò è
leggibile nel della libertà» ), o in quello (detto «dei cilindri» ), del
l'ntroduzione di un invito esplicito a una lettura in movimento, in cui
il percorso dell'sservatore, indirizzato secondo la direttrice
avvolgente delle rampe che attraversano e spaccano.trasversalmente la '
«mac china» formale, segue da vicino l'quilibrio di namico degli oggetti
geometrici che si contrappongono nella loro assoluta disomogeneità 5 6.
Nel secondo, la «distorsione», il priem, entra nella stessa definizione
dei singoli elementi: i cilindri vetrati, che si intersecano e si
raccordano solo nel «piatto» superiore che unifica la frase geometrica,
contrastano con la loro articolazione il neutro parallepipedo da cui
emergono. ( E ' la medesima esibizione di un montaggio di forme
discontinue che gli allievi di Ladovskij e Doukacaev avevano
sperimentato allo stato puro nel V chutemas, e che verrà ripreso da Ilya
Golosov, nel club Zujev o nel progetto di tea tro per Sverdlovsk).

Ciò che caratterizza tali esperienze, comunque, è – come per le
Kinopravda di Vertov ' il tentativo di estrarre un massimo di
comunicazione dal montaggio di segni privi di s ignificati intrinseci.

L'avanguardia torna su se stessa, ripercorre filologicamente le tappe
del proprio processo.

La teoria dell'ir rile vanza della dimensione semantica nel contesto
poetico ha ormai dato la mano all'utopia del Significato. Il primato
della parola-segno – o, in architettura, dell'oggetto-segno – si rivela
come giustificazione di tecniche di montaggio formale che comportino una
«tensione nella ricerca del significato». 5 7. E' esattamente quanto
accade nel cinema di Dziga Vertov, nelle ermetiche aggregazioni di
Mel'nikov e Leonidov, negli assemblages metaforici di El Lisickij.
Degradandosi volontariamente a combinazione di vuoti segni, il
linguaggio artistico recupera così l'nica stratificazione di semanticità
ormai ammissibile, e proietta la questione del proprio senso al di là di
se stesso, al di là dell'mmediato presente (proprio l'opposto di quanto
teorizzato da Tatlin, Rodčenko e dalla Stepanova nel '20 ).

Ancora una volta l'arte, in quanto puro progetto, in qu anto
anticipazione di una globalità pianificata che ' attende un significato,
in quanto istanza che dichiara implicitamente la propria impossibilità
oggettiva a riempire di senso le pro prie antlcIpazioni, cerca nel
futuro le condizioni della propria sopravvivenza. E nello stesso momento
in cui essa rinchiude l'intera questione del proprio valore in una
ipotesi, essa segna le vie della propria estinzione. L'ipotesi vale in
se stessa, recupera un proprio significato solo se sospende se stessa al
di sopra del reale: le forme in tensione e in opposizione dei garages e
dei clubs operai di Mel'nikov non alludono ad alcuna utopia, affermano
solo se stesse, al di là di ogni attributo funzionale.

Sotto questo aspetto la vicenda dell'arte russa non si differenzia da
quella dell'vanguardia nel suo complesso, altro che per la radicalità
con cui essa si sviluppa nell'Unione Sovietica tra il '20 e il '30. Il
p'ensiero negativo, questo culmine dell'laborazione dialettica borghese,
sceglie ancora una volta il terreno dell'ipotesi fine a se stessa, per
scongiurat e provvisoriamente un'eutanasia paventata come inevitabile.

Il dilemma, allo scadere degli anni '30, diviene angosciante. Rimanere
fedeli – come fanno Tatlin, Mel'ikov, i fratelli Golosov o Leonidov -
alla legge dell'ostranenie, dello «straniamento» dell'ggetto, non può
che significare il ritorno allo «slittamento semantico», teorizzato dal
giovane Šklovskij come artificio necessario all'strazione dell'oggetto
stesso dall'utomatismo della percezione. (I traslati geometrici e le
intersezioni articolate dei clubs operai di Mel'nikov, la riinvenzione
continua dei modi di articolazione degli oggetti, l'so dello
perimentalismo basato sull'associazione arbitraria delle interiezioni
geometriche nel Dom Narkontiazproma di Leonidov, ne sono solo le prove
più appariscenti). Ma con ciò si sancisce definitivamente la propria com
pleta estraneità al progetto cui tutte le avanguardie avevano teso:
l'organizzazione di un universo tipizzato, dominato dalle leggi
dell'automatismo percettivo. La distanza, alla fine, da quello che
Benjamin chiamerà l'uso «politico» della riproduzione tecnologica[^58].

Né può sfuggire che la protesta implicita nelle metafore di Mel'nikov è
tutta calata nella tradizione della negazione romantica. L:immagine del
nuovo universo della produzione
- cui alludono, malgrado tutto, i suoi «montaggi» formali
- ha come fine l'esorcizzazione di quello stesso universo, si, risolve
  in un 'ipotesi, data in partenza come ineffettuale, circa la
  possibilità di un dominio soggettivo e intellettuale su di esso.

Al di là delle autogiustificazioni programmatiche ciò ha un solo
significato. L'arte – riproponendosi come tale scopre di poter
sopravvivere solo tornando ac;l estraniarsi dalla «bandiera che sventola
sulla cittadella». Anche per i formalisti, dopo il '30, non rimane che
la poesia come atto privato, ermetico, improduttivo, àl limite
incomunicabile.

La stessa teoria dell'irrilevanza semantica nel contesto formale implica
la disponibilità delle strutture geometriche a significati
predeterminati: ed è innegabile che gli artifici combinatori di Mel'ikov
sono – al di là del loro macchinismo generieo – oggetti tanto più
disponibili quanto più concentrati sulla specificità del loro autonomo
costruirsi.

Un solo passo avanti e otterremo il Letatlin, l'uomomacchina volante
elaborato da Tatlin, nel 19;3 0-'31, nel monastero Novodevicky di Mosca.
Nel Letatlin c'è tutto: la mistica della macchina e l'stanza di un
dominio «umano» su di essa, la tensione verso il futuro e il naufragio
nell'arcaico, l'ansia produttiva e il recupero di un'incolmabile
distanza dalla produzione  9. Ma c'è, principalmente, la scoperta
dell'nattualità della stessa avanguardia. Per sopravvivere non c'è che
tornare alle origini, accettare di giocare di nuovo, su un filo da
equilibrista, una partita con la storia in bilico tra l'utile e il
ridicolo. La distanza – per il Tatlin del '30 fra l'quilibrista e il
clown è minima.

In questo tornare al banale di un'arte che nonostante tutto si ostina a
volersi presentare come produttiva, è inutile leggere fasi biomorfiche
della poetica tatliniana o profezi[^60]. bilanciate da un profondo
disincantamento

Il gesto-buffonata dei primi budetljane si ripresenta con il Letatlin
privato di aggressività. Comunicando inequivoca bilmente che il vuoto
oggettivo che esso occupa è l'eco, la rappresentazione, l'evocazione del
deserto scoperto nel '13 da Malevic: con la differenza che laddove il
deserto di Malevic era un campo vuoto, da colmare di infinite forze
potenziali, quello del Letatlin è il simbolo del definitivo «addio al
mondo» dato dall'avanguardia. Un addio che è tuttavia tutto scontato
nelle proprie premesse e che era già contenuto nel coerente rifiuto
della cosiddetta «ala destra» del Costruttivismo o dei primi saggi
dell'Opoj az a risolvere l'arte nella produzione.

E' proprio la «macchina inutile» tatliniana che rende evidente la
fondamentale ambiguità dell'Avanguardia: una ambiguità che
l'architettura ha avuto il'compito storico di coprire piuttosto che di
rivelare.

Il Formalismo aveva visto giusto: il soggiacere al reale, alle cose,
alle parole-cose del Futurismo, era in realtà un diabolico artificio per
sfuggire al dominio del reale. Lo straniamento, la deformazione del
dato, non è che l'ltimo progetto di dominio soggettivo del!'anima sul
mondo attraverso le forme.

Assumendo i connotati della macchina – e di una macchina particolarmente
carica di valori simbolici, come l'uomo volante – ed estraniando
quest'ultima dalla concretezza della produzione, Tatlin torna
all'origine. Il pathos cosmico del Suprematismo, nella sua tensione
verso la conquista di una riconciliazione globale dell'uomo con
l'universo, si trasforma
- nel Letatlin progettato nell'RSS del primo piano quinquennale
  all'interno di una torre monastica[^61] – in un mònito ironico e
  disincantato ad un tempo, che sancisce,.insieme al totale recupero
  dell'estraneità dell'arte alla vita, la nullità di se stessa, in
  quanto groviglio di cuntraddizioni soggettive tanto prive di soluzioni
  quanto del tutto ineffettuali.

Il ritorno dell'arte a se stessa e il suo estinguersi reale,
verificabile in tutte le più significative esperienze dell'architettura
sovietica alle soglie degli anni Trenta, non presenta assolutamente
nulla di «eroico», nulla di imposto dall'esterno. Il Kitsch staliniano è
solo una cartina di tornasole che chiarisce un destino storico che
accomuna l e avànguardie sovietiche a quelle occident aÌi, e che in
quanto tale attende di essere ristudiato.

E' piuttosto significativo che nelle recenti rivalutazioni delle tesi
«eroiche» del Costruttivismo russo, si eviti accura. tamente di
osservare come 't utto quell'nticipare ideologicamente, metaforicamente,
simbolicamente, il momento «necessario» dell'rganizzazione produttiva,
del funzionamento del ciclo economico complessivo come «macchina 
globale, non trovasse altra giustificazione che in ragioni tutte immerse
in una visione palingenetica ed etica del «mondo nuovo». In tutto
quell'gitare il vessillo della «conoscenza», dell'indagine analitica,
della ricerca formale come scavo nelle strutture logiche del reale,
nessuna indagine oggettiva si è preoccupata di mettere in luce il
significato ultimo dell'organizzazione e della razionalizzazione del
lavoro in sé, come modi del controllo e del dominio del capitale sociale
sui movimenti di classe. L 'ideologia del lavoro, offerta e riprodotta
dagli intellettuali, proprio per la connessione tutta mistificata da
essi istituita fra il volto sociale della classe operaia e la rior
ganizzazione della divisione sociale del lavoro, fra,clàsse e piano
produttivo, non può che assumere  un ruolo celbrativo della
«costruzione del Socialismo»: tanto da poter capire, in questa chiave,
le insofferenze di un Gan di un Pasternak o di un Majakovskij nei
confronti del lucido esperimento della NEP.

Ma è chiaro che non è solo l'alleanza tutta verbale fra avanguardie ' e
Rivoluzione che provoca l'impasse. La distanza incolmabile fra
intellettuali e processo  rivoluzionario è proprio nell'impossibilità di
quegli intellettuali a rovesciare integralmente in  analisi politicà i
loro strumenti di conoscenza, nella loro incapacità a rileggere
criticamente il ruolo con solidato delle loro discipline. Scoprire il
ruolo di strumento di valorizzazione del capitale, proprio di
quell'organizzazione del piano da loro agitato come asse ideologico, ri
scoprire marxianamente l'pposizione ineliminabile. fra  capi tale e
lavoro, rifiutarsi di agitare, nella prOleZlOne sociale dei rapporti di
produzione, la mitologia della raggiunta sparizione della divisione
sociale del lavoro: sono questi 1 compiti politici cui gli intellettuali
russi si sottraggono storicamente. Cosi, da rendere lampante che la loro
Rivoluzione aveva ben poco a che fare con quella bolScevica. Come per il
Lukàcs di Storia e coscienza di classe, come per Karl Korsch, come per
tutto il Linkskommunismus europeQ o per gli intellettuali «radicali»
della Germania di Weimar, la rivoluzione è il compimento, per loro, del
compito storico del proletariato: la restaurazione della categoria della
totalità, la distruzione immediata e soggettiva della reificazione, il
recupero dell'integralità dell'Uomo come Sp'irito hegeliano finalmente
autorealizzato. Confondendo la Rivoluzione bolscevica con la Rivoluzione
etica auspicata per tutto il corso dell'800 dagli intellettuali
«dissidenti», l'intelligencija russa rivela come in tutta la sua volontà
di costruzione del «mondo nuovo» per l'omo nuovo si celi il permanere
dell'antico sogno dell'intellettuale europeo: porsi come guida morale
dell'rganizzazione di classe. Perché nient'altro significa quel continuo
affrontare il tema della costruzione del volto sociale del potere
proletario, che far passare sopra generici slogans politici il preciso
obiettivo di una ristrutturazione globale della divisione sociale del
lavoro, in cui l'elaborazione intellettule continui ad assumere un
ruolo privilegiato.

Con il che si scopre che è proprio nelle autogiustificazione politiche
dell'ASNOVA, dell'OSA, dell'ARU,  nella riorganizzazione disciplinare
compiuta nel Vchutemas e nel Vchutein, nei tentativi di definire
formalmente l'assetto della «città socialista», che gli intellettuali,
al di là di tutte le loro pur reali differenziazioni, offrono il loro
formidabile appoggio all'ideologia del socialismo realizzato. Tanto, che
è legittimo chiedersi se nella vicenda che vede quel socialismo
fagocitare le ideologie che ne avevano facilitato la crescita, sia da
vedersi una tragedia o non piuttosto una «necessaria ironia della
storia».

<!--
vim: spelllang=it spell
-->
