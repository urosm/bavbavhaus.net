---
title: "Manfredo Tafuri, Avstromarksizem in mesto: 'Das rote Wien'"
...

<!-- # 1. Le premesse ideologiche -->

<!-- Al disinteresse che la storiografia dell'urbanistica moderna ha
tradizionalmente riservato all'episodio storicamente eccezionale della
gestione socialdemocratica di Vienna, fra il 1920 e il 1933, si va
sostituendo da qualche tempo un rinnovato filone di studi, tesi a
rivalutare il valore specifico di quell'esperienza[^1].

[^1]: Nei manuali di Storia dell'architettura moderna di Hitchcock, Zevi
    e Benevolo (prime edizioni) l'Operazione urbanistica del Comune di
    Vienna viene praticamente ignorata, non rientrando nello schema
    ideologico dei loro autori. Un accenno ad essa è nel volume di P.
    Lavedan, *Histoire de l'Urbanisme*, Paris 1952, vol. III, p. 369 ss.
    Come è noto, Edoardo Persico aveva legato la resistenza operaia al
    nazismo, opposta nel Karl-Marx-Hof, alle origini "liberatorie"
    dell'architettura moderna, alimentando una equivoca lettura
    "antifascista" del complesso viennese. Cfr. E. Persico, *Profezia
    dell'architettura*, conferenza, Torino, 21 gennaio 1935, pubblicata
    più volte, ora in *Scritti d'architettura 1927--35*, Firenze,
    Vallecchi, 1968, pp. 117--125 (vedi in particolare la nota 13 di
    Giulia Veronesi).

    Per trovare una lettura documentata della politica urbanistica
    socialdemocratica a Vienna, è necessario rivolgersi a un'opera di
    storia politica come quella di Charles A. Gulick, *Österreich von
    Habsburg zu Hitler*, 5 voll., Wien 1948, trad. inglese: *Austria
    from Hausburg to Hitler*, Berkeley-Los Angeles, University of
    California Press, 1948 (2 volumi), in particolare alle pp. 407--504,
    opera cui ci riferiremo spesso, per i molti dati documentari ivi
    raccolti. Il nuovo interesse per l'esperimento socialista viennese è
    testimoniato da una serie di articoli e saggi pubblicati nell'ultimo
    decennio, fra i quali ricordiamo: Jörg Mauthe, *Der phantastische
    Gemeindebau. Polemische Bemerkungen zu einem Wiener Architektur -
    Kapitel*, in "Alte und moderne Kunst" 44, 1961, pp. 17--20; Helfried
    Koyré, *Die Entwicklung des Wiener sozialen Wohnungsbaues in den
    Jahren 1918--1938*, in "Der Aufbau" 9, 1964; Carlo Aymonino, *Gli
    alloggi della Municipalità di Vienna, 1922--1932*, Bari 1965
    (ripubblicato con poche varianti nell'introduzione al volume
    *L'abitazione razionale. Atti dei Congressi CIAM 1929--1930*,
    Padova, Marsilio, 1971, pp. 13--34); Anton Seda, *Ursachen und
    Entwicklung des kommunalen sozialen Wohnungsbaues*, in "Der Aufbau"
    1--2, 1965, pp. 34--40; H. Bobek e E. Lichtenberger, *Wien. Bauliche
    Gestalt und Entwicklung seit der Mitte des 19. Jahrhunderts*,
    Graz-Köln 1966, fondamentale analisi della geografia urbana
    viennese, di cui si veda in particolare il capitolo sullo "Sviluppo
    dell'architettura nel periodo fra le due guerre". È inoltre
    attualmente in elaborazione un volume sull'argomento da parte del
    prof. Kunze di Vienna. -->

<!-- Non è certo casuale che tali recenti "riscoperte" dell'operazione
austromarxista nell'ambito della "rote Wien" tendano quasi sempre a
ignorare l'unitarietà degli obbiettivi politici, economici, urbanistici
e ideologici di quella operazione stessa, presentandola di volta in
volta come soluzione politica di transizione o come struttura di
intervento atipica nell'ambito della gestione socialista delle città
mitteleuropee fra le due guerre. (Non senza, ovviamente, punte polemiche
tese a ricercare in essa possibili modelli "attuali" di intervento). -->

<!-- Nel presente saggio non ci sarà sufficiente individuare le ragioni
di tale recupero tardivo della mitologia della "Vienna rossa",
alimentata a lungo dalla stampa di partito e da alcuni settori della
cultura radicale europea[^2]. La ricostruzione storica di tale
esperimento di gestione socialista della città non può infatti non fare
i conti da un lato con la particolare ideologia che contraddistingue la
tradizione austromarxista, dall'altro lato con i fenomeni economici
complessivi di cui la "soluzione viennese del problema degli alloggi" è
fattore determinante. Le realizzazioni edilizie del Comune di Vienna,
rese leggendarie dalla resistenza operaia contro le camicie brune
hitleriane in esse svoltasi, risultano del resto incomprensibili se si
prescinde della tradizione *etica* dell'austromarxismo e dalla sua
politica economica.

[^2]: Cfr. Robert Danneberg, *Das neue Wien*, Wien 1930 (trad. inglese
    *The New Vienna*, London 1931), Jérôme e Jean Tharaud, *Vienne la
    Rouge*, Paris 1934; E. Persico, op. cit.; Hans Riemer, *Abunm von
    Roten Wien*, Wien, Julius Deutsch und Co. Verlag, 1947. Quest'ultimo
    volume, peraltro utile come raccolta documentaria, costituisce uno
    dei più mitici bilanci della gestione socialista di Vienna, redatto
    palesemente nella prospettiva di un rilancio politico del partito.
    "Vienna -- scrive il Riemer (op. cit., p. III) -- la prima delle
    grandi città dominate dalla classe operaia, fu anche la prima ad
    avere l'occasione di dimostrare che anche i lavoratori possono
    amministrare una grande comunità e con quali principi essi lo fanno.
    Vienna socialista ha spezzato radicalmente i metodi tradizionali di
    amministrazione comunale; l'apparato di potere fu riformato a fondo
    e fu data mano libera all'iniziativa dei funzionari, mentre imprese
    municipali e officine furono riorganizzate fissando per i loro
    prodotti il sano principio del prezzo di costo". Tale mitologia, che
    giunge fino a dichiarare che nella "Vienna rossa" è stato creato
    "ein neuer Baustil" adatto alla classe operaia, non ha ancora
    cessato di far sentire le sue influenze. Cfr. l'articolo, certo
    superato dai suoi autori, di Claudio Greppi e Antonio Pedrolli,
    *Produzione e programmazione territoriale*, in "Quaderni Rossi" 3,
    1963, pp. 94--101 (pp. 100--101 in particolare), e Massimo Scolari,
    *Hannes Meyer e la Scuola di Architettura*, in "Controspazio" 4--5,
    1970 (in particolare a p. 89). -->

<!-- È ben noto infatti come esista una relativa coincidenza fra le
teorie di Bernstein e Kautsky, e quelle dell'austromarxismo in genere, a
proposito del dibattito nei confronti del neokantismo e del
neopositivismo tedesco. -->

<!-- È Bernstein a considerare i rapporti di produzione come una forza
naturale economica (*oekonomische Naturkraft*) riconducendoli quindi nel
grande alveo della Necessità sovrastorica[^3]. Kautsky, di rimando,
sembra riprendere la distinzione fra *giudizi di fatto e giudizi di
valore*: la scienza, identificata con il "socialismo scientifico", non è
altro che "lo studio delle leggi che governano l'evoluzione
dell'organismo sociale"[^4], e come tale dovrà prescindere il più
possibile dall'ideologia.

[^3]: Cfr. Bernstein, *Die Voraussetzungen des Sozialismus und die
    Aufgaben der Sozialdemokratie*, Stuttgart, Dietz, 1889, trad. it.,
    Bari, Laterza, 1968.

[^4]: K. Kautsky, *Ethik und materialistische Geschichtsauffassung*,
    Stuttgart 1906, p. 141. -->

<!-- Nè lontano da tali posizioni è Hilferding, che osserva, nella
prefazione al suo *Capitale finanziario*, che è "concezione errata,
anche se diffusa *intra ed extra muros*, identificare senz'altro
marxismo e socialismo (...). Considerato logicamente (...) il marxismo è
solo una teoria delle leggi del divenire della società"[^5].
"Riconoscere la validità del marxismo -- egli continua[^6] -- non
significa in alcun modo formulare valutazioni, né tanto meno significa
additare una linea di condotta pratica. Poichè una cosa è riconoscere
una necessità, altra cosa è porsi al servizio di quella necessità".

[^5]: R. Hilferding, *Das Finanzkapital*, Wien 1910, trad. it., Milano
    1961, pp. 5--6.

[^6]: Ibidem. -->

<!-- La vera "miseria" della *Bernstein Debatte* e del marxismo della
Seconda Internazionale non è tanto nel "divorzio tra scienza e
rivoluzione", come vorrebbe Colletti[^7], quanto nel l'incapacità a
leggere le trasformazioni reali del capitalismo moderno alla luce di un
marxismo "non teologico".

[^7]: "Il divorzio fra scienza e rivoluzione, -- scrive Colletti -- tra
    conoscenza e trasformazione del mondo non potrebbe essere più
    completo, e in questo divorzio è tutto il carattere subalterno del
    marxismo della Seconda Internazionale diviso fra scientismo
    positivistico e neokantismo, e in questa posizione tuttavia
    internamente solidale. L'oggettivismo deterministico non riesce a
    includere il momento ideologico, il programma politico
    rivoluzionario" (L. Colletti, Introduzione a Bernstein, *Socialismo
    e socialdemocrazia*, Bari, Laterza, 1968; ora in *Ideologia e
    società*, Bari, Laterza, 1970, p. 100). -->

<!-- È pertanto determinante far risaltare come tale diffuso
riconoscimento della *naturalità* delle leggi economiche si saldi, senza
soluzioni di continuità, al recupero esplicito del "dover essere"
kantiano, in quanto "ideale etico" cui unicamente possa essere rimesso
il realizzarsi del socialismo. -->

<!-- Non è un caso che il gruppo austriaco formatosi nei primissimi anni
del '900 intorno all'associazione "Zukunft" (1903) e alle riviste
"Marx-Studien" (1904) e "Der Kampf" (1907) -- Max Adler, Karl Renner,
Rudolf Hilferding, Gustav Eckstein, Otto Bauer[^8] -- si caratterizzi,
prima che come insieme di quadri politicamente impegnati, come centro di
studiosi interessati a confrontare l'eredità "speculativa" del marxismo
con la filosofia neokantiana, con il pensiero di Stammler, Windelband e
Rickert, con gli apporti dell'economia marginalista austriaca, con le
teorie di Mach. Confrontare socialismo e neokantismo non costituisce una
semplice velleità intellettuale, quanto la conseguenza diretta di una
assoluta scissione fra il regno economico della Necessità e quello tutto
ideologico della Libertà socialista. Con un risultato del tutto
particolare, però, che differenzia in qualche modo il ceppo originario
dell'austromarxismo dalla tradizione storica della Socialdemocrazia
tedesca. La riduzione del socialismo a ideologia non si fonda ancora,
nel primo Bauer, nel progetto di ricomposizione di capitale e lavoro: ha
qui ben ragione Colletti a vedere nell'ideologia, riproposta come mondo
della "libertà etica" accanto al mondo della "necessità" naturale, una
riproduzione del dualismo kantiano tra *Müssen* e *Sollen*, tra
Necessità e Dovere[^9].

[^8]: Cfr. Yvon Bourdet, *Otto Bauer et la Révolution*, Paris, EDI,
    1968, pp. 14--15.

[^9]: L. Colletti, op. cit., p. 101. "Questo discorso -- continua
    Colletti -- in Hilferding come in Max Adler e nell'austromarxismo in
    genere, si svolge con una finezza di argomentazioni che sarebbe vano
    cercare negli scritti filosofici di Kautsky o di Plekhanov. E
    tuttavia la convinzione che possa darsi un corpo di conoscenze
    scientifiche, acquisite indipendentemente da ogni *valutazione*,
    mostra bene l'ingenuo positivismo che è alla base di quel discorso e
    la sua incapacità di intendere che il ruolo cui adempie il finalismo
    nella ricerca scientifica è, *per un certo verso*, il ruolo stesso
    della *deduzione*". -->

<!-- Nell'austromarxismo del primo '900 non c'è spazio per una
mediazione fra questi opposti. Gli intellettuali austriaci di parte
marxista sembrano voler accettare la tesi weberiana che pone una
radicale incomunicabilità fra conoscenza e vita, fra scienza e
ideologia, fra scienza e *valori*: con il risultato di vedere in questi
ultimi qualcosa di incomunicabile, se non irrazionale. (Rickert -- tanto
studiato dagli austromarxisti -- parla esplicitamente di "fuoriuscita
dal pensiero" per l'intuizione dello storico). -->

<!-- Accettando per il socialismo un puro ruolo ideologico,
riconducendolo a un'etica del tutto irrelativa alla necessità delle
vicende economiche, l'austromarxismo crede di poter superare quelle che
apparivano allora le "aporie" delle previsioni marxiane o le
"autocritiche" dell'ultimo Engels. -->

<!-- Se il *conoscere* quella Necessità (farsi coscienti della
*necessità del capitale*, quindi) è l'unica forma possibile di
oggettività, non vi è altro modo per ostinarsi ad essere socialisti che
appellarsi a un "dover essere" etico e volontaristico. È mettiamo fra
parentesi, a questo punto, quanto quella *conoscenza* delle leggi
economiche fosse reale, o quanto il richiamo al "dover essere" borghese
rendesse l'intero corpus teorico dell'austromarxismo altamente
angoscioso. -->

<!-- Lo stesso Max Adler, il rappresentante più a *sinistra*
dell'austromarxismo, scrive nel 1908 che "ogni causalità sociale corre
solo all'interno di una determinata forma teleologica, che le imprime la
natura spirituale dell'uomo, ed è quindi intrinsecamente
finalistica"[^10]; nell'uomo, quindi, "l'essere non è più uno stato
materiale, bensì è qualcosa da non considerare altrimenti che come
realizzazione spirituale, come pensiero, volontà e azione"[^11].
L'inscindibilità di base e sovrastruttura si fonda quindi, per Max
Adler, su "uno stesso e identico carattere e, precisamente, un carattere
spirituale"[^12]. È qui che Adler si incontra con Bernstein: il
socialismo è per loro un "ideale morale", posto "liberamente" di fronte
all'oggettività dell'ordinamento capitalistico.

[^10]: Max Adler, *Marx als Denker*, Berlin 1908, p. 35.

[^11]: Ibidem, p. 38.

[^12]: M. Adler, *Die Staatsauffassung des Marxismus*, Wien 1922, p. 88,
    cit. in L. Colletti, op. cit., p. 43. -->

<!-- Da tali premesse risulta comprensibile come la "teoria del la
rivoluzione lenta" di Otto Bauer si riversi in un progetto di
socializzazione della produzione che oscilla paurosamente fra la
"giustizia distributiva", un astratto riconoscimento della necessità di
una programmazione economica decentrata, e un progetto di gestione
consiliare a forte impronta corporativa[^13].

[^13]: Cfr. Y. Bourdet, *Otto Bauer* cit.: Julius Braunthal, *Otto
    Bauer*, in "Critica sociale" 15, 1963, p. 423 ss.; Herbert Steiner,
    *Am Beispiel Otto Bauers. Die Oktober-Revolution und der
    Austromarxismus*, in "Weg und Ziel", numero speciale del luglio
    1967; Norbert Lesen, *Tod und Leben des Austromarxismus*, in "Neues
    Forum" 1966, pp. 344--347; *Die Tragòdie des Austromarxizsmus*,
    ibidem, pp. 482--486; *Was bleibt vom Austromarxismus*, ibidem, pp.
    612--818; trad. it. *È morto l'austromarxismo? Bilancio di una
    concezione politica*, in "Dialoghi del XX", 1967 (*Gli intellettuali
    di sinistra tra le due guerre*, pp. 152--190). -->

<!-- Il che ha delle immediate conseguenze quando, dopo i rivolgimenti
politici del 1918, la Socialdemocrazia austriaca si trova ad assumere il
peso di una gestione diretta e indiretta dello Stato, e, più in
particolare, del destino economico e urbanistico della sua capitale.
L'intera tematica austromarxista -- con tutto il suo complesso bagaglio
ideologico -- è chiamata ora a misurarsi con problemi eminentemente
materiali, e, principalmente, ad assumersi un doppio peso: quello della
"*necessità*" capitalistica e quello del "*soggettivismo*" operaio. -->

<!-- Diviene così impossibile separare la storia della Socialdemocrazia
austriaca dalle vicende della politica di intervento sociale da essa
condotta: il partito sceglie di impegnarsi nella concreta soluzione dei
problemi che erano stati emarginati dal bilancio della politica
economica della borghesia capitalistica ottocentesca. -->

<!-- Non a caso l'amministrazione socialista di Vienna si trova
impegnata in una lunga ed estenuante battaglia, protrattasi fino al
1933, per la realizzazione di programmi che hanno tutti la loro origine
nelle tematiche affrontate nel famoso studio del Philippovic (1894)
sulla condizione delle abitazioni operaie[^14].

[^14]: "Se si lascerà che ogni cosa rimanga come è, se metteremo le mani
    in grembo -- scriveva il Philippovic -- allora lo stato dei nostri
    alloggi continuerà a far sentire i suoi spaventosi e deleteri
    effetti sulla vita, sulla salute e sullo sviluppo mentale della
    popolazione. Ogni tanto scoppieranno delle epidemie che porteranno i
    loro germi distruttivi bel al di là dei confini delle classi povere;
    i malati diventeranno un onere per la comunità; i cadaveri di coloro
    che sono morti prima del tempo leveranno il loro muto lamento e
    susciteranno nel cuore di chi vive un odio per le classi possidenti
    e per il nostro ordinamento sociale, più profondo di quello che
    potrebbe mai suscitare il più eloquente degli agitatori. I
    sentimenti morali del popolo finiranno per essere distrutti e
    soffocati dagli istinti più rudi della vita animale. Non solo un
    sentimento di umanità, la compassione per le sofferenze dei nostri
    simili, ma anche una saggia riflessione e un'illuminata *raison
    d'état* ci impongono di intervenire e di organizzare in maniera
    insensata la condizione, prima di una normale vita sia fisica che
    psichica, le case per il popolo" (cit. in Gulick, op. cit.). -->

<!-- Confrontarsi con tali tematiche in una capitale abnorme, che nel
corso del XIX secolo aveva assunto solo esteriormente le caratteristiche
di una moderna Grosstadt, mentre consolidava in realtà la propria facies
di città-simbolo del mito imperiale, ha sin dall'inizio per il partito
socialdemocratico un valore tutto politico. Il problema che esso
affronta è infatti la saldatura dei movimenti di classe a una
ristrutturazione democratica delle istituzioni: sotto tale ottica il
risanamento della situazione degli alloggi a Vienna -- tipico tema in
cui convergono interessi operai e piccolo-borghesi -- non poteva che
presentarsi come prioritario. -->

<!-- Qualsiasi ragionamento economico in senso complessivo viene di
conseguenza messo fra parentesi. Come cercheremo di dimostrare, l'unica
politica economica legata agli interventi urbanistici della
Socialdemocrazia è la difesa della competitività dell'industria di
esportazione nel mercato internazionale. È per questo che quegli
interventi scelgono di insistere sulla soluzione dei problemi lasciati
aperti dalle gestioni urbane dell'800: la negativa eredità della Vienna
speculativa balza ora in primo piano come "piaga sociale" da risolvere
con urgenza. Né può esser negato che quell'eredità costituisse un peso
di notevole incidenza: le leggi viennesi sull'edilizia, basate su un
decreto del 1883 (a sua volta ripetizione di decreti risalenti al 1868 e
1829), erano state l'espressione più compiuta di una concezione
puramente *quantitativa* del tema residenziale a livello di massa, con
tutte le premesse da cui essa ha origine: attacco sistematico al salario
reale operaio, speculazione fondiaria, accumulazione primitiva di
capitale. Va anche notato che dal 1890 al 1900 la speculazione sui
terreni poteva trarre profitti oscillanti fra il 1000% e il 1200% --
profitti che raggiungono le punte del 2400--2300% in casi particolari --
mentre la speculazione sugli affitti, grazie al sistema di tassazione
vigente e malgrado livelli di fitti che raggiungono 1/4 del salario
operaio, permettevano un ricavo pari al 20% dell'affitto lordo[^15].
Conseguenza, questa, di ulteriori levitazioni dei fitti e del fenomeno,
quantitativamente pauroso a Vienna, del subaffitto dei locali o di
singole porzioni di locale. La disastrosa situazione degli alloggi a
Vienna -- recentemente ristudiata nei suoi aspetti particolari in saggi
cui rimandiamo direttamente[^16] -- costituisce immediatamente per la
Socialdemocrazia il terreno privilegiato della sua lotta politica. Il
programma di socializzazione steso da Otto Bauer agli inizi del 1919
prende largamente in esame il problema degli alloggi e del "diritto
della casa"[^17], pamphlets e programmi di partito vengono stesi fra il
'19 e il '21 da Brod e Danneberg a favore di un diretto intervento
municipale nell'edilizia, finanziato da speciali sistemi di tassazione,
mentre l'utilizzazione dei molti decreti sulla requisizione degli
alloggi permette al Comune di Vienna un'operazione di ridistribuzione
dello stock edilizio esistente e di riassicurare la mobilità operaia --
bloccata dalle precedenti leggi di protezione degli inquilini[^18] --
attraverso un "Bollettino degli scambi e dei subaffitti".

[^15]: Il 40 % degli affitti veniva pagato sotto forma di tasse al
    governo centrale e alle municipalità, il 5--6 % andava alle spese di
    amministrazione, il 7--10 % era assorbito dalle spese di
    manutenzione. Del restante 44--48 % una metà andava ai mutui
    ipotecari, che raggiungevano più del 50 % del valore complessivo
    delle abitazioni viennesi. Il ricavato netto per i proprietari di
    case si aggirava quindi intorno al 20 % dell'affitto lordo (cfr. Ch.
    R. Gulick, op. cit.). La tassazione sui fitti, insieme alla politica
    di acquisizione dei terreni da parte del Comune, contribuì ad
    aggravare la situazione degli alloggi a Vienna: gli stanziamenti
    comunali in spese di urbanizzazione, derivanti dai proventi delle
    tasse, provocava un continuo aumento del valore dei terreni, e, di
    conseguenza, aumenti degli affitti.

    Il 17 ottobre 1911 il Consiglio comunale di Vienna approva una
    risoluzione che garantisce la partecipazione del Comune ad una
    società a responsabilità limitata per l'immediata costruzione di 250
    alloggi di emergenza per i senza tetto: il Consiglio stanzia 200.000
    corone in contanti pari a 1/3 del capitale totale necessario per
    l'operazione, si impegna a costruire opere di urbanizzazione per
    150.000 corone e decide di compensare con 4000 corone annue un
    ospedale che sarebbe stato danneggiato da tale intervento
    urbanistico. Nel corso della prima guerra mondiale, il sindaco di
    Vienna scrive che i 3000 piccoli alloggi costruiti dalla
    municipalità hanno sanato la situazione in tale settore edilizio.
    "Le critiche rivolte a questo tipo di intervento -- scrive il Gulick
    (op. cit.) -- sottolineavano il fatto che gli appartamenti erano
    stati in realtà costruiti delle imprese municipali dei trasporti
    urbani, del gas e dell'elettricità ad esclusivo vantaggio dei logo
    impiegati e che i capitali provenivano, in gran parte, dal fondo
    pensioni degli impiegati stessi (...). Tali critiche sostenevano
    anche che gli interventi in questione erano stati suggeriti dal
    desiderio di avere le case degli operai vicine al posto di lavoro,
    ragion per cui già nella Vienna di ante-guerra il fattore
    'abitazione' intralciava in qualche modo la libertà di movimento
    della mano d'opera."

    Nel 1913, infine, viene istituito un Ufficio dei piccoli alloggi
    presso il Comune di Vienna, che tuttavia non svolge attività di
    rilievo. Si noti l'insistere dei Consigli comunali conservatori sul
    tema del "piccolo alloggio".

[^16]: Oltre al Gulick, op. cit., cfr. Bobek e Lichtenberger, *Wien*
    cit.: C. Aymonino, op. cit.; e Sokratis Dimitriou, *Grosstadt Wien.
    Städtebau der Jahrhundertwende*, in "Der Aufbau" 4--5, 1964, pp.
    188--200. Cfr. anche l'interessante saggio di Werner Hagemann, *Der
    Städtebau nach den Ergebnissen der Städtebau-Austellung*, Berlin,
    Verlag von Ernst Wasmuth, 1913.

[^17]: Otto Bauer, *Der Weg zum Sozialismus*, Wien 1919.

[^18]: La protezione degli inquilini limita i diritti del padrone di
    casa ad aumentare gli affitti e a dare gli sfratti: essa è sancita
    dal decreto del 26 gennaio 1917 (riferito però unicamente ad
    appartamenti e a locali di uso commerciale che procurino un reddito
    minore di quello stabilito, nelle varie città austriache, da una
    tabella che divide le città stesse in cinque categorie), emendato
    successivamente nel 1918 e nel 1922, estendendo i diritti degli
    affittuari ed eliminando le restrizioni stabilite dal decreto del
    '17. -->

<!-- Ma non basta. L'intervento dei socialdemocratici induce a
trasformare, con legge del 15 aprile 1919, l'ex Fondo Imperiale per
l'Assistenza Alloggi in un Fondo Federale per l'Edilizia, creato con lo
scopo di rifondere i capitali non produttivi investiti nell'edilizia
sociale e di emergenza dei comuni: fondo alimentato dai contributi del
Governo Federale e da una tassa speciale gravante sui datori di lavoro.
-->

<!-- È ai nostri fini relativamente importante seguire nei loro
particolari le battaglie parlamentari sostenute sul tema dalla
Socialdemocrazia, o le vicende del Fondo Federale per l'Edilizia
(rivelatosi ben presto strumento inefficace di fronte alle condizioni
imposte dal periodo di inflazione e sostituito il 18 marzo 1921 dal
Fondo Comunale per l'Edilizia istituito dalla Municipalità socialista di
Vienna)[^19]. Ciò che maggiormente ci interessa è valutare le ragioni
politiche della scelta socialdemocratica, evitando di accettare
passivamente come oggettivo -- come è stato quasi sempre fatto finora --
un programma economico-sociale che nelle condizioni dell'Austria uscita
dalla prima guerra mondiale privilegia senza remora alcuna la "questione
delle abitazioni".

[^19]: In realtà, il provvedimento che dà l'avvio alla politica edilizia
    del Comune di Vienna è la legge sulle tasse di affitto del 10
    febbraio 1922, entrata in vigore nel maggio dello stesso anno. Con
    essa si stabilisce che i proventi della tassa sull'affitto debbono
    essere usati per la costruzione e la manutenzione di case popolari e
    di alloggi di emergenza. Si fissa contemporaneamente che il 60 % dei
    proventi vada devoluto alle spese di costruzione, il 10 % alla
    manutenzione, il 30 % agli alloggi di emergenza: di conseguenza, il
    vecchio Fondo per l'Edilizia viene abolito. -->

<!-- # 2. Dall'ideologia al programma economico -->

<!-- Un primo elemento di chiarificazione può essere offerto dal piano
di socializzazione steso da Otto Bauer nel 1919[^20]. Il problema
cardine del socialismo è per Bauer il collegamento fra una "giusta
distribuzione" e una distruzione radicale del capitale "passivo", di
origine feudale. La "costruzione della società socialista" dovrà
procedere progressivamente verso un'*organizzazione sociale della
produzione e della distribuzione* fondata su un "lavoro liberato"[^21].
La socializzazione delle fonti energetiche e delle industrie figura di
conseguenza al primo posto nel suo programma: in modo ancor più confuso
che nelle proposte di Korsch, nella sua visione della gestione
consiliare la salvaguardia degli interessi "sociali" non può avvenire
che attraverso un meccanismo di reciproco aggiustaggio regolato dalla
mediazione statale, inserita fra gli interessi dei consigli dei
produttori e quelli dei rappresentanti dei "consumatori".

[^20]: O. Bauer, *Der Weg zum Sozialismus* cit.

[^21]: O. Bauer, op. cit., capitoli I e II. -->

<!-- Anche il capitalismo democratico di Bauer giunge all'eliminazione
del "padrone" al fine di cancellare ogni instabilità nelle relazioni fra
capitale e lavoro; ma si rifiuta ad una gestione tecnica dell'impresa da
parte dei Consigli operai. -->

<!-- Ai Consigli, al contrario, è demandata l'amministrazione delle
istituzioni di immediato interesse operaio: le abitazioni operaie, gli
economati, le cucine collettive, i servizi[^22]; in altre parole le
istituzioni in grado di educare il proletariato ad assumersi i compiti
assegnati da Max Adler all'"uomo nuovo"[^23], Ed è sulle medesime basi
che Bauer affronta la questione della socializzazione dei terreni urbani
e degli alloggi.

[^22]: Ibidem, capitolo IV.

[^23]: Cfr. Max Adler, *Neue Menschen. Gedanken über sozialistische
    Erziehung*, Berlin 1924. È interessante notare che nel 1922 i
    socialdemocratici presentano una legge con cui si propone di
    istituire in ogni casa un comitato di inquilini con il compito di
    approvare o meno -- con compiti persuasori nei confronti
    dell'assemblea degli inquilini, in caso positivo -- gli aumenti
    degli affitti per la manutenzione e l'amministrazione delle case.
    -->

<!-- Dopo aver brevemente ricapitolato le origini del problema delle
concentrazioni metropolitane ed aver perentoriamente affermato che la
soluzione del problema degli alloggi spetta esclusivamente ai singoli
comuni, una volta che lo Stato abbia loro assicurato adeguati strumenti
legali, egli affronta il tema della formazione dei demani comunali. -->

<!-- Otto Bauer non nega la necessità di un esproprio socialista degli
antichi espropriatori. Ma tale espropriazione -- egli scrive -- "non
può, non deve avvenire, sotto la forma di una brutale confisca della
grande proprietà capitalista e immobiliare, dato che in tale forma essa
non potrebbe compiersi che al prezzo di una devastazione spaventosa dei
mezzi di produzione che ridurrebbe al lastrico le masse popolari ed
esaurirebbe le fonti della ricchezza nazionale. L'espropriazione degli
espropriatori deve piuttosto avvenire nell'ordine e nella regola, in
modo tale da non distruggere l'apparato produttivo della società, e da
non impedire lo sviluppo dell'industria e dell'agricoltura. Lo strumento
principale di tale espropriazione regolare può essere concentrata nel
sistema tributario"[^24]. Un socialismo basato su una ridistribuzione
*equa* della ricchezza, quindi, e sulla progressiva trasformazione del
capitalismo privato in capitalismo socializzato, sulla base di una
partecipazione azionaria dei vecchi proprietari.

[^24]: O. Bauer, op. cit., capitolo VII. -->

<!-- È comunque significativo che Otto Bauer non investa, con il suo
progetto politico, l'organizzazione della città nel suo complesso, ma si
limiti a trattare il tema della socializzazione delle aree fabbricabili
e delle residenze popolari[^25]; il che è del tutto coerente con le
premesse del suo programma di democratizzazione decentrata e isolata
nell'ambito dei singoli settori economici.

[^25]: Ibidem, capitolo IX. -->

<!-- Non va però escluso che dietro a tale disinteresse per la funzione
della città nello sviluppo economico si celassero, nel Bauer del '19,
considerazioni tutte politiche. Nell'assoluta incertezza circa il
destino nazionale dell'Austria e i possibili modi della sua integrazione
nell'area produttiva mitteleuropea, puntare su obiettivi delimitati e
controllabili corrisponde ad una tattica di transizione, che vede al
primo posto la conquista alla Socialdemocrazia dei ceti medi e
piccolo-borghesi. -->

<!-- Il principio del "diritto all'alloggio", il programma di esproprio
generalizzato dei terreni fabbricabili da parte dei comuni, con
interventi statali puramente finanziari, l'indennizzo agli antichi
proprietari mediante titoli a interesse fisso sul nuovo valore del suolo
reso comunale, sono tutti strumenti esplicitamente rivolti a tale
politica "popolare". Ne emerge comunque la più assoluta negazione del
ruolo produttivo dell'edilizia: a parte le misure contingenti tese alla
ridistribuzione dello stock edilizio esistente, sono le prospettive
indicate da Bauer a tracciare le linee di una politica urbanistica che
toccherà il suo apogeo nella "Vienna rossa". -->

<!-- "Lo Stato -- scrive Bauer[^26] -- dovrà (...) regolare il prezzo
degli affitti negli alloggi comunali: il principio cardine seguito dai
comuni sarà quello di calcolare il prezzo di affitto relativo alle case,
ai laboratori, ai negozi, in modo tale da coprire unicamente le spese di
costruzione. Essi non avranno alcun diritto ad estrarre profitti da tali
affitti. Solo le abitazioni di lusso, gli appartamenti e i negozi
favoriti nella loro localizzazione saranno affittati a prezzi più alti,
e tale profitto potrà essere impiegato per ridurre i prezzi dei piccoli
alloggi o per soddisfare i bisogni generali della città".

[^26]: Ibidem, capitolo VII. -->

<!-- Il principio bernsteiniano della "distribuzione socialista"
all'interno dei rapporti di produzione capitalistici è qui applicato in
pieno a proposito dell'economia urbana. E possiamo rilevare al proposito
quanto, all'atto pratico, nel leader politico dell'austromarxismo la
"naturalità" delle leggi economiche, tanto proclamata in astratto, si
traduca poi in programmi continuamente in bilico fra una politica
manageriale e di autogestione di stampo populista, e un progetto di
integrazione sociale in cui riemerge potentemente l'autentico strumento
originale dell'austromarxismo stesso: la socializzazione come
realizzazione della *nuova etica* della democrazia socialista. -->

<!-- Nessuna meraviglia, quindi, sul fatto che Bauer possa trascurare
con tanta noncuranza la questione dei finanziamenti dei progetti di
esproprio generalizzato e di gestione comunale dell'edilizia, o quella
dell'integrazione di tali spese sociali nell'ambito dell'economia
nazionale. Il suo vero programma, in tale settore, è esposto nella
seconda parte del capitolo relativo alla questione delle abitazioni del
suo *Der Weg zum Sozialismus*. -->

<!-- Escludendo una diretta gestione comunale delle abitazioni popolari,
Bauer ritiene "necessario far amministrare le case dagli stessi
abitanti. L'amministrazione dei diversi caseggiati sarà affidata a
comitati di affittuari che dovranno occuparsi della conservazione, della
manutenzione e della pulizia delle abitazioni". Tali comitati --
paralleli, si noti, ai comitati operai di fabbrica -- "saranno
autorizzati, al fine di alleviare il lavoro domestico, a installare nel
nuovo edificio o in settori di esso cucine centralizzate, lavanderie ed
essicatoi comuni, ambienti per il gioco e lo studio dei bambini, mense
comuni, sale di lettura e per il gioco degli adulti, e ad assumere il
personale necessario al funzionamento di tali servizi. I residenti
parteciperanno alle spese in ragione proporzionale al prezzo di affitto
da loro pagato"[^27].

[^27]: Ibidem. "In tal modo -- continua Bauer -- i servizi saranno in
    parte socializzati: molti ruoli, assolti oggi separatamente in ogni
    alloggio, saranno assolti in comune da più alloggi, sotto la tutela
    del comitato inquilini e dei suoi organi. La donna che lavora non
    dovrà più sopportare il doppio fardello della professione e delle
    cure domestiche. I bambini saranno curati meglio di oggi (...).
    Infine, grazie a tale parziale socializzazione, gli inquilini
    troveranno un interno più godibile. Mentre oggi l'operaio deve
    trascorrere il suo tempo libero nel vano che funge da cucina,
    lavatoio e stanza di gioco per i bambini, mentre oggi egli fin
    troppo spesso abbandona la sua spiacevole abitazione per correre,
    quando può, all'osteria, egli troverà nel suo caseggiato,
    immediatamente vicino al suo alloggio, sale di lettura, di gioco e
    di conversazione dove passare felicemente il tempo libero". Il
    confronto fra le tesi di Bauer e i passi di Lenin e, più tardi, dei
    teorici sovietici della casa-comune, si impone come immediato. -->

<!-- Alla democratizzazione economica corrisponde quindi la
democratizzazione della gestione delle abitazioni. Anzi, nello scritto
di Bauer -- che avrà nella politica del Comune di Vienna, dal '23 in
poi, conseguenze dirette sul piano realizzativo -- si può cogliere il
progetto politico che guida le sue proposte. Se il "diritto alla casa"
dovrà spingere le municipalità a una adeguata risposta alla domanda di
abitazioni, esercitando il proprio diritto di esproprio e costruendo
(direttamente o tramite cooperative) gli alloggi, la "gestione dal
basso" degli alloggi stessi e delle strutture relative è strumento per
una *educazione alla democrazia tramite* la socializzazione dei servizi.
-->

<!-- L'accento posto sui servizi comuni, sulla liberazione della donna
dalle schiavitù domestiche, sull'*equo canone* fissato una volta per
tutte sui prezzi di costruzione, sul valore comunitario della gestione
delle residenze da parte dei comitati di alloggio eletti
democraticamente, ha chiaramente due risvolti. -->

<!-- Da un lato il programma di Bauer rappresenta una risposta
"costruttiva" alle impostazioni sovietiche del tema residenziale[^28] --
non a caso egli prefigura un modello di casa "semi-comune" da realizzare
senza imposizione alcuna, per libera scelta degli inquilini --;
dall'altro tende a saldare i nuovi poteri attribuiti ai comuni per la
programmazione edilizia alla partecipazione popolare. Il dilemma su cui
i fautori della gestione consiliare si scontrano -- da Korsch a Bauer
stesso -- per superare l'antitesi fra l'imperativo del piano produttivo
e l'autogestione operaia della produzione stessa, della distribuzione e
del consumo, viene saltato a piè pari, estraendo forzosamente la
socializzazione dei terreni e dell'edilizia dal ciclo economico
complessivo.

[^28]: Cfr. fra l'altro: P. Martell, *La legislazione sugli alloggi
    nella Russia sovietica*, in "Wohnungswirtschaft" 23, 1928, pp.
    140--143, pubblicato in italiano in appendice alla traduzione del
    volume di El Lisickij, *La ricostruzione dell'arcbitettura in
    Russia: 1929*, Firenze, Vallechi, 1969, pp. 140--144; V. Rabinovič,
    A. Rjabuscin, *V. I. Lenin o Problemach Novogo Byta i Žilišča* [*V.
    I. Lenin sui problemi del nuovo modo di vita e della residenza*] in
    "Stroitel'stvo i Architektura Moskvy" 2, 1970, pp. 1--2. -->

<!-- Aymonino ha giustamente messo in relazione il dibattito interno
alla Socialdemocrazia tedesca sul tema edilizio alle indicazioni offerte
da Karl Kautsky nel 1919, con le sue "Direttive per un programma di
azione socialista"[^29]. Per Kautsky costruzione e amministrazione degli
alloggi spettano direttamente ai comuni, lasciando alla "maturità e
all'organizzazione delle masse lavoratrici stabilire con quali mezzi
potranno essere ottenuti i risultati migliori, se cioè il comune dovrà
servirsi di imprenditori privati, ai quali imporrà le proprie
condizioni" o se sarà meglio implicare nella gestione delle imprese le
stesse organizzazioni operaie[^30].

[^29]: C. Aymonino, *La città di Berlino*, in *L'abitazione razionale*
    cit., pp. 56--27.

[^30]: Karl Kautsky, *Direttive per un programma di azione socialista*
    (1919) cit. in C. Aymonino, op. cit., p. 56. -->

<!-- Sulla base della seconda soluzione indicata da Kautsky, Martin
Wagner elaborerà a Berlino la propria teoria sulla socializzazione
dell'attività edilizia, con l'intervento combinato dei comuni e di
"società edilizie socializzate" -- si tratta, in sostanza di un primo
abbozzo di quella che sarà la politica urbanistica dell'ADGB e della
GEWOG nella Germania di Weimar. Otto Bauer, al contrario, pur elaborando
il proprio programma di socializzazione dei terreni e dell'edilizia
sulla base di uno schema teorico tangente a quello di Kautsky, evita
accuratamente -- come si è visto -- di entrare in merito al
finanziamento e alla gestione dell'attività edilizia. -->

<!-- Non a caso, infatti, la casa come spesa sociale a carattere
assistenziale si completa nel progetto tutto pedagogico della
socializzazione dei servizi e della loro gestione. È qui che la tematica
*etica* dell'austromarxismo si traduce direttamente in programma
politico. La "questione degli alloggi" è ora chiamata a divenire il
terreno su cui fondare modelli dimostrativi di *socialdemocrazia
realizzata*. È su tale base che il problema urbano nel suo complesso può
essere ignorato da Bauer: in quanto tale -- in quanto eredità "negativa"
dell'organizzazione capitalistica -- esso dovrà essere messo fra
parentesi, mentre al suo interno vengono formandosi organizzazioni
parziali di nuova "democrazia residenziale". -->

<!-- La scritto di Bauer è del '19 ed è ancora all'interno di una
prospettiva ottimistica circa la possibilità di un blocco democratico
ancorato ad un progetto di "cauta socializzazione", che prenda le sue
distanze sia dall'esperienza sovietica che dalla ideologia consiliare di
un Korsch o di un Pannekoek. Come ha notato Norbert Leser, Bauer si
riferisce poco più tardi, nel suo *Die Osterreichische Revolution*, del
1923, a un passo dell'engelsiana *Origine della famiglia*, in cui è
pronosticata una situazione "in cui le classi che si affrontano vengono
a trovarsi in una situazione di equilibrio", e dove, di conseguenza, lo
Stato cessa di essere strumento di dominio della borghesia per assumere
una funzione mediatrice, per poi riacquistare il proprio tradizionale
ruolo alla fine della "situazione di equilibrio"[^31].

[^31]: N. Leser, op. cit., pp. 164--165 dell'edizione italiana. -->

<!-- Bauer interpreta appunto l'Austria, fra l'autunno del 1919 e
l'autunno del '22, come "Repubblica popolare" in senso engelsiano,
mentre nella politica di risanamento monetario intrapresa da Seipel, con
l'appoggio del capitale straniero, Bauer leggerà la "rivincita" presa
dalla borghesia "per il 12 novembre 1918"[^32].

[^32]: O. Bauer, *Die Österreichische Revolution*, Wien 1923, 1965^2, p.
    285. -->

<!-- Eppure gli sforzi socialdemocratici per imporre una accelerata
socializzazione del capitale erano stati costantemente frustrati: nel
'19 la Socialdemocrazia è battuta nella questione della socializzazione
dell'*Alpine-Montangesellschaft*, la maggiore impresa mineraria
austriaca, la legge del 30 maggio 1919 sulla procedura di
nazionalizzazione delle imprese rimane priva di applicazione concreta, i
progetti di socializzazione dell'industria carbonifera e delle imprese
connesse alla attività edilizia cadono di fronte all'opposizione
cristiano-sociale[^33].

[^33]: Cfr. N. Leser, op. cit., pp. 165--166. -->

<!-- Uscendo nel 1920 dalla coalizione governativa, il partito
socialdemocratico fa certo i conti con il fallimento della teoria
dell'"equilibrio neutrale", ma anche, e principalmente, con
l'orientamento delle masse operaie, pesantemente critiche circa
l'operazione politica condotta dal partito a livello statale[^34].

[^34]: Ibidem, p. 168. -->

<!-- Fra l'uscita dalla coalizione e la concentrazione delle volontà
"costruttive" della Socialdemocrazia a Vienna esiste quindi una stretta
correlazione. -->

<!-- Le occasioni di tale svolta sono da un lato il dissenso
socialdemocratico sul problema dei fiduciari nell'esercito, dall'altra
la schiacciante vittoria del partito ottenuta nel '20 alle elezioni
municipali viennesi, grazie all'estensione del suffragio universale. Di
colpo il partito socialdemocratico si trova così a dover gestire una
situazione urbana fallimentare per due diversi ordini di motivi: -->

<!-- (a) per la pesante eredità tramandata dalla politica urbanistica
dell''800; -->

<!-- (b) per l'assurda caratterizzazione assunta da Vienna, una volta
avvenuto il distacco definitivo della capitale dell'ex impero asburgico
dalla Germania e dai territori produttivi che ne avevano giustificato
l'esistenza e le forme particolari di sviluppo. -->

<!-- Nell'ambito finanziario, la perdita della posizione economica di
Vienna, ganglio terziario di portata internazionale nell'ambito
dell'impero asburgico, si registra dal '18 in poi con effetti
disastrosi. I bilanci delle banche viennesi passano da un valore
superiore ai 10 miliardi di corone nel 1913 a una cifra pari a un
ventesimo di tale ammontare nel periodo dell'inflazione post bellica,
nel periodo della ripresa raggiungono solo 1/3 in più del valore
originario, per trovarsi immediatamente dopo riportati a situazioni
deficitarie: nel 1934.più di 1/4-dei lavoratori salariati risulta
disoccupato (261.650 persone con il 37,7 % di disoccupazione
strettamente operaia). L'intero sistema territoriale di organizzazione
produttiva, basato sul decentramento dell'industria pesante nei
giacimenti carboniferi della Moravia e su.vaste interrelazioni fra i
vari paesi dell'impero, frana all'improvviso dopo il '19, rendendo di
colpo la Vienna terziaria di due milioni di abitanti -- una Vienna che
aveva potuto fino ad allora negarsi come "città del lavoro" per
affermarsi come mitica città della direzione e del consumo -- una sorta
di anacronistico e improduttivo agglomerato, alla disperata ricerca di
una qualche funzione[^35].

[^35]: Alla luce di tali considerazioni va riesaminato il significato
    politico-economico delle tesi austromarxiste sull'Anschluss con la
    Germania: è a causa del veto francese all'Anschluss che Otto Bauer
    rassegna le sue dimissioni, il 26 luglio 1919, dalla carica di
    Ministro degli Affari Esteri. -->

<!-- La diminuzione degli abitanti viennesi dai 2.031.498 del 1910 a
1.841.326 del 1920 non è solo conseguenza della guerra: indica un
andamento che rimarrà costante nel periodo dell'inflazione. Il distacco
di Vienna dal suo territorio -- dalle sue funzioni originarie -- sarà
quindi solo sancito definitivamente, e non provocato, dalla schiacciante
vittoria del partito socialdemocratico alle elezioni del '20. Da questa
data al 1933 Vienna diviene un vero piccolo stato :nello stato, grazie
alla sua autonomia di governo regionale all'interno della nuova
repubblica federale: la città è pronta a divenire il banco di
sperimentazione della "democrazia socialista" teorizzata
dall'austromarxismo. -->

<!-- Che tale distacco di Vienna dall'Austria rappresentasse per il
partito socialdemocratico un risultato politico è indubbio. Isolandosi
dalla campagna e dalle piccole città, Vienna tende a circoscrivere
l'influenza dei ceti più retrivi e conservatori, assumendosi un ruolo
egemone, affidato alla partecipazione della classe operaia viennese alla
costruzione della nuova democrazia. Realpolitik e ideologia si rifersano
totalmente in tale impresa. -->

<!-- Bisognerà "realizzare" una *rote Wien*, una *Vienna rossa*, a costo
di negare -- sulla base delle condizioni storiche date, sulla base
dell'assurdo politico ed economico da queste ultime indotte -- la
funzione specifica della metropoli moderna, a costo di fare
dell'anacronismo e del carattere parassitario di una capitale che
concentra in sé un terzo circa della popolazione dell'intero paese, la
condizione stessa di un'esperienza urbanistica "eccezionale". -->

<!-- È questa l'operazione "magica" intrapresa dall'austromarxismo:
fondare sul salto all'indietro dell'intera organizzazione produttiva le
basi di una "avanzata" operaia. Revisionismo e strategia di integrazione
operaia nell'autogestione della produzione e della distribuzione
sfociano in tale utopia regressiva: esaltando l'autonomia della città
improduttiva ed esaltando la separazione della classe dal
rivoluzionamento dei modi di produzione, lotta operaia e sviluppo non
solo si scollano l'una dall'altro, ma si annullano entrambi almeno nelle
loro funzioni storiche e materiali. -->

<!-- Non a caso il problema primario della Socialdemocrazia viennese
insiste sulla questione delle abitazioni. La gravità della situazione
ereditata dall''800 era di sicuro tale da non poter essere
sottovalutata: le iniziative del primo sindaco cristiano-sociale di
Vienna, Karl Lüger, erano cadute, dal 1897 in poi, nel vuoto, mentre il
censimento degli alloggi del 1917 rivela che il 73,1 % dello stock
edilizio complessivo della città è composto di appartamenti minimi, in
condizioni igieniche e di affollamento che non hanno nulla da invidiare
alle più disastrose situazioni residenziali delle tipiche città
industriali del XIX secolo. (Si noti inoltre che lo stesso censimento
registra nei sette distretti strettamente operai di Vienna una
percentuale di tali alloggi, tipologicamente assai vicini alla
*Mieteasernen* berlinesi, del 90 % sul totale, con una incidenza dei
fitti sul salario medio operaio del 15--25 %). -->

<!-- Puntare sulla eliminazione di tale brutale attacco, tutto
ottocentesco, alle condizioni di vita della classe operaia, in una
situazione inflazionistica e contraddittoria, ha evidentemente per il
partito socialdemocratico non solo un significato politico, ma anche un
significato specificamente economico. Certo, avviare una politica degli
alloggi capace di "sanare" le distorsioni ottocentesche vale come
riaffermazione di una linea di difesa che è tutta nella tradizione del
partito. Basterebbe ricordare, oltre agli scritti di Bauer, di Brod e di
Danneberg, i falliti tentativi di far passare, in sede nazionale,
l'estensione della legge sugli espropri e una serie di provvedimenti per
la requisizione delle imprese legate all'industria edilizia. Ma
l'autentico problema politico-economico è il contenimento della
disoccupazione crescente in una fase di ristagno prevedibilmente
prolungata. -->

<!-- Impedire lo scollamento delle masse dal partito, usare una forza
lavoro generica e mezzi di produzione arretrati in un massiccio
intervento gestito dal Comune nel campo edilizio, fare di tale politica
delle abitazioni intesa contemporaneamente come volano di ripresa
economica e come affermazione del "diritto alla casa" un'*immagine* del
"lavoro socialista", sono obbiettivi strettamente allacciati fra loro.
In tal senso l'esperienza viennese diviene non solo "eccezionale", ma
anche alternativa rispetto a quella della Socialdemocrazia della
Repubblica di Weimar che opera, anche a causa dell'inflazione, in
presenza di forti tendenze verso la riorganizzazione del capitale
monopolistico e di un rapporto profondamente rivoluzionato tra città e
territorio produttivo. -->

<!-- Giustamente il Gulick osserva che nella storia europea fra le
guerre non è dato nessun altro caso in cui una questione particolare,
come quella delle abitazioni, sia stata posta, come in Austria, al
centro del dibattito politico ed economico: durante la campagna
elettorale del 1927 -- egli scrive -- circolava una battuta di spirito
che consigliava di cambiare il nome ufficiale del partito, da quello di
"partito socialdemocratico operaio", in quello di "partito
socialdemocratico degli inquilini austriaci". -->

<!-- "Per qualche tempo -- prosegue il Gulick -- la protezione degli
inquilini sembrò essere l'espediente per trasformare il partito degli
operai nel senso stretto della parola in un partito del popolo". -->

<!-- Vista sotto tale ottica, la decisione di puntare decisamente sulla
soluzione del problema "sociale" più vistoso ereditato dalle gestioni
ottocentesche ha una sua logica spiegazione. -->

<!-- È comunque assai significativo che la gestione socialdemocratica di
Vienna riprenda puntualmente la scissione fra politica residenziale e
politica urbana nel suo complesso, prefigurata da Otto Bauer. -->

<!-- Nel corso dei due piani quinquennali per l'edilizia comunale,
pressoché nessuna modificazione viene introdotta nell'assetto della
city, nella rete delle comunicazioni primarie urbane ed extra urbane,
nel sistema dei servizi a scala urbana e territoriale. Come notano Bobek
e Lichtenberger, nel corso della *Spätgrunderzeit* tutta l'attenzione si
era rivolta alla trasformazione attiva del centro cittadino e delle sue
attrezzature terziarie, mentre la "questione degli alloggi" era stata
considerata nel suo aspetto puramente e brutalmente *quantitativo*[^36].
Lo stesso progetto di Otto Wagner per una Vienna dilatabile
all'infinito, a carattere metropolitano, pone il tema residenziale sotto
il segno della più neutra "quantità"[^37]. Il nuovo stock edilizio -- di
trasformazione o di nuovo impianto -- realizzato nel centro urbano
viennese nel corso dell'amministrazione socialdemocratica si riduce così
al 3 % del totale.

[^36]: Bobek e Lichtenberger, op. cit.

[^37]: Cfr. Otto Wagner, *Die Grosstadt*, Wien 1911. -->

<!-- Si può giustificare tale fenomeno con argomenti puramente
urbanistici -- con l'alto livello qualitativo e quantitativo raggiunto a
Vienna dagli insediamenti terziari, dalla metà dell'800 al 1914 -- ma si
perderebbe così la faccia puramente politica dell'operazione. -->

<!-- La città capitalistica tardo ottocentesca e del primo '900 è
considerata dall'austromarxismo solo come persistenza ineliminabile *da
contestare*. La "nuova Vienna" non sarà una città globalmente investita
da nuove funzioni che chiamino direttamente in causa la sua
ristrutturazione generale o le sue relazioni con il territorio, ma sarà
una sorta di *città-mostra*, una sorta di gigantesca "esposizione" della
nuova *etica proletaria* arroccata negli alloggi operai. Nessun progetto
globale di "città alternativa", dunque. Solo la realizzazione parziale
-- obbligatoriamente parziale -- di un'*epica socialista*, di un'epica
della "democrazia residenziale" operaia. -->

<!-- Abbiamo così tutti gli elementi per leggere tale tipo di programma
nel quadro della tradizione teorica dell'austromarxismo. Da un lato la
città -- il regno della Necessità capitalistica --, dall'altro la scelta
soggettiva dell'etica "liberatoria" del proletariato, organizzata nelle
uniche zone urbane da esso gestibili direttamente, negli unici frammenti
di città che esso *vuole* sentire propri: nello spazio, cioè, della
riproduzione della forza lavoro. Né va sottovalutato che trasformare
l'alloggio operaio in polemica *presenza* formale, volutamente e
ostentatamente contrapposta al "negativo", alla città, ha, per tale
concezione neokantiana del "dover essere" proletario, una ragione in
gran parte affondata nella tradizione teorica engelsiana. -->

<!-- Certo, "risolvere" la questione degli alloggi nell'ambito di una
gestione tesa alla "rivoluzione lenta" socialdemocratica appare in prima
istanza antitetico alle drastiche conclusioni che lo stesso Engels aveva
dato in materia[^38]. Ma qualora si tenga presente il valore formale
delle roccaforti operaie viennesi realizzate fra il '23 e il '30,
emergono considerazioni capaci di mutare il quadro storico di
riferimento. Attribuendo una *qualità* enfaticamente "proletaria" agli
Höfe viennesi, la Socialdemocrazia tende a ostentare, a dichiarare
polemicamente alle forze che avevano oggettivamente trascurato -- dal
punto di vista capitalistico -- il tema della residenza operaia, che
quella classe operaia stessa non è solo capace di risolvere direttamente
tale *suo* problema, ma anche di restituire a se stessa un ruolo egemone
nell'ambito della "nuova democrazia".

[^38]: Friedrich Engels, *Zur Wohnungsfrage* (1887), trad. it. *La
    questione delle abitazioni*, Roma, Editori Riuniti, 1971. -->

<!-- In tal senso, sebbene parzialmente e in un solo obbligato settore
di intervento, il proletariato sembra riassumere, a Vienna, il ruolo
storico ad esso affidato dall'ultimo Engels: di "vendicatore" degli
abusi sofferti dalle classi popolari oppresse e di forza capace di
ricongiungere la *qualità del lavoro* alla *qualità umana* dei rapporti
sociali, in nome di una nuova società di *produttori coscienti*. -->

<!-- Ancora una volta le due facce dell'austromarxismo si ricongiungono.
Realpolitik e affermazione del potenziale etico del proletariato
procedono abbinati nella gestione socialdemocratica di "Vienna rossa".
-->

<!-- È a questo punto necessario, però, scendere a leggere nei
particolari i sistemi economici messi in moto dalla politica urbanistica
della Socialdemocrazia viennese. -->

<!-- Va anzitutto notato che la gestione socialista di Vienna si trova,
fra il °19 e il '25, a poter usare di tre strumenti coordinati fra loro:
il decreto sulla requisizione degli alloggi, che permette al Consiglio
municipale di Vienna di assicurarsi il controllo, nel periodo suddetto,
di 44.838 alloggi (stanze singole escluse), la riforma della legge sulla
protezione degli inquilini (7 dicembre 1922), promulgata a tempo
indeterminato, e la risoluzione approvata il 21 settembre 1923, con la
quale la Municipalità di Vienna programma un piano di costruzioni di
5000 appartamenti l'anno, pari a 75.000 abitanti in cinque anni. (Ma nel
1927 il programma viene portato a 30.000 appartamenti; cifra mantenuta
identica per il quinquennio 1928--1933). -->

<!-- Il finanziamento dell'attività edilizia municipale viene attinto
dalla nuova tassa sulla costruzione dei nuovi alloggi, a carattere
fortemente progressivo, e in genere dalla nuova politica tributaria del
Comune: dal 1 febbraio 1923 il rinnovato sistema di tassazione appare
come una vittoria ottenuta dalla Socialdemocrazia, che realizza così il
programma tributario formulato nel '21 da Danneberg e completa la
riforma della legge per la protezione degli inquilini. In tal modo, nel
1929 circa 600.000 alloggi e locali risultano soggetti alla tassa, con
un introito capace di coprire il 40 % delle spese; il restante 60 %
viene integrato da una tassa speciale e proporzionale per l'assistenza.
I finanziamenti vengono gestiti da imprese edilizie in tutto o in parte
di proprietà municipale; l'esigenza di una produzione di singoli
elementi standardizzati e di mobili moderni, capace di sostituire le
anacronistiche Wiener Werkstätten e di adeguarsi alle nuove tipologie
residenziali, viene soddisfatta mediante la creazione di apposite
cooperative di produzione, che sfruttano il legname dei boschi
circostanti la città, localizzando i centri produttivi nella corona
periferica. -->

<!-- Dal 1926 al 1933 le percentuali degli investimenti edilizi
sull'investimento totale del Comune di Vienna oscilla tra il 70 % e il
91 %, con punte massime negli anni 1926 (80,9 %) e 1932 (91,7 %): alla
fine del 1933 la città amministra complessivamente 66.270 alloggi e 3607
locali adibiti a negozi, ci- fra pari all'11 % dello stock edilizio
complessivo, ammontante, secondo il censimento del 1934, a 613.436
abitazioni[^39].

[^39]: Gli alloggi comunali vengono assegnati in affitto dall'Ufficio
    alloggi del Comune sulla base di un sistema di punteggio introdotto
    nel 1922: la cittadinanza austriaca viene valutata 1 punto, la
    residenza a Vienna fin dalla nascita 4 punti, ogni figlio al di
    sotto dei 14 anni 1 punto, ogni figlio al di sopra dei 14 anni 2
    punti, la gravidanza dopo il sesto mese 1 punto, il subaffitto 2
    punti, gravi infermità 5 punti, alloggi troppo piccoli rispetto al
    numero degli abitanti 5 punti, sfratto notificato e ammesso dalla
    legge, senza colpa dell'inquilino, 5 punti. I richiedenti con più di
    10 punti vengono inclusi nella prima categoria; tra essi vengono
    scelti i casi di emergenza cui è necessario attribuire l'alloggio il
    più presto possibile. Le persone con 5--9 punti vengono incluse
    nella seconda categoria. -->

<!-- Va inoltre osservato che la politica di reperimento delle aree
residenziali si innesta -- con segno cambiato -- su quella delle
amministrazioni anteguerra. -->

<!-- Alla fine del 1918, infatti, il Comune di Vienna, grazie ad una
costante operazione di acquisto di terreni, si trova a controllare 4690
ettari, pari al 17 % circa dell'area urbana. -->

<!-- È chiaro comunque che se per il Comune a maggioranza
cristiano-sociale tale politica urbanistica serviva a restringere il
mercato dei terreni, contribuendo a un innalzamento dei fitti, per
l'amministrazione socialdemocratica si tratta ora di gestire socialmente
tale eredità. -->

<!-- La priorità data a investimenti relativi a un bene di consumo
tipico come la casa va comunque valutata nel suo carattere di scelta
globale compiuta dalla gestione socialista. Dopo quanto si è detto non
possono, crediamo, esistere dubbi sulle finalità politiche propostesi da
quest'ultima: il contenimento della disoccupazione, il programma di
alleanza sistematica tra classe operaia e ceti medi, il legame fra
"popolo" e partito socialdemocratico sono indubbiamente obbiettivi
raggiunti dalla combattiva difesa, da quest'ultimo fatta, della
"giustizia ridistributiva" a livello residenziale. A malapena però il
partito riusciva a rispondere alle obiezioni degli economisti borghesi,
che osservavano come la protezione degli inquilini comportasse un
pericolo di "pietrificazione" per l'economia e una minaccia per la
mobilità della forza lavoro. Fino a quando le leggi di requisizione
rimangono in vigore la mobilità operaia è assicurata dal facilitato
scambio degli alloggi. Ma dopo il 1925 tale possibilità viene a cadere,
tanto da rendere inefficace persino il sistema di attribuzione in
affitto delle case direttamente finanziate dal Comune[^40], mentre
permane il fenomeno del subaffitto, malgrado l'elevata quantità di
abitazioni immessa dalla municipalità nel mercato. In altre parole, la
politica edilizia inaugurata nel 1923, venuto a mancare il supporto
delle leggi di requisizione, tende ad annullare uno dei principali
obbiettivi del piano stesso.

[^40]: Riportiamo uno specchietto relativo al mercato degli alloggi a
    Vienna tra il 1925 e il 1929:

                                             1925   1926  1927  1928  1929
    -------------------------------------- ------ ------ ----- ----- -----
    affittati scambiati con subaffittuari    9313   8379  6562  5680  5737
    successivamente affittuari              11089   4091  2186  1489   540
    subaffittuari                            2774   2012   690   403   348
    totali                                  23176  14482  9438  7572  6625

-->

<!-- Ma oltre a tali fenomeni va notato che la legge di protezione degli
inquilini viene ad avere due effetti complementari sull'economia
nazionale nel suo complesso: -->

<!-- (a) riduce la massa dei crediti circolanti in Austria,
precedentemente legati alla speculazione edilizia: lo stesso Otto Bauer,
subito citato per intero nel giornale dell'associazione industriali,
"Die Industrie", parlerà della crisi economica austriaca come "crisi di
credito"[^41];

[^41]: "È certo -- scrive il Gulick (op. cit.) -- che esisteva una
    connessione intima tra la riduzione del volume dei crediti e la
    protezione degli inquilini.Il problema era, tuttavia, quello di
    valutare la portata quantitativa di questa influenza. A questo
    proposito vale la pena di rifarsi alla dichiarazione di Gustav
    Stolper, uno dei più noti studiosi dell'economia austriaca. Egli
    stabilì esattamente l'importanza del capitale investito in case in
    seguito all'apporto di credito, estero e nazionale, scrivendo che,
    per quello che riguardava il capitale straniero, "noi possiamo
    soltanto sorridere (...). L'industria tedesca aveva potuto ottenere
    crediti ma non abbiamo sentito parlare molto di crediti sulle case
    di Berlino (...). Se, quindi, alcuni campioni della lotta contro la
    protezione inquilini giungono fino al punto di presentare una
    proposta di tassi di interesse più bassi come conseguenza di affitti
    più alti, allora ci troviamo di fronte ad un fatto ridicolo e
    puerile che può essere preso sul serio soltanto in un paese dove i
    leaders politici, quasi senza eccezione, non capiscono niente di
    economia".

    Va notato che Stolper -- nell'appoggiare l'operazione economica
    intrapresa dal Comune di Vienna -- giungerà a chiamare gli Höfe
    socialisti "il patrimonio economico nazionale che fa da barriera al
    deprezzamento delle case private". -->

<!-- (b) contribuisce a mantenere contemporaneamente il salario reale
operaio a un livello fra i più bassi del mondo: una graduatoria
stabilita nel 1924 dall'Ufficio Internazionale del Lavoro pone Vienna al
penultimo posto fra le 16 città più grandi del mondo; nel 1928 una
seconda graduatoria -- stabilita però su 18 città campione -- le assegna
il terz'ultimo posto insieme a Varsavia e Roma (indice di Filadelfia =
180; indice di Vienna = 42). -->

<!-- Combinando insieme tali due effetti si può ottenere un quadro non
mitico né moralistico delle conseguenze materiali della politica
socialdemocratica. -->

<!-- Giustamente il Gulick osserva che "l'abolizione della protezione
degli inquilini avrebbe portato ad un inevitabile aumento dei salari,
probabilmene ad un aumento maggiore di quello che sarebbe stato
l'ammontare dei fitti; a causa della pressione dei lavoratori"[^42]. Ma
non sono tanto l'aumento del costo della vita o la rottura tra
Socialdemocrazia e ceti medi le conseguenze che qui ci interessa
valutare. La politica residenziale socialista, combinata con il
contenimento artificiale dei salari operai, permette all'industria
austriaca di mantenere un basso livello di consumi interni -- compensato
politicamente dal "consumo sociale" del bene casa -- e una posizione
almeno competitiva sul mercato mondiale delle esportazioni. Condizione,
questa, del resto essenziale per il capitale austriaco, dato che il
basso livello di industrializzazione del paese, la scarsa presenza in
loco di materie prime e la carente attrezzatura tecnologica
difficilmente avrebbero permesso una presenza austriaca nel mercato
internazionale in assenza del premio di esportazione indirettamente
ottenuto dalla legge di protezione inquilini[^43].

[^42]: Ch. A. Gulick, op. cit.

[^43]: Si noti che tale politica economica basata sul contenimento del
    salario reale operaio è esplicitamente ammessa dalle pubblicazioni
    ufficiali del partito socialdemocratico. Cfr. *Die Wohnungspolitik
    der Gemeinde Wien*, Wien 1926, 1929^2, in particolare alla p. 56 ss.
    -->

<!-- Le connessioni fra il livello ideologico e il livello economico
dell'operazione austromarxista ci sembra stiano in tal modo venendo alla
luce. Mentre lo sviluppo dell'attività edilizia comunale funge da argine
politico nei confronti di una disoccupazione potenziale nel ramo più
vasto della forza lavoro austriaca concentrata in Vienna -- gli operai
edili erano da sempre stati in Austria, a parte i contadini, lo strato
di classe più numeroso -- il partito socialdemoctratico contribuisce ad
assicurare al capitale industriale condizioni di competitività
internazionale attraverso il controllo del salario reale: solo sotto
tale luce può essere correttamente letta la combattività più che intensa
con cui il partito difende a Viennae in Parlamento la propria politica,
diretta a separare dal capitale "produttivo", imprenditoriale, quello
"parassitario", improduttivo, arroccato nella difesa di privilegi
feudali, quali la rendita fondiaria urbana[^44].

[^44]: La storia relativa alle lotte parlamentari su tale tema è
    minuziosamente seguita nel vol. cit. del Gulick, ai capitoli: "La
    lotta contro la protezione inquilini e l'edilizia municipale. Prima
    fase 1922--23" e "Seconda fase: lotte parlamentari". -->

<!-- Il che può essere completato da ulteriori considerazioni. La scelta
economica socialdemocratica può essere letta in altro modo, partendo da
ciò che il partito non fa piuttosto che da ciò che esso realizza.
L'investimento massiccio nell'edilizia, infatti, in una situazione di
sottoconsumo e di scarsezza di investimenti come l'Austria post-bellica,
e in una città come Vienna, in diminuzione demografica, è tutt'altro che
una scelta obbligata. Fatti salvi i provvedimenti di requisizione e
ridistribuzione del patrimonio edilizio esistente, nessun altro
argomento che non fosse estraneo alla più fumosa etica socialdemocratica
esisteva perché le capacità imprenditoriali del Comune di Vienna, con
tutto il peso da esso rivestito nei confronti dell'intero paese, non si
riversassero in un programma di investimenti capace di fare i conti con
gli aumenti dei salari reali e le accresciute conseguenti richieste di
consumi interni. Capace, in sostanza, di accettare la combattività
operaia come elemento di sviluppo economico e di riconoscere lucidamente
il ruolo dell'amministrazione "rossa" come acceleratore potenziale di
una dinamica capitalistica. (E sia pure nell'ambito dell'ideologia di un
capitale democratico e programmato). -->

<!-- È qui che la politica economica della Socialdemocrazia austriaca
diviene suicida. All'idealismo di fondo si unisce in essa una
irresistibile vocazione a *salvaguardare nei fatti un'economia
capitalistica essenzialmente "statica"*. -->

<!-- Nessuna ragione politica contingente può essere infatti invocata
per giustificare il circolo "virtuoso" stabilito fra il mantenimento del
capitale variabile a livelli minimi -- realizzando così la profezia di
Engels[^45] -- e il premio di esportazione "nascosto" concesso al
capitale industriale. La casa come bene sociale funge infatti nella
"Vienna rossa" come contropartita di una politica di appoggio
all'industria di esportazione, nell'ambito di un programma -- anche
economicamente miope -- di equilibrio fra interessi della classe operaia
(contenimento della disoccupaizone, politica residenziale), interessi
del capitale "attivo" e interessi della piccola borghesia impoverita
dall'inflazione. La prospettiva rimane quella di un arroccamento del
partito come difensore della costituzione democratica, nell'illusione --
duramente pagata -- di raggiungere, tramite tale "politica di alleanze",
la maggioranza in Parlamento.

[^45]: Engels, in *Zur Wohnungsfrage* cit., riteneva impossibile una
    "soluzione" del problema degli alloggi come provvedimento
    settoriale, anche in considerazione di un riassorbimento, nel più
    basso prezzo di mercato della forza lavoro, dei vantaggi conseguiti:
    egli ragiona quindi mettendo fra parentesi la lotta operaia. A
    Vienna il partito socialdemocratico adotta in pieno tale
    ragionamento, gestendo in proprio il controllo dei movimenti di
    classe e facendo pagare il costo di tale contenimento -- scambiato
    con un'offerta di case -- al capitale "improduttivo" proprietario di
    case e terreni. -->

<!-- Di fronte al compromesso economico socialista non si può dire che
esistesse in Austria un lucido piano di parte capitalista. Non solo le
associazioni dei proprietari di appartamenti formeranno una delle
tendenze più estremiste dello *Heimwehr*, da loro ampiamente
sovvenzionato, ma l'intera battaglia parlamentare dei partiti
conservatori contro la protezione degli inquilini e per la
liberalizzazione dell'edilizia risulta condotta al di fuori di una
logica capace di prescindere da contingenti calcoli politici, per
toccare direttamente il tema dello sviluppo economico complessivo. -->

<!-- # 3. Il dibattito sui modelli di intervento -->

<!-- In un volume di Leopold Bauer, Oberbaurat e professore
all'Accademia di Vienna, pubblicato nel 1919, viene tracciato un
programma chiaramente alternativo rispetto a quello di Otto Bauer[^46].
In esso, all'attacco contro i programmi di socializzazione sferrato in
nome della produttività della forza lavoro, si unisce un generico piano
di riforma del regime di proprietà dei suoli. Leopold Bauer propone
quindi un sistema di tassazione sul terreno edificabile e leggi di
esproprio generalizzate che assicurino, anche in un sistema di proprietà
privata dei suoli, la libera disponibilità delle aree per una radicale
ristrutturazione urbana: il modello da lui elaborato -- tutto interno
alla tradizione ottocentesca della teoria urbanistica tedesca -- si basa
sul decentramento combinato di industrie e residenze in sistemi
integrati e autosufficienti, che nel caso di Vienna avrebbero dovuto
incunearsi, sotto forma di Siedlungen a bassissima densità edilizia, nei
boschi circostanti la città[^47].

[^46]: Leopold Bauer, *Gesund wohnen und freudig arbeiten. Probleme
    unserer Zeit*, Wien, Kunstverlag Anton Schroll und Co., 1919.
    Leopold Bauer (1872--1938) era stato allievo della Wagnerschule,
    aveva vinto il primo premio nel concorso mondiale bandito da A. Koch
    a Darmstadt per la "casa di un amatore d'arte" e aveva realizzato
    una serie di notevoli opere architettoniche fra cui la Banca
    nazionale austriaca a Vienna (1913--16). Collaborerà in seguito alla
    realizzazione del piano edilizio viennese, con lo Speiserhof (1929)
    e con il blocco sulla Wurzbachergasse (1926) (cfr. O. Uhl, *Moderne
    Architektur in Wien*, Wien 1966, passim). Già il titolo della sua
    pubblicazione del 1919 -- *Abitare in modo sano e lavorare con
    gioia* -- è sintomatico: il suo merito è nello schierarsi con
    chiarezza, senza remore e moralismi, dalla parte del capitalismo,
    letto come apportatore di civiltà e "capace di sfruttare al massimo
    la produttività del lavoro umano".

[^47]: Cfr. L. Bauer, op. cit., pp. 25--29 e il successivo capitolo sui
    "Problemi estetici dell'urbanistica", in cui l'autore si rifà
    esplicitamente alle teorie di Camillo Sitte. -->

<!-- Ma anch'egli, giunto di fronte al problema di definire il ruolo
specifico dell'economia edilizia, non sa far altro che appellarsi alla
"ricchezza" costituita dalle capacità produttive della forza lavoro, non
senza accenni a teorie consumistiche e ad appelli dichiaratamente
prefascisti[^48]. È invece significativo che Leopold Bauer contrapponga
-- come Loos, e più tardi Josef Frank[^49] -- l'ideologia della casa
operaia a bassa densità e giardino annesso per l'autoconsumo agricolo,
al modello collettivistico difeso dai programmi socialdemocratici.

[^48]: Ibidem.

[^49]: Cfr. Josef Frank, *Der Volkswohnungspalast*, in "Der Aufbau" 7,
    1926, p. 107 ss. Ma cfr. anche i molti articoli in "Der Aufbau"
    sulla tematica della Siedlung e sulla sua storia. -->

<!-- Può quindi essere motivo di sorpresa constatare come la formazione
del primo piano quinquennale di edilizia comunale viennese venga
formulato dopo un episodio di lotta operaia per la casa guidata da Adolf
Loos, in genere dimenticato dagli storici che si sono occupati del
nostro tema. Nel 1920 un gruppo di operai occupa l'ex parco imperiale di
Lainz, utilizzando il legname, ricavato dall'abbattimento degli alberi,
per procurarsi i capitali necessari alla costruzione di un quartiere
appositamente progettato da Loos: né potrà meravigliare tale exploit
politico dell'architetto della villa Steiner e della casa sulla
Michaelerplatz, qualora si consideri quanta parte l'eccitazione
populista abbia avuto nell'ideologia degli intellettuali radicali
tedeschi e austriaci, fra il '18 e il '22, nel loro sforzo di recuperare
una funzione specifica nelle mutate condizioni sociali[^50].

[^50]: Cfr. A. S. Levetus, *Solving the Vienna Housing Problem*, in "The
    Studio" 431, 1929, p. 116 ss. Può essere interessante ricordare che
    Loos, subito dopo la fine della guerra, è pronto a invocare --
    insieme a Schönberg -- un intervento diretto dello Stato nelle
    questioni artistiche, secondo un programma non lontano da quelli
    dell'*Arbeitsrat für Kunst* e della *Novembergruppe* tedeschi. È
    proprio tale progetto di un "Ufficio d'arte" che scandalizza
    Wittgenstein, che dice di aver provato "orrore e disgusto" per
    "l'aria da intellettuale snob" assunta da Loos dopo il '19. Cfr.
    Adolf Loos, *Der Staat und die Kunst*, prefazione a *Richtlinien für
    ein Kunstamt*, in "Der Friede" 62, 1919, ora in A. Loos, *Sämtliche
    Schriften*, vol. 1, Wien 1962, pp. 352--354. La lettera di
    Wittgenstein a Paul Engelmann, sopra citata, è in *Letters from
    Ludwig Witigenstein. With a Memoir*, Oxford, Basil Blackwell, 1970,
    trad. it., Firenze, La Nuova Italia, 1970 (lettera 21, del 2
    settembre 1919, pp. 12--13). -->

<!-- La risposta del partito socialdemocratico a tale pericoloso indizio
di scollamento dalle masse non si fa attendere. Il quartiere di Lainz
non verrà realizzato, ma nello stesso 1920 Loos viene assunto come
architetto capo del settore delle abitazioni del Comune di Vienna, nel
1921 si dà avvio alla costruzione di una serie di Siedlungen di limitata
dimensione[^51], nel 1923 viene votato il varo del primo piano
quinquennale. Tale equivoco avvio, in presenza, come si è visto, di
precise impostazioni ideologiche ed economiche, ma in assenza, ancora,
di una loro traduzione in precisati modelli di intervento urbanistico,
dà origine a un conflitto risoltosi, proprio nel '23, con la sconfitta
dell'ideologia della Siedlung e la vittoria del modello dello Hof, del
superblocco attrezzato: ragione sufficiente, per le opposizioni
cristiano-sociali, di ergersi in difesa delle teorie di Leopol Bauer,
Frank e Loos.

[^51]: Gli alloggi costruiti dal Comune di Vienna in Siedlungen formate
    da piccoli villini costituiscono circa il 9% dello stock complessivo
    delle nuove abitazioni. In un primo momento il Comune mette lotti di
    terreno a disposizione di cooperative organizzate tra i lavoratori
    dei servizi pubblici, anticipando l'85 % delle spese di costruzione
    e accettando garanzie ipotecarie. Il restante 15 % viene coperto dal
    lavoro materiale dei futuri inquilini.

    Più tardi il Comune decide di finanziare completamente tale tipo di
    costruzione e fonda la Gesiba, società che riceve l'incarico di
    costruire e amministrare i villini destinati anche a ceti abbienti,
    grandi in generale sui 62 metri quadrati in due piani. Dal 1928 in
    poi la superficie per villino scende a 45 m^2., per renderla pari a
    quella delle abitazioni comunali organizzate in Höfe. -->

<!-- I primi due piani quinquennali elaborati dalla Municipalità di
Vienna hanno come effetto immediato l'annullamento dell'intervento
privato nell'edilizia di massa: l'unica possibilità rimasta aperta al
capitale privato è la costruzione di case o ville di proprietà. Ma
accanto a tale tipo di intervento -- divenuto del tutto trascurabile
dopo il '19 -- va registrato un sintomatico fenomeno che entra in
diretta competizione con la politica dello Hof. Già nel 1910 era stato
creato un feudo destinato al Bund, Lega per la costruzione di case
unifamiliari a bassa densità. Attraverso le leggi del 1919 e del 1921
tale fondo viene trasformato dal Bund in uno stanziamento finalizzato a
indirizzare in forme societarie e cooperativistiche la tendenza,
estremamente pressante, alla realizzazione di Siedlungen periferiche:
alle 5.917 abitazioni organizzate dal Comune di Vienna sotto forma di
Siedlungen vanno quindi aggiunte le 4.500 similmente realizzate dal Bund
fra le due guerre. -->

<!-- Ma la creazione di un vero e proprio conflitto fra le forme di
insediamento residenziale gestite dal Comune con la realizzazione degli
Höfe e la tipologia urbanistica della Siedlung diviene particolarmente
evidente qualora si consideri l'estensione dell'edilizia periferica in
zone esterne all'agglomerato urbano, come quelle di Perchtolsdorf, di
Klosternenburg, di Marchfeld. -->

<!-- Come hanno giustamente osservato Bobek e Lichtenberger, la piccola
borghesia e vasti ceti popolari, agendo al di fuori dell'iniziativa
comunale, iniziano la disorganizzata invasione degli spazi periferici
mediante il fenomeno, tipico delle città anglosassoni, dell'allineamento
di casette unifamiliari casualmente insediate, al di fuori di ogni
struttura complessiva e di una adeguata attrezzatura di servizi. È qui
che, anche all'interno della logica socialdemocratica, si rivelano le
aporie della gestione urbana della "rote Wien". Da un lato i monumenti
proletari, gli Höfe, localizzati nelle "zone bianche" lasciate
disponibili dalla speculazione ottocentesca, ai bordi del nucleo urbano;
dall'altro un azzonamento estensivo, casuale e provvisorio di Siedlungen
unifamiliari, a sviluppo incontrollato, e realizzate in proprio dagli
stessi ceti cui si indirizza la politica residenziale del Comune[^52].

[^52]: Cfr. H. Bobek e E. Lichtenberger, op. cit., e *Die
    Wohnungspolitik der Gemeinde Wien* cit., p. 27 ss. -->

<!-- Il dualismo economico e urbanistico è certo conseguenza diretta del
rifiuto austromarxista di una gestione complessiva del fenomeno urbano:
del rifiuto, in sostanza, di leggere quel fenomeno al di fuori
dell'esaltazione ideologica. Ma quello stesso dualismo corrisponde anche
a una polemica e a un dibattito culturale. L'operazione condotta nel
1920 da Loos per la zona di Lainz, ora ricordata, e la sua assunzione
come architetto capo del settore residenziale del Comune di Vienna si
inseriscono nel tentativo di create un'alternativa globale a quella che
sarà, dal '23 in poi, la linea generale della politica municipale delle
abitazioni. La Siedlung contro lo Hof: Loos e Frank contro Behrens e
Karl Ehn. Per Loos -- e per i sostenitori dello sviluppo urbano per
Siedlungen a bassa densità -- non si tratta tanto di aderire ai modelli
che parallelamente vengono elaborati da Haesler e Taut in Germania, ma
di estendere ai ceti popolari, nelle nuove condizioni sociali, i
privilegi che nell'anteguerra lo stesso Loos aveva assegnato a una
committenza talmente *di qualità* da saper accettare quella suprema
"qualità architettonica" che è la "negazione della forma" realizzata
dalle ville loosiane tra il 1898 e il 1914. -->

<!-- Josef Frank, come molti degli intervenuti al Congresso
Internazionale di Architettura tenuto a Vienna nel 1926, sono espliciti
nella loro critica, tutta architettonica, alla politica degli Höfe[^53]:
nello stesso '26 Loos, che aveva realizzato parzialmente la Siedlung am
Heuberg, tiene a Stoccarda la sua conferenza *Die moderne Siedlung*,
dove l'individualità delle singole cellule e l'autosufficienza economica
per il sostentamento familiare vengono sostenute nell'ambito di una
lettura dell'economia urbana di stupefacente ingenuità, anche se in più
punti collimante con le tesi avanzate sette anni prima da Leopold
Bauer[^54].

[^53]: Cfr. fra l'altro *Der Karl-Marx-Hof. Die Wohnhausanlage der
    Gemeinde Wien auf der Hagenweise in Heiligenstadt*, Wien 1930. I
    resoconti del congresso del '26 sono in *Das Bodenproblem und seine
    Beziehungen zur Stadt- und Landesplanung. Auszüge aus den Referaten
    am internationalen Wohnungs- und Städtebaukongress*, in "Der Aufbau"
    10, 1926, p. 186 ss. Si veda anche il catalogo *Internationale
    Städtebau Ausstellung*, Wien 1926.

[^54]: Adolf Loos, *Die moderne Siedlung* (1926), ora in *Sämtliche
    Schriften* cit., vol. I, pp. 402--408. Vedi anche L. Munz e G.
    Künstler, *Der Architekt Adolf Loos*, Wien-München, Verlag Anton
    Schroll, 1964, p. 145 ss. Loos critica esplicitamente i modelli
    distributivi degli Höfe viennesi e delle tipologie tedesche, come il
    soggiorno-pranzo passante, mentre sembra aver assorbito parte delle
    teorie wrightiane là dove consiglia l'uso generalizzato di divisioni
    mobili o dove afferma l'infinita trasformabilità della casa. Si noti
    però che in alcune ricerche del 1923 lo stesso Loos era ricorso a
    ben differenti tipologie, come quella della casa a gradoni, vicine a
    quelle studiate da Behrens. Cfr. anche H. J. Zechlin, *Siedlungen
    von Adolf Loos und Leopold Fischer*, in "Wasmuths Monatshefte für
    Baukunst" XIII, 1929, pp. 70--78. -->

<!-- La concentrazione di masse notevoli di popolazione in complessi
attrezzati, nei quali però la parte costruita corrisponda a meno del 50
% della superficie del lotto, con ampi spazi liberi nelle corti interne
o negli spazi abbracciati dall'articolazione del superblocco, risulta
quindi vincente dal '22 circa in poi. -->

<!-- Lo Hof, il superblocco residenziale, corrisponde infatti alla
logica dell'operazione economica innescata, e va valutato in tale
quadro. -->

<!-- Il programma quinquennale del '23 richiede comunque una
riorganizzazione degli uffici comunali preposti all'edilizia sociale e
di relazioni precise con gli architetti liberi professionisti. Sotto
l'amministrazione di Karl Seitz viene appositamente strutturata una
sezione di architettura municipale, guidata da Hugo Breitner, Franz
Siegel e Anton Weber: attraverso di essa il Comune guida, attraverso i
propri progettisti -- da Josef Bittner a Karl Ehn --, le realizzazioni
del piano e cura i rapporti con gli architetti privati; nel 1926, 118
architetti si trovano impegnati nella progettazione degli Höfe
viennesi[^55].

[^55]: Cfr. Josef Bittner, *Neubauten der Stadt Wien*, I. Band, *Die
    Wohnungsbauten*, Wien-Leipzig-New York, Verlag Gerlach und Wiedling,
    1926. -->

<!-- Dovremo quindi prendere in esame tre temi fondamentali: -->

<!-- (a) le relazioni fra l'organizzazione interna del modello dello Höf
(anche nel suo variare nel tempo o secondo le tendenze) e la sua
localizzazione nello spazio urbano; -->

<!-- (b) le relazioni fra l'impresa viennese e la gestione urbana da
parte dei comuni socialdemocratici della Germania di Weimar; -->

<!-- (c) le relazioni fra l'impostazione produttiva e ideologica di quel
modello e le soluzioni offerte a livello architettonico. -->

<!-- La localizzazione degli Höfe costituisce il tema in cui convergono
l'ideologia e la prassi economica dell'austromarxismo. In buona parte, i
complessi del Comune socialdemocratico sorgono su terreni assicurati
alla municipalità, come si è visto, da parte delle amministrazioni
cristiano-sociali operanti nell'anteguerra: tale scelta impegna
evidentemente il Comune di Vienna a confermare la frammentazione degli
interventi pregiudicata dalla politica urbanistica del partito ora
all'opposizione, e implica il crollo di ogni velleità di impostazione di
una organizzazione urbana complessivamente "diversa". La *diversità* non
dovrà risiedere nel corpo urbano in sé, ma nella gestione delle sue
parti. Il modello dello Hof non implica neanche come tendenza una
ipotesi radicale di nuova organizzazione: al contrario, esso si
inserisce volutamente nella maglia della città esistente, dominata dal
piano regolatore del 1893, accettandone tutti i vincoli e i bilanci
passivi. -->

<!-- Anzi, tale fedeltà alle leggi di crescita della città ottocentesca,
limitatamente al settore residenziale, diviene addirittura
programmatica. In fondo, gli Höfe del Comune socialdemocratico ereditano
la forma complessiva della Mietkaserne operaia del XIX secolo,
trasformandola in senso qualitativo e dotandola -- coerentemente ai
primi programmi di Otto Bauer -- di servizi accentrati e collettivi, in
una misura tale da apparire come una risposta parziale alla riduzione
del tema residenziale a puro parametro quantitativo, operata nella città
speculativa dell'800. L'accentramento di alloggi a basso costo in
complessi unitari di grandi dimensioni -- dai 1587 abitanti del Sand
Leiten realizzato nel 1923, si passa nel 1927 ai 5.000 abitanti in 1400
appartamenti del Karl-Marx-Hof, con una media di 200/400 abitanti a
complesso -- corrisponde in buona parte alla scelta urbanistica iniziale
e al condizionato reperimento delle aree. Ma corrisponde anche a una
concentrazione di forza lavoro in cantieri unitari a bassissimo
coefficiente tecnologico; esattamente la condizione che verrà sfruttata
al massimo dagli architetti, a livello espressivo. -->

<!-- Visto in tal modo, il campeggiare degli Höfe nella fascia
periferica suburbana di Vienna non può non assumere un valore simbolico.
Ed è certo che il senso di tale comunione fra realismo urbanistico e
volontà simboliche non è sfuggito ai tecnici socialisti: tanto che in
qualche modo le realizzazioni viennesi, volutamente configurate come
"monumenti proletari", costituiscono una risposta polemica al piano
leninista di propaganda monumentale[^56]: si noti con quanta cura
l'amministrazione di Vienna fa risaltare in ogni complesso le didascalie
in cui appaiono, oltre alla data di costruzione e il riferimento al
programma complessivo, il nome del complesso stesso, dedicato sempre a
personalità della tradizione socialista. Così, la "presenza" della
tradizione socialista nella città non è più affidata, a Vienna, al puro
livello ideologico, come nel piano monumentale sovietico. Ora il
"monumento socialista" è l'abitazione operaia, modello di attrezzatura
residenziale completa: la sua eloquenza dovrà entrare a far parte del
programma edilizio.

[^56]: Cfr. A. Michajlov, *Leninsky Plan Monumental'noj Propagandy i
    Architektura* [Il Piano leninista di Propaganda monumentale e
    l'architettura], in "Architektura SSSR" 4, 1968, pp. 2--15. -->

<!-- In tal senso, la distribuzione disomogenea e in forma di cintura
discontinua degli Höfe, fra il centro ottocentesco e le espansioni
suburbane, è funzionale agli obbiettivi simbolici propostisi; così come
ad essi funzionale è la forma stessa del superblocco, al cui interno
solamente vengono dislocati i servizi collettivi -- asili, scuole,
lavanderie e cucine collettive, laboratori artigiani, spazi verdi, ecc.
-- in un palese sforzo di separazione dello Hof dalla città circostante.
-->

<!-- In tale voluta ostentazione di immagini, di "simbolismo
proletario", la Socialdemocrazia viennese si oppone esplicitamente alle
più avanzate letture di parte capitalista del senso e della forma
generale della nuova metropoli come elemento attivo di sviluppo.
L'alloggio operaio -- fino al '27 almeno -- non deve presentarsi come il
rispecchiamento freddo e disincantato della "calcolata esattezza" che un
Simmel aveva visto proiettata nel comportamento urbano. La *Philosophie
des Geldes*, l'insensibilità ad ogni distinzione, che quella stessa
*filosofia del denaro* induce nell'atteggiamento *blasé* dell'individuo
metropolitano, la "dissociazione", che il medesimo Simmel legge come il
livello massimo di socializzazione provocato dalla reazione alla
svalutazione dell'intero mondo oggettivo, non possono essere assunti
come dati *reali* per la politica ideologica dell'austromarxismo. La
bruta qualificazione delle diversificazioni, cui "l'anonimità e
l'indifferenza" del valore astratto del denaro riducono i valori
*concreti*, deve essere negata nell'ambito metropolitano: e tale
negazione deve assumere connotati di classe[^57].

[^57]: Cfr. Georg Simmel, *Philosophie des Geldes*, München-Leipzig,
    Dunker und Humboldt, 1900; *Die Grosstadt. Vorträge und Aufsätze zur
    Städteausstellung* (volume collettivo), Dresden, Zalen und Jaensch,
    1903, pp. 185--206, trad. it. *Metropoli e personalità*, in *Città e
    analisi sociologica*, a cura di Guido Martinotti, Padova, Marsilio,
    1968, pp. 275--289. Sulla sociologia urbana di Simmel si veda
    l'acuto saggio di Massimo Cacciari in "Angelus novus" 21, 1971. -->

<!-- Se nella *Grosstadt* capitalista "tutti gli oggetti galleggiano con
uguale peso specifico nel movimento costante della corrente monetaria"
così che gli oggetti stessi "giacciono allo stesso livello e
differiscono tra di loro solo per l'area che ricoprono nello
spazio"[^58], nella *rote Wien*, nella Vienna socialdemocratica, i
luoghi deputati dei "valori" proletari dovranno assumere una funzione
antitetica a quel livellamento, a quell'annullamento del valore nella
"corrente monetaria", a quella indifferenziazione della qualità nel
disponibile vuoto della nuda necessità. -->

<!-- [^58]: G. Simmel, *Die Grosstadt* cit., p. 280 dell'edizione
italiana. -->

<!-- Non più, quindi, il *dominio della molteplicità* indicato da un
Hilberseimer come esaltazione della più assoluta "semplicità
organizzativa": non più, in altre parole, un'assunzione indifferenziata
delle "leggi" che dominano l'economia metropolitana. Il rifiuto
dell'elementarismo coincide con il rifiuto della gestione complessiva
della città. Ci si ricollega, qui, al dissidio fondamentale fra la
cultura viennese e la cultura radicale della Germania di Weimar: l'arte
o l'architettura di avanguardia vengono valutate come risultato di un
assorbimento, di un'introiezione di quella riduzione delle stesse
funzioni sociali a *segni* dell'organizzazione capitalistica
complessiva. La cultura architettonica austriaca e l'ideologia
austromarxista rifiutano quindi i risultati delle avanguardie europee
sulla base di una classica "utopia regressiva"[^60].

[^59]: Cfr. Ludwig Hilberseimer, *Grosstadtarchitektur*, Stuttgart,
    Verlag Hoffmann, 1927.

[^60]: Il significato qui attribuito all'"utopia regressiva" è
    evidentemente diverso da quello ad essa assegnato da Adorno.
    L'utopia degli Höfe viennesi, come si è più volte ribadito, è
    estranea all'ideologia dell'avanguardia: estranea cioè
    *all'ideologia dell'innovazione* e anche a quella
    dell'incomunicabilità provocatoria. Caso mai, quella viennese è
    un'*utopia della comunicabilità*, un'estrema "utopia del valore" e
    del significato. -->

<!-- In termini puramente culturali, la dominante espressionista che
emerge in tutta evidenza nelle realizzazioni del Comune di Vienna fra il
'23 e il '28 ha una sua logica specifica. In forte ritardo rispetto ai
sussulti dell'angoscia borghese che aveva tentato, nella Germania del
primo Novecento, di recuperare l'ultimo residuo di presenza "umana"
nell'autorispecchiamento nella *reificazione cosmica* da parte di
intellettuali in bilico fra l'eutanasia e l'accettazione della propria
intellettualità, "Vienna rossa" isola il tema del *Masse-Mensch*,
dell'uomo massa tolleriano e genericamente espressionista, dichiarando
la totale estraneità del proletariato a quella alienazione. O meglio:
dichiarando che, nei luoghi totalmente gestiti dal proletariato stesso,
la forza lavoro recupera la globalità della propria "umanità", la
totalità di un'esistenza tesa alla *coscienza* del "dover essere" della
classe e della sua liberatoria "missione storica". -->

<!-- Gli alloggi operai dovranno *realizzare* l'immagine di tale
recupero: se un residuo utopico dovrà rimanere in essi, questo sarà
rivolto a designare il fine teleologico della "rivoluzione lenta"
dell'austromarxismo, vale a dire la liberazione finale della società
intera sotto il segno umanizzatore del lavoro liberato. -->

<!-- Non vi è ragione alcuna, quindi, per sostenere -- come fa il
Koyré[^61] -- che l'autarchia e l'isolamento dell'edilizia operaia
viennese rispondano a una lettura della committenza sociale in quanto
struttura di massa. Al contrario: di fronte all'anonimato della "città
capitalista", all'individualismo estenuato dell'alta commitenza
borghese, di fronte ai due opposti che l'avanguardia tende a
riunificare, assorbendo per intero la contraddittorietà della loro
compresenza nel reale, la politica architettonica della Vienna
socialdemocratica afferma l'individualismo settario delle "tocche rosse"
operaie.

[^61]: Cfr. H. Koyré, *Die Entwicklung des Wiener sozialen
    Wohnungsbaues* cit. -->

<!-- È certo che tale convergenza tra cultura architettonica e ideologia
politica si fonda sulla struttura della classe operaia viennese. La
depressione economica del Comune di Vienna, cui abbiamo fatto cenno, la
strutturale ambiguità della collocazione della forza lavoro viennese
nell'ambito di un'economia non solo inflazionata, ma priva di strutture
capaci di rimettere in moto un ciclo in sviluppo, la bassa composizione
organica di capitale anche nelle imprese concentrate, l'alta mobilità
delle iniziative, conseguente a tale deficitaria composizione organica,
implicano una struttura della forza lavoro attestata a livelli del tutto
ottocenteschi e paleotecnici, anche nella difesa della sua declinante
"professionalità". -->

<!-- È appunto a tale classe operaia mista, in crisi in tutta Europa ma
persistente in una Vienna del tutto anomala rispetto allo sviluppo delle
nuove concentrazioni urbane e al nuovo ruolo da esse assunto nella
ristrutturazione capitalistica, che la Socialdemocrazia austriaca si
rivolge, per esaltarne le *qualità* sul piano della gestione urbana. -->

<!-- L'esaltazione espressionistica -- usiamo tale termine per
antonomasia, naturalmente[^62] -- delle *qualità* specifiche ed
esclusive dell'alloggio operaio può finalmente essere letta come
celebrazione, da parte dell'austromarxismo, della *coscienza di classe*
legata all'anacronistica professionalità dell'operaio viennese. È tutta
qui l'utopia regressiva dell'ideologia austromarxista: nel celebrare
come avanguardia "sociale" una struttura professionale della classe
artificiosamente mantenuta a livelli arretrati da rapporti di produzione
bloccati nel loro sviluppo dalle contingenze storiche.

[^62]: Le più recenti monografie sull'architettura dell'Espressionismo,
    da quella di D. Sharp a quella di Borsi e König, hanno ignorato
    l'esperienza viennese, più per trascuratezza che per libera scelta,
    data l'indeterminatezza dei criteri usati per definire l'ambito di
    pertinenza di tali studi. Va comunque osservato che nessun legame
    esplicito esiste fra gli Höfe di Ehn o di Behrens e la tradizione
    espressionista tedesca o austriaca: caso mai è l'identificazione del
    proletariato, come "uomo nuovo" collettivo, con *l'uomo nudo* di un
    Toller, che è implicita nell'epica del Karl-Marx-Hof e del
    Winarskyhof. -->

<!-- Lo Hof operaio dovrà quindi materializzare quell'essenza
"spirituale" della nuova umanità in cui il proletario assume un ruolo
egemone, letta da Max Adler come realizzazione di "pensiero, volontà,
azione". Saranno gli Höfe viennesi, dunque, a dare significato concreto
al ricongiungimento di Kant e di Marx, che abbiamo letto come
caratteristica fra le più precipue dell'austromarxismo del primo '900.
-->

<!-- In tal senso in esse dovrà vivere -- trasformandosi in armonia con
i nuovi compiti ad esse affidati -- l'intera eredità
"nazional-popolare". Su tale base avviene, in un primo momento,
l'incontro fra l'ideologia politica e le istanze conservatrici della
cultura architettonica viennese. Nel Metzleinstaler-Hof, realizzato da
Hubert Gessner fra il 1919 e il 1923, tale radice populista è filtrata
attraverso alcune memorie formali ereditate dalla Wagnerschule: ma nelle
opere di architetti come Peller, Stockl e Stoik -- le case sulla
Justgasse del '23 o quelle sulla Berzelinsgasse del '25 -- risulta
vincente la tradizione *Biedermeier*, tipica espressione della piccola
borghesia. -->

<!-- È ancora difficile individuare filologicamente a Vienna le esatte
relazioni fra la committenza politica e la risposta disciplinare. Non
può sfuggire, però, come al populismo incontrollato dei primi
esperimenti si sostituisca immediatamente una intesa fra
Socialdemocrazia e cultura architettonica, mediata da un'ideologia
comune, ampiamente diffusa dal '23 in poi. -->

<!-- Esistono infatti due modelli con cui la Socialdemocrazia austriaca
e gli architetti viennesi intendono confrontarsi direttamente e nei
riguardi dei quali affermare la propria più radicale estraneità. Da un
lato è il dibattito -- economico, culturale, ar chitettonico -- che si
svolge intensissimo negli anni della Nep nell'Unione Sovietica;
dall'altro è la gestione socialdemocratica e cooperativistica delle
città nella Germania di Weimar, fortemente influenzata dalla cultura
architettonica radicale, erede delle avanguardie storiche. Di fronte al
dibattito sovietico, letto come utopismo e progetto autoritario insieme,
Vienna deve dimostrare le proprie capacità "costruttive". All'*utopia
realizzata* delle Siedlungen di Haesler a Celle o di May e collaboratori
a Francoforte, essa deve opporre un'alternativa populista e
antiutopica[^63].

[^63]: Cfr. su tale tema Manfredo Tafuri, *Socialdemocrazia e città
    nella Germania di Weimar*, in "Contropiano" 1, 1971, e la
    bibliografia ivi citata. -->

<!-- Risulta quindi difficilmente condivisibile l'opinione di Aymonino,
dove questi afferma che "negli esempi viennesi l'autosufficienza --
garantita dall'attrezzatura di svago e di istruzione, dalle presenze
commerciali, dai servizi sanitari -- è ridotta alle sue reali dimensioni
di assolvimento di alcune necessità primarie, *è privata cioè di ogni
aggiunta ideologica*". "Non ha perciò un valore unicamente di 'difesa'
della città speculativa -- egli continua -- o di minimo vitale che
sopperisca all'isolamento o alla distanza dalla grande città, ma ha il
valore di garantire subito un livello di vita pari, se non superiore,
alle migliori zone di periferia cittadina. L'edilizia economica per la
forma specifica in cui viene realizzata acquista così compiutamente una
funzione di guida, determinante per tutto il settore cittadino che la
circonda; entro questa precisa dimensione è essa stessa un'alternativa
alla produzione speculativa, una risposta più completa alla 'domanda'
messa in moto dal processo produttivo contemporaneo"[^64].

[^64]: C. Aymonino, *L'esperienza viennese*, in *L'abitazione razionale*
    cit., pp. 33--34. "Non a caso -- continua Aymonino (p. 34) -- la
    sostanza dell'esperienza viennese -- cioè la possibilità di
    sviluppare ulteriormente la città tradizionale, organizzandola nelle
    sue linee generali e realizzandola per caseggiati, niente affatto
    autosufficienti, ma strettamente correlati -- una possibilità quindi
    di vita per la 'città compatta' -- è stata ripresa soltanto nei
    paesi socialisti, dove appunto la questione degli alloggi, intesa
    nella sua totalità, non poteva essere risolta che in un unico quadro
    urbano, con la presenza in esso di una nuova periferia; nel senso
    che lo sviluppo per caseggiati pone diversi problemi da quelli posti
    dallo sviluppo per blocchi speculativi o da quella per quartieri
    satelliti". La relazione fra l'esperienza viennese e quella dei
    paesi socialisti è pertinente, ma solo rispetto alla *forma degli
    insediamenti*, non al tipo di rapporto da essi istituito nei
    confronti della città, e, più in generale del quadro istituzionale
    di cui la città è parte. Sulla base dell'osservazione di Aymonino si
    potrebbero inoltre avvicinare lo sviluppo per blocchi di Vienna alle
    impostazioni date da Fritz Schumacher agli ampliamenti di Amburgo.
    -->

<!-- Va comunque valutato con maggiore attenzione l'incontro fra
l'ideologia del programma socialista e l'ideologia architettonica che la
realizza. È intanto importante sottolineare che buona parte degli
architetti che assumono un ruolo di rilievo nella progettazione degli
Höfe comunali esce dalla scuola di Wagner, e in genere da un clima da
essa fortemente influenzato: Hubert Gessner, Robert Oerley, Rudolf
Perco, Otto Schöntal, Emil Hoppe, Josef Hoffmann, malgrado i tentativi
di un approccio rinnovato alla tematica urbana presenti nelle ricerche
della Wagnerschule, si trovano in possesso di un bagaglio di esperienze
del tutto anacronistico rispetto ai temi proposti dalla nuova
committenza politica. -->

<!-- In un certo senso, si può dire che è proprio tale assenza di un
precedente dibattito architettonico alla scala dei nuovi problemi a
permettere agli architetti che avevano iniziato la loro carriera nel
clima della "gaia apocalisse" viennese e che avevano sperimentato
dolorosamente la *finis Austriae*, di inserirsi docilmente -- senza la
volontà o la capacità di proporre strade alternative -- nel programma
del Comune socialista. All'ideologia tutta borghese della Wagnerschule
essi sono in grado di sostituire solamente un'ideologia populista: il
solo Hoffmann tenta -- con scarso successo -- di saldare opere come il
Klosehof (1924) o il blocco da lui costruito sulla Luxemburger Strasse
(1931) alle proprie ricerche puristiche dell'anteguerra[^65].

[^65]: Abbiamo già rilevato il carattere Biedermeier del
    Metzleinstaler-Hof di Hubert Gessner, un architetto che fra il 1907
    e il 1912 aveva realizzato una serie di ville a Vienna, fra le più
    controllate nell'ambito della Wagnerschule (Sternwertestrasse,
    Rechte Wienzeile, Hasnerstrasse ecc.). Otto Schöntal è l'autore
    della casa al n. 14 della Linzerstrasse, in cui viene popolarizzato
    il linguaggio olbrichiano, e, insieme a Emil Hoppe -- una delle
    figure più interessanti della scuola di Wagner, per alcuni progetti
    che la critica ha avvicinato alle posteriori immagini
    architettoniche di Sant'Elia -- aveva costruito case di abitazione
    "secessioniste" al Prater e sulla Wiedner Hauptstrasse. La
    personalità maggiore (Hoffmann a parte) della cultura architettonica
    viennese dell'anteguerra, che ritroviamo più volte impegnata nella
    realizzazione del programma del Comune di Vienna, è senz'altro
    Robert Oerley (1876--1945). Le sue ville sulla Weimarer Strasse
    (1904--1905) e sulla Vegagasse (1906) o il sanatorio Auersperg
    (1907) sono opere scarsamente influenzate dai modi della
    Wagnerschule, pur nel clima di un linguaggio non lontano da alcune
    ricerche coeve di un Van de Velde. Non a caso sono di Gessner e di
    Oerley opere come il Lasalle-Hof (1924), lo Hanusch-Hof (1923), il
    Karl-Seitz-Hof (1926) o il Washington-Hof (1927) che in vario modo
    costituiscono dei modelli per l'edilizia socialista viennese. Per
    una moderna lettura del fenomeno della Wagnerschule cfr. Otto
    Antonia Graff, *Die Vergessene Wagnerschule*, Schriften des Museums
    des 20. Jahrhunderts, Wien, Verlag Jugend und Volk, 1969.

    Toni Biedermeier uniti ad elementi linguistici vagamente
    goticheggianti e "popolari" -- tetti fortemente emergenti, portici
    ad arco acuto, logge e coperture sottolineate da archeggiature,
    cornici mistilinee o stilizzate -- sono presenti ancora negli Höfe
    degli anni 1924--26. Come esempi particolarmente probanti di tale
    populismo sommesso, citiamo il blocco residenziale di Gessner
    prospiciente il Lassalle-Hof (1925--26), il blocco articolato di
    Rudolf Krauss sulla Wagramerstrasse (1925--26) il blocco di K. Krist
    nell'XI Bezirk (Grillgasse, Herbertgasse, 1924--25) e lo Hof
    quadrato di Franz Weissmann, ispettore del Comune, nell'XI Bezirk
    (1925--26). Cfr. i fascicoli relativi pubblicati a cura del Comune
    di Vienna, e J. Bittner, *Neubauten der Stadt Wien* cit.; Guenter
    Hirschel, *Die Gemeindebauten der Stadt Wien*, in "Wasmuths
    Monatschefte für Baukunst" 10, 1926, pp. 357--369; H. Koyré, op.
    cit. -->

<!-- In tale vuoto culturale, in cui il populismo rappresenta solo
un'ideologia di riserva, si inserisce il determinante apporto di Peter
Behrens. -->

<!-- # 4. L'ideologia realizzata -->

<!-- Abbiamo intenzionalmente contrapposto l'attività di Loos a quella
di Behrens, nell'ambito della politica edilizia viennese. Peter Behrens
inizia nel 1922 il suo insegnamento all'Accademia di Vienna, portando
con sé sia l'esperienza pratica e teorica accumulata negli anni
anteriori alla guerra, che una profonda crisi individuale[^66].

[^66]: Per l'attività didattica di Behrens a Vienna si veda *Peter
    Behrens und seine Wiener Akademische Meisterschule*, herausgegeben
    von K. Maria Grimme, Wien-Berlin-Leipzig, Adolf Lusser Verlag, 1930,
    con il saggio introduttivo dello stesso Behrens, *Zur Erziehung des
    baukünstlerischen Nachweises*, pp. 9--12, in cui vengono riaffermate
    tesi largamente circolanti nell'ambito della cultura architettonica
    tedesca circa la priorità dei temi della metropoli, della
    cooperazione collettiva, dell'annullamento del soggettivismo, della
    standardizzazione e della *Neue Sachlichkeit*, come programmi
    adeguati a un'impostazione interdisciplinare della tematica urbana.
    Behrens commenterà la sua opera come architetto del Comune di Vienna
    in un articolo in cui il coordinamento della progettazione e
    l'unitarietà degli interventi da questo permessa vengono esaltati
    come meriti precipui dell'amministrazione socialista. Cfr. P.
    Behrens, *Die Gemeinde Wien als Bauherr*, in "Bauwelt" 41, 1928
    (trad. it. in "Casabella" 240, 1960, p. 48). Sulle teorie
    behrensiane in tema di industrializzazione edilizia, vedi: Erich
    Goldner, *Gedanken zur Industrialisierung des Wohnungsbaues von
    Peter Behrens (1868--1940)*, in "Baukunst und Werkform" 5, 1955, pp.
    308--310. Le tesi di Behrens vanno comunque confrontate con il suo
    progressivo e tardivo accoglimento di tematiche espressioniste,
    particolarmente evidente negli anni immediatamente posteriori al
    1918, in opere come la Dombauhütte di Monaco (1922) per la quale
    egli chiede la "Misericordia" di Barlach e quadri di Toorop, Minne e
    Hölzel, la fabbrica Hoechst di Francoforte (1920) o il padiglione
    dell'Expo 1925 di Parigi. Su tale momento "critico" di Behrens, teso
    ad accogliere nella sua distaccata "etica del riserbo" frammenti di
    pathos espressionista, cfr. i due saggi, di Julius Posener,
    *L'oeuvre de Peter Behrens*, in "L'architecture d'aujourd'hui" 2,
    1934, pp. 8--29, e di José Imbert, *Peter Behrens*, ibidem, p. 30,
    scritti in contraddittorio tra loro, e Wilhelm Weber, *Zur Peter
    Behrens Ausstellung*, in *Peter Behrens (1868--1940). Gedenkschrift
    mit Katalog aus Anlass der Austellung 1966--67*, pp. 13--18; Heinz
    Thiersch, *Peter Behrens aus der Sicht von Heute*, ibidem, pp.
    23--32. -->

<!-- È significativo, pertanto, che nel suo lavoro per l'edilizia
comunale di Vienna Behrens usi solo parzialmente la prima, e tenda a
nascondere la seconda. -->

<!-- La tradizione culturale in cui Behrens è radicato, si noti, è quasi
del tutto estranea sia all'utopismo romantico che agli apporti
dell'avanguardia. Nei suoi scritti fra il 1908 e il 1914 Behrens aveva
analizzato con estrema lucidità il tema della Grosstadt contemporanea,
respingendo la tradizione morrisiana, il romanticismo inglese, la teoria
della città-giardino e dell'autosufficienza dei singoli settori urbani,
a favore di una lettura unitaria, globale del fenomeno urbano. Contro la
dispersione residenziale egli oppone una città compatta, dominata da
un'economia vagamente comunitaria, al romanticismo edilizio un "nuovo
classicismo", capace di assicurare all'universo capitalistico un
autonomo universo della Forma. Non a caso l'architettura di Behrens è
posta sotto il segno del monopolio elettrico dei Rathenau; l'"etica del
riserbo" che traspira dai suoi "monumenti industriali" è tutta
all'interno del "disincanto" weberiano, interpretato come nuovo
Valore[^67].

[^67]: Le radici idealistiche e spiritualistiche della cultura
    behrensiana e i suoi legami con le idee sociali e politiche di
    Friedrich Naumann, cofondatore del Deutscher Werkbund, sono state
    sottolineate nel documento saggio di Stanford Anderson, *Behrens'
    Changing Concept*, in "The Architectural Design" 2, 1969, pp.
    72--78. Cfr. anche Vittorio Gregotti, *Peter Behrens*, in
    "Casabella" 240, 1960, pp. 5--8. -->

<!-- Ciò che ci interessa in modo particolare è però la sua lucida
lettura della nuova realtà berlinese: rispondendo nel 1912 a
un'inchiesta del "Berliner Morgenpost" egli riconosce nella nuova
Berlino la fine della "città del lavoro" e la concreta presenza di un
centro terziario da "dominare" attraverso un preciso ed unitario
intervento, in cui sia determinante un adeguato coordinamento
tipologico[^68].

[^68]: Cfr. l'inchiesta *Il futuro di Berlino*, nel "Berliner
    Morgenpost" del 27 dicembre 1912. -->

<!-- Due anni dopo, riprendendo il discorso appena iniziato sulla
"forma" della Grosstadt, Behrens pubblica nell'annuario del Deutscher
Werkbund un fondamentale saggio in cui l'intera tematica
dell'avanguardia -- il dominio del caos e dell'angoscia indotte dal
"molteplice" urbano -- viene ripresa nell'ambito di una prospettiva
metodologica tutta all'interno della tradizione urbanistica
ottocentesca, che avrà i suoi sbocchi estremi nella trattatistica di
Ludwig Hilberseimer[^69].

[^69]: P. Behrens, *Einfluss von Zeit und Raumausnutzung auf moderne
    Formentwicklung*, in *Der Verkeher*, Jahrbuch des Deutschen
    Werkbundes 1914, Jena, Eugen Diederichs, 1914, pp. 7--10. -->

<!-- Gli studi di Behrens sulla tipologia della residenza continua a
gradoni e sulla città a grattacieli terrazzati, condotti a Vienna dal
'22 in poi in collaborazione con Alexander Popp, sono in sintonia con i
suoi studi teorici dell'anteguerra, e rappresentano la controfaccia
dell'espressionismo esasperato di opere come la Hoechst Farbwerke di
Francoforte (1920--23). Il dominio del molteplice attraverso
l'elementarismo della forma e il controllo globale delle variabili
fisiche ed economiche: in questo programma Behrens individua
l'alternativa "positiva" all'irrompere del caos espresso nel pathos
eclettico delle sue opere posteriori al 1920, in cui il frantumarsi
della "poetica del contegno" è certo una risposta soggettiva alla fine
della Kultur pangermanica viva nel programma di un Walter Rathenau come
in quello di Friedrich Naumann e del primo Werkbund. Ma anche i modelli
urbani di Behrens rappresentano, nella Vienna socialdemocratica,
un'utopia priva di agganci con i programmi politici in corso di
realizzazione. -->

<!-- Impegnandosi nella progettazione di una serie di blocchi
residenziali per il Comune di Vienna, Behrens deve rinunciare alla
compiutezza dei suoi modelli: l'unico esempio realizzato della sua "casa
a gradoni" (che sicuramente ha influenzato analoghe ricerche loosiane) è
il frammento costruito nel 1927 al Weissenhof di Stoccarda[^70].

[^70]: Cfr. P. Behrens, *Terrassen am Hause*, in *Bau und Wohnung*,
    herausgegeben vom Deutschen Werkbund, Stuttgart 1927, pp. 17--25, in
    cui Behrens dichiara esplicitamente che la piccola casa a terrazze
    realizzata nell'ambito dell'esposizione Sperimentale di Stoccarda è
    solo un frammento del programma tipologico e igienico -- il recupero
    dell'ambiente esterno e del verde nell'ambito di una città ad alta
    densità edilizia -- studiato sin dal '22 con le ricerche sulle case
    a gradoni svolte a Vienna. Sull'attività urbanistica di Behrens si
    veda Aldo Rossi, *Peter Behrens e il problema dell'abitazione
    moderna*, in "Casabella" 240, 1960, pp. 47--48. -->

<!-- Behrens realizza invece, fra il 1924 e il 1926, due complessi per
il Comune di Vienna destinati a rivestire valore di modello sia
organizzativo che formale -- il Winarskyhof (1924--26) in collaborazione
con Hoffmann, Frank, Strand, Wlach e altri architetti, e il blocco sulla
Kostanziagasse (1924--25) -- oltre al minore e più tardo Franz-Domes-Hof
(1928) sul Margaretengürtel[^71].

[^71]: Cfr., sul Winarskyhof, Max Eisler, *Wiener Villen und Kolonien*,
    in "Moderne Bauformen" 1, 1926, pp. 393--397; l'anonima nota
    *Maisons ouvriéres è Vienne*, in "Cahiers d'Art" 6, 1926, pp.
    132--135, in cui l'opera di Behrens, giudicata positivamente, viene
    contrapposta a quella di Hoffmann, dichiarata estranea
    all'architettura moderna; *Die Wohnhausanlage der Gemeinde Wien.
    Winarskyhof*, Wien 1926; P. Behrens, *Zeitloses und Zeitbewegtes*,
    in "Österreichische Kunst", Monatsschrift für bildende und
    darstellende Kunst, Architektur und Kunsthandwerk, 1932, p. 5 ss.,
    in cui vengono pubblicati, fra l'altro, i progetti elaborati con
    Alexander Popp per una città a grattacieli a gradoni e il
    Franz-Domes-Hof. -->

<!-- Va anzitutto notato il Winarskyhof è, dopo quello sulla
Rosa-Luxemburg-Gasse[^72], uno fra i complessi più grandi fra quelli
impostati fino al 1924: con le sue 534 abitazioni esso si estende dalla
Stromstrasse fino alla Durchlaustrasse (limitata a sua volta verso ovest
dalla Nordbahn), e dalla Vergartenstrasse alla Pasettistrasse: il blocco
chiuso, tipico dei caseggiati del primo periodo di attuazione del piano,
si ripresenta puro nel volume edilizio triangolare a corte interna,
dislocato nel settore sud occidentale, ma viene violentato, aperto alla
città, articolato, nel complesso principale, dalla Leystrasse, che
penetra in larghezza l'area residenziale, sfondando, attraverso quattro
monumentali portali, i successivi corpi di fabbrica.

[^72]: Il complesso realizzato fra la Sanleitengasse e la
    Rosa-Luxemburg-Gasse è opera di Schöntal, Hoppe, Materschek, Theis,
    Jaksch, Kraus e Tölk (1924--23) e costituisce un esempio fra i più
    probanti del trapasso degli architetti della Wagnerschule a una
    sorta di "intimismo di massa", per quanto l'attributo possa sembrare
    paradossale. Con i suoi 1587 abitanti, esso è uno fra i maggiori
    complessi del Comune di Vienna. -->

<!-- Superblocco e città si fronteggiano quindi e tentano una
impossibile integrazione; si veda anche come i servizi a scala urbana si
dislochino nello Hof: la Stadtburgerschule e il Birgitta Spital occupano
le estremità angolari dello Hof principale, mediandolo rispetto
all'intorno costruito. Ma le stesse caratteristiche del superblocco
escludono ogni reale mediazione nei confronti dell'ambiente.
L'ostentazione del puro gioco delle masse edilizie, sottolineate e
commentate dai ricorsi orizzontali che si addensano e si frantumano in
corrispondenza dei grandi portali tripartiti -- entrando, per di più, in
colloquio con l'enorme didascalia riferita all'opera della *Gemeinde
Wien* -- fa ragione nel Winarskyhof di ogni romanticismo decadente:
tanto che la collaborazione con Behrens di architetti come Hoffmann,
Strand, Grete Lihotzky o Dirnhuber, risulta essere di pura dipendenza, a
meno delle poche (e quasi sempre infelici) varianti da loro apportate al
modello iniziale[^73].

[^73]: Nel fascicolo *Die Wohnhausanlage der Gemeinde Wien. Winarskyhof*
    cit., appaiono come progettisti, nell'ordine: J. Hoffmann, P.
    Behrens, Oskar Strand, J. Frank, Oskar Wlach, Schuster, Loos, Grete
    Lihotzky, Karl Dirnhuber. Per le responsabilità assunte nella messa
    a punto dei singoli settori del Winarskyhof cfr. Max Eisler, op.
    cit. È pertanto assai dubbio che architetti "all'opposizione"
    rispetto al programma socialdemocratico, come Loos o Frank, abbiano
    effettivamente collaborato con Behrens.

    Il loro nominativo è pur tuttavia necessario all'amministrazione
    viennese per far figurare nella realizzazione di un complesso
    "modello", come il Winarskyhof, una collaborazione che raccoglie
    l'intera *intelligencija* architettonica viennese, nelle sue punte
    avanzate. -->

<!-- Nel Winarskyhof (assai più che nel caseggiato a doppio cortile
sulla Kostanziagasse, di un purismo quasi polemico), Behrens sferra un
deciso attacco contro il populismo fine a se stesso di complessi come
quello di Wilhelm Peter (XIX. Bezirk, Obkirchergasse/Leidesdorfgasse,
1924--25) o di Rodler, Stutterheim e Tremmel (II. Bezirk,
Kaisermühlendamm/Schiffmühlenstrasse) ma anche contro le più consistenti
ricerche formali di Robert Oerley e Clemens Holzmeister. (Ci riferiamo
in particolare allo Hanusch-Hof, iniziato del 1923 dal primo e al
Brathof, realizzato dal secondo fra il '24 e il '25). -->

<!-- L'ostentato sintetismo del Winarskyhof si oppone infatti alla
ricerca di modulazioni parietali basate sulla reiterazione discontinua
di aggetti triangolari, caratterizzante le opere citate, anche se il
moderato eclettismo di Oerley, Holzmeister e Gessner sarà capace di
fondare una tradizione indipendente che avrà le sue espressioni maggiori
nel Reumanhof (H. Gessner, 1924--26) e nelle paradossali dispersioni
formali del decisamente espressionistico Professor-Jodl-Hof costruito
nel 1925--26 da Rudolf Perco, Rudolf Frass e Karl Dorfmeister[^74].

[^74]: Cfr. *Die Wohnhausanlage der Gemeinde Wien in XIX. Bezirk.
    Professor-Jodl-Hof*, Wien 1926. Si noti, in questo complesso, la
    coerenza tra le deformazioni geometriche dei volumi corrispondenti
    agli snodi dei corpi di fabbrica e l'andamento libero e aperto del
    disegno planimetrico. Come nel Winarskyhof, il blocco costruito è
    attraversato da una strada, che collabora però, qui, a diluire
    l'accentramento della corte chiusa. Sulla base di quanto si è finora
    osservato, va recisamente negato ciò che è stato scritto dal Koyré
    (e ripetuto da Aymonino) circa il significato del "nuovo purismo"
    attribuito al Lassalle-Hof, costruito nel 1924--25 da Gessner, in
    collaborazione con Hans Paar, Fritz Waage e Fritz Schlossberg (cfr.
    *Lassalle-Hof. Die Wohnhausanlage der Gemeinde Wien im II. Bezirk*;
    H. Koyré, *Die Entwicklung* cit.; C. Aymonino, op. cit.). Non è
    pertanto possibile far risalire alle influenze di un'opera ancora
    eclettica, come il Lassallehof, l'impostazione della tematica del
    superblocco offerta da architetti come Karl Ehn, Karl Dirnhuber o
    Schmid e Aichinger. -->

<!-- Non a caso nel fascicolo pubblicato a cura del Comune di Vienna sul
Winarskyhof, si afferma che nella monumentale dialettica tra il cubismo
delle masse denudate e la stratificazione orizzontale del disegno
complessivo, gli architetti hanno voluto esprimere "l'immagine di una
grande città democratica"[^75].

[^75]: "Die Architekten wollten mit ihrem Werk den Beweis erbringen,
    dass in der kubischen Wirkung der Baumassen, im Verzicht auf schräge
    Dachflächen in der ruhigen horizontalen Lagerung, in der räumlichen
    Grösse der Trakte und Höfe, in der vollkommenen Weglassung aller
    dekorativen zutaten auf Mauerflächen und Dachabschlüssen alles das
    ausgedrückt werden kann, um zu dem Ziel eines wahrhaft modernen,
    bewusst demokratischen Grosstadtbildes zu gelangen" (*Die
    Wohnhausanlage der Gemeinde Wien. Winarskyhof* cit., p. 6). -->

<!-- È quindi con Behrens che il programma residenziale socialista si
traduce in una coerente ideologia architettonica. L'empirismo delle
soluzioni tipologiche, sia alla scala del singolo alloggio che a scala
urbana, l'artigianalità delle tecniche edilizie, la casuale dislocazione
dei complessi nella città, la negazione di ogni ricerca di standards
ottimali nel dimensionamento degli Höfe e in quello dei servizi, vengono
ora rovesciati, come valori positivi, in immagini che esaltano il
collettivismo "umano" e la "democrazia" residenziale dei complessi
dell'amministrazione "rossa". -->

<!-- Il rigoroso accentramento dei superblocchi intorno agli spazi
centrali attrezzati -- a meno di articolazioni che attenuino la rigidità
dello schema -- è funzionale a tale scoperta della capacità
dell'architettura a esprimere, senza compiacimenti folcloristici o tardo
romantici, l'epica socialista. Anzi, si può avanzare l'ipotesi che il
più coerente "realismo socialista" sia stato realizzato, in
architettura, nella Vienna socialdemocratica, in forte anticipo sulle
ricerche sovietiche, dal gruppo VOPRA in poi (si potrebbero persino
invocare somiglianze formali: si confronti il Karl-Marx-Hof, ad esempio,
con i successivi progetti dei fratelli Vesnin per il concorso per il
palazzo dell'Industria pesante a Mosca). -->

<!-- È nella linea tracciata da Behrens che si inseriscono opere come il
blocco di 75 appartamenti di Karl Dirnhuber (uno dei collaboratori del
maestro tedesco per il Winarskyhof), il polemico Wiederhoferhof di Josef
Frank[^76], il Pestalozzihof di Ella Briggs[^77], i molti interventi di
Schmid e Aichinger, e, principalmente, quelli di Karl Ehn, indubbiamente
il più significativo fra gli interpreti dell'*epica proletaria* come
ideologia architettonica di recupero.

[^76]: Cfr. il fascicolo del Comune di Vienna sul Wiederhoferhof. Come
    abbiamo già notato, Josef Frank si trova all'opposizione nei
    confronti della scelta tipologica fatta dall'Amministrazione.
    L'assoluta scarnificazione formale del Wiederhoferhof può quindi
    essere letta come una pura prestazione tecnica da parte di Frank,
    che è, del resto, fra gli architetti viennesi degli anni '20, il più
    vicino alla poetica della Neue Sachlichkeit.

[^77]: Cfr. *Die Wohnbausanlage der Gemeinde Wien. Pestalozzi-Hof im
    XIX. Bezirk*, Wien 1926, e L. A., *Wohnbausblock und Ledigenbeim
    "Pestalozzihof" im Wien*, in "Wasmuths Monatshefte für Baukunst"
    XII, 1928, p. 69 ss. -->

<!-- Ehn -- anch'egli uscito dalla Wagnerschule -- aveva esordito con
alcune opere isolate, influenzate dalla maniera di Holzmeister, ma si
impegna sin dal 1922 nell'operazione edilizia comunale: nel 1922 inizia
la costruzione dell'edificio di 164 appartamenti sulla Balderichgasse,
nel 1923 quella dello Zentraler Friedhof e della Siedlung Hermesweis,
nel 1924--25 del superblocco a cortile chiuso fra la Kreuzgasse e la
Paulinengasse[^78], fra il 1925 e il 1927 realizza la sua prima opera di
grande rilievo, il Bebelhof, accentrato su una corte quadrangolare
irregolare, con 301 appartamenti.

[^78]: Cfr. *Die Wohnbausanlagen der Gemeinde Wien im XVIII. Bezirk.
    Kreuzgasse*, Wien 1925. Il complesso di Karl Ehn, allungato in forma
    di cortile rettangolare a lati progressivamente aggettanti, fra la
    Kreuzgasse e lo Czartoryskipark, comprende 318 abitazioni, 6 negozi
    e 4 laboratori artigiani, ed è prospiciente al blocco articolato di
    Erich Leischner. Il linguaggio di Ehn assorbe già qui alcuni spunti
    mendelsohniani, ricomposti nell'articolato geometrismo dell'insieme:
    vedi le finestre angolari, le soluzioni di continuità risolte con
    volumi semicilindrici, i marcapiani che sottolineano le logge e le
    finestre, il gioco degli aggetti. Si noti che nel 1924 Karl Ehn è
    già architetto capo dell'Ufficio edilizio del Comune di Vienna. -->

<!-- Al purismo volumetrico e all'enfatizzazione degli snodi angolari,
Ehn assomma, nel Bebelhof, una serie di vere e proprie forme simboliche.
Ai lati dell'ingresso principale aggetti triangolari e volumi
semicircolari formano, insieme agli scatti volumetrici dei piani della
facciata e alle lame murarie sostenenti i pennoni, un complesso di
oggetti geometrici che si impongono alla "percezione distratta" del
pubblico urbano. Analogamente, nello Hof interno le superfici sono
articolate dal libero scorrere su di esse di aggetti volumetrici
dislocati su vari piani e legati fra loro secondo una astratta cadenza.
Qualora però si volesse leggere cosa corrisponda, nella struttura delle
abitazioni, a tale esaltazione del "patetico" formale si rimarrebbe
fortemente delusi. Come in buona parte degli Höfe viennesi, anche nel
Bebelhof il pathos della forma è ottenuto in modo indipendente
dall'organismo complessivo, introducendo casuali eccezioni
nell'uniformità dell'elementare -- e spesso non felice -- tipologia
residenziale. -->

<!-- È nel Karl-Marx-Hof che Ehn realizza la sintesi più completa fra
contributo delle avanguardie, enfasi strutturale e valori epici
enfatizzati da stilemi tradizionali: l'eclettismo deve comprendere in sé
l'avanguardia, annullarla in quanto tale -- in quanto utopia --
asservirla a un nuovo "realismo". Non più il *vuoto* del puro oggetto,
quindi, ma un'eloquenza delle immagini capace di usare il linguaggio
elementarista, le scomposizioni neoplastiche, le aggregazioni
costruttiviste, all'interno di una sintesi globale, comprensiva non solo
delle proposte delle avanguardie, ma anche dello storicismo insito nella
tradizione borghese del XIX secolo. -->

<!-- Il proletariato come classe egemone di un "nuovo mondo" che non
tagli i ponti con la grande Kultur borghese, dunque: nell'opera massima
di Karl Ehn tale motivo, profondamente lukácsiano, si traduce
direttamente nell'allusività delle forme e nel loro montaggio[^79]. Si
osservi l'alternarsi delle torri concluse da pennoni porta bandiera e da
enormi arconi, che chiudono, più di quanto non aprano, il complesso
verso il tessuto urbano circostante. L'enfasi dimensionale di tali
elementi allude a memorie formali della più opposta origine: gli arconi
che sfondano in profondità il corpo dell'edificio offrono all'insieme
una dimensione "eroica", tanto più accentuta in quanto ripresa dalla
teoria continua delle torri che si profilano come valori conclusivi del
superblocco, come masse che definiscono l'assoluta autonomia del
frammento urbano a scala gigantesca, dotandolo di una *skyline*
aggressivamente chiusa in se stessa.

[^79]: Sul Karl-Marx-Hof vedi *Der Karl-Marx-Hof. Die Wohbnhausanlage
    der Gemeinde Wien* cit., che come tutti i fascicoli pubblicati a
    cura del Comune di Vienna riporta i dati specifici del complesso; D.
    Brooke, *Karl-Marx-Hof*, in "RIBA Journal" 18, 1931, pp. 671--677;
    Anton Seda, *Ursachen und Entwicklung des kommunalen sozialen
    Wohnungsbaues* cit. Il Karl-Marx-Hof, il cui spazio interno è di
    10.480 m^2, copre un'area complessiva di 156.027 metri quadri di cui
    solo il 18,40% è edificato. In esso trovano posto 1382 abitazioni,
    pari a circa 5000 abitanti, due asili infantili, due lavanderie
    collettive, una clinica odontoiatrica, una biblioteca, una casa per
    la gioventù, un ambulatorio con annessa farmacia, un ufficio postale
    e molti negozi. La tipologia degli alloggi è estremamente variata:
    dalle stanze uniche con cucina (125) sì va ad appatrtamenti con 2
    stanze e cucina (748), con 3, 4 e 5 stanze, secondo proporzioni
    decrescenti. Nello spazio centrale sono collocate le immagini del
    "lavoro emancipato e dell'educazione socialista": la statua del
    Seminatore di Hofner, e le statue della Libertà, della Previdenza,
    del Pensiero illuminato e della Cultura del corpo, di Riedl (queste
    ultime in corrispondenza dei quattro ingressi principali).

    Nel parlare di motivi lukécsiani, a proposito del Karl-Marx-Hof, ben
    presenti le critiche rivolte da Lukács alla teoria e alla prassi
    austromarxiste. Tuttavia non esiste forse opera architettonica degli
    anni '20 più profondamente partecipe del pensiero estetico di Lukács
    del superblocco di Karl Ehn. In quanto sintesi della grande
    tradizione borghese proiettata nell'universo dei "valori"
    socialisti, esso merita, ancor più del Winarskyhof, la qualifica di
    opera massima del "realismo socialista". -->

<!-- Ma una volta affermato tale principio, la distesa modellazione del
complesso deve dimostrare le proprie capacità di articolazione: anzi,
tanto più valida sarà quella dimostrazione polemica di autonomia quanto
più essa sarà variata e composta dialetticamente con ulteriori rimandi
linguistici. È per questo che lo stesso assetto della teoria degli
elementi primari accentua l'isolamento del Karl-Marx-Hof dalla
disgregata città circostante: le abitazioni operaie, come luogo del
Semantico, del Significato recuperato, si oppongono alla città della
speculazione, della pura, *asemantica* quantità. Anche in ciò Karl Ehn
entra in polemica con l'architettura radicale tedesca. -->

<!-- Il massiccio insediamento residenziale, nucleo di auto-
organizzazione operaia, dovrà rifiutare, anche nella propria struttura
formale, la passiva accettazione di una forma coincidente con
l'*immagine del lavoro*, con l'immagine della catena di montaggio
realizzata nell'architettura. In qualche modo il superblocco di Ehn
assume qui un carattere utopistico. La "città umana" è un'utopia
regressiva realizzata in un settore della città borghese (negativa per
definizione): il Karl-Marx-Hof è il monumento dimostrativo di tale tesi,
tanto più in quanto l'*uomo* in esso prefigurato è il *produttore
cosciente* comunitario, estraneo e opposto alla "massificazione" indotta
dalla "tecnocrazia del capitale". -->

<!-- Se da tali implicazioni ideologiche si passa a esaminare
l'organizzazione interna degli alloggi, si nota un profondo disinteresse
del progettista per la ricerca tipologica. Gli alloggi del
Karl-Marx-Hof, come quelli della maggior parte dei complessi viennesi
fra le due guerre, del resto, si basano su una successione di vani del
tutto empirica e ricca di inconvenienti funzionali: alla notevole
densità dei servizi collettivi -- Kaganovič criticherà la mancanza di
impianti di riscaldamento centralizzato nel superblocco di Karl Ehn[^80]
-- corrisponde una sorprendente carenza di condizioni ottimali nella
distribuzione e nell'attrezzatura delle cellule residenziali. Le
distanze prese dalla cultura mitteleuropea, e dalle ricerche in tema di
Existenz-minimum da essa condotte, pesano quindi notevolmente sui
progettisti viennesi. Piuttosto che sulla base di una progettazione come
continua critica tipologica, essi procedono infatti sulla base di
*modelli*. Lo Hof attrezzato, in tutte le sue varianti, non sembra
ammettere verifiche attuate tramite standards fissati in relazione a
ricerche teoriche a priori; il basso coefficiente tecnologico che
caratterizza la realizzazione del programma socialdemocratico ha una sua
corrispondenza nelle deficienze tipologiche degli alloggi.

[^80]: Lazar Kaganovič, *L'Urbanisme sovietique. Rapport présenté à la
    Session plénière du CC du PC(b) de l'URSS*, Paris, giugno 1931, p.
    37. -->

<!-- La strada intrapresa da Karl Ehn, la più aderente all'ideologia
dell'operazione austromarxista, è comune alle opere di Schmid e
Aichinger, due architetti che assumono un ruolo di rilievo nell'ambito
del piano comunale. -->

<!-- Nel Reismanhof (1924), nel Rabenhof (1925), nel Matteottihof (1926)
e nel Somogyhof (1927), empirismo tipologico, pathos etico, esaltazione
della dimensione urbana dell'intervento, scarnificazione degli
intervenuti linguistici si uniscono strettamente fra loro. Anche nelle
opere di Schmid e Aichinger lo Hof si chiude alla città con chiari
risvolti polemici, evidenziati dalle immagini architettoniche. Tale
scelta diviene sintomatica nella localizzazione successiva dei vari
superblocchi nel nodo costituito dall'incrocio fra il Margaretengürtel e
la Fendgasse: qui si allineano in una scontrosa autonomia formale il
Reumanhof, il Metzleinstalerhof, lo Herweghof (di Schmid e Aichinger,
1926), il Matteottihof: quest'ultimo a cavallo della Fendgasse e
sviluppato in due Höfe indipendenti[^81].

[^81]: Josef Bittner, *Herweg-Hof, Matteotti-Hof*, Die Gemeinde Wien,
    1927. -->

<!-- I successivi interventi, pur occupando un'ampia porzione di suolo
urbano, evitano, con la loro autonomia, di offrire un'immagine unitaria
di una *possibile* città nuova: qui, più che in tutti gli altri
interventi del Comune di Vienna, realismo e ideologia si compongono in
una sintesi paradossale. -->

<!-- Anche il Karl-Seitz-Hof, iniziato nel 1926 da Gessner, si chiude
alla città nello stesso momento in cui *finge* di aprirsi ad essa. Il
vasto emiciclo che lo caratterizza si oppone infatti alla struttura
accentrata e suddivisa in Höfe minori e indipendenti, che si organizzano
nel trapezoide planimetrico suddiviso dall'arteria centrale,
perpendicolare all'asse del semicerchio prospettante la Jedleseer
Strasse. Il superblocco di Gessner si articola e si definisce in tale
sua struttura dialettica: lo spazio verde contenuto nell'emiciclo
esibisce ed esalta, al cospetto dell'area urbana dominata dall'edilizia
speculativa e frammentata, l'unitarietà del superblocco e le
caratteristiche dei suoi servizi. Anche qui all'epica populista -- si
veda il trattamento eclettico della torre, che si staglia come elemento
differenziato sulla sinistra del semicerchio, con i suoi richiami a un
linguaggio composito e "popolare" -- si unisce la polemica, il voluto
distacco dell'autonomia residenziale operaia dal complesso urbano. Ma al
modello fissato dal Winarskyhof, dai superblocchi di Ehn, di Schmid e
Aichinger, alla disposizione semiaperta del Karl-Seitz-Hof si
contrappone un diverso modello teso a diluire, per quanto possibile, la
dimensione e l'autonomia dello Hof in un paesaggio urbano tutto
costruito. Esiste quindi una linea alternativa a quella dell'epica
proletaria del Karl-Marx-Hof o del Matteottihof: il populismo del
complesso sulla Sandleitengasse e dello Hanusch-Hof trova così nel
Georg-Washington-Hof, realizzato da Robert Oerley e Karl Krist fra il
1927 e il 1930, il suo massimo compimento. Il *superblocco articolato*,
come viene definito dal Koyré[^82], suddiviso in spazi semichiusi
successivi -- il Birkenhof, il Fliederhof, lo Ahornhof lo Ulmenhof,
l'Akazienhof, la Eschenallee -- introduce una diversa interpretazione
della dimensione comunale dell'intervento: le 1085 abitazioni del
complesso, organizzate sulla base della consueta deficienza distruttiva,
si dispongono secondo una libera configurazione che ribalta l'interesse
sulla sistemazione dei vasti spazi verdi, occupanti il 76 % circa del
suolo a disposizione. In luogo dell'epica proletaria il Washington-Hof
propone una sorta di anticipazione di quello che sarà il cosiddetto
neoempirismo scandinavo. La "città dell'uomo" realizzata dal socialismo
abbandona qui il pathos a favore di un idillico sposalizio fra allusioni
a linguaggi nazional-popolari (vedi l'adozione di tetti aguzzi e aggetti
triangolari nelle facciate sulla Kastanien-Allee o le archeggiature
reiterate di Karl Krist, nel Birkenhof) e natura organizzata.
L'autonomia del superblocco viene quindi compromessa: fra gli Höfe
successivi e la città è ora possibile un più stretto colloquio.

[^82]: H. Koyré, op. cit. -->

<!-- Con ogni probabilità l'abbandono dell'autonomia dello Hof
nell'opera di Oerley e Krist va messa in relazione agli attacchi che in
sede politica vengono sferrati alle "fortezze rosse"[^83]. Malgrado
l'inconsistenza di tali attacchi -- la stessa politica del partito
socialdemocratico smentisce l'intento di organizzare gli Höfe come
possibili centri rivoluzionari -- il Comune di Vienna mostra, proprio
con il Washington-Hof (non a caso pari, come dimensione di intervento,
al Karl-Marx-Hof) di preoccuparsi di dimostrare la *socialità* della sua
"città dell'uomo". Il superblocco diluito è quindi una forma alternativa
di "realismo socialista": il suo linguaggio popolare è ostentato come
superamento del pathos proletario degli Höfe di Ehn.

[^83]: Le critiche dei cristiano-sociali al programma degli Höfe
    socialisti vengono riprese, come si è visto, nel corso del Congresso
    internazionale di edilizia e urbanistica tenuto a Vienna nel 1926.
    Significativamente, nel 1927, l'anno successivo, vengono
    contemporaneamente iniziati il Karl-Marx-Hof e il
    Georg-Washington-Hof, secondo due programmi tra loro alternativi. La
    critica alle "fortezze rosse" viene sferrata dall'opuscolo del
    dottor Schneider, *Der Fall der roten Festung*, Wien 1933, in cui si
    riprendono gli argomenti di Loos e Frank a favore delle case isolate
    o aggregate, con orto. Sulla polemica relativa alla forma degli Höfe
    cfr. Ch. A. Gulick, op. cit. -->

<!-- È estremamente interessante però notare come in un'opera tarda,
come il complesso dell'Engelsplatz di Rudolf Perco (1930--33), al
tentativo di spostare il tema edilizio verso un esibito uso di più
moderne tecniche di produzione -- nel complesso vien fatto uso di una
standardizzazione generalizzata delle strutture di tamponamento -- non
si disgiunga un largo margine concesso all'esaltazione di sovrastrutture
"qualificanti", relazionate alla scala dimensionale dell'insieme. Si
veda l'inserimento, all'interno della neutra pannellatura continua che
lega unitariamente lo Hof, planimetricamente accentrato sui servizi
comuni e definito in modo rigorosamente geometrico, dei balconi
ripetuti, delle torri di ingresso, dei pennoni, cui chiaramente
l'architetto attribuisce un ruolo "eloquente" contrapposto alla
schematicità della struttura complessiva. -->

<!-- Con l'Engelsplatz, sostanzialmente, assistiamo a un vero e proprio
"ritorno alle origini". Assai più che le opere di Schmid e Aichinger, di
Ehn, di Oerley o di Gessner, l'opera di Perco si riallaccia alle
tematiche avanzate da Otto Wagner nei complessi residenziali degli anni
1910--12 (le case sulla Neustiftgasse-Döblergasse, ad esempio). In luogo
dell'epos del Karl-Marx-Hof una accettazione della logica
produttivistica celebrata sotto il "segno operaio". Oggetto di
celebrazione diviene ora la ripetibilità degli elementi standardizzati e
la sua ostentazione, come elemento che entri nella scala urbana solo
grazie alle dimensioni dell'intervento. Anche per l'Engelsplatz si potrà
Parlare di "realismo socialista"; solo che ora ai valori "epici" si
sostituiscono valori basati sulla "poetica del riserbo". È in tal senso
che è giustificato parlare di una trasmissione -- nella logica di una
struttura massificata di intervento -- della poetica tutta borghese di
un Otto Wagner al dibattito culturale legato alla gestione
socialdemocratica delle abitazioni operaie a Vienna. -->

<!-- Con l'Engelsplatz o con un'opera anch'essa tarda e di notevole
interesse, come lo Speiserhof (di Leopold Bauer, Gloser, Lichtblau e
Scheffel, 1929), possiamo considerare chiusa l'e sperienza che abbiamo
seguito nelle sue diverse articolazioni. Fra il '30 e il '34, quando
l'attività del Comune di Vienna viene del tutto paralizzata a causa
dell'involuzione politica, il legame fra programma socialista e
ideologia architettonica vive solo come stanca prosecuzione delle
ricerche degli anni '20. -->

<!-- "Vienna rossa" ha del resto realizzato formalmente la propria
"immagine", E ciò, non ha caso, proprio mentre, fra le incertezze del
partito socialdemocratico, si verificano da un lato i primi scollamenti
fra masse e partito (a riprova della scarsa utilità dello sforzo
puramente ideologico fatto nell'ambito della tradizione austromarxista),
dall'altro la rinnovata violenza dello Stato contro le stese masse
sfuggite al controllo socialdemocratico. Può essere quindi il caso di
ricordare che nel 1927 -- a un anno di distanza dall'inizio della
costruzione del Karl-Marx-Hof -- a seguito di una sentenza assolutoria
pronunciata nei confronti di rappresentanti dell'organizzazione di
estrema destra Frontkämpfer, assassini di alcuni operai, la protesta del
proletariato democratico esaltato negli Höfe comunali si concluderà con
il ferimento di mille operai, la morte di 85 di loro e l'incendio del
palazzo di Giustizia. -->

<!-- Aver messo l'accento sul carattere di "utopia regressiva" della
politica socialdemocratica in tema di gestione urbana, sulla sostanziale
sostituzione di un'etica della "democrazia residenziale" allo sviluppo
delle lotte operaie da un lato e a quello della struttura urbana
complessiva dall'altro, aver sottolineato quanto la Realpolitik
austromarxista oscilli fra controllo dei movimenti di classe e
incapacità a leggere le direttrici dello sviluppo capitalistico (e le
sue interne contraddizioni, particolarmente eversive dopo il '27), ci
risparmierà di considerare l'intervento nazista come causa unica della
fine di un esperimento urbanistico di avanguardia e di aggiungere
ulteriori pagine retoriche sulla resistenza operaia contro le camicie
brune operata dall'interno delle "rocche rosse" viennesi. -->

<!-- Sul piano sentimentale ci si potrà ancora emozionare per la
riscoperta tardiva della lotta operaia in una fase ormai disperata, e
per la battaglia intentata da quella che venne chiamata la "Comune di
Vienna". -->

<!-- Ma non v'è frase più esplicita della frattura oggettiva fra la
politica austromarxista e la realtà della situazione di classe, che
quella pronunciata da uno degli operai di Anna Seghers: "Non è più tutto
come prima. Il Karl-Marx-Hof non è rovinato, è vero, lui ce l'ha fatta.
Ma la nostra fede nel partito ... quella sì, si è sfasciata"[^84].

[^84]: Anna Seghers, *Der Weg durch den Februar* (1935), trad. it. *La
    via di Febbraio*, Milano 1956. -->
