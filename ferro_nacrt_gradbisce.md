---
title: "Sérgio Ferro, Dessin/chantier"
---

## Gradbišče

Posebno pozornost namenja makroekonomski vlogi "zaostalosti" gradbene
industrije (ki je vzporejena tudi produktivni vlogi "zaostalosti"
periferije).


Gradbišče zaseda posebno mesto znotraj družbene produkcije.  Glede na
način produkcije gre za manufakturo, ki je jasno razločena tako od obrti
kot velike industrije: od obrti je različno po svoji naprednejši delitvi
dela, od velike industrije je različno po tem, da svojih delnih operacij
ne prenaša na stroje.  Občasno uporablja industrijske produkte kot so
materiali, sestavi in nebistveni stroji, a temeljna značilnost gradbišča
ostaja v uporabi žive delovne sile [@ferro2016dessin, 94].

V manufakturi je razmeroma več delovne sile kot v veliki industriji.
Organska kompozicija kapitala (razmerje med deležem variabilnega
kapitala -- vrednost delovne sile -- in deležem konstantnega kapitala --
vrednost poslopja, materiala, orodja -- v kolikor je določena s tehnično
kompozicijo istega) je v manufakturi nižja kot v industriji.  Če
vzamemo, da samo variabilni kapital ustvarja presežno vrednost,
manufaktura kapitalu zagotavlja izdatnejši del presežne vrednosti v
primerjavi z industrijo.  Kljub temu, da je zaostala, je manufaktura
ekonomsko donosnejša v primerjavi z veliko industrijo.  Razlika se
zakrije v procesu izravnave profitnih stopenj različnih panog
[gl. @marx1973kapital3, 175-195; ali @heinrich2013kritika, 153-158].
Ferro temu doda, da zgodovinsko tako imenovani "zaostali" sektorji s
svojo višjo presežno vrednostjo podpirajo "napredne" sektorje
[@ferro2016dessin, 95].

Po Ferru je posledica te lastnosti manufakture, posebej gradbene
dejavnosti, daljnosežna.  Ne samo, da presežna vrednost proizvedena v
gradbeništvu vzdržuje splošno profitno mero, temveč služi kot vir
akumulacije kapitala in 

> je ena glavnih naprav -- skupaj z monopolnim sistemom, kolonializmom
> in imperializmom --, ki jih kapital uporablja v borbi proti njegovi
> najhujši nočni mori: neizogibnemu tendenčnemu padanju profite mere s
> stalnim napredovanjem produktivnih sil [@ferro2016dessin, 95].

<!--
is one of the main devices -- together with monopoly, colonialism,
imperialism, etc. -- used by capital to fight against its worst
nightmare: the inevitable tendency of the rate of profit to fall with
the constant advance of the productive forces [@ferro2016dessin, 95].
-->

S tem makroekonomskim ogrodjem (poenostavljenim) Ferro pravi, da je
se industrializacija gradbišča (kar ne vključuje prefabrikacije) ne bo
zgodila, saj bi pomenila ekonomsko katastrofo.


> Gradbena manufaktura tako ostaja manufaktura z marginalnimi
> izboljšavami, kot jih na primer omogočijo napredki v informacijski
> tehnologiji.  

<!--
Building manufacture tends therefore to remain a manufacture, with
marginal improvements, such as those made possible by information
technology.  The features of this method of production inexorably
determine the professional practice of architects and other agents in
charge of the prescriptions, particularly those related to design.  Such
determinations are structural and, in our capitalist regime, mostly not
dependant on the will and position of these professionals
[@ferro2016dessin, 95-96].
-->

## Načrt



---
lang: sl
references:
########################################################################
- type: book
  id: lloyd2016industries
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  title: "Industries of architecture"
  publisher-place: London
  publisher: Routledge
  issued: 2016
  language: en
########################################################################
- type: chapter
  id: contier2016anintroduction
  author:
  - family: Contier
    given: Felipe
  title: "An introduction to Sérgio Ferro"
  container-title: "Industries of architecture"
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  publisher-place: London
  publisher: Routledge
  issued: 2016
  page: 87-93
  language: en
########################################################################
- type: chapter
  id: ferro2016dessin
  author:
  - family: Ferro
    given: Sérgio
  title: "Dessin/chantier: an introduction"
  title-short: "Dessin/chantier"
  container-title: "Industries of architecture"
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  translator:
  - family: Agarez
    given: Ricardo
  - family: Kapp
    given: Silke
  publisher-place: London
  publisher: Routledge
  issued: 2016
  page: 94-105
  language: en
# vim: spelllang=sl spell
...
