---
title: "Manfredo Tafuri, Norma in program: Vitruvij Daniela Barbare"
...

Nel finale dell'Amadigi, pubblicato nel 1560 da Gabriele Giolito de
Ferrari, Bernardo Tasso pone Daniele Barbaro al centro di un'ideale
parata di virtuosi patrizi veneziani, caratterizzandolo come totalmente
dedito alla vita contemplativa: "Il Barbaro, che alzando il suo pensiero
/ S'è dalle cure della patria tolto, / E pensa e scrive".[^1]

Ma Sperone Speroni e Paolo Paruta, rispettivamente in Della vita attiva
e contemplativa e in Della perfettione della vita politica, offrono un
ritratto alternativo del Barbaro. Entrambi lo raffigurano come difensore
della vita activa, e — fatto da non sottovalutare - il secondo lo fa
addirittura entrare in polemica sull'argomento con Giovanni Grimani, di
cui Daniele era stato designato successore eventuale alla carica di
patriarca di Aquileia.®

È certo al nuovo status del Barbaro che allude Bernardo Tasso; uno
status che Daniele aveva accettato dopo un travagliato iter spirituale.
Nel 1560, inoltre, l'opera maggiore del Barbaro, i commentari al De
architectura di Vitruvio, era uscita da soli quattro anni. Come spiegare
l'antinomica caratterizzazione attribuita dai contemporanei al Barbaro?

Pronipote del grande Ermolao e fratello di Marcantonio, Daniele è membro
di una grande famiglia veneziana legata alla curia pontificia e sodale
di altre grandi famiglie di orientamento 'romanista', come i Pisani, 1
Grimani, i Corner. Il che non è certo sufficiente a caratterizzare la
cultura e gli intenti del Barbaro, anche se offre alcune coordinate per
inquadrarne l'operato. Sin dal XV secolo, i 'papalisti' formano a
Venezia un gruppo che si distacca, per finalità politiche e per costumi
di vita, dal resto del patriziato, con una storia fatta di conflitti non
sempre palesi, resi però espliciti dalla crisi costituzionale del
1582-83 prima, dalla vicenda dell'Interdetto poi. Né poteva essere
altrimenti. Proprio in coincidenza delle guerre espansionistiche
nell'entroterra, alla ricerca di un primato nella penisola, Venezia
aveva consolidato la propria 'religione di Stato': una religione civica,
in cui l'identificazione della 'città-vergine' con il luogo della
perfetta giustizia tendeva a riaffermare l'indipendenza della
Serenissima dal primato pontificio, e non solo in materia temporale. I
'romanisti' costituivano pertanto un pericolo per tale linea tesa a
difendere in ogni campo l''unicità' di Venezia. Né va sottovalutato che
in seno alle famiglie legate a Roma e solidamente alleate fra loro,
grazie allo scambio e al controllo dei benefici ecclesiastici, fossero
vive tendenze oligarchiche di tipo assai spinto.3 Su tali orientamenti,
si innestavano, già nel XV secolo, afflati spiritualistici e spinte
indirizzate alla riforma della Chiesa vive nel cenobio di San Giorgio in
Alga e nell'ambiente di Gasparo Contarini: si tratta di tendenze che
Daniele Barbaro sembra aver assorbito nella sostanza del loro
evangelismo umanistico.

Ma è necessario precisare il ruolo di tali componenti nelle scelte
personali del Barbaro. Il quale, nato l'8 febbraio 1514 da Francesco e
da Elena di Alvise Pisani, è allievo, nello studio patavino, di
Marcantonio de' Passeri detto Genua, del Lampridio e di Federico
Delfino, l'autore del De fluxu et refluxu maris. Egli è inoltre membro
dell'Accademia degli Infiammati e nel 1545 risulta implicato nella
fondazione dell'Orto dei Semplici accanto a Pietro da Noale, professore
straordinario di medicina presso lo Studio di Padova, e all'architetto
Andrea Moroni, autore del progetto di sistemazione.* È questo, per
Barbaro — che frequenta nel frattempo Giovanni della Casa, Ludovico
Beccadelli, Bernardo Navagero, Benedetto Varchi, Matteo Macigni, Sperone
Speroni e Federico Badoer, il futuro fondatore dell'Accademia Veneziana
— un primo contatto con la res aedificatoria; un contatto, tuttavia,
assai limitato e tutt'altro che specifico. Non sono note altre relazioni
fra Daniele Barbaro e Andrea Moroni, al di là di quelle relative alla
programmazione dell'Orto Botanico; né è legittimo sopravvalutare la
funzione del futuro patriarca eletto di Aquileia in quell'opera.'
Rimangono pertanto velate le ragioni che spingono Barbaro a un così
intenso impegno sul testo di Vitruvio, su cui egli — come sappiamo da
una sua precisa ammissione — inizia a lavorare nel 1547: un anno prima,
dunque, della sua partenza come ambasciatore per l'Inghilterra (12
settembre 1548).° Né l'interesse per le arti di Ermolao - individuato da
Wendy Stedman Sheard come promotore, insieme al cugino Girolamo Donà,
del monumento al doge Andrea Vendramin' — è sufficiente a delineare una
tendenza familiare capace di spiegare il singolare exploit del Barbaro.

È infatti possibile che Daniele abbia inteso, con la sua opera
vitruviana, seguire le orme del prozio, autore delle Castigationes
plinianae e delle traduzioni di Aristotele. Ma rimane da spiegare cosa
abbia spinto un patrizio veneziano a scegliere come oggetto di intenso
studio il testo di Vitruvio negli anni a cavallo della metà del secolo
XVI. Né è sostenibile l'ipotesi di un vitruvianesimo serpeggiante sin
dal XV secolo fra le é/ttes veneziane. Indubbiamente, Bernardo Bembo è
interessato sia a Vitruvio che al De re aedificatoria di Leon Battista
Alberti,} mentre il Serlio fa intravedere un circolo di intendenti di
cui fanno parte 'dilettanti' come Francesco Zen e Gabriele Vendramin.?
Ma gli interessi del Bembo non sembrano valicare i confini di un
umanesimo interessato ad ogni espressione dell'antichità, così come non
specifici sono gli interessi artistici di Ermolao Barbaro e di Girolamo
Donà. D'altra parte, si è forse sopravalutato il 'rigorismo'
architettonico dei 'dilettanti' celebrati da Sebastiano Serlio. Almeno
per Francesco Zen un riscontro è possibile, dato che è lui stesso
l'autore del palazzo Zen presso l'antico convento dei Crociferi;'° e il
pastiche orientaleggiante della facciata di tale edificio — malgrado il
suo interesse specifico — parla di tutto fuorché di un purismo
vitruviano.

Fatto sta che l'architettura 'all'antica' è osteggiata, nella Venezia
del primo Cinquecento, dalla fedeltà alle tradizioni, dalle strutture
istituzionali, dalle forme del dibattito politico. Essa assumeva, hel
contesto lagunare, il significato di uno 'strappo', cul soltanto 1
'romanisti' erano disposti a sottomettere l'imago consolidata della
città. Tuttavia, negli ultimi anni del dogado Gritti (15231538), con le
opere sansoviniane nella platea marciana, la renovatio entra
trionfalmente nel cuore politico della Repubblica: ma con notevoli
difficoltà, mentre l'edilizia religiosa e quella privata battono — con
poche eccezioni — vie di compromesso o alternative.

In un tale clima, sottoporre al patriziato un'enciclopedia del sapere
tecnico fondata sul testo di Vitruvio assume un sapore inevitabilmente
polemico. Abbiamo un motivo di più per assumere come tema di analisi le
ragioni meno evidenti che hanno spinto Daniele Barbaro ad iniziare la
sua impresa: un tema stranamente ignorato finora dagli studiosi.

Iniziamo con il considerare alcune circostanze esterne. Dopo l'edizione
di Fra Giocondo (I511 e 1513), e le edizioni del Cesariano (1521) e del
Caporali (1536), le ricerche vitruviane ricevono un nuovo impulso grazie
al gruppo promosso, fra gli altri, da Claudio Tolomei, che fonda a Roma
l'Accademia della Virtù (1541-42), con il compito di chiarire
definitivamente i passi incerti di Vitruvio. Proprio alla fine del 1547
Tolomei è a Padova, reduce da Piacenza, sconvolta dall'assassinio di
Pier Luigi Farnese: su un possibile contatto TolomeiBarbaro è lecito
formulare un'ipotesi, come traccia, non foss'altro, per ulteriori
indagini al proposito. Che l'iniziativa di Daniele avesse anche il
significato di una civile competizione della cultura veneziana con
quella romana è probabile. Non va dimenticato che, secondo Francesco
Sansovino, qualcosa del genere si era verificato pochi anni prima,
quando il padre di Francesco, Jacopo, aveva sfidato gli intendenti di
architettura italiani — con la mediazione di Pietro Bembo — a risolvere
vitruvianamente il cantonale della Libreria marciana.'' Ma non va
dimenticato che il circolo del Tolomei cessa le sue attività intorno al
1545, quando il promotore parte da Roma. In una lettera scritta proprio
a Francesco Sansovino — non datata, ma anteriore al 1548 — Claudio
dichiara di non saper rispondere in merito alle questioni vitruviane su
cui era stato interpellato, essendosi "già quattro anni disviato da
cotali studi".'" Infine, va ricordato che proprio nel 1547 veniva
pubblicata a Venezia la lettera del Tolomei con il programma
dell'accademia vitruviana.'5

Quale senso attribuire, dunque, a un commento al De architectura, che
presuppone non solo intense ricerche di tipo archeologico, ma anche una
approfondita conoscenza delle tecniche e delle matematiche antiche?
Indubbiamente, i commentari del Barbaro, nelle due edizioni del 1556 e
del 1567, costituiscono una summa del sapere tecnico-scientifico antico
e moderno; e i continui riferimenti ad autori del Quattrocento e del
Cinquecento lasciano trapelare una concezione cumulativa del sapere
stesso. Barbaro mostra di conoscere non solo le fonti antiche e
medievali — Euclide, Tolomeo, Galeno, Boezio, Erone meccanico, 1
matematici arabi — ma anche l'/Horoscopio universalis di Johannes
Stabius, le opere matematiche e astronomiche di Johannes Werner, la
Compositio horologiorum di Sebastian Munster, l'Arte de navigacion di
Pedro de Medina, le opere di Leon Battista Alberti, Diùrer, Francesco di
Giorgio Martini, Serlio, Filandro, Francesco Maurolico, Federico
Delfino, Federico Commandino. Malgrado l'erudizione e l'alto livello
dell'indagine archeologica, i commentari del Barbaro sembrano validi
assai più come opera di chiarificazione, sistemazione e critica, che
come opera teoricamente originale.

Valutiamo, per prima cosa, l'apporto specifico offerto dal Barbaro agli
studi vitruviani. La traduzione da lui curata è indubbiamente la prima
che mostri una totale comprensione del testo latino: fino al XVIII
secolo, essa costituirà un riferimento indispensabile. Uguale cura
filologica è nel confronto fra il testo e la realtà dei monumenti
antichi. Nel 1554 Barbaro è a Roma, seguito dal Palladio, suo consulente
e autore delle più importanti tavole del trattato. È lo stesso Daniele a
rendere nota tale collaborazione, in un passo entusiastico nei confronti
dell'architetto,'' cui egli si riferisce più volte nel prosieguo dei
suoi commenti. Il rigore delle ricostruzioni archeologiche raggiunte da
Barbaro e Palladio è insuperato all'epoca. Ne emerge un 'codice', privo,
tuttavia, di dogmatismo. Gli ordini vengono analizzati con grande
minuzia, ma evitando di fissare astratte proporzioni canoniche; Daniele,
anzi, nota la varietà delle soluzioni antiche e interpreta le norme come
'ideali' interpretabili.

> Non si deve adunque alcuno dar meraviglia — scrive il Barbaro — se
> misurando le antichità di Roma, non rittrova spesso le misure delle
> colonne a punto, perché se egli si potesse vedere tutto il corpo,
> l'huomo non si meraviglierebbe della grandezza, o picciolezza de i
> membri, ma ritrovando un piede, o vero un braccio separato, non può
> dire questo piede, o questo braccio, è, grande, o picciolo ... Vedi
> che Vitruvio ci leva la soperstitione, l'obbligo, et la servitù senza
> ragione...'°

Dunque, è la symmetria dell'insieme che guida la scelta dei particolari.
La norma è un orizzonte di certezze, non una gabbia condizionante: siamo
lontani dall'astrazione precettistica della Regola del Vignola.'* Nel
commentare il tempio etrusco, Barbaro scrive che è opportuno adottarne
"alcune misure" e "mescolarle con gli altri generi", così da far
ammutolire

> quelli superstitiosi, che non vogliono preterire alcuni precetti dell'
> Architettura temendo, che ella sia tanto povera, che sempre formi le
> cose ad uno istesso modo, né sanno, che la ragione, è universale, ma
> l'applicarla è cosa d'ingenioso, e risvegliato Architetto, et che la
> bella mescolanza diletta, et le cose, che sono tutte ad un modo
> vengono in fastidio...''

L'elogio della "mescolanza" rinvia a simili passi del Serlio, lasciando
intravedere un comune terreno teorico relativo alla relazione
norma-licenza. Ma è indubbio che la licenza serliana sia antitetica a
quella presupposta dal Barbaro o sperimentata da Palladio, che gioca
ingegnosamente su mescolanze avvertibili soltanto da occhi esperti nel
cortile della Carità, nella facciata di San Francesco della Vigna, nella
loggia del Capitanio a Vicenza. Ed è indubbiamente rivelatrice
l'affermazione del Barbaro: "/o ho in odio non meno la soperstitione,
che la heresia"."* Un'affermazione significativa, che sembra costituire
un 'manifesto' relativo non soltanto all'architettura, visti gli
interessi religiosi dell'autore.

Ulteriori contributi fondamentali sono nella ricostruzione della domus
antica. Seguendo la corretta interpretazione dell' Alberti, ripresa da
Cesariano, Daniele riconosce che atrio e cavedio sono parti di uno
stesso cortile porticato: egli corregge pertanto un equivoco di Fra
Giocondo, che tuttavia era stato fecondo di soluzioni nei progetti di
Giuliano da Sangallo, di Antonio il Giovane, di Raffaello.'9 Nella domus
pubblicata nel libro VI °° è facile scorgere un Palladio interessato a
fissare il modello concettuale delle sue ricerche formali, specie
nell'ordine gigante riservato al vestibolo e all'atrio, in dialettica
con il doppio ordine del cortile. Né va dimenticato che la ricostruzione
del teatro antico, nel libro Vv, preceduta da lunghi passi sulle armonie
musicali ricondotte a rapporti aritmetici, segna una tappa non soltanto
per la cultura antiquaria, date le sue conseguenze nell'invenzione
palladiana dell'Olimpico. —

Il contributo di Palladio al Vitruvio va pertanto al di là della
consulenza archeologica e della pura espressione grafica. Il dialogo fra
testo e immagini, così come è impostato dal libro III in poi, rende
eloquente il trattato antico, grazie alla didascalica 'trasparenza'
delle ricostruzioni grafiche. Per le quali viene quasi sempre adottato
il metodo della proiezione piana formulato nella Lettera a Leone X.?' La
professionalità dei disegni palladiani relativi ai templi, agli ordini,
al teatro, alla domus, è la stessa dei suoi progetti coevi. La rigorosa
adozione della ortographia e della ichnographia permette la lettura e la
verifica dell'ordo metricoproporzionale e della symmetria che informa
gli organismi come tali.** A ciò, Palladio aggiunge una geometria
proiettiva fatta di trasparenze e di sovrapposizioni, tale da permettere
il contemporaneo controllo di membrature, piani e spazi dislocati a
profondità successive: la concezione albertiana e bramantesca
dell'edificio come un animans, come una 'macchina' fatta di correlazioni
organiche e necessarie, trova un'adeguata espressione grafica.

Nelle illustrazioni relative al tempio ionico (1556, pp. 86-7; 1567, pp.
138-9), al capitello ionico (1556, p. 95), ai portali posti sul fondo di
portici (1556, pp. 119, 120, 170; 1567, pp. 189, 191, 281), il metodo
della trasparenza è analogo a quello che appare, ad esempio, negli studi
per palazzo Da Porto-Festa (RIBA, XVII 127, 127), che hanno ingannato
l'occhio del Forssman a causa della loro novità grafica.*3 E va
osservato, per inciso, che esiste un legame fra tale rappresentazione
simultanea di piani successivi e la poetica palladiana delle facciate 'a
intersezione”.

La collaborazione fra Barbaro e Palladio costituisce un evento di grande
portata nella cultura architettonica del Cinquecento, anche se è
opportuno non schiacciare l'una sull'altra le due personalità. Com'è
evidente nel caso della villa di Maser — la meno palladiana delle opere
in cui il Palladio è implicato — l'interpretazione della norma, da parte
di Barbaro, è ben diversa da quella di Andrea.


È necessario approfondire cosa significasse, per il Barbaro, l'approccio
scientifico alla res aedificatoria. Va notato, innanzi tutto, un fatto
finora rimasto trascurato. Dai commentari emerge una razionalità tesa
all'evidenza dei propri procedimenti, in assenza di ogni esoterismo. E
enorme la distanza fra le considerazioni del Barbaro in merito ai
rapporti armonici, e la mistica numerica del De Harmonia Mundi di
Francesco Zorzi (1534); un'opera neoplatonica informata alla Kabbalah e
all'ermetismo, ben presto divenuta sospetta in ambito ecclesiastico.**
In tal senso, è sintomatico il passo contro le 'grottesche' inserito nel
commento al cap. V del libro vII di Vitruvio. Le grottesche sono
condannate in quanto pitture non imitative, innaturali, 'sofistiche',
frutti di mostruose e inverosimili fantasie.*

Ebbene, tale razionalismo radicale, e persino la semplificazione del
pensiero aristotelico che informa le pagine del Vitruvio, rimandano a
specifici interessi teologici dell'autore. Durante l'ambasceria in
Inghilterra, fra il 1549 e il 1550, Barbaro invia alla zia Cornelia,
suora nel convento di Santa Chiara a Murano, una serie di lettere
teologiche, da cui trapela una particolare lettura del Brevi/oquium di
Bonaventura da Bagnoregic. Come ha rilevato Peter Laven, i medesimi
principi di razionalità, semplicità e trasparenza, con cui Bonaventura
interpreta il revelatum, caratterizzeranno più tardi le pagine dei
commentari.? La stretta alleanza fra razionalismo umanistico e verità di
fede fa parte dello specifico atteggiamento del Barbaro, che si
inserisce nella storia dell'umanesimo cristiano con posizioni ereditate
più tardi dal cardinal Paleotti. Contro le eresie protestanti, Barbaro
redigerà, nel 1569, la sua Aurea Catena, su consiglio del cardinal
Sirleto; contro il dogmatismo antiumanistico, esprimerà posizioni
illuminate nell'ultima sessione del Concilio di Trento, a proposito
dell'Indice sui libri.”

Abbiamo ora nuove coordinate per comprendere la logica che informa il
suo concetto dell'architettura, vista come gioco pitagorico fondato su
una matematica qualitativa. Nessuna contraddizione, tuttavia, con quanto
Barbaro stesso aveva espresso in uno dei suoi primi scritti, la Predica
dei sogni edita da Francesco Marcolini — il medesimo editore del
Vitruvio — nel 1542.

Questo mondo / è un sogno — scriveva Barbaro — e noi mortali / poveri,
ciechi e frali / altro che fumo et ombra / non siamo... sotto la Luna /
non si trova alcuna / stabilitade o cosa / che non sia paurosa / come un
sogno.

Il tema della vita come sogno, che presuppone un risveglio per la vita
eterna, si unisce al tema del "sogno vero", che dà frutti positivi:

... l'opre leggiadre, / le belle inventioni, / i puri e bei sermoni / in
sonno son nasciuti, / e i dubbi soluti, / che quando vigilavi / t'eran
nascosi, gravi.

Nei cinque sonetti dedicati al dubbio si fa strada un aspetto
gnoseologico, specifico del dubbio stesso:

Colui ch'innanzi la sentenza pone / suo cor in dubbio, apprezza quel
ch'è vero ... Il dubbio è padre dell'inventione, / perché risveglia il
languido pensiero; / il dubbio pugne, isferza e fa leggiero / chi tardo
e pigro cerca la cagione.°9

La ricerca delle "cagioni": si tratta della virtù attribuita dal Barbaro
al "vero architetto". L'architetto, egli scrive infatti, è

capo, soprastante, et regolatore di tutti l'Artefici; come quello, che
non sia prima a tanto grado salito, ch'egli non s'habbia in molte, et
diverse opere, et dottrine essercitato: soprastando adunque dimostra,
dissegna, distribuisce, et commanda; et in questi uffici appare la
dignità all'Archi tettura esser alla Sapienza vicina: et come virtù
Heroica nel mezzo di tutte l'Arti dimorare, perché sola intende le
cagioni.*°

Architettura come scientia, dunque, risultato di una ricerca pungolata
dal dubbio, disciplina che pretende un comando indiscusso sulle 'arti':
dal punto di Vista teorico, l'unità corporativa delle 'arti' stesse
viene dissolta a favore di un "soprastante, et regolatore", dotato di un
sapere che "intende le cagioni". A tale figura che fa risplendere
nell'opera la "virtù Heroica" si apre un ulteriore dominio: la
relazione, chiaramente istituita, fra sapere e potere, è chiamata a
intervenire nella modificazione dell'intero ambiente fisico. Il che
comporta un problema relativo al rapporto fra tecnica e natura.

La natura, per Barbaro, è un orizzonte con cui l'attività umana è
chiamata a confrontarsi in un doppio esercizio: di riconoscimento della
divina e razionale armonia in essa immanente, e di gara tesa a piegare
alle necessità della vita civile quanto nella natura stessa sì presenta
ostile. L''arte' è chiamata a vincere la natura, dunque. Non si tratta
di una contraddizione o di una "confusione di significati", come
vorrebbe Laven,3' ma di una concezione aderente ad interpretazioni
patristiche e medievali. Le radici del doppio atteggiamento del Barbaro
nei confronti della natura sono nel Breviloguium di Bonaventura da
Bagnoregio ** e nella Reductio dello stesso autore, dove, a proposito
della "luce della capacità tecnica", è richiamato il Didascalicon di Ugo
di San Vittore.83

Le belle inventioni de gli huomini ... fatte a commune utilità — scrive
il Barbaro nella dedica al cardinale Ippolito d'Este 34 — portano a chi
non le intende meraviglia, et a chi le intende diletto grandissimo,
perché a quelli pare, che la natura sia vinta, e superata dall'arte, a
questi fatta migliore, et perfetta.

L'idea di una perfettibilità della natura è implicita nell'analogia, di
origine stoica, fra macrocosmo e microcosmo; tema riapparso nel XII
secolo all'interno della scuola di Chartres ed ereditato dalla
riflessione umanistica. Nel Barbaro, però, è costante la preoccupazione
di giustificare teoricamente la Vittoria della ratio sugli ostacoli
naturali:

Facendo adunque la natura alcune cose contra l'utilità de gli huomini,
et operando sempre ad uno istesso modo — egli scrive — è necessario che
a questa contrarietà si trovi un modo, che pieghi la natura al bisogno,
et all'uso humano. Questo modo è riposto nell'aiuto dell'Arte, con la
quale si vince la natura in quelle cose, nelle quali essa natura vince
noi.*

E in un passo del proemio dei commentari, la virtus della res
aedificatoria è individuata nella sua destinazione civile — il "riunire
in unità uomini rozzi, ridotti a culto e disciplina nelle Città" — e nel
suo esplicitarsi in una intensa e vasta opera di trasformazione
dell'ambiente fisico. Nel tagliare rupi, forare monti, riempire valli,
bonificare paludi, nel modificare il corso dei fiumi, nella costruzione
di navi, porti e ponti, il Barbaro indica il concreto carattere
'produttivo' dell'architettura, da armonizzare con le premesse e i
presupposti imitativi e assimilativi.3

In altra sede, abbiamo tentato di dimostrare quanto tale passo — che
parafrasa un analogo brano del De re aedificatoria di Leon Battista
Alberti — sia profondamente anti-albertiàano nella sua sostanza.” Qui è
piuttosto necessario insistere sulla logica che permette al Barbaro
un'attribuzione di senso così pregnante.

Si noti: sin dal commento al libro I di Vitruvio, il patriarca eletto di
Aquileia trasporta l'architettura nel regno dell'etica e della
conoscenza. Si tratta di una conoscenza certa e superiore, ben diversa,
questa volta, da quella riservata alle tecniche dalla Reductio di
Bonaventura. Infatti "nel conoscimento, et nel giudicio ella
[l'architettura] può essere con la Sapienza, et con la prudenza,
meritatamente paragonata, et per l'operare tra le arti come Heroica
Virtù chiaramente riluce".8* Sapienza e prudenza: essenzialmente, arti
di governo. Aristotele, nella Politica, aveva parlato metaforicamente
del politico come architetto; e che tale metafora fosse presente alla
cultura umanistica lo dimostra l'interesse mostrato per Vitruvio da un
politologo come Francesco Patrizi senese. Il Patrizi, vescovo di Gaeta,
nel 1461 aveva scritto ad Agostino Patrizi Piccolomini, comunicandogli
che, per dar compimento al suo De institutione rei publicae, gli era
necessario lo studio di Vitruvio. Non avendo a disposizione un codice
vitruviano — aveva aggiunto — egli lo chiederà in prestito all'amico
Roverella, vescovo di Ferrara. L'esempio addotto è tutt'altro che unico
nella cultura del Quattrocento e del Cinquecento. Proportio, eurythmia,
symmetria: 1 presupposti dell'estetica vitruviana rimandano a un
tentativo di riduzione del molteplice all'unità, in cui è immediato
leggere in trasparenza l'imago del 'buon governo'. Puntualmente, questa
viene raffigurata nell'opera di Patrizi senese nell'attività
dell'optimus architectus ordinatore della polis. Nel clima veneziano, è
nel De bene instituta re publica di Domenico Morosini che vengono
avanzate simili istanze.*°

Si può dire di più. Poiché l'architettura assorbe — per l' Alberti come
per il Barbaro — le verità superiori del sapere matematico tradotte in
proporzioni armoniche, in quel tentativo di dominio sul molteplice è
anche l'eco del 'bello come figura del bene' teorizzato da Platone nel
Filebo e nella Repubblica. Il Socrate che vien fatto parlare nella
Repubblica tratta l'idea del bene come qualcosa di difficile
comprensione — osserva Gadamer *' — riconoscibile solo dai suoì effetti.
Il bene esiste per noi soltanto nel dono da esso arrecato: conoscenza e
verità. Il che ha un significato profondo. Nel contesto della Repubblica
11 bene si presenta come l'unificatore supremo. Nel Febo, l'idea del
bene esercita una funzione di orientamento pratico alla vita giusta,
mescolanza di 'piacere' e di 'sapere': una mescolanza retta da un bello
definito dalle idee di misura, proporzione e razionalità. La cultura
rinascimentale riflette inoltre a lungo su un testo antico in cui le
idee platoniche vengono riproposte con coloriture latamente stoiche. Nel
De officiis, Cicerone insiste sulla categoria del decorum, definito in
analogia alla concinnitas del corpo umano: misura, temperanza e armonia
sono elette a parametri dei comportamenti privati e della gestione della
cosa pubblica. L'equivalenza ciceroniana fra l'onesto e l'utile è
fondata su un concetto ricco di implicazioni per gli ideali armonici
perseguiti dalle arti visive.

Daniele Barbaro risulta aver assorbito tali grandi temi. Se il bene si
'nasconde' nel bello, e se il 'numero' permette l'intelligibilità e
costituisce la 'svelatezza' (aletheia) dell'ordine armonico
dell'universo, è possibile la costruzione di una techne che rimandi
all'ideale, fondandosi sulla dynamis del bene stesso. L'architettura,
l'arte di 'ridurre a unità', si conferma come strumento fondante la
retta polis. Sua è la specifica tecnica del 'misurare' — il criterio del
'giusto mezzo' —, dunque suo è il principio fondamentale dell'etica
aristotelica. Ma suo è anche il 'numero', che svela l'ordine aprendo al
bene.

È dunque plausibile pensare che per Daniele Barbaro l'architettura sia
una grande metafora. Arte che unifica il molteplice, essa significa una
totalità di comportamenti, una strutturazione con-sonante della
pluralità, chiamata a introdurre concordia in una bene instituta
republica. Il "bene" coincide con l'ingresso trionfale, nelle strutture
istituzionali e nella mentalità collettiva, di 'ragioni' sottratte alla
doxa. Il che presuppone, per i commentari e per l'intera attività dei
fratelli Barbaro, una finalità politica da indagare nelle sue specifiche
articolazioni.

La sostanza polemica, nei confronti della tradizione veneziana, di
un'architettura come 'Sapienza', non può, al proposito, sfuggire.
L'obiettivo costante contro cul combatte la 'scienza' del Barbaro è
l'empiria dei 'proti': dei tecnici sine scientta, forniti di un sapere
fondato sull'empiria e sulla tradizione, del tutto soggetti alle scelte
delle magistrature e dei provveditori 'alle fabbriche. Figure come
quelle di Giorgio Spavento, Bartolomeo e Piero Bon, Antonio Abbondi
detto lo Scarpagnino — proti dei Procuratori de supra o della
Magistratura del Sal — non possono in alcun modo essere assimilati alla
nuova figura umanistica dell''architetto'. L'incertezza dei loro
linguaggi deriva dal loro approccio pragmatico alla forma; il vantaggio
che essi assicurano alle istituzioni veneziane è proporzionale alla loro
incapacità a formulare 'programmi', assicurati invece alle magistrature
patrizie. Il loro 'far di pratica' aveva tuttavia permesso — anche
nell'edilizia privata — uno sviluppo della forma urbana continua e
omogenea. Le innovazioni dei pochi maestri maggiori (i Lombardo, Mauro
Codussi) erano state da loro ricondotte nell'alveo di tipologie
collaudate dal tempo. In tal modo, Venezia aveva evitato lo choc della
novitas assoluta.

Uno degli obiettivi del Vitruvio del Barbaro è la rottura di tale
tradizione. Ma ciò non comporta una sottovalutazione dell''esperienza':
un'impostazione etica della disciplina architettonica non permette di
prescindere dal concetto aristotelico di phronesis.!?

L'isperienza adunque — scrive il Barbaro 4 — è simile all'orma, che ci
dimostra le Fiere perché sì come l'orma èprincipio di ritrovare il
Cervo, né però è parte del Cervo ... così l'isperienza è principio di
ritrovar le Arti, et non è parte di alcun'Arte, perché le cose a' sensi
sottoposte non sono Principi) delle Arti, ma occasioni, come chiaramente
si vede, perché il Principio delle Arti è universale, et non sottoposto
a' Sensi humani, benché da' Sensi stato sia trovato.

"Occasione" e universalità dei "principi": 'fabbrica' e 'discorso', per
il Barbaro, sono uniti, come vuole Vitruvio, ma con un indiscusso
privilegio per i "principil", unici garanti della certa verità (della
giusta finalità). Tali principii sono quelli delle matematiche insite da
Dio nell'universo. La triade "pondo, numero et mensura" menzionata nel
libro della Sapienza (11, 20), e ripresa da Boezio, da Agostino, da
Bonaventura,* ritorna legittimata dalla tradizione platonico-pitagorica.
Barbaro può così scrivere che

La Mathematica è quella, che non più riguarda al senso, ma è facultà di
giudicare secondo la speculatione, et la proposta ragione conveniente
alla Musica de i numeri sonori, et de i modi, et delle maniere delle
Canzoni, et de i mescolamenti, et de i versi de' Poeti, forsi più alto
salendo la Humana, et Mondana convenienza de i Cieli, et dell'Anima va
conside rando.*

La superiorità dell'architettura è affermata — su base matematica — sin
dalle prime pagine dei commentari:

a conoscere l'Arti più degne questa è la via: che quelle, nelle quali fa
bisogno l'Arte del numerare, la Geometria, et l'altre Mathematice, tutte
hanno del grande, il rimanente senza le dette Arti, (come dice Platone)
è vile, et abietto come cosa nata da semplice imaginatione, fallace
coniettu ra, et dal vero abbandonata Isperienza.*°

L'"Isperienza dal vero abbandonata" è quella che aveva dominato la
prassi dei 'protomagistri' nella Venezia del XV e del XVI secolo, con
l'eccezione di Jacopo Sansovino. Ma le opere sansoviniane posteriori al
1598-40 non potevano soddisfare il Barbaro, che del resto si mostra
assai tiepido nei confronti dell'artista fiorentino. Rispetto alle
continue lodi riservate al Palladio, nell'edizione del 1556, le scarse
menzioni del Sansovino sono significative: Daniele loda Jacopo per aver
inserito nelle metope della Libreria il leone alato, insegna della
Serenissima,* e cita la sua opera per la Zecca senza commento.

La critica alla pratica senza teoria rinvia alla mentalità di un
patriziato che appare al Barbaro pigramente soddisfatto di consuetudini
non sottoposte alla critica umanistica. L'insistere sulla 'teoria' è un
esplicito 'manifesto' rivolto al patriziato veneziano, ancor prima che
agli architetti. La funzione politica dei commentari trapela già dalla
scelta preventiva compiuta da Barbaro: è una sorta di appello al
rinnovamento — traumatico per Venezia — delle attrezzature mentali
consolidate, che la proposta 'romanista' contenuta nel Vitruvio del 1556
rivolge al ceto dirigente. Barbaro presenta tale romanismo sotto una
luce universalista; ma la sua proposta non poteva che suonare sospetta
al patriziato conservatore o antipapalista. Quell'universalismo,
infatti, appariva troppo connesso a quello della curia pontificia. La
stessa architettura come rigorosa ricostruzione delle sintassi antiche
parla a Venezia la lingua di una verità esclusiva e metastorica,
difficilmente accordabile con l'immagine fisica e ideale che la
'città-vergine' si era costruita nel tempo.

Il fondamento dell'attacco del Barbaro alla doxa degli empirici era
rinvenibile sia in Platone che in Aristotele. La techne, per il primo,
non sa nulla del 'bene in sè', procede senza accorgersi delle nebbie che
ne avvolgono il telos. Solo 'oltre il sensibile' si installa la
percezione del fine; musica e astronomia sono al contrario 'virtuose',
dato che rendono 'pure' le matematiche. Nell'Etica Nicomachea,
d'altronde, Aristotele osserva che la subordinazione del produrre
all'uso sbarra alla techne la strada verso l'areté.** L'insistere del
Barbaro sul carattere di "virtù Heroica" dell'architettura trae le
logiche conseguenze dal fondamento etico insito nel sapere 'per
principii'. È ancora ad Aristotele che il Barbaro attinge nel definire
la scientia una sintesi di nous ed episteme; anche se dell'episteme
Daniele dà un'interpretazione personale, identificandola con la certezza
delle matematiche.

Ma il punto è un altro. Vitruvio e il richiamo all'Antico soddisfano
l'impellente bisogno di norma vivo da più di un secolo. La norma,
all'interno della nuova 'età dell'immagine del mondo', significa
strumento di dominio sul futuro, potente esorcismo dell'imprevedibile,
dell'evento, del caso, tramite un eidos fissato per via filologica.

Pro-gettare, in tale ambito culturale, significa volere
'prospetticamente' il futuro: la volontà di norma si proietta nella
perfezione dell''idea' al di là del presente, presupponendo l'assoluta
stabilità del 'Vero'. E la norma più potente sarà quella eticamente
legittimata.** I motivi polemici dei commentari del Barbaro emergono più
chiaramente. Da un lato, il presupposto di una Repubblica capace di
rinnovarsi accogliendo all'interno delle proprie istituzioni un sapere
antiempirico. Dall'altro, l'esigenza di tenere uniti i saperi attraverso
una teoria forte', che compensi o addirittura scongiuri la
disseminazione degli specialismi.

Abbiamo più volte insistito sul fatto che l'architettura preconizzata da
Barbaro e realizzata dal Palladio non poteva che trovare resistenze a
Venezia, dove il sapere e il felos sono gelose prerogative di un
patriziato che guida l'operato di tecnici che non spezzino la
tradizionale struttura delle 'arti'. Ma anche in Daniele Barbaro vive
l'esigenza di mantenere la 'teoria' in mani patrizie. La sua summa
vitruviana esprime compiutamente la distanza implicita mente posta fra
ceti e ambiti di competenza. Lo scientismo che si esprime nei commentari
si riallaccia, peraltro, a tendenze già vive nell'ambiente veneziano,
dai tempi di Giorgio Valla almeno, specie tenendo presente i tentativi
compiuti sotto il dogado di Andrea Gritti. È piuttosto importante
interrogare il testo del Barbaro circa i fini immediati che esso si
propone.

I commentari assumono un valore particolare qualora si faccia attenzione
ad alcune pieghe contenute in essi. Nel testo si annidano indicazioni
relative ai problemi fondamentali della realtà veneziana: la tecnica —
l'artificium — viene invocata in funzione della più 'artificiosa' delle
città. Quattro sembrano le funzioni primarie cui l'auspicata res
aedificatoria elevata a scientia è chiamata a rispondere. E per ognuna
di esse Barbaro esprime posizioni che implicano profondi rinnovamenti
nei costumi e nelle istituzioni veneziane.

a) La difesa della città e dei Domini: Barbaro pubblica, alla fine del
libro 1 dell'edizione del 1556, il sommario del Libro delle
fortificazioni di Giovan Jacopo Leonardi, la cui redazione era stata
pressoché completata nel 1553. Il rinvio è altamente significativo. Il
trattato del Leonardi riflette i principii dell'ars fortificatoria di
Francesco Maria della Rovere, i cui allacci con lo scientismo favorito
dalla cerchia di Andrea Gritti sono stati acutamente messi in luce da
Ennio Concina.8 Lo stesso Concina ha dimostrato — sulla base di puntuali
riscontri — come fra il Vitruvio del Barbaro, il Libro di Leonardi e i
Quattro libri palladiani esista uno stretto legame: la securitas — nelle
concezioni di Francesco Maria e di Leonardi — è raggiungibile, da parte
della Serenissima, esaltando la via speculativa, in stretta associazione
con una decisa volontà politica e una specializzazione dei saperi. La
riforma dell'ars fortificatoria preconizzata da della Rovere, dal
Leonardi, dal Barbaro, è basata su un accentramento delle scelte in mano
di specialisti: una riforma che aveva suscitato notevoli opposizioni
contro Francesco Maria, e che, fino alla fine del secolo, rimarrà
inattuata e ostacolata.

b) La ristrutturazione del Cantiere di Stato, l' Arsenale: nel capitolo
xII del libro v, Barbaro esalta il ruolo di Nicolò Zen nella
ristrutturazione dell' Arsenale, ricordando, nella seconda edizione
dell'opera, gli esperimenti svolti da lui stesso con macchine adibite al
sollevamento di pesi eccezionali:9 abbiamo un'ulteriore indicazione
relativa all'uso civile della tecnica sottoposta alla ratto umanistica.
Né va sottovalutato che Nicolò Zen, proprio nel 1556, viene eletto
provveditore della nuova magistratura dei Beni Inculti, oltre che
provveditore al Cantiere di Stato.

La modernizzazione dell'Arsenale costituiva un tema spinoso. Vettor
Fausto aveva tentato di introdurre la nuova figura dell'architectus
navalis, con una 'usanza nuova' parallela a quella poi sostenuta dal
Palladio; ma il suo tentativo era stato accuratamente marginalizzato. Le
irrazionalità e le contraddizioni che agitano le vicende dell'Arsenale
per l'intero secolo XVI impediscono a Venezia di adeguare la propria
produzione navale ai nuovi standard europei, accentuando la crisi delle
mercature. L'empiria tradizionale viene contrapposta agli sforzi degli
innovatori, in nome di una continuità diffidente nei confronti
dell'accentramento dei poteri e della specializzazione dei saperi. Il
patrizio che continuamente muta le proprie cariche è, per istituzione,
un 'dilettante' in tutti 1 rami dello scibile; ogni riforma basata su
livelli alti di razionalità e teoria minaccia un cardine della
costituzione veneziana. I passi sull' Arsenale contenuti nel Vitruvio
del 1556 e del 1567, per quanto celebrativi ed encomiastici, nascondono
un motivo critico, dato l'accento posto sull'operato di uno dei
riformatori più radicali del Cantiere di Stato.

c) La preservazione della laguna: è un tema che appare nella seconda
edizione dei commentari, e in modo singolare. Barbaro enumera tre
elementi, il tempo, il mare e la terra, che sembrano congiurare insieme
in una 'guerra' scatenata contro la realtà fisica della Venezia
lagunare, "dico il mare, et la terra" egli scrive 5° "de i quali l'uno
pare, che voglia cedere, et l'altra occupare il luogo di queste lagune".
Torna il motivo della 'Natura' "talvolta nemica all'utilità de gli
huomini", associato al motivo del 'tempo corruttore'. Diviene evidente
il ruolo assegnato alla tecnica in questo delicato settore, al centro
delle preoccupazioni veneziane da più di un secolo. La tecnica ha
compiti di 'restauro' nella lotta contro le degenerazioni della
corruttibile. 'Natura' provocate da 'Kronos'. Più che 'innovare',
nell'assumere il comando della situazione idrogeologica, la tecnica deve
ripristinare equilibri spezzati. Né ci sembra casuale che il passo sia
stata aggiunto da Daniele Barbaro dopo la morte di Cristoforo Sabbadino,
il geniale proto dei Savi ed Esecutori alle Acque che aveva combattuto
con rara perizia contro gli interessi dei 'particulari', pericolosi per
l'equilibrio lagunare.

Sì legga come Barbaro affronta il tema:

però con lo essercitare de gli ingegni, et de gli animi de i Senatori,
in una grandissima impresa vuole, che'l mondo veda la grandezza dello
stato loro, la prudenza de gli huomini, et l'amore di giovare alla
patria dove sarà opera di speculatori della natura, et de i pratichi,
investigare le cause della atterratione di queste lagune, come sogliono
fare i medici, che prima considerano le cause delle infermità, et poi
danno i rimedij opportuni: troveranno, che la terra usa i fiumi in
questa usurpatione, che ella vuol fare, et da quelli si fa portare nelle
acque salse: troveranno, che le acque salse di loro natura rodeno, e
consumano le immonditie; troveranno, che più acqua salsa, che entra in
questa laguna è meglio, perché uscendo con maggiore empito porta via
poco terreno ... però moveranno quelli terreni, che già sono alquanto
induriti ... drizzeranno i canali, et i corsi delle acque, impediranno
la mescolanza delle dolci con le salate, faranno de gli argini, et non
lascieranno molto spacio. oltra quelli arare, et movere i terreni. et
finalmente condurranno quanto più da lontano si può i fiumi...57

In tale passo — mantenuto nell'edizione latina, anche se abbreviato — è
il riassunto dei grandi temi al centro della polemica fra Cristoforo
Sabbadino e Alvise Cornaro: fra le tesi che privilegiano lo 'Stato da
Mar”, con un attacco all'operato anarchico dei privati nell'entroterra,
e quelle che adottano l'ottica dei bonificatori, difendendo il primato
dello 'Stato da Terra'. Barbaro sembra adottare un'ipotesi mediatrice.
Il suo obiettivo è mettere a fuoco la necessità di Scelte e di
interventi guidati da una tecnica solidamente fondata. Non aveva egli
Stesso incluso nei compiti della res aedificatoria la trasformazione e
la cura dell'assetto territoriale? Non è difficile decifrare, dietro le
sue parole, la preoccupazione derivante dall'assistere a interventi
condizionati da incertezze, frammentarietà, rimandi indefiniti,
compromessi dovuti al premere degli interessi privati. La ratio invocata
anche in questo settore è, una volta di più, espressione critica nei
confronti di politiche da riformare radicalmente.

d) La 'forma urbis' e il costume edilizio veneziano: è questo il tema su
cui la sostanza programmatica dei commentari è più esplicita,
esprimendosi con toni privi di ambiguità o reticenze.

La polemica esplode, con insolita violenza, nel finale del capitolo x
del libro VI. Dopo aver annunciato "un libro delle case private,
composto, et dissegnato dal Palladio",°° Daniele auspica un vero e
proprio 'ravvedimento' della committenza veneziana, legata a un costume
fatto di 'errori'.

Et se o posso pregare — egli scrive — prego e riprego specialmente
quelli della patria mia, che si ricordino, che non mancando loro le
ricchezze, et 11 poter fare cose honorate, voglino ancho provedere, che
non si desideri in essi l'ingegno, et il sapere, il che faranno, quando
si persuaderanno di non sapere quello, che veramente non sanno, né
possono sapere senza pratica, e fatica, e scienza. Et se gli pare che
l'usanza delle loro fabriche gli debbia esser maestra, s'ingannano
grandemente, perché in fatti, è troppo vitiosa, et mala usanza, et sì
pure vogliono concieder all'uso alcuna cosa, il che anch'io conciedo, di
gratia siano contenti di lasciar moderare quell'uso da chi sì ne
intende, perché molto bene con pratica, et ragione si può acconciare una
cosa, e temperarla in modo, che levatole il male, ella si riduca ad una
forma ragionevole, e tolerabile con avantaggio dell'uso, della
commodità, et della bellezza...'

La critica investe, esplicitamente, la tipologia della casa patrizia:
vale a dire, uno dei fondamenti dell'autoidentificazione del patriziato
come garante della 'libertà' repubblicana. La mitica 'legge Daula' aveva
fino ad allora funto da regolatrice di un costume edilizio preservato
gelosamente come signum individuationis dell'unità del ceto dirigente e
della 'sacra immagine' della città lagunare. È esattamente tale
tradizione, indipendente dai linguaggi — bizantino, gotico,
protoumanistico — su di essa depositati, che Barbaro pone sotto accusa.
Il supporto formale della continuità politica della Repubblica è
definito addirittura "vitiosa e mala usanza". Il Barbaro ribadisce poco
oltre, nel capitolo x1 dello stesso libro VI, l''errore' in cui i
Veneziani sono confitti, dopo aver condannato alcune incongruenze
costruttive:

Di questi errori e danni molti ne sono nella città nostra, nella quale a
me pare che gli huomini per hora deono più presto esser avvertiti, che
non incorrino ne gli errori, che ammaestrati, che facciano belli, et
ragionevoli edifici. benché esser non può, che non fabrichino senza
errore, quando non fabricheranno con ragione...

Il patriarca eletto di Aquileia si propone un severo compito pedagogico
nei confronti dei suoi compatrioti. La ricostruzione della domus antica
assume un senso particolare: il Barbaro invita ad abbandonare quanto vi
è di più specifico nella facies edilizia lagunare, per affidarsi alla
'sapienza' del 'vero architetto'. La lingua universale dell'antico è
contrapposta al 'dialetto' veneziano: ve n'era abbastanza perché il
'latino' propugnato dal Barbaro ed esemplificato dalle architetture del
Palladio fosse letto dal patriziato antiromanista come una sfrontata
provocazione. |

Nell'esaltare Palladio come architetto civile, il Barbaro non fa che
ribadire la sua concezione dell'architettura come "virtù Heroica". Non è
certo Palladio il tecnico adeguato a dare concretezza a piani di
fortificazione urbana o territoriale, né ha le competenze necessarie a
intervenire nei cicli produttivi dell' Arsenale o nel problema della
dinamica lagunare. L''usanza nuova', come la chiamerà Palladio,
presuppone infatti un'alta specializzazione del sapere. Lo stesso
Cristoforo Sabbadino aveva difeso strenuamente la propria disciplina dal
dilettantismo — nel campo dell'idraulica — di architetti e di
"inzegneri":®' l'architettura di Palladio è coerente con tutta una
tendenza che va configurando 'saperi speciali' e che respinge i proti
tradizionali verso ambiti esecutivi e collaterali, dominati
dall'episteme posseduta dallo 'scienziato'.

Barbaro opera attivamente nell'introdurre il Palladio a Venezia. Nel
gennaio 1558 (m.v.), insieme al fratello Marcantonio, Daniele appare
come garante, presso il patriarca di Venezia Vincenzo Diedo, dell'opera
di Palladio per la facciata (rimasta ineseguita) della chiesa di San
Pietro di Castello. Si tratta del primo importante incarico lagunare
affidato all'architetto, e non a caso per una chiesa simbolo della
sgradita presenza nella laguna di un emissario della curia romana.
Vincenzo Diedo sarà attaccato duramente in senato, nel 1559, da Giovanni
di Bernardo Donà, per non aver pagato 2000 ducati di tasse e per la sua
vita fastosa, con epiteti come "hippocrito" e "lupo rapace". Il che
conferma l'ambito 'romanista' dei committenti e degli amici veneziani
del Palladio, che opererà per il convento dei Canonici Lateranensi della
Carità e per la facciata di San Francesco della Vigna, grazie alla più
che probabile mediazione del Barbaro. Ed è ben noto il sostegno che il
maestro riceverà da Marcantonio Barbaro dopo la morte di Daniele (1570).
È dunque poco credibile l'ipotesi relativa a un raffreddamento del
Barbaro nei confronti del Palladio a causa di dissensi sorti durante la
fabbrica della villa di Maser. Palladio figura sia nel 1560 che nel 1563
come testimone ad atti di procura stipulati dal Barbaro nel palazzo di
San Vidal, e nel testamento del patriarca eletto di Aquileia è ricordato
come "nostro amorevole", con un lascito di 15 ducati.*

Piuttosto, è significativo che né i Barbaro, né Jacopo Contarini,° 65 né
Giovanni Grimani, né Domenico Bollani usino il Palladio per clamorosi
rinnovamenti dei loro palazzi famigliari. Si potrebbe vedere in ciò una
contraddizione rispetto alle parole del Barbaro che sottolineano il
valore particolare del maestro nel campo dell'edilizia residenziale. Si
tratta, invece, di un'oculata politica culturale. Palladio è utilizzato
dalla committenza partecipe del programma del Barbaro in due modi: è
chiamato a dar forma aulica, nell'entroterra, alla nuova ideologia della
"Santa Agricoltura"; è designato, a Venezia, a dar forma ai luoghi sacri
in Quanto 'siti pubblici'. Ma si badi: Palladio non viene mai assunto
ufficialmente come funzionario pubblico a Venezia e le difficoltà da lui
incontrate nelle vicende del Redentore, di Palazzo Ducale e forse in
quelle di Rialto parlano esplicitamente delle resistenze che si
oppongono al programma del Barbaro. Malgrado quanto si è fantasticato
impropriamente circa un Palladio 'proto della Serenissima' — carica,
peraltro, inesistente -, Andrea di Pietro della Gondola non ricopre a
Venezia il ruolo già assegnato a Jacopo Sansovino. I successori di
Jacopo come proti della Procuratia de Supra sono Giacomo Spavento e, dal
1572, Simon Sorella.

Forse, gli insuccessi palladiani ai concorsi pubblici del 1554 e del
1555 non sono stati senza influenze sulla tattica individuata dal
Barbaro per introdurre a Venezia la 'vera architettura': una tattica che
rinuncia a un inserimento istituzionale dell'architetto, con la
conseguenza di farne una figura appoggiata dalle forze 'papaliste'.
Tuttavia, quest'ultima è soltanto una conseguenza. Palladio è, per
Barbaro, la personificazione della sintesi di scientia e phronesis
invocata per strappare Venezia dal culto delle proprie tradizioni.
Inoltre, Palladio prefigura un nuovo professionista: un artista che
oppone alla fallace doxa un sistema di verità fondato su 'principil'.

In parte utopico, il programma del Barbaro. Egli sottovalutava il costo
che uno strappo dalle tradizioni avrebbe reso inevitabile per una
Venezia arroccata in un mito compensatorio, dopo lo choc della pace di
Bologna e la frustrante constatazione del ruolo secondario rivestito
all'interno del nuovo assetto europeo. Senza contare che la
'specificità' di Venezia veniva difesa da chi s1 proponeva, spezzando
l'egemonia dei Primi, di limitare il potere del Consiglio dei Dieci,
rilanciando la vocazione mercantile della Serenissima e aprendosi
all'Europa sulla base di una ritrovata autonomia dello Stato.®

Il conflitto culturale e politico che agita la Venezia del XVI secolo
assume toni tragici, specie considerando il suo esito. Al proposito, va
notato che, con i quattro settori di intervento sopra elencati, Barbaro
individua temi sui quali il dibattito veneziano permarrà scottante fino
alla fine del secolo. Le polemiche relative a Palmanova, le
contraddizioni e le crisi che agitano l'Arsenale, il modo in cui vengono
recepite le indicazioni offerte dal piano del Sabbadino del 1557, gli
accesi dibattiti degli anni '80 sui cantieri di Rialto, delle Procuratie
Nuove, delle chiese della Celestia e di San Nicolò da Tolentino, provano
l'esistenza di tendenze ben delineate, che rendono conflittuali le
decisioni proprio in merito al temi indicati dal Barbaro come i soggetti
specifici della res aedificatoria.®

La nuova ratio, che il Barbaro vorrebbe pienamente dispiegata nelle
strutture portanti della Serenissima, è tutt'altro che mentale. Essa,
tuttavia, non fa i conti con le leggi che guidano le scelte della
Repubblica: il paradigma scientista sembra richiedere decisionalità
concepibili soltanto in uno Stato assoluto.

Siamo ora in grado di rispondere al nostro quesito iniziale. Erano nel
giusto sia Bernardo Tasso che Sperone Speroni e Paolo Paruta. In Barbaro
è viva l'esigenza di un'azione guidata da teoria: di vita activa fondata
solidamente su vita contemplativa, dando a tale ultima
un'interpretazione sia civile che religiosa. É non è certo estranea alla
mentalità che Daniele Barbaro tenta di introdurre e consolidare a
Venezia l'espressione che Giovan Francesco Sagredo userà in una lettera
a Galileo dell'agosto 1611; invocando uno "stromento" capace di
distinguere "l'architetto intelligente da un proto ostinato e
ignorante".

Esiste un ulteriore spunto polemico nei commentari. Si tratta di un tema
che investe i costumi religiosi e l'atteggiamento patrizio nei confronti
della morte e della fama terrena.

L'intero commento del capitolo virt del libro IV è dedicato all'edilizia
ecclesiastica. Daniele ricorda la norma che vuole gli altari rivolti ad
oriente, aggiungendo: "se stiano meglio, più altari, o d'un solo lo
lascio decidere ad altri".?° Dopo aver consigliato di disporre le
sagrestie accanto al coro, "in quelle parti dove anticamente ne i Tempi
era il postico", egli dà consigli per i campanili e parla infine dei
cimiteri posti sul retro delle chiese:

Hanno dietro la Chiesa il Cimitero, dove si sepeliscono i corpi, luogo
Sacro, imperoché la bene ordinata nostra Religione ha voluto haver cura
del sepelire i corpi, essendo i corpi humani stati vasi dello spirito
Santo ... Ma Dio voglia, che a nostri Tempi non si facciano simili
uffici) più presto a pompa de' vivi, che a pietà, e consolatione de i
morti. Non è lodevole, che i monumenti, o sepulture siano nelle Chiese,
pure egli si usa a grandezza nelle capelle a questo con pregio
appropiate, et in luoghi eminenti si pongono più alte de i Sacri Altari,
et stappongono le memorie, 1 titoli gli Epigrammi, i Trofei, e le
insegne de gli antipassati, dove le vere effigie di bellissime, et
finissime pietre si vedono, et i gloriosi gesti in lettere d'oro
intagliati si leggono cose da esser poste più presto nel Foro, et nella
piazza, che nella Chiesa, et solamente de gli huomini illustri, et di
quelli le opere virtuose de i quali, esser possono di memorabile, et
imitabile essempio a i Cittadini.”

La polemica contro gli sfarzosi monumenti tombali all'interno delle
chiese ha un significato particolare a Venezia, ricordando il
proliferare, nelle chiese lagunari, degli apparati monumentali chiamati
ad assicurare la fama terrena di patrizi e condottieri della Repubblica.
Con un processo già avviato nel XV secolo, con i monumenti di Antonio
Rizzo, Pietro e Tullio Lombardo, Lorenzo Bregno e Giovanni Buora, chiese
come quelle dei Frari o dei Santi-Giovanni e Paolo si erano andate
trasformando in veri e propri Pantheon di dogi, eroi militari,
benemeriti della Repubblica. La critica del Barbaro ha un precedente
nelle Constitutiones del vescovo Giberti per Verona (1542), e sarebbe
scorretto leggervi semplicemente un atteggiamento pretridentino. Il tono
è piuttosto erasmiano, e non è escluso che agissero sul Barbaro residui
di temi agitati dal pensiero evangelico. Atteggiamenti congruenti con
quelli espressi dal patriarca eletto di Aquileia hanno precedenti in
scelte di patrizi veneziani a lui vicini. Marco Grimani — fratello del
cardinale Marino, di Vettore e di Giovanni, patriarca di Aquileia —
ordina la propria sepoltura in uno dei chiostri della chiesa di
Sant'Antonio di Castello, aggiungendo: "né per via nessuna voglio esser
sepolto in chiesa, perché né a me, né ad altri si conviene seppellire
l'ossa de peccatori et massime le mie in luoghi simili, né voglio gli
sia altare né si ne facci, acciò non si celebrasse in alcun tempo dove
riposassero l'ossa d'un peccatore".'?

Ancor più significativa è la volontà dello stesso Daniele Barbaro, che
ordina di venire sepolto nel cimitero del convento francescano di San
Francesco della Vigna — il 4 gennaio 1561 egli era stato nominato
"defensor et conservator omnium privilegiorum Seraphici Ordinis et
Religionis Sancti Francisci de Observantia" — invece che nella tomba di
famiglia. Per far rispettare la volontà del Barbaro dovrà intervenire
Pio IV, contro la Signoria che avrebbe voluto onorare la memoria di
Daniele in modo più eloquente: "et così fo sepulto in Campo Santo per
mezo l'organo, con un monte de terra alto, et per reverentia, acciò se
cognoscesse dove era sepulto".?*

L'umiltà evengelica è contrapposta all'appropriazione dei luoghi sacri
da parte di cittadini e patrizi spinti da un profano esibizionismo, che
non esita ad investire le facciate stesse delle chiese. Ancora una
volta, il Barbaro appare fautore di una renovatio conforme allo spirito
dei riformisti cattolici, anche per la combinazione di evangelismo e
aristocraticità che lo ispira.

Rimane da valutare il ruolo svolto dai commentari all'interno del
dibattito scientifico cinquecentesco. A tale scopo è utile esaminare il
testo in margine al libro IX di Vitruvio, da cui emerge un interesse
specifico dell'autore, che si impegna con particolare applicazione allo
studio della gnomonica. È di Vitruvio la definizione della gnomonica
come "seconda parte dell'architettura", e certo il Barbaro ha colto il
contributo originale offerto dallo scrittore antico con l'enunciazione
teorica dell''analemma', vale a dire - semplificando — del metodo di
proiezione del moto del sole sul piano del meridiano.' Il valore della
gnomonica è precisato dallo stesso Barbaro, scrivendo che grazie ad essa
"si vede gli effetti, che fanno i lucenti corpi del Cielo con i raggi
loro nel mondo": ne deriva la possibilità di trascendere la condizione
mondana, per contemplare la divinità del cielo.? C°è qualcosa di
particolare nella dedizione del Barbaro a tali studi. Il modello supremo
cui attinge la res aedificatoria nel suo tentativo di imporre
consonantia al reale — l'armonia universale — è reso rappresentabile,
sottoposto a regole di costruzione geometrica, captato dal soggetto che
riduce a immagine gli enti, tramite la tecnica di costruzione
dell'analemma. È poiché l'analemma stesso è al fondamento della
realizzazione degli gnomoni, quell'imago della "gran macchina"
dell'universo capta lo scorrere del tempo universale riducendolo a
successione numerica, a diacronia calcolabile.

La gnomonica legittima la concezione 'sferica' che fa dell'architectura
un'immagine del vero universale: in essa, 'rappresentare' e 'costruire'
coincidono, in una virtuosa geometrizzazione proiettiva del 'libro
dell'universo'. L'impegno di Barbaro è al proposito testimoniato dal
manoscritto inedito De horologiis describendis libellus,° che sembra
formato da appunti per la redazione dei commenti al libro IX vitruviano,
ma anche per la rielaborazione del testo in occasione dell'edizione del
1567. Era infatti accaduto qualcosa, nel frattempo, e di non poca
importanza. Nel 1562, Federico Commandino aveva pubblicato, con dedica
al cardinale Ranuccio Farnese e con un impegnativo commento, il libro di
Tolomeo sull'analemma: un'opera scritta prima dell'Almagesto e in cui
Tolomeo - un secolo circa dopo il testo di Vitruvio — aveva posto le
basi della trigonometria sferica.” Commandino aveva unito a quell'opera
un Liber de Horologiorum, dove aveva affrontato il tema - non
esplicitato da Tolomeo dell'applicazione dell'analemma ai quadranti
solari. Barbaro recepisce immediatamente l'importanza dell'opera e se ne
serve nella rielaborazione del commento al libro IX, rendendo omaggio
sia al Commandino che a Francesco Maurolico per i loro apporti
innovatori.”

Di nuovo, si potrebbe osservare, un'operazione di aggiornamento
culturale, non una ricerca originale. Barbaro non ha certo fondato nuovi
metodi di indagine e l'orizzonte epistemologico cui si riferisce è
tradizionale, anche se la coloritura platonica della filosofia
aristotelica lo conduce a risultati non trascurabili. E anche nel campo
delle scelte formali il suo gusto non appare del tutto coerente, almeno
a giudicare dalle due opere in cul egli sembra implicato, il palazzo del
giurista Camillo Trevisan - membro dell' Accademia Veneziana — e la
villa di Maser. Ma l'opera del patriarca eletto di Aquileia va
considerata sotto altra luce: come 'programma' relativo alla diffusione
di nuovi atteggiamenti mentali, in sintonia con gli apporti delle nuove
scoperte scientifiche.

Il continuo aggiornamento e le fitte relazioni intessute da Daniele con
il mondo scientifico italiano sono rivelatori al proposito. Da quanto
emerge dagli epistolari, dai manoscritti, dalle opere a stampa, è
possibile parlare di un vero e proprio ambiente scientista che lega fra
loro Barbaro, Nicolò Zen, Matteo Macigni, Giuseppe Moleto, Francesco
Barozzi, Jacopo Contarini, Gian Vincenzo Pinelli, proiettandosi nel
tempo verso esiti niente affatto scontati. Matteo Macigni e Giuseppe
Moleto, anzitutto. Al primo, collega di Barbaro negli anni di studio
patavini, Daniele dedicherà la Pratica della perspettiva.”? Il secondo —
in amicizia con Nicolò Zen, Jacopo Contarini e Pinelli — è il
predecessore di Galileo alla cattedra dell'Università di Padova, autore
di una notevole quantità di manoscritti dedicati all'uso civile e
militare delle matematiche, già membro dell'Accademia della Fama e
inquisito, nel 1562, dal Tribunale del Sant'Uffizio. Moleto - allievo di
Francesco Maurolico e autore di un Discorso universale pubblicato in
appendice alla Geografia di Tolomeo edita nel 1561 — collabora con
Matteo Macigni per il De corrigendo ecclesiastico calendario, pubblicato
a Venezia. Una collaborazione non priva di tensioni, tuttavia, per
un'opera che lo stesso Moleto — da quanto si arguisce da alcune lettere
a Gian Vincenzo Pinelli do gludica innovativa, tanto da temere le
reazioni del pubblico. Da notare, al proposito, che, per l'elaborazione
delle Tabulae Gregorianae contenute nel volume, Moleto sì basa sui
sistemi di calcolo introdotti da Copernico, ma modifica il sistema di
quest'ultimo, facendo riassumere alla terra, prudenzialmente, il ruolo
di centro fisso.

Gli scritti del Barbaro e di Gioseffo Zarlino vengono spesso citati
nelle opere del Moleto, che — come s'è detto — sarà in relazione con
Jacopo Contarini: un patrizio che seguirà le tracce del patriarca eletto
di Aquileia nella Venezia del secondo Cinquecento.®' Non mancano
tuttavia critiche rivolte dal Contarini al Moleto, che appare estraneo
al culto della teoria privilegiato dal Barbaro. Tracce di una nuova
epistemologia sono invece rinvenibili in Francesco Barozzi, uno dei più
interessanti matematici del XVI secolo, nativo di Candia, studioso di
testi greci — principalmente Proclo, Pappo ed Erone bizantino —, amico
di Ulisse Aldovrandi, di Federico Commandino, di Jacopo Contarini, di
Gabriele Paleotti, del Moleto e, più tardi, di Paolo Sarpi. Barozzi
pubblica nel 1560 un Opusculum, che comprende due Quaestiones relative
all'applicazione delle matematiche alla conoscenza della natura.*
L'Opusculum — si noti - è dedicato a Daniele Barbaro, il quale, di
rimando, invierà a Francesco Barozzi la seconda edizione del suo
Vitruvio, pregando l'amico di segnalargli sviste ed errori; il
matematico risponderà con entusiastiche parole di encomio.* Più tardi,
Barozzi citerà senza commento la gnomonica del Barbaro, nella prefazione
della sua Cosmographia, dopo riconoscimenti a Commandino e a Guidobaldo
del Monte e un attacco a Copernico, "il qual segue la falsa opinione di
Aristarcho".* L'anticopernicanesimo non impedisce tuttavia al Barozzi di
raggiungere risultati degni di nota. L'interesse per Pappo Alessandrino
non è dovuto soltanto all'esposizione del metodo analitico di Archimede,
ma anche alla trattazione del problema del centro di gravità,
fondamentale — come osserverà lo stesso Barozzi ® — per la costruzione
delle macchine di uso civile e militare.

La machinatio, ancora legata, nella prima metà del XVI secolo, ai
modelli del Taccola e di Francesco di Giorgio, passa così a una nuova
fase. Non a caso, Guidobaldo del Monte rende omaggio a Pappo nel
Mechanicorum liber.

Per il Barozzi la verità delle matematiche è fondata metafisicamente.
Ciò appare esplicito nel Commentarium in locum Platonis obscurissimum,
dedicato, nel 1566, a Gabriele Paleotti, mentre nel suo commento a
Proclo egli riconosce un'essenza matematica all'anima umana. Nel suo
pensiero si fa strada l'idea di una logica matematica dotata di
autonomia rispetto alla logica aristotelica. Il che è evidente nella
polemica sostenuta contro le tesi di Alessandro Piccolomini, che nel
Commentarium de certitudine mathematicarum disciplinarum (1547) aveva
escluso che le matematiche potessero dischiudere l'accesso alla
conoscenza del mondo sensibile.

La certitudo della matematica, dunque, veniva fatta dipendere unicamente
dall'astrattezza dei suoi oggetti. L'autonomia acquisita nei confronti
della sillogistica veniva pagata da una perdita di 'potenza' logica.
Esattamente contro tale concezione reagisce il Barozzi, affermando
l'inerenza della certitudo al rigore delle dimostrazioni matematiche e
soprattutto riconoscendo agli oggetti matematici — nel De medietate,
compreso anch'esso nell'Opusculum dedicato al Barbaro — una posizione
intermedia fra enti divini ed enti naturali. In definitiva, nella
concezione di Francesco Barozzi gli strumenti matematici divengono
attrezzature logiche capaci di permettere la decifrazione del 'gran
libro dell'universo”, formando l'humus per la rifondazione metodologica
operata da Galileo.

E assai significativo che Daniele Barbaro, dopo aver preso visione di
tali opere del Barozzi, scriva all'amico parole di incondizionata
adesione all'attacco sferrato contro le tesi di Alessandro Piccolomini.
L'aristotelismo del patriarca eletto di Aquileia è tutt'altro che
dogmatico e l'apertura verso le teorie del Barozzi rientra nel programma
esplicitato nei commentari.

La mentalità nuova auspicata dal Barbaro si specifica, dunque, e si
ramifica, trovando punti di coagulo e di scambio nel circolo pinelliano
a Padova e nell'azione di Jacopo Contarini a Venezia: principalmente al
di fuori dello Studio patavino si consolida il clima scientista sotto il
cui segno Daniele aveva iniziato lo studio di Vitruvio. Le meccaniche,
la nautica, l'architettura militare sono privilegiate da Jacopo
Contarini, che è in stretta relazione con Guidobaldo del Monte e
Francesco Barozzi, ma anche con un innovatore nel settore della scienza
delle fortificazioni come Giulio Savorgnan, e che sarà tra i fautori
dell'assegnazione a Galileo della cattedra patavina. Né è certo un caso
se il Contarini - mostrandosi fedele alla linea culturale battuta da
Daniele e Marcantonio Barbaro — proteggerà prima il Palladio, poi
Vincenzo Scamozzi, nelle loro attività veneziane.

Con il Contarini ci troviamo in una Venezia diversa da quella vissuta
dal Barbaro: principalmente, in.una diversa storia, in cui molti dei
nodi politici prima lasciati sommersi vengono al pettine, condizionando
le vicende delle arti e della cultura scientifica. Al di là di tale
considerazione, rimane la vicenda paradossale del paradigma
epistemologico cui il Barbaro, come tanta parte della cultura
rinascimentale, mostra di aderire. Fondato in modo 'forte', quel
paradigma assumeva l'unicità del vero come proprio obiettivo, tentando
una fusione di ragione metastorica e di ragione storica. Sottratta al
divemre, la verità era anche sottratta a ogni discorrere, rimanendo
ambiguamente sospesa fra l'ermeneutica e una conferma per via erudita di
immutabili presupposti. Eppure, il reticolo matematico, che per via
platonico-aristotelica veniva steso sul mondo, era frutto di un soggetto
interpretante dotato di inedita volontà di potenza. Il fatto che si
tratti di un'interpretazione che non si sappia tale, che ignori la
necessità o il bisogno di autointerpretarsi, nulla toglie alle sue
caratteristiche precipue. Piutto sto, l'obbligato calarsi nella
storicità di quel paradigma, che si voleva al di sopra della storia,
produce un paradosso: man mano che l'alleanza fra matematica, meccanica
e fisica si concreta in risultati apprezzabili, il paradigma che aveva
sorretto la nuova ragione si incrina o viene compromesso.

La stessa idea portante dei commentari — la trasparenza del bene da un
'bello' calcolabile e legittimato per via metafisica — è esposta a
critica implicita alla fine del XVI secolo, durante i dibattiti relativi
al ponte di Rialto e alla nuova forma di piazza San Marco. E in entrambe
le occasioni è il fratello di Daniele, Marcantonio Barbaro, a difendere
l''usanza nuova' contro le opinioni di Alvise Zorzi di Benetto, di
Alberto Badoer, di Andrea Dolfin e di Leonardo Donà.® Nello stesso
tempo, anche la "virtù Heroica" attribuita all'architettura inizia a
perdere la sua vis polemica. Fra la fine del XVI secolo e gli inizi del
XVII, una nuova generazione di proti — dai Contini, a Simon Sorella, a
Francesco Smeraldi — inizia ad assimilare la sintassi 'alta' cul figure
come Scarpagnino o Antonio da Ponte avevano voltato le spalle. Si tratta
di un'assimilazione di tipo convenzionale, certo, ma che denota un
mutamento di costume che avviene in assenza di consapevolezze teoriche,
malgrado l'enciclopedica /dea scamozziana.

Neanche 1 tentativi di ridefinizione dei vari saperi speciali compiuti a
Venezia tra la fine del Cinquecento e gli inizi del secolo successivo
sembrano interessati a fissare nuovi paradigmi epistemologici: non ci
riferiamo qui a Galileo, quanto agli studi idraulici di Alessandro Zorzi
e di Marco Antonio Cornaro, o alle sperimentazioni di Giulio Savorgnan.*
In definitiva, il paradigma metafisico sembra aver fondato una
piattaforma provvisoria, instabile a dispetto della sua natura, con
effetti acceleratori sui singoli saperi speciali. In tal senso, le
critiche di Gianni Micheli al processo di formazione delle nuove scienze
nell'Italia del Rinascimento mantengono un significato.” Al di là di una
lettura finalistica dei fenomeni storici, rimane da considerare iuxta
propria principia lo sforzo teorico di cui il Barbaro non è che un
portavoce: la fondazione di un universo che legittimi la sua propria
calcolabilità invocando la metafisicità dell'erdos. Un tentativo di
sintesi precario, va ribadito, ma che segna un crinale con cui le
antinomie del 'moderno' hanno ancora a che fare.

Manfredo Tafuri

## NOTE

Abbreviazioni:

ASV = Archivio di Stato di Venezia. BAM = Biblioteca Ambrosiana, Milano.
BMC = Biblioteca del Museo Correr, Venezia. BMV = Biblioteca Nazionale
Marciana, Venezia.

1. B. TASSO, Amadigi, Venezia 1560 (ma citiamo dall'ed. 1581), canto C,
pp. 721-2.

2. Cfr. s. SPERONI, Della vita attiva e contemplativa, in Opere di M.
Sperone Speroni, Venezia 1740, tomo II, pp. 1-2: "mosso dalle ragioni et
autorità d'Aristotile, io vi lodava i filosofi; i quali ... allontanati
dal vulgo ed in se stessi raccolti, altro quasi non sanno, che specular
tuttavia con molto studio, e contemplare intentamente la cagion delle
cose: ma allo 'ncontro mi si faceva quel vostro ingegno divino [Speroni
si rivolge sempre a Daniele Barbaro], uso da' primi anni a spiare
felicemente i secreti della natura e di Dio. Il quale ingegno oltre la
sua prontezza Natia, acceso oltre modo del buono amore che voi portate
alla vostra patria, solo ricetto dell'onore e libertà italiana, toglieva
al cielo con somme lodi quei virtuosi, i quali vivono umanamente, cose
operando, con le quali mentre onorano se medesimi, giovino altrui, e qua
e là travagliando pongano in pace 1 loro cittadini". Cfr. inoltre P.
PARUTA, Della perfettione della vita politica, Venezia 1579. In tale
dialogo, Francesco Molin, nipote del Barbaro, si rivolge
all'ambasciatore Michele Surian, sollecitandolo a dimostrare "che la
vera felicità umana da noi conseguir si possa, non nelle solitudini
vivendo né dando opera alle speculationi; ma ben usando nelle Città, e
in esse virtuosamente operando; la qual maniera di vita voi, con nome
assai conveniente, Politica chiamar solete. E tanto più ci sarà questo
caro d'intendere partitamente — continua Molin — quanto mi pare che un
tal conclusione molto nuova sia, e molto da quella diversa che i nostri
maestri di filosofia nello Studio di Padova difender sogliono". (/bidem,
p. 22). Su tali argomenti e sulle amicizie giovanili di Daniele Barbaro,
che Tequenta, fra l'altro, il circolo esclusivo di Pier Francesco
Contarini, cfr. P.J. LAVEN, Daniele Barbaro, Patriarch Elect of
Aquileia, with Special Reference to his Circle of Scholars and to his
Literary Achievement, Thesis for Ph.D., University of London, 1957, vol.
1, pp. 47 sgg., 164 sgg. Su Daniele Barbaro, cfr. la bibliografia citata
in M. TAFURI, Venezia e il Rinascimento, Torino 1985), 1986°, pp. 180-1
nota 92, cui vanno aggiunti i saggi di G. SANTINELLO, Filosofia e
architettura in Daniele Barbaro patrizio veneto, in "Quaderni della
Biblioteca Filosofica di Torino", Torino s.d. [1981]; V. FONTANA, /l
'Vitruvio' del 1556: Barbaro, Palladio, Marcolini, in AA.VV. Trattati
scientifici nel Veneto fra il XV e il XVI secolo, Vicenza 1985, pp.
39-72; H.-W. KRUFT, Geschichte der Architekturtheorie, Minchen 1986, pp.
95-7. Cfr. anche l'ottimo saggio di P.N. PAGLIARA, Vitruvio da testo a
canone, in AA.VV., Memoria dell'antico nell'arte italiana, a cura di S.
Settis, 11, Dalla tradizione al'archeologia, Torino 1986, pp. 5-85, e la
tesi di laurea inedita di M. LOSITO, Il JX tbro dei Commentari
vitruviani di Daniele Barbaro, Istituto Univ. di Arch. di Venezia,
Dipartimento di Storia, luglio 1986, relatore M. Tafuri. Su Daniele
Barbaro storiografo della Repubblica, cfr. G. COZZI, Cultura politica e
religione nella “pubblica storiografia' veneziana del '500, in
"Bollettino dell'Istituto di Storia della Società e dello Stato
Veneziano", v-vI (1963-64), pp. 215-94.

3. Su tali temi cfr. G. COZZI, Il doge Nicolò Contarini. Ricerche sul
patriziato veneziano agli inizi del ficento, Venezia-Roma 1958; W.J.
BOUWSMA, Venice and the Defense of Republican Liberty. Renaissance
Values in the Age of Counter-Reformation, Berkeley-Los Angeles 1968,
trad. it. Bologna 1977; P.F. GRENDLER, The Roman Inquisition and the
Venetian Press, 1540-1605, Princeton, NJ. 1977, trad. it. Roma 1983 (ma
vedi anche la recensione di G. COZZI, Books and Society, in "Journal of
Modern History", 1979, n. 51, pp. 86-98); R. FINLAY, Politics in
Renaissance Venice, ew Brunswick 1980, trad. it. Milano 1982; G. COZZI,
Repubblica di Venezia e stati italiani, Torino 1982; M. TAFURI, Venezia
e il Rinascimento cit. Sulle origini del 'papalismo' a Venezia cfr. il
fondamentale volume di G. COZZI e M. KNAPTON, Storia della Repubblica di
Venezia. Dalla guerra di Chioggia alla riconquista della Terraferma,
Torino 1986.

4. Cfr. al proposito M. AZZI VISENTINI, L'orto botanico di Padova e il
giardino del Rinascimento, Milano 1984.

5. Azzi Visentini tende invece a sottovalutare le qualità professionali
di Andrea Moroni, senza l'appoggio di verifiche circostanziate. Cfr.
L'orto botanico cit., pp. 149-54. Nulla dimostra inoltre che Daniele
Barbaro possegga, già nel 1545, competenze architettoniche. Il Barbaro
si trova, nel 1548, a giudicare, come Provveditor di Comun, insieme a
Zuan Mauro da Molin e Antonio Bernardo, una supplica di Alberto Moroni
di Albino — probabilmente un parente di Andrea Moroni - per il brevetto
di un "artificioso instrumento de filar con doi fusi et torzer ... di
molto benefficio all'università et maxime al lanificio in questa Città
nostra..." (ASV, Senato Terra, filza 7, 19 giugno 1548). Anche Andrea
Moroni è fecondo nel campo della machinatio: è noto il privilegio
concessogli dal senato, il 23 luglio 1545, per un artificio "con il qual
facilmente si alzano, et bassano le saracinesche di Padova". In una
lettera inedita inviata dai Rettori di Padova al senato, il 17 settembre
1544, è citato un "modello" del Moroni "che si può trasportar da loco a
loco, molto bene inteso et facile per il levar et bassar le ditte
saracinesche" (ASV, Senato Terra, filza 1, 17 settembre 1544). Egli
appare particolarmente esperto di problemi idraulici, dato che è
invitato a studiare la cronica questione della rosta di Limena: vedi la
sua relazione, anch'essa inedita, in cui constata "la rovina de arzeni
che se sta cavati da li brentani chi fu in questi zorni passati" e
predice disastri futuri qualora non si prendano immediati provvedimenti
(ASV, Senato Terra, filza 23, lettera di Andrea Moroni del 25 febbraio
1556).

6. I dieci libri dell'architettura di M. Vitruvio tradutti et commentati
da monsignor Barbaro eletto patriarca d' Aquileggia, in Vinegia, per
Francesco Marcolini, 1556, p. 274 (d'ora in avanti citati con la sola
data di pubblicazione, la pagina, e dopo una virgola il rigo). Il
privilegio di stampa per la prima edizione, richiesto da Francesco
Marcolini, viene concesso il 30 giugno 1556 (ASV, Senato Terra, filza
23, alla data, e reg. 40, c. 120r-v). Il privilegio di stampa per la
seconda edizione è concesso a Francesco Senese il 10 maggio 1567
(ibidem, filza 49, alla data, e reg. 46, c. 1317). Sulle variazioni
contenute nella seconda edizione, si veda lo studio di Manuela Morresi,
in questo stesso volume.

7. Cfr. W. STEDMAN SHEARD, The Tomb of Doge Andrea Vendramin in Venice
by Tullio Lombardo, Ph. D., Yale University, 1971, University
Microfilms, Ann Arbor, Mich., vol. I, in particolare alle pp. 91-2. Vedi
anche s. ROMANO, 7ullto Lombardo, il monumento al doge Andrea Vendramin,
Venezia 1985.

8. Cfr. c. GRAYSON, Un codice del De re aedificatoria posseduto da
Bernardo Bembo, in AA.VV., Studi letterari, Miscellanea in onore di
Emilio Santini, Padova 1956, pp. 181-8; ID., Alberti, Poliziano e
Bernardo Bembo, in AA.VV., Il Poliziano e il suo tempo, Atti del IV
Convegno Internazionale di Studi sul Rinascimento (Firenze 1954),
Firenze 1957, pp. 111-7. Cfr. inoltre N. GIANNETTO, Bernardo Bembo
umanista e politico veneziano, Firenze 1985, pp. 301-3, 328-9, e passim.
Su alcuni aspetti delle élites umanistiche patrizie, nella Venezia del
Quattrocento, cfr. F. GILBERT, 'Humanism in Venice, in AA.VV., Florence
and Venice: Comparisons and Relations, I. Quattrocento, Firenze 1979,
pp. 13-26.

9. S. SERLIO, Regole generali di architettura, Venezia 1537, f. 3r; ID.,
Il terzo libro di Sebastiano Serlio Bolognese, Venezia 1540, p. 155.

10. Cfr. E. CONCINA, Fra Oriente e Occidente: gli Zen, un palazzo e il
mito di Trebisonda, in AA.VV., 'Renovatio urbis”. Venezia nell'età di
Andrea Gritti (1523-1538), a cura di M. Tafuri, Roma 1984, pp. 265 sgg.

11. F. SANSOVINO, Venetia città nobilissima et singolare, Venezia 1581,
ff. 1120 -119r. Cfr. anche ID., Delle cose notabili che sono in Venetia,
Venezia 1561, f. 23r-v.

12. D. ATANAGI, Lettere di XIII Huomini illustri, Venezia 1540, pp.
396-7. Cfr. anche P.N. PAGLIARA, Vitruvio cit., p. 73 nota 34.

13. C. TOLOMEI, Delle lettere di Claudio Tolomei libri sette, Venezia
1547, ff. 81r -85r. Cfr. anche P. BAROCCHI, Scritti d'arte del
Cinquecento, Milano-Napoli 1977, III, pp. 3037-46, con commento e
bibliografia.

14. 1556, p. 40.

15. 1556, p. 82, 53-62. Cfr. anche / dieci libri dell'architettura di M.
Vitruvio, tradotti et commentati da Mons. Daniel Barbaro eletto
Patriarca d' Aquileia, da lui riveduti et ampliati; et hora în più
commoda forma ridotti, in Venetia, Appresso Francesco de' Franceschi
Senese, et Giovanni Chrieger Alemano Compagni, 1567, P- 133, 33-45
(d'ora in poi citati con la sola data di pubblicazione e la pagina, e
dopo una virgola il rigo). È questa l'edizione qui riprodotta.

16. Cfr. c. THOENES, Vignolas 'Regola delli cinque ordini', in
"Romisches Jahrbuch fir Kunstgeschichte", xX, 1983, pp. 347-76; P.N.
PAGLIARA, Vitruvio cit., pp. 84-5.

17. 1556, p. 122, 61-4, corsivi nostri. Cfr. anche 1556, p. 171, 24
(1567, p. 282, 14-5), in cui fra l'altro Barbaro difende la cornice del
Teatro di Marcello, criticata dal Serlio con un eccesso di rigorismo.

18. 1556, p. 82, 66. Corsivi nostri.

19. Cfr. P.N. PAGLIARA, La casa romana nella trattatistica vitruviana,
in "Controspazio", IV, 1972, n. 7, pp. 22-36; ID., Vitruvio cit.

20. 1556, pp. 167-70; 1567, pp. 278-811.

21. Cfr. il saggio, ormai classico, di W. LOTZ, Das Raumbild in der
italienischen Architekturzeichnung der Renaissance, in "Mitteilungen des
Kunsthistorischen Institutes in Florenz", VII, 1956, PP. 193-226, ora in
ID., Studies in Italian Renaissance Architecture, Cambridge, Mass.,
1981°, pp. 1-41. Nuove ipotesi sulla Lettera sono nel saggio di C.
THOENES, La 'lettera' a Leone X, in AA.VV, Raffaello a Roma. Il Convegno
del 1983, Roma 1986, pp. 373-81. (Ma cfr. anche A. NESSELRATH, Raphaels
Archaeological Method, in ibidem, PP- 357-71, con ipotesi alternative).
Sulla relazione Barbaro-Palladio, cfr. E. FORSSMAN, Palladio e Daniele
Barbaro, in "Bollettino del C.I.S.A. Palladio", VIII, 1966, parte Il,
pp. 68-81; L. PuPPI, Andrea Palladio, Milano 1973, fassim; N. HUSE,
Palladio und die Villa Barbaro in Maser: Bemerkungen zum Probleme der
Authorschaft, in "Arte Veneta", XXVIII, 1974, pp. 106-22; H. BURNS
(editor), The Portico and the Farmyard. Andrea Palladio 1508-1580,
London 1975; R. SMITH, A Matter of Choice: Veronese, Palladio and
Barbaro, in "Arte Veneta", XXXI, 1977, pp. 60-71; L. PUPPI, Per Paolo
Veronese architetto. Un documento inedito, una figura e uno strano
silenzio di Palladio, in "Palladio", 111, 1980, n. 1-4, pp. 53-76; D.
BATTILOTTI, Villa Barbaro a Maser: un difficile cantiere, in "Storia
dell'arte", 1985, n. 53, pp. 3348; M. TAFURI, Venezia e il Rinascimento
cit., cap. V.

22. Vedere il commento del Barbaro al libro I: 1556, pp. 19-20; 1567, p.
30. , 23. E. FORSSMAN, Palazzo Da Porto-Festa, Corpus Palladianum 8,
Vicenza 1973, pp. 23 e 25, in cui l'autore scambia per un portale a
serliana, disegnato come alternativa per la facciata, la sezione
dell'atrio, disegnata da Palladio in trasparenza.

24. Cfr. c. VASOLI, Profezia e ragione. Studi sulla cultura del
Cinquecento e del Seicento, Napoli 1974, pp. 131-403; ID., / miti e gli
astri, Napoli 1977, pp. 19! sgg.; A. ROTONDÒ, La censura ecclesiastica e
la cultura, in Storia d'Italia Einaudi, V: I documenti, 2, Torino 1973,
pp. 1436-8 e 1456-7; A. FOSCARI e M. TAFURI, L'armonia e i conflitti. La
chiesa di San Francesco della Vigna nella

25. Su tale argomento, cfr. PH. MOREL, // funzionamento simbolico e la
critica delle grottesche nella seconda metà del Cinquecento, in AA.VV.,
Roma e l'antico nell'arte e nella cultura del Cinquecento, a cura di
Marcello Fagiolo, Roma 1985, pp. 149-78, in cui, fra l'altro, viene
ricordata la critica del Lomazzo alla condanna del Barbaro.

26. PJ.LAVEN, Daniele Barbaro cit., 1, pp. 122 sgg. Cfr. inoltre le
Lettere di Daniele Barbaro date in luce la prima volta..., a cura di S.
Soldati, Padova 1829.

27. Cfr. P.F. GRENDLER, The Roman Inquisition cit. (trad. it.), pp.
207-8. Secondo Barbaro, tutti 1 libri "scientiarum et artium" scritti da
protestanti avrebbero dovuto circolare liberamente — a meno che non
contenessero passi immorali e diffamatori — senza il nome degli autori.
Inoltre, non andavano condannate le poesie d'amore giovanili.

28. Predica dei sogni composta per lo reverendo padre D. Hypneo da
Schio, Venezia 1542. Nella copia conservata presso la Biblioteca
Marciana è segnato a mano sul verso della copertina il nome dell'autore,
"Barbaro Daniel"; la notizia è confermata da un foglietto incollato fra
copertina e frontespizio. Cfr. P. PASCHINI, Gli scritti religiosi di
Daniele Barbaro, in "Rivista di Storia della Chiesa in Italia", 5
(1951), pp. 340-9; G. SANTINELLO, Filosofia e architettura cit., pp.
462-3, 475-6 nota 10.

20. Predica dei sogni cit., Del dubbio, IV. Sul ruolo del dubbio, cfr.
1556, p. 24, 24 sgg.; 1567, P- 33, 12 S88.

30. 1556, p. 7, 62-5. Corsivi nostri. Cfr. 1567, p. 7, 2-7.

31. J.P. LAVEN, Dantele Barbaro cit., II, p. 506.

92. BONAVENTURA DA BAGNOREGIO, Breviloquium, Il 4, 5, ora in ID.,
Itinerario dell'anima a Dio..., a cura di L. Mauro, Milano 1985, p. 151.

33. BONAVENTURA DA BAGNOREGIO, De reductione artium ad theologiam, 2,
ora in ID, Itinerario cit., pp. 412 sgg. Cfr. inoltre UGO DA SAN
VITTORE, Didascalicon, IH 1 e 20. Sul pensiero di Bonaventura, che
difende la prospettiva agostiniana di un'unica sapientia, che comprenda
insieme fides e ratio, cfr. l'introduzione di L. Mauro all'/tinerario,
in particolare alle pp. 66-8.

34. 1550,

P. 3.

35: 1556, p. 254, 5-7; cfr. 1567, p. 440, 9-13.

36. 1556, p. 15, 46 sgg.; cfr. 1567, p. 21, 22 sgg.

37. M. TAFURI, Venezia e il Rinascimento cit., pp. 187-8.

38. 1556, p. 15, 45-6; cfr. 1567, p. 21, 19-20.

39. Sull'opera di Francesco Patrizi senese in relazione ai suoi
interessi vitruviani e alla sua amicizia con Fra Giocondo, cfr. F.
BATTAGLIA, Enea Silvio Piccolomini e Francesco Patrizi, due politici
senesi del Quattrocento, Siena 1936; L.F. SMITH, A notice of the
Epigrammata of Francesco Patrizi, Bishop of Gaeta, in "Studies in the
Renaissance", 15 (1986), pp. 92-143; M. TAFURI, Venezia e il
Rinascimento cit., pp. 159-62; P.N. PAGLIARA, Vitruvio cit., pp. 28-30.
Per la notizia data nel testo, vedi M. DYKMANS S.J., L'oeuvre de Patrizi
Piccolomini ou le cérémonial papal de la première Renaissance, Studi e
testi 293, tomo I, Città del Vaticano 1980, p. 3 nota 16. "Concepi iam
dudum libros de re publica quos parere aliquando cupio — scrive
Francesco Patrizi Attamen propter penuriam librorum cum difficultate
maxima id agere possum ... Roga tamen eum ut tibi Vitruvium concedat pro
aliquibus diebus quem ad me mittas et ego ad te remittam cum primo
videro locos quosdam qui operi meo necessarii admodum sunt". In ottobre,
inoltre, il vescovo di Gaeta invia ad Agostino Patrizi il testo di
Columella da lui corretto per il papa. Cfr. ibidem, p. 4 nota 17.

40. Cfr. G. COZzI, Domenico Morosini e il 'De bene instituta republica',
in "Studi veneziani", XII (1970), pp. 405-57; M. TAFURI, Venezia e il
Rinascimento cit., pp. 156-8.

41. Cfr. H.G. GADAMER, Die Idee des guten zwischen Plato und
Aristoteles, Heidelberg 1978, trad. it. in ID., Studi platonici, 2, a
cura di G. Moretto, Casale Monferrato 1984, pp. 151 sgg., in particolare
alle pp. 155-71. Cfr. inoltre PLATONE, La Repubblica, VII, 525, 526, in
Opere complete, vi, Roma-Bari 1983, pp. 240-1. Dopo aver stabilito che
"lo studio dell'unità sarà fra quelli che conducono e rivolgono a
contemplare l'essere" (525a), Socrate giunge a riconoscere che calcolo e
matematica, totalmente basate sul numero, "appaiono atte a guidare la
verità" e continua: "sarebbe opportuno, Glaucone, prescrivere per legge
la disciplina di cui stiamo parlando, e persuadere chi dovrà svolgere
nello stato le funzioni più importanti, a studiare il calcolo e a
contemplare la natura dei numeri, senza usarne per comprare e vendere
come fanno grossisti e mercanti, ma per ragioni belliche e per aiutare
l'anima stessa a volgersi dal mondo della generazione alla verità e
all'essere". Si veda più oltre, alle pp. 243-7, sul valore speculativo
dell'astronomia. Le idee platoniche erano state introdotte presso il
largo pubblico veneziano dalla famosa conferenza di Luca Pacioli nella
chiesa di San Bartolomeo di Rialto, l'11 agosto 1508, ed erano state
fuse nel pensiero sincretico di Francesco Zorzi. (Sulle relazioni fra
Zorzi e la res aedificatoria, cfr. A. FOSCARI e M. TAFURI, L'armonia e i
conflitti cit., passim). Sì consideri inoltre lo sviluppo dato da
Plotino alla grande analogia di anima, città e mondo. È alla suprema
facoltà dell'anima, al nous che coglie il razionale, che Plotino dà il
compito di guida. Su ciò e sulla similitudine del pensiero razionale con
la trottola, che unisce stabilità e mobilità, cfr. H.G. GADAMER, Studi
platonici, 2 cit., pp. 265-90.

42. ARISTOTELE, Etica Nicomachea, V, VI, in Opere, 7, Roma-Bari 1983,
pp. 105-61. Si noti che è proprio Aristotele a stabilire analogie fra
politica e architettura (ibidem, 1, p. 4). "Poiché dunque l'architettura
è un'arte" egli scrive nell'Etica Nicomachea (ed. cit., p. 145) "in
quanto è una disposizione creativa accompagnata da ragione ... saranno
dunque la stessa cosa l'arte e la disposizione creativa accompagnata da
ragione verace. Ogni arte riguarda la produzione, e il cercare con
l'abilità e la teoria come possa prodursi qualcuna delle cose che
possono sia esserci sia non esserci e di cui il principio è in chi crea
e non in ciò che ha creato; infatti l'arte non riguarda le cose che sono
o che si producono necessariamente, né per natura, in quanto queste
hanno il loro principio in se stesse". Più oltre, Aristotele scrive:
"Sia la scienza politica che la saggezza hanno bensì la stessa
definizione, però la loro essenza non è la stessa. È per quanto riguarda
il governo della città, la saggezza, in quanto architettonica, è
legislatrice; l'altra, in quanto riguarda i particolari, ha il nome
comune di politica". Nel 1 libro, egli aveva affermato che sia
l'architetto che il geometra "ricercano in maniere differenti l'angolo
retto [ma] l'uno lo ricerca solo per quanto è utile al suo lavoro,
l'altro invece ricerca che cosa esso sta e di quale qualità: egli è
infatti un contemplatore del vero" (ibidem, pp. 15-6, corsivi nostri).
Sia Il tema dell'architettura come analogon della saggezza politica, sia
il tema dell'architetto distinto dal 'geometra' per via speculativa
soflo colti da Daniele Barbaro e attualizzati nell'ambito del suo
commento a Vitruvio.

43. 1556, p. 6, 49-53; cfr. 1567, p. 4, 17-22.

44. Cfr. AGOSTINO, De vera religione, 40, 74-6, con la considerazione
delle diverse specie di numeri per mezzo dei quali ci si eleva a Dio, e
De musica, VI, con la definizione delle differenti categorie numeriche
che si elevano gradatamente dalla realtà sensibile al sommo Artefice; S.
BOEZIO, De institutione arithmetica, 1 2: "Il numero è il principale
modello nella mente del Creatore"; BONAVENTURA DA BAGNOREGIO,
Breviloquium, 11 1, e Itinerarium mentis in Deum, Il 10, in /tinerario
cit., rispettivamente a p. 143 e alle pp. 371-2. "Ai numeri espressi"
scrive Bonaventura (p. 372) "ci eleviamo anche gradatamente, passando
dai numeri sonori a quelli intesi, e poi a quelli sensibili e a quelli
della memoria. Tutte le cose, quindi, sono belle e generano un qualche
diletto, e poiché, inoltre, non vi può essere bellezza e diletto senza
che ci sia proporzione, e la proporzione si trova prima di tutto nei
numeri, è necessario che tutte le cose siano costituite secondo una
proporzione numerica..." (corsivi nostri). Attraverso la numero logia
mistica di Agostino, Boezio e Bonaventura, Daniele Barbaro — come buona
parte della cultura neoplatonica - può coniugare il pensiero antico a
quello cristiano; con spostamenti sensibili di accento, tuttavia, sul
polo produttivo che la 'Sapienza', modellata sul pensiero divino,
dischiude al fare mondano.

45. 1556, p. 14, 47-50 (corsivo nostro); cfr. 1567, p. 19, 13-7.

46. 1556, P. 7; 13-5; cfr. 1567, p. 5, 30-3.

47. 1556, P. 94, 28.

48. ARISTOTELE, Etica Nicomachea, 24, cit., p. 143. Cfr. anche PLATONE,
La Repubblica cit., IV, 428b sgg., pp. 140 Sgg.

49. Si considerino, su tale tema, le acute osservazioni contenute nel
volume di R. ESPOSITO, Ordine e conflitto. Machiavelli e la letteratura
politica del Rinascimento italiano, Napoli 1984, in particolare nel cap.
III, pp. 75-108.

50. Cfr. i saggi contenuti nel volume 'Renovatio urbis' cit., e i due
volumi di E. CONCINA. La macchina territoriale. La progettazione della
difesa nel Cinquecento veneto, Roma-Bari 1983, e L' Arsenale della
Repubblica di Venezia. Tecniche e istituzioni dal medioevo all'età
moderna, Milano
1984. Si noti che Giorgio Valla aveva illustrato, poco dopo il 1492,
"suis figuris mathemati €ls" un perduto commentario a Vitruvio. Cfr. v.
JUREN, Fra Giovanni Giocondo et le début des études vitruviennes en
France, in "Rinascimento", serie II, 14, 1974, p. 102; G.B. VERMIGLIOLI,
Memorie di Jacopo Antiquari, Perugia 1813, p. 419.

51. Cfr. G.G. LEONARDI, Libro delle fortificazioni dei nostri tempi,
trascrizione e note di T. Scalesse, in "Quaderni dell'Istituto di Storia
dell'Architettura", Facoltà di Architettura, Università di Roma, serie
XX-XXI (1975), n. 115-126; E. CONCINA, La macchina territoriale cit.,
pp. 50-3 e passim.

52. E. CONCINA, La macchina territoriale cit., pp. 15 Sgg.

53. Ibidem, pp. 55 Sgg.

54. Cfr. 1556; p. 163, 66-8; 1567, p. 271, 8-13; cfr. anche E. CONCINA,
L' Arsenale cit., pp. 135 Sgg.

55. E. CONCINA, L'Arsenale cit., p. 148.

56. 1567, p. 271, 17-8.

57. 1567, p. 271, 22 sgg.

58. 1556, p. 179, 14; cfr. 1567, p. 303, 9-10.

59. 1556, p. 179, 27-33 (corsivi nostri); cfr. 1567, p. 303, 27-37.

60. 1556, p. 180, 19-21; cfr. 1567, p. 304, 30-4.

61.C. SABBADINO, Discorsi sopra la laguna, in Antichi scrittori
d'idraulica veneta, a cura di R. Cessi, vol. II, parte I, Venezia 1930,
p. 48.

62. Cfr. P.F. GRENDLER, The “Tre Savi sopra Eresia” 1547-1605: a
Prosopographical Study, in "Studi veneziani", n.s., III, 1979, p. 320
nota 55.

63. ASV, Atti notaio Vettor Maffei, prot. 8120, cc. 281-2; ibidem, prot.
8132, cc. 292-3 e 301-3. Cfr. M.F. TIEPOLO (a cura di), Testimonianze
veneziane di interesse palladiano, Venezia 1980, pp. 39 e 37.

64. Cfr. B. BOUCHER, The last Will of Daniele Barbaro, in "Journal of
the Warburg and Courtauld Institutes", XLII, 1979, pp. 277-82.

65. È assolutamente da rifiutare l'ipotesi, formulata dalla Bassi e
ripresa più volte, relativa a una responsabilità palladiana per la
quadrifora coperta da frontone, nella facciata di palazzo Contarini
delle Figure, residenza di Jacopo Contarini. Cfr. E. BASSI, Palazzi di
Venezia, Venezia 1971', 19803, pp. 382-4; M.F. TIEPOLO (a cura di),
Testimonianze veneziane cit., scheda 23, p. 19; L. PUPPI, La morte e î
funerali di Palladio, in AA.VV., Palladio e Venezia, Firenze 1982, pp.
160-1. E evidente che la quadrifora fa parte dell'impianto originario
della facciata, databile ai primi decenni del XVI secolo: gli archi
riprendono puntualmente le finestre laterali, compreso l'inquadramento
da parte di allungate semicolonne con capitelli arcaici. Le basi delle
colonne sono 'vitruviane' e poggiano su piedistalli cilindrici.

66. Vedi, ad esempio, H. LORENZ, // trattato come strumento di
“autorappresentazione”. Palladio e 7.B. Fischer von Erlach, in
"Bollettino del C.I.S.A. Palladio", XXI, 1979, p. 151; F. BARBIERI,
Aspetti del Palladio 'urbanista': la 'scena' vicentina, in "Bollettino
del C.I.S.A. Palladio", XXII, 1980, parte II, p. 127.

67. Cfr. G. cozzI, Politica, cultura e religione, in AA.VV., Cultura e
società nel Rinascimento tra riforme e manierismi, a cura di V. Branca e
C. Ossola, Firenze 1984, pp. 21-42. Discutibile il saggio, peraltro
assai utile per la documentazione, di M.J. LOWRY, The Reform of the
Council of Ten, 1582-83; an Unsettled Problem?, in "Studi veneziani",
XIII, 1971, pp. 275-310. Vedi anche il fondamentale G. COZZI, Paolo
Sarpi tra Venezia e l'Europa, Torino 1979.

68. Cfr. M. TAFURI, Venezia e il Rinascimento cit., pp. 244 Sgg.

69. G. GALILEI, Opere, a cura di A. Favaro e I. Del Lungo, xI, Firenze
1934, p. 172. Lettera da Venezia del 13 agosto 1611.

70. 1556, p. 125, 40.

71. 1556, p. 125, 54-62; cfr. 1567, p. 202, 4-16. Analoghe critiche, di
esplicita intonazione erasmiana, erano state espresse in DIEGO DE
SAGREDO, Las medidas del romano, Toledo 1526.

72. ASV, S. Antonio di Castello, t.x, c. 19gr. Cfr. A. FOSCARI, M.
TAFURI, Sebastiano da Lugano, i Grimani e Facopo Sansovino. Artisti e
committenti nella chiesa di Sant' Antonio di Castello, in "Arte Veneta",
XXXVI, 1982, p. 120. Sull'azione riformatrice di G.M. Giberti a Verona,
cfr. A. PROSPERI, Tra Evangelismo e Controriforma. G.M. Giberti
(1495-1543), Roma 1969.

73. Cfr. A. FOSCARI, M. TAFURI, L'armonia e i conflitti cit., pp. 142-3.

74. Sull'originalità dell'enunciato teorico di Vitruvio in seno alla
cosmografia antica, si veda il saggio di L. RONCA, Gnomonica sulla sfera
ed analemma in Vitruvio, Accademia Nazionale dei Lincei, quaderno 224,
Roma 1976, in particolare alle pp. 10-1. Cfr. anche P. PORTOGHESI,
Horologiorum inventio, in ID., Infanzia delle macchine, Roma 1965, pp.
67 sgg.; J. SOUBIRAN, Vitruve: de l' Architecture, livre 1X, Paris 1969.
Sul commento del Barbaro al Ix libro di Vitruvio, cfr. la dettagliata
analisi di M. LOSITO, Il IX Libro dei Commentari vitruviani cit.

75. 1556, p. 201; 1567, p. 347.

76. BMV, Cod. Lat., CI. vili, 42=3097.

77. Claudii Ptolomaei liber de Analemmate a Federico Commandino urbinate
instauratus ... Eiusdem Federici Commandini liber de Horologiorum
descriptione, apud Paulum Manutium Aldi F., Romae
1562. Sull'importanza di tale opera di Tolomeo, cfr. L RONCA, Gnomonica
sulla sfera cit.

78. 1567; pp. 372, 377, 398. Daniele Barbaro riprende, nel suo commento,
la Cosmographia Francisci Maurolici messinensis, Venetiis, Apud haeredes
Lucae Antonii Iuntae Florentini, 1543, dedicata a Pietro Bembo. Allo
stesso Bembo, il Maurolico aveva scritto nel 1536, esponendo un piano di
rinascita delle matematiche greche, e nel febbraio 1540 su altri
argomenti scientifici. Cfr. G. SPEZI, Lettere inedite del cardinal
Pietro Bembo e di altri scrittori del secolo XVI, Roma 1862, pp. 80,
85-04.

79. D. BARBARO, Pratica della perspettiva, Venezia 1569, dedica. Anche
in tale volume il Barbaro polemizza con il fare per sola pratica,
riconoscendo il suo debito nei confronti di Giovanni Zamberto, Federico
Commandino e Baldassarre Lanci. Sulla Perspettiva del Barba ro, in
relazione all'ottica di Alhazen, al De prospectiva pingendi di Piero
della Francesca e al tema del movimento dell'occhio, cfr. TH.
FRANGENBERG, The Image and the Moving Eye, Fean Pélerin ( Viator ) to
Guidobaldo Del Monte, in "Journal of the Warburg and Courtauld
Institutes", vol. 49, 1986, pp. 150-71, in particolare alle pp. 157-8 e
162-4.

80. La corrispondenza Moleto-Pinelli conservata presso la Biblioteca
Ambrosiana di Milano è molto ricca e comprende notizie relative al
comune amico Jacopo Contarini. "Ho dato ad intendere a M. Gerardo
l'instrumento del Contarini", scrive il Moleto al Pinelli da Venezia "il
dì di S. Caterina 1 579", "mi dice che non solo ne farà che mostrino
minuti, ma anco secondi, et in diversi modi ... ma aspetto che possi
havere uno che lavori di ottone a farci l''instrumento da poterci metter
dentro le rotule..." (BAM, S 105 Sup., c. 38r). Nel 1571 (20 giugno),
Moleto scrive al Pinelli interpretando alcune definizioni di Tolomeo e
soffermandosi sul termine chorographia (S 105 Sup., c. 2770). In una
lettera in cui confronta la propria edizione di Tolomeo con quella del
Ruscelli, pesantemente criticata (A_71 Inf., c. 2r2), il Moleto scrive
anche sugli gnomoni, criticando le tesi a lui esposte per lettera dal
Sepulveda (lettera al Pinelli del 5 febbraio 1566; A 71 Inf., c. 1r0).
Un gruppo di lettere al Pinelli si riferisce al De corrigendo
ecclesiastico calendaric: "Ho inteso" egli scrive "quanto V.S. mi
discorre, et prudentemente, intorno allo stampare il libro costi [in
Alemagna], ma non si può ritornare indietro, quello ch'è stato fatto per
consiglio di diece. Hanno determinato che si stampi qui, et ch'io sia
qui insino alla fine". Nel contesto della lettera, il Moleto ricorda la
coHaborazione del Macigni, ma il 14 gennaio 1580 egli scrive: "Intorno
al mio libro sarebbe a quest'ora finito se non fosse cresciuto alquanti
fogli, spero però che con due dì dell'altra settimana si finirà. Ho
pensato di non voler valermi dell'opera del Signor Macigni, ben dimane o
l'altro scriverò a Sua Signoria con dirle che hoggimai è finito il
libro, ma non altro ... intorno al libro non voglio per hora da lui
altro" (BAM, S 105 Sup., c.5170). Sul Moleto cfr. A. FAVARO, Amici e
corrispondenti di Galileo Galilei. XL. Giuseppe Moletti, in "Atti del R.
Ist. Veneto di Scienze, Lettere ed Arti", 77 (1917-18), parte H1, pp.
47-118; A. CARUGO, L'insegnamento della matematica all'Università di
Padova prima e dopo Galileo, in AA.VV. Storia della cultura veneta. Il
Seicento, 4/11, Vicenza 1984, pp. 151 sgg., in particolare alle pp.
170-86.

81. Cfr. PL. ROSE, /acopo Contarini (1536-1595). A Venetian Patron and
Collector of Mathematical Instruments and Books, in "Physis", XVIII
(1976), n. 2, pp. 117-30.

82. Cfr. P.L. ROSE, A Venetian Patron and Mathematician of the Sixteenth
Century: Francesco Barozzi (1537-1604), in "Studi Veneziani", n.s.
(1977), pp. 119-78.

83. Cfr. il testo della lettera al Barbaro, da Rethimo, datata 22 agosto
1567, in P.L. ROSE, 4 Venetian Patron cit., p. 162.

84. F. BAROZZI, Cosmographia, Venezia 1585 (ma citiamo dall'edizione del
1607, in volgare): nella dedica a Francesco Maria II di Urbino sono gli
omaggi a Guidobaldo del Monte e a Federico Commandino; a c. 4r è
l'attacco a Copernico; a c. 40 la citazione del Barbaro.

85. F. BAROZZI, Admirandum illud geometricum problema. Tredecim modis
demonstratum, Venezia 1586, dedica, p. 10.

86. Cfr., al proposito, G.C. GIACOBBE, Francesco Barozzi e la “Quaestio
de certitudine mathematicarum”, in "Physis", XIV (1972), n. 4, pp.
357-74; A. CARUGO, L'insegnamento della matematica cit., PP. 153 S88.

87. Cfr. P.L. ROSE, A Venetian Patron cit., pp. 122-3, in cui è
trascritta la lettera di Daniele Barbaro conservata presso la
Bibliothèque Nationale di Parigi.

88. Cfr. al proposito M. TAFURI, Venezia e il Rinascimento cit., cap.
VII, pp. 244-97. Si noti che fra 1 corrispondenti del matematico
Giovanni Battista Benedetti è il figlio di Marcantonio Barbaro,
Francesco (1546-1616), ambasciatore a Torino presso Emanuele Filiberto —
cui il Palladio dedica il suo Quarto libro — e, poi, presso Carlo
Emanuele I. Nel 1593 Francesco Barbaro diverrà patriarca di Aquileia e
sosterrà la supremazia temporale e spirituale della Santa Sede nei
confronti della Serenissima. Francesco Barbaro contesterà inoltre la
sovranità della Repubblica sui territori friulano e cadorino del
Patriarcato. Il Benedetti dedica a Francesco una lettera sulla "lucerna
spirituale", ispirata all'opera di Erone. Il figlio di Marcantonio
lascia manoscritti di argomento militare, fra cui un trattato sulla
guerra di Cipro (BMC, ms. Cicogna 3186, cc. 43r-169v). Cfr. La scienza a
Venezia tra Quattrocento e Cinquecento. Opere manoscritte e a stampa,
Venezia 1985, scheda XVI.II, p. 110.

89. Cfr. BMv, Cod. It., IV, 349=5118, cc.1r-74v (Alessandro Zorzi), e
Cod. It. Iv, 691=5583, cc. 1r7-72r (Dialogo della Laguna, con quello che
si ricerca per la sua lunga conservatione, composto da s.M. Antonio
Cornaro quondam s. Zuanne, ecc.). Cfr. La scienza a Venezia cit., schede
XV, 4 € XV, 5, pp. 105-6. Cfr. inoltre G. SAVORGNAN, Discorso sopra le
lagune di Venezia (ASV, Secreta, Materie miste notabili, reg. 16,
cc.360-377, parzialmente trascritto in Ambiente Scientifico veneziano
tra Cinquecento e Seicento. Testimonianze d'archivio, a cura di M.F.
Tiepolo, Venezia 1985, pp. 378. Sulle tematiche che dominano il
dibattito veneziano sull'idraulica nel XVI secolo, cfr. S. ESCOBAR, //
controllo delle acque: problemi tecnici e interessi economici, in Storia
d' Italia Einaudi. Annali 3, Scienza e tecnica nella cultura e nella
società dal Rinascimento a oggi, a cura di G. Micheli, Torino 1980, pp.
85-153; S. CIRIACONO, Scrittori d'idraulica e politica delle acque, in
Storia della cultura veneta. Dal primo Quattrocento al Concilio di
Trento, a cura di G. Arnaldi e M. Pastore Stocchi, 3/11, Vicenza 1980,
pp. 491-512. Cfr. inoltre A. MANNO, Politica e architettura militare: le
difese di Venezia (1557-1573), in "Studi veneziani", n.s., XI (1986),
pp. 91-137, con un'analisi dell'opera dei Savorgnan e cenni sulle
posizioni del Barbaro e di Palladio in merito all'arte militare.

90. Cfr. G. MICHELI, L'assimilazione della scienza greca, in Storia
d'Italia Einaudi, Annali 3, Scienza e tecnica cit., pp. 201-57.
