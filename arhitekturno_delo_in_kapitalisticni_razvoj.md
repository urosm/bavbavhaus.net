<!--

## SS

> Tema je relevantna in primerna za raziskovanje v okviru doktorskega projekta.
> Uporaba pojma realne subsumpcije na arhitekturno delo je izvirno in ima potencial obogatiti razumevanje dela v kapitalizmu, kjer se študije večinoma osredotočajo na industrijsko delo kot paradigmatski primer, kot tudi prispevati k demistifikaciji kreativnih in intelektualnih poklicev, ki prepogosto niso analizirani kot delo.
>
> Za uspešno izvedbo predlagam 
>
> 1. Bolj jasen fokus doktorske naloge
>
>    Dispozicija je trenutno zastavljena zelo široko tako po časovno-prostorskem obsegu kot tematsko.
>    Tematsko pokriva tako vprašanja, ki sodijo na področje sociologije dela (pogoji dela arhitektov, konec koncev osrednjost koncepta realne subsumpcije nakazuje tak fokus), družbeno vlogo arhitekture kot tudi vsebino arhitekturnih projektov.
>    Predlagam bolj ozek fokus na enega izmed teh vidikov.
>    Predlagam tudi, da se kandidat osredotoči prostorsko in časovno in podrobneje obravnava ključne prelomne trenutke.
>    Morda bi bile študije izbranih primerov lahko primeren način za zamejitev naloge. 
>
> 2. Pojem realne subsumpcije
>
>    Pojem je Marx uporabil za opis zgodovinskega procesa primarne akumulacije kapitala, torej predzgodovine kapitalizma.
>    Posledično ta pojem pri Marxu ni povsem razvit in ni jasno, ali in kako ga lahko uporabimo za razlago pojavov v razvitem kapitalizmu.
>    Uporaba pojma zato potrebuje dodatno utemeljitev in teoretsko delo, tudi s pomočjo relevantne literature.
>
> 3. Vloga tehnologije
>
>    Dispozicija se na več mestih lahko bere kot da zastopa samostojno vzročno vlogo tehnologije (=tehnološki determinizem).
>    Predlagam, da kandidat bolj eksplicitno utemelji vlogo tehnologije v procesih, ki jih raziskuje.
>    Predlagam, da za začetek prebere dela marksističnega teoretika tehnologije Davida Nobla  (predvsem knjigi Forces of production in America by Design).
>
> 4. Hipoteze
>
>    Menim, da sama oblika hipoteze za doktorsko nalogo ni najprimernejša, saj vnaprej preveč omejuje pozornost kandidata.
>    Predlagam, da kandidat namesto tega namen naloge skrči v obliko raziskovalnih vprašanj.
>
> 5. Dodatni teoretski pristopi
>
>    Predvsem za obravnavo vidikov sociologije dela, bi kandidatu koristila literatura pristopa analize delovnega procesa (Labour process theory), ki z marksističnega vidika analizira kapitalistični delovni proces.
>    Utemeljitveno delo pristopa je Labor and Monopoly Capital Harrya Bravermana, predvsem pa priporočam dela Michaela Burawoya, predvsem Manufacturing Consent in Politics of Production.
>    Obstaja pa tudi serija knjig, povezanih z mednarodno konferenco pristopa analize delovnega procesa: https://www.ilpc.org.uk/Book-Series. 
>    Relevantna pa je tudi knjiga The Global Auction Phillipa Browna, Hugha Lauderja in Davida Ashtona, predvsem njihov pojem digitalnega taylorizma.

## PŠ

> - Tema je za arhitekturno teorijo relevantna.
>   Za bolj jasno artikulacijo problema in njegovo časovno opredelitev pa svetujem uvodno širšo analizo sprememb, ki privedejo do "obdobja realne subsumpcije arhitekture", ki ga kandidat detektira.
> - Namesto hipoteze, ki bi bila lahko za raziskavo preveč omejujoča zato v dispoziciji predlagam zastavitev več raziskovalnih vprašanj, ki naslavljajo za obravnavano temo pomembne vsebinske in druge "obrate" v arhitekturi v določenih obdobjih v povezavi z razvojem tehnologije.
>   Pomembna referenca so dela Maria Carpa, npr. *Architecture in the Age of Printing*, *The Alphabet and the Algorithm*, *The Digital Turn in Architecture* idr.
>   Na to se (verjetno) nanaša tudi detektiranje "obrata" od formalne k realni subsumpciji arhitekture.
> - Pri urejanju dispozicije svetujem bolj odprto/diskurzivno podajanje nekaterih trditev, ki jih ni možno povsem enoznačno zagovarjati: npr. "A kritična obravnava novega stanja arhitekturi po večini ostaja omejena na klasična pojmovanja odnosa med arhitekti kot intelektualci in družbo."
> - Pri opisu metod predlagam navedbo in opredelitev načinov iskanja odgovorov na raziskovalna vprašanja: navedba teoretskih virov in (uvodnega predlaganega) nabora študij primerov arhitekturnih del.
>   Smiselno bi bilo opredeliti tudi, kako bodo arhitekturni projekti prikazani ali analizirani.
>   S slednjim bo disertacija dobila primeren arhitekturni okvir.
> - Za obvladljivost raziskave svetujem zamejitev obsega, ki je lahko definiran časovno, vsebinsko itd.
>   Ali bo v disertaciji vključena obravnava najsodobnejših dogajanj v arhitekturi v povezavi z dostopnimi orodji umetne inteligence?
> - Opredeliti in opisati prispevek disertacije k znanosti.

-->

<!--

Dispozicija doktorske disertacije obsega:

- predlog naslova disertacije v slovenskem in angleškem jeziku,
- znanstveno področje teme doktorske disertacije,
- pregled ožjega znanstvenega področja in opis vsebine, ki jo bo doktorand obravnaval,
- opredelitev raziskovalnega problema, ki obsega:
  - prikaz dosedanjih raziskav na področju teme,
  - jasno predstavljene hipoteze oziroma raziskovalna vprašanja s kratko obrazložitvijo,
  - zasnovo raziskav in opis metod raziskovanja,
  - opredelitev pričakovanih rezultatov in izvirni prispevek k znanosti,
  - seznam relevantne literature s področja teme.
- življenjepis doktoranda,
- bibliografija doktoranda (seznam doktorandovih objavljenih znanstvenih, strokovnih, projektnih in drugih del),
- dispozicije doktorskih disertacij, ki so namenjene potrditvi na KDrŠ UL naj obsegajo od 3 do največ 5 strani (brez literature).
  V dispoziciji naj kandidati ustrezno citirajo oz. navedejo literaturo, ki jo bodo uporabili,

-->

<!--

Naslov naj jedrnato izrazi bistveno vsebino naloge, natančno, jasno (lokacijsko, časovno ...) naj opredeli objekt raziskave ter morebitne metode in tehnike raziskave.
Vsebuje naj ključne in bistvene besede obravnavane tematike (do 140 znakov, brez kratic).

- Realna subsumpcija arhitekturnega dela
- Arhitekturno delo in kapitalistični razvoj
- Arhitekturno delo in kapitalistični razvoj med ideologijo in tehnologijo

-->

---
title: "Arhitekturno delo in kapitalistični razvoj"
author: Uroš Mikanovič
mentor: Petra Čeferin
# date: 2022-12-12
...

## Problemsko ozadje in namen naloge

<!--

Predstavitev vključuje oceno dosedanjih raziskav na področju (podpodročju), ki se navezujejo na objekt raziskave.
Predstaviti in pojasniti bistvo problema, ki ga raziskava obravnava, njegovo aktualnost in relevantnost.
Kaj je o problemu znanega in kaj ne, oziroma, kaj je še odprto.
Oris problemskega ozadja in teme naj vsebuje ustrezne reference s področja obravnavane tematike, ki dokazujejo kandidatovo poglobljeno seznanjenost s področjem raziskave.
Utemeljitev teme naj vsebuje kritično utemeljitev, da do trenutka prijave načrta raziskave niti doma niti v svetu ni na koncipiran način celovito raziskana in strokovni javnosti prezentirana predložena tema (originalnost raziskave).

Znanstveno utemeljiti in opredeliti namen in cilje raziskovanja.
Glavni cilj raziskave je lahko razčlenjen v več podciljev.

-->

<!--

- Položaj arhitekturnega dela je v zadnjem desetletju zaradi sodobnih procesov informacijsko podprte prestrukturacije področja zidave ena osrednjih tem znotraj arhitekturne kulture, saj uvajanje novih tehnologij v projektantske in proizvodne procese -- parametrizacija in digitalna fabrikacija -- odločilno spreminja tehnično podlago arhitekturne prakse in s tem potencialno spreminja arhitektkine ali arhitektove obstoječe naloge ter vloge.

- Tehnološki razvoj, njegov družben kontekst ter posledice so pomembno ozadje sami ustanovitvi arhitekture kot intelektualne prakse v renesansi in osrednja problematika, ki spremlja modernistično arhitekturo.

- Ker ima arhitektura opravka z objekti, ki so hkrati tudi tehnični objekti, imajo tehnične invencije in tehnološki razvoj pomembno mesto znotraj arhitekturne zgodovine in teorije.

- Tafurijanska problematika je marksovska, a njeno bistveno področje je arhitektura.

-->

Informacijsko podprta prestrukturacija področja zidave, digitalizacija prostorskega načrtovanja ter snovanja in izdelovanja arhitekture, je temeljito spremenila vsakodnevne prakse arhitektov in njihov odnos do proizvodnih procesov in tako tudi vplivala na vprašanja in teme, ki jih obravnava arhitekturna teorija.
[@lloyd2016industries, 2-3]
Rutinizacija dela in poudarek na nadzorni vlogi tehničnih poklicev, ki ju uvajanje informacijskih tehnologij v delovne procese običajno predstavlja, sta na primer vzbudila pozornost do vprašanj statusa arhitekturnega dela kot intelektualnega dela ter odnosa arhitekturnega dela do ostalih delavcev na področju zidave.
[@corna2022architecture]
Ob tem je širši pomen sodobnega tehnološkega razvoja spodbudil tudi ponovne preglede zgodovinskega odnosa med arhitekturo in tehnologijo oziroma tehnološke podlage vzpostavitve novoveške arhitekture.
[gl. @aureli2023architecture; @carpo2011thealphabet]

Aktualno razpravo produktivno informirajo prispevki italijanskega arhitekturnega zgodovinarja in teoretika Manfreda Tafurija iz 60. in 70. let 20. stoletja, kjer tehnični predlogi in njihova družbena vsebina predstavljajo pomembno ozadje sami vzpostavitvi [arhitekture kot intelektualne prakse v renesansi](renesansa.md) in osrednjo problematiko, ki spremlja modernistično arhitekturo.
Tako Brunelleschijevo tehnološko novo rešitev zasnove in izvedbe kupole florentinske katedrale Tafuri označi za revolucijo v proizvodnih odnosih.
S projektom kupole, ki je omogočal, da je ta na vsaki stopnji izvedbe samonosilna in samouravnalna, je renesančni arhitekt racionaliziral tehniko in načine gradbene proizvodnje, prekinil kontinuiteto organizacije tradicionalnega gradbišča in "naglo postavil v ospredje temo moderne družbene delitve dela".
[@tafuri1972larchitettura, 17-19]

Pri tej zgodovinski aferi nam ni ključna zgolj osnovna ugotovitev, da se arhitekt z uveljavljanjem lastne avtonomije postavlja na čelo vladajočih razredov ali da je začetek poklicne poti arhitekta politično sponzoriran.
Tehnološka podlaga na kateri se arhitektura uveljavlja kot avtonomna intelektualna praksa ima objektiven značaj, ki se potrjuje tudi v konfliktu z lastnimi izhodišči in učinkuje neodvisno od strokovnih, umetniških ali političnih interesov arhitekta oziroma z njimi sovpada le slučajno.
Za Tafurija arhitekti *quattrocenta* nakazujejo na dinamiko, ki se bo z industrijsko revolucijo šele popolnoma razvila, in predstavljajo model zgodovinskim avantgardam in arhitekturnim modernističnim gibanjem 20. stoletja.
[gl. @tafuri1969peruna]
Ti z razdelavo lastnih programov, ki se kritično a produktivno nanašajo na politične in gospodarske programe naprednih institucij industrializiranega kapitalizma, končno napovedujejo odpravo ideološke funkcije arhitekture in njeno integracijo v kapitalizem.
V primeru arhitekturnih poskusov nemške klasične socialdemokracije, ki ga poda, podobe in metode industrijskega dela, ki so podlaga projektu modernistične arhitekture, končno zajemajo tudi arhitekturo samo: odpravijo arhitekturni objekt kot "izjemo" v razmerju do mesta ter arhitekturo kot posebno prakso na področju zidave.
[@tafuri1969peruna, 57-58]

Zaradi omejujočih zaključkov glede usod zgodovinskih avantgard in polemike, ki je usmerjena v neoavantgarde in povojne humanistične arhitekturne tendence, sicer priznane prispevke [tafurijanske kritike arhitekturne ideologije](kritika_arhitekturne_ideologije.md) znotraj arhitekturne kulture spremljajo zadržki glede njihovega kulturnega pesimizma ter zgodovinskega ali ekonomskega determinizma.
Toda motivi takšne historizacije modernistične arhitekture so v zgodovinarju sodobnih razvojih arhitekturne prakse, ki se, menimo, ponavljajo v današnjem kontekstu digitalizacije načrtovanja, snovanja in izdelovanja arhitekture.
Ti motivi so izraženi v Tafurijevem članku *Lavoro intellettuale e sviluppo capitalistico*, kjer med drugim obravnava takrat najsodobnejše politične in tehnične predloge izobraževanja in zaposlovanja intelektualcev v gospodarstvu ter formalizacijo njihovih funkcij v programske jezike za namene dinamičnega upravljanja in načrtovanja.
[@tafuri1970lavoro]
Ker tematizira razvoj informacijskih tehnologij šele v svojem nastajanju in v času, ko so v primerjavi z danes sorazmerno površno in izjemoma vključene v delovne procese, deluje nekoliko tendenciozno.
Vsebina članka je zato v manjši meri vključena v kasnejše izdaje Tafurijevih del ter posledično še ni povsem izčrpana.
Tafuri svojem prispevku zazna bistveno disruptivno vlogo, ki jo informacijska tehnologija šele bo imela pri prekvalifikaciji intelektualnega dela.

Njegovo hipotezo, da programski jeziki in avtomatski sistemi nadzora ter vrednotenja, s tem ko številne funkcije intelektualnih poklicev objektivirajo v računalniških postopkih, naznanjajo realno avtonomijo kapitala in preseganje ideološke funkcije intelektualcev, lahko razberemo v sodobnih marksovskih interpretacijah kapitala in kapitalizma.
[gl. @krasovec2021tujost]
Te se, da bi zajele kvalitativno razliko, ki jo predstavlja razvoj informacijskih tehnologij in njihova vseprisotna raba v produkcijskih procesih, vračajo k Marxovemu konceptu [realne subsumpcije](realna_subsumpcija.md).
Marx z njim označuje proces pri katerem kapital -- na podlagi formalne subsumpcije, to je formalne podreditve predobstoječega delovnega procesa kapitalskemu razmerju, in pod zahtevo produkcije relativne presežne vrednosti -- materialno, na ravni tehnološkega procesa, spremeni delovni proces in postane specifičen način produkcije.
[@marx1994economic, 105-106]
Stroji sprva, v času industrijske revolucije, lažje nadomeščajo motorične in mehanične človeške funkcije in tako spreminjajo manualno, obrtniško delo.
Izum računalnikov -- strojev s spominom in programom -- ter njihova množična raba pa naj bi napovedovala možnosti avtomatizacije nekaterih specializiranih nalog, ki jih opravljajo intelektualni poklici, in s tem realno subsumpcijo intelektualnega dela pod kapitalom.
Osrednjo problematiko tako vidimo v subsumpciji arhitekturnega dela kot intelektualnega dela v produktivno delo, to je v delo za kapital.

Vprašanje subsumpcije arhitekturnega dela tudi natančneje usmeri razpravo o povezavi med arhitekturo in kapitalizmom, ki se je, v sklopu splošne marksistične obravnave odnosa med kapitalizmom in kulturo, večkrat omejila na vprašanja okoli položaja umetnosti na trgu oziroma o statusu umetniškega dela kot blaga.
Ključno vprašanje pa je, pravi teoretik in umetnik Dave Beech, 

Potencialno pa produktivno usmerja tudi aktualne razprave o delu in tehnologiji znotraj arhitekturne teorije.

Kritika arhitekturne ideologije intelektualne forme arhitekturne prakse umesti v družbeno dinamiko tako, da jih poveže z motivacijami in interesi njihovih producentov.
Kritično obravnava njihov videz, da obstajajo sami po sebi, in njihovo avtonomijo ter pogoje pod katerimi sploh postanejo avtonomni misli skupaj z družbenozgodovinskim razvojem znotraj katerega nastajajo in opravljajo svojo funkcijo, to je kot posledico delitve dela.
Pa vendar izvora arhitekturnih ideologij ne prepozna enostavno v producentih samih, kot posledico njihovega konsenza, ampak v praksah in navadah, ki nimajo idejne podlage, med drugim na ravni tehnologije kot objektivnega momenta kapitala.
[funkcijo kritike ideologije izpeljujemo iz @adorno2022contribution; gl. tudi @adorno2016uvod, 152-153; in glede specifično tafurijanske kritike ideologije @jameson1994seeds, 40-41]
Pojem arhitekture tako razvija skozi različne vloge, ki jih arhitekturna praksa zavzema znotraj kapitalističnega razvoja.
Enkrat na strani, ko vodi industrializacijo gradbenega sektorja, drugič, ko je sama predmet industrializacije.




Dokler stroji nadomeščajo pretežno ročno delo in mehanske dejavnosti, arhitektura te procese upravlja.
Postavlja se za avantgardo industrializacije, ne le gradbišča in gradbene industrije, temveč industrijske odnose posplošuje na gospodinjstvo, stanovanjski blok, mesto -- na področju reprodukcije delovne sile.

[@ferro2016dessin]

::: {lang=en}

> With the development of computers, electronic instruments and, later, microprocessors, the many specialized tasks performed by engineering, design and technical service departments could now be automated and certain detailed functions imitated repeatedly at a much faster rate than that possible by human labour.
>
> If the elements of manual labour could be broken down to a certain number of basic physical motions, then, in principle at any rate, a similar breakdown of elements of mental labour can also be achieved.
> When "codified", these can be incorporated into computers and other microelectronic devices.
> It is this form of subdivision, classification and codification of the elements of mental labour that helps to accelerate the process of automation of conception.
>
> At present the process is in its infancy, and the hardware and software components of this process appear as "tools" and "aids" to the conception workers rather than as their dispossessors.
> But the technology of, for instance, computer-aided design (CAD) -- developed on the basis of minicomputers, digitizing boards and visual display units -- contains, in embryonic form, the very elements which can lead to a greater objectiflcatlon of the process of conception.
[@ramtin1991capitalism, 75]

:::

<!-- HERE -->

<!--

> Teoretična tradicija, ki pogojuje sodobne politično-ekonomske teorije o intelektualnem delu in vse tisto, kar je na tej osnovi nastalo kot kritika politične ekonomije, izhaja iz Marxovih določitev razlikovanja med produktivnim in neproduktivnim delom (tema je najširše obdelana v nedokončanem rokopisu, ki se običajno naslavlja kot Teorije o presežni vrednosti (besedilo je dostopno na spletu, tudi v angleškem jeziku).
> Na tem ozadju je nastala debela študija Dave Beecha Art and Value: Art's Economic Exceptionalism in Classical, Neoclassical and Marxist Economics (2015).
> Po tej liniji bi kazalo k Primožu Kraševcu dodati še knjigo Rade Pantića Umetnost skozi teorijo: Historičnomaterialistične analize (2019).
> Vse to priporočam zato, ker menim, da je subsumpcija intelektualnega dela v produktivno delo (delo za kapital oz. pod poveljstvom kapitala) v jedru problematike disertacije.
[@kreft]

-->



<!-- krasovec2021tujost, subsumpcija -->

Za razlago kvalitativne razlike, ki jo predstavlja ta zvezen in, se zdi, stopnjujoč se proces, se nam zdi uporaben Marxov koncept *realne subsumpcije*.
[povzemamo po @krasovec2017tujost; gl. tudi @marx1978rezultati]
S tem označuje popolno prisvojitev in podreditev produkcije kapitalu.
Sprva je subsumpcija le formalna, kapitalist postane lastnik obrata in delodajalec tam zaposlenih zgolj "formalno".
Postane lastnik obrata kot ga je našel in delodajalec delavcev kot jih je našel -- tradicionalni, obrtniški.
Razmerje kapitala do produkcije je formalno, saj so se do sedaj spremenila le pravna razmerja, ne pa način dela.
Realna subsumpcija pa pomeni transformacija samih produkcijskih tehnik in tehnologij na način, ki je posebej prilagojen kapitalu.
Uvajanje tehnologije ima v kapitalizmu posebno vlogo.
Je posebno področje, kjer se "prisilni zakon konkurence" lahko izvaja, tehnološke inovacije delujejo kot materializacije imperativa konkurence.
Na različnih področjih tehnično revolucioniranje delovnih procesov poteka različno hitro.

Pri realni subsumpciji intelektualnih delovnih procesov gre torej za razmeroma sodoben pojav, na katerega se pravzaprav "obrat k delu" v arhitekturi nanaša.
Kot intelektualno področje delovanja arhitektura misli in razvija lasten program, kot diskurzivna praksa se nenehno opredeljuje do lastnih pogojev.
Opredeljuje se torej tudi do tehnoloških sprememb svojega področja.
Do tehnologije lahko zavzame, na primer, skeptično pozicijo, ki vsebino arhitekturnega poklica razume esencialistično, disrupcijo, ki jo predstavlja nova tehnologija, pa razume kot prekršek bistva arhitekture.
[gl. na primer @frampton2010intention]
Lahko pa zavzame tudi pozicijo, ki bi jo lahko označili za tehnooptimistično.
V tehnični revoluciji vidi možnost prenove bistva arhitekturnega poklica.
[gl. na primer v istem zborniku @deamer2010detail]
Pri tem ne gre za izrazito drugačne vrste odzivanja na tehnologijo, kot jih arhitekturna zgodovina že pozna.
Arhitekturna kultura je znano reflektirala tudi industrializacijo tekom 19. in 20. stoletja, kjer lahko prav tako sledimo skepsi -- gibanje *arts and crafts*, ekspresionizem ... -- ali navdušenju -- modernizem.
S to razliko, da je tema arhitekturnega dela kot intelektualnega dela takrat zgolj implicitna, saj obdobje modernizma predstavlja realno subsumpcijo ročnega in obrtniškega dela in ne še intelektualnega.

Za Krašovca je prehod kapitala od nadomeščanja motoričnih in mehaničnih človeških funkcij do nadomeščanja intelektualnih funkcij ključen.
Dokler stroji nadomeščajo zgolj silo, trend tega nadomeščanja nadzira človek, četudi posledic tega nima popolnoma pod nadzorom.
Ko pa se človeka lahko nadomesti tudi s tako imenovanimi mislečimi stroji -- sistemi, ki lahko sami načrtujejo in prilagajajo svojo dejavnost -- konkurenčno določen razvoj razvoj kapitala postane popolnoma *ambivalenten* do človeka.

Izpostavljanje potenciala avtonomnega samospreminjanja kapitala v tem primeru lahko deluje nekoliko tendenciozno.
Toda popredmetenje odločevalskih in intelektualnih funkcij v kompleksnih sistemih s povratnimi informacijskimi zankami kvalitativno spreminja dosedanje sheme intelektualnega delovanja ter zahteva ponoven razmislek o temeljnih določilih razmerja med intelektualnimi področji delovanja in kapitalističnim razvojem.

Kot razmislek v tej smeri lahko razumemo tudi dela italijanskega arhitekturnega zgodovinarja Manfreda Tafurija, ki imajo -- v delih kjer se navezujejo na modernizem -- podoben obris kot krašovska tujost kapitala.

Že v *Theories and History of Architecture* [@tafuri1980theories] je proces, kateremu so arhitekturne avantgarde od toskanskega "quattrocenta" dalje podvržene, proces postopnega spodkopavanja diskurzivnega prostora intelektualnih družbenih skupin.
Poudarek na tehnološki podlagi tega procesa pa nastopi v slavnejših spisih zbranih v *Projekt in utopija* ter *The Sphere and The Labyrinth*. 
[@tafuri1985projekt; @tafuri1987sphere]
V procesu kjer intelektualne družbene skupine na začetku 20. stoletja svoje programe opredelijo v terminih naprednih gospodarskih ali političnih institucij objektificirajo nujnost in nespornost, razredno nevtralnost, tehnološkega razvoja.
Ta substitucija ciljev delavskih gibanj s tehnološkimi ukrepi ne pomeni le, da je imela arhitektura, v zadnji instanci, reakcionarno vlogo -- oziroma to ni glavna točka tafurijanske kritike.
Temveč je ključna točka tafurijanske kritike to, da je konstruktivna vloga arhitekture pri realni subsumpciji produkcije je pripravila teren za realno subsumpcijo arhitekture:

> Arhitekti, ki so nesposobni, da bi, za tem ko *so ideološko anticipirali železni zakon plana*, zgodovinsko prebrali izvršeno pot, se do ekstremnih posledic upirajo procesom, *ki so jih sami pomagali vzpostaviti*.
[@tafuri1985projekt, 112, poudarki naši]

Vsaka morebitna vloga, tudi konstruktivna, intelektualcev pri odločanju je izključena.
Tu ne gre več za enostavno odvečnost zaradi slučajne trenutne neustreznosti, temveč za absolutno tujost kapitala od kakršnega koli umnega upravljanja, ideje, programa ali načrtu v diskurzivni obliki.

Tu se tafurijanska in kraševska analiza kar se da zbližata.
Pomembno je omeniti tudi, da je pri obeh eden izmed izhodiščnih signalov za nadaljnja sklepanja pojav računalnikov oziroma razdelava umetnih jezikov, ki so uporabni v komunikaciji znotraj sistema računalnikov.
Po Tafuriju so pogoj avtomatskosti Plana dinamični modeli nadzora, ki delujejo na podlagi umetnih jezikov, ki zagotavljajo lastne mehanizme vrednotenja.

Za obe, četudi tendenciozni, analizi menimo, da zaznata bistveno disruptivno vlogo, ki jo, na eni strani izum, na drugi množična uporaba, informacijske tehnologije imajo pri dekvalifikaciji ali rekvalifikaciji intelektualnega dela.

Obdobje od 70. let 20. stoletja do začetka 21. stoletja znotraj arhitekture in širše zaznamuje več pragmatičnih in optimističnih obratov, ki na praktični ravni niso terjali posebne analize tehnologije v razmerju do intelektualnega delovnega procesa.
Nove funkcije intelektualnega dela v luči novoizumljenih trgov (kar je bila tudi funkcija tako imenovanega neoliberalnega obrata, ki sovpada s tem obdobjem) urbane regeneracije, zvezdniške arhitekture, računalniško podprtih formalnih eksperimentov in celo ekoloških znanosti so nakazovali na nova polja priložnosti, kjer arhitektura lahko spet opredeli *lastne* programe delovanja do teh trgov.
Toda prav tako je to obdobje temeljnih tehničnih sprememb arhitekturnega dela in formalizacije številnih aspektov projektiranja v programske jezike.
Z navezavo na ti dve analizi je možno obdobje pomodernistične arhitekture -- to je arhitekture postmodernizma in dalje -- utemeljiti v terminih racionalizacije, avtomatizacije in rekvalifikacije, kar omogoča tudi reinterpretacijo najbolj vidnih -- stilskih, estetskih, diskurzivnih -- značilnosti obdobja.

<!-- -->


::: {lang=en}

> The technical aspects of the N/C as a package of software and hardware allows no other interpretation than the fact that its development was determined by the social imperative of reducing the dependence of production upon direct living labour; to remove the need for direct decision-making within the production process; to speed up the change of particular jobs being performed; to enable reductions in the interruption of the overall operation; and to allow a more efficient and continuous use of expensive technologies.
> All of which have a *direct* impact on productivity, and therefore on profits and the accumulation of capital.
>
> Thus by taking into account the fusion of the technical and the social aspects of the numerical-control system, it then becomes possible to view its development as having been determined by the requirements of capital production and not that of "general production" or "technical efficiency" in the abstract.
> It was a technology that in its technical aspects and functioning respected the needs of capital at a particular historical phase of its development.
> Its technical features, moreover, made this technology an objective moment of capital; in its application and as it functioned *it was capital*.
> And this was neither by chance nor by accident, but a purposely designed characteristic; a characteristic which is best exemplified by the concept of "feedback" as a fundamental feature of the advance of control systems, and, as we shall see, of automation.
[@ramtin1991capitalism, 44]

:::


Ponovno branje 

Po Manovichu manjka zgodovina softvera.
"What was the thinking and motivation of people who between 1960 and the late 1970s created the concepts and practical techniques that underlie today's cultural software?" [@manovich2013software, 43]

(Arhitekturna kultura in tehnologija.)


V obdobju od konca 60. let 20. stoletja do danes -- po Levu Manovichu
obdobje "softwareizacije" kulture -- lahko sledimo
številnim razvojem znotraj arhitekturne kulture, za katere 

V obdobju od konca 60. let 20. stoletja do danes -- po Levu Manovichu
gre za obdobje "softwareizacije" kulture -- lahko sledimo številnim
razvojem znotraj arhitekturne kulture, ki tematizirajo nove tehnologije
ali njihov vpliv na položaj arhitekturnega dela.

V obdobju od konca 60. let 20. stoletja do danes -- po Levu Manovichu obdobje "softwareizacije" kulture [@manovich2013software, 5] -- lahko sledimo številnim razvojem znotraj arhitekturne kulture, ki tematizirajo nove tehnologije ali njihov vpliv na položaj arhitekturnega dela.
Pri tem se postmodernizem in tako imenovani "digitalni obrat" v arhitekturi večkrat razume kot preseganje modernizma.
[@carpo2013thedigital, 10]

A grafični oblikovalec Dakota Brown, na primer, teoretizira o računalniško podprtem eklekticizmu obdobja kot o poskusih razumevanja in prilastitve transformacij kreativnega dela, ki jih spodbuja industrializacija, in ne kot enostavno odkrivanje novega terena svobode.
[@brown2022out]
Brown sicer obravnava spremembe na področju grafičnega oblikovanja, a zanimivo je, da te spremembe kontekstualizira znotraj arhitekturne zgodovine modernizma in institucij modernistične arhitekture.
S tem pomemben del tendenc tako imenovanih kreativinh poklicev, ki sebe razumejo kot preseganje ali prelom z modernizmom, kontekstualizira kot *nadaljevanje* in *stopnjevanje* teme arhitekture in delitve dela kot jo poznamo iz modernizma.

Prav tako se digitalni obrat v arhitekturi pojmuje kot antiteza industrijskim metodam, ki so podlaga modernističnim sredstvom projektiranja:

> v splošnem smislu se digitalni obrat v arhitekturi lahko razume kot zapoznelo potrditev nekaterih načel same postmoderne arhitekture: proti modernistični standardizaciji so postmodernisti zagovarjali diferenciacijo, variacijo in izbiro; skoraj eno generacijo kasneje so digitalne tehnologije zagotovile najprimernejša tehnična sredstva za dosego tega cilja.
[@carpo2013thedigital, 10]

A ravno globoka standardizacija in racionalizacija je tista, ki zagotavlja pogoje za industrijsko izdelavo navidezno neskončno variabilnih prefabrikatov. Oziroma kot poglabljanje standardizacije zazna Rem Koolhaas:

> Ravno v trenutku, ko sta pravilnost in ponavljanje opuščena kot represivna, so gradbeni materiali postali vse bolj modularni, enotni in standardizirani, kot da je snov že vnaprej digitalna (naslednja stopnja abstrakcije).
> Modul postaja vse manjši in manjši, dokler ne postane mozaik.
> Z velikimi težavami -- prepiri, pogajanja, sabotaža -- se iz enakih elementov konstruirata nepravilnost in edinstvenost.
[@koolhaas2010junkspace, 140]

Tako kot modernistično navdušenje nad "civilisation machiniste" dokaj zanesljivo spremljajo tudi razočarane kritike posledic industrializacije gradbenega sektorja -- sem lahko prištevamo številne pomembne zaznamke arhitekturnega modernizma, od gibanja *Arts and Crafts*, nemškega ekspresionizma, Adolfa Loosa, do samokritičnih ugotovitev o zaostalosti in nepripravljenosti arhitektov Martina Wagnerja --, tako tudi sodobne pragmatične in optimistične ocene digitalizacije arhitekturne industrije [gl. na primer @deamer2010detail] spremljajo tehnoskeptične pozicije, ki prek regionalizmov in tradicionalizmov tako ali drugače zagovarjajo obnovo obrtniške razsežnosti arhitekturnega izdelovanja.
[gl. na primer @frampton2010intention; @ferro2016dessin]
V zadnjem desetletju pa lahko znotraj arhitekturne kulture sledimo tudi "obratu k delu", posebni pozornosti na pogoje arhitekturnega dela v luči novih tehnologij in financializacije področja zidave.
[gl. na primer @lloyd2016industries; @deamer2020architecture]

Za Tafurija so se umetniške avantgarde v svojih poskusih kritičnega a produktivnega vstopa v politične in gospodarske programe naprednih institucij morale nanašati na nekaj še bolj neposredno vključenega v ekonomske procese.
V enem pomembnejših del arhitekturne teorije, *Projekt in utopija*, Tafuri tako sledi vstopu zgodovinskih umetniških avantgard v arhitekturo in *design*, oziroma vstopu modernističnega gibanja v planiranje prestrukturacije produkcije in konsumpcije.

Poda primer arhitekturnih poskusov nemške klasične socialdemokracije, ki si zada naloge racionalizacije gradbišča in gradbene industrije po vzoru drugih, naprednejših, sektorjev.

(V sorodnem kontekstu bo brazilski arhitekturni teoretik Sergio Ferro arhitekturo -- oziroma arhitekturni načrt -- v tem odnosu do gradbišča bolj neposredno opredelil kot tisti "objektivni skelet" kapitalističnega produkcijskega procesa, ki ga predstavljajo stroji velike industrije.)

Tafuri nadaljuje, da modernistično gibanje podobe in metode industrijskega dela prenese tudi na organizacijo arhitekturnega projekta in metode projektiranja.
Modernistično arhitekturno gibanje se torej ne le postavlja na čelo procesov, ki jih sproža velika industrija, ampak industrijske odnose posplošuje tudi na področje reprodukcije delovne sile.
V skrajnem primeru urbanista Hilberseimerja, ki odčarano sprejme nove naloge zvezne organizacije racionalizacije od gradbišča do urbane strukture, Tafuri pravi, da arhitekturni objekt kot "izjema" v razmerju do mesta popolnoma izgine. 

Tafuri bo arhitekturnim poskusom nanašanja na gospodarske programe pripisal dodaten značaj in sicer, da napoveduje podreditev same arhitekture tem procesom.
Do skrajnosti bo to razvil v članku *Intelektualno delo in kapitalistični razvoj* iz leta 1970.
V svojem času sicer tendenciozna analiza pomasovljenja intelektualnega dela, a mislim da zazna bistveno disruptivno vlogo, ki jo bo informacijska tehnologija imela pri prekvalifikaciji intelektualnega dela.
Hkrati pa lahko Tafurijevo hipotezo, da računalniški jeziki in avtomatski sistemi nadzora ter vrednotenja naznanjajo realno avtonomijo kapitala ter odpravljajo ideološke funkcije intelektualcev, razberemo tudi v sodobnih marksovskih ali postmarksovskih interpretacijah kapitala in kapitalizma.

Odzivi na "pomasovljenje" intelektualnega dela.

V zadnjem desetletju se je tehnooptimizem, ki je prevladoval ocenjujem tam do svetovne finančne krize že večinoma izčrpal.
V luči financializacije arhitekture in sorodnih industrij ter prekarizacije kreativnih delavcev lahko na področju arhitekturne teorije in zgodovine spremljamo tako imenovani "obrat k delu", pri katerem je prav tako ena osrednjih tem digitalna industrializacija arhitekturnega poklica.
A kritična obravnava novega stanja arhitekture po večini ostaja omejena na klasična pojmovanja arhitekta kot intelektualca v zunanjem odnosu do družbe.
Poenostavljeno rečeno, lastnih izhodišč, da informacijske tehnologije odpravljajo nekatere kritične in intelektualne funkcije arhitekta, ne jemljejo resno.

(In tretjič, da informacijska tehnologija predstavlja posebno težavo
tradicionalni kritični arhitekturni zavesti, saj so nekateri aspekti
informacijskih tehnologij -- na splošno: avtomatske informacijske
povratne zanke -- "brezbrižne" do arhitekture v njeni ideološki funkciji
(oziroma do arhitekture kot ideacije).)

Za Krašovca je prehod kapitala od nadomeščanja motoričnih in mehaničnih
človeških funkcij do nadomeščanja intelektualnih funkcij ključen.
Dokler stroji nadomeščajo zgolj silo, trend tega nadomeščanja nadzira
človek, četudi posledic tega nima popolnoma pod nadzorom. Ko pa se
človeka lahko nadomesti tudi s tako imenovanimi mislečimi stroji --
sistemi, ki lahko sami načrtujejo in prilagajajo svojo dejavnost --
konkurenčno določen razvoj razvoj kapitala postane popolnoma
*ambivalenten* do človeka.


Namen naloge je aktualnim arhitekturnoteoretskim razpravam o delu in tehnologiji pristopiti iz tafurijanske perspektive, v kateri vidimo 

S tem želimo hkrati tudi odpreti krtičnoteoretsko razpravo o nekaterih "zgolj" tehničnih temah kot so zgodovina, koncepti in predpostavke arhitekturnega softvera.



Namen naloge je opredeliti in utemeljiti razvoj pomoderne arhitekture v terminih racionalizacije, avtomatizacije in rekvalifikacije arhitekturnega dela znotraj izrazito tehnološke dinamike kapitalističnega razvoja.
"Obrat k delu" kot arhitekturnoteoretski trend nakazuje, da področje sprememb vsebin in oblik arhitekturnega dela postaja eno pomembnejših področij za razumevanje sodobne arhitekture.
Toda razvoj intelektualnih oziroma informacijskih funkcij strojev ne odpravlja zgolj nekatere ideološke funkcije intelektualnih poklicev, temveč potencialno odpravlja ideološko funkcijo kot tako.
Opredelitve do in vrednotenje lastnega položaja, skeptično ali optimistično, ni več ustrezno v kolikor so ti procesi še vedno pojmovani znotraj humanistične teorije odtujitve.
Pri realni subsumpciji intelektualnega dela namreč ne gre za proces objektivizacije človeških lastnosti, temveč ima ta proces svojo realno avtonomijo.
Namen naloge je zato tudi teoretsko antihumanistična opredelitev avtonomije kapitalističnega razvoja v razmerju do arhitekturnega dela, tudi skozi reaktualizacijo nekaterih manj znanih del kanoničnih arhitekturnoteoretskih avtorjev.

Razvoj pomodernistične arhitekture je mogoče opredeliti v terminih racionalizacije, avtomatizacije in rekvalifikacije arhitekturnega dela znotraj izrazito tehnološke dinamike kapitalističnega razvoja.
V obdobju pomodernistične arhitekture lahko govorimo o *realni subsumpciji* arhitekturnega dela kot intelektualnega dela pod kapitalom.
Ta subsumpcija ima tehnično podlago v informacijski tehnologiji, ki objektificira in formalizira prej intelektualne (diskurzivne) procese.
To je anticipirala že modernistična arhitekturna kultura, ki je proces doživljala kot krizo, nadaljnji razvoj arhitekture pa do procesa razvije številne odzive -- od skeptičnih, optimističnih, ciničnih ...
Skozi koncepte (realne subsumpcije) arhitekturnega dela in realne avtonomije kapitalističnega razvoja je mogoče razložiti bistvene značilnosti sodobne arhitekture.

V nalogi bi rad zagovarjal, da obdobje od konca 60. let 20. stoletja dalje lahko opredelimo kot obdobje *realne subsumpcije arhitekture*, to je kot obdobje temeljnih notranjih sprememb arhitekture po izrazito tehnološko posredovani logiki kapitala.
Ta proces so napovedovala že arhitekturna nanašanja na industrializacijo iz obdobja modernizma, ko arhitekturna kultura sebe razume tudi kot avantgardo te industrializacije.
Z razvojem in uvedbo informacijskih tehnologij na področje gradnje in načrtovanja pa, sorodno kot dotedaj manualno delo, arhitekturno delo vse bolj zaznamujejo vnaprej določene operacije s stroji.
Te spremembe v vsebini in vlogi arhitekturnega dela se odražajo znotraj arhitekturne kulture, kot optimistično ali skeptično opredeljevanje do tehnologije in kot poskusi obnove ideološke funkcije arhitekture, a nove ravni tehnološke posredovanosti arhitekturnega dela odpravljajo funkcije arhitekture kot intelektualne prakse, kot so opredeljevanje in predpisovanje.

Izpostavil bi nekaj ozadij tega sklopa trditev:

In sicer, da se zanašam na specifične poudarke iz del italijanskega arhitekturnega zgodovinarja Manfreda Tafurija in njegove analize modernističnega gibanja v razmerju do kapitalistične prestrukturacije produkcije in konsupcije. 

Drugič, da razvoje znotraj sodobne arhitekture kulture -- tu imam v mislih tako imenovani "obrat k delu", to je nova kritična pozornost arhitekturnega diskurza, teorije in zgodovine do tem delitve dela in tehnologije -- razumem kot odziv na sodobne procese informacijsko podprte prestrukturacije gradbenega sektorja.
Zato ker uvajanje novih tehnologij v projektantske in proizvodne procese -- kot so danes parametrizacija in digitalna fabrikacija -- spreminjajo tehnično podlago arhitekturnega poklica in s tem se potencialno širijo ali krčijo arhitektkine ali arhitektove naloge ter vloge.

In tretjič, da informacijska tehnologija predstavlja posebno težavo tradicionalni kritični arhitekturni zavesti, saj so nekateri aspekti informacijskih tehnologij -- na splošno: avtomatske informacijske povratne zanke -- "brezbrižne" do arhitekture v njeni ideološki funkciji (oziroma do arhitekture kot ideacije).


Mesto arhitekture znotraj Beechove ekscepcionalistične umetnosti.


## Raziskovalna vprašanja

<!--

Natančno opredeliti raziskovalno vprašanje, na katerega želimo z raziskavo odgovoriti.
Nedvoumno formulirati problem, ki ga želimo z raziskavo pojasniti, razrešiti... 
Opredeliti morebitno delovno hipotezo.

-->

- Pri [vrednotenju] zgodovine N/C in CNC je neizpodbitno, da je prevladal [interes kapitala] (ramtin).
  Ve se, da je ta razvoj podlaga za razvoj arhitekturnega softvera, ki je svoje začetke doživljal sočasno takrat na MIT, toda pod drugačnimi pomeni (kot humanistični projekt).
  Kako se dinamika bitke med N/C in CNC izražala v času ko so se postavljali temelji arhitekturnega softvera.

Naša teza je da lahko spremembe na področju arhitekture v obdobje od 60. let 20. stoletja dalje opredelimo kot obdobje *realne subsumpcije arhitekture*, to je kot obdobje temeljnih notranjih sprememb arhitekture po izrazito tehnološko posredovani logiki kapitala.

Takšen proces so napovedovala že arhitekturna nanašanja na industrializacijo iz obdobja modernizma, ko je realna subsumpcija produkcije pomenila predvsem avtomatizacijo manualnih dejavnosti.
Z izumom in uporabo računalnikov pa lahko sledimo tudi avtomatizaciji intelektualnih dejavnosti.
Z razvojem in uvedbo informacijskih tehnologij na področje arhitekture -- računalniška formalizacija risbe in načrta ter podatkovna razširitev teh formatov -- je dejavnost arhitektov pretvorjena v vnaprej določene operacije s stroji, ki so sproti vrednotene onkraj tradicionalnih metod arhitekture kot intelektualne prakse.

Te spremembe se odražajo tudi znotraj arhitekturne kulture, kot optimistično ali skeptično opredeljevanje do tehnologije in industrializacije, a nova tehnološka posredovanost arhitekturnega delovanja ravno odpravlja funkcije arhitekture kot intelektualne dejavnosti, kot so opredeljevanje in predpisovanje.


## Pristop, materiali in metode

<!--

Natančno predstaviti objekt raziskave (predmet, obravnavani raziskovalni problem, temo raziskave), ki izhaja iz opredeljenega cilja raziskave.
Navesti in utemeljiti: pristop, metodo in pomožne metode, ki bodo v raziskavi uporabljene, morebitne njihove kombinacije in uporabljene tehnike.

-->

Naloge pred tehnološko določeno arhitekturno dejavnostjo so torej razumeti *logiko* tehnologije na področju kjer se uveljavlja: kot racionalizacija in avtomatizacija arhitekturnega dela.

Predmet raziskave so uvedbe informacijske tehnologije v načrtovalske procese in njihovo učinkovanje na vsebino in obliko arhitekturnega dela.
Raziskava tega vključuje pregled relevantne literature in gradiva o rabah različnih tehnologij, kontekst njihove uvedbe (kdaj so uvedene in kateri problem dotedanjega načina dela naj bi naslavljale) in njihov učinek kot ga je zaznala in opisovala takratna arhitekturna kultura.

Ključne tehnološke prelomnice so vsaj: prenos programskega jezika za numerično krmiljenje (CNC) v splošen jezik računalniško podprtega risanja (CAD) -- s tem je omogočen prevod iz risbe v gibe, ki jih mora opraviti stroj -- ter razširitev funkcij CAD formatov tako, da postanejo nosilci dodatnih informacij (BIM), ki jih je mogoče povezovati oziroma pretvarjati v druge sisteme.
S tem korakom arhitekturni načrt postane sistem spremenljivk, ki ga je mogoče simulirati in na podlagi simulacije vrednotiti vsebino načrta.
Slednji pomeni bistven prenos kognitivnih aspektov arhitekturnega dela na stroje.

Glede na kontekst uvajanja tehnologije (razloge in odzive), vsaka predstavlja, vsaj pri arhitekturi, na eni strani premestitev kreativnih mej -- kot orodja omogočajo drugačno izdelovanje --, na drugi pa v njih lahko razberemo tudi funkcijo optimizacije s stališča kapitala nerodnih in nezanesljivih človeških faktorjev.
Analizirati je potrebno oba (oziroma vse) momenta saj ne gre za enoznačne pojave.

Razvoj arhitekture od 60. let 20. stoletja dalje je mogoče opredeliti v terminih racionalizacije, avtomatizacije in rekvalifikacije arhitekturnega dela znotraj izrazito tehnološke dinamike kapitalističnega razvoja.
V tem obdobju lahko govorimo o *realni subsumpciji* arhitekturnega dela kot intelektualnega dela pod kapitalom.
Ta subsumpcija ima tehnično podlago v informacijski tehnologiji, ki objektivizira in formalizira prej intelektualne (diskurzivne) procese.


## Pričakovani rezultati in prispevek k razvoju znanosti

<!--

Navesti pomembnejše rezultate, ki jih lahko pričakujemo z izvedbo raziskave.

Predstaviti teoretski pomen rezultatov raziskave in možnosti aplikativne uporabe: pričakovani strokovni in znanstveni prispevek utemeljiti kot nadgradnjo konkretnih dosedanjih vedenj in spoznanj s področja raziskave.

-->

Razvoj arhitekture od 60. let 20. stoletja dalje je mogoče opredeliti v terminih racionalizacije, avtomatizacije in rekvalifikacije arhitekturnega dela znotraj izrazito tehnološke dinamike kapitalističnega razvoja.
V tem obdobju lahko govorimo o *realni subsumpciji* arhitekturnega dela kot intelektualnega dela pod kapitalom.
Ta subsumpcija ima tehnično podlago v informacijski tehnologiji, ki objektivizira in formalizira prej intelektualne (diskurzivne) procese.

Podobna rekontekstualizacija skozi temo odnosa arhitektura--tehnologija--delo na primeru arhitekture modernizma je predstavljala bogato torišče arhitekturnoteoretskih konceptov in konceptov arhitekturne zgodovine ter prispevala k razumevanju modernistične arhitekture kot kompleksnega pojma.

Takšna opredelitev bo omogočala interpretacijo nedavne in sodobne arhitekture onkraj najbolj vidnih značilnosti obdobja in reaktualizirala nekatera manj znana dela kanoničnih arhitekturnoteoretskih avtorjev.

Opredelitev obdobja modernistične arhitekture kot obdobja realne subsumpcije industrije in postmodernistične ter sodobne arhitekture kot realne subsumpcije intelektualnega dela bo osredinila arhitekturo in njen odnos do tehnologije kot bistven objektiv za razumevanje sodobnega položaja arhitekture v družbi.

> The advent of computer-aided design and drafting is having contradictory consequences for engineers and designers.
> On the one hand, it is being introduced to concentrate control over production in the hands of the technical staff rather than the work force on the shop floor, but, on the other hand, it is being used to deskill and displace engineers and to routinize their jobs also.
> Thus, the automaters themselves are being automated.
[@noble2011forces, 329-330n]

- Ni še obravnavano da te tehnološke inovacije ustrezajo zahtevam kapitalističnega prod. procesa (ne samo tehnološkim zahtevam).

- Objektivne zahteve KPP verjetno ne predstavljajo celotne vsebine arhitekturnega početja (ker je kreativna praksa), a nas prav tako zanima kako protislovja objektivnih zahtev KPP predstavljajo potenciale ...


V zadnjem desetletju se je tehnooptimizem digitalnega obrata že večinoma izčrpal.
V luči financializacije arhitekture in sorodnih industrij ter prekarizacije kreativnih delavcev lahko na področju arhitekturne teorije in zgodovine spremljamo tako imenovani "obrat k delu", pri katerem je prav tako ena osrednjih tem digitalna industrializacija arhitekturnega poklica.
A kritična obravnava novega stanja arhitekture po večini ostaja omejena na klasična pojmovanja odnosa med arhitekti kot intelektualci in družbo.

Tako tudi teoretsko informativne in napredne študije objektivnega značaja arhitekturnih tehnologij kot je *Architecture and Abstraction* arhitekturnega teoretika Pier Vittorio Aurelija [@aureli2023architecture] zadržujejo rezervo možnosti obnove predsubsumiranih funkcij arhitekta kot intelektualca, ki jih izpeljuje iz kritičnih epizod modernističnega gibanja.
Pomanjkljivost tega zadržka je, da zanemarja povezavo med realno subsumpcijo produkcije iz časa modernizma in realno subsumpcijo arhitekture kot intelektualne dejavnosti.
Arhitekturni modernizem lahko, po Tafuriju, opredelimo kot obdobje slučajnega sovpadanja interesov arhitektov in realne subsumpcije produkcije, stopnjevanje in nadaljevanje teh interesov pa temeljno spremeni možnosti kritične obravnave odnosa med arhitekturo in kapitalizmom, ki s ga ne moremo več zajeti zgolj v terminih nasilja in ideologije.
[gl. @mau2023mute]



Raziskava preverja spoznavno vrednost utemeljevanja razvoja pomoderne arhitekturne v terminih racionalizacije, avtomatizacije in rekvalifikacije *arhitekturnega dela* znotraj značilno tehnološke dinamike kapitalističnega razvoja, saj se dosedanje uveljavljeno osredotočanje zgolj na najbolj vidne značilnosti nedavne arhitekturne zgodovine v luči sodobnih teoretskih trendov "obrata k delu" in razvoja informacijskih tehnologij znotraj načrtovalskih delovnih procesov zdi nezadostno.
Podobna rekontekstualizacija na primeru modernistične arhitekture je predstavljala bogato torišče arhitekturnoteoretskih konceptov in konceptov arhitekturne zgodovine ter prispevala k razumevanju modernistične arhitekture kot kompleksnega pojma.
Prav tako bi radi pokazali, da je prav materialistična zgodovina arhitekture kot intelektualnega področja delovanja prepričljiv objektiv za razumevanje sprememb dela, njegove organizacije in njegovih subjektov v sodobnem kapitalizmu.

Posledice tehnološkega razvoja, vsaj kar se tiče vprašanj "proletarizacije arhitekta" ali možnosti njihovega avtonomnega delovanja, ostajajo dvoumne.

<!--

## Vsebinska vpetost naloge v raziskovalni program fakultete

-->

<!--

V sodelovanju z mentorjem (somentorjem in konzultanti) predstaviti navezavo načrta raziskave na raziskovalne programe oziroma projekte fakultete in/ali drugih institucij.

-->

<!--

Mentorica prof. dr. Petra Čeferin se ukvarja s strukturno logiko arhitekture kot kreativne prakse, pri čemer se ta nanaša tudi na vlogo arhitekta ali arhitektke v kreativnem procesu.
Prav tako teoretizira o večih funkcijah arhitekture v družbi (glede na pozicijo, ki jo arhitekti zavzemajo) in na pogoje preseganja tega stanja.
Pri svoji opredelitvi arhitekture kot kreativne samodoločujoče se dejavnosti upošteva tudi tafurijansko diagnozo krize (modernistične zavesti) arhitekture.

Raziskavo je mogoče razumeti kot logično nadaljevanje moje magistrske naloge *Prispevek k obnovi kritike arhitekturne ideologije*, kjer me je zanimalo, ali je sodobna arhitekturna kultura, v nasprotju s prevladujočo zavestjo o koncu zgodovine in kritike, lahko predmet kritično-zgodovinske obravnave kot jo poznamo iz obdobja krize povojnega modernizma.
Poseben odnos med arhitekturo in družbo kot zgodovinsko formacijo produkcijskih razmerij, ali na ravni ideološkega posredovanja, ali kot razvoj in specifike arhitekturnega dela v kapitalizmu, je stalna tema, ki jo naslavljam pri svojem teoretskem delu.

-->

---
lang: sl
reference-section-title: "Literatura"
references:
- type: book
  id: lloyd2016industries
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  title: "Industries of architecture"
  publisher-place: London
  publisher: Routledge
  issued: 2016
  language: en
- type: chapter
  id: corna2022architecture
  author:
  - family: Corna
    given: Luisa Lorenza
  title: "Architecture"
  editor:
  - family: Skeggs
    given: Beverly
  - family: Farris
    given: Sara R.
  - family: Toscano
    given: Alberto
  - family: Bromberg
    given: Svenja
  container-title: "The SAGE handbook of Marxism"
  publisher-place: Los Angeles
  publisher: SAGE
  issued: 2022
  page: 766-784
  language: en
- type: book
  id: aureli2023architecture
  author:
  - family: Aureli
    given: Pier Vittorio
  title: "Architecture and abstraction"
  publisher-place: Cambridge, Mass.
  publisher: The MIT Press
  issued: 2023
  language: en
- type: book
  id: carpo2011thealphabet
  author:
  - family: Carpo
    given: Mario
  title: "The alphabet and the algorithm"
  publisher-place: Cambridge, Mass.
  publisher: The MIT Press
  issued: 2011
  lang: en
- type: book
  id: tafuri1972larchitettura
  author:
  author:
  - family: Tafuri
    given: Manfredo
  title: "L'architettura dell'umanesimo"
  publisher-place: Bari
  publisher: Laterza
  issued: 1972
  language: it
- type: article-journal
  id: tafuri1969peruna
  author:
  - family: Tafuri
    given: Manfredo
  title: "Per una critica dell'ideologia archittetonica"
  title-short: "Per una critica dell'ideologia archittetonica"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1969
  page: 31-79
  language: it
- type: article-journal
  id: tafuri1970lavoro
  author:
  - family: Tafuri
    given: Manfredo
  title: "Lavoro intellettuale e sviluppo capitalistico"
  title-short: "Lavoro intellettuale"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1970
  page: 241-281
  language: it
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
- type: book
  id: marx1994economic
  author:
  - family: Marx
    given: Karl
  title: "Economic manuscript of 1861--63 (conclusion)"
  container-title: "Collected works"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Frederick
  volume: 34
  publisher-place: London
  publisher: Lawrence & Wishart
  issued: 1994
  language: en
- type: book
  id: adorno2016uvod
  author:
  - family: Adorno
    given: Theodor
  title: "Uvod v sociologijo"
  translator:
  - family: Leskovec
    given: Alfred
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2016
  language: sl
- type: book
  id: jameson1994seeds
  author:
  - family: Jameson
    given: Frederic
  title: "The seeds of time"
  publisher-place: New York
  publisher: Columbia University Press
  issued: 1994
  language: en
################################################################################
- type: book
  id: beech2015art
  author:
  - family: Beech
    given: Dave
  title: "Art and value: art's economic exceptionalism in classical, neoclassical and Marxist economics"
  title-short: "Art and value"
  publisher-place: Leiden
  publisher: Brill
  issued: 2015
  language: en
- type: article-journal
  id: adorno2022contribution
  author:
  - family: Adorno
    given: Theodor
  title: "Contribution to the theory of ideology"
  translator:
  - family: Bard-Rosenberd
    given: Jacob
  container-title: "Selva: a journal of the history of art"
  issue: 4
  issued:
    season: 3
    year: 2024
  page: 19-33
  language: en
- type: book
  id: tronti2019workers
  author:
  - family: Tronti
    given: Mario
  title: "Workers and capital"
  translator:
  - family: Broder
    given: David
  publisher-place: London
  publisher: Verso
  issued: 2019
  language: en
- type: article-journal
  id: argan1946thearchitecture
  author:
  - family: Argan
    given: Giulio Carlo
  title: "The architecture of Brunelleschi and the origins of perspective theory in the fifteenth century"
  title-short: "The architecture of Brunelleschi"
  container-title: "Journal of the Warburg and Courtauld institutes"
  volume: 9
  issued: 1946
  page: 96-121
  language: en
- type: book
  id: ramtin1991capitalism
  author:
  - family: Ramtin
    given: Ramin
  title: "Capitalism and automation: revolution in technology and capitalist breakdown"
  title-short: "Capitalism and automation"
  publisher-place: London
  publisher: Pluto Press
  issued: 1991
  language: en
...
