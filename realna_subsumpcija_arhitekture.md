---
references:
# A ############################################################################
################################################################################
- type: chapter
  id: adorno2020funkcionalizem
  author:
  - family: Adorno
    given: Theodor W.
  title: "Funkcionalizem danes"
  editor:
  - family: Šenk
    given: Peter
  translator:
  - family: Riha
    given: Rado
  container-title: "Funkcija v arhitekturi"
  publisher-place: Ljubljana
  publisher: Založba ZRC
  issued: 2020
  # page: TODO
  language: sl
################################################################################
- type: book
  id: aitchison2014thearchitecture
  editor:
  - family: Aitchison
    given: Mathew
  title: "The architecture of industry: Changing paradigms in industrial building and planning"
  title-short: "The architecture of industry"
  publisher-place: Farnham
  publisher: Ashgate
  issued: 2014
  language: en
################################################################################
- type: article-journal
  id: alquati1961relazione
  author:
  - family: Alquati
    given: Romano
  title: "Relazione di R. Alquati sulle 'forze nuove' (Convegno del PSI sulla FIAT -- gennaio 1961)"
  container-title: "Quaderni rossi"
  volume: 1
  issued: 1961
  page: 215-240
  language: it
################################################################################
- type: article-journal
  id: alquati1962composizione
  author:
  - family: Alquati
    given: Romano
  title: "Composizione organica del capitale e forza-lavoro alla Olivetti"
  container-title: "Quaderni rossi"
  volume: 2
  issued: 1962
  page: 63-98
  language: it
################################################################################
- type: article-journal
  id: alquati1963composizione
  author:
  - family: Alquati
    given: Romano
  title: "Composizione organica del capitale e forza-lavoro alla Olivetti"
  container-title: "Quaderni rossi"
  volume: 3
  issued: 1963
  page: 119-185
  language: it
################################################################################
- type: article-journal
  id: asorrosa1962ilpunto
  author:
  - family: Asor Rosa
    given: Alberto
  title: "Il punto di vista operaio e la cultura socialista"
  container-title: "Quaderni rossi"
  volume: 2
  issued: 1962
  page: 117-130
  language: it
################################################################################
- type: article-journal
  id: asorrosa1968primo
  author:
  - family: Asor Rosa
    given: Alberto
  - family: Cacciari
    given: Massimo
  title: "Primo bilancio"
  container-title: "Contropiano: materiali marxisti"
  volume: 2
  issued: 1968
  # page: TODO
  language: it
################################################################################
- type: book
  id: asorrosa1972socialismo
  author:
  - family: Asor Rosa
    given: Alberto
  - family: Cassetti
    given: Bruno
  - family: Ciucci
    given: Giorgio
  - family: Dal Co
    given: Francesco
  - family: De Michelis
    given: Marco
  - family: Di Leo
    given: Rita
  - family: Junghanns
    given: Kurt
  - family: Oorthuys
    given: Gerritt
  - family: Procházka
    given: Vítĕzslav
  - family: Schmidt
    given: Hans
  - family: Tafuri
    given: Manfredo
  title: "Socialismo, città, architettura URSS 1917--1937: Il contributo degli architetti europei"
  title-short: "Socialismo, città, architettura URSS 1917--1937"
  publisher-place: Roma
  publisher: Officina edizioni
  issued: 1972
  language: it
################################################################################
- type: article-journal
  id: asorrosa1995critique
  author:
  - family: Asor Rosa
    given: Alberto
  title: "Critique of ideology and historical practice"
  container-title: "Casabella"
  issue: 619-620
  issued: 1995
  # page: TODO
  language: en
################################################################################
- type: book
  id: aureli2008project
  author:
  - family: Aureli
    given: Pier Vittorio
  title: "The project of autonomy: Politics and architecture within and against capitalism"
  title-short: The project of autonomy
  publisher-place: New York
  publisher: Princeton Architectural Press
  issued: 2008
  language: en
################################################################################
- type: article-journal
  id: aureli2010recontextualizing
  author:
  - family: Aureli
    given: Pier Vittorio
  title: "Recontextualizing Tafuri's critique of ideology"
  container-title: "Log"
  issue: 18
  issued: 2010
  page: 89-100
  language: en
################################################################################
# B ############################################################################
################################################################################
- type: chapter
  id: beech2016onsite
  author:
  - family: Beech
    given: Nick
  - family: Clarke
    given: Linda
  - family: Wall
    given: Christine
  title: "On site"
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  container-title: "Industries of architecture"
  publisher-place: London
  publisher: Routledge
  issued: 2016
  # page: TODO
  language: en
################################################################################
- type: book
  id: benanav2022automation
  author:
  - family: Benanav
    given: Aaron
  title: "Automation and the future of work"
  publisher-place: London
  publisher: Verso
  issued: 2022
  language: en
################################################################################
- type: chapter
  id: benjamin1974goethes
  author:
  - family: Benjamin
    given: Walter
  title: "Goethes Wahlverwandtschaften"
  container-title: "Gesammelte Schriften"
  volume: I
  publisher-place: Frankfurt am Main
  publisher: Suhrkamp
  issued: 1974
  # page: TODO
  language: de
################################################################################
- type: chapter
  id: benjamin1997surrealism
  author:
  - family: Benjamin
    given: Walter
  title: "Surrealism: the last snapshot of the european intelligentsia"
  title-short: "Surrealism"
  container-title: "One-way street and other writings"
  publisher-place: London
  publisher: Verso
  issued: 1997
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: benjamin2009avtor
  author:
  - family: Benjamin
    given: Walter
  title: "Avtor kot proizvajalec"
  container-author:
  - family: Brecht
    given: Bertolt
  - family: Benjamin
    given: Walter
  container-title: "Zgodbe gospoda Keunerja; Me-ti. Knjiga obratov; Poskusi o Brechtu"
  translator:
  - family: Maček
    given: Amalija
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2009
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: benjamin1998umetnina
  author:
  - family: Benjamin
    given: Walter
  title: "Umetnina v času, ko jo je mogoče tehnično reproducirati"
  container-title: "Izbrani spisi"
  translator:
  - family: Janez
    given: Vrečko
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 1998
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: benjamin1998opojmu
  author:
  - family: Benjamin
    given: Walter
  title: "O pojmu zgodovine"
  container-title: "Izbrani spisi"
  translator:
  - family: Janez
    given: Vrečko
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 1998
  # page: TODO
  language: sl
################################################################################
- type: book
  id: benson2002between
  editor:
  - family: Benson
    given: Timothy O.
  - family: Forgács
    given: Éva
  title: "Between worlds: a sourcebook of central european avant-gardes, 1910--1930"
  title-short: "Between worlds"
  publisher-place: Cambridge, Mass.
  publisher: The MIT Press
  issued: 2002
  language: en
################################################################################
- type: chapter
  id: bernstein2010models
  author:
  - family: Bernstein
    given: Phillip G.
  title: "Models for practice: Past, present, future"
  container-title: "Building (in) the future: Recasting labor in architecture"
  editor:
  - family: Deamer
    given: Peggy
  - family: Bernstein
    given: Phillip G.
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  # page: TODO
  language: en
################################################################################
- type: book
  id: biraghi2013project
  author:
  - family: Biraghi
    given: Marco
  title: "Project of crisis: Manfredo Tafuri and contemporary architecture"
  title-short: "Project of crisis"
  translator:
  - family: Price
    given: Alta
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 2013
  language: en
################################################################################
################################################################################
- type: book
  id: braverman1998labor
  author:
  - family: Braverman
    given: Harry
  title: "Labor and monopoly capital: the degradation of work in the twentieth century"
  title-short: "Labor and monopoly capital"
  publisher-place: New York
  publisher: Monthly Review Press
  issued: 1998
  language: en
################################################################################
- type: book
  id: breznik2009kultura
  author:
  - family: Breznik
    given: Maja
  title: "Kultura danajskih darov: od mecenstva do avtorstva"
  title-short: "Kultura danajskih darov"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2009
  language: sl
################################################################################
- type: book
  id: breznik2021mezdno
  author:
  - family: Breznik
    given: Maja
  title: "Mezdno delo: kritika teorij prekarnosti"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
################################################################################
- type: book
  id: buerger1984theory
  author:
  - family: Bürger
    given: Peter
  title: "Theory of the avant-garde"
  publisher-place: Manchester
  publisher: Manchester University Press
  issued: 1984
  language: en
# C ############################################################################
################################################################################
- type: chapter
  id: cacciari1993dialectics
  author:
  - family: Cacciari
    given: Massimo
  title: "The dialectics of the negative and the Metropolis"
  publisher-place: New Haven
  publisher: Yale University Press
  issued: 1993
  # page: TODO
  language: en
################################################################################
- type: book
  id: ceferin2016niti
  author:
  - family: Čeferin
    given: Petra
  title: "Niti uporabni niti estetski objekt: strukturna logika arhitekture"
  title-short: "Niti uporabni niti estetski objekt"
  publisher-place: Ljubljana
  publisher: Založba ZRC
  collection-title: Teoretska praksa arhitekture
  issued: 2016
  language: sl
################################################################################
- type: book
  id: ciucci1973citta
  author:
  - family: Ciucci
    given: Giorgio
  - family: Dal Co
    given: Francesco
  - family: Manieri-Elia
    given: Mario
  - family: Tafuri
    given: Manfredo
  title: "La città americana dalla guerra civile al New Deal"
  publisher-place: Rim
  publisher: Laterza
  issued: 1973
  language: it
################################################################################
- type: book
  id: ciucci1980theamerican
  author:
  - family: Ciucci
    given: Giorgio
  - family: Dal Co
    given: Francesco
  - family: Manieri-Elia
    given: Mario
  - family: Tafuri
    given: Manfredo
  title: "The American city: From the Civil War to the New Deal"
  title-short: "The American city"
  publisher-place: London
  publisher: Granada
  issued: 1980
  language: en
################################################################################
- type: thesis
  id: corna2016thinking
  # genre: TODO
  author:
  - family: Corna
    given: Luisa Lorenza
  title: "Thinking through antinomies: an enquiry into Manfredo Tafuri's historical method"
  title-short: "Thinking through antinomies"
  publisher: University of Leeds
  issued: 2016
  language: en
################################################################################
- type: review
  id: corna2018journey
  author:
  - family: Corna
    given: Luisa Lorenza
  title: "A journey through Tafuri's unsolvable contradictions: A review of Project of crisis: Manfredo Tafuri and contemporary architecutre by Marco Biraghi"
  title-short: "A journey through Tafuri's unsolvable contradictions"
  reviewed-author:
  - family: Biraghi
    given: Marco
  reviewed-title: "Project of crisis: Manfredo Tafuri and contemporary architecture"
  container-title: "Historical Materialism"
  volume: 26
  issue: 3
  issued: 2018
  page: 210-230
  language: en
################################################################################
# D ############################################################################
################################################################################
- type: article-journal
  id: day2005strategies
  author:
  - family: Day
    given: Gail
  title: "Strategies in the metropolitan merz: Manfredo tafuri and italian workerism"
  title-short: "Strategies in the metropolitan merz"
  container-title: "Radical Philosophy"
  issue: 133
  issued: 2005
  page: 26-38
  language: en
################################################################################
- type: article-journal
  id: day2012manfredo
  author:
  - family: Day
    given: Gail
  title: "Manfredo Tafuri, Fredric Jameson and the contestations of political memory"
  container-title: "Historical Materialism"
  volume: 20
  issue: 1
  issued: 2012
  page: 31-77
  language: en
################################################################################
- type: book
  id: deamer2010building
  editor:
  - family: Deamer
    given: Peggy
  - family: Bernstein
    given: Phillip G.
  title: "Building (in) the future: Recasting labor in architecture"
  title-short: "Building (in) the future"
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  language: en
################################################################################
- type: chapter
  id: deamer2010introduction
  author:
  - family: Deamer
    given: Peggy
  title: "Introduction"
  container-title: "Building (in) the future: Recasting labor in architecture"
  editor:
  - family: Deamer
    given: Peggy
  - family: Bernstein
    given: Phillip G.
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  # page: TODO
  language: en
################################################################################
- type: chapter
  id: deamer2010detail 
  author:
  - family: Deamer
    given: Peggy
  title: "Detail deliberations"
  container-title: "Building (in) the future: Recasting labor in architecture"
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  page: 80-88
  language: en
################################################################################
- type: book
  id: deamer2014architecture
  editor:
  - family: Deamer
    given: Peggy
  title: "Architecture and capitalism: 1845 to present"
  title-short: "Architecture and capitalism"
  publisher-place: London
  publisher: Routledge
  issued: 2014
  language: en
################################################################################
- type: book
  id: deamer2020architecture
  author:
  - family: Deamer
    given: Peggy
  title: "Architecture and labor"
  publisher-place: New York
  publisher: Routledge
  issued: 2020
  language: en
# E ############################################################################
################################################################################
################################################################################
- type: book
  id: fortini2016test
  author:
  - family: Fortini
    given: Franco
  title: "Test of powers: Writings on criticism and literary institutions"
  title-short: "Test of powers"
  translator:
  - family: Toscano
    given: Alberto
  publisher-place: London
  publisher: Seagull Books
  issued: 2016
  language: en
################################################################################
- type: chapter
  id: fortini2016cunning
  author:
  - family: Fortini
    given: Franco
  title: "Cunning as doves"
  container-title: "Test of powers: writings on criticism and literary institutions"
  translator:
  - family: Toscano
    given: Alberto
  publisher-place: London
  publisher: Seagull Books
  issued: 2016
  # page: TODO
  language: en
################################################################################
- type: article-journal
  id: foster1999marx
  author:
  - family: Foster
    given: John Bellamy
  title: "Marx's theory of metabolic rift: Classical foundations for environmental sociology"
  title-short: "Marx's theory of metabolic rift"
  container-title: "American Journal of Sociology"
  volume: 105
  issue: 2
  issued: 1999
  page: 366-405
  language: en
################################################################################
- type: chapter
  id: foucault1984space
  author:
  - family: Foucault
    given: Michel
  title: "Space, knowledge, and power"
  container-title: "Foucault reader"
  editor:
  - family: Rabinow
    given: Paul
  publisher-place: New York
  publisher: Pantheon Books
  issued: 1984
  # page: TODO
  language: en
################################################################################
- type: chapter
  id: foucault2008oko
  author:
  - family: Foucault
    given: Michel
  title: "Oko oblasti"
  container-title: "Vednost -- oblast -- subjekt"
  publisher-place: Ljubljana
  publisher: Krtina
  issued: 2008
  page: 151-171
  language: sl
################################################################################
- type: book
  id: fowkes2023communism
  author:
  - family: Fowkes
    given: Ben
  title: "Communism and the avant-garde in Weimar Germany: a selection of documents"
  title-short: "Communism and the avant-garde in Weimar Germany"
  publisher-place: Boston
  publisher: Brill
  issued: 2023
  language: en
################################################################################
- type: chapter
  id: fowkes2023theshock
  author:
  - family: Fowkes
    given: Ben
  title: "The shock of war and its aftermath"
  container-title: "Communism and the avant-garde in Weimar Germany: a selection of documents"
  publisher-place: Boston
  publisher: Brill
  issued: 2023
  # page: TODO
  language: en
################################################################################
- type: chapter
  id: frampton2010intention
  author:
  - family: Frampton
    given: Kenneth
  title: "Intention, craft, and rationality"
  container-title: "Building (in) the future: Recasting labor in architecture"
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  page: 28-37
  language: en
################################################################################
- type: chapter
  id: frampton2020yugoslavia
  author:
  - family: Frampton
    given: Kenneth
  title: "Former Yugoslavia"
  container-title: "Modern architecture: A critical history"
  publisher-place: London
  publisher: Thames & Hudson
  issued: 2020
  # page: TODO
  language: en
################################################################################
- type: chapter
  id: furlan2013michael
  author:
  - family: Furlan
    given: Sašo
  title: "Michael Heinrich in novo branje Marxa"
  container-author:
  - family: Heinrich
    given: Michael
  container-title: "Kritika politične ekonomije: uvod"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2013
  # page: TODO
  language: sl
# G ############################################################################
################################################################################
- type: book
  id: galimberti2022images
  author:
  - family: Galimberti
    given: Jacopo
  title: "Images of class: Operaismo, Autonomia and the visual Arts (1962--1988)"
  title-short: "Images of class"
  publisher-place: London
  publisher: Verso
  issued: 2022
  language: en
################################################################################
- type: book
  id: gantar1985urbanizem
  author:
  - family: Gantar
    given: Pavel
  title: "Urbanizem, družbeni konflikti, planiranje"
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1985
  language: sl
################################################################################
- type: chapter
  id: ghosh2021toward
  author:
  - family: Ghosh
    given: Swarnabh
  title: "Toward a critique of labor-in-construction"
  container-title: "Non-extractive architecture vol. 1: On designing without depletion"
  publisher-place: London
  publisher: Steinberg Press
  issued: 2021
  # page: TODO
  language: en
################################################################################
- type: book
  id: ginsborg1990history
  author:
  - family: Ginsborg
    given: Paul
  title: "History of contemporary italy: Society and politics: 1943-1988"
  title-short: "History of contemporary Italy"
  publisher-place: London
  publisher: Penguin Books
  issued: 1990
  language: en
################################################################################
- type: article-journal
  id: greppi1963produzione
  author:
  - family: Greppi
    given: Claudio
  - family: Pedrolli
    given: Alberto
  title: "Produzione e programmazione territoriale"
  container-title: "Quaderni rossi"
  volume: 3
  issued: 1963
  page: 94-101
  language: it
################################################################################
- type: chapter
  id: grima2021design
  author:
  - family: Grima
    given: Joseph
  title: "Design without depletion: On the need for a new paradigm in architecture"
  title-short: "Design without depletion"
  container-title: "Non-extractive architecture vol. 1: On designing without depletion"
  editor: Space Caviar
  publisher-place: London
  publisher: Steinberg Press
  issued: 2021
  # page: TODO
  language: en
# H ############################################################################
################################################################################
- type: book
  id: hardt2000empire
  author:
  - family: Hardt
    given: Michael
  - family: Negri
    given: Antonio
  title: "Empire"
  publisher-place: Cambridge, Mass.
  publisher: Harvard University Press
  issued: 2000
  language: en
################################################################################
- type: book
  id: hays1998architecture
  editor:
  - family: Hays
    given: K. Michael
  title: "Architecture theory since 1968"
  publisher-place: Cambridge (Mass.)
  publisher: The MIT Press
  issued: 1998
  language: en
################################################################################
- type: chapter
  id: hays1998introduction
  author:
  - family: Hays
    given: K. Michael
  title: "Introduction"
  editor:
  - family: Hays
    given: K. Michael
  container-title: "Architecture theory since 1968"
  publisher-place: Cambridge (Mass.)
  publisher: The MIT Press
  issued: 1998
  # page: TODO
  language: en
################################################################################
- type: book
  id: hays1998oppositions
  editor:
  - family: Hays
    given: K. Michael
  title: "Oppositions reader: Selected essays 1973--1984"
  title-short: "Oppositions reader"
  publisher-place: New York
  publisher: Princeton Architectural Press
  issued: 1998
  language: en
################################################################################
- type: chapter
  id: hays1998theoppositions
  author:
  - family: Hays
    given: K. Michael
  title: "The oppositions of autonomy and history"
  editor:
  - family: Hays
    given: K. Michael
  container-title: "Oppositions reader: Selected essays 1973--1984"
  container-title-short: "Oppositions reader"
  publisher-place: New York
  publisher: Princeton Architectural Press
  issued: 1998
  # page: TODO
  language: en
################################################################################
- type: book
  id: heinrich2013kritika
  author:
  - family: Heinrich
    given: Michael
  title: "Kritika politične ekonomije: uvod"
  title-short: "Kritika politične ekonomije"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2013
  language: sl
################################################################################
- type: thesis
  id: hoekstra2005building
  # genre: TODO
  author:
  - family: Hoekstra
    given: Titia Rixt
  title: "Building versus bildung: Manfredo Tafuri and the construction of a historical discipline"
  title-short: "Building versus bildung"
  publisher: University of Groningen
  issued: 2005
  language: en
# I ############################################################################
################################################################################
# J ############################################################################
################################################################################
- type: book
  id: jakhel1979iluzija
  author:
  - family: Jakhel
    given: Rudolf
  title: "Iluzija in resničnost urbanih središč: Prispevek h kritiki urbanistične ideologije"
  title-short: "Iluzija in resničnost urbanih središč"
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1979
  language: sl
################################################################################
- type: chapter
  id: jameson1985architecture
  author:
  - family: Jameson
    given: Fredric
  title: "Architecture and the critique of ideology"
  editor:
  - family: Ockman
    given: Joan
  - family: Berke
    given: Deborah
  - family: McLeod
    given: Mary
  container-title: "Architecutre, criticism, ideology"
  publisher-place: Princeton, New Jersey
  publisher: Princeton Architectural Press
  issued: 1985
  page: 51-93
  language: en
################################################################################
- type: chapter
  id: jameson1998architecture
  author:
  - family: Jameson
    given: Fredric
  title: "Architecture and the critique of ideology"
  editor:
  - family: Hays
    given: K. Michael
  container-title: "Architecture theory since 1968"
  publisher-place: Cambridge (Mass.)
  publisher: The MIT Press
  issued: 1998
  page: 442-461
  language: en
################################################################################
- type: book
  id: jameson1994theseeds
  author:
  - family: Jameson
    given: Fredric
  title: "The seeds of time"
  publisher-place: New York
  publisher: Columbia University Press
  issued: 1994
  language: en
################################################################################
- type: book
  id: jameson2012kulturni
  author:
  - family: Jameson
    given: Fredric
  title: "Kulturni obrat: Izbrani spisi o postmoderni, 1983--1998"
  title-short: "Kulturni obrat"
  translator:
  - family: Krašovec
    given: Primož
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2012
  language: sl
# K ############################################################################
################################################################################
- type: book
  id: keynes2006splosna
  author:
  - family: Keynes
    given: John Maynard
  title: "Splošna teorija zaposlenosti, obresti in denarja"
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2006
  language: sl
################################################################################
- type: thesis
  id: keyvanian1992manfredo
  # genre: TODO
  author:
  - family: Keyvanian
    given: Karla L
  title: "Manfredo Tafuri's notion of history and its methodological sources: From Walter Benjamin to Roland Barthes"
  title-short: "Manfredo Tafuri's notion of history and its methodological sources"
  publisher: MIT
  issued: 1992
  language: en
################################################################################
- type: thesis
  id: kocjancic2019zacetki
  # genre: TODO
  author:
  - family: Kocjančič
    given: Maruša
  title: "Začetki Trajne delovne skupnosti samostojnih kulturnih delavcev Ljubljana – Equrna in njen razvoj v osemdesetih letih"
  title-short: "Začetki Trajne delovne skupnosti samostojnih kulturnih delavcev Ljubljana"
  publisher: Filozofska fakulteta Univerze v Ljubljani
  issued: 2019
  language: sl
################################################################################
# L ############################################################################
################################################################################
- type: paper-conference
  id: leach2002everything
  author:
  - family: Leach
    given: Andrew
  title: "'Everything we do is but the larva of our intentions': Manfredo Tafuri and storia dell'architettura italiana 1944-1985"
  title-short: "'Everything we do is but the larva of our intentions'"
  container-title: "Additions to architectural history: XIXth annual conference of the society of architectural historians, Australia and New Zealand"
  container-title-short: "Additions to architectural history"
  issued: 2002
  language: en
################################################################################
- type: thesis
  id: leach2006choosing
  # genre: TODO
  author:
  - family: Leach
    given: Andrew
  title: "Choosing history: A study of Manfredo Tafuri's theorisation of architectural history and architectural history research"
  title-short: "Choosing history"
  publisher: Universiteit Gent
  issued: 2006
  language: en
################################################################################
- type: book
  id: lefebvre2014architecture
  author:
  - family: Lefebvre
    given: Henri
  title: "Toward an architecture of enjoyment"
  editor:
  - family: Stanek
    given: Łukasz
  publisher-place: Minneapolis
  publisher: University of Minnesota Press
  issued: 2014
  language: en
################################################################################
################################################################################
- type: book
  id: lynch2011podoba
  author:
  - family: Lynch
    given: Kevin
  title: "Podoba mesta"
  translator:
  - family: Peklenik
    given: Maja
  publisher-place: Ljubljana
  publisher: Goga
  issued: 2011
  language: sl
# M ############################################################################
################################################################################
- type: book
  id: malesic2021skupno
  editor:
  - family: Malešič
    given: Martina
  - family: Vrečko
    given: Asta
  title: "Skupno v skupnosti: Sedemdeset let zadružnih domov kot družbene infrastrukture"
  title-short: "Skupno v skupnosti"
  publisher-place: Ljubljana
  publisher: Muzej za arhitekturo in oblikovanje
  issued: 2021
  language: sl
################################################################################
- type: book
  id: mandic1996stanovanje
  author:
  - family: Mandič
    given: Srna
  title: "Stanovanje in država"
  publisher-place: Ljubljana
  publisher: Znanstveno in publicistično središče
  issued: 1996
  language: sl
################################################################################
- type: book
  id: manovich2013software
  author:
  - family: Manovich
    given: Lev
  title: "Software takes command: extending the language of new media"
  title-short: "Software takes command"
  publisher-place: New York
  publisher: Bloomsbury Academic
  issued: 2013
  language: en
################################################################################
- type: book
  id: marx1967kapital2
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 2
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1967
  language: sl
################################################################################
- type: book
  id: marx1973kapital3
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 3
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1973
  language: sl
################################################################################
- type: chapter
  id: marx1976nemska
  title: "Nemška ideologija"
  container-title: "Izbrana dela"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: 2
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1976
  language: sl
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: marx1976teze
  author:
  - family: Marx
    given: Karl
  title: "Teze o Feuerbachu"
  container-title: "Izbrana dela"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: 2
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1976
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: marx1976mezdno
  author:
  - family: Marx
    given: Karl
  title: "Mezdno delo in kapital"
  container-title: "Izbrana dela"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: 2
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1976
  # page: TODO
  language: sl
################################################################################
- type: article-journal
  id: marx1976rezultati
  author:
  - family: Marx
    given: Karl
  title: "Rezultati neposrednega produkcijskega procesa"
  container-title: "Časopis za kritiko znanosti"
  volume: 3
  issue: 17-18
  issued: 1976
  page: 241-276
  language: sl
################################################################################
- type: chapter
  id: marx1985uvod
  author:
  - family: Marx
    given: Karl
  title: "Uvod k Očrtom kritike politične ekonomije"
  collection-title: "Temeljna izdaja"
  collection-number: I/8
  container-title: "Kritika politične ekonomije: 1857/58"
  publisher-place: Ljubljana
  publisher: Delavska enotnost
  issued: 1985
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: marx1985ocrti
  author:
  - family: Marx
    given: Karl
  title: "Očrti kritike politične ekonomije"
  title-short: "Očrti"
  collection-title: "Temeljna izdaja"
  collection-number: I/8
  container-title: "Kritika politične ekonomije: 1857/58"
  publisher-place: Ljubljana
  publisher: Delavska enotnost
  issued: 1985
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: marx1989hkritiki
  author:
  - family: Marx
    given: Karl
  title: "H kritiki politične ekonomije. Prvi zvezek"
  collection-title: "Temeljna izdaja"
  collection-number: I/9
  container-title: "H kritiki politične ekonomije: 1858--1861"
  publisher-place: Ljubljana
  publisher: Marksistični center CK ZKS
  issued: 1989
  # page: TODO
  language: sl
################################################################################
- type: book
  id: marx2018brumaire
  author:
  - family: Marx
    given: Karl
  title: "Osemnajsti brumaire Ludvika Bonaparta"
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2018
  language: sl
################################################################################
- type: book
  id: marx2012kapital1
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 1
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2012
  language: sl
################################################################################
- type: book
  id: mau2023mute
  author:
  - family: Mau
    given: Søren
  title: "Mute compulsion: a Marxist theory of the economic power of capital"
  publisher-place: London
  publisher: Verso
  issued: 2023
  language: en
# N ############################################################################
################################################################################
- type: book
  id: negri1979operaio
  author:
  - family: Negri
    given: Antonio
  title: "Dall'operaio massa all'operaio sociale: Intervista sull'operaismo"
  title-short: "Dall'operaio massa all'operaio sociale"
  publisher-place: Milan
  publisher: Multhipla edizioni
  issued: 1979
  language: it
################################################################################
- type: book
  id: negri1984delavci
  author:
  - family: Negri
    given: Antonio
  title: "Delavci in država; Gospostvo in sabotaža; Marx onkraj Marxa"
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1984
  language: sl
################################################################################
- type: book
  id: nesbitt1996theorizing
  editor:
  - family: Nesbitt
    given: Kate
  title: "Theorizing a new agenda for architecture: An anthology of architectural theory 1965--1995"
  title-short: "Theorizing a new agenda for architecture"
  publisher-place: New York
  publisher: Princenton Architectural Press
  issued: 1996
  language: en
# O ############################################################################
################################################################################
- type: book
  id: ockman1985architecture
  editor:
  - family: Ockman
    given: Joan
  - family: Berke
    given: Deborah
  - family: McLeod
    given: Mary
  title: "Architecutre, criticism, ideology"
  publisher-place: Princeton
  publisher: Princeton Architectural Press
  issued: 1985
  language: en
################################################################################
- type: article-journal
  id: ockman1995venice
  author:
  - family: Ockman
    given: Joan
  title: "Venice and New York"
  container-title: "Casabella"
  issue: 619-620
  issued: 1995
  # page: TODO
  language: en
# P ############################################################################
################################################################################
- type: article-journal
  id: panzieri1961luso
  author:
  - family: Panzieri
    given: Raniero
  title: "Sull'uso capitalistico delle macchine nel neocapitalismo"
  container-title: "Quaderni rossi"
  volume: 1
  issued: 1961
  page: 53-72
  language: it
################################################################################
- type: chapter
  id: panzieri1973azione
  author:
  - family: Panzieri
    given: Raniero
  title: "Azione politica e cultura"
  container-title: "Scritti 1956--1960"
  publisher-place: Milano
  publisher: Lampugnani Nigri
  issued: 1973
  # page: TODO
  language: it
# Q ############################################################################
################################################################################
# R ############################################################################
################################################################################
- type: chapter
  id: ranciere2010nezgode
  author:
  - family: Rancière
    given: Jacques
  title: "Nezgode kritične misli"
  container-title: "Emancipirani gledalec"
  translator:
  - family: Koncut
    given: Suzana
  publisher-place: Ljubljana
  publisher: Maska
  issued: 2010
  # page: TODO
  language: sl
################################################################################
- type: chapter
  id: ranciere2018koncept
  author:
  - family: Rancière
    given: Jacques
  title: "Koncept kritike in kritika politične ekonomije: od Pariških rokopisov 1844 do Kapitala"
  title-short: "Koncept kritike in kritika politične ekonomije"
  container-title: "Branje Kapitala"
  container-author:
  - family: Althusser
    given: Louis
  - family: Balibar
    given: Étienne
  - family: Establet
    given: Roger
  - family: Macherey
    given: Pierre
  - family: Rancière
    given: Jacques
  translator:
  - family: Breznik
    given: Maja
  - family: Koncut
    given: Suzana
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2018
  # page: TODO
  language: sl
################################################################################
- type: book
  id: rotar1981pomeni
  author:
  - family: Rotar
    given: Braco
  title: "Pomeni prostora: Ideologije v urbanizmu in arhitekturi"
  title-short: Pomeni prostora
  publisher-place: Ljubljana
  publisher: Delavska enotnost
  issued: 1981
  language: sl
################################################################################
- type: book
  id: rotar1985risarji
  author:
  - family: Rotar
    given: Braco
  title: "Risarji : učenjaki: Ideologije v urbanizmu in arhitekturi"
  title-short: "Risarji : učenjaki"
  publisher-place: Ljubljana
  publisher: Delavska enotnost
  issued: 1985
  language: sl
# S ############################################################################
################################################################################
- type: article-journal
  id: sbardella2000nep
  author:
  - family: Sbardella
    name: Raffaele
  title: "La NEP di 'Classe Operaia'"
  container-title: "Vis-à-vis"
  issue: 8
  issued: 2000
  page: 172-188
  language: it
################################################################################
- type: chapter
  id: simmel2000metropole
  author:
  - family: Simmel
    given: Georg
  title: "Metropole in mentalno življenje"
  container-title: "Izbrani spisi o kulturi"
  translator:
  - family: Erbežnik
    given: Zdenka
  - family: Leskovec
    given: Alfred
  - family: Mihelač
    given: Špela
  publisher-place: Ljubljana
  publisher: Studia humanitatis
  issued: 2000
  # page: TODO
  language: sl
################################################################################
- type: book
  id: smith2020service
  author:
  - family: Smith
    given: Jason E.
  title: "Smart machines and service work: Automation in an age of stagnation"
  title-short: "Smart machines and service work"
  publisher-place: London
  publisher: Reaktion Books
  issued: 2020
  language: en
################################################################################
- type: book
  id: spacecaviar2021nonextractive
  editor: Space Caviar
  title: "Non-extractive architecture vol. 1: On designing without depletion"
  title-short: "Non-extractive architecture vol. 1"
  publisher-place: London
  publisher: Steinberg Press
  issued: 2021
  language: en
################################################################################
- type: book
  id: stierli2018concrete
  author:
  - family: Stierli
    given: Martino
  - family: Kulić
    given: Vladimir
  title: "Toward a concrete utopia: Architecture in Yugoslavia 1948--1980"
  title-short: "Toward a concrete utopia"
  publisher-place: New York
  publisher: The Museum of Modern Art
  issued: 2018
  language: en
# T ############################################################################
################################################################################
- type: book
  id: tafuri1968teorie
  author:
  - family: Tafuri
    given: Manfredo
  title: "Teorie e storia dell'architettura"
  publisher-place: Bari
  publisher: Laterza
  issued: 1968
  language: it
################################################################################
- type: article-journal
  id: tafuri1969peruna
  author:
  - family: Tafuri
    given: Manfredo
  title: "Per una critica dell'ideologia archittetonica"
  title-short: "Per una critica dell'ideologia archittetonica"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1969
  page: 31-79
  language: it
################################################################################
- type: entry-dictionary
  id: tafuri1969rinascimento
  author:
  - family: Tafuri
    given: Manfredo
  title: "Rinascimento"
  editor:
  - family: Portoghesi
    given: Paolo
  container-title: "Dizionario enciclopedico di architettura e urbanistica 5: Posnik-Sipario"
  publisher-place: Rim
  publisher: Istituto editoriale romano
  issued: 1969
  page: 173-232
  language: it
################################################################################
- type: article-journal
  id: tafuri1970lavoro
  author:
  - family: Tafuri
    given: Manfredo
  title: "Lavoro intellettuale e sviluppo capitalistico"
  title-short: "Lavoro intellettuale"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1970
  page: 241-281
  language: it
################################################################################
- type: article-journal
  id: tafuri1971austromarxismo
  author:
  - family: Tafuri
    given: Manfredo
  title: "Austromarxismo e città: 'Das rote Wien'"
  title-short: "Austromarxismo e città"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1971
  page: 259-311
  language: it
################################################################################
- type: article-journal
  id: tafuri1971socialdemocrazia
  author:
  - family: Tafuri
    given: Manfredo
  title: "Socialdemocrazia e città nella Repubblica di Weimar"
  title-short: "Socialdemocrazia e città"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1971
  page: 207-223
  language: it
################################################################################
- type: book
  id: tafuri1973progetto
  author:
  - family: Tafuri
    given: Manfredo
  title: "Progetto e utopia: Architettura e sviluppo capitalistico"
  title-short: "Progetto e utopia"
  publisher-place: Bari
  publisher: Laterza
  issued: 1973
  language: it
################################################################################
- type: book
  id: tafuri1980sfera
  author:
  - family: Tafuri
    given: Manfredo
  title: "La sfera e il labirinto: avanguardie e architettura da Piranesi agli anni '70"
  title-short: "La sfera e il labirinto"
  publisher-place: Torino
  publisher: Einaudi
  issued: 1980
  language: it
################################################################################
- type: book
  id: tafuri1980theories
  author:
  - family: Tafuri
    given: Manfredo
  title: "Theories and history of architecture"
  translator:
  - family: Verrecchia
    given: Giorgio
  publisher-place: London
  publisher: Granada
  issued: 1980
  language: en
################################################################################
- type: book
  id: tafuri1980vienna
  editor:
  - family: Tafuri
    given: Manfredo
  title: "Vienna Rossa: La politica residenziale nella Vienna socialista"
  title-short: "Vienna Rossa"
  publisher-place: Milano
  publisher: Electa
  issued: 1980
  language: it
################################################################################
- type: book
  id: tafuri1985projekt
  author:
  - family: Tafuri
    given: Manfredo
  title: "Projekt in utopija"
  translator:
  - family: Zlodre
    given: Janko
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1985
  language: sl
################################################################################
- type: book
  id: tafuri1987sphere
  author:
  - family: Tafuri
    given: Manfredo
  title: "The sphere and the labyrinth: Avant-gardes and architecture from Piranesi to the 1970s"
  title-short: "The sphere and the labyrinth"
  translator:
  - family: d'Acierno
    given: Pellegrino
  - family: Connoly
    given: Robert
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 1987
  language: en
################################################################################
- type: chapter
  id: tafuri1987ussrberlin
  author:
  - family: Tafuri
    given: Manfredo
  title: "U.S.S.R.--Berlin, 1922: From populism to constructivist international"
  title-short: "U.S.S.R.--Berlin, 1922"
  container-title: "The sphere and the labyrinth: Avant-gardes and architecture from Piranesi to the 1970s"
  translator:
  - family: d'Acierno
    given: Pellegrino
  - family: Connoly
    given: Robert
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 1987
  page: 119-148
  language: en
################################################################################
- type: chapter
  id: tafuri1987sozialpolitik
  author:
  - family: Tafuri
    given: Manfredo
  title: "Sozialpolitik and the city in Weimar Germany"
  title-short: "Sozialpolitik and the city"
  container-title: "The sphere and the labyrinth: Avant-gardes and architecture from Piranesi to the 1970s"
  page: 197-233
  publisher-place: Cambridge, Mass.
  publisher: The MIT Press
  issued: 1987
  page: 196-233
  language: en
################################################################################
- type: article-journal
  id: tafuri1987architecture
  author:
  - family: Tafuri
    given: Manfredo
  title: "L'architecture dans le boudoir"
  translator:
  - family: Zlodre
    given: Janko
  container-title: "Arhitektov bilten"
  issue: 91/92
  issued: 1987
  # page: TODO
  language: sl
################################################################################
- type: article-journal
  id: tafuri1988architecture
  author:
  - family: Tafuri
    given: Manfredo
  title: "L'architecture dans le boudoir"
  translator:
  - family: Zlodre
    given: Janko
  container-title: "Arhitektov bilten"
  issue: 93/94
  issued: 1988
  # page: TODO
  language: sl
################################################################################
- type: book
  id: tafuri1989history
  author:
  - family: Tafuri
    given: Manfredo
  title: "History of Italian architecture, 1944--1985"
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 1989
  language: en
################################################################################
- type: interview
  id: tafuri1993storia
  author:
  - family: Tafuri
    given: Manfredo
  title: "La storia come progetto"
  interviewer:
  - family: Passerini
    given: Luisa
  issued: 1993
  language: it
################################################################################
- type: book
  id: tafuri2002storia
  author:
  - family: Tafuri
    given: Manfredo
  title: "Storia dell'architettura italiana 1944--1985"
  publisher-place: Torino
  publisher: Einaudi
  issued: 2002
  language: it
################################################################################
- type: book
  id: tafuri2022progetto
  author:
  - family: Tafuri
    given: Manfredo
  title: "Dal progetto alla storia"
  editor:
  - family: Skansi
    given: Luka
  publisher-place: Macerata
  publisher: Quodlibet
  issued: 2022
  language: it
################################################################################
- type: chapter
  id: tafuri2022citta
  author:
  - family: Tafuri
    given: Manfredo
  - family: Piccinato
    given: Giorgio
  - family: Quilici
    given: Vieri
  title: "La città territtorio: Verso una nuova dimensione"
  container-title: "Dal progetto alla storia"
  publisher-place: Macerata
  publisher: Quodlibet
  issued: 2022
  # page: TODO
  language: it
################################################################################
- type: chapter
  id: tafuri2022teoria
  author:
  - family: Tafuri
    given: Manfredo
  title: "Teoria e critica nella cultura urbanistica italiana del dopoguerra"
  container-title: "Dal progetto alla storia"
  publisher-place: Macerata
  publisher: Quodlibet
  issued: 2022
  # page: TODO
  language: it
################################################################################
- type: book
  id: thoburn2022brutalism
  author:
  - family: Thoburn
    given: Nicholas
  title: "Brutalism as found: housing crisis at Robin Hood gardens"
  title-short: "Brutalism as found"
  publisher-place: London
  publisher: Goldsmiths Press
  issued: 2022
  language: en
################################################################################
- type: chapter
  id: tomba2014thefragment
  author:
  - family: Tomba
    given: Massimiliano
  - family: Bellofiore
    given: Riccardo
  title: "The 'Fragment on machines' and the Grundrisse: The workerist reading in question"
  title-short: "The 'Fragment on machines' and the Grundrisse"
  editor:
  - dropping-particle: van der
    family: Linden
    given: Marcel
  - family: Heinz Roth
    given: Karl
  container-title: "Beyond Marx: Theorising the global labour relations of the twenty-first century"
  container-title-short: Beyond Marx
  publisher-place: Boston
  publisher: Brill
  issued: 2014
  # page: TODO
  language: en
################################################################################
- type: article-journal
  id: tronti1962fabbrica
  author:
  - family: Tronti
    given: Mario
  title: "La fabbrica e la società"
  container-title: "Quaderni rossi"
  volume: 2
  issued: 1962
  page: 1-31
  language: it
################################################################################
- type: book
  id: tronti2019workers
  author:
  - family: Tronti
    given: Mario
  title: "Workers and capital"
  translator:
  - family: Broder
    given: David
  publisher-place: New York
  publisher: Verso
  issued: 2019
  language: en
################################################################################
- type: chapter
  id: tronti2019our
  author:
  - family: Tronti
    given: Mario
  title: "Our operaismo"
  container-title: "Workers and capital"
  translator:
  - family: Broder
    given: David
  publisher-place: New York
  publisher: Verso
  issued: 2019
  # page: TODO
  language: en
# U ############################################################################
################################################################################
# V ############################################################################
################################################################################
- type: book
  id: vidler2016zgodovina
  author:
  - family: Vidler
    given: Anthony
  title: "Zgodovina neposredne sedanjosti: invencije arhitekturnega modernizma"
  title-short: "Zgodovine neposredne sedanjosti"
  translator:
  - family: Lovrenov
    given: Maja
  - family: Jerič
    given: Monika
  publisher-place: Ljubljana
  publisher: Založba ZRC
  collection-title: Teoretska praksa arhitekture
  issued: 2016
  language: sl
# W ############################################################################
################################################################################
- type: chapter
  id: wagner1987thesocialization
  author:
  - family: Wagner
    given: Martin
  title: "The socialization of the building industry"
  container-title: "The sphere and the labyrinth: Avant-gardes and architecture from Piranesi to the 1970s"
  translator:
  - family: d'Acierno
    given: Pellegrino
  - family: Connoly
    given: Robert
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 1987
  page: 234-263
  language: en
################################################################################
- type: book
  id: wright2002storming
  author:
  - family: Wright
    given: Steve
  title: "Storming heaven: Class composition and struggle in Italian autonomist marxism"
  title-short: "Storming heaven"
  publisher-place: London
  publisher: Pluto Press
  issued: 2002
  language: en
################################################################################
- type: book
  id: wright2021weight
  author:
  - family: Wright
    given: Steve
  title: "The weight of the printed word: Text, context and militancy in operaismo"
  title-short: "The weight of the printed word"
  publisher-place: Leiden
  publisher: Brill
  issued: 2021
  language: en
# X ############################################################################
################################################################################
# Y ############################################################################
################################################################################
# Z ############################################################################
################################################################################
- type: article-journal
  id: zanini2010philosophical
  author:
  - family: Zanini
    given: Adelino
  title: "On the 'philosophical foundations' of italian Workerism: a conceptual aproach"
  container-title: "Historical Materialism"
  volume: 18
  issued: 2010
  page: 39-63
  language: en
################################################################################
- type: chapter
  id: zlodre1988opastirju
  author:
  - family: Zlodre
    given: Janko
  title: "O pastirju modernega in njegovi drami"
  container-author:
  - family: Loos
    given: Adolf
  - family: Wittgenstein
    given: Ludwig
  - family: Amendolagine
    given: Francesco
  - family: Cacciari
    given: Massimo
  editor:
  - family: Dešman
    given: Miha
  - family: Mojca
    given: Dobnikar
  container-title: "Zbornik oikos in drugo: O Loosu in Wittgensteinu"
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1988
  # page: TODO
  language: sl
################################################################################
- type: book
  id: zlodre2011notice
  author:
  - family: Gerdol Zlodre
    given: Janko
  title: "Notice o arhitekturi in drugem"
  publisher-place: Ljubljana
  publisher: "/*cf."
  issued: 2011
  language: sl
################################################################################
- type: chapter
  id: zlodre2011zapiski
  author:
  - family: Gerdol Zlodre
    given: Janko
  title: "Zapiski o ideoloških oblikah arhitekturne zavesti"
  container-title: "Notice o arhitekturi in drugem"
  publisher-place: Ljubljana
  publisher: "/*cf."
  issued: 2011
  # page: TODO
  language: sl
################################################################################
################################################################################
################################################################################
reference-section-title: "Literatura"
references:
########################################################################
- type: book
  id: deamer2020architecture
  author:
  - family: Deamer
    given: Peggy
  title: "Architecture and labor"
  publisher-place: New York
  publisher: Routledge
  issued: 2020
  language: en
########################################################################
########################################################################
- type: book
  id: biraghi2019larchitetto
  author:
  - family: Biraghi
    given: Marco
  title: "L'architetto come intellettuale"
  publisher-place: Torino
  publisher: Einaudi
  issued: 2019
  language: it
########################################################################
- type: book
  id: tafuri1969larchitettura
  author:
  - family: Tafuri
    given: Manfredo
  title: "L'architettura dell'Umanesimo"
  publisher-place: Bari
  publisher: Laterza
  issued: 1969
  language: it
########################################################################
- type: book
  id: tafuri1980theories
  author:
  - family: Tafuri
    given: Manfredo
  title: "Theories and history of architecture"
  translator:
  - family: Verrecchia
    given: Giorgio
  publisher-place: London
  publisher: Granada
  issued: 1980
  language: en
########################################################################
- type: article-journal
  id: tafuri1971socialdemocrazia
  author:
  - family: Tafuri
    given: Manfredo
  title: "Socialdemocrazia e città nella Repubblica di Weimar"
  title-short: "Socialdemocrazia e città"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1971
  page: 207-223
  language: it
########################################################################
- type: article-journal
  id: tafuri1969peruna
  author:
  - family: Tafuri
    given: Manfredo
  title: "Per una critica dell'ideologia archittetonica"
  title-short: "Per una critica dell'ideologia archittetonica"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1969
  page: 31-79
  language: it
########################################################################
- type: book
  id: tafuri1985projekt
  author:
  - family: Tafuri
    given: Manfredo
  title: "Projekt in utopija"
  translator:
  - family: Zlodre
    given: Janko
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1985
  language: sl
########################################################################
- type: thesis
  id: corna2016thinking
  genre: doktorska disertacija
  author:
  - family: Corna
    given: Luisa Lorenza
  title: "Thinking through antinomies: an enquiry into Manfredo Tafuri's historical method"
  title-short: "Thinking through antinomies"
  publisher: University of Leeds
  issued: 2016
  language: en
########################################################################
- type: book
  id: galimberti2022images
  author:
  - family: Galimberti
    given: Jacopo
  title: "Images of class: Operaismo, Autonomia and the visual Arts (1962--1988)"
  title-short: "Images of class"
  publisher-place: London
  publisher: Verso
  issued: 2022
  language: en
########################################################################
- type: article-journal
  id: tafuri1970lavoro
  author:
  - family: Tafuri
    given: Manfredo
  title: "Lavoro intellettuale e sviluppo capitalistico"
  title-short: "Lavoro intellettuale"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1970
  page: 241-281
  language: it
########################################################################
########################################################################
- type: thesis
  id: brown2022out
  genre: doktorska disertacija
  author:
  - family: Brown
    given: J. Dakota
  title: "Out of sorts: machinery, theory, and the revolutions in typographical labor"
  title-short: "Out of sorts"
  publisher: Northwestern University
  issued: 2022
  language: en
########################################################################
- type: chapter
  id: koolhaas2010junkspace
  author:
  - family: Koolhaas
    given: Rem
  title: "Junkspace"
  container-title: "Constructing a new agenda for architecture: architectural theory 1993--2009"
  editor:
  - family: Sykes
    given: A. Krista
  publisher-place: New York
  publisher: Princeton Architectural Press
  issued: 2010
  page: 136-151
  language: en
########################################################################
########################################################################
- type: book
  id: deamer2010building
  editor:
  - family: Deamer
    given: Peggy
  - family: Bernstein
    given: Phillip G.
  title: "Building (in) the future: recasting labor in architecture"
  title-short: "Building (in) the future"
  publisher-place: New Haven
  publisher: Princeton Architectural Press
  issued: 2010
  language: en
########################################################################
- type: book
  id: deamer2014architecture
  editor:
  - family: Deamer
    given: Peggy
  title: "Architecture and capitalism: 1845 to present"
  title-short: "Architecture and capitalism"
  publisher-place: London
  publisher: Routledge
  issued: 2014
  language: en
########################################################################
- type: book
  id: deamer2015thearchitect
  title: "The architect as worker: immaterial labor, the creative class, and the politics of design"
  title-short: "The architect as worker"
  editor:
  - family: Deamer
    given: Peggy
  publisher-place: London
  publisher: Bloomsbury
  issued: 2015
  language: en
########################################################################
########################################################################
- type: article-journal
  id: tafuri1971austromarxismo
  author:
  - family: Tafuri
    given: Manfredo
  title: "Austromarxismo e città: 'Das rote Wien'"
  title-short: "Austromarxismo e città"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1971
  page: 259-311
  language: it
########################################################################
# dispozicija
########################################################################
- type: book
  id: biraghi2019larchitetto
  author:
  - family: Biraghi
    given: Marco
  title: "L'architetto come intellettuale"
  publisher-place: Torino
  publisher: Einaudi
  issued: 2019
  language: it
########################################################################
- type: book
  id: tafuri1985projekt
  author:
  - family: Tafuri
    given: Manfredo
  title: "Projekt in utopija"
  translator:
  - family: Zlodre
    given: Janko
  publisher-place: Ljubljana
  publisher: Univerzitetna konferenca ZSMS
  issued: 1985
  language: sl
########################################################################
- type: article-journal
  id: tafuri1970lavoro
  author:
  - family: Tafuri
    given: Manfredo
  title: "Lavoro intellettuale e sviluppo capitalistico"
  title-short: "Lavoro intellettuale"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1970
  page: 241-281
  language: it
########################################################################
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
########################################################################
########################################################################
- type: book
  id: deamer2020architecture
  author:
  - family: Deamer
    given: Peggy
  title: "Architecture and labor"
  publisher-place: New York
  publisher: Routledge
  issued: 2020
  language: en
########################################################################
########################################################################
########################################################################
- type: thesis
  id: corna2016thinking
  genre: doktorska disertacija
  author:
  - family: Corna
    given: Luisa Lorenza
  title: "Thinking through antinomies: an enquiry into Manfredo Tafuri's historical method"
  title-short: "Thinking through antinomies"
  publisher: University of Leeds
  issued: 2016
  language: en
########################################################################
- type: book
  id: galimberti2022images
  author:
  - family: Galimberti
    given: Jacopo
  title: "Images of class: Operaismo, Autonomia and the visual Arts (1962--1988)"
  title-short: "Images of class"
  publisher-place: London
  publisher: Verso
  issued: 2022
  language: en
########################################################################
- type: book
  id: deamer2015thearchitect
  title: "The architect as worker: immaterial labor, the creative class, and the politics of design"
  title-short: "The architect as worker"
  editor:
  - family: Deamer
    given: Peggy
  publisher-place: London
  publisher: Bloomsbury
  issued: 2015
  language: en
########################################################################
########################################################################
- type: book
  id: mau2023mute
  author:
  - family: Mau
    given: Søren
  title: "Mute compulsion: a Marxist theory of the economic power of capital"
  publisher-place: London
  publisher: Verso
  issued: 2023
  language: en
########################################################################
- type: book
  id: carpo2013thedigital
  editor:
  - family: Carpo
    given: Mario
  title: "The digital turn in architecture, 1992--2012"
  publisher-place: Chichester
  publisher: Wiley
  issued: 2013
  language: en
########################################################################
- type: book
  id: manovich2013software
  author:
  - family: Manovich
    given: Lev
  title: "Software takes command: extending the language of new media"
  title-short: "Software takes command"
  publisher-place: New York
  publisher: Bloomsbury Academic
  issued: 2013
  language: en
########################################################################
- type: book
  id: marx2012kapital1
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 1
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2012
  language: sl
########################################################################
- type: chapter
  id: ferro2016dessin
  author:
  - family: Ferro
    given: Sérgio
  title: "Dessin/chantier: an introduction"
  title-short: "Dessin/chantier"
  container-title: "Industries of architecture"
  editor:
  - family: Lloyd Thomas
    given: Katie
  - family: Amhoff
    given: Tilo
  - family: Beech
    given: Nick
  translator:
  - family: Agarez
    given: Ricardo
  - family: Kapp
    given: Silke
  publisher-place: London
  publisher: Routledge
  issued: 2016
  page: 94-105
  language: en
########################################################################
- type: thesis
  id: brown2022out
  genre: doktorska disertacija
  author:
  - family: Brown
    given: J. Dakota
  title: "Out of sorts: machinery, theory, and the revolutions in typographical labor"
  title-short: "Out of sorts"
  publisher: Northwestern University
  issued: 2022
  language: en
########################################################################
- type: book
  id: pasquinelli2023theeye
  author:
  - family: Pasquinelli
    given: Matteo
  title: "The eye of the master: a social history of artificial intelligence"
  title-short: "The eye of the master"
  publisher-place: London
  publisher: Verso
  issued: 2023
  language: en
########################################################################
- type: book
  id: kreft1994estetika
  author:
  - family: Kreft
    given: Lev
  title: "Estetika in poslanstvo"
  collection-title: Sophia
  publisher-place: Ljubljana
  publisher: Znanstveno in publicistično središče
  issued: 1994
  language: sl
########################################################################
- type: book
  id: senk2015kapsula
  author:
  - family: Šenk
    given: Peter
  title: "Kapsula: tipologija druge arhitekture"
  title-short: "Kapsula"
  collection-title: Teoretska praksa arhitekture
  publisher-place: Ljubljana
  publisher: Založba ZRC
  issued: 2015
  language: sl
########################################################################
# vim: spelllang=sl,en
...
