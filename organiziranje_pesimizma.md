---
title: "Organiziranje pesimizma"
description: |
  Zapiski k teorijam revolucionarnega pesimizma.
...

V ozadju vsakega spraševanja o progresivnem objektu je intelektualni razvoj od
naše novejše zgodovine do naše sedanjosti, ki takšno spraševanje otežuje.
Prevladujočo pripoved zadnjih štirideset let povzame filozof Jacques Rancière:

> Pravijo, da smo doživeli konec določenega zgodovinskega obdobja: ne le konec
> delitve sveta na kapitalistični in komunistični blok, temveč tudi pogleda na
> svet skozi prizmo razrednega konflikta; ne le konec razrednega konflikta,
> temveč tudi politike, pojmovane kot prakse delitve. Ne le konec mnogih
> revolucionarnih upov ali iluzij, temveč tudi utopij in ideologij nasploh --
> ali v najbolj izčrpni formulaciji, "velikih pripovedi" in verjetij glede usode
> človeštva. Ne le konec nekega obdobja v zgodovini, temveč "zgodovine" same,
> razumljene kot čas obljube, ki naj bi se izpolnila. [@ranciere2012je, 133-134]

Ta pripoved o naši zgodovini kot o "koncu" in naši sedanjosti kot "času post" po
Rancièrju vsiljuje določen pogled na naš čas, na to kaj naš čas omogoča in kaj
onemogoča:

> Čeprav se predstavlja kot kritičen in radikalen, je diskurz o koncu in o
> "post" zgolj določen način izrekanja konfiguracije sveta, ki jo danes
> narekujejo dominantne sile. Priprada teoretskemu arzenalo, ki naj bi obstoječi
> red potrdil kot edini možni svet. [@ranciere2012je, 134]

Takšna intelektualna pokrajina ni ozadje, ki je vredno posebne pozornosti zgolj
kakšne političnofilozofske misli in na katerega se arhitekturna misel lahko
poljubno ozira. "Intelektualna protirevolucija", kot uveljavljanju te pripovedi
pravi Rancière, morda še bolj očitno zadeva arhitekturno misel, kjer so
prispevki k tej pripovedi in njene posledice dobesedno postavljeni pred nas:
intelektualna protirevolucija je na področju arhitekturne očitna, kot je očiten
horizont *Canary Wharf* za rušitvijo *Robin Hood Gardens*.

Rancièrjeva opredelitev tega intelektualnega razvoja kot "protirevolucije" je
pomembna. Ne gre za kolektivno razčaranje, streznitev ali za kolektivno
preusmeritev zavesti. Ne gre za izgubo, ki se je zgodila, ampak za poraz, ki je
bil prizadejan. S takšno opredelitvijo izpostavi, da gre za dejavno
*protirevolucijo*, ki kritično in revolucionarno mišljenje predeluje v lasten
diskurz, izpostavi da je diskurz intelektualne protirevolucije svojo hegemonijo
zgradil "z vključitvijo opisov in pripovedi, argumentov in verjetij, izposojenih
iz kritične tradicije" [@ranciere2012je, 134]. Ta poanta ima dva vredna momenta.

Prvi je, da apologetskemu govoru o nekem koncu in nekem času "post" postavi
vprašanje o kontinuiteti in stopnjevanju. V tem ga lahko vzporedimo tafurijanski
opredelitvi postmodernizma kot do skrajnosti privedenih značilnosti modernega,
kot *hipermodernizem* [gl. @tafuri1989history, 189-193]. Ali, na nekoliko
prenesenem terenu, opredelitvi sodobne arhitekture, ki jo je postavil Rem
Koolhaas, da ta ravno v točkah, kjer se želi osvoboditi modernistične represije
pravil in repeticije, poglablja standardizacijo do razsežnosti kjer modul
postane mozaik [@koolhaas2010junkspace, 140].

Drugi pomemben moment Rancièrjeve opredelitve sodobne intelektualne krajine kot
protirevolucije pa je v tem, da postavi vprašanje: kaj je v kritični zavesti
tako uporabnega za protirevolucionarni diskurz? Ravno to, kar je anticipirano v
vprašanju "ali je čas emancipacije minil?" Ravno to pojmovanje časovnosti,
pojmovanje časa kot niza možnosti. Sodobne kritične zavesti si z intelektualno
protirevolucijo delijo

> predpostavko enosmerne časovnosti globalnega procesa zgodovine oziroma
> trditev, da vse oblike delovanja določa časovnost globalnega procesa, ki
> posameznike in družbe vodi k cilju, ki se ga ne zavedajo. [@ranciere2012je,
> 139]

Rancière imenuje dve, nasprotujoči si, sodobni kritični alternativi
intelektualni protirevoluciji, ki si delita to predpostavko časovnosti. Eno
opredeli kot optimistično diagnozo, drugo kot pesimistično. Prvo predstavlja
postoperaistična teza, da sta prihodnost svobode in avtonomija že navzoči v
oblikah kapitalistične dominacije. Torej interpretacija, da proces
visokotehnološke in dinamične "postindustrializacije" proizvaja novo obliko
subjekta -- *kognitivnega delavca* --, ki bo (objektivno) poklican in opremljen
uresničiti nov svet svobode in enakosti. Drugo predstavlja nasprotna
interpretacija, da pojav kognitivnega delavca pomeni, da je tudi kognitivno moč
človeka zasegel proces industrijske proizvodnje in ga objektiviral kot človeku
tujo moč. Ta apokaliptični diskurz je, po Rancièrju, potem podlaga tistim
teorijam, ki o revoluciji teoretizirajo kot o dejanju nihilistične subverzije
[@ranciere2012je, 140].

Kar po Rancièrju pri obeh tezah prispeva k sodobni pripovedi o "koncu" in k
potrjevanju obstoječega reda kot edinega možnega sveta je, da obe "emancipacijo"
umestita "v obliko časovnosti, ki jo naredi za rezultat svojega nasprotja". S
tako pojmovano emancipacijo je "konec", "ker naj bi bila možnost, ki jo
proizvede nemožnost" [@ranciere2012je, 141]. Izhod, ki ga ponudi Rancière, je
prelom s takšnim pojmovanjem časa in ponoven premislek o tem kaj "emancipacija"
pomeni. V razmerju do imenovanih tez torej zavrne pojmovanje časa kot niza
možnosti, kot procesa, ki globalno ureja ves ritem individualnega in
kolektivnega življenja. Emancipatorno prakso pa opredeli kot prakso, ki afirmira
egalitarnost, ki je predpostavljena tudi v odnosih neenakosti, ["[N]obena naloga
ne more biti izvršena, nobena vednost prenesena, nobena avtoriteta
vzpostavljena, ne da bi gospodar ali učitelj moral vsaj v najmanjši meri s
tistim, ki mu zapoveduje ali ga poučuje, spregovoriti kot 'enak z enakim', torej
predpostaviti, da ima tudi ta zmožnost razumevanja, ki je enaka njegovi. Odnosi
neenakosti lahko delujejo le, zahvaljujoč mnoštvu egalitarnih odnosov."
@ranciere2012je, 142] in ki trenutkom, ko subjekti z lastnim delovanjem
dokazujejo svojo enakost, podeli trajno obliko vidnosti in inteligibilnosti
[@ranciere2012je, 144]. *Splošnejši izraz te formule -- ki je: nasproti
totalizirajočim diagnozam o našem času afirmirati primere, ki niso povsem
zvedljivi na takšno diagnozo, ter jih teoretsko opredeliti -- je tudi podlaga
prispevkom v tej knjigi in je torej tudi prispevek te knjige.*

Toda ustavlili bi se tudi pri temi, ki se ponuja na poti Rancièrjeve kritike:
pri opredelitvi sodobnega razvoja intelektualne krajine kot *protirevolucije* in
razvrščanju na os *optimizem--pesimizem*. Izhajajoč iz te teme lahko opredelimo
Rancièreju obraten -- v smislu komplementaren -- teoretski projekt, ki
ugotovitev, da odnosi neenakosti lahko delujejo le zahvaljujoč mnoštvu
egalitarnih odnosov, ne vzame kot zagotovilo, temveč kot dodatno opozorilo o
delovalnosti protirevolucije. Teoretski projekt, ki Rancièrjevo predpostavko
enakosti razume *znotraj* delovanja protirevolucije, oziroma kot je problem
izražen v romanu *Rdeča lilija*:

> Na Francoskem smo vojaki in državljani. Biti državljan, vzrok spet za ponos!
> Za reveže to pomeni, da vzdržujejo in ohranjajo bogataše na oblasti in v
> brezdelju. Delati morajo tam pred veličastno enakostjo zakonov, ki tako
> bogatinom, kakor revežem enako prepovedujejo spati pod mostovi, beračiti po
> ulicah in krasti kruh. To je ena izmed dobrin Revolucije. [@france1962rdeca,
> 77]

Torej ob tem, da Rancière izpostavljeni nevarnosti protirevolucije nazadnje
vseeno zoperstavi optimistično "zaupanje v njeno pozitivno dinamiko", ostaja
luknja za tisto misel, ki izhaja iz "pesimistične diagnoze". Te ne moremo
podcenjevati, da gre zgolj za samozadovoljno "pankersko zavest", temveč ji
priznati upravičenost njenega vprašanja: ali avtomatski in dinamični strojni
sistemi, ki vzhajajo na podlagi aktualnega razvoja v zbiranju in obdelovanju
podatkov, uhajajo razlagam utemeljenih v družbenosti (političnosti) teh
procesov, ker uhajajo človeški skupnosti?

Takšno -- algoritemsko izpeljano onkraj običajnih sposobnosti človeške kognicije
-- arhitekturo lahko šele pričakujemo. [gl. @morel2019theorigins] A osnovna
*tujost* procesov, ki se vršijo na področju zidave, je že dolgo priznana.
Nadležen prikaz tega je na primer diskurz o stanovanjski krizi in ostalih
posledicah špekulacije oziroma zasebne lastnine. Kljub temu, da gre za redno
temo novinarskega poročanja in strokovnih razprav, informacije in mnenja "iz
prve roke" lahko slišimo zgolj s strani žrtev teh procesov. Ko že mislimo, da
smo na okroglo mizo ali na informativno oddajo povabili odgovorno osebo, nam ta,
solidarno z nami, opiše delovanje *trga*. Nazadnje pa je tujost procesov
predstavljena tudi kot *želen* način (ne)poseganja v prostor. Tako je višji
častnik izraelskih vojaških sil umik človeškega preverjanja rezultatov
avtomatskih statističnih modelov za določanje tarč -- ljudi in stavb v Gazi --
komentiral sistem:

> Dokazal se je. Nekaj je na tem statističnem pristopu, kar vzdržuje določeno
> normo in standard. V tej operaciji je bilo nelogično veliko bombnih napadov.
> Po mojem spominu, brez primere. In veliko bolj zaupam statističnemu mehanizmu
> kot vojaku, ki je pred dvema dnevoma izgubil prijatelja. Vsi tam, vključno z
> mano, so 7\. oktobra nekoga izgubili. Stroj je to storil hladnokrvno in zaradi
> tega je bilo lažje. [@abraham2024lavender]

Pesimistične diagnoze torej imajo svoj smisel. Odzivne so na vse vidnejšo
tendenco "outsourcanja" profesionalnih in avtorskih odločitev družbenim strojem,
kjer človeška odgovornost ni zgolj prikrita, temveč je samo vprašanje
odgovornosti onemogočeno. Proces v svoji osnovi ni nov. Ob neki drugi, a
tehnopolitično sorodni priložnosti teoretik in kritik Walter Benjamin, s
srhljivo anticipacijo, zavzame pesimistično stališče:

> [P]esimizem na celi črti. Vsekakor in povsem. Nezaupanje v usodo literature,
> nezaupanje v usodo svobode, nezaupanje v usodo evropskega človeštva, predvsem
> pa nezaupanje, nezaupanje in nezaupanje v vsak sporazum: med razredi, med
> narodi in med posamezniki. Ter neomejeno zaupanje izključno v I. G. Farben in
> v mirno izpopolnitev vojnega letalstva. [@benjamin2016nadrealizem, 50]

Toda ne kot melanholični fatalizem ali za voljo nekakšne poetike odpovedi.
Fatalizem je prepoznal ravno na strani optimizma takratnih meščanskih in
reformističnih strank, ki so svoje kompromise opravičevale z vero v napredek. S
to vero so opravičevale obljubo, da je prihodnost svobode v tem, da vsakdo že
živi "kot da je svoboden" -- torej, da je svoboda že v drži do podobe svobode
--, med tem ko napredek koraka *svoj* korak.

Nasproti temu Benjamin predlaga neodložljivo *organiziranje pesimizma*. To je,
odpoved tistim nalogam, h katerim so politične in umetniške avantgarde rade
poklicane -- posredovanje podob uresničene ali prihajajoče svobode -- in
mapiranje tistih funkcij, ki preostanejo po tej odpovedi, ali so z njo šele
omogočene. Torej organiziranje na lastnem področju delovanja, brez alibijev v
idejah svobode, napredka, enakosti. Ta poziv -- preusmeritev profesionalne
pozornosti od vprašanj odnosa umetniškega dela *do* družbe k vprašanju odnosa
umetniškega dela *znotraj* nje -- Benjamin nadaljuje v *Avtor kot proizvajalec*,
[@benjamin2016avtor] kjer za organiziranje pesimizma postavi neposrednejša
vprašanja. Z njimi lahko olajšamo uvodne zadržke do spraševanja: namesto, da se
vprašamo o odnosu arhitekturnega objekta do ideje progresivnosti, se lahko
vprašamo o progresivnosti znotraj arhitekture. Pri iskanju progresivnega objekta
arhitekture se lahko, povzeto po Benjaminu, vprašamo: *Ali je svoje sodelavce --
v našem primeru arhitektke in arhitekte -- kaj naučil in ali je podal predloge
za prefunkcioniranje romana, drame, pesmi in, v našem primeru, arhitekture?*

<!--

Pesimističen v tem, da se odpoveduje zavzemanju tistih vlog, ki 

Fatalizem
prepoznal ravno pri takratnih meščanskih in reformističnih strankah, ki so svoje
kompromise utemeljevale v optimizmu, ki je veleval, da je lepša prihodnost v
tem, da vsakdo že živi "kot da je svoboden", torej v spremembi drže do podobe
svobode. 
V tem primeru pesimizem predstavlja odpoved od tistih vlog, ki si jih je 
inteligenca in umetniške avantgarde

*Organiziranje*, ker morebitnih nalog inteligence ni več mogoče kontemplativno
izpolniti. *Pesimizem*, ker se odpoveduje 

Ne vprašanje ali je kar političnega v arhitekturi, ampak ali je kaj
arhitekturnega v politiki.

Vprašanje odnosa arhitekturnega dela do napredka, ampak postavi vprašanje
napredka znotraj arhitekturnega delovanja. 

Iskanje progresivnega objekta kot iskanje tistih margin 
Funkcija pesimizma je natančneje mapirati 

> V imenu svojih litararnih prijateljev postavi ultimat, vpričo katerega mora ta
> brezvestni, diletantski optimizem nedvomno pokazati svoje barve: kje so
> predpostavke revolucije? V spremembi drže ali zunanjih razmer? To je
> kardinalno vprašanje, ki določa razmerje politike do morale in ne dopušča
> nobene potlačitve. [@benjamin2016nadrealizem, 50]

In kaj je odgovor?

scientific method: What
if automation already shows that there is a dynamic relation intrinsic to
computational processing between input data and algorithmic instructions,
involving a non-linear elaboration of data? What if this dynamic is not simply
explainable in terms of its a posteriori use, i.e., once it is either socially used or
mentally processed?

zakaj je org. pesimizma še pomembna za arh.? (ker vrne pozornost nazaj na
tehniko? Kako?)

[rancixre opredeli naš čas kot intelektualno protirevolucijo, potrebujemo
znanost protirevolucij]

On predlaga nekaj tretjega, (KAJ) mi pa bi nekoliko premestili njegov diskurz. Zakaj?
Ker ima intelektualna protirevolucija realno podlago. Želimo teorijo
protirevolucij. Osredotočili bi se na njegovo opredelitev optimistične in
pesimistične diagnoze in v to umestili tudi njegov predlog, ki je nenazadnje
tudi optimističen.

Metoda enakosti se nenazadnje poigrava z zaupanjem v "pozitivno dinamiko" (145)
torej ga lahko umestimo med optimiste in zatorej moramo svojo kritiko
nadaljevati med pesimisti.

Konstruktivna uporaba svojih veščin (optimizem) se vedno dogaja v kontektstu
poraza. Osredotočanje na optimistične in progresivne plati ne odpravlja dejstva,
da absolutno, z znanstvenim konsenzom, izgubljamo. Hkrati pa odkrito izhodišče,
da je konec, ni alibi. Temu pravim, po Benjaminu, organizacija pesimizma.

[gl. Delitev čutnega](delitev_cutnega.md)

-->

---
lang: sl
reference-section-title: Literatura
references:
- type: article-journal
  id: ranciere2012je
  author:
  - family: Rancière
    given: Jacques
  title: "Je čas emancipacije minil?"
  translator:
  - family: Benčin
    given: Rok
  container-title: "Filozofski vestnik"
  volume: XXXIII
  issue: 1
  issued: 2012
  page: 133-145
  language: sl
- type: book
  id: tafuri1989history
  author:
  - family: Tafuri
    given: Manfredo
  title: "History of Italian architecture, 1944--1985"
  publisher-place: Cambridge, Mass.
  publisher: MIT Press
  issued: 1989
  language: en
- type: chapter
  id: koolhaas2010junkspace
  author:
  - family: Koolhaas
    given: Rem
  title: "Junkspace"
  container-title: "Constructing a new agenda for architecture: architectural theory 1993--2009"
  editor:
  - family: Sykes
    given: A. Krista
  publisher-place: New York
  publisher: Princeton Architectural Press
  issued: 2010
  page: 136-151
  language: en
- type: book
  id: france1962rdeca
  author:
  - family: France
    given: Anatole
  title: "Rdeča lilija"
  translator:
  - family: Dobida
    given: Karel
  publisher-place: Ljubljana
  publisher: Državna založba Slovenije
  issued: 1962
  language: sl
- type: article-journal
  id: morel2019theorigins
  author:
  - family: Morel
    given: Philippe
  title: "The origins of discretism: thinking unthinkable architecture"
  title-short: "The origins of discretism"
  container-title: "Architectural design"
  volume-title: "Discrete: reappraising the digital in architecture"
  volume: 89
  issue: 2
  issued: 2019
  page: 15-21
  language: en
- type: article-magazine
  id: abraham2024lavender
  author:
  - family: Abraham
    given: Yuval
  title: "'Lavender': the AI machine directing Israel's bombing spree in Gaza"
  container-title: "+972"
  issued: 2024-04-03
  url: https://www.972mag.com/lavender-ai-israeli-army-gaza/
  language: en
- type: chapter
  id: benjamin2016nadrealizem
  author:
  - family: Benjamin
    given: Walter
  title: "Nadrealizem: zadnji posnetek evropske inteligence"
  translator:
  - family: Tomšič
    given: Samo
  container-title: "Usoda in značaj"
  publisher-place: Ljubljana
  publisher: Beletrina
  issued: 2016
  page: 39-51
  language: sl
- type: chapter
  id: benjamin2016avtor
  author:
  - family: Benjamin
    given: Walter
  title: "Avtor kot proizvajalec"
  translator:
  - family: Gorenšek
    given: Peter
  container-title: "Usoda in značaj"
  publisher-place: Ljubljana
  publisher: Beletrina
  issued: 2016
  page: 81-96
  language: sl
- type: article-magazine
  id: tronti2022ondestituent
  author:
  - family: Tronti
    given: Mario
  title: "On destituent power"
  container-title: "Ill will"
  issued: 2024-04-22
  accessed: 2024-05-08
  URL: https://illwill.com/on-destituent-power
  language: en
# vim: spelllang=sl
...
