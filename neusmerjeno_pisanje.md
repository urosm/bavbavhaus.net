---
title: "Neusmerjeno pisanje"
description: |
  Metazapis o <https://bavbavhaus.net> kot spletišču in praksi.
...

> Forma te razprave bi bila po eni strani bolj strogo znanstvena, po drugi pa v marsikaterih izpeljavah manj natančna, če njen prvotni namen ne bi bil ta, da je doktorska disertacija.
> K temu, da jo vseeno v tej obliki dajem v tisk, so me spodbudili znani razlogi.
> Razen tega mislim, da sem le rešil neki do zdaj nerešeni problem zgodovine grške filozofije.
[@marx1989spisi, 19]

## Ena izhodišča: pisava/koda

(@)
    > Razvoj novih, računalniških medijev od druge polovice 20. stoletja naprej je hkrati razvoj novih načinov družbene uporabe medijev.
    > Zmožnost koncentracije na eno samo vsebino, globinsko razumevanje in razmišljanje o določeni temi, značilno za dobo knjige, nadomeščata razpršena, fragmentirana pozornost na več vsebin hkrati in zmožnost večopravilnosti.
    [@krasovec2021tujost, 136]

(@)
    > [Internetne vsebine] so v provizoričnem, eksperimentalnem, spremenljivem stanju (denimo zapisi na blogih, debatne intervencije na forumih, odlomki še neizdelanih teorij), z njimi se bolj igramo, kot pa izdelujemo resne in trdne miselne sheme.
    [@krasovec2021tujost, 138]

    Iteracija.
    Glej tudi @workflow.

(@nelinea)
    > [D]ružbena oblika mišljenja, ki se danes uveljavlja s prevlado tehnopodob, je v nasprotju z družbeno obliko mišljenja, ki se formira s pisavo, bolj sinhorna kot pa linearna.
    > Za "dešifriranje" novomedijskih sporočil je pomembna zmožnost hkratnega sledenja večjemu številu sporočil in povezovanja med njimi, medtem ko je bila najpomembnejša kognitivna funkcija zgodovinske zavesti to, kar filozofija obravnava kot izpeljevanje, kot zmožnost produkcije linearnega vzročno-posledičnega sosledja vzrokov in učinkov ali premis in sklepov ali razlogov in dejanj v pisanju, a tudi njihove reprodukcije v branju na način poglobljenega razumevanja.
    [@krasovec2021tujost, 146]

(@)
    > Nadbesedilo pravzaprav ni nič pretresljivo novega, saj le posplošuje in paradigmatizira našo predračunalniško izkušnjo z nelinearnimi, nesekvenčnimi publikacijami, kakršne so enciklopedije, slovarji, biblija, kuharske in strokovne knjige, opremljene s stvarnim in avtorskim kazalom, opombami, seznamom literature ipd.
    > Takih publikacij ne beremo zbrano in zvezno od začetka do konca, ampak skačemo od teme do teme, od gesla do gesla, od strani do strani glede na trenutni interes.
    > Elektronsko nadbesedilo organizira informacije v nesekvenčni, prostorski obliki in tako olajšuje nezvezno strokovno branje, utegne pa biti koristno tudi leposlovnemu branju, saj rešuje problem, ki je mučil že romanopisce 19. stoletja, ko so želeli predstaviti simultano dogajanje in jih je pri tem omejevala linearna narava knjige – pomislimo samo na Jurčičeve tozadevne tožbe v Desetem bratu.
    > Znanstveno branje je pravzaprav zelo redko linearno.
    > K nelinearnosti prispevajo opombe in literatura, na katero se sklicujemo.
    > Empirične študije ugotavljajo, da je plačilo za svobodo, ki jo bralcu ponuja nadbesedilo, izguba poglobljenega in razmišljajočega branja.
    > Nadbesedilo deluje na bralca kot skušnjava in tako močno pritegne njegovo pozornost na svoje vabljive povezave, da sporočilo samo stopi v ozadje.

    Kontrapunkt @nelinea.

## Druga izhodišča: "sanjski datotečni format"

Glavno izhodišče je prispevek T. H. Nelsona "Complex information processing" iz
leta 1965. [@nelson1965complex] Kljub tehničnosti Nelsonove spekulacije in
predloga ter iz tega skoraj nujno izhajajoči tehnični zastarelosti nekaterih
izpostavljenih problemov, prispevek ostaja aktualen zaradi svojega enostavnega
vprašanja: *kakšne so (nove) možnosti uporabe osebnega računalnika*.

Natančneje se sprašuje o vrsti datotečnih struktur, ki so primerne za osebno
(kreativno) rabo, onkraj ustaljenih vrst za poslovno in znanstveno obdelavo
podatkov. Omogočati bi morale, pravi, "zapletene in občutljive razporeditve,
popolno spremenljivost, neopredeljene alternative in temeljito interno
dokumentacijo". Zaradi številnih možnih primerov uporabe mora predlagana
datotečna struktura biti preprost sistem gradnikov, ki je usmerjen k uporabniku
in namenjena splošni (nedoločeni) uporabi. [@nelson1965complex, 84] Tako
opredeli primer rabe, ki bi ga nov datotečni sistem naslovil:

::: {lang=en}

> [This work's] purpose was to create techniques for handling personal file
> systems and manuscripts in progress. These two purposes are closely related
> and not sharply distinct. [...] Indeed, often personal files shade into
> manuscripts, and the assembly of textual notes *becomes* the writing of text
> without a sharp break.
>
> I knew from my own experiment what can be done for these purposes with card
> file, notebook, index tabs, edge-punching, file folders, scissors and paste,
> graphic boards, index-strip frames, Xerox machine and the roll-top desk. My
> intent was not merely to computerize these tasks but to think out (and
> eventually program) the *dream* file: the file system that would have every
> feature a novelist or absent-minded professor could want, holding everything
> he wanted in just the complicated way he wanted it held, and handling notes
> and manuscripts in as subtle and complex ways as he wanted them handled.
> [@nelson1965complex, 85]

:::

## Tehnologija pisanja

(@workflow)
    Delovni tok:

    ```
    $ vi neusmerjeno_pisanje.md
    $ make
    $ git commit neusmerjeno_pisanje.md
    $ git push
    ```

    "Recepti" so zapisani v [`makefile`](makefile).


(@) *Sploščitev ELF*. Nelsonov predlog ima tri sestavine: *zapise*, *sezname* in
   *povezave*. Zapis je enota informacije, poljubnega obsega in oblike. Seznam
   je urejen niz zapisov. Povezava pa je povezava med dvema zapisoma iz
   različnih seznamov. V našem primeru gre za sploščitev zapisov in seznamov,
   kar obrne tudi funkcioniranje povezav znotraj sistema: zapisi se povezujejo z
   drugimi zapisi, ampak ti niso urejeni v sezname; ob prikazu posameznega
   zapisa se nanizajo tudi vsi povezani zapisi, kar pomeni, da povezave zapisa
   do drugih zapisov tvorijo seznam. To sploščitev lahko razumemo kot
   funkcionalno okrnitev *ELF*. Kar je v našem primeru zapis, bi pri *ELF*
   lahko veljalo za seznam zapisov (npr. odstavkov ...). Pri *ELF* bi
   posamezne odstavke lahko povezovali s posameznimi odstavki v drugih
   seznamih (odstavkov).

(@) *Kontekstualne povezave*.


(@) *Ikonoklazem*.

(@) *Tekst kot tekstura in slučajne povezave*. Ker vztrajamo na sočasnem
   prikazovanju in ...

## Stil, smisel in doktrina pisanja

(@bullshit)
[*Don't believe your own bullshit.*]{lang=en}
To se nanaša predvsem na nekatere samozadovoljne izpeljave iz fraze: "brez revolucionarne teorije ni revolucionarne prakse".
Če logike kapitala ni mogoče spontano spoznati, še ne pomeni, da je tvoje pisanje posebno poklicano ali uporabno "revolucionarni praksi".

(@)
Obstajajo tim. intimni cilji pisanja.
Teoretski in politični cilji, ki niso neposredno izraženi, ampak implicitno vodijo pisanje.
Njihova funkcija in smisel sta najbrž tudi intimna, glej točko @bullshit.

(@)
V okviru enega besedila opombe predstavljajo drugo strujo narativa, ki nagovarja drugo občinstvo.
V arhitekturnoteoretsko besedilo lahko na primer v opombe umestimo marksološke digresije.
(Včasih pa mora arhitekturnoteoretsko občinstvo brati tudi marksološke digresije.)

(@) Na podlagi tega, da so slabe, prepovedujemo naslednje besede in besedne zveze:
bojda;
bržkone;
v kolikor;
v luči;
ploden;
poroditi;
potemtakem;
prestolnica;
prej ko slej;
slej ko prej;
za svoj čas;
začenši.

(@) Na podlagi tega, da so dobre, priporočamo naslednje besede in besedne zveze:
odnosno.


---
lang: sl
references:
- type: book
  id: marx1989spisi
  author:
  - family: Marx
    given: Karl
  title: "Spisi o antični filozofiji"
  container-title: "Temeljna izdaja I"
  container-author:
  - family: Marx
    given: Karl
  - family: Engels
    given: Friedrich
  volume: 1
  publisher-place: Ljubljana
  publisher: Marksistični center CK ZKS
  issued: 1989
  language: sl
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
- type: paper-conference
  id: nelson1965complex
  author:
  - family: Nelson
    given: T. H.
  title: "Complex information processing: a file structure for the complex, the changing and the indeterminate"
  container-title: "Proceedings of the 1965 20th national conference (ACM '65)"
  publisher-place: New York
  publisher: Association for Computing Machinery
  issued: 1965
  page: 84-100
  language: en
- type: article-magazine
  id: bush1945aswe
  author:
  - family: Bush
    given: Vannevar
  title: "As we may think"
  container-title: "The Atlantic"
  issued: 1945-07-01
  page: 101-108
  language: en
...
