---
title: "Manfredo Tafuri, *Teorije in zgodovina arhitekture*"
...

<!-- # Introduzione -->

## Uvod

<!-- Che la critica di architettura si trovi oggi in una situazione a dir
poco "difficile" è un dato che crediamo non abbia bisogno di molte
dimostrazioni. -->

Dejstvo, da je arhitekturna kritika trenutno v milo rečeno "težkem"
položaju, po našem mnenju ni treba posebej dokazovati.

<!-- Criticare significa infatti cogliere la fragranza storica dei fenomeni,
sottoporli al vaglio di una rigorosa valutazione, rivelarne
mistificazioni, valori, contraddizioni e intime dialettiche, farne
esplodere l'intera carica di significati. Ma quando mistificazioni e
geniali eversioni, storicità ed antistoricità, intellettualismi
esasperati e disarmanti mitologie si intrecciano in modo così
indissolubile, nella produzione artistica, come accade nel periodo che
stiamo attualmente vivendo; il critico si trova costretto a instaurare
un rapporto estremamente problematico con la prassi operativa, specie
tenendo conto della tradizione culturale in cui egli si muove. -->

Kritika namreč pomeni, da je treba dojeti zgodovinski vonj pojavov, jih
podvreči strogi presoji, razkriti njihove mistifikacije, vrednote,
protislovja in intimno dialektiko ter razkrinkati ves njihov pomenski
naboj. Toda ko se v umetniški produkciji tako neločljivo prepletajo
mistifikacije in domiselne subverzije, zgodovinskost in
antizgodovinskost, pretirani intelektualizem in razorožujoče mitologije,
kot se to dogaja v obdobju, ki ga trenutno živimo, je kritik prisiljen
vzpostaviti izjemno problematičen odnos z operativno prakso, zlasti ob
upoštevanju kulturne tradicije, v kateri deluje.

<!-- Quando si combatte una rivoluzione culturale, infatti, esiste una
stretta convivenza fra critica ed operazione. -->

V boju proti kulturni revoluciji sta kritika in delovanje v tesnem
sožitju.

<!-- Tutte le armi del critico che abbracci la causa della rivoluzione si
appuntano contro il vecchio ordine, ne scavano fino in fondo le
contraddizioni e le ipocrisie, costruendo un bagaglio ideologico nuovo
che può anche sfociare in una creazione di miti: dato che per ogni
rivoluzione i miti sono le idee-forza necessarie ed indispensabili per
forzare la situazione. Ma quando la rivoluzione -- e non v'è dubbio che
le avanguardie artistiche del xx secolo abbiano combattuto per una
rivoluzione -- ha ormai raggiunto i suoi scopi, il sostegno che la
critica trovava prima nella sua compromissione totale con la causa
rivoluzionaria viene a mancare. -->

Vse orožje kritika, ki se zavzema za revolucijo, je uperjeno proti
staremu redu, koplje v njegova protislovja in hinavščine, ustvarja novo
ideološko prtljago, ki lahko privede celo do ustvarjanja mitov, saj so
miti za vsako revolucijo nujna in nepogrešljiva sila -- ideje, ki silijo
v položaj. Toda ko revolucija -- in ni dvoma, da so se umetniške
avantgarde 20\. stoletja borile za revolucijo -- doseže svoje cilje, se
podpora, ki jo je kritika prej našla v svoji popolni zavezanosti
revolucionarni stvari, izgubi.

<!-- Per non rinunciare al proprio specifico compito, là critica dovrà allora
iniziare a rivolgersi alla storia del movimento innovatore, scoprendo in
esso, questa volta, carenze, contraddizioni, compiti traditi,
fallimenti, e, principalmente, dimostrarne la complessità e la
frammentarietà. -->

Da se ne bi odrekla svoji posebni nalogi, se mora kritika začeti
obračati k zgodovini avantgardnih gibanj in v njej tokrat odkrivati
pomanjkljivosti, protislovja, izdane naloge, neuspehe, predvsem pa
dokazovati njegovo kompleksnost in razdrobljenost.

<!-- I miti generosi della prima fase eroica, perso il loro carattere di
idee-forza, divengono ora oggetto di contestazione. -->

Velikodušni miti prve junaške faze, ki so izgubili svojo idejno moč,
postanejo predmet spora.

<!-- Nel nostro caso, alla presenza di un così violento esplodere di
involuzioni, di fermenti di istanze nuove, che rendono multiforme e
caotico il panorama della cultura architettonica internazionale alle
soglie degli anni settanta, il problema più inquietante che si presenti
ad una critica che non voglia affondare la testa nella sabbia, per
vivere in un'evasiva pace i miti già consunti, è quello della
storicizzazione delle contraddizioni attuali. Il che significa un
coraggioso e spietato vaglio delle basi stesse del movimento moderno:
anzi, una spietata indagine sulla legittimità di parlare ancora di
movimento moderno come monolitico *corpus* di idee, di poetiche, di
tradizioni linguistiche. La prima operazione da fare, quindi, è quella
di non volgere sdegnosamente il capo al panorama dei faticosi, incerti,
e se si vuole anche rinunciatari tentativi che gli architetti vanno
accumulando. Parlare di evasioni, rinunce, ritirate e dilapidazioni di
un patrimonio ben altrimenti utilizzabile, può essere doveroso. Ma non
basta, Il critico che si fermasse a queste constatazioni talmente
evidenti da risultare inutili o quasi, avrebbe già tradito il suo
compito, che è quello, prima di ogni altra: cosa, di spiegare, di
diagnosticare con esattezza, di fuggire il moralismo per scrutare nel
contesto dei fatti negativi quali errori iniziali si vadano ora
scontando, e quali valori nuovi si annidino nella difficile e sconnessa
congiuntura che viviamo giorno per giorno. Il pericolo maggiore diviene,
in tal caso, quello di rimanere impigliati nell'ambiguità da cui si
tenta di estrarre una struttura logica, un senso da anteporre ad ogni
operazione futura che si proponga lo scopo di raggiungere una diversa e
non nostalgica costruttività. -->

V našem primeru je ob tako silovitem izbruhu involucij, fermentacij
novih primerov, ki so na pragu sedemdesetih let prejšnjega stoletja
naredili panoramo mednarodne arhitekturne kulture večplastno in
kaotično, najbolj zaskrbljujoč problem, s katerim se sooča kritika, ki
noče zakopati glave v pesek in živeti v izogibajočem miru z že
obrabljenimi miti, zgodovinjenje trenutnih protislovij. To pomeni
pogumno in neusmiljeno preverjanje samih temeljev modernega gibanja:
pravzaprav neusmiljeno preverjanje upravičenosti tega, da o modernem
gibanju še vedno govorimo kot o monolitnem *korpusu* idej, poetik in
jezikovnih tradicij. Zato najprej ne smemo prezirljivo pogledati na
panoramo utrujajočih, negotovih in, če hočete, odrekanj, ki jih kopičijo
arhitekti. Govoriti o izmikanju, odrekanju, umikanju in zapravljanju
dediščine, ki bi jo sicer lahko uporabili, je dolžnost. Če bi se kritik
ustavil pri teh ugotovitvah, ki so tako očitne, da so neuporabne ali
skoraj neuporabne, bi se že izneveril svoji nalogi, ki je predvsem
pojasniti, natančno diagnosticirati, se izogniti moraliziranju, da bi v
okviru negativnih dejstev natančno preveril, katere prvotne napake se
zdaj odpravljajo in katere nove vrednote se skrivajo v težkem in
razdrobljenem položaju, v katerem živimo iz dneva v dan. V tem primeru
je največja nevarnost, da se ujamemo v dvoumnost, iz katere skušamo
izluščiti logično strukturo, pomen, ki ga je treba postaviti pred vsako
prihodnjo operacijo, katere cilj je doseči drugačno, ne nostalgično
konstruktivnost.

<!-- Mai come oggi, forse, è stata necessaria al critico tanta
*disponibilità* ad accogliere, senza il velo di falsanti pregiudizi, le
proposte che si avanzano nella più assoluta incorenza. È mai come oggi,
ancora, sono stati necessari un atteggiamento rigoristico, un profondo
senso e una profonda conoscenza della storia, un'attenzione così vigile
per dirimere, nel vasto contesto di movimenti, ricerche, o singoli
progetti, le influenze dettate dalla moda, e persino dallo snobismo
culturale, dalle intuizioni innovatrici. -->

Morda še nikoli prej kritik ni potreboval takšne *pripravljenosti, da
bi* brez tančice lažnih predsodkov sprejel predloge, ki so predstavljeni
v največji nedoslednosti. In še nikoli prej ni bila potrebna tako stroga
drža, tako globok občutek in poznavanje zgodovine, tako budna pozornost,
da bi v širokem kontekstu gibanj, raziskav ali posameznih projektov
ločili vplive mode in celo kulturnega snobizma od inovativnih intuicij.

<!-- Se l'architetto può sempre trovare una propria coerenza immergendosi
nelle contraddizioni proprie al suo "mestiere" di progettista, il
critico che si compiacesse di una simile situazione di incertezza non
sarebbe che uno scettico, o un imperdonabile superficiale. Nella ricerca
delle possibilità insite nelle poetiche e nei codici dell'architettura
contemporanea fatta da chi opera in concreto, è possibile recuperare una
positività ed una costruttività culturali, anche se limitate, spesso, a
problemi marginali. Ma per il critico che sia cosciente della situazione
labile e *pericolosa* in cui versa l'architettura moderna, non sono
concessi illusioni o artificiali entusiasmi: così come non gli sono
concessi -- e questo è forse ancora più importante -- atteggiamenti
apocalittici o disfattisti. -->

Če lahko arhitekt vedno najde lastno doslednost tako, da se zatopi v
protislovja lastna njegovemu "poklicu" projektiranja, bi kritik, ki bi
užival v podobnem položaju negotovosti, lahko bil zgolj skeptik ali
nedopustno površen. Raziskovanje zmožnosti, ki so vpisane v poetike in
kode sodobne arhitekture, ki jo opravljajo tisti, ki delujejo konkretno,
je mogoče obnoviti kulturno pozitivnost in konstruktivnost, četudi
pogosto omejeno na obrobne probleme. Toda kritiku, ki se zaveda
negotovega in *nevarnega* položaja v katerem se je znašla moderna
arhitektura, niso dovoljene nobene iluzije ali umetno navdušenje: prav
tako -- in to je morda še pomembneje -- kot niso dovoljene defetistične
ali apokaliptične drže.

<!-- Il critico è colui che è costretto, per scelta personale, a mantenere
l'equilibrio su di un filo, mentre venti che mutano di continuo
direzione fanno di tutto per provocarne la caduta. L'immagine non è
affatto retorica: da quando il movimento moderno ha scoperto la propria
multiformità reagendo con sgomento a tale scoperta, l'alleanza iniziale
tra critica impegnata e architettura nuova, anzi l'identificazione della
critica con un'operazione che insiste sulla medesima area di premesse e
di problemi, si è necessariamente incrinata. Il confluire delle figure
dell'architetto e del critico nella medesima persona fisica -- fenomeno
che è quasi una norma per l'architettura, a differenza di altre tecniche
di comunicazione visiva -- ha coperto tale frattura, ma non
completamente: è più che frequente, ad esempio, uno sdoppiamento della
personalità fra l'architetto che scrive e che teorizza, e lo stesso
architetto che opera. -->

Kritik je tisti, ki mora po lastni izbiri ohranjati ravnotežje na nitki,
medtem ko se vetrovi, ki nenehno spreminjajo smer, na vse pretege
trudijo, da bi padel. Podoba nikakor ni retorična: odkar je moderno
gibanje odkrilo svojo večplastnost in se z grozo odzvalo na to odkritje,
se je prvotno zavezništvo med angažirano kritiko in novo arhitekturo
oziroma identifikacija kritike z delovanjem, ki vztraja na istem območju
predpostavk in problemov, nujno razpadlo. Združitev figur arhitekta in
kritika v isto fizično osebo -- pojav, ki je za arhitekturo v nasprotju z
drugimi tehnikami vizualne komunikacije skorajda normalni -- je ta prelom
prekrila, vendar ne povsem: več kot pogosta je na primer razcepitev
osebnosti med arhitektom, ki piše in teoretizira, ter arhitektom, ki
dela.

<!-- È per questo che il *critico puro* comincia ad essere guardato come
figura pericolosa: da qui il tentativo di etichettarlo con il marchio di
un movimento, di una tendenza, di una poetica. Poiché la critica che
vuol mantenere una distanza dalla prassi operativa non può che
sottoporre quest'ultima ad una costante demistificazione per superare le
sue contraddizioni o, almeno, per farle presenti con esattezza, ecco gli
architetti tentare una *cattura* di quella critica, tentare, in fondo,
una sua esorcizzazione. -->

*Zato so čistega kritika* začeli obravnavati kot nevarno figuro: zato so
ga poskušali označiti z znakom gibanja, smeri, poetike. Ker kritika, ki
želi ohraniti distanco do operativne prakse, ne more drugače, kot da
slednjo nenehno demistificira, da bi premagala njena protislovja ali jih
vsaj natančno predstavila, so tu arhitekti, ki poskušajo *zajeti* to
kritiko in jo na koncu poskušajo pregnati.

<!-- Il tentativo di sottrarsi a tale cattura potrà sembrare dettato dalla
paura solo agli stupidi o ai disonesti. Per la critica tornare ad
assumere su di sé il compito che le è proprio -- quello della diagnosi
storica oggettiva e spregiudicata e non quello del suggeritore o del
"correttore di bozze" -- richiede al contrario una buona dose di
coraggio, dato che, nello storicizzare la drammatica pregnanza del
momento odierno, essa rischia di avventurarsi in un terreno minato. -->

Poskus izogibanja takšnemu zajetju se lahko zdi, da je strah narekoval
le neumnim ali nepoštenim. Za kritike, ki se vrnejo k nalogi objektivne
in nepristranske zgodovinske diagnoze namesto sugestije ali
"popravljanja", je potreben velik odmerek poguma, saj z zgodovinjenjem
dramskega pomena sedanjega trenutka tvegajo, da se bodo podali na minsko
polje.

<!-- Perché è inutile nasconderselo, la minaccia che pende sul capo di chi
voglia "capire" demolendo radicalmente ogni mito contemporaneo è la
stessa che il Vasari oscuramente sentiva agli inizi della seconda metà
del Cinquecento: ogni giorno di più si è invitati a rispondere alla
tragica domanda sulla liceità storica della continuità con la tradizione
del movimento moderno. -->

Ker je nesmiselno skrivati, je grožnja, ki visi nad glavo tistih, ki
želijo "razumeti" z radikalnim rušenjem vsakega sodobnega mita, ista,
kot jo je na začetku druge polovice 16\. stoletja nejasno občutil
Vasari: vsak dan bolj smo vabljeni, da odgovorimo na tragično vprašanje
o zgodovinski legitimnosti kontinuitete s tradicijo modernega gibanja.

<!-- Il solo fatto di porsi come problema se l'architettura contemporanea si
trovi o meno ad una svolta radicale, ha valore di sintomo. Esso
significa che ci sentiamo, insieme, dentro e fuori di una tradizione
storica, immersi in essa e al di là di essa, ambiguamente coinvolti in
una rivoluzione figurativa che, tutta fondata sulla permanente
contestazione di ogni verità acquisita, vediamo con inquietudine
rivolgere le proprie armi anche contro se stessa. -->

Že samo postavljanje vprašanja, ali je sodobna arhitektura na radikalnem
prelomu ali ne, je simptom. To pomeni, da se hkrati počutimo znotraj in
zunaj zgodovinskega izročila, potopljeni vanj in zunaj njega, dvoumno
vpleteni v figurativno revolucijo, ki v celoti temelji na nenehnem
izpodbijanju vsake pridobljene resnice in z zaskrbljenostjo opažamo, da
obrača svoje orožje tudi proti sebi.

<!-- "Né l'arte, né la critica rivoluzionaria -- ha scritto Rosenberg
contestando il falso radicalismo di Sir Herbert Read -- possono uscire da
questa contraddizione in base a cui l'arte è tale quando si pone contro
l'arte, ma: poi tende ad affermarsi come la sola autentica e sincera. La
stessa contraddizione costringe la critica a considerare l'esplosività
come un principio estetico da proteggere dalla minaccia di
annichilimento che verrebbe da una „consapevole negazione“ dei principi.
In lotta con se stessa, l'arte e la critica rivoluzionarie non possono
evitare il ridicolo della situazione. Nella nostra epoca rivoluzionaria
la vita dell'arte dipende ancora dalle contraddizioni della rivoluzione,
e così l'arte e la critica debbono continuare ad accettarne
l'assurdità"[^1]. Ma, forse, accettare solamente il ridicolo della
situazione non è sufficiente.

[^1]: Harold Rosenberg, *The Tradition of the New*, Horizon Press 1959;
    trad. it.: *La tradizione del nuovo*, Feltrinelli, Milano 1964, p.
    65\. Nel medesimo saggio (*La rivoluzione e il concetto di
    bellezza*, pp. 57 sgg.), Rosenberg denuncia l'ambiguità delle
    tangenze fra rivoluzioni politiche e rivoluzioni artistiche
    riferendosi alle confusioni -- oggi peraltro notevolmente attenuate --
    create dalla cultura degli anni quaranta-cinquanta in America. Ma la
    sua diagnosi ci sembra contenere una verità ancora attuale quando
    egli afferma che da quegli equivoci "risulta una cattiva coscienza,
    una situazione di inganno e di autoinganno. Non è esagerato anzi
    affermare che la cattiva coscienza a proposito della rivoluzione è
    la malattia tipica dell'arte odierna" (p. 60). -->

"Niti umetnost niti revolucionarna kritika," je zapisal Rosenberg, ko je
oporekal lažnemu radikalizmu sira Herberta Reada, "se ne moreta izogniti
temu protislovju, po katerem je umetnost takšna, ko se postavi proti
umetnosti, a: takrat se skuša uveljaviti kot edina pristna in iskrena.
Isto protislovje sili kritike, da eksplozivnost obravnavajo kot estetsko
načelo, ki ga je treba zaščititi pred nevarnostjo izničenja, ki bi jo
prineslo "zavestno zanikanje" načel. Revolucionarna umetnost in kritika,
ki se bori sama s seboj, se ne more izogniti smešenju situacije. V naši
revolucionarni dobi je življenje umetnosti še vedno odvisno od
protislovij revolucije, zato morata umetnost in kritika še naprej
sprejemati njeno absurdnost"[^1]. Morda pa samo sprejemanje smešnosti
situacije ni dovolj.

[^1]: Harold Rosenberg, *The Tradition of the New,* Horizon Press 1959;
    italijanski prevod: *La tradizione del nuovo,* Feltrinelli, Milano
    1964, str. 65\. V istem eseju (*The Revolution and the Concept of
    Beauty*, str. 57 in nasl.) Rosenberg obsoja dvoumnost stičnih točk
    med politično in umetniško revolucijo, pri čemer se sklicuje na
    zmedo, ki jo je -- zdaj že precej omiljeno -- ustvarila kultura
    štiridesetih in petdesetih let v Ameriki. Vendar se zdi, da njegova
    diagnoza vsebuje resnico, ki je še vedno aktualna, ko pravi, da iz
    teh nesporazumov "izhaja slaba vest, stanje prevare in samoprevare.
    Brez pretiravanja lahko rečemo, da je slaba vest o revoluciji
    tipična bolezen današnje umetnosti" (str. 60).

<!-- Gli stessi compiti della critica, infatti, sono mutati. Se il problema è
quello di operare una sorta di selezione mutevole e senza appoggi
aprioristici, al fine di individuare la struttura dei problemi che
vengono confusamente affrontati e lasciati allo stato nascente dalle
giovani generazioni e dai pochi maestri della "terza generazione" del
movimento moderno culturalmente vivi, i termini di confronto adatti a
fondare un'analisi storica non possono essere più, neanche essi, dati
una volta per tutte. Anche la critica è costretta, come l'architettura,
a rivoluzionare continuamente se stessa alla ricerca di parametri di
volta in volta adeguati. -->

Pravzaprav so se spremenile same naloge kritike. Če gre za to, da se
izvede nekakšna spremenljiva selekcija brez aprioristične podpore, da bi
ugotovili strukturo problemov, s katerimi se zmedeno soočajo in jih
puščajo v nastajanju mlajše generacije in tisti redki mojstri "tretje
generacije" modernega gibanja, ki so kulturno živi, ni več mogoče enkrat
za vselej določiti primerjalnih pogojev, primernih za utemeljitev
zgodovinske analize. Tako kot arhitektura se je tudi kritika prisiljena
nenehno revolucionirati v iskanju parametrov, ki so vsakič znova
primerni.

<!-- Né è più possibile rifugiarsi in quella che, per tradizione, è stata la
valvola di sicurezza della critica: il giudizio assolutorio o di
condanna sull'opera in sé. -->

Prav tako se ni več mogoče zateči k temu, kar je tradicionalno
predstavljalo varnostni ventil kritike: k oprostilni ali obsojajoči
sodbi o samem delu.

<!-- È fin troppo chiara l'inutilità di un simile atteggiamento dogmatico da
parte della critica. Con il che, non vorremmo equivocasse: non
intendiamo affatto dire che il giudizio debba essere eliminato in una
sorta di limbo relativistico ove *tout se tient*. Ciò su cui vogliamo
piuttosto porre l'accento è la difficoltà in cui si trova immerso chi è
costretto ad entrare quotidianamente in contatto non con opere bene o
male finite, bensì con una ridda di irrisolte intenzioni, di spunti
nuovi immessi nei più sconsolanti e tradizionali contesti, di reticenti
intuizioni, di progetti volutamente irrealizzabili. -->

Nesmiselnost takšne dogmatične drže kritikov je preveč očitna. S tem ne
želimo biti napačno razumljeni: nikakor ne mislimo, da bi bilo treba
sodbo odpraviti v nekakšnem relativističnem limbu, kjer *tout se tient*.
Bolj želimo poudariti težave, v katere se znajdejo tisti, ki so
prisiljeni vsakodnevno prihajati v stik ne z dobro ali slabo dokončanimi
deli, temveč z mešanico nerešenih namenov, novih idej, vpeljanih v
najbolj razočarane in tradicionalne kontekste, zadržanih intuicij in
projektov, ki so namerno neizvedljivi.

<!-- È possibile valutare in modo tradizionale tale sconcertante panorama, o
non diviene piuttosto, a questo punto, compito specifico della critica
comprenderne i significati intrinseci e far ordine in esso in una
provvisoria sospensione del giudizio? -->

Ali je mogoče to motečo panoramo oceniti na tradicionalen način, ali pa
na tej točki ne postane posebna naloga kritike, da razume njene notranje
pomene in jo uredi v začasnem suspenzu sodbe?

<!-- Ad un esame spregiudicato dell'attuale situazione della cultura
architettonica, le incongruenze e l'inafferrabilità stessa di tanta
produzione odierna rendono evidente che ci troviamo di fronte ad un
tacito e forse inconsapevole sforzo teso a decretare, da un lato la
morte stessa dell'architettura, a rinvenire confusamente, dall'altro,
una dimensione nuova ed imprevista dell'operare architettonico. -->

Nepristranski pregled trenutnega stanja arhitekturne kulture pokaže, da
je zaradi neskladnosti in izmuzljivosti velikega dela današnje
produkcije jasno, da se soočamo s tihim in morda nezavednim
prizadevanjem, da bi na eni strani razglasili smrt arhitekture, na drugi
strani pa zmedeno našli novo in nepredvideno razsežnost arhitekturnega
dela.

<!-- È per questo che abbiamo parlato dell'opportunità di una disponibilità
nuova da parte del critico: di una disponibilità rigorosa e controllata,
certo, ma pur sempre commisurata ai compiti imposti dal momento
storico[^2].

[^2]: Tale disponibilità è resa ancor più necessaria dal continuo
    variare dell'esperienza artistica in una permutazione di aree
    semantiche spesso sovrapposte fra loro. Per questo la crisi di ogni
    estetica definitoria, sancita dalla critica semantica storicizzata
    dal Plebe e ricondotta dal Garroni ad una "nozione plurale di arte",
    richiede ovviamente un modo di fare critica estremamente complesso,
    ed un'estetica -- caso mai -- come "indicativa di un senso aperto del
    campo artistico, esso stesso aperto e imprevedibile", come ha
    riconosciuto anche l'Anceschi. (Cfr. Armando Plebe, *Processo
    all'estetica*, La Nuova Italia, Firenze 1952; Emilio Garroni, *La
    crisi semantica delle arti*, Officina ed., Roma 1964, pp. 109 sgg.;
    Luciano Anceschi, *Progetto di una sistematica dell'arte*, Mursia,
    Milano 1962 e: *Critica, Filosofia, Fenomenologia* in: "Il Verri",
    1967, n. 18, pp. 13--24). All'interno di questo quadro la critica,
    persa la speranza di trovare sostegni precostituiti in un pensiero
    estetico rigorosamente filosofico, acquista responsabilità nuove e
    nuova autonomia. -->

Zato smo govorili o priložnosti za novo razpoložljivost kritika: gotovo
strogo in nadzorovano, vendar še vedno sorazmerno z nalogami, ki jih
nalaga zgodovinski trenutek[^2].

[^2]: Ta razpoložljivost je še toliko bolj potrebna zaradi nenehnega
    spreminjanja umetniških izkušenj na različnih pomenskih področjih,
    ki se pogosto prekrivajo. Zato kriza vsake definicijske estetike, ki
    jo sankcionira Plebejova historizirana semantična kritika in ki jo
    Garroni vrača k "pluralnemu pojmu umetnosti", očitno zahteva izjemno
    kompleksen način kritike in estetiko -- če sploh -- kot "pokazatelj
    odprtega občutka umetniškega polja, ki je samo odprto in
    nepredvidljivo", kot ugotavlja tudi Anceschi. (Prim. Armando Plebe,
    *Processo all'estetica*, La Nuova Italia, Firenze 1952; Emilio
    Garroni, *La crisi semantica delle arti,* Officina ed., Roma 1964,
    str. 109 in naslednje; Luciano Anceschi, *Progetto di una
    sistematica dell'arte,* Mursia, Milano 1962 in: *Kritika,
    filozofija, fenomenologija* in: "Il Verri", 1967, št. 18, str.
    13--24). V tem okviru je kritika, ki je izgubila upanje, da bo našla
    vnaprej določeno oporo v strogo filozofski estetski misli, dobila
    nove odgovornosti in avtonomijo.

<!-- A questo punto emerge inderogabile il problema di una rigorosa
fondazione degli strumenti critici. -->

Na tej točki se pojavi problem strogega temelja kritičnih instrumentov.

<!-- In fondo, la critica dell'architettura moderna è stata obbligata a
procedere, quasi fino ad oggi, su binari fondati su di una spregiudicata
empiria: era questa l'unica via percorribile, forse, dato che sin troppo
spesso l'arte del nostro secolo ha scavalcato le convenzioni
ideologiche, i fondamenti speculativi, le stesse estetiche di cui il
critico si trovava a disposizione. Tanto che una autentica critica
dell'arte moderna si è avuta, specie fra il '20 e il '40, solo da parte
di chi ha avuto il coraggio di non dedurre i propri metodi di analisi da
sistemi filosofici precostituiti, ma dal diretto ed empirico contatto
con i problemi del tutto nuovi della comunicazione visiva impostati
dalle avanguardie. -->

Navsezadnje je bila kritika moderne arhitekture skoraj do danes
prisiljena nadaljevati po poteh, ki temeljijo na brezobzirnem empirizmu:
to je bila morda edina možna pot, saj je umetnost našega stoletja
prepogosto zaobšla ideološke konvencije, spekulativne temelje in samo
estetiko, ki jo je imel kritik na voljo. Tako zelo, da so avtentično
kritiko moderne umetnosti, zlasti med dvajsetimi in štiridesetimi leti
20\. stoletja, lahko izvajali le tisti, ki so imeli pogum, da svojih
metod analize niso izpeljali iz že uveljavljenih filozofskih sistemov,
temveč iz neposrednega in empiričnega stika s povsem novimi problemi
vizualne komunikacije, ki jih je postavila avantgarda.

<!-- Non bisogna giudicare affrettatamente tale empirismo critico. Attraverso
di esso Pevsner, Behne, Benjamin, Giedion, Persico, Giolli, Argan,
Dorner o Shand hanno, nell'anteguerra, scavalcato i limiti degli
strumenti di pensiero del loro tempo, assumendo, dalle poetiche
dell'arte moderna, una capacità di leggere i fenomeni nuovi nel loro
carattere di aperto processo, di perpetua mutabilità, di riscatto del
casuale, del non razionale, del gestuale, al limite. -->

Tega kritičnega empirizma ne smemo presojati prehitro. Pevsner, Behne,
Benjamin, Giedion, Persico, Giolli, Argan, Dorner ali Shand so v
predvojnem obdobju z njo presegli omejitve miselnih orodij svojega časa
in od poetike moderne umetnosti prevzeli sposobnost branja novih pojavov
v njihovi naravi odprtega procesa, nenehne spremenljivosti, odrešitve
naključnega, nerazumskega, gestualnega, na meji.

<!-- Su questa tradizione empirica la critica del dopoguerra ha potuto
innestare, volta per volta, i contributi del realismo lukácsiano,
dell'esistenzialismo e del relativismo, della fenomenologia husserliana,
di alcuni recuperi bergsoniani, di un riletto puro-visibilismo di marca
fiedleriana, del crocianesimo, e, più recentemente, della
*Gestaltpsychologie*, della linguistica strutturale, della semantica,
della semiologia, della teoria dell'informazione e dello strutturalismo
antropologico. -->

Na to empirično tradicijo je povojna kritika lahko cepila prispevke
Lukacijevega realizma, eksistencializma in relativizma, Husserlove
fenomenologije, nekaterih Bergsonovih obnovitev, ponovnega branja
čistega vizibilizma v slogu Fiedlerja, Croceanizma in v zadnjem času
*Gestaltpsihologije*, strukturnega jezikoslovja, semantike, semiologije,
teorije informacij in antropološkega strukturalizma.

<!-- Eclettismo critico dunque? Forse; ma non del tutto, almeno, negativo. -->

Torej kritični eklekticizem? Morda, vendar vsaj ne povsem negativno.

<!-- In parte quell'eclettismo ha dimostrato che i problemi posti
dall'architettura moderna sono più avanzati rispetto a quelli di volta
in volta affrontati dai metodi di lettura; in parte ha dimostrato
l'indipendenza operativa della critica, tanto che gli stessi filosofi
hanno ammesso di aver ricevuto da essa stimoli e problematiche
inedite[^3]. -->

<!-- [^3]: Interessanti, al proposito, i risultati della tavola rotonda
  tenuta nella sede romana dall'INARCH nel marzo 1961 (interventi di G.
  Calogero, R. Assunto, R. Bonelli, A. Plebe), parzialmente
  riprodotta in: "L'Archiiettura cronache e storia", 1961, VII, n. 71,
  pp. 336--37. -->

Ta eklekticizem je deloma pokazal, da so problemi sodobne arhitekture
naprednejši od tistih, ki jih obravnavajo metode razlage, deloma pa je
pokazal operativno neodvisnost kritike, tako da so tudi sami filozofi
priznali, da od nje prejemajo nove spodbude in probleme[^3].

[^3]: V zvezi s tem so rezultati okrogle mize, ki je potekala na rimskem
  sedežu INARCH marca 1961 (govorili so G. Calogero, R. Assunto, R.
  Bonelli, A. Plebe), deloma objavljeni v: "L'Archiiettura cronache e
  storia", 1961, VII, št. 71, str. 336--37.

<!-- Ma una volta intravista una crisi dell'operatività della critica (dato
che non le è più possibile prendere una ed una sola posizione a favore
di una ed una sola corrente dell'architettura moderna), una volta
scoperta l'immanente dialettica dell'arte attuale, quell'empirismo e
quell'eclettismo appaiono come pericoli, come strumenti che non
assicurano più un rigore interno alla critica. -->

Toda ko se pokaže kriza v delovanju kritike (saj ni več mogoče, da bi
zavzemala eno samo stališče v korist enega samega toka moderne
arhitekture), ko se odkrije imanentna dialektika aktualne umetnosti, se
empirizem in eklekticizem pokažeta kot nevarnosti, kot orodji, ki ne
zagotavljata več notranje strogosti kritike.

<!-- È indicativo l'interesse che la critica di architettura ha manifestato
negli ultimi tempi per le ricerche che hanno introdotto nelle scienze
dell'uomo e nell'analisi delle comunicazioni linguistiche e visive,
metodi analoghi a quelli delle scienze empiriche e sperimentali. -->

To kaže na zanimanje, ki ga je v zadnjem času izkazala arhitekturna
kritika za raziskave, ki so v humanistiko ter analizo jezikovnega in
vizualnega komuniciranja vnesle metode, podobne tistim iz empiričnih in
eksperimentalnih znanosti.

<!-- Strutturalismo e semiolog1a sono oggi all'ordine del giorno, anche negli
studi di architettura. E possiamo indicare subito gli apporti positivi
da essi dati all'analisi della progettazione: anzitutto soddisfano
l'esigenza di una sua fondazione scientifica, e ben sappiamo come
l'oggettività sia un'esigenza tutta propria di momenti di profonda
inquietudine ed incertezza. Ma in secondo luogo esse propongono un
impegno sistematico di comprensione dei fenomeni che fa ragione delle
poetiche dell'angoscia e della crisi, divenute, per consunzione, evasive
e inoperanti. -->

Strukturalizem in semiologija sta danes na dnevnem redu celo v
arhitekturnih praksah. In takoj lahko izpostavimo njihove pozitivne
prispevke k analizi oblikovanja: najprej zadovoljijo potrebo po
znanstveni podlagi, vsi pa vemo, da je objektivnost potreba, ki je
povsem primerna za trenutke globokega vznemirjenja in negotovosti.
Drugič pa predlagajo sistematično zavezanost razumevanju pojavov, ki
odpravlja poetiko tesnobe in krize, ki sta zaradi potrošnje postali
izmikajoči se in neučinkoviti.

<!-- La critica che non voglia abbandonarsi a inutili geremiadi deve
fondamentalmente diagnosticare. Come metodi di diagnosi -- una volta
riconosciute come tali, e non come dottrine alla moda o unitario
*corpus* dogmatico -- strutturalismo e semiologia hanno già dimostrato la
loro efficienza. -->

Kritika, ki se ne želi prepustiti nekoristnim jeremijam, mora temeljito
diagnosticirati. Strukturalizem in semiologija sta kot diagnostični
metodi -- ko sta bili prepoznani kot taki in ne kot modni doktrini ali
enotni dogmatični *korpus -* že dokazali svojo učinkovitost.

<!-- Ma hanno, insieme, mostrato i loro pericoli, ed anche l'ideologismo che
si cela dietro la loro apparente sospensione ideologica. Ancora una
volta la critica è quindi chiamata a dare il proprio contributo, a
scegliere a riportare nell'ambito di un fondato storicismo i materiali a
lei offerti. -->

Hkrati pa so pokazale svoje nevarnosti in tudi ideologizem, ki se skriva
za njihovo navidezno ideološko suspenzijo. Zato je kritika ponovno
pozvana, da sama prispeva, da se odloči, da bo gradivo, ki ji je
ponujeno, vrnila na področje dobro utemeljenega historizma.

<!-- Se è vero però, come ha scritto François Furet[^4], che il fascino
esercitato dallo strutturalismo di Lévi-Strauss sull'intellighenzia di
sinistra francese è dovuto al fatto che "semplicemente, e tratto per
tratto la descrizione strutturale di un uomo divenuto oggetto ha preso
il posto dell'avvento, nella storia, dell'uomo-dio", è anche vero che
gran parte dell'arte moderna aveva anticipato tale ribaltamento da Dada
ed alcuni aspetti del De Stijl e del Costruttivismo sovietico, fino allo
stesso Le Corbusier, se letto opportunamente; e non si può certo dire
che l'accettazione della fine del mito dell'antropocentrismo umanistico
non sia sfociata, per quelle esperienze artistiche, in una nuova e più
autentica collocazione dell'uomo rispetto al mondo da lui strutturato.
-->

<!-- [^4]: François Furet, *Gli intellettuali francesi e lo strutturalismo*,
  in: "Tempo presente", 1967, n. 7, p. 14. -->

Če je res, kot je zapisal François Furet[^4], da je Lévi-Straussov
strukturalizem očaral francosko levičarsko inteligenco zato, ker je
"preprosto in postopoma strukturni opis človeka, ki je postal predmet,
nadomestil prihod človeka-boga v zgodovini", je prav tako res, da je
večina moderne umetnosti ta preobrat pričakovala, od dadaizma in
nekaterih vidikov De Stijla in sovjetskega konstruktivizma do Le
Corbusiera, če se pravilno bere; in zagotovo ni mogoče reči, da
sprejetje konca mita humanističnega antropocentrizma za te umetniške
izkušnje ni povzročilo novega in pristnejšega mesta človeka v odnosu do
sveta, ki ga je strukturiral.

[^4]: François Furet, *Gli intellettuali francesi e lo strutturalismo*,
  v: Tempo presente, 1967, št. 7, str. 14.

<!-- In qualche modo potremmo considerare Lévi-Strauss come il Parmenide di
questa filosofia (*malgré soi*) di neoeleatico sapore: la struttura e
l'ordine contro il disordine della storia, la permanenza dell'Essere
contro la fenomenologia del divenire, la stabilità dei meccanismi comuni
cui viene ridotto l'uomo, contro la sartriana ragione dialettica (ma già
si avvertono le avvisaglie dei paradossi demolitori dei neo-Zenoni): Per
ora ci interessa capire il significato di alcune notevoli tangenze fra
la *vogue* strutturalista, ed alcuni fenomeni che avvengono all'interno
della cultura artistica e architettonica, in particolare. Tangenze, per
giustificare le quali non è necessario ricorrere all'ipotesi di
influenze dirette, quasi sicuramente inesistenti. Anzi, lo stesso fatto
che tale diretto scambio non sia dimostrabile, diviene a questo punto
un'autonoma ragione di interesse, dimostrando l'esistenza di un
orizzonte comune creato da un comune atteggiamento nei confronti
dell'attuale condizione dell'uomo. -->

Na neki način bi lahko Lévi-Straussa imeli za Parmenida te filozofije
(*malgré soi)* neohelvetijskega okusa: struktura in red proti neredu
zgodovine, stalnost Biti proti fenomenologiji postajanja, stabilnost
skupnih mehanizmov, na katere je zreduciran človek, proti sartrovskemu
dialektičnemu razumu (vendar lahko že vidimo znake rušilnih paradoksov
neozenonov): Za zdaj nas zanima razumevanje pomena nekaterih izjemnih
stičišč med strukturalistično *modo* in nekaterimi pojavi, ki se
pojavljajo zlasti v umetniški in arhitekturni kulturi. Tangence, za
utemeljitev katerih se ni treba zatekati k hipotezi o neposrednih
vplivih, ki skoraj zagotovo ne obstajajo. Nasprotno, prav dejstvo, da ta
neposredna izmenjava ni dokazljiva, postane na tej točki avtonomen
razlog za zanimanje, ki kaže na obstoj skupnega obzorja, ustvarjenega s
skupnim odnosom do trenutnega stanja človeka.

<!-- Ha particolare rilevanza, in questa sede, notare che sia lo
strutturalismo etnologico nella sua originaria accezione, che
"l'archeologia delle scienze umane" di Foucault, che l'orgia
anti-umanistica della Pop Art, che le ricerche di una *nuova
oggettività* da parte di un Kahn e delle correnti da lui derivate,
nell'architettura, insistono tutte su di una medesima area ideale (e
avremmo voluto dire ideologica, se questo termine non avesse assunto,
per tali ricerche, il sapore di un polemico paradosso). Scoprire che
quell'area ideale è la proposizione di un sapere e di un fare
antistorici può spaventare o sconcertare. Ma saremo assai meno
sconcertati se cercheremo di andare più in là, di scavare più a fondo
nei fenomeni, di non lasciarci trasportare da remore ideologiche
inadeguate. -->

Pri tem je še posebej pomembno opozoriti, da tako etnološki
strukturalizem v prvotnem pomenu, Foucaultova "arheologija humanističnih
ved", antihumanistična orgija poparta in Kahnovo iskanje *nove
objektivnosti* v arhitekturi vztrajajo na istem idealnem področju (in
radi bi rekli ideološkem, če ta izraz ne bi dobil okusa polemičnega
paradoksa za takšne raziskave). Odkritje, da je to idealno območje
predlog protizgodovinskega znanja in načina delovanja, je lahko
strašljivo ali neprijetno. Vendar bomo veliko manj razočarani, če bomo
poskušali iti dlje, se poglobiti v pojave in se ne bomo pustili
odpeljati neustreznim ideološkim pomislekom.

<!-- L'arte moderna non si è presentata sin dal suo primo apparire, nei
movimenti di avanguardia europei, come una vera e propria sfida alla
storia? Non ha forse tentato di distruggere non solo la storia, ma
addirittura se stessa come oggetto storico? Dada e De Stijl non sono poi
così antitetici se riletti da questo particolarissimo punto di vista.
-->

Ali se ni moderna umetnost že od svojega prvega pojava v evropskih
avantgardnih gibanjih predstavljala kot pravi izziv zgodovini? Ali ni
poskušala uničiti ne le zgodovine, temveč celo sebe kot zgodovinski
objekt? Dada in De Stijl nista tako nasprotna, če ju ponovno beremo s
tega posebnega vidika.

<!-- Ma c'è di più. Fra il mito dell'Ordine di Louis Kahn, nel suo ermetico
affondare nei materiali offerti dalla storia per destoricizzare fino
all'eccesso la progettazione architettonica, e la mistica neoplastica,
tesa a distruggere opposizioni e drammi in una pacificazione messianica,
esiste assai meno distanza di quanto non lascerebbe supporre un diretto
confronto fra opere, poniamo, come i laboratori Salk e la casa Schröder.
-->

Vendar je še več. Med mitom reda Louisa Kahna, ki se hermetično pogreza
v materiale, ki jih ponuja zgodovina, da bi do skrajnosti destoriciziral
arhitekturno oblikovanje, in neoplastičnim misticizmom, katerega cilj je
uničiti nasprotja in drame v mesijanski pomiritvi, je veliko manjša
razdalja, kot bi lahko sklepali iz neposredne primerjave med deli, kot
sta delavnici Salk in Schröderjeva hiša.

<!-- L'antistoricismo tradizionale delle avanguardie trova quindi una sua
conferma nelle stesse esperienze che si pongono il compito di superarlo.
E vi è un perché. Se è vero che il "mito è contro la storia" come
dimostra Barthes, e che esso compie la sua mistificazione nella
dissimulazione dell'artificiale (e dell'artificialità ideologica) dietro
la maschera di una pretesa "naturalità"[^5], un momento così
intensamente rivolto ad evadere tramite *nuovi miti* l'impegno della
comprensione del presente, come quello attuale, non può che far divenire
*moda* e *mito* persino le ricerche che, con rinnovato rigore e vigore,
tentano di impostare una lettura sistematica ed oggettiva del mondo,
delle cose, della storia, delle convenzioni umane. -->

<!-- [^5]: Ci riferiamo, ovviamente, al Roland Barthes delle *Mythologies*. -->

Tradicionalni antihistorizem avantgarde tako najde svojo potrditev prav
v izkušnjah, ki so si zadale nalogo, da ga presežejo. Za to obstaja
razlog. Če je res, da je "mit proti zgodovini", kot dokazuje Barthes, in
da svojo mistifikacijo doseže z disimulacijo umetnega (in ideološke
umetnosti) za masko domnevne "naravnosti"[^5], lahko trenutek, ki je
tako intenzivno usmerjen v to, da se z *novimi miti* izogne zavezanosti
razumevanju sedanjosti, kot je sedanja, povzroči le, da postanejo *moda*
in *mit* celo raziskave, ki s ponovno strogostjo in močjo skušajo
vzpostaviti sistematično in objektivno branje sveta, stvari, zgodovine,
človeških konvencij.

[^5]: Pri tem imamo seveda v mislih Rolanda Barthesa *mitologij*.

<!-- Nella cultura architettonica, in particolare (ma il discorso vale per
tutte le tecniche di comunicazione Visiva) la ricerca di un più avanzato
criticismo sembra compromettere le basi stesse dello spirito critico.
-->

Zlasti v arhitekturni kulturi (enako velja za vse tehnike vizualne
komunikacije) se zdi, da iskanje naprednejše kritike ogroža same temelje
kritičnega duha.

<!-- "Se una crisi è in atto -- scriveva Argan nel 1957 --, questa non è la
crisi del criticismo che, e non da ieri, ha messo in difficoltà le
grandi ideologie e concezioni e sistemi. Né questa deve essere, per se
stessa, causa di sgomento: il criticismo è fatto di crisi, non è
altrimenti pensabile che come critica e superamento dei suoi risultati
stessi. L'esito di questa crisi non è prevedibile: la critica non
ammette salvezze o condanne predestinate. Tuttavia la salvezza -- la
salvezza dello spirito di critica che l'arte e la cultura moderna hanno
ereditato dall'Illuminismo -- sarà probabile, se la critica sarà critica
di esperienze e non di ipotesi: se sarà infine, anche nell'arte, critica
storica."[^6] Ebbene, a distanza di dieci anni circa dalla data in cui
Argan pronunciava la sua lucida diagnosi possiamo dire che mai come oggi
l'arte e l'architettura sono state più dominate dall'ineffabilità
dell'ipotesi e tanto poco creatrici di esperienze. Né, va aggiunto, la
critica ha assunto mai, forse, un carattere così poco storicistico quale
quella che accompagna il proliferare di quell'architettura. -->

<!-- [^6]: Giulio Carlo Argan, *La crisi dei valori* (1957), ora in:
  *Salvezza e caduta dell'arte moderna*, Il Saggiatore, Milano 1964, p.
  38. -->

"Če obstaja kriza -- je leta 1957 zapisal Argan --, to ni kriza kritike,
ki ne od včeraj postavlja velike ideologije, koncepte in sisteme v
težave. Tudi to samo po sebi ne bi smelo biti razlog za preplah: kritika
je sestavljena iz kriz, ni je mogoče misliti drugače kot kritiko in
premagovanje lastnih rezultatov. Izida te krize ni mogoče predvideti:
kritika ne dopušča odrešitve ali vnaprejšnje obsodbe. Vendar pa bo
rešitev -- rešitev duha kritike, ki sta ga moderna umetnost in kultura
podedovali od razsvetljenstva -- verjetna, če bo kritika kritika izkustva
in ne hipotez: če bo končno, tudi v umetnosti, zgodovinska kritika."[^6]
No, približno deset let po Arganovi jasni diagnozi lahko rečemo, da v
umetnosti in arhitekturi še nikoli ni tako prevladovala neizrekljivost
hipoteze in tako malo ustvarjalca izkušnje. Prav tako je treba dodati,
da kritika nikoli ni imela tako nezgodovinskega značaja kot tista, ki
spremlja širjenje te arhitekture.

[^6]: Giulio Carlo Argan, *La crisi dei valori* (1957), zdaj v:
  *Odrešitev in padec moderne umetnosti,* Il Saggiatore, Milano 1964,
  str. 38.

<!-- Da quanto abbiamo potuto brevemente accennare sino ad ora, risulta che
il critico che voglia rendere storiche le esperienze dell'architettura
contemporanea e che intenda recuperare la loro storicità dal seno del
passato, si trova in buona parte controcorrente, in una posizione di
contestazione. -->

Iz tega, kar smo lahko na kratko omenili do zdaj, je razvidno, da se
kritik, ki želi iz izkušenj sodobne arhitekture narediti zgodovinske in
ki hoče njihovo zgodovinskost iztrgati iz primeža preteklosti, v veliki
meri znajde proti toku, v spornem položaju.

<!-- Fino a che punto tale contestazione coinvolge la storicità del movimento
moderno? E fino a che punto il distacco dal flusso della prassi è indice
di una profonda crisi della *critica operativa* o non si dischiude, per
la critica, un nuovo modo di operatività? Ed ancora, quali sono le
relazioni che la storia e la critica possono legittimamente porre con le
scienze nuove e le teorie delle comunicazioni, mantenendo le loro
specifiche prerogative, i loro specifici ruoli ed i loro specifici
metodi? -->

V kolikšni meri ta spor vključuje zgodovinskost modernega gibanja? In v
kolikšni meri ločitev od toka prakse kaže na globoko krizo *operativne
kritike* ali pa ne odpira novega načina delovanja kritike? In še enkrat,
kakšne odnose lahko zgodovina in kritika legitimno vzpostavita z novimi
znanostmi in komunikacijskimi teorijami, pri tem pa ohranita svoje
posebne pravice, svoje posebne vloge in svoje posebne metode?

<!-- Sono queste, fondamentalmente, le domande alla base di questo libro: nel
quale, forse, più che risposte definitive andranno cercate delle
impostazioni e delle soluzioni provvisorie; convinti, come siamo, che le
metodologie della storia vanno strettamente legate ai compiti che la
storia stessa, nella problematicità del suo svolgersi, propone a chi
rifiuti di farsi assorbire sia dalle mitologie quotidiane, che dalle
mitologie -- analoghe e contrarie -- della catarsi, che si vorrebbe far
scaturire da un silenzioso sprofondare nell'annullamento della "ragione
storica". -->

To so v bistvu vprašanja, na katerih temelji ta knjiga in v katerih je
morda namesto dokončnih odgovorov treba iskati začasne pristope in
rešitve; prepričani smo, da morajo biti metodologije zgodovine tesno
povezane z nalogami, ki jih zgodovina sama v problematičnosti svojega
razvoja ponuja tistim, ki se nočejo prepustiti vsakdanjim mitologijam in
mitologijam -- analognim in nasprotnim -- katarze, ki bi jih radi izvlekli
iz tihega pogrezanja v ničnost "zgodovinskega razuma".

<!-- # Gli strumenti della critica -->

## Instrumenti kritike

<!-- Se nel S. Carlino borrominiano non è possibile scindete nettamente la
struttura tipologica da quella simbolica, tecnologica o geometrica,
nella Casa dei Filippini il sistema funzionale emerge come valore
primario mentre quello simbolico e quello tecnologico divetrigono
episodici. Le relazioni che legano i vari sistemi fra loro mutano del
tutto, poi, nell’architettura contemporanea, dove il concettoa di "
codice " si può applicare solo avendo ben chiaro che esso si applica
quasi esclusivamente ai metodi di progettazione. -->

<!-- In altre parole ciò che è specifico dell’architettura è il modo di porre
in relazione fra loto le varie strutture che confluiscono in essa. È
questo, non casualmente, il problema delle teorie architettoniche, da
Vitruvio a tutta la trattatistica ottocentesca. -->

<!-- Interpretare l’architettura, nella sua struttura complessa e non nella
sua pura apparenza visiva, dà poi la chiave per ricondurre ad unità e ad
organicità quella struttura. Ma per fare ciò è chiaro che diviene
necessario chiarire in che modo sia possibile riinserire le utopie del
passato nella realtà presente; in che modo éd entro quali limiti ci sia
concesso recuperare gli otiginari significati storici di architetture
che sono entrate a far parte delle moderne strutture urbane e
territoriali, in che modo i miti attuali permettano la decifrazione dei
miti, dei valori e dei disvalori dei fenomeni architettonici nella
storia. -->

<!-- # I compiti della critica -->

## Naloge kritike

<!-- Se l'analisi strutturale riuscirà ad assolvere un simile compito, la
demistificazione operata dalla critica avrà un formidabile sostegno
teorico e tecnico. Se si chiuderà nell'esercitazione astorica non sarà
più neppur lecito parlare ancora di "critica". -->

<!-- Né può dirsi che i compiti ora enunciati riducano la critica ad azione
terroristica e nichilistica. Sartre affermava che il compito della
letteratura è quello di "chiamare alla libertà esibendo la propria'
libertà". Se oggi l'architettura non riesce a chiamare nessuno alla
libertà, se la sua stessa libertà è illusoria, se tutte le sue istanze
affondano in una palude di "immagini" al massimo "divertenti", non v'è
alcuna ragione per non assumere una posizione di decisa contestazione
nei confronti dell'architettura medesima, oltre che del contesto
generale che ne condiziona l'esistenza. -->

<!-- La critica, in tal modo, pone ostacoli avanzati all'architetto,
sfidandolo a superarli. Ma si badi bene: tali ostacoli non sono
"inventati" dal critico, bensì scaturiscono dalla storicizzazione
continua che egli fa del presente. -->

<!-- In quanto profondamente storicizzata, la critica può recuperare inoltre,
per l'architettura, la sua dimensione specifica: quella del futuro. Ed è
chiaro che riconducendo nell'alveo storico i fenomeni dell'architettura
contemporanea, la critica si trova a dover contestare l'antistoricismo
che, nelle pagine che precedono, abbiamo riconosciuto come il grande
problema irrisolto dell'arte moderna. -->

<!-- Non è affatto detto che opponendo una demitizzazione storica al mito
dell'antistoria la critica possa risolvere questa fondamentale aporia
del nostro secolo: anzi, è dimostrabile che un'astratta istanza di
storicizzazione della progettazione conduce direttamente a battute
d'arresto e a contraddizioni a dir poco dispersive. Ancora una volta
bisogna saper accettare una realtà e un dibattito in termini dialettici.
È del resto veramente illusorio e del tutto ingiustificabile pretendere
di "dominare" in qualche modo una situazione culturale così labile e
frantumata come quella attuale. Poiché le contraddizioni formano ormai
una loro realtà autonoma, la critica deve saper riconoscere il proprio
ruolo particolare e limitato, (ma indispensabile), al loro interno. -->

<!-- In tal senso possiamo far nostra Ja constatazione che Weiss mette in
bocca al "divino marchese" alla fine del suo *Marat- Sade*: "in questi
dialoghi nostra era intenzione | alcune antitesi porre in opposizione |
e queste per piu volte far scontrare | e i dubbi a questo modo
illuminare"; né importa che il critico -- come il Sade di Weiss — debba
poi dichiarare che nella presente situazione egli si vede obbligato a
"disperare di trovar la soluzione". -->

<!-- Nella storia non esistono "soluzioni". Ma si può sempre diagnosticare
che l'unica via possibile è l'esasperazione delle antitesi, lo scontro
frontale delle posizioni, l'accentuazione delle contraddizioni. E questo
non per un particolare sado-masochismo, ma nell'ipotesi di un mutamento
radicale che ci faccia ritenere superati, insieme all'angosciosa
situazione presente, anche i compiti provvisori che abbiamo tentato di
chiarire a noi stessi con questo volume. -->

## Instrumenti kritike

Končni cilj kritične metode je penetracija "razum zgodovine" (gl. tudi
naslov, *teorije in zgodovina*).
