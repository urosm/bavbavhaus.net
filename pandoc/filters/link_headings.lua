SCRIPT_NAME = "link_headings.lua"
os.setlocale("C")

local Link = pandoc.Link

return {
  { Header = function(h)
    h.content = Link(h.content, ("#%s"):format(h.attr.identifier))
    return h
  end }
}
