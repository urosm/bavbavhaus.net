SCRIPT_NAME = "strip_alts.lua"
os.setlocale("C")

local Inlines = pandoc.Inlines

return {
  { Div = function(d)
    if d.classes:includes("horizontal") then
      return d.content[1]
    end
  end }
}
