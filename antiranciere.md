---
title: "Antirancière"
description: |
  Kritike poznorancièrovskih političnofilozofskih konceptov.
...

## "Je čas emancipacije minil?" [@ranciere2012je]




: Tabela @todo

                  spontanizem        organizacija
---------------  ------------------ --------------
 **optimizem**    Rancière           Negri
 **pesimizem**    Comité invisible



Tronti, Tafuri, Benjamin ...

---
lang: sl
references:
- type: article-journal
  id: ranciere2012je
  author:
  - family: Rancière
    given: Jacques
  title: "Je čas emancipacije minil?"
  translator:
  - family: Benčin
    given: Rok
  container-title: "Filozofski vestnik"
  volume: XXXIII
  issue: 1
  issued: 2012
  page: 133-145
  language: sl
...
