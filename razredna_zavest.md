---
title: "Razredna zavest"
description: |
  Izpiski iz Lukácseve *Zgodovina in razredna zavest*, [@lukacs1985zgodovina]
  predvsem o formiranju zavesti (intelektualcev) in s pozornostjo do
  operaističnih branj.
...

<!--

<https://www.counterfire.org/article/marxisms-missing-link-a-readers-guide-to-lukacs-history-and-class-consciousness/>

<https://plato.stanford.edu/entries/lukacs/#HistClasCons>

-->

Lukácseva *Zgodovina in razredna zavest* je zbirka esejev, ki so nastali kot
posledica izkušnje revolucionarnega vala, ki se je začel z letom 1917.

Lukácsu gre za nadaljevanje marksovskega kritičnega projekta. Ne gre mu za
potrjevanje morebitnih rezultatov specifično Marxovih del, temveč za
nadaljevanje iz istih *metodoloških* izhodišč. Takšno razumevanje marksovskega
projekta Lukács tudi zoperstavlja njemu sodobnemu uradnemu marksizmu druge
internacionale. Iz teh razlogov zato poda zanimivo neortodoksen odgovor na to,
kaj je ortodoksni marksizem:

> Tudi če izhajamo iz predpostavke, čeprav se s tem ne strinjamo, da je novejše
> raziskovanje nedvomno dokazalo, da so vse posamezne Marxove trditve dejansko
> napačne, bi lahko vsak resen "ortodoksni" marksist vse te nove rezultate
> brezpogojno priznal in zavrgel vse posamezne Marxove teze, ne da bi niti za
> hip moral opustiti svojo marksistično ortodoksnost. Ortodoksni marksizem torej
> ne pomeni, da nekritično priznavamo rezultate Marxovega raziskovanja, da
> "verujemo" v to ali ono tezo, da razlagamo "sveto" knjigo. Ortodoksnost v
> marksizmu velja namreč izključno za *metodo*. [In nadaljuje: "Je znanstveno
prepričanje, da smo z dialektičnim marksizmom odkrili pravilno raziskovalno
metodo, da je mogoče to metodo izpopolnjevati, razvijati naprej in poglabljati
le v duhu njenih utemeljiteljev in da so vsi poskusi premaganja ali
"izboljšanja" te metode peljali in tudi morali pripeljati le v poplitvenje, v
trivialnost, v eklekticizem." @lukacs1985zgodovina, 33]

Preden lahko poda izhodišča metode, Lukács materialistično dialektiko opredeli
kot revolucionarno dialektiko. Teorija, tj. dialektična metoda, je v posebnem
odnosu do svojega predmeta, tj. revolucionarne spremembe, dejanje
ovedenja je bistveno in dejansko povezano z delovanjem samim. Pogoji za takšno
enotnost teorije in prakse so teoretsko-zgodovinski. 

Ovedenje je *odločilen korak*, ki ga mora 

Pogoji za tak odnos med teorijo in prakso so *zgodovinski*: pravilno spoznanje
družbe za nek razred je postalo neposredni pogoj za njegovo samouveljavitev v
boju
[@lukacs1985zgodovina, 34]

in njenega
odnosa do svojega predmeta prakso 
Teorija (marksizem) in praksa (revolucija) sta v
posebnem odnosu
Na podlagi posebnega odnosa med spoznavnim ciljem 

Vzpostavlja oziroma izhaja iz posebnega odnosa do
prakse, dejanj.
Teorija ima praktično bistvo -- sprememba sveta, neposreden
poseg v družbeno prevrati proces -- 

"po svpjem bistvu ni nič drugega kot miselni izraz
revolucionarnega procesa". [@lukacs1985zgodovina, 34]

Izhodišča te metode so:

- Marksovska metoda v nasprotju z buržoazno znanostjo poudarja *konkretno
  enotnost celote*. [@lukacs1985zgodovina, 37] Produkcijska razmerja sleherne
  družbe tvorijo celoto, totalnost. [@lukacs1985zgodovina, 39] Torej, družbo je
  možno in potrebno obravnavati kot celoto.
- Totalnost je protislovna. Različni momenti celote vzajemno učinkujejo. A pri
  tem ne gre za kavzalno vplivanje dveh predmetov, ki so drugače nespremenljivi.
  [41] Marksovski kritični projekt raztrga "večnostni ovoj kategorij". [42]
  Razvoj.
  Totaliteta je procesualna.
- "Čutnost", predmet, dejanskost je človekova čutna dejavnost.
  Gre za -- nazadnje -- človeška dejanja.

Za marksizem torej konec koncev ni n i k a k r š n e samostojne zna-
nosti, politične ekonomije, zgodovine itn., temveč ena sama — zgodovinsko-
dialektična — znanost o razvoju družbe kot totalnosti.[52]

Proletarsko stališče, gledišče: koincidiranje samospoznanja in spoznanja
totalitete. Proletariat pošteka svojo zgodovinsko nalogo, ko pošteka kaj je
proletariat.


it is not the primacy of economic motives in historical explanation that constitutes the decisive difference between Marxism and bourgeois thought, but the point of view of totality

Dopolnjuje luknje v marksovski literaturi (ne teoriji). Glede tega je zanimivo,
da pred "izidom" *Nemške ideologije* izpelje nekatere glavne njene teze.

Tekst nas zanima v kontekstu njegovega -- posrednega ali neposrednega -- vpliva
na operaistično misel

- gl. Corna
- gl. Galimberti 
- gl. Tronti

Pri Lukácsu stališče proletariata predstavlja tudi epistemološko stališče.
Njegova razdelava pa zadeva tudi odnos med intelektualci in to "izključno
proletarsko" sposobnostjo revolucionarno vedeti.


<!--

- Kritika videza neposredovanosti dejstev.
- Blagovna forma determinira obliko mišljenja. (tudi naravoslovje) (str. 36-39)

- Webrov pojem razčaranja: v sodobnem svetu imamo tehničen odnos do predmetov.
  Tehničen pogled na svet: vse stvari so lahko razložene, ni več skrivnosti, vse
  se da izkoristit. Tudi to kar še ni pojasnjeno se razume kot, da bo mogoče
  pojasniti oz. je na poti pojasnjenja in (industrijskega) izkoriščanja.

-->

V eseju, oziroma poglavju, "Razredna zavest" gre Lukácsu za nadaljevanje s
točke, kjer se Marxovo poglavitno delo preneha: na točki kjer se loti
opredeljevanja razredov. [@lukacs1985zgodovina, 63] Gre za zadnje poglavje
tretjega zvezka Kapitala

> Prvo vprašanje, ki je treba nanj odgovoriti, se glasi: Kaj tvori razred?
> Odgovor izhaja sam po sebi iz odgovora na drugo vprašanje: Zakaj sestavljajo
> mezdni delavci, kapitalisti in zemljiški lastniki tri velike družbene razrede?
> 
> Na prvi pogled zaradi istosti dohodkov in virov dohodkov. Imamo tri velike
> družbene skupine, katerih komponente, posamezniki, ki jih sestavljajo, živijo
> od mezde, profita oziroma zemljiške rente, od vrednostnega izkoriščanja svoje
> delovne sile, svojega kapitala oziroma svoje zemljiške lastnine.
> 
> Toda s tega vidika bi, na primer, tudi zdravniki in uradniki sestavljali dva
> razreda, saj pripadajo dvema različnima družbenima skupinama in dohodki članov
> vsake skupine dotekajo iz istega vira. Isto bi veljalo tudi za neskončno
> razdrobljene interese in položaje, ki jih ustvarja družbena delitev dela tako
> med delavci kakor med kapitalisti in zemljiškimi lastniki; le-ti se, na
> primer, delijo na posestnike vinogradov, njiv, gozdov, rudnikov, ribnikov.
> [@marx1973kapital3, 986]

In takoj za tem Engelsov uredniški pripis: "Tu se rokopis konča."

- Kaj pomeni razredna zavest?
- Kaj naj (teoretsko) razumemo z razredno zavestjo?
- Kakšna je funkcija tako razumljene razredne zavesti (praktično) v samem
  razrednem boju?
- Ali gre pri vprašanju razredne zavesti za neko "splošno" sociološko vprašanje
  ali pa pomeni to vprašanje za proletariat nekaj povsem drugega kot za vsak
  drug razred, ki se je doslej pojavil v zgodovini?
- Ali sta bistvo in vloga razredne zavesti nekaj enotnega ali pa lahko tudi tu
  razlikujemo stopnje in plasti?
- Če da: kakšen je njihov praktični pomen v razrednem boju proletariata?

## 1. [neodvisnost dejanskih gibalnih sil zgodovine od zavesti ljudi o njih]

Jasno je, da je človeška zgodovina niz hotenih in zavestnih dejanj. Toda pri
razumevanju zgodovine moramo seči preko tega. Na eni strani lahko ugotovimo, da
množica teh posamičnih volj poraja povsem drugačne rezultate od hotenih. Motivi
k določenemu rezultatu so torej podrejenega pomena. Na drugi strani se lahko
vprašamo kaj so tiste zgodovinske sile, ki stojijo za temi motivi. Kaj so
zgodovinske sile, ki spravijo ljudi (razrede) v gibanje za zgodovinsko
spremembo? Za Lukácsa so razlike med vzroki, motivi in rezultati metodološka
osnova znanstvenega marksizma:

> Bistvo znanstvenega marksizma je torej v spoznanju neodvisnosti dejanskih
> gibalnih sil zgodovine od (psihološke) zavesti ljudi o njih.
> [@lukacs1985zgodovina, 64]

Ta neodvisnost gibalnih sil zgodovine se najprej kaže kot narava, kot "večni"
naravni zakoni.

Meščanska misel razmišljanje o človeških oblikah življenja začne
nasprotno od dejanskega razvoja človeških oblik življenja: znanstvena analiza se
začne *post festum*, z gotovimi rezultati razvojnega procesa. Za meščansko misel
oblike že pridobijo značaj naravnih oblik družbenega življenja, preden si skuša
razložiti njihovo vsebino. Nasproti temu dogmatizmu, Lukács marxovsko metodo
opredeli kot kriticizem, teorijo teorije, zavest zavesti, kot historično
kritiko. Družbene tvorbe razkriva kot tvorbe, ki so zgodovinsko *nastale*, torej
so *podvržene* zgodovinskemu razvoju in lahko tudi zgodovinsko *propadejo*.

> Tako se zgodovina ne dogaja niti *zgolj* znotraj veljavnega okvira teh oblik
> (to bi pomenilo, da je zgodovina le spreminjanje vsebin, ljudi, situacij itn.
> ob večni nespremenljivi veljavnosti družbenih načel) niti niso te oblike
> *cilj*, h kateremu teži vsaka zgodovina, ki bi bila po dosegu tega cilja
> odpravljena, saj bi izpolnila svojo nalogo. Temveč je to prav zgodovina teh
> oblik, njihovih pretvorb kot oblik združevanja ljudi v družbo, kot oblik, ki
> izhajajo iz ekonomskih stvarnih odnosov in gospodujejo nad celoto medsebojnih
> odnosov med ljudmi (in s tem tudi odnosov ljudi do samih sebe, do narave
> itn.). [@lukacs1985zgodovina, 64]

Ker sta s tem izhodišče in cilj meščanske misli vedno potrdilo za
nespremenljivost obstoječega reda stvari, mora meščanska misel tu trčiti na
neprekoračljivo mejo. Zgodovinskega procesa ne more miselno obvladati. Lahko ga
miselno odpravi in organizacijske oblike sedanjosti dojame kot večne naravne
zakone, ki so se v preteklosti -- na način, ki je nezdružljiv prav z načeli
meščanske racionalne in k zakonitostim stremeče znanosti -- uveljavljali
nepopolno ali pa sploh ne. Lahko pa iz zgodovinskega procesa izloči vse, kar je
smiselno in teži k nekemu cilju, ter ostane pri goli individualnosti
zgodovinskih epoh in njihovih družbenih in človeških nosilcev.

V prvem primeru zgodovina otrdi v *formalizem*, ki ne more zapopasti
družbeno-zgodovinskih tvorb v njihovem bistvu kot odnosov med ljudmi. V drugem
primeru zgodovina postane brezumno gospostvo slepih sil, ki jih lahko le
pragmatično opišemo, ne moremo pa jih dojeti kot umne. Lahko jih le *estetsko
organiziramo*, ali pa jih moramo dojeti kot *na sebi nesmiseln material* za
uresničevanje brezčasnih, nadzgodovinskih, *etičnih* načel.

V teh medsebojno izključujočih nazorih o istem predmetu pri, na eni strani,
*formalistično-racionalnem* ter, na drugi, *iracionalno-individualističnem*
pojmovanju zgodovine, se zrcali antagonizem kapitalističnega produkcijskega
reda. 

Prvi pravzaprav pove, da je človek v meščanski družbi podrejen produkcijskim
silam. Družbeno gibanje ima zanje obliko gibanja stvari, pod nadzorom katerih
so, namesto da bi oni nadzorovali nje. [@marx2012kapital1, 60] Nasproti temu
Marx postavi pojmovanje kapitala (in vseh predmetnih oblik politične ekonomije)
kot *družbenega odnosa* med ljudmi, ki se izraža s stvarmi. Z redukcijo človeku
tuje stvarnosti družbenih tvorb na odnose med ljudmi Marx hkrati odpravi z drugo
platjo dileme. Odnosi človeka do človeka so pri marksovski historični kritiki le
temelj tuje stvarnosti družbenih tvorb. Z redukcijo torej ne odpravlja neodvisne
zakonitosti in objektivnosti teh tvorb do človeških volj oziroma do volje in
mišljenja posameznika. Pri Marxu je objektivnost samoobjektiviranja človeške
družbe na določeni stopnji svojega razvoja in velja zgolj v okviru tistega
zgodovinskega okolja, ki jo proizvaja in ki ga ta spet določa.


Videti je, da v Marxovi razrešitvi spet ni prostora za zavest. Res zavestni
odsevi različnih stopenj ekonomskega razvoja ostajajo pomembna historična
dejstva, dialektični materializem ne zanika, da ljudje sami izvršujejo svoja
zgodovinska dejanja in da to delajo zavestno. Vendar je to, po M & E, napačna
zavest ...

Toda ne moremo se enostavno zadovoljiti s preprosto ugotovitvijo o "napačnosti"
te zavesti, oziroma z zoperstavljanjem pravilnega in napačnega. Pač pa metoda
zahteva *konkretno raziskavo* te napačne zavesti.

- Napačna zavest kot moment tiste zgodovinske totalnosti, ki ji pripada, kot
stopnja tistega zgodovinskega procesa, v katerem deluje.

Tudi meščanska zgodovinska veda "konkretno raziskuje". Njena napaka, pravi
Lukács, da to konkretno poskuša najti v empiričnem historičnem individuu in v
njegovi empirično dani, psihološki ali množičnopsihološki zavesti. Toda tu
zgreši zajeti *družbo kot konkretno totalnost*, zgreši produkcijski red na neki
določeni stopnji družbenega razvoja in z njim pogojeno členitev družbe v
razrede. Tako nekaj abstraktnega -- odnos individua do individua -- zajame kot
nekaj konkretnega. (spregleda pa da gre za konkretne odnose v teh konkretnih
družbenih formacijah, npr. razmerje delavca do kapitalista, najemnika do
zemljiškega lastnika itn.)

Kaj je torej marksovsko *konkretno raziskovanje*?

> Konkretno raziskovanje pomeni torej: odnos do družbe kot *celote*. Šele
> v tem odnosu se namreč pojavi v vseh svojih bistvenih določilih vsakokratna
> zavest ljudi o svojem obstoju. [@lukacs1985zgodovina, 66]

Ta zavest se po eni strani pojavlja kot nekaj, kar je *subjektivno* upravičeno,
razumljeno in mogoče razumeti iz družbenozgodovinskega položaja. Torej kot
"pravilna zavest". Po drugi strani kot nekaj, kar *objektivno* zaobide bistvo
družbenega razvoja, ki ga ne zadeva in ne izraža ustrezno. Torej kot "napačna
zavest".

> Razredna zavest je torej -- abstraktno formalno gledano -- hkrati razredno
> določeno *nezavedanje* lastnega družbenozgodovinskega ekonomskega položaja.
> [@lukacs1985zgodovina, 67]

### 2.


### 3.

### 4.

### 5.

- Zanima nas v navezavi na formiranje ideologije
- Glavni stališči sta od proletariata in kapitalistov (kapitala). Ostale
  družbene skupine imajo nestalna, zastarela, minljiva stališča.
- proletariat je strukturno mesto. Intelektualci lahko zavzamejo proletarsko
  stališče (je to razredna kritika?)

<!--
## [Za kaj gre pri problemu reifikacije?]

Razširitev Marxove obravnave blagovnega fetišizma. Za Lukácsa je vprašanje kako
blagovna forma oblikuje delovanje vseh družbenih sfer. Kaj blagovna forma pomeni
za medije, politiko, vsakdanje življenje ... Kako blagovna forma predstavla neko
paradigmo racionalnosti ki prežema vse aspekte družbenega življenja.
-->

---
lang: sl
references:
- type: chapter
  id: fortini2016lukacs
  author:
  - family: Fortini
    given: Franco
  title: "Lukács in Italy"
  container-title: "Test of powers: writings on criticism and literary institutions"
  translator:
  - family: Toscano
    given: Alberto
  publisher-place: London
  publisher: Seagull Books
  issued: 2016
  page: 319-353
  language: en
- type: book
  id: lukacs1985zgodovina
  author:
  - family: Lukács
    given: György
  title: "Zgodovina in razredna zavest: študije o marksistični dialektiki"
  title-short: "Zgodovina in razredna zavest"
  publisher-place: Ljubljana
  publisher: Inštitut za marksistične študije ZRC SAZU
  issued: 1985
  language: sl
- type: book
  id: marx2012kapital1
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 1
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2012
  language: sl
# vim: spelllang=sl
...
