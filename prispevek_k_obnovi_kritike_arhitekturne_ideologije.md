---
title: "Prispevek k obnovi kritike arhitekturne ideologije"
author: Uroš Mikanovič
date: 2022
abstract: |
  Naloga obravnava zgodovinska izhodišča tafurijanske kritike
  arhitekturne ideologije, njene teoretske vire in obzorje znotraj
  katerega je delovala, da bi preverila sodobne pogoje njene obnove, to
  je aktualizacije. S tem posegamo v sodobno razpravo, kjer so zopet v
  ospredju možnosti arhitekturnega delovanja na družbo in v njej, s tem
  pa je obnovljeno tudi zanimanje za kanonizirana arhitekturnoteoretska
  dela italijanskega arhitekturnega zgodovinarja Manfreda Tafurija. Ker
  ta dela zadevajo najosnovnejša določila discipline, je njihov
  zgodovinski sprejem in povzemanje zgodovina teoretskih polemik. K
  obnovi zato pristopamo z natančnejšim pregledom tafurijanskih
  teoretskih izhodišč, da bi natančneje določili teoretski aparat in
  tako vprašanja, ki si jih zastavlja. Obnovi sledimo usmerjeno v tisti
  del tafurijanskega opusa, ki je predmet najostrejših polemik in ki mu
  pripisujemo najizrazitejšo distinktivnost do ostalih teoretskih
  tradicij znotraj arhitekturne teorije, to je usmerjeno v Tafurijev
  predlog kritike arhitekturne ideologije, ki je umeščen znotraj
  marksovske kritično-zgodovinske tradicije. Z nalogo želimo
  obravnavati najintenzivnejše teoretske formulacije določil
  arhitekturnega delovanja, nasloviti prevladujoče (površne)
  interpretacije teh ter predlagati njihovo doslednejše branje, ki je,
  menimo, še vedno veljavno v kontekstu sodobne arhitekture. Uspešno
  obnovo kritike arhitekturne ideologije razumemo kot njeno (delno)
  preseganje skozi strog sprejem njenih temeljnih teoretskih kategorij.
  Uvodoma bi naslovili osnovna problemska izhodišča, ki zaznamujejo
  marksovsko, torej tafurijansko, kritiko, ter se s tem lotili
  odpravljanja njenih najbolj poenostavljenih interpretacij. Obnovljeno
  interpretacijo bi gradili na natančnejši kontekstualizaciji
  predhodnega in neposrednega intelektualnega miljeja osrednjih besedil
  tafurijanske kritike. Šele na tej podlagi preverjamo produktivnost
  kritike arhitekturne ideologije, kot je izvorno predlagana, pri
  razlagi položaja in statusa arhitekturnega delovanja v trenutnem
  kapitalističnem ciklu.
# abstract: |
#   The thesis examines the historical foundations of the
#   Tafurian critique of architectural ideology, its theoretical sources
#   and the horizon within which it operated, in order to examine the
#   contemporary conditions of its renewal, that is its actualization. In
#   doing so, we are intervening in a contemporary debate where the
#   possibilities of architectural action on and in society are once again
#   at the forefront, and with it there is a renewed interest in the
#   canonised architectural theoretical works of the Italian architectural
#   historian Manfredo Tafuri. Since these works concern the most
#   fundamental determinants of the discipline, their historical reception
#   and summarisation is a history of theoretical polemics. We therefore
#   approach the restoration with a closer examination of Tafurian
#   theoretical foundations, in order to define more precisely the
#   theoretical apparatus and thus the questions it raises. We follow the
#   renewal by focusing on the part of Tafurian oeuvre that is the subject
#   of the most acute polemics and to which we attribute the most
#   pronounced distinctiveness toward other theoretical traditions within
#   architectural theory, that is by focusing on Tafuri's proposal for a
#   critique of architectural ideology, which is situated within the
#   Marxian critical-historical tradition. The aim of the thesis is to
#   address the most intensive theoretical formulations of the
#   determinations of architectural action, to address the dominant
#   (superficial) interpretations of these, and to propose a more rigorous
#   reading of them, which we believe is still valid in the context of
#   contemporary architecture. The successful renewal of the critique of
#   architectural ideology is understood as its (partial) transcendence
#   through a rigorous acceptance of its fundamental theoretical
#   categories. We will begin by addressing the basic problematic starting
#   points that characterise Marxian, that is Tafurian, critique, and
#   thereby set out to dismantle its most simplistic interpretations. A
#   renewed interpretation will be built on a more accurate
#   contextualisation of the prior and immediate intellectual milieu of
#   the central texts of Tafurian critique. Only on this basis do we test
#   the productivity of the critique of architectural ideology, as
#   originally proposed, in explaining the position and status of
#   architectural activity in the current capitalist cycle.
keywords:
  - arhitekturna teorija
  - Manfredo Tafuri
  - kritika arhitekturne ideologije
  - kapitalistični razvoj
  - arhitekturno delo
  - marksizem
# keywords:
#   - architectural theory
#   - Manfredo Tafuri
#   - critique of ideology
#   - capitalist development
#   - architectural work
#   - Marxism
lang: sl
...

<!--
# Zahvala

Vsako besedilo še pred avtorjevo končno obstaja v številnih drugih
oblikah. Hvala vsem, ki so s svojim moje delo naredili enostavnejše.
Zato upam, da si delijo tudi moj ponos.

Hvala mentorici Petri Čeferin za vodstvo skozi študij in izdelavo. Hvala
za vsa opozorila na to kjer kritika žargona sama postaja žargon.
Arhitekturnoteoretska praksa, kot jo sam lahko pojmujem in zasledujem,
dolguje njenemu stalnemu delovanju.

Hvala sošolcu in sodelavcu Blažu Šenici za vzgajanje kulture neizprosne
kritike vsega obstoječega od špranj študentskega organiziranja do valov
radijskega oddajanja. Tekom tega so se, in se še bodo, preverjale
številne premise in zaključki pričujoče naloge.

Hvala mami, babi in bratu za razumevanje ter za ustvarjalnost in
avtonomijo pri pojmovanju in obvladovanju izmuzljive družbene
mobilnosti. Zaradi njih se je korak od abecede do magistrske naloge zdel
posebej enostaven.

Hvala Nastji Mezek za potrpežljivost, za vse izlete v Italijo in za
pomoč pri prevajanju in sledenju drobtin do celote. Hvala ji za vse
pomene, ki jih naloga privzema izven svojega akademskega konteksta in ki
jih razumeva le midva.
-->

<!--
# Zagovor

Vprašanje, ki si ga na najsplošnejši ravni zastavlja naloga, je
»zadevnost« tafurijanske kritike arhitekturne ideologije danes.
Natančneje se to vprašanje zastavlja kot vprašanje, ali se struktura
arhitekturne zavesti do družbe še vedno pojavlja v enakih obrisih kot se
je ob koncu povojnega modernizma, čas, ki ga Tafurijeva besedila
neposredno nagovarjajo. V nalogi predlagam, recimo temu, natančnejše
branje Tafurija, tako da ga berem v njegovem zgodovinskem predvsem pa v
njegovem teoretskem kontekstu in zagovarjam, da je še vedno aktualen
avtor.

Osrednja obravnavana Tafurijeva besedila v nalogi so knjiga *Teorije in
zgodovina arhitekture* iz leta oseminšestdeset in esej *H kritiki
arhitekturne ideologije* v reviji *Contropiano* iz leta
devetinšestdeset. Gre za nekakšni napovedi programa kritike arhitekturne
ideologije, ki mu sledi desetletje organiziranega raziskovanja v to
smer.

Ta del njegovega opusa ima cela sedemdeseta in osemdeseta zagotovljeno
mesto v evropskih in ameriških arhitekturnih razpravah. Obstaja torej
zgodovina branj in interpretacij Tafurija, ki pa je, po njegovih lastnih
besedah, zgodovina nenatančnih ali napačnih branj.

Če navedem nekaj primerov:

V predgovoru k *Projektu in utopiji*, razširjeni izdaji nekaterih esejev
in *Contropiana*, avtor omeni obtožbe o »plesu na grobu arhitekture« in
o »poetiki odpovedi«. Te pomeni so mu povsem tuji in napoti nas na
intelektualni kontekst revije *Contropiano*.

In dalje, na primer, se avtor ne prepozna v svojem ameriškem renomeju,
ki je ustvarjen tekom zelo naklonjenih branj v sklopu študijskih in
raziskovalnih skupin *Oppositions* in *Revisions*. V pismu ameriški
arhitekturni teoretičarki, objavljenem petindevetdesetega v Kasabeli
Tafuri implicira, da američani zlorabljajo njegov teoretski aparat in
jim svetuje naj njegovo misel raje historizirajo. To sicer naklonjeno in
hiperpolitično branje je v angloameriškem sprejemu Tafurija sodelovalo
pri konstrukciji nekakšne karikature tafurijanskega absolutnega
determinizma, nekakšne mehanske pogojenosti arhitekture. 

V devetdesetih znotraj arhitekture sledita dve desetletji pragmatičnega
obrata stran od teorije na sploh, v povezavi s splošnim svetovnim
kontekstom pa sledi še posebej oster obrat stran od marksovske teoretske
tradicije. Toda zadnje desetletje je, pod pritiskom družbene, politične
in okoljske krize, priča nekakšni obuditvi vprašanj alternative in
možnosti sistemske kritike. Znotraj arhitekture se spet zastavljajo
vprašanja kritične vloge arhitekturne discipline ali njene soudeležbe v
obstoječem redu. Od leta dva tisoč osem, včasih tudi v povezavi s tem,
dobimo številna nova branja Tafurijevih del. Naše obnove se tako ne
lotevamo v vakumu in in pomenov, ki jih pripisujemo tafurijanski
kritiki, si nismo izmislili sami. 

In v teh dveh kontekstih predlagam natančnejše branje. Na eni strani
imamo zgodovino napačnih branj, na drugi sedanjost, ki zopet izrazito
odpira predelane teme. Z natančnejšim branjem se lahko izognemo
teoretskim nesporazumom in natančneje določimo področje aktualnosti
tafurijanske kritike arhitekturne ideologije. Tega ne počnemo, ker bi
popravljali kakšno zgodovinsko krivico, ampak ker mislimo, da aktualne
razprave o arhitekturni problematiki dospevajo do omejitev in
ugotovitev, ki jih je že Tafuri zelo produktivno razgrnil.

In če hočemo natančneje brat, moramo brat v zgodovinskem in
intelektualnem kontekstu. Tafurijevo misel moramo historizirat in
vstopit v njegovo teoretsko obzorje, da bi sploh določili vprašanja, ki
si jih zastavlja.

Konkretno v nalogi se obrnem k italijanski arhitekturni razpravi ob
povojni obnovi. V napeti politični situaciji, ki se v veliki meri kaže
tudi kot prostorski, stanovanjski in načrtovalski problem, napredni
arhitekti oblikujejo serijo reformističnih *tendenc*. Vsaki sledi dvojno
razočaranje: na eni strani se vsakemu arhitekturnemu predlogu
»prikradejo« nearhitekturni, kapitalistični interesi, na drugi razvoj
tehnologij, prostorskega načrtovanja nakazuje na postopno »degradacijo«
poklica. Do srede šestdesetih pesimizem, razočaranje ali kritična zavest
niso Tafurijeve osebne kaprice, ampak posplošeni občutki znotraj
širokega dela arhitekturne zavesti.

Ti občutki niso omejeni znotraj arhitekturne kulture, ampak je stvar
širše napredne inteligence, reformističnih strank in naprednih gibanj.
Zakaj kljub dobrim namenom sodelujemo in vzdržujemo neko objektivno
stanje, ki ga zasebno želimo odpraviti?

Na splošnem naprednem političnem področju *operaisti* predlagajo kar bo
na koncu teoretski projekt kritike ideologij. V nalogi to pokažem v treh
stopnjah besedil klasičnega operaizma: Pancierijevo kritiko
»objektivističnega« pojmovanja tehnološkega razvoja, to je kritiko
tistega pojmovanja tehnologije, ki ji pripisuje vzroke in učinke, ki so
ločeni in neodvisni od družbenih produkcijskih razmerij, Trontijevo tezo
o »družbeni tovarni«, to je da kapitalizem oblikuje celotno družbo po
svoji podobi, ki izhaja iz tovarne, in Fortinijevo kritiko
pokroviteljstva intelektualcev in predlog naj svoja področja poskušajo
misliti materialistično, to je kot del materialnih produkcijskih
razmerij.

Na točki, ko obnavljam *Teorije in zgodovino arhitekture* in *H kritiki
arhitekturne ideologije*, bi rad pokazal, da Tafuri nadaljuje in
nadgrajuje »operaistično« kritiko ideologije na področju arhitekture.

Ko govori o mrku zgodovine, govori o tisti intelektualni zavesti, ki
strogo ločuje preteklost kot zgodovino od sedanjosti. To sodobnemu
kritiku ali arhitektu omogoča, da zase misli, da ima vse možnosti
samovoljnjega povzemanja zgodovine v namene sodobnosti ali prihodnosti,
sebe pa iz zgodovine izvzame – svojo dobo tako pojmuje *ahistorično*,
naravno. Tu so, pravi Tafuri, miselni ali ideološki pogoji, ki kasneje v
modernizmu omogočajo spontano, nereflektirano vero v napredek, ločen od
kapitalističnega družbenega konteksta.

Tafurijeva kritika operativne zgodovine torej ni zoperstavljanje eni
»zgodovini kot je bila« z drugo »zgodovino kot je v resnici bila«, ampak
Tafuri predlaga koncept zgodovine, ki miselno ne blokira razmisleka o
razvoju arhitekture in nenazadnje ne blokira razmisleka o vzrokih in
krizi arhitekture ob koncu modernizma.

In to je natančnejši predmet *Projekta in utopije*.

Ko govori o nemški medvojni socialdemokratski arhitekturi govori o
Olivettijevem tehnoutopičnem reformizmu.

Ko govori o avstrijski medvojni socialdemokratski arhitekturi govori o
italijanskem neorealizmu.

Ko govori o Le Korbuzejevem *Planu Obus*, govori o Kejnsovi *Splošni
teoriji*. Na tej točki zaključuje, da je zdaj jasno, da si niti
programsko in formalno najnaprednejši arhitekturni predlogi ne morejo
več obetati biti subjekt to je upravljalec kapitalističnega razvoja,
ampak zgolj objekt, torej so že upravljani s strani kapitalističnega
razvoja.

Tafurijev esej dalje neposredno diagnosticira postmoderne obrate k
historičnem eklekticizmu, k podobi, k pluralnosti pomenov, k ironiji ...
kot soočenje s to dokončno izgubo ene funkcije arhitekture.

Za en del arhitekturne kulture so te umiki vsekakor vzdrževali nekakšen
občutek optimizma. Ampak danes je nevzdržnost tega optimizma že jasna.
Še posebej po svetovni gospodarski krizi dva tisoč osem in po vse
resnejših opozorilih znanstvene skupnosti o prihajajoči okoljski krizi
napredna arhitekturna kultura spet pretendira po podobni aktivni vlogi v
družbi, ki jo pripisuje tudi zgodovinskim avantgardam. Poteguje se za
*solidarnost* z univerzalnim stališčem.

Današnji ideološki kontekst se mi zdi zelo podoben povojnemu, oziroma
Tafurijevemu. Še več: mislim, da sploh ne gre za subtilne podobnosti.

Ob novem, digitalnem valu avtomatizacije gradbenega sektorja se napredna
arhitekturna kultura spet zateka v »objektivistična« pojmovanja
tehnologije in si za svojo stroko obeta, da je subjekt, ne objekt plana.
Na drugi strani, ko želi določiti zgodovinske in materialne pogoje
okoljske krize, sebe iz te zgodovine in iz teh materialnih odnosov
izvzema in torej ideološko blokira realne možnosti njenega delovanja.

Ne bi poudarjal, da to pomeni, da je današnja arhitekturna zavest
zaostala, temveč da kritika arhitekturne ideologije presega svoj
zgodovinski kontekst in je še vedno aktualna za mišljenje današnjega
statusa in vloge arhitekture v svetu.

Predpogoj *solidarnosti* *do* družbe, ki jo napredna arhitekturna
kultura terja od sebe, je trezna in kritična analiza vlog in pomenov
arhitekture znotraj kapitalističnega razvoja. Za Tafurijevo kritiko
arhitekturne ideologije tako ne moremo reči, da je »umik« ali »odpoved«,
ampak je poskus organizacije pesimizma. Pesimizma, ki ga kapitalistična
družba nenehno izziva. Ta organizacija pesimizma pa želi izzvati
solidarnost *znotraj* družbe.

Če pravilno beremo Tafurijev historični materializem, ta ne »omejuje« in
ne izganja arhitekturo v absolutno mehansko družbeno ali zgodovinsko
določenost. Ampak želi natančno in jasno razgrniti vsa parametre te
določenosti. Da je arhitektura materialno določena lahko pomeni le, da
je del materialnega sveta, da ko se poteguje »za svet« se lahko poteguje
le za sebe in obratno. In da je zgodovinsko določena lahko pomeni le, da
pomeni, statusi in odnosi, ki jih arhitektura nosi, nastajajo, se
razvijajo in opuščajo tekom zgodovine, torej niso univerzalni in
zaključeni, ampak spremenljivi in odprti.
-->

# Uvod in kar je na kocki

Zdi se, da ponoven začetek razprave o marksovski kritiki arhitekturne
ideologije, potrebuje obsežnejši uvod. Na eni strani zato, ker mora
vsako novo branje vstopiti na »homersko bojišče« živahnih in tekmovalnih
interpretacij zapuščine besedil, ki so po svojem izidu pred več kot
petdesetimi leti stalen predmet arhitekturnoteoretske razprave, do te
mere, da je ta zapuščina vključena v sam kanon arhitekturne teorije.
Govorimo, seveda, o prispevkih Manfreda Tafurija v reviji *Contropiano*
iz konca šestdesetih in začetka sedemdesetih let 20\. stoletja, [gl.
@tafuri1969critica; -@tafuri1970lavoro; -@tafuri1971socialdemocrazia;
-@tafuri1971austromarxismo] med katerimi so nekatera kasneje razširjena
in izdana kot samostojno delo. [gl. @tafuri1977progetto; slovenski
prevod v @tafuri1985projekt] Ta dela, dela sorodnih avtorjev ter
komentarji in kritike argumentacije in teoretskega obzorja teh del
skupaj tvorijo eno najbolj zastopanih »zvrsti« v antologijah, ki
obravnavajo obdobje po njihovem nastanku. [gl. @nesbitt1996theorizing;
@hays1998architecture] Zastopanost teh besedil in njihovih posledic v
arhitekturnih razpravah priča o pomembnosti, morda nerazrešenosti, teme,
ki jo naslavljajo: to je tema, v najsplošnejšem obsegu, položaja in
vloge arhitekturnega poklica, to je intelektualnega dela, znotraj
kapitalističnega razvoja. Narava razprave – to, da se poteguje za
razlago najosnovnejših značilnosti družbenega ustroja discipline,
področja ali poklica ter posledično za opis mej kritično zavestnega
delovanja znotraj njega – do neke mere izključuje možnost zadovoljivih
absolutnih zaključkov ali zadnjih besed. Vprašanju aktualnosti je torej,
zahvaljujoč tej naravi, samoumevno zadoščeno. A obuditev razprave o
zapuščini kritike arhitekturne ideologije, kvalitetni prispevki o
intelektualni zgodovini Manfreda Tafurija in šole, ki jo predstavlja
znotraj arhitekturne teorije in zgodovine, ter pozivi k strožjem in
doslednejšem historiziranju [v mislih imamo vsaj zelo natančno
bibliografsko študijo @leach2006choosing; kvalitetno biografsko študijo
@hoekstra2005building; razdelavo tafurijanskega projekta v odnosu do
arhitekturne sodobnosti @biraghi2013project; ter (ponoven) poziv k virom
@aureli2010recontextualizing] vseeno terjajo strožjo in natančnejšo
razdelavo mej in ciljev predloga novega branja. Na drugi strani zato,
ker je zgodovina gradiva tudi zgodovina njegovega nerazumevanja in
»neodgovornih branj«. Vsako novo branje mora zato biti tudi strategija
izogibanja že popisanih slepih ulic in stereotipov. To bi bilo
odgovorno. Kaj točno to pomeni, katere strategije so to, mora dokončno
biti prikazano tekom same naloge, uvodoma pa naj na razliko, ki jo
zasledujemo, namigne naša ocena, da je zgodovinska in nominalna
»pomembnost« prispevkov marksovske kritike arhitekturne ideologije in
kritične arhitekturne zgodovine možno pripisati distinktivnosti teh
prispevkov, ne le znotraj arhitekturne teorije, ampak tudi znotraj
marksistične tradicije. Kvalitete »epistemološkega reza« – katere bomo
postopno izpeljali iz orisa njegovega splošnega teoretskega obzorja in
natančneje iz specifične zgodovinske epizode v kateri so se razvile in
izkazale –, ki ga je opravil tafurijanski moment, ni mogoče omejiti
zgolj na arhitekturno področje, ampak zadeva širša družbena in
zgodovinska vprašanja intelektualnega dela, umetnosti in mesta. Zadeva
torej področja za razumevanje katerih se poteguje tudi marksovski
teoretski aparat posebej. Kar se tiče obravnave kritike arhitekturne
ideologije, novo branje torej v osnovi predlaga, namesto posledic in
zapuščine, raziskovanje njenih teoretskih in epistemoloških pogojev, če
želimo njen predmet položaja intelektualnega dela znotraj
kapitalističnega razvoja zopet kritično prikazati z istim uspehom, ki ji
je zgodovinsko že priznan. 

Nedolžnih branj, se ve, ni, vsak sprejem besedila je obremenjen ravno s
tistimi vprašanji, s katerimi je lahko zgolj sam obremenjen. Iz tega pa
ne sledi, da so vsa branja še vedno zadevna. Ravno nasprotno, brez slabe
vesti, ki jo nalagajo postmoderni interpretativni ključi, lahko priznamo
skromno merilo, ki, menimo, uveljavlja in razveljavlja posamična branja:
merilo zgodovinske razrešitve »strani« iz katere se je pristransko
bralo, torej, izginotje izvornega bremena, ki je besedilu pripisalo
svoja lastna vprašanja. Kar ne pomeni, da je že sama pristranskost ali
omejenost nelegitimna, temveč zgolj, da se zgodovinsko ni izkazala kot
zadovoljiv odgovor na še vedno odprta vprašanja, ki so jih je odprli
ravno izvorni prispevki. Če se želimo natančneje potegovati za »pravilno
branje«, moramo pravzaprav govoriti o širšem miljeju – tako biografskem,
filološkem, zgodovinskem, predvsem pa teoretskem – katerega skoraj
slučajen rezultat je besedilo. Dogmatična raztopitev besedila v njegov
kontekst je sicer plodno polje špekulacij in dvoumnih alibijev, a temu
se lahko, skoraj protislovno, zoperstavlja z natančnim branjem besedila.
Morda osrednje sporočilo predgovora k *Projektu in utopiji* je poskus
kontekstualizacije očitno napačno izpeljanih sporočil in njihovih
posledic:

> Kontekst revije, ki je gostila ta esej (in druge eseje pisca in
> tovarišev, ki so delovali v tej smeri), je bil na tak način opredeljen
> z njegovo politično zgodovino in smermi notranje razprave, da smo
> domnevali, da se bomo številnim dvoumnim branjem a priori izognili.
> 
> A ni bilo tako. Z osamitvijo arhitekturne tematike od teoretskega
> konteksta, katerega nosilka je bila revija, so naš spis presojali kot
> poklon nekemu apokaliptičnemu obnašanju, kot »poetiko odpovedi«, kot
> ekstremno napoved »smrti arhitekture«.
[@tafuri1985projekt, 3]

Posamezni članki v omenjeni reviji se nanašajo na raziskave in
argumentacije v člankih, ki so objavljeni v prejšnjih ali istih
številkah iste revije. Ansambel piscev in razpon tematik je v treh letih
obstoja revije precej stalen, spremembe te sestave pa so transparentno
objavljene, preusmeritve zavzeto argumentirane. Če značaja ne izda že
sam polni naslov – *Contropiano: materiali marxisti* –, kontekst revije
v istem predgovoru povzame tudi Tafuri: »[P]olje politične teorije, ki
je prisotno v najbolj razvitih raziskavah marksističnega mišljenja od
l\. 1960 do danes [1973, op\. a.].« [@tafuri1985projekt, 4] Del
dvoumnosti gradiva, lahko torej povzamemo, izhaja iz, na eni strani
precenjene, na drugi neizkoriščene, »tehnične pristojnosti« bralcev.
Sorodno oceno bralcev zasledimo tudi v Tafurijevi arhitekturni zgodovini
moderne Italije kjer »neodgovorni intelektualni ludizem« neoavantgardnih
arhitekturnih skupin kot so *Archizoom* in *Superstudio* izhaja iz
»nepremišljenega branja« revij »nove levice« kot so *Quaderni rossi*,
*Classe operaia* in *Contropiano*. [@tafuri1989history, 99;
@tafuri2002storia, 125] Ne gre torej zgolj za nenaklonjena arhitekturna
branja, temeljno nerazumevanje teoretskega obzorja kritike ideologije
arhitekture je bilo lastnost tudi naklonjenih, *arhitekturno omejenih*
branj. Nobeno produktivno branje ne more biti zgolj sintetična
aplikacija najbolj površinskih obrisov enega besedila v termine
praktičnega področja. Za vsakim besedilom stoji kontinent intelektualne
in teoretske zgodovine, kar ob enostranskem prestopanju kontekstov (na
primer z »osamitvijo arhitekturne tematike«) ni samoumevna ugotovitev.

Ker je možnost, oziroma nezmožnost, sinteze (arhitekturnega) *projekta*
na podlagi kritične (strokovne) zavesti poseben predmet prispevkov v
*Contropianu* bodo tovrstna projektantska branja naslovljena na mestu,
kjer bomo rekonstruirali argumente teh prispevkov. Razliko strategije
našega branja moramo prikazati na podlagi enega od pomembnejših
teoretskih sprejemov in njegovega razvoja.

Specifično angloameriško arhitekturnoteoretsko branje tafurijanske
kritike ideologije je še vedno prevladujoče optika skozi katero se
razume in prikazuje ta opus. Sicer gre za navdušen sprejem, ki pa
pretežno ni bil dovzeten za specifično politično stališče prispevkov,
temveč je »evropsko teorijo« obravnaval kot generičen prikaz
»marksizma«. [Angloameriški sprejem povzemamo po @day2012manfredo, kjer
je to »napačno« branje tudi produktivno uporabljeno; gl.
@day2012manfredo, 36 in dalje; deloma povzemamo tudi po
@leach2006choosing, poglavje 1, kjer je poudarjena selektivnost
angleških prevodov na račun Tafurijevih zgodovinskih spisov o renesansi;
o tej temi gl. tudi @ockman1995venice] Angloameriški sprejem je bil
filtriran skozi specifične interese skupin sprva okoli zbornika
*Oppositions* ter kasneje okoli študijske skupine *Revisions*. Zbornik
je skozi leta gostil več originalnih besedil beneške šole, a v splošnem
se je evrofilsko navduševal nad pestrim razvojem široko razumljene
*theory* »izmov« ter, sicer zgodovinsko razumljivo, dekontekstualiziral
specifično zahodnoevropsko problematiko kritike razvoja, napredka,
reformizma in marksistične sholastike. To eklektično zoperstavljanje
karikatur – kar bo končno zoperstavljanje karikature »zgodovinskega
determinizma« in »avtonomnega formalizma«, kot nasprotja znotraj
zbornika dramatizira Hays [@hays1998oppositions:hays, ix] – bo eden
izmed izvorov ozkega ameriškega Tafurija. Ločeno od zbornika, a znotraj
istega *Institute of Architecture and Urban Studies*, se je skupina
*Revisions* ustanovila ravno z namenom bolj osredotočeno raziskovati
»arhitekturo in politiko«. V začetku osemdesetih so organizirali
simpozij o arhitekturi in ideologiji ter kasneje na tej podlagi izdali
knjigo. [gl. @ockman1985architecture] Takrat, že v času začetka konca
*Oppositions*, si je *Revisions* zadal nalogo v arhitekturno razpravo,
vsaj na retorični ravni, vnesti kategorije razreda, kritike ideologije,
ter celo vprašanje družbene ali disciplinarne transformacije –
revolucije. A, morda antiklimaktično v razmerju do takratne zanesenosti
skupine, je razprava ostala omejena znotraj akademskega kroga in
svetovna arhitekturna kultura, z angloameriško na čelu, se je, tudi s
spoznanjem, da produktivnost kritike ni premo sorazmerna s subjektivnimi
strastmi kritikov, preusmerila v bolj pragmatična početja. Na tem mestu
Gail Day v svojem spisu zanimivo trdi, da je do depolitizacije
ameriškega Tafurija prišlo ravno z njegovo naivno politizacijo: »Kljub
temu se je zgodil proces, v katerem je bilo Tafurijevo pisanje ločeno od
njegovega konteksta družbenega spomina. Paradoksalno je šlo za
depolitizacijo, ki se je zgodila *skozi* vztrajanje na politiki.«
[@day2012manfredo, 293] Če lahko dvoumnosti projektantskih branj
pripišemo zanemarjanju razdalje med disciplinami, gre v tem primeru,
menimo, za zanemarjanje razdalj teoretskega obzorja. Slepa pega takšne
dekontekstualizacije in konstruktiven nasvet proti tej je nakazan v
pismu Manfreda Tafurija naslovljenega Joan Ockman. V pismu je
italijanski profesor pod vtisom, da so Američani »proizvedli Tafurija,
ki je nekoliko preveč drugačen od tistega, ki ga sam pozna.«
[@ockman1995venice, 67; Pismo je, natančneje, odziv na izid
@ockman1985architecture oziroma na nekatera vsebovana besedila] Sledijo
kratke razjasnitve pojmov *ideologija* in *revolucija* kot jih misli in
uporablja avtor, zaključi pa se s predlogom, da naj opustijo enostavne
»tipologije« in njegovo misel raje *historizirajo*. Deliti si teoretsko
obzorje ni zgolj stvar formaliziranega prenosa njegovega aparata, ampak
je kompleksna stvar njegove smiselne rabe, ki vsakič, ko izkaže svojo
uporabnost, ustvari tudi meje te uporabnosti (»1973 ni 1980, ni 1985
...« [Manfredo Tafuri navedeno po @ockman1995venice, 67]) Ta
»streznitev« skupine *Revisions* pravzaprav sorodno z determinističnim
Tafurijem zbornika *Oppositions* sodeluje v tistih branjih, ki jih
naslavlja predgovor *Projekta in utopije*: kritika ideologije kot
»apokaliptična« »poetika odpovedi«.

A ta epizoda vsebuje tudi (posredno) pozitivno demonstracijo branja, ki
ga zasledujemo: prispevek Fredrica Jamesona na simpoziju skupine
*Revisions*, kjer obravnava tafurijansko kritiko ideologije arhitekture,
[@jameson1985architecture; zdaj tudi v @jameson1998architecture] in
kasnejši razvoj Jamesonove misli v odnosu do kritičnega delovanja
znotraj arhitekture. V prispevku je tafurijanska kritika ideologije
ocenjena za enega najnaprednejših predlogov »tretjega termina«, ki za
Jamesona pomeni tisti odgovor na slepo ulico fenomenološkega in
strukturalističnega pojmovanja prostora. Hkrati pa je do Tafurijevega
pojmovanja zgodovine kot »totalnega sistema« kritičen, saj je takšno
pojmovanje dovzetno za pesimistična in brezupna pojmovanja delovanja
znotraj kulturne sfere. [@jameson1998architecture, 452] Zanj je torej
nujno, nasprotno od Tafurija, konstruiranje nekakšnih enklav, na ravni
nadstavbe, to je na ravni kulture, anticipirati novo družbo. Predlaga
torej, nejasno razdelan, pozitiven *gramšijanski* arhitekturni projekt.
S tem izrazom označuje praktičen politični projekt, ki bi danes
anticipiral prihodnjo osvobojeno družbo v kulturnih terminih. Pustimo ob
strani – da ne bi špekulirali o jezikovni dostopnosti besedil in
intimnih namerah avtorja –, da tafurijanski argument spodbuja k
premisleku ravno tistih profesionalnih kulturnih praks, ki se s
precenjevanjem lastne avtonomije nujno zaključijo v brezupu, in da je
konstrukcija enklav poseben predmet kritike arhitekturne ideologije in
ekspliciten predmet študij, ki izhajajo iz začetnih deklaracij
tafurijanskega kritičnega programa [v desetletju po izidu prvih besedil
v *Contropiano* Tafuri in sodelavci sistematično obravnavajo zgodovinske
primere realiziranih utopičnih enklav, njihova prizadevanja in zaključke
ter njihovo vlogo kot operativni zgodovinski modeli, ki informirajo
sedanjost. Gl. obravnavo arhitekture Oktobrske revolucije v
@asorrosa1972socialismo; obravnavo ameriške rekonstrukcije v
@ciucci1980american; obravnavo Rdečega Dunaja v @tafuri1980vienna; cikel
teh študij se zaključi z izidom @tafuri1987sphere] V kasnejši Jamesonovi
kritiki kritičnega regionalizma, [@jameson1994seeds, 189-205] za
katerega bi lahko rekli, da je ravno takšne vrste gramšijanski
arhitekturni projekt, pa lahko prepoznamo podobno strukturo
argumentacije kot v širšem opusu Manfreda Tafurija. Četudi vemo, da je
Tafurijevo pisanje pomembno vplivalo na Jamesona, [za pregled tega
vpliva gl. @day2012manfredo, 42-46; ali v slovenščino prevedena dela
@jameson2012kulturni, poglavje 1, 7 in 8] ga je ta, kot smo morda videli
tudi zgoraj, vedno obravnaval kritično; zatorej menimo, da gre za ločena
spoznanja, ki se kasneje (nezavedno) zbližajo. Podobnosti so zato
subtilne, našteli bi, nesistematično, zgolj nekaj točk. (1) Izpeljava
postmodernizma iz modernizma: »jasno je, da je zadostna mera modernizma
stanje postmodernizma in začetna točka marsikaterim razvojem slednjega.«
[@jameson1994seeds, 131] Sicer ne zagovarja popolne izpeljave enega iz
drugega (ali totalizirajoče enačenje enega z drugim), kot bi Jameson sam
morda očital Tafuriju (za katerega sta tako »postmodernizem« kot »kriza
modernizma« pomirjujoča diskurza in neuporabni kategoriji historizacije
kapitalizma), a vseeno menimo, da gre v povezavi z ostalimi točkami za
primer zbliževanja. (2) Vprašanje političnih in ekonomskih pogojev, ki
bi šele omogočali arhitekturni projekt: 

> Dejansko, je pri [kritičnemu regionalizmu] težava tudi v tem, da je
> odnos do družbenih in političnih gibanj, ki naj bi spremljala njegov
> razvoj, služila kot kulturni kontekst ali zagotavljala moralo in
> podporo, neteoretiziran [@jameson1994seeds, 193. Vsi prevodi iz tuje literature so naši, prevedeni iz navedenih objav in izdaj.].

Gre za podobno vprašanje, kot je zastavljeno v *Projekt in utopija*:

> Kdor zastavlja [slogan »kake osvobojene družbe«], se izogne vprašanju,
> če pustimo ob strani njegov boleč utopizem, ali se ta cilj lahko
> doseže brez lingvistične, metodološke, strukturalne revolucije, katere
> teža je daleč stran od enostavne subjektivne volje in enostavne
> posodobitve sintakse.
[@tafuri1985projekt, 114]

Tudi če ugibamo o političnem stanju regionalne avtonomije, je avtonomija
intelektualcev, ki bi izvrševali tak preskriptiven arhitekturni projekt,
zgolj prispodoba za nacionalno (politično-ekonomsko) avtonomijo, torej
za politično-ekonomski projekt umeščen, v tem primeru, v geopolitično
shemo globaliziranega kapitalizma. Kar privede do zadnje točke
zbliževanja, ki bi jo izpostavili. (3) Vprašanje povezave estetskih
zahtev s povsem notranjimi (objektivnimi) zahtevami kapitalizma:

> Še resnejši ugovor proti strategijam kritičnega regionalizma, kot tudi
> različnim postmodernizmom na splošno, ko sebi pripisujejo politično
> poslanstvo, se poraja zaradi vrednote pluralizma in slogana
> različnosti, ki ju vsi tako ali drugače podpirajo. Ugovor ne sestoji
> iz nekega prepričanja, da je pluralizem vedno liberalna, ne pa
> resnično radikalna, vrednota – dogmatično in doktrinarno stališče, ki
> bi ga ovrgel pregled katerega koli od številnih aktivnih momentov v
> zgodovini. Ne, nelagodje izvira iz same narave poznega kapitalizma, o
> katerem se lahko vprašamo, če nista pluralizem in razlike nekako
> povezana z njegovo globljo notranjo dinamiko.
[@jameson1994seeds, 204]

Ta razvoj Jamesonove misli glede arhitekturnih ideologij lahko uporabimo
za natančnejšo opredelitev novega branja, ki ga zasledujemo. Manfredo
Tafuri in Fredric Jameson si v tem momentu delita *teoretsko obzorje*,
ki jima na teoretskem torišču arhitekture omogoča zastavljanje istih
vprašanj. Enostavneje rečeno, arhitekturne problematike se lotevata
znotraj marksovske tradicije. Distinkcija, ki jo zavzemajo Tafurijevi
prispevki v *Contropiano* in Jamesonova obravnava postmoderne, do
ostalih pristopov k arhitekturni problematiki, distinkcija, ki ji
pripisujemo produktivnost, ki je drugim pristopom ne, je v spoznavnih
orodjih, katerih pogoji so razviti le znotraj te marksovske tradicije.
Skupaj z distanco do projektantskih branj ima naša strategija torej dva
momenta, ki opisujeta, končno, tudi pristranskost in interese našega
početja: področje arhitekture problematiziramo kot arhitekturni
teoretiki, ne kot arhitekti na sploh, in problematiziramo ga kot
marksovci, ne kot teoretiki na sploh. To se nam zdi primeren pristop k
eksplicitnemu predmetu naloge, ki je položaj in vloga arhitekturnega
dela znotraj kapitalističnega razvoja, kot produktivno izhodišče za
obravnavo arhitekture nasploh. Hkrati pa zasleduje tudi intimno tezo
naloge, ki trdi, da, če že želimo povedati kaj o arhitekturi, moramo
govoriti o svetu in obratno, če želimo povedati kaj o svetu, moramo
govoriti o arhitekturi – torej da je arhitektura, kot način materialnega
mišljenja, bistvena vstopna točka zgodovinsko in materialistično
obravnavo družbe.

Zadnji nalogi uvoda v takšno početje sta torej uvoda v vprašanji, kaj je
teorija in kaj je marksovska tradicija.

Gre za uvoda, ker nam nikoli ne gre za *bistvo* teh ali za njihovo
povzemanje *v celoti*. Obstaja, menimo, didaktičen vstop v ta vprašanja,
ki so se že odvila kot teoretska polemika med »marksovsko strujo«
arhitekturne teorije ter, kot jo sami poimenujemo, ljubljansko
arhitekturno šolo. Struja je zgodovinsko zavzemala stroga teoretska
stališča, ki se lahko znotraj ljubljanske arhitekturne šole kazala zgolj
kot krtačenje te šole proti njeni dlaki, saj: »Učenjaki niso risarji in
risarji niso možje besede; zato so bila moja predavanja slaba.« [Jože
Plečnik, navedeno po @omahen1976izpoved, 159]

Antiintelektualizem ljubljanske arhitekturne šole pa še ne pomeni, da ta
šola ne priznava *nobene* teorije. Prav v marksistični literaturi
arhitekt nastopa kot idealni producent in prav tam velja, da celo
najslabši med njimi mislijo:

> Pajek opravlja operacije, ki so podobne tkalčevim, in čebela z gradnjo
> svojih voščenih celic osramoti marsikaterega človeškega stavbenika. A
> tudi najslabšega stavbenika že vnaprej pred najboljšo čebelo odlikuje
> to, da celico, preden jo gradi v vosku, zgradi v svoji glavi. Na koncu
> delovnega procesa nastane rezultat, ki je bil ob njegovem začetku že
> navzoč v delavčevi predstavi, torej že idejno.
[@marx2012kapital, 149-150]

Sami bi šli še korak dlje in dodali: ne le, da je kakor vsaka družbena
praksa tudi arhitektura nujno miselna praksa, temveč, da orodja, s
katerimi ljubljanska arhitekturna šola misli, tvorijo skladen sistem, ki
ima poleg praktičnih tudi povsem teoretske cilje. Četudi je
antiintelektualistična in pragmatična, koncepti ljubljanske
teorije^[Glede na stališče, ki ga tudi sami zavzemamo znotraj te
polemike, bi morali reči »ljubljanske ideologije«, ker strogo rečeno ne
gre za alternativna pristopa ene ali drugačne teorije, temveč za
izključujoča pojmovanja samega spoznavnega predmeta arhitekture, kar en
»pristop« postavlja kot teorijo, drugega pa kot »apologetski diskurz«.]
– v mislih imamo ponotranjen žargon: *kontekst*, *iskrenost*,
*avtentičnost* s pridruženim *konceptom* – delujejo ne le v smeri
vsakokratne opore in opravičila določenega projekta v teku, ampak tudi
za zagovor arhitekture *nasploh*, kot praktične discipline.

In ta diskurz je nujna posledica vzpostavitve arhitekture kot avtonomne
discipline in nujen pogoj njenega vsakokratnega vzpostavljanja kot
avtonomne discipline. Poseganje v človeški svet in njegovo spreminjanje
v skladu s predhodno zamišljenimi idejami o tem svetu ni zgolj tema za
katero se posamezno arhitekturno podvzetje lahko odloči: je bistvena
predpostavka arhitekturnega delovanja in torej predpisana tema vsakega
arhitekturnega projekta. V tem smislu je arhitektura vedno ideološka
praksa: normativen predlog uresničevanja zamisli sveta, ki s poljubnimi
metodami legitimira svoje objektivizirane ideale. Skupinski refleksiji
(arhitektov in arhitektk kot družbene skupine z družbenim položajem in
družbenimi cilji) teh določujočih parametrov arhitekturnega delovanja
pravimo arhitekturna zavest, ki »vedno znova obravnava lasten predmet,«
[@zlodre2011zapiski, 7] to je pojasnjuje splošne in temeljne
predpostavke, ki omogočajo razumevanje arhitekture. Pojmovanje
(konceptualizacija) predmeta je bistveno za vzpostavitev področja
predmeta. Arhitekturna zavest znotraj družb blagovne proizvodnje si
vprašanja o svojem predmetu zastavlja kot vprašanja o svojem
*proizvodu*. Prevladujoče pojmovanje predmeta je tako pojmovanje
proizvoda ali *projektiranje*. Pojmovanje predmeta strukturira celotno
področje zato je prevladujoče pojmovanje tudi vladajoče pojmovanje, ki
se mora priznavati kot bistveno določilo področja arhitekture, kot edini
proces, ki arhitekturo utemeljuje in jo uveljavlja. Vzvratno pa mora
projektiranje oziroma projektant arhitekturo predstaviti kot objektivno
in univerzalno kategorijo, ki je predvsem izraz nadzgodovinsko
veljavnega odnosa med projektantovo (arhitektovo) zavestjo in projektom
(arhitekturo). Vsa »zunanja« problematika, oziroma dejanska protislovja,
se tako skozi strokovno optiko reducira na spopad med (arhitekturno)
zavestjo in dejanskostjo. Protislovja so postavljena na abstraktno
raven, arhitektura je izenačena z idejo samo, z ideali, ki so razglašeni
za splošno človeške. Arhitektura izenačena z idejo samo (Arhitektura)
torej sprva ni projektirana, temveč pojmovana, teoretizirana. Razlike
med oblikami arhitekturne zavesti se vzpostavljajo z različnimi odgovori
znotraj istega pojmovanja predmeta. Preobrati, kritike, revolucije,
alternativne definicije in teorije obstoječemu projektiranju
nasprotujejo z drugačnim projektiranjem, z »antiprojektiranjem«, a še
vedno pripadajo isti teoretični ravni, ki jo določa naloga, ki si jo
zavest zastavlja: projektiranje proizvodov. Razlike med njimi torej
dejansko ni, dokler je ne vzpostavimo s spremembo teoretične ravni,
torej s spremembo vprašanja oziroma naloge. [@zlodre2011zapiski, 7-8]

A četudi smo vsi vpleteni prisiljeni priznati, da arhitekturna teorija,
poleg arhitekturnega projektiranja, mora biti sestavni del Arhitekture,
so priznane zgolj tiste teorije, ki priznavajo primat projektiranja.
Takšne teorije so subsumirane znotraj projektantske naloge in ne
potrebujejo posebnega zagovora. (Za merilo priznanja je tu vzeta
potrebnost posebnega zagovora.) Sprememba teoretične ravni in posledična
sprememba naloge, ki jo zasledujejo tiste teorije s temeljno drugačnimi
pristopi, pa se kaže kot grožnja Arhitekturi in vsemu kar je izenačeno s
tem pojmom (Ideja ter Dobro). Te teorije, oziroma arhitekturna teorija
kot teorija, iz zgodovinsko legitimnih vzrokov na ljubljanski šoli za
arhitekturo potrebujejo poseben zagovor.

Postopoma se oddaljujemo od vključujočih pojmovanj teorije, ki zajemajo
katerikoli diskurz, ki spremlja praktične dejavnosti. Teza o posebni
vrednosti marksovske kritike znotraj arhitekturne teorije pa bo terjala
tudi postopno oddaljevanje od široko razumljene *theory* kot obstaja tam
kjer je sicer institucionalizirana kot samosvoja disciplina. Ta v svoji
najmanj vključujoči definiciji začetek arhitekturne teorije postavi v
šestdeseta leta 20\. stoletja, ko je »prevladovalo občutje, da se od
kulturne produkcije v njenem tradicionalnem pojmovanju – kultura kot
hkrati nekaj čemur nekdo pripada in kar nekdo ima [...] – ne more več
pričakovati, da bo spontano vzniknila kot dejstvo družbenega poteka,
temveč, da mora biti nenehno konstruirana, dekonstruirana in
rekonstruirana skozi samozavedne teoretske postopke.«
[@hays1998architecture:hays, x] Natančneje torej v obdobje za katerega
se zdi, da je od arhitekture začelo terjati posebno preizpraševanje, saj
zaradi objektivnih okoliščin (konkretno gre za krizo modernizma, tudi
modernizma v arhitekturi) sooznačevanje ali enačenje Arhitekture in
Dobrega ni več samoumevno ali spontano na voljo. Četudi takšno
antologiziranje priznava primat kritičnega obrata, ki so ga prispevale
marksistična in poststrukturalistična literatura, še vedno na isti ravni
vzporeja tako apologetsko kot kritično teorijo – nenazadnje mora
vključevati tudi tiste pragmatične obrate, ki teorijo kot samostojno
prakso zanikajo. To vzporejanje privede torej do nestabilne kategorije
teorije, ki znotraj sebe zanika svoje nasprotujoče si dele: na eni
strani ne ustreza naši tezi o posebni vrednosti marksovske kritike
znotraj arhitekturne teorije, na drugi ne ustreza ljubljanski šoli, ki
je v svojih šestdesetih (z značilno zamudo torej v osemdesetih letih) iz
svojega obrata izobčila marksovce (in ustoličila prej omenjeni žargon
avtentičnosti). Ta polemika je zabeležena v zbirki del
[@jakhel1979iluzija; @rotar1981pomeni; @gantar1985urbanizem;
@rotar1985risarji; @tafuri1985projekt], ki

> materialistično in zgodovinsko konceptualizirajo ideologijo na
> področju zidave, gre za prispevke k *spoznanju* problematike in
> problematičnosti arhitekture ter zidave mest. Ta dela, drugače kot
> dušni pastirji, ki jih zanimajo samo udobne molilnice in higienski
> kropilniki in za katere sta arhitektura in mesto zgolj »estetski
> objekt«, objekt apologetskega diskurza in malikovanja, so arhitekturo
> tudi pri nas končno premestila na torišča raziskovanja oziroma
> *teorije*. Utrla so problemsko polje, kjer se je arhitektura zares
> postavila pod vprašaj, postavila v krizo, in kjer je postala »predmet«
> analize.
[@zlodre1988pastirju, 15]

Natančnejši povzetek polemike, zagovor »materialističnega in
zgodovinskega« pojmovanja arhitekture ter opis splošne strukture med
področji in teorijami področij (med ideologijo in znanostjo) poda Braco
Rotar v svojih študijah slovenske arhitekturne kulture
[@rotar1981pomeni; @rotar1985risarji]. Tam opisuje dve skupini bralcev
njegovih del, oziroma dve skupini arhitektov, pri čemer so eni
»arhitekturni arhitekti« drugi pa teoretiki. [@rotar1985risarji, 7-9]
Glavna razlika je v njihovem pojmovanju arhitektur in mesta. Za prve so
to zgolj estetski objekti, za druge pa torišče raziskovanja
(konceptualno torišče). Drugo ima lastnosti, ki si jih arhitektura kot
umetniško početje in estetski objekt ne more priznati (zato publikacijo
spremlja polemika in nelagodje). Obe pojmovanji pa imata svojo zgodovino
in sta torej obe »zgodovinsko legitimni«:

> [A]rhitektura (mesto) kot estetski objekt lahko obstaja le, če doseže
> tako družbeno priznanje, ki velja za splošno družbeno priznanje. Tako
> priznanje je glavni cilj apologetskega diskurza, ki »spremlja«,
> natančneje, konstituira arhitekturo kot umetnost oziroma kot
> intelektualen poklic, odkar se je v Quattrocentu vzpostavila kot
> avtonomen poklic. Arhitektura kot estetski objekt je potemtakem vselej
> podvojena z apologetskim diskurzom (traktatom, programom, kritiko), ta
> dvojnost pa je tisto, kar omogoča, da se ta koncepcija perpetuira,
> natančneje, omogoča, da se nenehno regenerira. Znanost oziroma teorija
> tej nujnosti konstitucije arhitekture arhitektov ne streže. Zanjo je
> arhitektura realen objekt, se pravi, empirična kategorija, ki jo
> teorija lahko konceptualizira le kot snop ali skupek konceptov, ki so
> teoretsko pertinentni. Spoznanje namreč ni natančno isto kot
> izdelovanje ali občudovanje.
[@rotar1985risarji, 8]

(Takšna struktura »torišča« – arhitekturna disciplina kot praksa
podvojena z »apologetskim diskurzom«, ki jo v tej strukturi »realno«
zapopade zgodovinski in materialistični teoretski pristop – bo z drugimi
oznakami prisotna tudi v Tafurijevih spisih. Tam arhitekturno disciplino
tvorita arhitekturni tehnični diskurz in arhitekturni teoretski diskurz,
ki jo »realneje« zapopade kritična zgodovina te strukture.) [gl.
@tafuri1980theories; @tafuri1968teorie; prav tako gl. »renesančna
problematika« pod Tafurijevim geslom -@tafuri1969rinascimento, 223 in
dalje]

Dalje Rotar razliko opiše kot razliko v vprašanjih, ki si jih skupini
sploh lahko zastavita. Arhitekturi kot formi družbene institucije,
pravi, pripada kod družbenega obnašanja, ki velja tudi kot vrednota, kot
nekaj samoumevnega, univerzalnega, od narave ali od boga danega. Da je
sploh možno postaviti vprašanje o naravi in zgodovinski izdelavi tega
koda, ali pa vprašanje o drugih družbenih interesih, ki se skozi te
forme uveljavljajo, pomeni zanikati to vrednoto. Zaradi tega med
arhitekturnim arhitektom in teoretikom »ni mogoč takšen dialog, v
katerem bi lahko obe strani ostali na svojem zemljišču – eden ali drugi
se mora odpovedati sami naravi svojega početja.« [@rotar1985risarji, 9]
Poudarili bi, da ne gre za nasprotne interese dveh različnih skupin, ali
za medsebojno izključujočo nesposobnost. Interesi so neprimerljivi in
razmejujejo (torej omejujejo) dva načina delovanja, četudi delujejo na
istem področju. Nujnost enega in drugega je ravno v omejenosti
nasprotnega, ki se razkrije v kriznih situacijah. Tako razumemo, če
nadaljujemo z »naštevanjem« distinktivno marksovskih prispevkov k
arhitekturni problematiki, tudi Adornov poziv k začasni suspenziji
»tehnične pristojnosti«:

> Vendar se mi ne zdi popolnoma nemogoče, da bi ne bilo s časa na čas –
> v latentnih kriznih situacijah – vendarle tudi dobro, da zavzamemo do
> fenomenov večjo distanco, kot bi jo bil pripravljen dopuščati patos
> tehnične pristojnosti. Ustrezanje materialu (*Materialgerechtigkeit*)
> temelji na delitvi dela, s tem pa je priporočljivo, da tudi izvedenec
> od časa do časa položi račun o tem, v kolikšni meri njegovo
> izvedenstvo trpi zaradi delitve dela, v kolikšni meri lahko potrebna
> umetniška naivnost postane svoja lastna pregrada.«
[@adorno2020funkcionalizem, 146]

Med seboj različni avtorji znotraj marksovske tradicije, ločeno drug od
drugega, teoretizirajo o »epistemoloških rezih«, kritičnih
(distanciranih) obravnavah, ki naj suspendirajo omejitve delitve dela,
tudi o revolucijah v mišljenju (torej o presekih, prevratih,
prekinitvah), pravzaprav o nujnosti *teoretskega* pristopa tam kjer
prevladujejo spontane ali praktične skrbi. Povsem verjetno zato, ker
takšno vlogo zgodovinsko igra marksovska kritika do svojega predmeta in
ki jo marksovska kritika tudi postulira kot nujno v obravnavi tega
predmeta. Tu zasledujemo kar najbolj sekularno opredelitev te kritike,
opredelitev za katero menimo, da bi bila nepopolna v kontekstih v katere
je njen namen poseči – to je politični kontekst in končno tudi
zgodovinski –, a znotraj naloge, ki ima tudi omejen namen zgolj pokazati
osnovno edinstvenost in nezvedljivost doprinosa te kritike – kar se tiče
obravnave vloge in položaja arhitekturnega dela v kapitalističnem
razvoju –, je takšna omejena opredelitev, menimo, bolj primerna.
Pomagali bi si s tezo Michaela Heinricha o štirih rezih Marxove kritike
politične ekonomije s teoretskim poljem klasične politične ekonomije. Ne
le zato, ker gre za kakovosten jedrnat povzetek Marxove misli, ampak
tudi, ker je teza bila razvita znotraj splošnega teoretskega momenta, ki
je soroden in podoben Tafurijevemu: gre za različna ponovna branja
Marxa, ki so se po Madžarski revoluciji leta 1956 pojavila v Zahodni
Evropi izven uradnih komunističnih strank. Natančneje gre pri Heinrichu
za »novo branje Marxa« (*neue Marx-Lektüre*), za specifično recepcijo in
reinterpretacijo Marxa, ki se je razvila v Nemčiji v šestdesetih letih
20\. stoletja in katere danes najvidnejši predstavnik je prav on. Novo
branje se osredotoča na pojasnjevanje osnovnih pojmov in na
rekonstrukcijo Marxove kritike v polemiki s tistimi branji, ki to
kritiko pojmujejo kot pozitivno ekonomsko znanost. Ta polemika je tudi
posredno povzeta v morda najslavnejšem Tafurijevem citatu, kjer je tudi
podlaga glavni tezi:

> Mi se raje sprašujemo, kako to, da je marksistično inspirirana kultura
> vse do sedaj z ekstremno pozornostjo in vztrajnostjo, ki je vredna
> česa boljšega, zanikala ali z občutkom krivde zakrivala enostavno
> resnico: tako kot ne more obstajati razredna Politična ekonomija,
> ampak samo razredna kritika Politične ekonomije, tako se ne more
> utemeljiti razredne estetike, umetnosti, arhitekture, ampak samo
> razredno kritiko estetike, umetnosti, arhitekture, mesta.
[@tafuri1985projekt, 114]

To tezo, v kolikor se nanaša specifično na arhitekturo, bomo obravnavali
kasneje, tu pa jo navajamo za prikaz, da je izpeljana, oziroma povezana,
iz osnovne marksovske teoretske operacije do politične ekonomije.
Natančneje se bomo v pogoje in značilnosti reinterpretacije Marxa, ki
zaznamovala Tafurijevo misel, poglobili v naslednjih poglavjih, ko bo ta
podlaga razdelana za namene natančnejšega razumevanja tafurijanske
kritike arhitekturne ideologije in, predvsem, njene distiktivnosti. Če
se vrnemo k Heinrichovi tezi o Marxovih štirih rezih, gre za: rez z
antropologizmom, z individualizmom, z empirizmom in z ahistorizmom.
[povzemamo po @furlan2013michael, 270-277]

Vsi štirje so rezi s temeljnimi predpostavkami klasične politične
ekonomije, ki imajo svojo vlogo tudi kot ideologije. Antropologizem
temelji na ideji nadzgodovinskega človeškega bistva: takšni »posamezni
in oposameznjeni« Robinzoni so izhodišča političnih ekonomistov kot sta
Smith in Ricardo. Marx, nasprotno, trdi, da je to ideal, ki je možen le
(kot ideal) v zgodovinskem kontekstu družbe svobodne konkurence, ki je
nastopila po razkroju fevdalnih družbenih form. Antropologizem je torej
projekcija idealov človeka na zgodovino, postavljanje tega ideala kot
izhodišča zgodovine, ne kot historični rezultat. [@marx1985uvod, 19]
Drugi rez, prelom z (metodološkim) individualizmom, je zgoščen v Marxovi
tezi, da »družba ne sestoji iz individuov, ampak izraža vsoto odnosov,
razmerij, v katerih so ti individui drug do drugega.« [@marx1985ocrti,
159] Spet, v nasprotju z, na primer, Smithom in Ricardom, ki
družbenoekonomske pojave poskušata deducirati iz opazovanja posameznika,
Marx tem pojavom pristopi iz obratne smeri. S tem metodološkim obratom
lahko Marx osnovne kategorije politične ekonomije obravnava kot družbeno
nujne, torej hkrati so zgodovinske, se spreminjajo, in hkrati se zdijo
objektivne. Rez z empirizmom je rez s tezo, da je čutno izkustvo edini
vir vednosti. Ta rez je v arhitekturni teoriji in teoriji (kritični
teoriji družbe) morda še najbolj posplošen. Spoznavni predmet kritike
politične ekonomije ni prosojen, njegova »dejstva« niso neposredno
dostopna čutni zavesti. To je pravzaprav izhodišče ali predpostavka
kritike ideologije (ali kritične teorije družbe, ki je v takšni ali
drugačni obliki bila in še vedno je zelo zastopana v arhitekturni
teoriji): [Gl. tudi @hays1998architecture:hays, navedeno tudi zgoraj]
družbo je treba spoznati tudi v njeni neistovetnosti, povezano tudi s
prejšnjim rezom, gre za pristop, ki je nujen, ko se posledice povsem
zavestnih, subjektivnih dejanj zdijo objektivne, stvarne. Zadnji rez,
rez z ahistorizmom, je pri Tafuriju morda najbolj očiten, saj ga lahko
zelo očitno primerjamo z njegovo izhodiščno kritiko modernizma in
njegovo poklicno odločitvijo. Ahistorizem, kot predpostavka buržoazne
misli, vse institucije deli na naravne in umetne: institucije fevdalizma
so umetne, buržoazne institucije so naravne, zgodovina potemtakem je
bila, ni je pa več. Zdi se pa tudi, da je to rez, ki ga je še danes
najpomembneje poudariti: ena vztrajnejših značilnosti poznega
kapitalizma je ideologija konca zgodovine in naturalizacija obstoječega
stanja.

Na teh osnovah, ki se v samih besedilih sicer nikoli ne pojavijo tako
sistematizirano ali plastično, menimo, da se razprava o marksovskem delu
opusa Manfreda Tafurija šele lahko produktivno odvije. Z razumevanjem
teh metodoloških ali epistemoloških razmejitev je lažje pristopiti k
aparatu, ki je svoje kategorije razvil znotraj njih, da bi postopoma
razdelali in razumeli distinktivnost njihovega delovanja, ter na podlagi
tega preverili možnosti njihove teoretske aktualizacije. Še zadnjo
splošno marksovsko opombo, ki jo morda terja nenaslovljena različnost
področij politične ekonomije in kulture ali tistega področja človeškega
življenja in zgodovine, ki pač ni politična ekonomija. Res so predmet
najbolj razdelanih Marxovih del strogo političnoekonomske kategorije, a
to zato, ker je organizacija zadoščenja potreb vsakokratne družbe, po
Marxu, osnova, kateri šele ostale oblike zavesti ustrezajo:

> Obči rezultat, do katerega sem prišel in ki je, ko sem ga enkrat imel,
> rabil mojim študijam za vodilo, lahko na kratko formuliram takole: V
> družbeni produkciji svojega življenja stopajo ljudje v določena,
> nujna, od njihove volje neodvisna razmerja, produkcijska razmerja, ki
> ustrezajo neki določeni razvojni stopnji njihovih materialnih
> produktivnih sil. Celokupnost teh produkcijskih razmerij tvori
> ekonomsko strukturo družbe, realno bazo, na kateri se dviga neka
> pravna in politična nadzidava, in kateri ustrezajo določene družbene
> forme zavesti. Produkcijski način materialnega življenja pogojuje
> socialni, politični in duhovni življenjski proces sploh. Ni zavest
> ljudi tista, ki določa njihovo bit, temveč narobe, njihova družbena
> bit je tista, ki določa njihovo zavest.
[@marx1989kritiki, 90]

Poenostavitev odnosa med ekonomsko strukturo in njeno nadzidavo v tem
povzetku drugje ni natančneje razdelana. Težko torej govorimo o
kanonični razdelani Marxovi teoriji o bazi in nadzidavi. Zlahka pa bi na
podlagi štirih vrstic, brez soočenja z natančnejšimi argumenti, odpisali
eno pomembnejših epizod arhitekturne teorije na podlagi očitkov
»determinističnega« razumevanja arhitekture. Odnos med bazo in nadzidavo
natančneje teoretizirajo kasnejši misleci in to počnejo na različne
načine, najživahneje seveda znotraj marksovske tradicije. Tafurijevi
spisi bodo kritično posegli ravno v eno takšnih nadaljevanih in razvitih
pojmovanj te dvojice. Natančnejša zgodovina teh in njihova uporaba
znotraj Tafurijevega miljeja torej ne spada v uvod.

Na teh osnovah lahko prispevamo k različnim strategijam preseganja
Tafurija. Naše zainteresirano branje zasleduje aktualizacijo kritike
arhitekturne ideologije. Menimo pa, da opisujemo tudi splošne možnosti
preseganja: razumevanje marksovskega dela in njegovo delovanje znotraj
Tafurijevega opusa je pogoj tudi za kakšno konzervativno obnovo področja
arhitekture ali angažirano obnovo njene splošne družbene učinkovitosti,
torej za razvitejšo kritiko Tafurija na teoretski ravni, ki je lahko
zgolj delna, saj menimo, da so pogoji preseganja tafurijanske kritike
arhitekturne ideologije v strogem sprejetju njenih teoretskih kategorij.

Glede na uvedeno pa za našo nalogo ustreza naslednja, mestoma že
napovedana, struktura: zgodovinska in teoretska kontekstualizacija ter
izvor natančnejšega epistemološkega in metodološkega aparata kot je ta
bil potreben za osnovne teze tafurijanske kritike plana in ideologije;
na tej podlagi rekonstrukcija argumenta kot se prvič pojavi v reviji
*Contropiano*, da je arhitekturna zavest že strukturirana po shemah, ki
jih posredno narekuje kapitalistični razvoj; demonstracija delovanja teh
shem, to je demonstracija »ideološkosti« arhitekture, tudi v pogojih
postmodernizma, ko prevladujejo ideje o »koncu zgodovine in ideologije«;
ter predlog teoretskega projekta osvetlitve značaja arhitekturne
produkcije in arhitekturnega dela v trenutnem kapitalističnem ciklu.
Zadnjemu poglavju pripisujemo tudi osvetlitev mej arhitekturnega
delovanja, ki navdušeno išče možnost zavestne spremembe sveta, da bi v
teoretskih mejah prek dramatizacije objektivne situacije spodbudili
projektivne dvome v iskanje novih orodij delovanja na višji ravni
zavesti. Na tem mestu naj malo historiziramo tudi lasten položaj in
pripomnimo, da s tem predlogom ne impliciramo, da gre za izčrpen
preizkus najproduktivnejšega pristopa. Možno, da je najproduktivnejši
način preseganja usodne kritike arhitekturne ideologije v kakšnem bolj
pragmatičnem početju – za arhitekturo kot disciplino je misliti to
možnost bistveno – in da je naša teoretska usmeritev bolj soočenje s
subjektivnimi, ne objektivnimi preprekami arhitekturnega poklica v
kapitalizmu. A statistično je to manj verjetno. Kakor koli pa, v luči
tega pogrešanega pragmatizma, je projektantski obrat za to nalogo že
zamujen, ker se je že začela.

# Arhitektura in plan kapitala

> Neogibni pogoj za znosen položaj delavca *je torej kar najhitrejše
> naraščanje produktivnega kapitala*.
> 
> – K\. Marx, *Mezdno delo in kapital*, 1949

> Kaj pa je naraščanje produktivnega kapitala? Naraščanje moči
> nakopičenega dela nad živim delom. Naraščanje gospostva buržoazije nad
> delavskim razredom.
> 
> – K\. Marx, prav tam

Vsako novejše zainteresirano branje Tafurija, ki izpostavi njegovo mesto
v (pozabljeni) intelektualni zgodovini italijanskega disidentskega
marksizma šestdesetih let 20\. stoletja – tako imenovanega *operaizma* –
vsaj do neke mere dolguje Pier Vittorio Aurelijevem eksplicitnem pozivu
k takšni rekontekstualizaciji, [@aureli2010recontextualizing] ali pa
njegovi implicitni uporabi te dediščine operaizma pri teoretiziranju
njegove dobro poznane arhitekturne drže. [@aureli2008project] Predlog
rekontekstualizacije besedil v njihove bolj neposredne miselne tokove v
njihovem specifičnem zgodovinskem okolju je kvalitativen preskok po
desetletjih poenostavljenega mapiranja tafurijanskega doprinosa kot
zgolj pojmovnega samorazvoja nekje med Althusserjem in Foucaultom.
Vseeno pa ta rekontekstualizacija z Aurelijem še ni izvršena ali
ustrezno izvedena na podlagi vsaj dveh točk. Operacionalizacija
operaizma, oziroma njegove intelektualne dediščine, v namene
arhitekturne avtonomije, menimo, nima ustrezne podlage v gradivu na
katerega se sklicuje. Celovitejši pregledi tega dela zgodovine
italijanskega marksizma so zadržani do povzdigovanja enotnih konceptov,
ki bi lahko zajemali *ves* razvoj operaizma (pravzaprav gre zgolj za
prepoznavanje skupnih sestavin širokega teoretskega kontinenta), če pa
že morajo opraviti takšno poenostavitev, pa se odločijo za operaistični
koncept *razredne sestave*. [@wright2002storming, 4] S tem bi se tudi
sami strinjali, saj je začetni operaistični vzgib v začetku šestdesetih
let analiza delavskih veščin in subjektivnosti v luči tovarniške
modernizacije, eden ključnih trenutkov takoj po koncu »klasičnega«
operaizma je Tafurijev prispevek s konca šestdesetih let o razvoju vlog
intelektualnega dela v luči kapitalističnega razvoja (racionalizacije),
eden zadnjih (post)operaističnih konceptov v sedemdesetih letih pa so
prve teorije prekarizacije v luči informacijskih tehnologij. Drugo točko
pa prej razumemo kot komplementarno, ne izpodbijajočo, jedru Aurelijeve
rekontekstualizacije. Zgodovina operaizma ni prosojna, je večkrat
razcepljena, pozabljena in različno povzeta. Rekontektualizacija
Tafurija kot dejavnega znotraj in ob operaizmu torej ni dovolj, saj pove
zelo malo, oziroma ne razreši napetosti, ki kljub kategorizaciji
ostajajo. V ta namen je potrebna tudi kritična obnova operaizma in
pregled (še obstoječih in dejavnih) posledic njegovih epistemoloških
novosti znotraj marksizma; Aurelijev povzetek tega konteksta je lahko
dobrodošel začetek, v svoji osnovni obliki pa še ne zajema specifične
polemike operaizma do takratne marksistične tradicije in specifične
tafurijanske polemike do operaizma. Dodatno natančnost razdelave pomena
in odnosov operaistično razvitih konceptov znotraj tafurijanske kritike
ideologije terja tudi, podobno kot pri angloameriškem
arhitekturnoteoretskem sprejemu, delno in nedosledno prehajanje te
intelektualne tradicije v angloameriško akademijo, kjer koncepti
ponavadi šele dobijo svoj splošno priznani pomen, ki je posredovan in
odvisen od specifičnega interesa te akademije. Sklicevanje na operaizmo
na sploh in kot na teoretsko ozadje ključnih člankov kritike
arhitekturne ideologije zato lahko zdrsne v sklicevanje na njegovo
romantizirano ali neprizanesljivo karikaturo. Strategij kako enotno in
enostavno povzeti ves operaizem je več. Najmanj informirano, a v sili
tudi ustrezno, saj karikature pač imajo svoje racionalno jedro, se
povzame z dobesedno angleško inačico *workerism*, ki se ponavadi
uporablja kot slabšalna oznaka za vse teoretske nazore, ki tovarniškemu
delavcu pripisujejo večji (revolucionarni) pomen kot ga razumno zasluži,
ali pa ta pomen pripisuje na škodo ostalim podjarmljenim elementom
družbe. Drugačni povzetki se osredotočajo na posamične avtorje, kar
pomeni, da so znotraj istega pojma sploščeni dramatično raznoliki
pristopi kot se te pojavljajo v življenjskem delu nekaterih
marksističnih teoretikov. Primer tega je osredotočanje na Tonija
Negrija. Pozornost na njegov prispevek k operaizmu so izzvala tudi
njegova poznejša dela s sodelavcem Michaelom Hardtom, [gl.
@hardt2000empire] ki niso nujno skladna z zgodnejšimi ali pa je misel
nadaljevana do mere, ko ni več značilna za tudi za obdobje, ki ga
zasledujemo orisati. [Tu mislimo na prispevke v revijah, ki jih še bomo
obravnavali. Na tem mestu naj omenimo, da slovenski prevodi nekaterih
njegovih del nastopijo v istih okvirjih (študentsko oporečništvo in
*Zveza socialistične mladine Slovenije*) kot slovenski prevod *Projekta
in utopije*, gl. @negri1984delavci]. Drug primer bi bil Mario Tronti
katerega knjiga, označena za temeljno delo operaizma, se v angleškem
prevodu pojavi več kot pol stoletja za izvirnikom [gl.
@tronti2018workers]. K novi izdaji je dodano avtorjevo pričevanje in
osebno pojmovanje operaizma [@tronti2019our] kjer izrazi nujno
parcialnost pojmovanja, ki prej izključuje različne odvode gibanja kot
pa jih povzema. Najbolj celovito, vsaj v angloameriškem kulturnem
območju, razlago konteksta in vsebine operaizmov poda Steve Wright
[-@wright2002storming; Gl. tudi @wright2021weight kjer je v središču
revijalno življenje te struje], ki tudi, menimo, ustrezno osredini
njihove notranje napetosti in razlike.

Naša strategija povzemanja bo sledila zgodovinskim pogojem v katerih je
operaizmo kot metodološki in epistemološki teoretski projekt nastal: v
najbolj osnovnem pomenu je »tako imenovani *operaizmo* poskus
političnega odziva na krizo delavskega gibanja v petdesetih letih.«
[@negri1979operaio, 31] Lotili bi se torej obnove tega zgodovinskega
ozadja, postopno in (zgolj) do točke, ko nastopi *Contropiano*.
Osredotočali bi se na teoretske pojme, ki značilno nastopijo tudi
znotraj tafurijanske kritike arhitekturne ideologije, razvili pa so se
znotraj marksistične razprave, ki je skušala zapopasti nove stopnje
kapitalističnega razvoja v povojni Italiji in kritizirati neustreznost
njegovih ustaljenih političnih reprezentacij. Časovno se torej omejujemo
na obdobje petdesetih in šestdesetih let 20\. stoletja,
intelektualno-zgodovinsko pa na obdobje revij *Quaderni rossi*, *Classe
operaia* in končno *Contropiano*, ko je sam izraz operaizmo lahko že
neprimeren.^[Strogo operaistična, v smislu samega izvora pojma, je zgolj
*Classe operaia*, ki ni teoretska publikacija in zato niti ni poučna za
namene teoretskega razvoja kritike arhitekturne ideologije. Revija
*Quaderni rossi* nastopi pred njo in jo tako smatramo kot »klasični
operaizmo« kjer so osnovne teoretske značilnosti operaizma, ki bodo,
menimo, odločilno informirale Tafurijev teoretski razvoj, prvič
razdelane. *Contropiano* pa že označuje ponoven teoretski umik in
refleksijo operaizma, ki pa se kot politično gibanje razvija ločeno, kot
*avtonomizmo*, in več ne informira teoretskega razvoja, ki ga tu
zasledujemo.]

Leta tik po vojni in petdeseta leta v Italiji, tako imenovano obdobje
rekonstrukcije, so bila obdobje temeljnih ekonomskih in družbenih
sprememb: od gospodarskega kaosa in množične podhranjenosti
prebivalstva, do večkratnega vrednostnega in gmotnega povečanja
proizvodnje in potrošnje. Gospodarski čudež, razvoj sekundarne
industrije, je gonilo priseljevanje delovne sile iz juga države, kjer jo
je pred tem sprostila agrarna reforma. Ni pa ta rast bila povsem
linearna ali brez ovir. Četudi se je večina fiksnega kapitala iz vojnega
obdobja ohranila, je *Resistenza*, široka koalicija oboroženega
antifašističnega odpora pod vodstvom Italijanske komunistične partije
(PCI), uvedla nekatere nove načine ravnanja z njim: starejši severni
industrijski delavci so med vojno nekatere tovarne zasegli in izborjene
upravne vloge tudi ohranili v novi republiki, posledično pa so lahko
svoje politične zahteve uveljavljali tudi izven tovarn. Iz stališča
kapitala je bila glavna ovira pred nemoteno rekonstrukcijo nepokornost
severne tovarniške delovne sile [@wright2002storming, 6-7; glej tudi
@ginsborg1990history, 72-73]. In PCI, takrat druga največja stranka v
Italiji, z največjim vplivom na prej omenjenih samoupravnih delovnih
mestih, in največja komunistična stranka na zahodu, s Palmirom
Togliattijem na čelu je bila, iz previdnosti zaradi grškega scenarija
ter zaradi občih parlamentarnih političnih ciljev v tej rekonstrukciji
pripravljena konstruktivno sodelovati. Uradna politika PCI je bila torej
medrazredno sodelovanje pri industrializaciji in modernizaciji Italije,
kar je dejansko pomenilo pripravljenost uveljavljanja miru med delavci v
severnih tovarnah ter ponovne utrditve menedžerske hierarhije in
pripravljenost italijanskemu kapitalu omogočiti želeno rast. Stališče
PCI o razvoju se tako ni kaj razlikovalo od stališča političnih
predstavnikov kapitala: *razvoj* je politično oziroma razredno nevtralen
in višja produktivnost, ki jo prinaša, je osvobajajoča sila. Kljub
iskrenim naporom pri krotenju lastne baze pa so leve stranke bile
izključene iz parlamenta, nenazadnje so ZDA s tem pogojevale razvojne
investicije in denarno pomoč *Marshallovega načrta*. Z zlomljenim
vplivom na delovnih mestih, odtujenimi sindikati in izobčenimi
delavskimi strankami iz parlamenta bo imel kapital prosto pot, da družbo
oblikuje po svoji podobi.

In temu razvoju arhitekturna problematika ni vzporedna, ravno nasprotno:
povojna obnova, odnos intelektualne avantgarde do delovnih razredov in
objektivne zahteve kapitala bodo v središču arhitekturne problematike
ter obratno, arhitekturna problematika, bolj izrazito pod stanovanjskimi
in sociološkimi znaki, bo v središču evropske povojne razvojne
problematike.

Kar se tiče demonstracije, da so parametri razrednega boja ključni za
razumevanje Tafurijeve zgodovinarske zavzetosti, lahko navedemo več
virov. Primarni vir bi seveda moralo biti že omenjeno dejstvo, da je
serija najbolj prepoznavnih spisov nastala v tesnem sodelovanju znotraj
marksovskega projekta, ki kritizira stanje in usmeritev uradnega
delavskega in komunističnega gibanja. Drugi je sama vsebina teh in
ostalih spisov, kjer je na delu analiza in kritika iz »strogega a
parcialnega marksističnega stališča«. A kljub eksplicitnem izražanju
jukstapozicija analize zgodovinskih avantgard – na primer Weimarskega in
avstromarksističnega arhitekturnega reformizma in populizma – s povojnim
arhitekturnim razvojem znotraj arhitekturne teorije običajno ni
natančneje razdelana, ko se poskuša povzemati ali obnavljati
tafurijanski projekt. V ta namen je zelo uporabna Tafurijeva zgodovina
povojne italijanske arhitekture. [@tafuri1989history; @tafuri2002storia]
Prva oblika besedila nastane 1982 za serijo zgodovine italijanske
umetnosti, kasneje (prvič 1986) je razširjena in izdana samostojno. V
tem delu, za katerega bi lahko rekli, da ne sodi več v avtorjevo
izrazito »marksovsko obdobje« (knjiga je naslovljena splošni strokovni
javnosti, oziroma ne nastaja znotraj izrecno marksistične publicistike),
so izpostavljene teme povojnega arhitekturnega razvoja, ki, lahko
sklepamo, so avtorju obče ključne za razumevanje arhitekture in ki,
menimo, so povzetki ali ponovna razdelava tistih tez, ki so bile
namensko preverjene na zgodovinskih primerih kritike ideologije
arhitekture. Za dodatno razumevanje je zanimiva tudi zbirka Tafurijevih
zgodnjih zapisov, [@tafuri2022progetto] ki so nekakšen sproten
prvoosebni zapis tega obdobja, a ga še razlagajo in vrednotijo na enak
način kot kasneje napisana zgodovina. [Zanimivo je, da Tafuri sam sebe v
*Zgodovini italijanske arhitekture* nikoli ne omeni, čeprav je, kot
arhitekt ali avtor, v njej neposredno udeležen. O »operativnosti« te
»iluzije« gl. @leach2002everything] Predmet tafurijanske kritike
arhitekturne ideologije torej ni abstrakten ali oddaljen proces ali
zgolj interpretacija zgodovinskih avantgard, temveč v prvem planu
(sodobne) možnosti in omejitve angažiranega intelektualnega dela, v
drugem torej razvoj tega dela znotraj kapitalističnega razvoja in torej
struktura reprezentacij te intelektualne prakse do zgodovine.

V letih rekonstrukcije bodo arhitekti, primorani poiskati lastno
identiteto utemeljeno v novi realnosti, svoji disciplini nadeli serijo
ideologij: soočenje arhitekture z aktivno politiko se je zdelo nujno. Na
etični ravni bo šlo za udejanjanje vrednot *Resistenze*, na tisti ravni
kjer arhitektura vstopa v najsplošnejše družbene procese pa za poziv k
novem nacionalnem planu. Sledijo prve, čisto formalne vaje – na primer
plani racionalizacije podeželja v namene boljše porazdelitve
prebivalstva in turizma ter plan za Milano in njegovo širše območje, ki
ga razdela CIAM, ki zasleduje antišpekulativne cilje –, ki ostanejo
ravno to: čisto formalne vaje, saj so predlagane kot arhitekturne
izvedbe samostojnih ekonomskih in političnih odločitev, ne pa kot sama
ekonomska in politična dejavnost:

> Italijanski urbanisti, soočeni s problemom rekonstrukcije, so lastno
> disciplinarno tradicijo vztrajno povezovali s politično-ekonomskimi
> odločitvami, ki so predlagane »same na sebi.« Bolj kot »nadomeščanje«
> je njihovo delo ostalo »simulacija«.
[@tafuri2002storia, 9]

V tem obdobju Tafuri prepozna čisto arhitekturni projekt nekakšne
redefinicije ekonomskih problemov v terminih teritorialne organizacije.
To je pogoj za razumevanje arhitekture kot tiste discipline, ki
pravzaprav predlaga obliko družbenega reda, torej za razumevanje
arhitekture kot ideološke prakse. Neustreznost formalnih vaj –
nezapopadljivost podeželja s strani plana ter neučinkovitost statičnih
mestnih planov iz vidika dinamičnih zahtev povojne ekonomije – je
arhitekte silila svoja prizadevanja po novem družbenem redu izraziti v
oblikovnih, formalnih sporočilih. Druga odprta fronta, ki jo je bojevala
arhitekturna skupnost, pa je bila notranja: arhitekti so morali
odpraviti tudi s svojimi starimi tradicijami in institucijami, ki so jih
prej vezale na prejšnji red, kot pa približevale oblikovanju novega. To
je nekakšno arheološko polje arhitekturnega *neorealizma*, kot ga oceni
Tafuri: nejasna politična opredelitev, ki stremi profesionalno
arhitekturno usodo zvezati s prevladujočim širšim kulturnim in
političnim projektom.

Ključna praksa bo zgoščena okoli osebnosti arhitektov Maria Ridolfija in
Ludovica Quaronija, oziroma njunih socialnih stanovanjskih projektov.
Njihovi principi so bili najprej razdelani kot teoretske tipološke
študije in arhitekturni priročniki, kasneje pa uresničeni in preverjeni
znotraj programa *INA-Casa*, državne sheme za gradnjo stanovanj, ki je
imela prednostni cilj tudi vpiti čim večji delež nezaposlene populacije
– modernizacija javnega gradbenega sektorja je zato namenoma zavrta. V
teh pogojih se arhitekturni neorealizem na formalni ravni razvije kot
populistična arhitekturna smer, ki svoje revne materialne pogoje
povzdigne v realistično estetiko, ki se ljudstvu želi približati s
povzemanjem njihovih spontanih zavračanj ponavljanja in uniformnosti
značilnih za industrijsko estetiko. To soočenje med intelektualci in
ljudstvom Tafuri označi za »avtobiografsko«:

> Ti intelektualci so zavzeli sartrovsko stališče: izbrali so
> identificirati usodo svoje tehnike in njenega jezika z usodo razredov,
> ki so se nenadoma pojavili v ospredju, bogati z »izgubljeno«
> preteklostjo, a prežeti z vrednotami, če so jim te omogočale
> profilirati se kot nosilci nove »čistosti«. Malo pomembno je bilo, če
> je ta zraslost preveč spominjala na katarzično kopel, če je
> raziskovanje teh tradicij zakrivalo mazohistično potrebo po
> identifikaciji s poraženci, če je iskanje korenin v kmečkem ognjišču
> odstranjevalo tesnobo zaradi dezorientiranosti, ki se jo doživlja ob
> stiku z množično družbo. Niti niso znali oceniti, da jih je z
> razmišljanjem, da bodo delovali kot kralji modreci ter da bodo
> novoizvoljenim podarili lasten *engagement*, dobesedno nagovoril načrt
> znotraj katerega so nezavedno postajali ubogljivo orodje.
[@tafuri2002storia, 15-16)]

Avtobiografsko torej zato, ker Tafuri neorealizem – realistično poetiko
»ponosne skromnosti«, ki se lahko sporazumeva s preprostim ljudstvom –
oceni kot arhitekturno usmeritev, katere namen je, še preden se izkaže
za nesposobno uresničiti lasten cilj najprej zgraditi in potem
oplemenititi preprosto življenje svojih uporabnikov, pomiriti
arhitektovo tesnobo pred izgubo lastnega smisla in vloge v stiku z
»množično družbo«. Intelektualna praksa, samorazumljena kot zavestno
upravljanje s planom (kot subjekt plana), se prav tako izkaže za objekt
plana. Samospraševanje in implicirano samorazumevanje intelektualnih
poklicev glede svoje vloge *do* družbe še danes uokvirja ustaljene
razlage arhitekturne zgodovine. Pretresi znotraj arhitekturne kulture
(na primer konec modernizma in začetek postmodernizma) so opredeljeni
kot projekti propadli na ravni zadoščanja abstraktnim željam po
samoizpolnjevanju (bolj konkretne razlage gredo korak nazaj in propad
modernizma pripišejo njegovi dozdevni nenaklonjenosti barvam, erotiki in
variaciji v množičnih stanovanjih – torej propad zaradi potrošniške
izbire –, a onkraj popularizacije barvne fotografije se pomanjkljivosti
modernizma v splošnem obsegu še ni naslovilo), napredek arhitekture bi
po takem pojmovanju bil odvisen od zasebne inventivnosti arhitektov v
pogojih njihove splošne naveličanosti na ravni formalnega snovanja.

A razočaranje na področju zidave je bolj konkretno, družbeno odločilno
in izmerljivo kot bi disciplina s samopripisanim kar najširšim družbenim
pomenom dopuščala misliti.

Povojna nagla gospodarska rast se je izkazovala tudi na področju zidave,
na eni strani kot katastrofa v odnosu do naravne krajine in historičnih
mestnih središč – predvsem zaradi turizma, ki se je uveljavil kot varna
nacionalna ekonomska usmeritev, in zemljiške špekulacije –, na drugi, a
vendar povezano, kot katastrofa v odnosu do življenjskega standarda v
kolikor je ta odvisen od kakovosti in varnosti novogradenj ter gostote
in opremljenosti novih naselij. Povsem znano stanje modernosti, ki sega
skozi dovolj različnih zgodovinskih in geografskih enot, da ga ne moremo
esencializirati: izhaja iz natančnih zgodovinskih in političnih pogojev
zakonsko zagotovljene oziroma državno varovane najvišje stopnje svobode
zasebnim investitorjem, ki je pogoj tako pojmovane gospodarske rasti.
[@ginsborg1990history, 246-247] Kot ukrep proti spremljajoči rasti
brezposelnosti (Tafuri poroča o rasti od milijon in pol leta 1946 do
dveh milijonov brezposelnih leta 1948) [@tafuri2002storia] in
naraščajočim razlikam v razvoju med severom in jugom je leta 1949
ustanovljena že omenjena *INA-Casa*, javna služba z namenom *povečanja
delovnih mest in zagotavljanja gradnje delavskih stanovanj*. Nameni
*plana*, javnega oziroma državnega posredovanja v gradbeni in
stanovanjski sektor, so jasno torej zaustavitev rasti brezposelnosti,
podreditev gradbenega sektorja gonilnim sektorjem, njegova ohranitev na
predindustrijski ravni in vezava na razvoj malih podjetij. S tem čim
dlje stabilizirati nihajoči del delavskega razreda, ki ga je mogoče
izsiljevati, a ne pomasoviti, ter iz javnega službe napraviti podporo
zasebnemu posredovanju. [@tafuri2002storia, 22] Teoretske študije in
razdelave predstavnikov arhitekturnega neorelizma s temi cilji, po
Tafuriju, vstopijo »posebno sožitje«: »slavljenje obrtništva, lokalizma,
ročnega dela, pa tudi vztrajanje pri organskosti naselij, ki so – tako
idealno kot prostorsko – oddaljena od „mesta kompromisov“, so
priviligirane sestavine neorealistične poetike in prvih sedmih let
INA-Casa.« [@tafuri2002storia, 22-23]

A že na urbanistični ravni ta javna stanovanjska politika arhitekturni
vesti predstavi izziv. Umikanje javnih gradbenih projektov stran od
urbanih središč, da bi izkoristili ugodnejše cene zemljišč, okoli novih
sosesk spodbudi zasebne investicije in tako javno financirana
infrastruktura postane dodatno gonilo izhodiščni katastrofi, ki naj bi
jo naslavljala.

Šolski primer arhitekturnega neorealizma je stanovanjska soseska
*INA-Casa Tiburtino* v Rimu, ki sta ga, med drugimi, načrtovala oba
glavna predstavnika, Quaroni in Ridolfi. Na tem projektu so preverjene
vse formalne sestavine neorealizma: oddaljenost od mesta, antiformalizem
oziroma izrazit realizem estetike revnih materialov, posnemanje
tradicionalnih nacionalnih motivov v elementih in opremi, posnemanje
ruralne spontanosti z zavračanjem geometrijske mreže. A ljudsko izročilo
povzdignjeno v oblikovni material, ki je potem poljubno uporabljen kot
zasebna intelektualna odločitev, samih arhitektov ni prepričalo, da je
realizem lahko formalna izbira, temveč je lahko zgolj vsiljeno stanje.
Pri analizi tega projekta Tafuri spet poudari avtobiografsko dejanje
prevajanja arhitekturnega »stanja duha« v opeke, plošče in nekakovosten
omet. [@tafuri2002storia, 25] Arhitekturna družbena zavzetost do
uporabnikov in antiformalistična subverzivnost pa je učinkovala
nasprotno od njenih dobrih namenov:

> Jasno je namreč, da ves subverzivni naboj, ki izhaja iz antiformalizma
> Tiburtina, iz tega spomenika negotovi meji, ki ločuje razočaranje in
> *engagement*, vsebuje, paradoksalno, »veliki da« izrečen silam, ki iz
> izolacije socialnih stanovanj napravijo spodbudo za špekulacijo, iz
> tehnološke zaostalosti instrument razvoja naprednih sektorjev, iz
> elokvence motiv stabilizacije.
[@tafuri2002storia, 25]

Kljub občutnem razočaranju je sklep arhitektov lahko le realpolitično
sodelovanje in iskanje prizanesljivejših centrov moči. Eden teh se je
ponujal pod mecenstvom tovarnarja Adriana Olivettija oziroma njegove
levosredinske stranke *Comunità*. Olivetti je že pred vojno snoval
hipotetične projekte racionalno urejenih mest, koncipiranih okoli
trgovine. Kot lastnik enega večjih in bolj dinamičnih podjetij je
interese delil z levimi političnimi izbirami – državno poseganje je v
enem delu zasebnega gospodarstva (FIAT, Pirelli, Olivetti) spodbujalo
rast, prav tako je prisotnost socialistov v parlamentu pomirjala
napetosti na tovarniških tleh na severu države [@ginsborg1990history,
264-265] – in kot idejni (in finančni) pobudnik »kapitalizma s človeškim
obrazom« z napredno inteligenco. Tovarnar je svoje politično delovanje
usmerjal tudi v tako imenovano južno vprašanje in stanovanjsko
problematiko. Kot funkcionar v *UNRRA-Casas*, stanovanjski sklad, ki je
sredstva prejemal od sklada Združenih narodov za povojno obnovo, se je
lotil »sramote Italije« *Sassi di Matera*, naselja jamskih bivališč na
jugu države. Pri najbolj »literarnem« primeru
arhitekturno-urbanističnega naslavljanja južnega vprašanja, projekt
novega naselja *La Martella*, je prav tako sodeloval *Quaroni* z iztekom
v podobnem razočaranju. Osnovni cilj premestitve jamskih stanovalcev v
nova naselja višjih in sodobnejših standardov, poroča Tafuri,
[@tafuri2002storia, 33-36] ni bil dosežen že zaradi posredovanja
lokalnih političnih institucij, ki so jame evakuirale, preprečile pa
zemljiško reformo, ki bi omogočila njihovo nadomeščanje v zadostnem
obsegu. Tako jasno ni dosežen niti višji cilj nasloviti neenakosti v
razvoju med jugom in severom: neenakost se zaostri in vloga juga kot
(zgolj) agrarnega še bolj utrdi. Takšen status juga in njegovih novih
naselij, ugotavlja Tafuri, najde svoje funkcionalno mesto v
kapitalističnem razvoju:

> Primer Matera, ki mu je italijanska kultura posvetila toliko
> pozornosti, zagotovo ni najresnejši primer nacionalne nerazvitosti: je
> pa zagotovo najbolj »literaren«, kar opravičuje osredotočenost
> zanimanja. V resnici so v južnih mestih kot so Neapelj, Bari ali
> Palermo javna dela in gradnja služila kot sredstvo za obvladovanje
> brezposelnosti in kot instrument začetnega usposabljanja kmečkega
> razreda ter njegovega usmerjanja v priseljevanje v razvite regije, da
> bi tam oblikovali rezervno armado za ohranjanje nizke ravni plač.
>
> Soočena s tem temeljnim planom arhitekturna in urbanistična kultura ni
> imela ustreznega orožja in sklicevanje na stranke levice – sicer
> splošno in sumljivo – ga prav tako ni moglo zagotoviti. Povsod kjer so
> arhitekti poskušali uporabiti lastno tehniko pri preoblikovanju
> struktur se znajdejo v težavnem šahu: mesta in obmestja so prizorišča
> najbolj nebrzdanih špekulacij kot postranska posledica neoliberalnih
> politik, ki jih vsiljujejo centri moči.
[@tafuri2002storia, 35-36]

Izkušnjo rekonstrukcije in prvih sedem let *INA-Casa*, obdobje
arhitekturnega stremljenje k realnosti, Tafuri povzame kot obkolitev
sosesk s strani špekulativnega mesta – kar je »predviden in izračunan
pojav« – ter ugotavlja, da arhitekturnemu oblikovanju ni uspelo izvesti
niti »otokov realizirane utopije«. Realizem, zaključi, se izkaže za to
kar v resnici je: produkt nekoristnega kompromisa. [@tafuri2002storia,
46]

Vsi poskusi redefinicije in razrešitve ekonomskega problema kot
arhitekturnega projekta so zavreti in vsakič zavora namiguje na zahteve,
ki izhajajo iz notranjosti tovarne, kjer se je med tem *Olivetti*, na
primeru lastne tovarne v *Ivrei*, osredotočal na preoblikovanje
delovnega okolja z uvajanjem parcelacije dela in kasneje s pospešeno
mehanizacijo. Tu ni šlo za alternativni center moči, kot ga je tovarnar
predstavljal kot javni funkcionar v *UNRRA-Casas*: njegove teorije
skupnosti in humanistične podobe produkcije naj bi, nasprotno od
nejasnosti javnega posredovanja, bile uresničljive pod pogoji
podjetnikovega jasnega razsvetljenstva ter na njegovi lasti kot zasebno
posredovanje.

Zavezništvo med Olivettijevimi kulturno-menedžerskimi politikami in
podobo, ki so jo ponudili arhitekti, povzema Tafuri, je vzdržalo na
ravni izdelkov, na ravni planiranja pa ne. [@tafuri2002storia 47] V
*Ivrei* so se kakovostna arhitekturna dela, brez vnaprej določenega
jezika, kopičila kot zbirateljski predmeti: od mednarodnega sloga, bolj
sproščenih geometrizmov rombov in šest-kotnikov, do organske
arhitekture. Pomenska pa je posamezna uporaba arhitekturnega jezika v
razmerju do funkcij, ki jih te prostori privzemajo znotraj procesa
produkcije in reprodukcije:

> »[D]obra oblika« je tam zato, da zabriše vsako razliko, da pokaže, da
> tiste, ki želijo vstopiti v *koiné*, ki ga omogočajo produkcijska
> razmerja, v katerih kapital in delo sprejemata nove oblike menjave,
> čaka »drugačno življenje«; »delavnica v steklu« želi biti poklon
> transparentnosti te menjave, teži izničiti – tako kot, med drugim,
> »organske« storitvene stavbe – realnost neizbrisljivih razlik,
> realnost dela za tekočim trakom, zakonitosti (nepojmljive vsakemu
> skupnostnemu projektu), ki urejajo nacionalno in mednarodno strategijo
> podjetja.
[@tafuri2002storia, 49]

Še najbolj uspešno sodelovanje med intelektualnim delom in kar je
nenazadnje tržna operacija pa je na ravni samih izdelkov podjetja in
njihove predstavitve. Industrijsko zavezništvo je namreč zajemalo tudi
pisce in oblikovalce, ki so okoli najslavnejših izdelkov – predvsem je
takrat šlo za tipkarske stroje – oblikovali nadzgodovinsko podobo
javnosti in družbe. Izdelki se torej niso predstavljali kot izdelki,
ampak kot ideja nove družbe, kot del projekta »nove klasike«, kateremu
tradicionalni prostori distribucije niso več ustrezali. Pod tem znakom,
razume Tafuri, je treba obravnavati Olivettijeve trgovine v Italiji in
po svetu, katerih značaj je zaupan »arhitekturnemu nadrealizmu, ki je
izdelek postavil v vakuum, ki ga izolira, predvsem, od njegovega
materialnega konteksta, da bi izbrisal njegov blagovni značaj,« izdelki
so torej zaupani arhitektom, da jim ti vdahnejo »nedoumljivo avro«.
[@tafuri2002storia, 51]

A to občasno uspešno sovpadanje Olivettijevega razsvetljenega gibanja in
arhitekturnih ambicij ni imelo dovolj trdne osnove, da bi preživelo dlje
od kratkega obdobja, ko se je zdelo, da Olivetti lahko zagotavlja tisto,
česar ostale institucije niso uspele. Na ravni urbanističnega
načrtovanja oziroma planiranja je odnos med intelektualci in poslovnimi
interesi postajal vse bolj težaven. Tovarnar je, vezano tudi z njegovim
interesom do »južnega vprašanja« razumljenega kot
prostorsko-načrtovalskega problema, za namene regionalnega načrta Ivree
združil arhitekte (med njimi spet Quaroni) in obsežno skupino
ekonomistov, sociologov ter agrarnih in industrijskih strokovnjakov v
*Gruppo Tecnico Coordinamento Urbanistico del Canavese*. Skupina je bila
priložnost, da se kompleksen problem, ki zajema najrazličnejše vidike
teritorialne strukture, naslovi interdisciplinarno na način, ki bi
privedel do splošno boljšega razumevanja prostorskega problema kot se
zastavlja v kapitalizmu (razvojni prepad med mestom in podeželjem) in do
splošno uporabnih interdisciplinarnih (znanstvenih) orodij. Svoje
razočaranje so arhitekti predstavili na desetem Milanskem trienalu
(1954) v seriji treh kratkih filmov, ki sta jih, med drugimi, pripravila
*Quaroni* in *Giancarlo De Carlo*. V filmih je »mit
interdisciplinarnosti« neposredno kritiziran: tri karikature – arhitekt,
prometni inženir in birokrat – se lotijo mesta kjer vsak zaporedoma
izniči prizadevanja in strokovnost prejšnjega s poslednjo besedo
birokrata, ki nastopa kot zastopnik ekonomske abstrakcije in
kalkulacije. Prav tako je v filmih predstavljena ideja mesta kot
posledica zgodovine razrednega konflikta in človeškega stremljenja k
lastni osvoboditvi, ki nagovarja Olivettijeve slogane »skupnosti« in
»urbanističnega načrtovanja za enotnost kulture« v točkah kjer te
slogani dele »skupnosti« in njihove interese izključujejo, ker so izven
mej poslovnih interesov. Kakor koli pa Olivettijev utopizem naleti na
objektivne meje finančne krize in težave pri poskusu vstopa na trg
elektrotehnike.

Značaj preostanka petdesetih in začetka šestdesetih let je v veliki meri
že določen s parametri, ki so se vzpostavili tekom prvega obdobja
*INA-Casa* in izvornega Olivettijevega razsvetljenstva. Drugo obdobje je
tako nekakšna mešanica rutine in stopnjevanja. Javni gradbeni sektor
dobi novo funkcionalno vlogo kjer ne le vpija presežno delovno silo,
ampak regulira tudi povpraševanje po potrošnih dobrinah, spet, podrejeno
najnaprednejšim sektorjem ekonomije:

> Obsežna infrastruktura – avtoceste in plinovodi –, poklicana naj
> reorganizira teritorij, je dejansko vzdrževala rast zasebne potrošnje
> (avtomobilski sektor), krepila oblikovanje velemestnih območij in se
> razkrila kot nezmožna obrniti trend geografije razvoja.
[@tafuri2002storia, 55]

Kljub premostitvi izvornih ekonomskih težav po vojni in kljub velikem
industrijskem razvoju gradbeni sektor ohranja svojo nazadnjaško vlogo v
razmerju do prvakov industrije. Večanje migracijskega toka na razvita
območja se ohrani, gradbeni sektor, ki ostaja na nizki tehnološki ravni,
vpija rezervno armado delovne sile, ki prek osebne potrošnje vpija
povečano gospodarsko proizvodnjo. A ker gradbeni bum nima več sekundarne
funkcije zagotavljati delavskih stanovanj, je pritisk toliko večji na
središčno nerentabilno urbano zemljo, kot so zgodovinska središča in
zelene površine – na eni strani tako prostorski razvoj postane moralni
problem splošnega obsega, ne le nesreča delavskega razreda.

Druga posledica pa je vsaj delno spremenjeno vzdušje znotraj
arhitekturne discipline, ki na podlagi dolgoletnih izkušenj privzema vse
bolj kritično držo do lastnih instrumentov in išče nove tehnike. In zdaj
je jasno, preverjeno na širokem vzorcu alternativ, da bi kakršna koli
nova tehnika zahtevala pogoje – konec liberalnega zemljiškega trga –, ki
ne sovpadajo z interesi vladajočih sil. Na tem mestu Tafuri določi
dokončen konec realizma kot ideologije, a brez politične opredelitve
akterja, ki bi izbojeval pogoje za nove arhitekturne tehnike, doda, je
realizem zamenjala obnova utopije. [@tafuri2002storia, 63]

V takšna šestdeseta vstopa napredna inteligenca. Kritika »mita
ravnovesja« – kot ekonomsko ideologijo, tako državno kot tisto naprednih
tovarnarjev, opredeli Tafuri – terja razvoj nove discipline, ločene od
uradnih centrov moči. In na tem mestu intelektualna zgodovina
arhitekturne teorije in marksovske kritike privzema iste oblike. Menimo,
da ni naključje, da se arhitekturna in marksistična problematika
povezave med védenjem in delovanjem tako skladno prekrivata: obe
»disciplini« imata, najsplošneje izraženo, za svoj predmet zavestno
spreminjanje družbe, s poseganjem v okolje ali v zgodovino. Prometejski
mit obeh, napravljen v funkcionalno gonilo *sveta kot je*, pa je
potreboval kritični pretres. Z dokončnim propadom stalinističnega mita
leta 1956 je PCI svojo politiko medrazrednega sodelovanja uradno
povzdignila v samostojen program »Italijanske poti k socializmu« in iz
lastnih vrst izgnala, če niso razočarani že sami odšli, še zadnje
ostanke bolj vzhodno usmerjenih intelektualcev in vstajniško nagnjene
ortodoksije, ki so bili tudi zadnji ostanki njene predvojne razredne
politike. Socialistična stranka (PSI), kot leva alternativa tudi za
tiste marksiste, ki so se hoteli izogniti politični hegemoniji PCI, je
združevala toliko različnih pojmovanj sebe kot je imela članov, kar
pomeni tudi, da dokler se ni do konca desetletja konsolidirala v
zmernejšo socialdemokracijo, je znotraj sebe dopuščala prostor za tiste
kritične intelektualce, ki bodo postali pomembne referenčne točke za
operaiste. A tisti, ki bo morda prvi, ki bo problem desetletja izrazil v
kar bo kasneje znano kot operaistični problematiki, je Raniero Panzieri:
kot funkcionar v centralnem komiteju PSI bo kulturne in politične izgube
levice pripisal dotlej nereflektiranem sooznačevanju teorije, partije in
razreda. Temeljna naloga, zanj, je ponovno obnova marksizma (teorije)
kot »permanentne kritike« in, kar gre z roko v roki, politični preporod
delavskega gibanja (razreda). [gl. @wright2002storming, 17-19] In v
svoji kritiki ne bo sam. Cela mlajša generacija članov tako PSI kot PCI
bo na krizo organizacij odgovorila z obnovljeno nujo po kritiki
ideologije, ki prevladuje tudi znotraj uradnega delavskega gibanja, in s
projektom ponovne preveritve marksističnih hipotez na njihovem
»izvornem« terenu: na tovarniških tleh, kjer je stavkovni val v letih
1959–1963 – izdan iz strani uradnih sindikalnih in parlamentarnih
predstavnikov – nakazoval in terjal novo analizo in novo politiko izven
uradnih organizacij.

Olivettijeva politika zaposlovanja naprednih strokovnjakov bo ključna
tudi za začetne teze operaizma. Na tem primeru bo humanistični zagovor
mehanizacije, napredka, racionalizacije in planiranja praktično ovržen,
demonstracija tega pa neposredno dostopna sociologom, piscem in
teoretikom, ki jih je »kapital s človeškim obrazom« zaposloval. Vzdušje
znotraj tega dela napredne inteligence tako privzame destruktivne cilje,
kot to izrazi *Alberto Asor Rosa* v eni izmed številk revije *Quaderni
rossi*, prvega projekta *operaizma*:

> Resnica je, da ta (psevdo)znanstvena nevtralnost [znanosti, orodij,
> planiranja, op\. a.] predstavlja največjo mistifikacijo v svetu, ki je
> razdeljen na razrede in *sam po sebi* ni nevtralen. Pravzaprav: če
> pozabimo na družbeno dinamiko, ki poganja industrijski svet, na eni
> strani dobimo njegovo deformirano in delno podobo in industrija sama,
> ki je stalna referenčna točka tovrstnega diskurza, postane zgolj
> abstrakcija brez realnega zgodovinskega in družbenega pomena; na drugi
> strani pa novega kulturnega predloga ne moremo umestiti znotraj
> ideoloških shem sistema, saj je tudi sistem v tem primeru (lažna)
> nepremična danost, objektivna, torej neodpravljiva, možna je zgolj
> integracija: in mi vemo, da je edini način za razumevanje sistema
> zamišljanje njegovega uničenja.
[@asorrosa1962punto, 125]

To intenzivno teoretsko obdobje bomo povzeli z obnovo njegovih ključnih
pojmovnih stopenj, ki so, menimo, temeljni za produktivno razumevanje
osrednjih besedil kritike arhitekturne ideologije: *Panzirerijeva*
kritika objektivnosti razvoja in racionalizacije (*plana*) kot
razrednega gospostva; *Trontijeva* teza, da je celotna družba prizorišče
razrednega boja, ki po svoji obliki izhaja iz tovarne; ter *Fortinijevi*
komentarji o strategiji in vlogi intelektualnega dela kot kulturnih
posrednikih dejstva *družbene tovarne*. [V prvih dveh točkah gre za
formalno enak pristop kakor v @aureli2008project, gl. predvsem poglavji
4 in 5; gl. tudi @wright2002storming, 36-46]

Panzierijevo besedilo o kapitalistični rabi strojev [@panzieri1961uso]
je teoretski prispevek k poročilom in študijam, ki so postopoma
prihajale iz najnaprednejših tovarn [gl. @alquati1961relazione;
@alquati1962composizione; @alquati1963composizione] in je tako eno prvih
demonstracij operaističnega programa ponovne preveritve marksističnih
tez v novem kapitalističnem ciklu za katerega je značilno planiranje v
velikem obsegu.^[V italijanskih izvirnikih uporabljen izraz
*neocapitalismo* se je v angleščino prevajalo kot *late capitalism*, ki
se v slovenščini pojavlja kot *pozni kapitalizem*. Opozarjamo, da ima ta
izraz danes že novo rabo, ki želi zajeti kvalitativne spremembe
kapitalizma v obdobju zadnjih 30 let.]

Uvodoma Panzieri obnovi poglavja iz *Kapitala* o kooperaciji,
manufakturi ter veliki industriji, [gl. @marx2012kapital, poglavja 11,
12 in 13] da bi izpeljal svojo osnovno tezo v polemiki s tako
imenovanimi »objektivisti«. Kapitalizem predpostavlja svobodnega
mezdnega delavca, ki *kot posameznik* svojo delovno sposobnost proda
kapitalu. Stopanje delavcev v razmerja drug z drugimi, se začne šele v
delovnem procesu, ko so že nehali pripadati sami sebi. Produktivna sila,
ki izhaja iz teh okoliščin sodelovanja, je zato produktivna sila
kapitala, delavec je ne razvije sam. Družbena produktivna sila se tako
pojavlja kot kapitalova imanentna produktivna sila. Znotraj tovarne, ki
je temeljno mesto procesa razvoja delitve dela, se torej delavcem
nasproti postavlja kapitalist kot zastopnik enotnosti in volje delovnega
telesa. Panzieri nadaljuje, z gostim sklicevanjem na *Kapital*, in
poudari vlogo razvoja tehnologije na delovnih mestih: prva organizacija
dela pod kapitalom je zgolj formalna, podlaga manufakturi je še vedno
obrtna izučenost posameznega delavca, celoten mehanizem, ki funkcionira
v manufakturi torej še nima »objektivnega skeleta, neodvisnega od samih
delavcev« in zato »se kapital stalno spopada z nepokorščino delavcev«.
Ozka tehnična podlaga manufakture tako vstopi v protislovje »s
produkcijskimi potrebami, ki jih je sama ustvarila,« ki ga kapitalist
preseže z uvajanjem strojev. Značaj mehanizacije delovnega procesa je
dvojen: na eni strani delavca osvobaja od dosmrtne specializiranosti, na
drugi ga napravi dosmrtno odvisnega od strojev, torej od tovarniške
celote, torej od kapitalista. Na tem mestu so že izražena merila s
katerimi bo Panzieri analiziral povojni cikel avtomatizacije tovarn.
Uvajanje tehnologije v delovni proces izzove razredna nasprotja na
delovnih mestih, torej tehnološki razvoj se dogaja povsem znotraj
kapitalističnega procesa. In obratno prav tako, razvoj tehnologije,
oziroma produktivna sila tehnologije se kaže kot lastnost kapitala, kot
kapitalistični razvoj. Torej, ugotavlja, prvič, da kapitalistična raba
strojev ni enostavno izkrivljanje ali odklon od »objektivnega« razvoja,
ki bi bil sam po sebi racionalen, temveč da kapitalistična raba določa
tehnološki razvoj. In drugič, da »znanost, neizmerne naravne sile in
množično družbeno delo«, ki so utelešeni v strojih, skupaj s stroji
predstavljajo moč »gospodarja«. [@panzieri1961uso, 53-56]

Tako imenovane »objektivistične« interpretacije tehnološkega napredka
pa, po Panzieriju, tehnologijo obravnavajo v idealizirani, čisti formi.
S to kritiko, pravi, se ne obrača na kapitalistične ideologe, ampak na
predstavnike delavskega gibanja, ki tehnološkemu napredku pripisujejo
skrito objektivnost, ki bo »avtomatsko« ali »nujno« odpravila obstoječe
odnose. (Torej, tako napredek kot revolucijo pojmujejo kot objektivni
dejstvi, neodvisni od razrednih nasprotij. To teoretsko stališče na
praktično-politični ravni predstavljajo tako imenovani rumeni sindikati,
parlamentarna levica in reformistični intelektualci.) Pomembno je
poudariti, da Panzierijeva kritika racionalnosti ne zajema zgolj ozko
razumljene tehnologije grobih strojev, temveč se nanaša na tehnologijo
kot organizacijo delovnega procesa in tako zajema tudi nove tehnike –
razne menedžmentske znanosti, od *taylorizma*, *fordizma* in takrat
aktualne *methods-time measurement* ter novih informacijskih tehnik – in
torej nove hierarhizacije delovnega procesa z uvajanjem sociologov in
inženirjev v njegovo organizacijo. V tehnološko-idilični predstavi
»objektivistov« se novovzpostavljene vloge povezane z upravljanjem
pojavljajo kot težnja proti razdrobljenosti in anarhiji kapitalistične
produkcije, ki v sebi združujejo s stroji odvzeto vsebino dela. Nov tip
delavca – tehnik, »intelektualec produkcije« – analogno s stroji, ki
zopet združijo v razvoju ločene gibe delavca – združuje naloge
odgovornosti, odločanja, priprave in vodenja, ki so bili z uvedbo
strojev delavcem odvzeti. Protislovja na delovnih mestih se njim
pojavljajo kot začasna »tehnična neskladnost«, kot nujno zlo v določeni
fazi produkcije, ki bo preseženo z nadaljnjim tehnološkim razvojem. A
realnost, ki jo kažejo aktualni boji, opozarja Panzieri, je, da se
ročnim delavcem vsako planiranje procesa kaže kot gospostvo, tehnični
delavci pa ugotavljajo, da morajo svoje objektivno primernejše rešitve
zavračati, da bi upoštevali omejitve, ki jih določajo zasebni interesi.
[@panzieri1961uso, 56-61]

Panzieri v nadaljevanju prispevka opredeljuje politične dimenzije
opravljenih analiz. Zavzemati »objektivna« stališča o kapitalistični
racionalizaciji postane politično neustrezno za opredeljevanje značaja
kapitalistične produkcije, saj ta objektivnost zgodovinsko nastaja
znotraj sistema kapitalistične produkcije – produkcijska razmerja so del
produkcijskih sil, potrdi Panzieri, s sklicevanjem na *Kapital*. Takrat
ustaljeni politični domišljiji pa umanjka potencialno revolucionarna
ugotovitev, da objektivni razvoj kapitalizma nujno ustvarja tudi delna
stališča znotraj sebe, ki dvojni značaj produkcije – konkretno
produkcijo uporabnih stvari in abstrakten uvrednotevalni proces – lahko
zaznajo enotno le kot razredno gospostvo. Vrnitev teoretske analize in
političnega delovanja na *razredno* raven, enotno razumevanje
»tehnološkega« momenta s politično-gosposkim, pravi

> se ne izraža kot napredek, temveč kot prelom, ne kot »razkritje«
> skrite racionalnosti, ki je neločljivo povezana s sodobnim
> produkcijskim procesom, temveč kot izgradnja radikalno nove
> racionalnosti, ki nasprotuje racionalnosti, ki jo prakticira
> kapitalizem.
[@panzieri1961uso, 60]

Prispevek *Maria Trontija* v naslednji številki *Quaderni rossi*,
*Tovarna in družba*, [@tronti1962fabbrica; angleški prevod danes v
@tronti2019workers] lahko razumemo kot delno logično nadaljevanje
*Kapitalistične rabe strojev*. Če je Panzieri razdelal sopomen razvoja
in kapitalističnega gospostva znotraj tovarne, je Tronti obravnavo tega
prenesel na celotno družbo kjer so produkcija, distribucija in
konsumpcija sestavljene v »organsko celoto«. [@tronti1962fabbrica, 7]
Podobno kot Panzieri na začetku prispevka primerja italijansko povojni
bum z zgodovinskimi vložki v *Kapitalu*, Tronti moderno realnost
premisli skozi Marxovo analizo bojev za normalni delovni dan in tako
imenovanih tovarniških zakonov. [gl. @marx2012kapital, poglavje 8]
Sredi 19\. stoletja so britanski kapitalisti prisiljeni, tako s strani
delavskega boja kot s strani »kolektivnega kapitalista«, skrajšati
delovni dan. Prisvajanje presežne vrednosti je tako dobilo absolutne
meje, kapitalistični odziv je bil torej relativno povečati to
prisvajanje [gl. tudi @marx2012kapital, 10\. poglavje] s preurejanjem
razmerij na tovarniških tleh med mrtvim in živim delom, to je med
konstantnim in variabilnim kapitalom, to je med delavci in stroji.
Delovna sila, ugotavlja Tronti, tako deluje kot kapitalu notranji moment
njegovega razvoja – in obratno, kapitalistični razvoj pod tem pritiskom
nenehno reorganizira, »razgrajuje in ponovno sestavlja«, delovni proces
in s tem delavski razred kot produktivno silo. V tej »dialektiki«,
prepozna Tronti, se kapitalizem vse bolj socializira: vsak spopad med
delom in kapitalom razredni antagonizem prestavi na »višjo družbeno
raven«, kar je na primeru bojev za osemurni delavnik in tovarniških
zakonov premik od posamičnih antagonizmov med kolektivi in njihovimi
lastniki do urejanja delovnih razmerij in, posledično, urejanja vstopnih
pogojev za kapital na obči družbeni, torej politični ravni.

In tu je pomen Trontijevega koncepta *družbene tovarne*: posploševanje
kapitalističnih odnosov širše od tovarniških tal ne pomeni le, da
tovarniško stanje gospodari nad družbo, temveč, da družbo povsem
absorbira. Moment, ko se mora razredni antagonizem izraziti na
splošnejši ravni je moment, ko kapital postane družbena sila. Vsa
področja realno še izven kapitalistične produkcije zdaj postanejo
predmet kapitalističnega razvoja. Kapitalistični razvoj postane interna
kolonizacija vseh družbenih področij, postane kapitalovo urejanje družbe
po njegovi podobi, postane *realna subsumpcija* dela pod kapitalom:

> Bolj ko kapitalistični razvoj napreduje, to je bolj ko se produkcija
> relativne presežne vrednosti širi in prodira, bolj se cikel
> produkcija-distribucija-menjava-konsumpcija nujno sklene, to je bolj
> postaja razmerje med kapitalistično proizvodnjo in buržoazno družbo,
> med tovarno in družbo, med družbo in državo organsko. Na najvišji
> stopnji kapitalističnega razvoja družbeno razmerje postane *moment*
> produkcijskega razmerja, celotna družba postane *izraz* produkcije, to
> je celotna družba živi kot funkcija tovarne in tovarna razširi svojo
> izključno oblast nad celotno družbo.
[@tronti1962fabbrica, 19-20]

Toda posploševanje kapitalističnih razmerij, posploševanje mezdnega
dela, opozarja Tronti, paradoksalno ta razmerja ravno mistificira:

> Ko tovarna prevzame vso družbo – ko celotna družbena produkcija
> postane industrijska produkcija – se posebne značilnosti tovarne
> izgubijo v splošnih značilnostih družbe. Ko je vsa družba reducirana
> na tovarno, se zdi, da tovarna kot taka *izgine*. Na tej materialni
> bazi, na višji realni ravni, se nadaljuje in zaključi najvišji
> ideološki razvoj buržoaznih metamorfoz. Najvišja stopnja razvoja
> kapitalistične produkcije pomeni najglobljo mistifikacijo vseh
> buržoaznih družbenih razmerij. Realni proces vse večje
> *proletarizacije* se predstavlja kot formalni proces *terciarizacije*.
> Redukcija vsake oblike dela na industrijsko delo, vsake vrste dela na
> blago delovne sile se predstavlja kot izumiranje same delovne sile kot
> blaga in tako kot razvrednotenje njene vrednosti kot produkta.
> Plačevanje vseh cen dela v obliki mezd se predstavlja kot absolutno
> zanikanje kapitalističnega profita, kot absolutna odprava presežnega
> dela delavcev.
[@tronti1962fabbrica, 21-22; Prim\. »Če je analiza resničnih, notranjih
zvez v kapitalističnem procesu produkcije, kot je bralec na svojo žalost
spoznal, zelo zapletena reč in zelo obširno delo, če je naloga znanosti,
da reducira vidno, na površini se pojavljajoče gibanje na notranje
resnično gibanje, potem je čisto samo po sebi umevno, da morajo nastati
v glavah agentov kapitalistične produkcije in cirkulacije o zakonih
produkcije predstave, ki so popolnoma drugačne od teh zakonov in ki so
le zavestni izraz navideznega gibanja.« @marx1973kapital3, 351]

Posledica Trontijeve razširitve analize kapitalističnih mistifikacij
onkraj rabe strojev na celotno družbo vodi tudi v preskok v zaključku.
Četudi je zopet, kakor v Panzierijevi *Kapitalistični rabi strojev*,
poudarjena tema parcialnosti stališč – to je odstranjevanje mistifikacij
z obravnavo države s stališča družbe, družbe s stališča tovarne in
tovarne s stališča delavcev –, Trontijeva do skrajnosti pripeljana
analiza obriše kategorijo *političnega*, ki je raven najvišje
socializiranega razrednega antagonizma in ki je zato posebej avtonomno
področje. Tronti svoj politični zaključek zato ne usmerja v delavsko
zoperstavljanje »radikalno nove racionalnosti« kapitalističnemu
upravljanju produkcije, ampak predlaga zoperstavljanje najbolj parcialne
delavske subjektivnosti najsplošnejši družbeni formi, ki jo privzema kot
blago:

> [D]elavski razred se mora materialno odkriti kot *del* kapitala, če se
> želi zoperstaviti *celotnemu* kapitalu. Prepoznati se mora kot
> *poseben* del kapitala, če se želi predstaviti kot njegov *splošen*
> antagonist. Kolektivni delavec se ne zoperstavlja zgolj stroju kot
> konstantnemu kapitalu, temveč tudi sami delovni sili kot variabilnem
> kapitalu. Delo mora svojega sovražnika videti v delovni sili *kot
> blagu*.
[@tronti1962fabbrica, 25]

Kljub teoretski prebojnosti *Tovarne in družbe* pa pojmovanje
kapitalizma kot identitete nasprotij ni enostavno prevedljivo v
politično strategijo pod sloganom »znotraj in proti«. Dvoumnosti, ki se
porajajo ob težavni praktični sintezi širokega pojmovanja tovarne (kot
vse družbe) in hkratnega ozkega pojmovanja delavskega razreda (kot zgolj
zaposlenih v tovarni), bodo informirale tudi kasnejša karikirana
pojmovanja operaizma kot nerazumno redukcionističnega v luči
terciarizacije industrije, [V buržoazni ekonomiji se uporablja pojem
»deindustrializacija«, ki namiguje na nekakšno preseganje modernističnih
oblik dela in samih kapitalističnih odnosov, čeprav se to preseganje
odraža zgolj v delavskih standardih, kot njihovo izginotje, v
materialnem značaju »postindustrijskega« dela in zaposlitev pa ne. Ravno
v »postindustrijski« ekonomski retoriki je potrjena Trontijeva teza, da
se »[p]lačevanje vseh cen dela v obliki mezd predstavlja kot absolutno
zanikanje kapitalističnega profita, kot absolutna odprava presežnega
dela delavcev,« @tronti1962fabbrica, 22] ki so jo sami teoretsko
napovedali. [za kritično analizo marksoloških »zmot« v operaistični
teoriji gl. @tomba2014fragment; za kritiko filozofskih temeljev
operaizma gl. @zanini2010philosophical; na tem mestu se poraja
vprašanje če in kako so te dvoumnosti presežene v Aurelijevem
-@aureli2008project: ali so teoretsko dosledno ponovljene, ali so na
bolj sproščenem obzorju arhitekturne teorije realno odpravljene ali
zgolj prikrite] Onkraj pojmovanja možnih logičnih sosledij med
Panzierijevo in Trontijevo analizo, pa te teoretski preboji označujejo
korak k postopni teoretski ločitvi, ki jo bodo kasneje dokončno potrdile
razlike v analizi in vrednotenju izgredov na *Piazza statuto*. Po dolgih
pogajanjih o novi kolektivni pogodbi v FIATovi tovarni sta
reprezentativna sindikata zjutraj sedmega julija 1962 sprejela sklep o
začetku stavke. Tako imenovani »rumeni sindikat«, ki ga je organiziralo
vodstvo tovarne, pa je z upravo sprejel ločen sporazum, kar je izzvalo
nekaj tisoč stavkajočih napasti njihov sedež na *Piazza Statuto* v
središču Turina. Po konfrontaciji s policijo so se začeli tridnevni
izgredi, ki so se končali s tisoč aretacijami. Izgredi so dodatno
otežili odnos med operaisti in uradnim delavskim gibanjem ter prav tako
med samimi operaisti. Splošno javno mnenje ni bilo naklonjeno
izgrednikom, ki naj bi bili večinoma ročni delavci priseljeni z juga
države, sindikati in PCI so izgrede pripisale saboterjem in
provokatorjem izven delavskega razreda, del operaistov, skupina okoli
*Trontija*, je v izgredih videl potrditev, da je samodelovanje razreda
preseglo potrebo po uradnih sindikatih, med tem ko *Panzieri* izgredom
ni pripisal posebne revolucionarne vrednosti in alternative sindikatom
trenutno ni videl. S tem razkolom in posledično ustanovitvijo nove
revije, *Classe operaia*, se stroga zgodovina operaizma pravzaprav šele
začne. Trontijev zaključek kot je nakazan že v tem prispevku, ki bo kot
politična strategija pomenil absolutno podrejanje vseh (političnih in
teoretskih) revolucionarnih anticipacij vsakokratnemu vedênju delavskega
razreda, kasneje ne nastopa neposredno v kritiki arhitekturne ideologije
(enostavno zato, bi lahko rekli, ker Tafuri ne naslavlja delavskega
razreda). Nastopa pa kot estetska tema, oziroma likovna strategija, v
operaistični likovni produkciji: kapitalistično velemesto se ne
upodablja več kot potencialni raj, ki si ga je potrebno izboriti, temveč
konkretni pekel modernosti, in zoper ustaljenega upodabljanja delavskega
razreda kot množice zglednih in postavnih herojev upodabljajo delavca
kot premetenega nasprotnika vsaki buržoazni omiki, posebej tisti, ki
postavlja delo kot vrednoto. [gl. prihajajočo knjigo
@galimberti2022images] Operaistična pojmovanja kapitalističnega razvoja
in delavske subjektivnosti zato v odnosu do širše kulture, znanosti in
politike zavzemajo mesto škandala.

Kot morda dokazuje zgodnje teoretsko sodelovanje arhitektov pri
*Quaderni rossi*, [gl. @greppi1963produzione] kasnejše estetizirano
obravnavo operaističnih tem s strani nekaterih italijanskih
arhitekturnih neoavantgard šestdesetih let in nenazadnje tudi Aurelijevo
vzdrževanje te misli danes, je jasno, da arhitekt ali arhitektka ne
potrebujeta posebnega povabila ali opravičila, da marksovsko obravnavo
kapitalističnega cikla kot je razvita na straneh revije prevedeta,
uporabita ali k njej tudi prispevata: obravnavana tematika je, menimo,
že arhitekturna, oziroma, arhitekturno področje je že družbeno. A vseeno
zasledujemo tafurijansko distinkcijo, ki zahteva še en korak natančnosti
pri sledenju konstitutivne teoretske mreže kritike arhitekturne
ideologije. Omenili smo že, da razvijanje teorije subjektivitete
tovarniškega delavca ni osrednji argumentativni cilj kritike
arhitekturne ideologije: značaj industrijske družbe, kapitalističnih
odnosov in kategorije kapitalističnega plana ter družbene tovarne so že
(v obravnavanem obdobju izhajanja prispevkov kritike arhitekturne
ideologije) teoretska izhodišča, kar se zasleduje pa je teoretizacija
vloge kulture, intelektualnega dela in njegovega posredovalnega značaja
znotraj kapitalističnega razvoja. Prve razdelave v takšnih okvirjih, ki
združuje tako teoretske preboje v *Quaderni rossi* kot prve izražene
kritike razočaranih kulturnih delavcev v službi javnih političnih
institucij ali razsvetljenih tovarnarjev (med njimi tudi arhitekti
Quaroni, De Carlo in, seveda, Tafuri), bo opravil pesnik in literarni
kritik Franco Fortini.

Njegovo besedilo *Premeteni kot golobje* [@fortini2016cunning] se opira
na Trontijevo *Tovarno in družbo*, posega pa v razpravo v literarni
reviji *Il menabò*^[Literarna revija, ki sta jo ustanovila in urejala
Elio Vittorini in Italo Calvino in ki je neredno izhajala med leti 1959
in 1967] v številki o industriji in literaturi ter številki o literarni
komunikaciji in industrijski organizaciji kjer je besedilo leta 1962
tudi prvič objavljeno. Razprava je potekala o odnosu kulturnikov do nove
industrijske realnosti in o primernih umetniških formah ter temah za
naslavljanje te realnosti: natančneje, bi lahko povzeli, o možnosti
pristnega umetniškega posredovanja odtujenosti človeka v industrijski
družbi, ne da bi to umetniško posredovanje prisostvovalo isti
odtujenosti – predmet razprave je torej možnost progresivnega delovanja
v kulturni sferi. Kljub izrecnejši obravnavi literature so razpravo
spremljali tudi arhitekti. Tako tudi Tafuri v soavtorstvu v enem
zgodnejših besedil, kjer obravnava vsiljeno »novo razsežnost«
arhitekturnega področja (mesto-teritorij) in predlaga snovanje novih
konceptualnih in političnih orodij za napredno arhitekturno delovanje,
omenja zadevnost literarne razprave za arhitekturo:

> V podobnih, če ne tudi bolj eksplicitnih, terminih bi bilo treba
> ubesediti zanimanje, ki ga je vzbudila razprava v četrti in peti
> številki »Il Menabò«. Vittorinijevo vabilo pisateljem naj, četudi
> fantazijsko, upoštevajo »spremembe na industrijski ravni, ki jih
> industrijska „stvar“ povzroča v vseh drugih vrstah stvari« se lahko
> skoraj v celoti prenese na italijansko arhitekturo in urbanizem, ki se
> spričo teh novih realnosti zdita tako raztresena in, v skrajnem
> primeru, opisna.
[@tafuri2022citta, 136]

In nadaljuje z navedkom:

> Industrijski svet, ki je prek človeške roke nadomestil »naravnega«, je
> še vedno svet, ki ga ne posedujemo in ki nas poseduje prav tako kakor
> »naravni«. Od njega je podedoval staro moč, da nas določa vse do naše
> zmožnosti njegovega koriščenja, in zato mora prestati nadaljnjo
> preobrazbo, ki mu odvzame moč pogojevanja naših odločitev.
[Elio Vittorini, *Industria e letteratura*, navedeno po
@tafuri2022citta, 136]

Franco Fortini bo kritiziral to, zanj, nenatančno in mistifirajočo
pojmovanje industrijskega sveta in zoper Vittorinijev predlog nove
humanistične in progresivistično-realistične izrazne etike predlagal
drugačno strategijo. Prva točka kritike se tiče predlaganega
»materializma stvari«, ki pozablja, da predmet industrije niso le
stvari, temveč tudi človeška dejavnost. Njihovo poudarjanje do točke
estetizacije dejstva sveta industrijskih stvari, blag, pravi Fortini,
pozablja, da vse to predpostavlja tudi človekovo stanje kot blago. Ta
zmota, pravi, ima paternalistične in razsvetljenske posledice, ki so
predvidene že v Marxovi tretji tezi o Feuerbachu [»Materialistični nauk
o spremembi okoliščin in vzgoje pozablja, da okoliščine spreminjajo
ljudje in da mora vzgojitelj sam biti vzgajan. Zato mora družbo
razdeliti na dva dela – od katerih je eden vzvišen nad njo. Sovpadanje
spreminjanja okoliščin in človeške dejavnosti ali samospreminjanje je
moči dojeti in racionalno razumeti le kot revolucionarno prakso.«
@marx1976teze, 357] in ki jih progresivna italijanska kultura izkuša že
desetletje v službi javnih in zasebnih reformističnih projektov. Na tej
zmoti sloni vsa progresivna skrb o umetniškem posredovanju pravilnih
tem, ki pa za Fortinija niso enostavno odločitve intelektualca. Zanj je
osveščanje o temah in ciljih intelektualnega ali umetniškega dela
sočasno z (razrednimi) napetostmi, ki zaznamujejo tudi sektor
intelektualne in umetniške produkcije. Vse možne kombinacije med
angažiranostjo in pasivnostjo ter temami in cilji »našega časa« so torej
že strukturirane v kulturne, ideološke in svetovnonazorske sheme. Pri
vseh možnih ravneh branja dela torej ni možno vedeti, ali podcenjujemo
njegov eksplicitni predmet ali precenjujemo njegove sociološke in
kulturne kontingence. Če, pravi, »*obstaja privilegirana tema, to je
„tema našega časa“, ne obstaja privilegiranega „subjekta“, ki bi to temo
prenašal*.« [@fortini2016cunning, 127] Umetniško delo kot *tudi*
zgodovinsko, filozofsko ali politično delo bo njegova zadnja, ne prva
beseda. Fortini v razpravo ne vstopa le kot literarni teoretik, ampak
tudi politično:

> Z vso močjo moramo negirati lažni progresivizem po katerem bi morala
> industrijska realnost, v njenem momentu produkcije ali konsumpcije,
> najti literarni izraz ker je »pomembna«. Posebej tisti, ki, kakor jaz,
> potrjujejo primat industrijske produkcije pri določanju naše družbene
> usode; posebej tisti, ki prepoznavanju in uničevanju kapitalističnih
> korenin naše družbe (in torej produkcijskega načina in prisvajanja)
> dodeljujejo *vso preverljivo* dolžnost ljudi, se morajo odzvati proti
> današnjemu literarnemu »progresivizmu«. Slednji izsiljuje vsako
> stališče in zavest, ki resnično želi negirati sedanjo družbo, in
> predlaga elemente, ki vsekakor so uporabni za spoznanje-delovanje,
> vendar skupaj z vse hujšimi odmerki reformističnih banalnosti, ki so v
> praksi diverzije. »Progresivizem«, ki se je med drugim vedno
> opravičeval z zloglasnim argumentom o zaostalosti množic. Zaostalost v
> razmerju do česa? V razmerju do radikalnega progresivizma
> reformističnega vodstva, ki si napredka množic ne more zamisliti
> drugače kot »vzpon« na njihovo raven ...
[@fortini2016cunning, 128]

Če je reformistična zavzetost v praksi škodljiva in če literarni objekt
na formalni ravni lahko presega svoje svetovnonazorske, razredne in
družbene okoliščine ter lahko tako samo po sebi posreduje enako močne
»energije«, kot tiste, ki izhajajo iz zasebnih predstav avtorjev, ne da
bi izčrpalo lastno resnico, je odnos med literarnim objektom in
industrijsko produkcijo, pravi, stvar zgodovinopiscev. Namesto tega
literaturi (in sektorju intelektualnega dela nasploh) predlaga
osredotočanje na odnos med intelektualci kot producenti in industrijo
kot nosilko družbenih odnosov. Razprava, pravi, bi imela korist od
premisleka o spremembah ekonomskega in družbenega statusa intelektualcev
zadnjih dvajset let, ko je osebni odnos intelektualca do industrije kot
vira njegovega dohodka postal mnogo bolj razširjen in odločilen. Vidimo,
da gre za ponovno obujen Benjaminov predlog kot je postavljen v *Avtor
kot proizvajalec* [»Namesto da se sprašujemo: kakšen je odnos določenega
dela do proizvodnih odnosov določene dobe? Se z njimi strinja, je
reakcionarno ali pa si prizadeva za preobrat, je revolucionarno? –
namesto tega vprašanja ali vsekakor pred tem vprašanjem bi vam rad
predlagal drugo vprašanje. Se pravi, preden se vprašam: kakšen je odnos
določenega pesništva do proizvodnih odnosov neke dobe, bi rad vprašal:
kakšen je položaj pesništva *znotraj* teh proizvodnih odnosov? To
vprašanje se neposredno nanaša na funkcije, ki jih ima literarno delo
znotraj pisateljskih proizvodnih procesov svojega časa.«
@benjamin2009avtor, 319-320] s poudarjeno spremembo ekonomskega statusa
intelektualcev: če so še pred vojno, ko so sicer sredstva množičnih
medijev tehnično že dovršena, avtorji bili odvisni od države, kot
civilni uslužbenci, od agrarne rente, kot aristokrati, ali od štipendij
političnih grupacij, jih danes pretežno, posredno ali neposredno,
zaposluje javna ali zasebna *kulturna industrija*. To pa zaostruje tiste
pogoje, da kakšno delo, če uporabimo Benjaminov izraz, ne bi bilo zgolj
rutinsko. Ko je intelektualna produkcija vključena v krogotok
industrijskega kapitala, to je, ko kapital zaposluje intelektualno delo,
pomeni, da je porajanje nove vrednosti že osamosvojeno od kakršne koli
konkretne vsebine najetega dela. Zadostuje, da je vsak produkt
intelektualnega dela predogled naslednjega, kar omogoča, da kapitalizem
sistematično financira lastno opozicijo in izniči vsakršno negacijo
obstoječega reda, ki je morebitna vsebinska tema dela.

Kjer se v razpravo ne vstopa iz te smeri, pravi, so vse kritične
operacije zgolj apologija: nasproti »abstraktnega« in »formalističnega«
govora o »ideologiji« Vittorini postavlja »stvari«, kar zgodovino
nadomesti s »starim« in »novim«. S takšno ahistorično obravnavo so
izključena vprašanja o industrijskih pogojih, koga zaposluje, komu
koristi – industrijski svet je zgolj postavljen v kontrast z naravnim
kjer je potem mogoče obravnavati značilnosti te ahistorične »stvari«. In
to je podlaga mistificiranemu pojmovanju industrije kot tehnologije (ki
jo opisuje že prej obravnavan Panzierijev prispevek):

> Res je, da pri Vittoriniju »industrija« pomeni »tehnika«, to je »način
> delanja stvari«. Toda kateri že sindikalist mi je pred časom govoril o
> čudovitih zbirkah kratkih dokumentarnih filmov o posameznih
> produkcijskih dejavnostih, o katerih nemški sindikalisti razpravljajo
> z lastniki podjetij? Ti sindikati so to kar so, to je zgolj orodje
> razrednega sodelovanja in sicer prav v kolikor sprejemajo ločitev
> tehničnega od političnega ali celostnega momenta. Če industrijo
> izenačimo s tehniko (ali s tehnologijo kot pravimo s precejšnjim
> anglizmom), pomeni, da industriji pripisujemo racionalno usodnost, ki
> je nima. To je čista apologetika. In noben apologet ni tako učinkovit
> kot tisti v dobri veri.
[@fortini2016cunning, 132]

Kulturna obravnava industrije kot literarne teme, nadaljuje, je težavna
in (progresivno) politično neplodna, ker industrija ni *neka* tema,
temveč manifestacija *poglavitne* teme, ki je kapitalizem. Posplošen
razvoj literarne obravnave industrije, kot ga povzame, od naturalistične
(skoraj etnološke ali sociološke, populistične) obravnave notranjščine
tovarne, to je dela, do njemu sodobne obravnave industrijske
zunanjščine, to je stvari, industrijskih izdelkov (kar so tudi tematski
parametri takratnega tehnološkega humanizma v arhitekturi), je razvoj
njihove postopne politične neuporabnosti, saj takšne statične obravnave
posameznih aspektov realnosti ne zajemajo historičnega značaja
industrije, ki se, pravi, razkrije ob realizaciji njenih protislovij,
konkretno, predlaga primer, ob zaustavitvi dela, sabotaži, stavki. Tu
Fortini v kulturnih terminih povzema tezo *Tovarne in družbe*, kjer
posplošitev notranjščine tovarne v njeno zunanjščino posebne značilnosti
tovarne izgubi v splošnih značilnostih družbe (in, opominjamo, »ko je
vsa družba reducirana na tovarno, se zdi, da tovarna kot taka
*izgine*«), razkrije pa jo lahko samo delavski razred v momentu, ko
kljubuje sebi kot delovni sili, blagu.

Njegov predlog intelektualne drže v takšnih pogojih bo preizprašujoč in,
do neke mere namerno, dvoumen. Če je med realnostjo industrijske
produkcije in intelektualnim delom celo brezno kulture, ki je, zanj,
tudi epistemološko brezno v smislu, da intelektualec spoznanj o
površinskih posledicah industrije in o njenem notranjem delovanju ne
črpa iz istega vira, in če je poglavitna ideološka značilnost
industrijskega kapitala, da vpije in negira vsak protest, ki ga
predlaga ideološka produkcija napredne inteligence v obliki nenehnega
poustvarjanja neoavantgard, je prvi način, če želimo razočarati
pričakovanja kapitala, da lastnega dela ne krasimo z alibijem
progresivne zavesti. Tako naj se prekine rutinsko dovajanje materiala
in se ostanke revolucionarne zmožnosti intelektualnega dela prihrani
ter zakrije kakor »pilo v štruco namenjeno dosmrtnemu zaporniku«.
Predlaga torej novo potujitveno napravo, sorodno Brechtovski, ki jo
povzame s hiazmom slavnega biblijskega reka: »Bodite torej premeteni
kakor golobje in preprosti kakor kače.« [Matej 10,16 v
@fortini2016cunning]

Jasno bo, kako so Fortinijeva kritika neoavantgard, njegova strategija
preverbe kritičnih orodij, ki so intelektualnemu delu dostopna, ter
materialistična in zgodovinska obravnava vloge intelektualnega dela
znotraj kapitalističnega razvoja kasneje predpostavke tafurijanske
kritike arhitekturne ideologije. Biografsko-kronološko smo sicer še
prezgodnji – prvi razglas projekta kritike arhitekturne ideologije, in
prvo neposredno sklicevanje na Fortinija, bo nastajal od leta 1964 in
izšel 1968, ko se kmalu začne tudi Tafurijevo tesno sodelovanje z revijo
*Contropiano* – a že v zadnjem besedilu v zbirki Tafurijevih zgodnjih
zapisov, ki naj bi sledila razvoju njegovih dokončnih poklicnih
odločitev, lahko vidimo vzporednice s Fortinijevo strategijo zavračanja
(ali vsaj zaostreno pozornostjo) in preusmeritev v študijo zgodovinskih
pogojev arhitekturne in urbanistične kulture kot ideološke produkcije,
čeprav še vedno obremenjeno z aktualnimi praktičnimi skrbmi.
[@tafuri2022teoria]

Izhodišče prispevka je kritičen zgodovinski pregled evropskih
urbanističnih ideologij, da bi »preverili zgodovinskost in kontinuiteto
aktualnega kulturnega momenta z njegovimi izvori.« [@tafuri2022teoria,
151] Tafuri prepozna dva tokova buržoazne teorije mest
(ideološko-utopičnega izraženega v misli Owen–Fourier–Cabet in
metodološkega v misli Garnier–Le Corbusier/Gropius) in poudari, da
obstaja očitna potreba po sintezi, ki je anticipirana v marksistični
misli, ta anticipacija pa se kaže »v opustitvi kakršne koli toge
vizualizacije prihodnje organizacije mesta bodisi v figurativnem bodisi
v sociološkem smislu.« [@tafuri2022teoria, 152] Sledi dokaj prvoosebna
obnova povojnega urbanizma in prostorske politike in začrtana je še
zadnja margina programskih upov:

> [V] sedanjem načrtovalskem ozračju, ki je sicer brezbrižno in ni
> odporno korporativističnim in tehnokratskim defektom, je naloga
> angažirane kulture, da nenehno razdeluje in predlaga programske
> alternative, da se ne zapira v agnostično in neutemeljeno izolacijo,
> da se pasivno ne prepusti vpitju v nove strukture in telesa.
[@tafuri2022teoria, 159-160]

Ter z optimizmom volje povabi vse strokovne negotovosti, ki so se
nakopičile tekom povojne obnove, da se soočijo z negotovostmi, ki
prihajajo v naslednjem poglavju:

> [Z]daj se je začela nova, naprednejša faza boja in od vseh nas bo
> odvisno, da se ne konča z novimi, kot je to v Italiji pogosto, še bolj
> katastrofalnimi neuspehi.
[@tafuri2022teoria, 160]

# Kritika kot *kontraplan*

> Čudovita stvar je moč v skupno dobro zbrati surove ljudi, jih
> podrediti kulturi in disciplini, varnosti in miru v mestih in utrdbah;
> potem z večjim nasiljem proti naravi sekati skale, votliti gore,
> polniti doline, sušiti močvirja, graditi ladje, usmerjati reke,
> utrjevati pristanišča, graditi mostove in presegati samo naravo v
> stvareh, v katerih narava presega nas, in tako delno zadovoljevati
> prirojeno željo po Večnosti.
> 
> – D\. Barbaro, *I dieci libri dell'architettvra di m\. Vitrvvio*, 1556

> Čim več produkcijskih sil vzbuja kapitalizem, tem več sil upora vzbuja
> in pripravlja znotraj sebe. Ko se bodo v Benetkah poleg kupol in
> zvonikov za slavo neba potegovali dimniki in bo glas siren prekinil
> lenobno tišino lagune, bo socializem tu imel svoj pravi začetek.
> 
> – F\. Ciancani, *Per l'industrializzazione di Venezia*, 1914

Znotraj toka intelektualne zgodovine in teoretske argumentacije, ki mu
sledimo, naslednjo »fazo« predstavljajo Tafurijevi teoretski spisi, ki,
ali programsko razdelajo arhitekturnoteoretski projekt kritike
arhitekturne ideologije, ali pa temu projektu sledijo kot demonstracije
te kritike. Kritika arhitekturne ideologije je ta »sprememba
strategije«, ki jo napovedujejo in obrisujejo (marksovske) analize in
kritike opravljene tekom desetletja izkušenj intelektualnega dela v
službi obnove in reformizma ter izkušenj operaistične politične prakse.
Najširši možni predmet poglavja so torej izhodiščne teze v *Teorija in
zgodovina arhitekture*, leta sodelovanja z revijo *Contropiano* in spisi
nastali tekom usmerjenega raziskovanja arhitekturnih avantgard in
arhitekture 20\. stoletja na *Inštitutu za kritično in zgodovinsko
analizo* na *IUAV*: omejili pa se bomo na *Teorija in zgodovina
arhitekture* in na prispevke v *Contropiano*, ki sta eksplicitni
napovedi teoretskega programa kritike arhitekturne ideologije, oziroma
so *Teorija in zgodovina* retrogradno označena za njegovo napoved.
[@tafuri1980theories; @tafuri1969critica] Ker smo v samem uvodu
marginalno odprli temo selektivnosti (branj, prevajanj, založništva) v
razmerju do širšega opusa, bi naslovili tudi razpravo o odnosu kritike
arhitekturne ideologije do, na primer, kasnejšega Tafurijevega
zgodovinopisja, ki naj bi predstavljal umik od kategorij ideologije,
politične ekonomije in razreda. Ocenjujemo, da je formula »dveh
Tafurijev«, kjer naj bi eden bil eksplicitno politično motiviran, drug
pa resigniran filolog, še vedno prevladujoča. [Takšno stališče
reproducirajo tudi nekateri bivši sodelavci, na primer Alberto Asor Rosa
v -@asorrosa1995critique] To formulo informira tudi uvodoma omenjen
angloameriški sprejem, ki favorizira zaporedje izhajanja prevodov, kar
zakriva morebiten vzorec stopnjevanja kritičnih namenov v zaporedju
italijanskih izvirnikov. Obstaja pa tudi korekcija takšne delitve, ki
neprekinjeno kontinuiteto misli utemeljuje v disciplinski kontinuiteti
Tafurija kot arhitekturnega zgodovinarja. Za enega najkvalitetnejših
prispevkov v tej smeri štejemo doktorsko disertacijo Andrewa Leacha,
[@leach2006choosing, gl. predvsem 1\. poglavje] ki se vsem
uveljavljenim »resnicam« o tej zapuščini produktivno izogne s
sistematično obravnavo »primarnih virov« te zapuščine in kvalitetno
rekonstruira Tafurijeva raziskovalna vodila in njihove posledice za
arhitekturno zgodovino kot disciplino. Tako arhitekturnozgodovinske
preboje ne reducira na enostavne dnevnopolitične izbire in jih lahko
obravnava in vrednoti na sebi kot zgodovinopisne metode. Z njegovo
oceno, da se arhitekturna teorija na Tafurija nanaša predvsem preko
retoričnih stališč, se moramo, kot smo se že v uvodu, strinjati. Vseeno
pa moramo zavrniti tihi zaključek njegove korekcije, ki zanemarja
sledenje distinktivnosti marksovskega prispevka tafurijanski
kritično-zgodovinski metodi in opusu (ki je v disertaciji sicer, menimo,
posredno prisotno v točkah kjer je prikazana »zapuščina« Benjaminovih
spisov o kritiki in zgodovini). [gl. @leach2006choosing, poglavje 5.
Seveda s tem prav tako trdimo, da je Benjamin *predvsem* marksovski
pisec, kar je tudi svojevrstna polemika.] Leacheva obravnava je
disciplinsko sicer – v vprašanjih in ciljih – ločena od arhitekturne
teorije, menimo pa, da v svoji gesti nakazuje tudi na
arhitekturnoteoretski kriterij, ki lahko ločuje teoretske *prispevke* od
retoričnih stališč (ki sicer lahko pogosto krasijo čisto
arhitekturnoteoretske prispevke, a si z gradivom ne delijo teoretskega
obzorja). Ta gesta sestoji seveda iz obravnave prispevkov na sebi.
Znotraj naše naloge to pomeni znotraj discipline arhitekturne teorije.
In v kolikor sami, za potrebe sledenja teoretskim prispevkom, kritiko
arhitekturne ideologije »izpeljujemo« iz njenega (dnevnopolitičnega,
kulturnega) miljeja, je to vedno podrejeno izkazovanju, da je ta »nujno«
predstavljala najbolj produktivno obravnavo lastne problematike, ker je
problematika najbolje zapopadena tudi kot arhitekturnoteoretski problem
– to, menimo, bo jasneje izraženo v nadaljevanju, tiče pa se stalne
prisotnosti arhitekturne teorije v reviji *Contropiano*. Na ta način se
izogibamo redukciji prispevkov kritike arhitekturne ideologije na
politične ali biografske muhe avtorja in spet poudarjamo uvodoma
opredeljeni dvojni kriterij produktivnega branja: področje arhitekture
problematiziramo kot arhitekturni teoretiki, ne kot arhitekti na sploh,
in problematiziramo ga kot marksovci, ne kot teoretiki na sploh. V tem
smislu menimo, da naše omejevanje na le določene tafurijanske prispevke
in le določeno intelektualno obdobje ne sodeluje pri formuli »dveh
Tafurijev« – ob kakšni drugi priložnosti bi lahko celo natančneje
zagovarjali, da je ravno marksovska kritika arhitekturne ideologije kot
razkrivanje odnosov med miselnimi strukturami dobe (avtorja) in
strukturami, ki jih vzpostavlja arhitekturno delo, tista stalna
značilnost tafurijanske zgodovinske discipline tudi, ko je predmet
obravnave renesansa –, ampak je to omejevanje primerno nalogi, ki
tafurijansko arhitekturno teorijo rekonstruira kot kritiko arhitekturne
ideologije in to rekonstruira kot distinktiven marksovski prispevek
arhitekturni teoriji. Redukcija arhitekturne teorije na politično
dogajanje prav tako reducira delovanje marksovske tradicije znotraj
Tafurijeve argumentacije na retorično stališče, ki je ustrezalo
takratnim trendom in se je spreminjalo skupaj z usodo tega trenda. Da se
je način obravnave spreminjal v odnosu do prejšnjih del, v odnosu do
spremenjenih institucionalnih okoliščin ter v odnosu do obravnavane teme
ali obdobja, se nam ne zdi pretirano pomenljiva ugotovitev glede na to,
da zasledujemo specifična teoretska in metodološka jedra ene misli in ne
popisujemo občih biografskih kuriozitet, katerih končne ugotovitve, da
je nekoč v Italiji obstajal arhitekturni zgodovinar z zelo širokim
pogledom na stvari, so obče veljavne, a ne pretirano produktivne, ko
želimo gradivo razumeti kot osredotočeno teoretsko *prakso*.

In *Teorije in zgodovina* v arhitekturno problematiko vstopa prav kot
osredotočena analiza in kritika, kot sprememba strategije, ki je, v tem
primeru, sprememba (teoretskega) obzorja. Delo ima sicer svoj predmet,
ločen (morda splošnejši) od predmeta prispevkov v *Contropiano*, ki so
podrejeni zasledovanju arhitekturne institucije kot ideološkega
instrumenta kapitalizma – in tudi teoretskemu in političnemu programu
revije –, a vseeno predstavlja »prolog v nebesih« kritike arhitekturne
ideologije. *Teorije in zgodovino* torej obravnavamo na mestih, ki jih
sami razumemo kot eksplicitno napoved ali teoretski pogoj kritike
arhitekturne ideologije.

Izhodišče dela je priznanje, da je arhitekturna kritika – razumljena kot
demistifikacija pojavov z njihovo obravnavo kot zgodovinskih – v težkem
položaju, saj so znotraj umetniške produkcije normativne in kritične
geste ali operacije neločljivo zvezane. Umetniške avantgarde v svoji
zgodovini sprva nastopijo kot revolucionarna kritika – kopičijo in
stopnjujejo protislovja starega reda – in s tem hkrati ustvarjajo novo
»ideološko prtljago«, nove mite, ki po zmagi, ko doseže svoje cilje,
prek svoje uresničitve izgubijo svojo idejno moč in postanejo predmet
razprave. In v takšna sedemdeseta (knjiga prvič izide leta 1968) vstopa
mednarodna arhitekturna kultura, ki materialno izčrpano tradicijo
modernizma selektivno povzema na številne (odrekajoče ali obnavljajoče)
načine. V takšno kaotično stanje, ugotavlja, mora vstopiti kritik, da bi
iz njega izluščil logično strukturo med samimi temelji modernega
gibanja, med njegovo vsakdanjo obliko kot monoliten korpus idej ter med
novimi vrednotami, ki se porajajo ob njegovi krizi. (Vsekakor *Teorije
in zgodovina* torej v razpravo vstopa kot polemika proti normativni
arhitekturni zgodovini, ki je takrat v Rimu dobila nov institucionalni
zagon, a poleg tega gre tudi za revizijo in pripravo lastnega
teoretskega – kritično-zgodovinskega – projekta, ki je povsem trezno
naslavljal tudi lastno izkušnjo neprepričljivosti tako
humanistično-modernistične nostalgije, kot neoavantgard, ki so posredno
prav tako nostalgična, torej regresivna izhodišča.) Če povzamemo, je, po
Tafuriju, naloga kritike, brez moraliziranja ali usmiljenja do
predpripravljenih zaključkov, zgodovinsko zapopasti modernizem, ki se
predstavlja kot ahistorična, naravna institucija sedanjosti; *Teorije in
zgodovina* arhitekturni teoriji predstavlja *rez z ahistorizmom*, ki je
epistemološki pogoj, da je modernizem sploh mogoče historizirati. Pogoj
takšne naloge pa je, meni, odpovedati se za arhitekturno normalni
neločljivosti prakse od kritike:

> Če lahko arhitekt vedno najde lastno doslednost tako, da se zatopi v
> protislovja lastna njegovemu »poklicu« projektiranja, bi kritik, ki bi
> užival v podobnem položaju negotovosti, lahko bil zgolj skeptik ali
> nedopustno površen. V raziskovanju zmožnosti, ki so vpisane v poetike
> in kode sodobne arhitekture, ki jo opravljajo tisti, ki delujejo
> konkretno, je mogoče obnoviti kulturno pozitivnost in konstruktivnost,
> četudi pogosto omejeno na obrobne probleme. Toda kritiku, ki se zaveda
> negotovega in *nevarnega* položaja v katerem se je znašla moderna
> arhitektura, niso dovoljene nobene iluzije ali umetno navdušenje: prav
> tako – in to je morda še pomembneje – kot mu niso dovoljene
> defetistične ali apokaliptične drže.
[@tafuri1968teorie, 10-11]

Kakršnakoli kriza modernizma torej zanj ni zgolj površinski neuspeh,
temveč notranja, disciplinska kriza – prej kriza tiste »neločene«
kritične prakse, kakor pa samega projektiranja – med reprezentacijo
zgodovinske vednosti in arhitekturne zavesti, med zgodovino in
avantgardo. Prva natančnejša tema (pravzaprav pa »druga« bistvena tema,
če štejemo tudi programsko napoved kritike), ki bo podlaga argumentaciji
kasneje v *Contropiano*, so torej zgodovinske avantgarde in njihova
vloga ter odnosi z zgodovino. »Pravi izvor« modela avantgarde prepozna v
toskanskem quattrocentu. V tem obdobju prepozna prvi arhitekturni poskus
aktualizacije zgodovinskih vrednot – prevajanje mitološkega časa v
sedanji čas, arhaičnih pomenov v revolucionarne, antičnih besed v
civilna dejanja –, katerega protagonista sta Brunelleschi, s selektivnim
sestavljanjem antičnih fragmentov in njihovim zoperstavljanjem
srednjeveškemu urbanemu tkivu, in Alberti s formalizacijo tega jezika.
To je prvi primer nanašanja na zgodovino, ki to zgodovino pravzaprav
»dehistorizira« in poljubna merila tega nanašanja sestavi v (sodobnosti)
primeren ideološki kod, ki ga povzdigne v novo naravo. Torej, prvi
primer selektivnega nanašanja na zgodovino za namene dokazovanja
nezgodovinskosti. V tem Tafuri prepozna začetek dinamike, kjer se zdi,
da se »izničena zgodovina« maščuje z vsakokratnim izkazovanjem
poljubnosti vsega, kar je povzdignjeno v večno in naravno: zgodovina
postane skupek sodb o zgodovini, ki se morajo nenehno potrjevati ali
presegati, vsakič kot rez s preteklostjo, ki je poljubno določen v
skladu s aktualnimi vrednotami in cilji. Dostopnost zgodovinskega
gradiva za prilastitev v cilje prihodnosti – prenos zgodovinske
abstrakcije v ideološko sporočilo – postane podlaga arhitekturnemu
delovanju, tradiciji. Na tej podlagi – kot izključna manipulantka
zgodovinskih sporočil na področju zidave – vzpostavlja arhitektura,
arhitekturni objekt, avtonomno sporočilo v odnosu do ostalega zidanega
tkiva in tako arhitekturna produkcija avtonomno področje od njenega
širšega urbanega, vojaškega, verskega konteksta. [@tafuri1968teorie,
24-28] Že na tem mestu lahko opozorimo na več odločilnejših »sestavin«.
Prvič, četudi je jasno – v smislu prisotnosti takšnega pojmovanja skozi
besedilo –, da so ekonomski pogoji arhitekture kot avtonomne discipline
v vzhajajočem trgovskem kapitalu (ni slučaj, da so središča začetkov
renesanse trgovske republike) in novih tehnikah organizacije gradbišč
(tu imamo v mislih »tehnologije« formalizacije, kot je perspektiva, ki
posežejo v delitev dela na področju zidave), je raven razprave o njenem
položaju na ravni kjer nastopa avtonomno preko lastnih predmetov
discipline: na ravni stavb, diskurza, simbolov in sporočil, torej
objektov in teorije. Drugič, izkaz te avtonomije je vzpostavljen na
odnosu do ostalega tkiva, torej eno določilo zgodovinske dinamike
arhitekture je odnos med objektom in mestom. Struktura dinamike razvoja
»avantgard« – to maščevanje zgodovine – sledi z naraščajočim razkorakom
med tradicijo in zgodovino. Ravno z ustanovitvijo tradicije ta preneha
biti zgodovina in, ker je dostopnost zgodovine predpogoj in izvor
teoretskega diskurza, ki vzpostavlja avtonomijo arhitekture, lahko
zgodovina – tisto kar je z razkorakom s tradicijo lahko (ponovno)
prepoznano kot zgodovina – vsakič ponovno vstopi v tradicijo (in torej
vsakič kot rez in nova sinteza). Ker gre za protislovno in stopnjujočo
interno dinamiko arhitekture kot avtonomne discipline, Tafuri uporablja
izraz dialektika. Arhitekturna kultura 16\. stoletja zato lahko
zgodovino odkrije ponovno in lahko arhitekturni tradiciji predstavi novo
provokativno motnjo. Arhitekturna praksa ima zdaj tudi zgodovino
lastnega odnosa do preteklosti, kar omogoča, da sam ta odnos postane
predmet arhitekturnega teoretskega diskurza. Borromini, razvija Tafuri,
se lahko tega predmeta loti problemsko in tako zastavi arhitekturi
povsem notranji projekt sestavljanja celega loka zgodovinskih objektov v
tipološke sinteze in prostorske konfiguracije, ki naj bi kritizirale
posredovan odnos med preteklostjo in njihovo arhitekturno
reprezentacijo, hkrati pa izkazovale arhitekturi lasten značaj kot
komunikacijsko orodje. Kot taka pa lahko arhitektura razvija in
razdeluje kompleksen sistem podob in geometrijsko-simbolnih matric torej
širi polje arhitekturne komunikacije onkraj gradnje. [@tafuri1968teorie,
30]

Borrominijevo citiranje zgodovinskega gradiva je torej povsem
nedogmatično in usmerjeno v prenos pomenov v okvir neodvisno izgrajenega
novega organskega prostora. Na tej »oskrunitvi« vsebine, arheološki
dostopnosti antike, na novi vrednoti neposredovanega védenja se lahko
baročnemu zoperstavi razsvetljenski *revolucionarni historicizem*,
katerega pomen Tafuri povzame z Marxovimi besedami:

> Tradicija vseh mrtvih rodov kakor mora leži na možganih živih ljudi.
> In ravno ko se zdi, da si prizadevajo spremeniti sebe in stvari ter
> ustvariti, česar še ni bilo, ravno v takih obdobjih revolucionarne
> krize boječe zaklinjajo duhove preteklosti, naj jim služijo,
> izposojajo si njihova imena, bojna gesla in kostume, da bi v tej stari
> častitljivi preobleki in v tem izposojenem jeziku uprizorili nov
> prizor svetovne zgodovine. Tako se je Luther maskiral v apostola
> Pavla, revolucija v letih 1789–1814 se je izmenično drapirala kot
> rimska republika in kot rimsko cesarstvo [...]
> 
> Obujanje mrtvih je bilo torej v takratnih revolucijah namenjeno temu,
> da bi poveličevali nove boje, ne pa da bi parodirali stare, da bi dano
> nalogo poveličali v domišljiji, ne pa da bi zbežali pred njeno
> rešitvijo v resničnosti, da bi spet našli duha revolucije, ne pa da bi
> povzročili, da bi spet hodila okoli kot pošast.
[@marx2018brumaire, 14-15]

Od razsvetljenske revolucije dalje simbolnim strukturam umetniške
dejavnosti ne vladajo več absolutne vrednote, »temveč se kot
protagonistke predlagajo pustolovščine človeštva, ki terjajo odkritje
nove konstruktivnosti forme, neposredno povezane z zaznavo,
koristnostjo, z mondenim in kontingentnim simbolizmom« – narava in
zgodovina sta združeni pod »kultom razuma«. Arhitekturna praksa in
zgodovinska praksa sta tako ločeni: »avtoriteta diskurza razsvetljenske
arhitektura temelji na selektivni zgodovini izbrani v luči razuma, toda
poljubnost tega izbora se prikriva«. [@tafuri1968teorie, 40] Toda to še
ne bo pomenilo, da arhitekturna kultura sebe ne bo več predstavlja kot
legitimno tolmačko zgodovine, kar bo pomenilo vabilo zunanjim vrednotam
v arhitekturo, da bi tako opravičevala svoje razloge. Imperativ
arhitekturne produkcije po sprejemanju *posredovanih* zgodovinskih
pomenov pa pomeni vstop zunanjih določil v arhitekturo, ki se
predstavljajo kot notranji. Jasno je torej, da kritika
operacionalizirane zgodovine, to je operativne kritike – ki je morda
najslavnejša tema *Teorije in zgodovina* – ni splošna kritika o
dejanskosti vsebine operativnih zgodovin, niti splošna etična kritika o
vtisu, ki bi ga operativna zgodovina lahko želela podati o neki dobi.
Kritika se usmerja v čisto spoznavni problem, ki nastane ob ideološki
razprodaji vseh spoznavnih orodij o statusu arhitekture v svetu.

Sledenje »dialektiki avantgarde« kot krizi ali mrku zgodovine se seveda
ne konča pri razsvetljenstvu – statusu preteklosti se bo sledilo vse do
antihistoricizma modernih avantgard ter operacionalizacije zgodovinskega
védenja v utopične namene modernega gibanja –, a nadaljnjemu razvoju
bomo sledili, ko se ta, z drugačnimi poudarki, pojavi v *Kritiki
arhitekturne ideologije*. Pred tem pa je treba upoštevati še eno
bistveno temo, ki jo *Teorije in zgodovina* odpira sočasno z krizo
zgodovine. Razsvetljenska raztopitev pomenov dela v mondeno in slučajno
pomeni tudi konec tradicionalnega pojma umetniškega dela. Na tem mestu
Tafuri Heglovo prerokbo o smrti umetnosti rajši predlaga kot diagnozo in
z mrkom zgodovine poveže tudi konec *objekta*, ki ga je razsvetljenstvo,
z »osvoboditvijo« arhitekture od njenih formalnih procesov,
»normaliziralo«, napravilo vsakdanje. Status »objekta« je prav tako
kritično-zgodovinski spoznavni problem: zastavlja se kot vprašanje
ustreznega razmerja med analizo objekta od »znotraj« – in se s tem
odpovedati vsaki možnosti objektivnosti –, ali popolnoma od »zunaj« – in
se s tem odpovedati razumevanju njegovih posebnih značilnosti. Umetnost,
ki je zavrnila sporočanje absolutnih in nadzgodovinskih vrednot, da bi
se sporazumevala skozi razsvetljenski epiricizem, senzualizem in
kriticizem, ugotovi, da svojih razlogov ne more več iskati samo v sebi.
[@tafuri1968teorie, 95-98] Arhitekturni objekt postane relativna
vrednota umeščena v svoj »naravni« kontekst in objekti morajo izgubiti
ves svoj pomenski naboj in avtonomijo, da jih lahko zaznamo preko
njihovih čisto relacijskih vrednot. Kriza objekta je, po razvoju
industrije kot določujoče zunanje logike, po uvedbi tehnične
reprodukcije v arhitekturno produkcijo, povezana tudi s koncem »avre«
arhitekturnega objekta:

> Četudi okoliščine, v katerih se utegne znajti proizvod tehnične
> reprodukcije umetnine, ne bi prizadevale njegovih temeljev, vsekakor
> razvrednotijo njegov »tukaj« in »zdaj« [... Ta postopek prizadeva]
> najobčutljivejše jedro v umetniškem procesu, ki mu po občutljivosti v
> naravi ni podobnih – to je njegova pristnost. Pristnost neke stvari je
> zbir vsega, kar ji je od začetka dodala tradicija, od njenega
> materialnega rajanja vse do zgodovinskega pričevanja. Ker je drugo
> zasnovano v prvem, v reprodukciji pa se je prvo človeku izmaknilo,
> izgubi zadnje, se pravi zgodovinsko pričevanje, svojo trdnost. V
> resnici se zamaje avtoriteta stvari.
[@benjamin1998umetnina, 151]

Dvoumen status objekta v razsvetljenem svetu ima kot posledico torej
nesoglasje med statusom objekta iz stališča avtorja – objekt kot cilj –
in statusom objekta iz stališča uporabnika, občinstva in nenazadnje
zgodovine – objekt kot rezultat. Sama tehnika reprodukcije privzame
komunikativne in pomenske funkcije, privzame značaj sistema simbolov in
objekti znotraj njega značaj člena. Kot posamezen izdelek, vzet sam na
sebi, postane izven procesa, znotraj katerega je nastal, nem:

> Ločevanje predmeta od njegove lupine, razkroj avre, je znamenje
> čutnega zaznavanja, katerega »smisel za enakovrstno v svetu« se je
> tako povečal, da človek opazi to enakovrstnost s pomočjo reprodukcije
> tudi v enkratnem pojavu. Tako se na čutnem področju razodeva tisto,
> kar na področju teorije opažamo kot povečani pomen statistike.
> Usmeritev realnosti k množicam in množic k njej je dogajanje z
> neomejenimi posledicami, tako za mišljenje kot za vizualno opažanje.
[@benjamin1998umetnina, 153]

Protislovje med »prepričevalno« vlogo objekta do občinstva in kritično
vlogo objekta do svoje dejavnosti, ki jo arhitekturna ideologija nalaga
arhitekturi, bo »gonilo« dialektike avantgarde v razpravi *Kritike
arhitekturne ideologije*. Benjaminova razprava o statusu umetniškega
dela, oziroma Tafurijeva o koncu tradicionalnega pojmovanja objekta, pa
ima tudi »metodološke« posledice za Tafurijevo obravnavo in pojmovanje
arhitekture. Če je avtor pri mišljenju pomenov svojega objekta omejen s
pogoji njegovega izdelovanja in če občinstvo prav tako objekt zaznava v
seriji kot žanr teh pogojev, vprašanje ustreznega pristopa ni od
»znotraj« ali od »zunaj« ampak v »stičišču«, v pogojih njegovega
izdelovanja. Zato ker arhitekturne tradicije več ne gospodujejo
produkcijskim pogojem in pogojem recepcije arhitekture, se analiza
arhitekture ne more več nadaljevati zgolj v arhitekturnih (ideoloških)
terminih. Drugače bi bila reduktivna.

Vprašanje statusa arhitekture v svetu in vprašanje pojmovanja
arhitekture, ki ne reducira kompleksnih dvoumnosti, ki jih ta
vzpostavlja do svoje notranjščine in zunanjščine, so na kocki pri
razlikovanju kritično-zgodovinske analize ali zgodovinskega in
materialističnega pojmovanja arhitekture od, iz stališča tega
razlikovanja imenovane, operativne zgodovine ali ideološkega pojmovanja
arhitekture. Takšno početje ne more neposredno predlagati *pozitivnega*
delovanja, toda:

> Prav tako ne moremo trditi, da naloge, ki jih zdaj izgovarjamo,
> kritiko reducirajo na teroristično in nihilistično delovanje. Sartre
> je dejal, da je naloga literature, da »kliče k svobodi tako, da izkaže
> lastno svobodo«. Če arhitektura danes nikogar ne poziva k svobodi, če
> je njena svoboda sama iluzorna, če vsi njeni primeri tonejo v močvirju
> »podob«, ki so v najboljšem primeru »zabavne«, ni razloga, da ne bi
> zavzeli odločnega stališča proti sami arhitekturi in splošnemu
> kontekstu, ki pogojuje njen obstoj.
[@tafuri1968teorie, 271-272]

Praktična stava takšnega teoretskega projekta je zgolj posredna: »kritik
s tem arhitektu postavlja napredne ovire in ga izziva jih premagati«.
[@tafuri1968teorie, 272] Vsekakor to početje sebe ne razume, kot da
»obvlada« stanje in da lahko gotovo razreši »temeljno aporijo našega
stoletja«:

> V zgodovini ne obstajajo »rešitve«. Ampak lahko vsakič prepoznamo, da
> je dramatizacija antitez, spopad stališč, poudarjanje protislovij
> edina možna pot. In to ne zaradi kakšnega posebnega sadomazohizma,
> temveč zaradi hipoteze o radikalni spremembi, ki bi nas prisilila, da
> bi začasne naloge, ki smo si jih s to knjigo skušali razjasniti,
> skupaj z zaskrbljujočim sedanjim položajem, šteli za presežene.
[@tafuri1968teorie, 272]

In ti zaključki bodo zanimivi za nastajajoč revijalni projekt pod imenom
*Contropiano*.

Revija je leta 1967 ustanovljena kot organ teoretske refleksije, ki naj
bi po desetletju delavskih bojev tistim, ki so bili potisnjeni v ta boj,
prispeval orientacijo in premislek. Gre za zgodovinsko točko, ko strogi
operaistični postulat sledenja vedênju delavskega razreda že predstavlja
teoretsko in politično slepo ulico: Trontijev idealizem (razrednega)
subjekta pomeni, da je delovanje tega subjekta *ipso facto* nesporno
pravilno. V obdobjih, ko se razredni boj stopnjuje, so posledice tega
idealizma pretežno neškodljive, a v obdobjih razrednega umika – ali
nazaj k uradnim organom, ali nazaj k nepolitičnim zahtevam – je
operaistična teorija prisiljena, pod pritiskom lastnih ugotovitev in
legitimnosti, slediti in opravičevati ta umik. V obeh primerih pa pomeni
potrditev že obstoječega. [za politično oceno operaizma, prvič
objavljeno leta 1980, gl. @sbardella2000nep] Revija delno zasleduje ta
umik, ki, ob presenečenju svetovnega protestniškega gibanja, že ob prvi
številki vodi v spor in izhod Tonija Negrija iz uredništva. Preostali
del uredništva – Massimo Cacciari in Alberto Asor Rosa – ter skupina
tesnih sodelavcev revije – med njimi tudi Mario Tronti in Manfredo
Tafuri – pa se včlanijo (nazaj) v PCI za namene obrambe (ali
rekonstrukcije) njenih zadnjih političnih ostankov levo od
socialdemokracije. Temu političnemu kontekstu ustreza teoretska in
uredniška usmeritev revije, ki, nasprotno od sorodnih predhodnic in
kljub »maju 68«, programsko zasleduje distanco, refleksijo in
daljnovidnost. Vsekakor prevladuje prepričanje, »*da se eno zgodovinsko
obdobje izteka, drugo pa se naporno in boleče odpira*,« in da situacija
prekipeva od možnosti, »ki čakajo, da jih teoretsko spoznamo in
organizacijsko rekonstruiramo,« a da nedvomno »ne smemo podcenjevati
druge plati tega pojava, ki se negativno kaže v tem, da stežka določimo
skupno os in enotno usmeritev tistih, ki prepogosto in iz
najrazličnejših smeri domnevajo, da so že sami odkrili ključ
revolucionarne zgodbe v Evropi«. [@asorrosa1968primo, 238-239] Problem
je torej primarno teoretski: od razumevanja in vrednotenja razrednih
gibanj do razumevanja in vrednotenja kapitalistične družbe v njeni
celoti. Demistificiran opis tega spoznavnega predmeta in nakazilo
preoblikovanja tega spoznanja v delovanje pa, menijo, je mogoč le iz
*stališča popolnoma odtujenega razreda*, iz stališča »negativnosti«, ki
jo sistem nujno poraja, torej iz notranje točke protislovja. Njihov
raziskovalni program torej pojmujejo kot »delavsko znanost«, ki ne more
biti drugega kot kritika ideologije kapitalističnih institucij na vseh
ravneh:

> Naša revija dejansko naslavlja zelo širok razpon interesov, kar je
> razvidno že iz njene prve številke. Menimo namreč, da se mora odziv
> delavskega stališča izkazati za učinkovitega ob soočenju z vsemi
> ravnmi kapitalističnih »institucij«. Seveda je povsem jasno, da na tem
> področju revija ne more dajati pozitivnih napotkov, temveč le spodbude
> za zavračanje in negacijo. Skratka, ne nameravamo rekonstruirati
> kulturne sistematike delavskega razreda [...], temveč sistematično in
> do najglobljih temeljev [...] uničiti *razredno kulturo buržoaznega
> nasprotnika*, ki se še vedno zdi vse prej kot poražena in neučinkovita
> (še posebej, če jo razumemo kot »sistem« intelektualnega gospostva in
> ideološke prisile enega razreda nad drugim).
[@asorrosa1968primo, 243]

Na tej točki lahko, menimo, še preden natančneje razdelamo argumente
najslavnejših prispevkov kritike arhitekturne ideologije v tej reviji,
konkretneje naslovimo dve vrsti sodb, ki so (in še vedno) spremljajo
tovrstna arhitekturnoteoretska početja [Opominjamo: »[z] osamitvijo
arhitekturne tematike od teoretskega konteksta, katerega nosilka je bila
revija, so naš spis presojali kot poklon nekemu apokaliptičnemu
obnašanju, kot „poetiko odpovedi“, kot ekstremno napoved „smrti
arhitekture“.« @tafuri1985projekt, 3] Programsko usmerjenost tega
raziskovalnega projekta lahko razumemo le kot dolgoročno stavo kar
najširših praktičnih posledic: če obstaja enotna tema intelektualne
zavesti povojne Evrope je to odnos med spoznanjem in delovanjem, teorijo
in prakso. Po zdaj že dveh desetletjih vztrajnega zmanjševanja
manevrskega prostora, kjer se je preoblikovanje enega v drugega še zdelo
pod strokovno, umetniško ali intelektualno pristojnostjo, postane
predmet obravnave ravno dinamika izgube tega manevrskega prostora, da bi
spoznanje o tem lahko informiralo delovanje zoper to izgubo. Toliko o
»poetiki odpovedi«. Da mora takšno početje privzeti obliko kritike
*vseh* ravni kapitalističnih institucij je dedukcija iz
*materialističnega* pojmovanja *odnosov* kapitalistične družbe. Da je
takšno početje razumljeno kot ples na grobu arhitekture, pa priča o
omejenem pojmovanju arhitekture (s strani takšnih branj), ki jo miselno
hkrati postavlja nad materialni svet (nad družbo), hkrati pa je ne zmore
pojmovati drugače kakor znotraj mej, ki ji jih je ta materialni svet
odstopil. Toliko o »smrti«.

(In če tudi sproščeno posežemo v tisto raven razprave, ki kritiki
ideologije pripisuje moralni značaj: Lefebvrejeva kritika Tafuriju očita
omejevanje marksistične obravnave arhitekture na zgolj kritiko
arhitekturne ideologije, ki »komaj zakriva njegovo ljubezen do nje«.
[@lefebvre2014architecture, xxxvi] Čeprav menimo, da se govor o
kritičnem početju kot o ljubezni ali zlobi izogiba razpravi o
konceptualni strogosti in metodološkosti kritike ideologije, je
analogija posredno posrečena. Benjaminovo branje Goetheja in sočasna
demonstracija njegove teorije kritike, ki je vsekakor vplivala na
Tafurija, uporabi ravno takšen primer: »Recimo, da spoznamo osebo, ki je
lepa in privlačna, vendar zaprta, ker v sebi nosi skrivnost. Obsojanja
vredno bi bilo, če bi želeli prodreti vanjo. Vendar dovoljeno bi bilo
vprašati, ali ima sorojence in ali njihov značaj do neke mere pojasni
tujčevo skrivnost. Povsem na enak način išče kritika sorojence
umetniškega dela.«) [@benjamin1974goethes, 172]

Distanca, ki pa jo zavzemajo, je do *takojšnje nujnosti* kulturne
kodifikacije protestništva,^[Eno prvih soglasij med protestništvom in
silami predmajske Evrope bo ravno stavba novega umetniškega centra v
Parizu.] torej odpoved – če govorimo o arhitekturi – od neposredno
pozitivnih (v smislu normativnih) projektivnih napotkov: najširša
javnost kritike ideologije je teoretska javnost, ki ima arhitekturo za
spoznavni objekt, ne cilj izdelovanja (ali ožja javnost, ki pogoje tega
izdelovanja vsaj problematizira).

Kar pa bi lahko bilo za raziskovalce nujnosti arhitekture bolj zanimivo
je, da ob večplastnem pristopu revije do raziskovanja najširše
zastavljenih problemov ravno arhitekturno področje zavzema stalno mesto
kot produktivna točka za vstop v takšno raziskovanje [O Tafurijevi vlogi
znotraj revije in pomenu arhitekturne teorije za celoten projekt gl.
@asorrosa1995critique] (in ne zgolj kot ena plast: stalno prisotne so
tudi razprave o urbanističnem in prostorskem načrtovanju). Kontekstualne
razmere teh arhitekturnoteoretskih spisov ima za nas, poleg izogibanju
»dvoumnim branjem«, dodaten pomen, saj izkazujejo da, če že želimo
povedati kaj o arhitekturi, moramo govoriti o svetu in obratno, če
želimo povedati kaj o svetu, moramo govoriti o arhitekturi.

*Kritiko arhitekturne ideologije*[@tafuri1969critica] lahko razumemo kot
natančnejše nadaljevanje ali dopolnjevanje *Teorij in zgodovine* v
točki, kjer je v slednji teoretiziran vstop zunanje logike v
arhitekturno produkcijo. Predmet je torej sledenje interni »dialektiki«
arhitekture kot je ta razvrščena v sistem pomenov, torej relacijske
vloge arhitekture v tem sistemu. V strožje marksovskih kategorijah, ki
so eksplicitna izhodišča revije, je predmet obravnave *razredna
funkcija* arhitekture.

»Oddaljiti tesnobo z razumevanjem in ponotranjenjem njenih vzrokov: to
se zdi eden poglavitnih etičnih imperativov buržoazne umetnosti.«
[@tafuri1969critica, 31] S tem je povzeta dinamika intelektualnih
poklicev v kapitalističnem razvoju, ki je izhodišče tako prispevka kot
revije nasploh: intelektualno delo posreduje konflikte in protislovja,
ki jih ta razvoj poraja – govora je razrednih konfliktih, tu bi opomnili
spet na prispevke Panzierija in Trontija – v njihovi družbeno
sprejemljivi, mistificirani obliki. V tem smislu obstaja med
intelektualnimi avantgardami in naprednim kapitalom »tiho soglasje«, ki
ga ideološke konotacije revolucije in reforme, ki so se zgodovinsko
nalagale tem gibanjem, zakrivajo. In ravno v zgodovinski točki kjer del
teh ideoloških funkcij izginja, je videz neposrednega konflikta med
zasebno intelektualno zavestjo in kapitalom, torej videz, da imajo
intelektualni produkti objektivne vrednote, ločene in nasprotne od
kapitala, največji. Takšno mistificirano pojmovanje družbenega konflikta
pa ne more zapopasti vseh konstruktivnih vlog, ki jih je intelektualno
delo znotraj kapitalističnega razvoja privzemalo, in položaja, v katerem
se je potem samo znašlo. Razsvetliti to tiho soglasje med kapitalom in
arhitekturo, meni Tafuri, pomeni poskusiti razsvetliti, zakaj mora eden
zgodovinsko najbolj funkcionalnih predlogov reorganizacije sodobnega
kapitala doživljati »najbolj ponižujoča razočaranja«.
[@tafuri1969critica, 31] Iz prej obravnavanega razvoja sledi, da so
»razočaranja«, o katerih je govora, relativni neuspeh, v odnosu do enega
pojmovanja modernizma, zadovoljivo vključiti arhitekturo v tiste podobe
prihodnosti, ki ji je sledil povojni industrijski kapital. Omeniti pa je
potrebno tudi, da imajo te strukturne spremembe prostorskih investicij
in načrtovanja *materialne* posledice tudi za nove generacije arhitektov
in arhitektk. Kot so povzete v *Zgodovini italijanske arhitekture* gre
za: manjše zaposlitvene možnosti novih diplomantov (samo 36\ % milanskih
diplomantov arhitekture generacij 1963–1969 je dejansko opravljalo
poklic arhitekta); okrnjeno vlogo arhitekture v gradbeni dejavnosti
(groba ocena deleža projektiranih in izvedenih kubičnih metrov, ki so
delo arhitektov, je leta 1974 znašala 3\ %); padec javne gradnje, ki je
pravzaprav materialni pogoj modernistične arhitekturne kulture, v katero
je še danes usmerjene toliko nostalgije (delež pade iz ravni 25\ % leta
1951 na 6\ % leta 1968). [@tafuri2002storia, 123 in dalje] Obravnavati
ta razvoj arhitekture iz stališča kritike arhitekturne ideologije, da
zaobjema celoten cikel moderne arhitekture, narekuje začetek zgodovinske
obravnave v obdobju, »ki je priča tesni povezavi med buržoazno
ideologijo in intelektualnimi pričakovanji«: razsvetljenstvo.
[@tafuri1969critica, 32] To obdobje je priča formiranju arhitekturnih
ideologij v razmerju do mesta – formacija arhitekta kot ideologa
»družbenega« in opredelitev polja poseganja v urbano – in formiranju
protislovij, ki še vedno spremljajo potek sodobne umetnosti – kompleksen
odnos vlog, ki jih arhitektura vzpostavlja do svojega »občinstva« in do
svoje dejavnosti, ter kompleksen odnos vlog med arhitekturnim objektom
in urbano strukturo. 

Tafuri bo sledil razsvetljenskim arhitekturnim tokovom 18\. stoletja,
začenši z Laugierjevo razpravo, ki naj bi imela dvojno namero: na eni
strani reducirati mesto na naraven pojav, na drugi povzdigniti obravnavo
mesta na isto formalno področje kot slikarstvo. Na ideološki ravni
enačenje mesta z *naravnim* objektom pomeni asimilacijo urbanih procesov
– to je predrevolucionarnih buržoaznih procesov akumulacije, ločenih od
izkoriščanja zemlje – v naraven, ahistoričen in univerzalen proces, ki
je tako »osvobojen vsake strukturne obravnave« in prikazan kot
»objektivno nujen«. Estetika pitoresknega pa umetniški dejavnosti na
področju urbanega zagotavlja ideološko vlogo pri poseganju v to mesto,
ki, četudi smatrano za naravno, mora preiti skozi sito umetnikove
kritične selekcije in pridobiti »pečat družbene moralnosti«.
Arhitekturna urbana ideologija na tej točki deluje kot »ideološka odeja«
nad protislovji *ancien régime*, kjer že trčita urbani kapitalizem v
formiranju in predkapitalistično izkoriščanje zemlje: urbani naturalizem
prepričuje, da ni nobenega preskoka med uvrednotenjem narave in
uvrednotenjem mesta. A to soočenje Narave in Razuma pravzaprav pomeni,
da razsvetljenski racionalizem še ne more prevzeti vse odgovornosti za
strukturne spremembe v mestih. Ker še nima razvitih orodij in metod, da
bi konstruktivno posegel v ta razvoj – lahko ga samo ideološko zakriva
–, ima predvsem destruktivno, kritično vlogo do dosedanjih (baročnih)
disciplinskih parametrov in konvencij (In v tej točki Tafuri pripisuje
Piranesiju, oziroma njegovim grafikam, »izum« buržoazne znanosti kritike
vsebovane v praksi ali *kritičnega delovanja*). Na eni strani to pomeni
odpoved formiranju objektov, da bi postala tehnika organizacije vnaprej
formiranih materialov. Kar disciplini nalaga dvoumen – v razmerju do
njene avtonomije – položaj, ko se morajo arhitekturni instrumenti
preverjati izven arhitekture, in postavi oblikovno vlogo arhitekture
znotraj mesta v oklepaj. Zato mora arhitektura svoje tradicionalne
simbolne vloge, da bi se izognila uničenju same sebe, zamenjati za
znanstveno poslanstvo: na eni strani lahko postane instrument družbenega
ravnovesja (vprašanje tipov), na drugi znanost senzacij (*architecture
parlante*). S tem arhitektura sprejme »politično« nalogo in iz te naloge
sledi imperativ nenehnih iznajdb rešitev, ki so uporabne na
najsplošnejših ravneh. Na tej točki Tafuri obrobno polemizira z sodobnim
zgodovinopisjem o razsvetljenstvu (in opozarjamo, če ni očitno, gre za
posredno polemiko s sodobnim zgodovinopisjem o modernizmu), ki temu
pripisuje utopičen značaj. Nasprotno trdi, da razsvetljenstvo pravzaprav
ne vsebuje nobenih utopij, ampak da gre za popoln realizem, ki, tudi ko
zasleduje geometrijsko izčiščenost ali ko dimenzijsko poveličuje,
preverja uresničljivost modelov za nove metode projektiranja, ki bi bili
primerni za vstop arhitekture v strukture buržoaznega mesta, to pa bo
pomenilo razkroj arhitekture – v razmerju do mesta – v »uniformnost, ki
jo zagotavljajo vnaprej vzpostavljene tipologije«. [@tafuri1969critica,
38] Posledice tega razkroja, nadaljuje, do skrajnosti predvidi Piranesi
v svojem *Campomarzio*, ki naj bi bil »grafični spomenik« poznobaročne
kulture v soočenju z revolucionarnimi zahtevami razsvetljenske: z
združitvijo arhitekturnih kosov, ki so zdaj obravnavani kot zgolj
fragment enega reda, v mesto so ti izropani vsake avtonomije, četudi na
ravni posameznega arhitektura sledi popolnosti; oblikovanje posameznih
tipov je v nasprotju s samim pojmom tipologije; vpoklicana vrednota
zgodovine zavrača arheološko realnost; in imperativ oblikovne invencije,
ki se zdi, da izraža lasten primat, terja obsedeno ponavljanje izumov in
reducira celoten urbani organizem na neke vrste velikanski »nekoristni
stroj«. [@tafuri1969critica, 39] S tem se zdi, da racionalizem odkrije
lastno iracionalnost, in novo arhitekturno načelo soočeno z mestom, ki
ostaja neznanka, terja, da se morata iracionalno in racionalno prenehati
izključevati:

> Nadzor neorganske realnosti, da bi ukrepali zoper to neorganskost, ne
> zaradi predrugačenja strukture, temveč, da bi iz nje izluščili
> kompleksen nabor sočasnih pomenov: to je zahteva, ki jo spisi
> Laugierja, Piranesija, Milizije in – kasneje ter z zmernimi toni –
> Quatremère de Quincy uvedejo v arhitekturno razpravo.
[@tafuri1969critica, 41]

Proti tej zahtevi vzniknejo zahteve tradicionalnega rigorizma, idealno
opozicijo med njimi Tafuri prepozna v opoziciji, tik ob začetku 19\.
stoletja, med Antolinijevim planom za *Foro Bonaparte* in posegi vladne
(napoleonske) arhitekturne komisije. Slednja sprejema »razpravo« z
nakopičeno strukturo mesta, kjer naj »spopad« med novimi in starimi
posegi in rušitvami kompleksnemu zgodovinskemu tkivu poda novo
racionalnost in tako preko urejenega kaosa izkazuje novo civilno
vrednost ter nakazuje usodo mesta. Antolini pa to razpravo zavrača:
njegov plan predstavlja radikalno alternativo zgodovini mesta in si
zastavi spremembo urbane strukture v celoti. Obliko teh alternativ
Tafuri prepozna v celotnem poteku moderne umetnosti: na eni strani
kritično dejavnost, ki se skuša dokopati do drobovja realnosti, da bi
spoznala ter prevzela njene vrednote in bedo, na drugi kritična
dejavnost, ki želi seči onkraj realnosti, zgraditi novo realnost z
novimi vrednotami in javnimi simboli. Tretja pot, ki konkretno presega
to epizodo in razreši dilemo arhitekturne kulture, se kaže v
pragmatičnih shemah urbanizma novega sveta. Ameriški urbanizem,
pravzaprav neobremenjen s prostorskimi in disciplinskimi zapuščinami
*ancien régime*, omogoča posvajanje novih orodij in enostavnejše
zavezništvo med »silami, ki izzivajo morfološke spremembe v mestu« in
arhitekturo. Pravilne prometne mreže so uporabljene kot podpora urbani
strukturi, v kateri je nenehna spremenljivost predvidena. S tem je
uresničen cilj avtonomije in svobode fragmenta znotraj hkratnih rigidnih
zakonov na ravni celote:

> Urbanizem in arhitektura sta končno ločeni. Geometrizem plana ne želi
> poiskati, niti v Washingtonu, niti v Philadelphii in kasneje v New
> Yorku, arhitekturnega soglasja v posameznih formah zgradb. Za razliko
> od Peterburga ali od Berlina je arhitektura prosta, da raziskuje
> najrazličnejša in najbolj oddaljena polja komunikacije. Urbanemu
> sistemu je odrejena naloga izražanja stopnje nadomestljivosti te
> figurativne svobode, ali bolje rečeno, naloga zagotavljanja stabilne
> referenčne dimenzije kot njegove formalne rigidnosti. Urbana struktura
> na ta način izžareva neverjetno izrazno bogastvo, ki se, še posebej od
> druge polovice 19\. stoletja dalje, nalaga na svobodno mrežo mest ZDA:
> svobodnjaška etika se sreča z pionirskimi miti.
[@tafuri1969critica, 43-44]

Iz analize arhitekturne kulture 18\. stoletja, povzema Tafuri, izhaja
kriza tradicionalnega pojma *forme*, ki je odkrita ravno z zavestjo o
mestu kot avtonomnem polju komunikativnih izkušenj. Na eni strani je
odziv arhitekturne kulture 19\. stoletja lingvistični eklekticizem Toda
osredotočanje na arhitekturno operacijo, brez njene preveritve izven
zaprtega kroga arhitekturne razprave, se izkaže za utopizem. Kriza
odnosa med arhitekturo in mestom, ki jo zastavi razsvetljenstvo, se s
pojavom industrijskih mest samo še zaostri. Romantični eklekticizem
morda za arhitekte takrat predstavlja pravilen odgovor na razkrojevalne
učinke nove tehnološke realnosti, toda potrdil se bo nov značaj urbane
strukture, ki je prav zmožnost, da privzema oblike odprte strukture, ki
lahko vpije celo predlagano dvoumnost kot vrednoto. Romantični
eklekticizem, pravi, je zgolj tolmač »neusmiljene konkretnosti
ublagovljenja človeškega okolja,« ki dokazuje, da noben »subjektiven
napor ne more več povrniti za vedno izgubljene avtentičnosti«.
[@tafuri1969critica, 46] Alternativo, čeprav ob enakih ugotovitvah,
predstavljajo predlogi, ki presegajo ta tradicionalni utopizem bega in
predlagajo utopijo, ki je implicirana v realiziranih dejstvih, utopijo
»stvari«. Drugačen odziv je torej prekinitev s tradicionalnim
pojmovanjem arhitekture, ki podaja formo trajnim vrednotam:

> Tistemu, ki želi razbiti to tradicionalno pojmovanje in arhitekturo
> povezati z usodo mesta, ne preostane drugega kot samo mesto pojmovati
> kot specifičen kraj tehnološke produkcije in kot tehnološki produkt
> sam, ter s tem redukcija arhitekture na enostaven moment produkcijske
> verige.
[@tafuri1969critica, 45]

Opozarjamo na uvodni stavek prispevka: eklekticizem ali tradicionalni
utopizem, bi lahko rekli, vzroke tesnobe »ponotranji«, novi politični
utopizem pa jih »razume«. Ena posledica tega razumevanja (povrnitve
splošnih problemov na strukturno raven) je prikaz konkretnega »šaha« v
katerega se samoobsodi utopija: možnost arhitekture, da predlaga usodo,
ki bo presegla protislovja mesta, se zanaša na mehanizme istega mesta.
In druga: z uničenjem iluzij o sovpadanju subjektivnih dejanj s potekom
»usode« družbe buržoazni misli razkrije, da je koncept *usode* produkt
novih produkcijskih razmerij in da lahko odločno sprejemanje usode
odreši od bede, ki jo je ta ista usoda vpeljala v vse ravni družbenega
delovanja in predvsem v mesto. Arhitekturno pojmovanje mesta kot
produkta, natančneje humanitarne in utopične kritike mesta, torej
arhitekturna obravnava mesta, prepriča napredno buržoazijo naj »ustrezno
zastavi temo soglasja med racionalnostjo in iracionalnostjo«.
[@tafuri1969critica, 45]

In tu Tafuri prepozna pogoje, ki sledijo razvoju arhitekture kot
ideološkem instrumentu kapitala:

> Arhitektura, v kolikor je neposredno povezana s produktivno
> realnostjo, torej ni zgolj prva, ki s strogo jasnostjo sprejme
> posledice svojega že izvršenega ublagovljenja: izhajajoč iz lastnih
> specifičnih problemov je moderna arhitektura v svoji celoti, še preden
> mehanizmi in teorije politične ekonomije priskrbijo instrumente za
> izvedbo, sposobna razviti ideološko vzdušje, ki je uperjeno k celostni
> integraciji *designa*, na vseh ravneh delovanja, v Projekt, ki je
> objektivno usmerjen v reorganizacijo produkcije, distribucije in
> konsumpcije v povezavi s kapitalističnim mestom.
[@tafuri1969critica, 47]

Ta razvoj, pravi, poteka od okoli projekta *industrijskega mesta* (1901)
Tonyja Garnierja, do okoli leta 1939, ko je kriza modernega gibanja kot
ideološkega instrumenta že preverljiva v vseh sektorjih in na vseh
ravneh. Sledimo lahko trem stopnjam tega razvoja: prva se zaključuje s
formiranjem urbane ideologije kot preseganja romanticizma; v drugi bomo
priča razvoju vlog umetniških avantgard kot ideoloških projektov
»nezadovoljenih potreb«, ki bodo izročeni arhitekturi za njihovo
konkretizacijo; ter tretjo, ko arhitekturna ideologija postane
*ideologija Plana*. [@tafuri1969critica, 48]

Sledi torej obravnava umetniških avantgard v odnosu do velemesta, ki je
tudi nadaljevanje ali razvoj distinktivno tafurijanske teorije o vlogah
in »funkcijah« avantgard, ki mora biti sestavni del kritično-zgodovinske
»metode«, da je zgodovinska obravnava (historiziranje) modernizma sploh
mogoča. Sprva, posredno preko Benjaminovih spisih o Baudelairju, uvede
pojem velemesta, [Nit teorije o velemestu seveda seže do v arhitekturi
dobro poznanega spisa @simmel2000metropole; Velemesto bo tudi osrednji
pojem Tafurijevih sodelavcev, gl. @cacciari1993dialectics] ki je
prizorišče kapitalističnih procesov družbene abstrakcije. Kot tako se ne
nanaša na konkreten kraj (čeprav velemestno stanje spremljajo konkretni
kapitalistični prostorski procesi), ampak je realna abstrakcija –
kapitalizmu ustrezna družbena forma tehnično-prostorske konfiguracije.
Eno prvih obravnav življenjskih pogojev velemesta opravi Engels, ki se
še, iz oddaljenega in provincialnega stališča, na nove značilnosti
množice odzove moralistično. Baudelaire pa, ugotavlja Benjamin, ki ga
povzema Tafuri, nanjo ni mogel gledati od zunaj in jasno mu je, da
množica pripada velemestu in velemesto pripada množici, da je idealni in
»naravni« tip posameznika iz takšne množice apatični *flâneur*, ki je
»prisiljen spoznati lasten nevzdržen položaj udeleženca v vse bolj
posplošenem ublagovljenju« [@tafuri1969critica, 49] in mora »iskanje
avtentičnosti reducirati na iskanje ekscentričnosti«.
[@tafuri1969critica, 50] Baudelairejevega flâneurja Benjamin primerja z
delavcem, oziroma velemestno stanje (kapitalistični urbani razvoj)
primerja s kapitalističnim razvojem znotraj tovarne: izginotje vsebine
dela in dresura delavca ob vpeljavi strojev ima svoj analog v
velemestnem Lunaparku, veleblagovnici, igralnici ... njegovi mimoidoči
se obnašajo, »kot da bi se, prilagojeni avtomatom, lahko izražali zgolj
avtomatsko«. [@tafuri1969critica, 50] Na tem mestu Tafuri nadaljuje z
izvirno povezavo med tem vdorom produkcijskih načinov v strukturo urbane
morfologije in odzivom velemestnih avantgard. Jasno je, da tu zapolnjuje
vrzel med operaistično *družbeno tovarno* oziroma tezo o posploševanju
kapitalističnih odnosov izven tovarne na vse bolj splošnih ravneh in
arhitekturno teorijo (od sociologije in zgodovine urbanega razvoja do
vloge modernističnih gibanj). Pejsaži in veleblagovnice, nadaljuje,
vsekakor so kraji, vizualni in prostorski instrumenti, za
»samoizobraževanje množic iz stališča kapitala«, to je instrumenti za
subjektivacijo po podobi kapitala. Toda ideologija občinstva ne more
biti sama sebi namen, »ni nič drugega kot moment ideologije mesta kot
produkcijske enote v pravem pomenu in sočasno kot instrumenta
koordinacije cikla produkcija-distribucija-konsumpcija«.
[@tafuri1969critica, 51] In tu arhitekturne tipologije 19\. stoletja
razkrivajo nepopolnost svojih predlogov: dokler izkušnjo množice
posredujejo le kot trpečo udeležbo, prispevajo k posploševanju
produkcijske realnosti, ne pa tudi k njenemu napredku. (Tafurijeva
teorija avantgard jim torej ne nalaga le posredovanja organske enotnosti
kapitalističnega cikla produkcija-distribucija-konsumpcija, ampak
teoretizira tudi o njenem produktivnem vključevanju vanj.) Velemestne
avantgarde se bodo torej ponudile tudi kot ideologije *pravilne uporabe*
mesta:

> Odvzeti izkušnjo *šoka* od vsakega avtomatizma, na tej izkušnji
> utemeljiti vizualne kodekse in kodekse delovanja izposojene od
> značilnosti, ki so jih kapitalistična velemesta že utrdila – hitrost
> preoblikovanja, organizacija in sočasnost komunikacij, pospešeni časi
> uporabe, eklekticizem –, reducirati strukturo umetniške izkušnje na
> čisti objekt (jasna metafora objekta-blaga), soudeležiti občinstvo, ki
> je poenoteno v deklarirano medrazredno, in *zatorej* protiburžoazno,
> ideologijo: to so naloge, ki so jih avantgarde 20\. stoletja, v
> celoti, privzele kot svoje.
[@tafuri1969critica, 52]

In to vlogo pripisuje zgodovinskim avantgardam *v celoti*, brez
bistvenega razlikovanja med njimi, pravi celo, da je njihova številčnost
odraz značaja industrijske produkcije nenehne tehnične revolucije.
Avantgarde posredujejo zakone produkcije v sfero doživljanja: predlagajo
model vedênja, kjer je velemestni *šok* ponotranjen in ga posredujejo
kot neizogibno, torej nujno, naravno in univerzalno stanje bivanja. In
ker tolmačijo zgolj nekaj nujnega, lahko sprejmejo začasno
nepriljubljenost in problematičnost do ostale umetnosti: na tem sploh
utemeljujejo vrednost svojega početja. Spet pa, kot sta ti dve »poti«
vzpostavljeni že v razsvetljenstvu, iz tega nastajajo »nova
nerazrešljiva protislovja«: antitetična življenje in umetnost (kar se
nas tiče: velemesto in arhitektura) morata poiskati ali instrumente
posredovanja med njima ali način prehajanja enega v drugo. Na tej točki
je obrobno predlagan kriterij koherentnosti celotnega modernega gibanja
– od Piranesija dalje – kot projekt modeliranja »buržoaznega človeka«
kot idealnega tipa: tako *Campomarzio* kot Picassov *Violon* sta
»projekta«, vsak na svojem področju, ki s kritičnimi instrumenti
formalne razdelave razkrivata globoko resnico in to, kar bi se še lahko
zdelo partikularno, napravita univerzalno. Toda z uvedbo in kodifikacijo
*readymade* objektov kot sredstev komunikacije je jasno, da je realnost
zdaj samozadostna, da ne potrebuje reprezentacije, ter da domnevno
slikarjevo gospostvo nad formo ni drugega kot zakrivanje, da je forma
zdaj tista, ki prevladuje nad slikarjem. S tem da je zdaj kot »formo«,
dodaja, treba razumeti »logiko subjektivnih reakcij na objektivni
univerzum produkcije«. [@tafuri1969critica, 54] Kubizem poskuša
opredeliti zakone teh reakcij, želi realizirati »projekt« vedênja, toda
na notranje protisloven način, ki sicer izhaja iz subjekta a se zaključi
z njegovim popolnim zavračanjem: kot »projekt« naslavlja občinstvo, mu
narekuje aktivacijo, a hkrati namerava dokazati nujni in univerzalni
značaj »nove narave« kapitala ter sovpadanje nujnosti in svobode v njej.
Avantgardni projekti vedênja, ki težijo k aktivni umestitvi občinstva in
k preseganju pasivnosti in apatičnosti flâneurja, ki ga poraja
velemestno stanje, so ravno v točki kjer ne sprejemajo negativnosti
velemesta pozitiven predlog k njegovemu napredku (k napredku organskosti
cikla produkcije–distribucije–konsumpcije). Ne neposredno, kot njihov
objekt, temveč kot referenčna vrednota, ki se nenehno predpostavlja. In
»pozitiven predlog« avantgard bo izpostavljena potreba po planu – De
Stijl nasproti kaosu postavi princip forme, dada to potrebo izpostavlja
*via negativa*, z ironizacijo kaosa –, ki jo avantgarde lahko
opredelijo, a mu niso kos podati ustrezne forme. Na tej točki lahko
vstopi arhitektura in na »neizpolnjeno potrebo«, ki so jo prepoznale
avantgarde, odgovori z Bauhausom in *designom*:

> Bauhaus, kot dekantacijska komora avantgard, je določil to zgodovinsko
> nalogo: izbrati vse prispevke avantgard ter jih postaviti v preizkus
> pred potrebe realnosti industrijske produkcije. *Design*, metoda
> organizacije produkcije še prej kot metoda konfiguracije objektov,
> osmisli utopične ostanke, ki so vpisani v poetiko avantgard.
> Ideologija se zdaj ne nalaga nad operacijami – zdaj konkretnimi, ker
> so povezane z realnimi produkcijskimi cikli –, ampak je notranja samim
> operacijam.
[@tafuri1969critica, 56-57]

Toda *design* prav tako postavlja neizpolnjene zahteve in torej vsebuje
vsaj margine utopije, toda ta je zdaj funkcionalna za cilje
reorganizacije produkcije. In zastavljeni cilji tega *plana*, ugotovi
arhitekturna kultura, so uresničljivi le ob povezovanju gradbenega
sektorja s splošnejšo reorganizacijo mest. Torej gre za stopnjevanje
»dialektike avantgarde«: tako kot so zgodovinske avantgarde razglasile
zahteve, ki so se nanašale na sektor vizualnih komunikacij, ki je
najbolj neposredno umeščen v ekonomske procese (arhitektura in
*design*). Tako se mora planiranje, ki ga razglašata arhitekturne in
urbanistične teorije, nanašati še bolj neposredno na nekaj izven sebe:
na *plan kapitala*. Utopija arhitekture je v zaupanju, da bosta
arhitektura in urbanizem z vstopom v »horizont splošne reorganizacije
produkcije« subjekt, ne objekt tega plana. Teh posledic ni bila
pripravljena sprejeti, svojo nalogo razume politično: arhitektura – ki
tu, pravi Tafuri, že označuje planiranje in plansko reorganizacijo mesta
– rajši kot revolucija. Toda s tem je arhitektura že na voljo
oblikovanju po podobi kapitala. Ravno v politično najbolj angažiranih
krogih se metoda projektiranja prilagodi idealizirani strukturi tekočega
traku: posamezna razrešitev delcev tekočega traku (standardnih
elementov) in njihova formalna raztopitev v montažo, s tem pa tudi
revolucija estetske izkušnje, to je premik kritične pozornosti od
objekta k *procesu*. [@tafuri1969critica, 58]

In s to integracijo projektiranja v termine tekočega traku se začne
epizoda najnaprednejših upoštevanih arhitekturnih predlogov: medvojni
reformizem evropske stanovanjske arhitekture. Njihov najbolj trezen
predstavnik je Hilberseimer, ki se zaveda realnosti velemesta: v
njegovih spisih lahko prepoznamo, pravi Tafuri, priznanje, da je
velemesto postalo ogromen »družben stroj«, in zastavitev problema
velemestne arhitekture, ki mora predvsem – izolirano in v abstrakciji –
razrešiti probleme posameznih prostorskih členov, ki si sledijo od
celice (sobe) do urbanega organizma v celoti (mesta). Gre za razrešitev
(četudi s premestitvijo) problematičnega odnosa med arhitekturnim
objektom in urbano strukturo, ki spremlja arhitekturno kulturo od
razsvetljenstva dalje. S takšno zastavitvijo se sicer specifična
arhitekturna dimenzija (vsaj v tradicionalnem smislu) popolnoma raztopi
v mestu, omogoča pa pojmovanje mesta kot enotnega objekta in torej ga
lahko arhitekturno pojmuje kot sklenjeno polje poseganja.
Hilberseimerjeva velemestna arhitektura zahteva oblikovanje občih
zakonov, ki lahko strukturirajo posamezne člene in odnose med njimi, to
pa zahteva oblikovanje enotnih oblikovnih zakonov, »redukcijo
arhitektonske forme na najpičlejše, najnujnejše, najobčejše«.
[Hilberseimer nav\. po @tafuri1969critica, 60] Tu, opozarja Tafuri, ne
gre za puristični manifest, temveč izražanje na najabstraktnejši ravni
vse koordinate in razsežnosti projektiranja, ki sploh ustreza novim
nalogam faze kapitalistične reorganizacije:

> Pred posodobitvami produkcijskih tehnik in ekspanziji ter
> racionalizaciji trga je arhitekt kot proizvajalec »objektov« zdaj
> neustrezna figura. Zdaj ne gre več za podajanje forme posameznim
> elementom mestnega tkiva, niti, kvečjemu, preprostim prototipom. Ko je
> mesto enkrat prepoznano kot realna enotnost produkcijskega cikla, je
> za arhitekta edina primerna naloga *organizator* tega cikla. Če
> pripeljemo to trditev do njene skrajnosti, je dejavnost razdelave
> »organizacijskih modelov«, od katere se Hilberseimer ne želi ločiti,
> edina v kateri se popolnoma odraža nujna potreba po taylorizaciji
> gradbeništva in nova naloga tehnika, ki je vanjo vključen na najvišji
> ravni.
[@tafuri1969critica, 60-61]

Toda protislovja in omejitve, ki jih pri Hilberseimerjevih in ostalih
predlogih napredne evropske arhitekturne kulture ne smemo spregledati,
izhajajo iz poskusa ločevanja »tehničnih predlogov od spoznavnih
ciljev«. [@tafuri1969critica, 61] Mayev Frankfurt, Wagnerjev Berlin, van
Eesterenjev Amsterdam, Schumacherjev Hamburg vsekakor so »realizirane
utopije« na obrobjih urbane realnosti, a izven teh enklav se protislovja
zgodovinskih mest še naprej kopičijo in kmalu postanejo odločilnejša od
instrumentov, ki jih je arhitekturna kultura razvila pri poskusu
njihovega nadzora. Vzporednice z italijansko povojno stanovanjsko
gradnjo so očitne, jasna pa je tudi prisotnost Panzierijeve kritike
tehnologije. Arhitekturi, pojmovani kot čisto tehnični predlog
poseganja, so reformistična gibanja pripisovala *objektivno* naprednost,
brez sklicevanja na subjektivne pogoje. Tehnologija in racionalizacija
sta torej pojmovani kot razredno nezaznamovani, tehnični predlog se
lahko poda sam na sebi in preživi sam na sebi, neodvisno od razrednih
razmerij, zgolj s sklicevanjem na lastno objektivnost. Alternativo temu
takrat predstavlja arhitektura ekspresionizma, ki protislovja vsekakor
komentira, do njih zavzema kritično stališče in se pri svojih objektih
vrača k formalni dramatizaciji, a vendar, opozarja Tafuri:

> naj nas videz ne zavede. Gre za dialektiko med intelektualci, ki za
> produkcijski sistem na poti k reorganizaciji svoj ideološki potencial
> reducirajo na instrumentalizacijo naprednih programov, in
> intelektualci, ki izkoriščajo zaostalosti evropskega kapitalizma.
> Häringov ali Mendelsohnov subjektivizem v tem smislu gotovo privzame
> kritičen pomen v razmerju do Hilberseimerjevega ali Gropiusovega
> taylorizma; toda objektivno gre za kritiko opravljeno iz retrogardne
> pozicije, torej nezmožne, zaradi njene narave, predlagati globalnih
> alternativ.
[@tafuri1969critica, 62]

Kljub očitni polarnosti predlogov, pa je na eni točki res točno govoriti
o tem obdobju kot »enotnem ciklu«. Pogoj srednjeevropske
socialdemokratske arhitekture je poenotenje upravne moči in
intelektualnih predlogov in, nadaljuje, zatorej ni slučaj, da May,
Wagner in Taut prevzemajo tudi politične funkcije upravljanja
socialdemokratskih mest. S tem pa se razširi problemsko polje, ki ga
projekti *Siedlungov* še izključujejo: zdaj mora celo mesto privzeti
strukturo tekočega traku in se soočiti s tistimi elementi, ki ekspanzijo
in posodabljanje zemljiškega trga ter industrializacijo in
racionalizacijo gradbišč – vse to so, opominjamo, pogoji »projekta« –
blokirajo, predvsem z zasebno lastnino zemlje. Arhitekturni predlog kot
urbani model, ki zajema vse ekonomske in tehnološke cikle mesta ter
povezuje vse njihove člene, je že v celoti vključen v ideologijo plana.
Na tej točki je vpeljana tudi formalna fleksibilnost znotraj
*Siedlungov*, da tem političnim ciljem poda »kulturni pečat« in jih
napravi »realne«. [@tafuri1969critica, 63] Predlogi socialdemokratske
srednjeevropske arhitekture temeljijo na zavezništvu med intelektualci
levice, naprednimi sektorji kapitala in lokalnimi upravami in preko
reprodukcije modela podjetja na ravni družbe v sferi distribucije in
konsumpcije želijo realizirati videz splošne proletarizacije. Politični
zaključki ideologije medrazrednosti, ki bodo vsekakor tudi svojevrsten
konec modernega gibanja, so dobro poznani in na tem mestu naslavljajo
tudi italijanske povojne medrazredne nacionalne politične in
arhitekturne projekte. Specifično na polju arhitekture pa predpostavke
metodološke enotnosti *designa* v različnih merilih izkažejo
protislovnost »ideologije Siedlunga«: na ravni celote zapuščina Bauhausa
in konstruktivizma vsekakor dovršuje »uničenje avre«, nasprotno pa
povabljeni ekspresionistični objekti znotraj njih težijo ravno k
povrnitvi neke »avre«. Sploh pa razviti instrumenti srednjeevropske
arhitekture z njihovo minimalno funkcionalno povezavo arhitekturnih
členov v »tekoči trak« težko ohranjajo svojo aktualnost v napredujočih
integracijskih potrebah kapitalističnega razvoja. Ne le, da zasledujejo
racionalnost po podobi kapitala, zasledujejo – in tu Tafuri razvije
Panzierijevo kritiko racionalizacije do nove ravni – zastarelo in
idealizirano idejo racionalizacije:

> Negotovost, polifunkcionalnost, mnogovrstnost in dezorganiziranost v
> vseh protislovnih vidikih privzetih v globini modernega mesta
> kapitala, ostajajo zunaj analitične racionalizacije, ki jo zasleduje
> srednjeevropska arhitektura.
[@tafuri1969critica, 65]

Vsa ta protislovja bo, nadaljuje Tafuri, razrešil, oziroma vsaj naslovil
Le Corbusier med svojim *recherche patiente* (1919–1929) in izkušnjami
predlogov planov, nazadnje s planom *Obus* (1929–1931). In lotil se jih
bo, trdi, z lucidnostjo, ki na področju *progresivne* evropske kulture
nima para. »Arhitekt kot organizator« zanj ni samo slogan, temveč
imperativ, ki povezuje intelektualno prizadevanje z najvišje opredeljeno
idejo družbe, *civilisation machiniste*. Kot avantgarda te ideje mora
členiti svoje delo na tri tire: na eni ravni iskanje produktivnega
zavezništva z industrijo; na drugi iskanje avtoritete, ki lahko gradbeno
in urbanistično planiranje posreduje kot civilno reorganizacijo; in
tretje na ravni interpelacije arhitekturnega občinstva, da se napravi v
aktiven subjekt konsumpcije. Do neke mere posamezno že prepoznani nujni
vzvodi, a za Le Corbusierja ti predhodijo samim določilom plana in raven
na kateri naj bodo sintetizirani je celoten antropogeografski potek
družbe. Projekt na katerem Tafuri zagovarja Le Corbusierjevo
»najnaprednejšo teoretsko hipotezo modernega urbanizma« je plan *Obus*
za Alžir, ki obstoječe kazbatsko tkivo, zaliv in hribovje jemlje za
*readymades* v gigantskem merilu in jih poveže, pravzaprav preseže, z
novo enotno urbano strukturo. Le Corbusier prekine gropiusevsko
zaporedje arhitektura-četrt-mesto in predlaga fizično in funkcionalno
enoto, ki odnose vzpostavlja na geografski ravni mesto-teren. Ob
maksimalni pogojenosti (ki jo uveljavlja s formalno zaključenostjo)
predvideva maksimalno fleksibilnost in tako celoten tridimenzionalen
prostor urbane strukture postane razpoložljiv in, kot posplošljiv model,
celotna zemlja. Protislovja, ki smo jim sledili pri zgodovinskih
avantgardah, – napetosti med »kosom« in strukturo, funkcijo in simbolom,
racionalnim in problematičnim – so tu pozitivno izrabljena, vzroki
tesnobe so dejansko ponotranjeni. Toda plan *Obus* se ne omejuje le
pomiritev arhitekturne zavesti, temveč dejansko predstavlja tudi
napreden predlog v razmerju do kapitalističnega razvoja. Povrnitev
maksimalne fleksibilnosti na raven najmanjše celice – delavska
stanovanja v serpentinah – pomeni vabilo občinstvu, da se napravi v
aktivnega projektanta mesta, predvidi celo vstavljanje ekscentričnih in
eklektičnih struktur v mrežo fiksnih struktur: svoboda občinstva se mora
zagotoviti do te mere, da mu omogoča razdelavo lastnega »slabega okusa«
in torej maksimalno integracijo. In v odnosu do industrije je ta svoboda
še pomembnejša: Le Corbusier ne gre, kakor May, do funkcionalnih
elementov kuhinje, temveč se ustavi pri stanovanju in tako upošteva
zahtevo po nenehni tehnološki revoluciji in hitri konsumpciji, ki jo
narekuje kapitalizem. Stanovanje je tako teoretično potrošljivo v hitrem
času in mogoče ga je zamenjati ob vsaki spremembi (individualnih ali
industrijskih) potreb. Gre za projekt integracije – občinstva,
arhitekta, industrije – do skrajnih ravni:

> Subjekt urbane reorganizacije je občinstvo, ki je nagovorjeno in
> napravljeno kritično sodelovati v svoji kreativni vlogi: industrijska
> avantgarda, »oblast«, potrošniki so, s teoretično homogenimi
> funkcijami, soudeleženi v zagnanem in »povzdignjenem« procesu
> nenehnega razvoja in preoblikovanja. Od produkcijske realnosti, do
> podob, do uporabe podob, ves urbani stroj potiska »družbene«
> potenciale *civilisation machiniste* do konca skrajnosti njenih
> implicitnih možnosti.
[@tafuri1969critica, 70]

Na vprašanje, odgovarja, kako to, da projekt za Alžir ni nikoli
uresničen, se lahko odgovori na več načinov. Najprej, Le Corbusier se ne
povezuje z lokalnimi in državnimi oblastmi. Dalje, njegova metoda je
obratna tej weimarske socialdemokracije: projekt razdeluje od
posameznega k univerzalnemu in je zato kot metoda posplošljiva in
privzema značilnosti eksperimentov, ki se ne morejo v celoti prevesti v
realnost. Njihova posplošljivost pa je na tej točki že v nasprotju z
zaostalimi strukturami, ki jih naslavlja:

> če so zahteve po revolucioniziranju arhitekture v skladu z
> najnaprednejšimi nalogami ekonomske in tehnološke realnosti še
> nezmožne jim dati koherentne in organske forme, ni čudno, da se
> realizem hipotez predstavlja kot utopija.
[@tafuri1969critica, 70-71]

»Brodolom« Alžira je zato potrebno brati kot pojav mednarodne krize
moderne arhitekture.

Tu Tafuriju pripisujemo najizvirnejše in najproduktivnejše
historiziranje krize modernizma. Vse cilje arhitekturne ideologije plana
prepozna tudi v Keynesovi *Splošni teoriji*, [@keynes2006splosna] ki
odločno informira mednarodno reorganizacijo kapitala in vzpostavitev
anticikličnih sistemov po gospodarski krizi 1929. Temelje
keynesijanskega intervencionizma prepozna v temelju poetike moderne
umetnosti in Le Corbusierjevih urbanističnih teorij: razredni kompromis
– med delom in kapitalom – v obliki subvencioniranja in spodbujanja
potrošnje ter s tem prenos konfliktov na višjo raven, ki je kosumpcija.
[Na tem mestu se Tafuri sklicuje na razredno analizo keynesijanizma prav
tako objavljeno v *Contropiano* in prevedeno tudi v slovenščino, gl.
@negri1984delavci] Tako v keynesijanizmu kot pri Le Corbusierju je
strukturna negativnost kapitalizma (stopnjevanje razrednih konfliktov in
»posledični« dvig mezd) napravljena v njegovo pozitivno spodbudo.
Potrošnja je napravljena v aktivni faktor produkcije. In v tej
uresničitvi plana je premagana arhitekturna ideologija plana, ki zdaj
nima več funkcije:

> Kriza moderne arhitekture se začne natanko v trenutku, ko njen naravni
> naslovnik – veliki industrijski kapital – njeno temeljno ideologijo
> naredi za svojo ter postavi na stran nadzidave. Od tega trenutka dalje
> arhitekturna ideologija izčrpa lastne naloge: njena vztrajna želja
> videti realizacijo lastnih hipotez postane, ali sredstvo za
> premagovanje zaostale realnosti, ali nadležna motnja.
[@tafuri1969critica, 72]

In v tej luči Tafuri bere nazadovanja in involucijo modernega gibanja od
1935 dalje. Na eni strani lahko spremljamo nadaljnje zahteve po
racionalizaciji mest, ki ostajajo neizpolnjene, saj se ta problem zdaj
naslavlja na višji ravni. Na drugi, kot odziv, pa opustitev
realističnega poslanstva in potopitev nazaj v kontemplacijo Nereda in
privzemanje dvoumnih sloganov, ki izzivajo »tehnološko civilizacijo«. Po
odpovedi iskanju vzrokov neuspehov pri poseganju v mesto se umetnost
(ali arhitekturna kultura) osredotoča na iskanje novih vsebin, ki bi jih
ponudila za nova zavezništva z industrijo. Te Tafuri prepozna v obratu k
podobam – govora je o pojavih *op* in *pop arta*, *industrial design*,
jasno pa tudi o *Podoba mesta* [gl. @lynch2011podoba] –, ki z
metodološkimi predlogi in analizami raznovrstnih podob realna
protislovja mesta, ki še vedno uhajajo planu, simbolno povzdignejo na
formalno kompleksnost, katerih obravnava kot gradivo je omejena na raven
nadzidave, torej brez izhodišč v njihove materialne pogoje. Poleg
povratka v demonstracijo neizbežnosti neravnovesja in kaosa, je tem
pripisana tudi neomejena možnost uporabe in neodkrit potencial igre. Na
tej podlagi se oblikuje tudi nova urbana ideologija, ki spet, v odnosu
do občinstva, privzame prepričevalno vlogo. Konkreten primer teorije
neoavantgard, ki ga obravnava, je *Livre blanc de l'art total* Pierrja
Restanyja, vzorec, ki ga prepozna, pa je vsekakor opazen tudi
situacionistični kritiki mesta (preko sklica na francoski »maj«), ki
ima še danes zagotovljeno romantično stojišče v kritični umetniški
dejavnosti v razmerju do mesta:

> Krog se sklene. Markuzejanska mitologija je izkoriščena za
> dokazovanje, da je možno doseči neko nedoločeno »kolektivno svobodo«
> znotraj trenutnih produkcijskih razmerij in ne z njihovim
> spodkopavanjem. Dovolj je zgolj »podružbiti umetnost« in jo postaviti
> na čelo tehnološkega »napredka«: ni pomembno, če celoten cikel moderne
> umetnosti dokazuje – včeraj morda razumljivo, danes zgolj zaostalo –
> utopičnost teh predlogov. Za to postane dopustno celo vpijanje najbolj
> dvoumnih *slogans* francoskega »maja«. *Imagination au povoir*
> odobrava dogovor med protestništvom in konzeravtivizmom, med
> simbolično metaforo in produkcijskimi procesi, med begom in
> *realpolitik*.
[@tafuri1969critica, 75]

Kritika torej postane operativna, umeščena znotraj produkcijskih ciklov
njegova protislovja prestavlja na vedno naprednejše ravni, ki so tam
izolirano kontemplirane v mejah domišljije (in predvsem lobanje).

Neumorno iskanje odvodov v simbolne in marginalne politične akcije (ter
njihova takojšnja funkcionalizacija), spoznavanje ogromnih tehnoloških
zmožnosti racionalizacije (skupaj z vsakodnevnimi ugotovitvami o njihovi
brezkoristnosti) in zastaranje metod projektiranja še preden so njihove
hipoteze lahko preverjene, pravi, tvorijo tesnobno vzdušje, ki na
obzorju nakazuje zastrašujoč prizor proletarizacije arhitekturnega dela.
Da se, zaradi nezmožnosti zgodovinske in materialne analize »opravljene
poti«, posledicam procesov, ki so jih »sami sprožili«, upirajo s poskusi
rekuperacije »etike« moderne arhitekture in rekuperacije idealiziranega
poklicnega dostojanstva, kaže na »politično zaostalost te skupine
intelektualcev«. [@tafuri1969critica, 77] Iz stališča opravljene analize
si zgodovinski pomeni mnogovrstnih poskusov – konstruktivizem in
»protestna umetnost«, racionalizacija in subjektivizem, plan in kaos –
končno ne nasprotujejo: oba pola sta bistvena za kapitalistični razvoj.
Tako projekt kot utopija (iluzija možnosti nasprotovanja projektu z
alternativnim projektiranjem), sta »bistvena za integracijo kapitalizma
v vse baze in nadzidave človeškega obstoja«. Na tej analizi
zgodovinskega poteka in izteka avantgard sloni slavni zaključek:

> Lahko bi se reklo, da za arhitekturo in planning obstajajo številne
> marginalne in retrogardne naloge. Nas rajši zanima kako to, da je
> marksistično usmerjena kultura, doslej s skrajno skrbnostjo in
> vztrajnostjo, ki je vredna boljšega cilja, po krivem zanikala ali
> zakrivala to preprosto resnico: da, kot ne more obstajati razredna
> politična ekonomija, ampak zgolj razredna kritika politične ekonomije,
> tako ne moremo utemeljiti razredne estetike, umetnosti, arhitekture,
> ampak zgolj razredno kritiko estetike, umetnosti, arhitekture, mesta.
[@tafuri1969critica, 78]

Del odlomka verjetno predstavlja najširše reproduciran del prispevka,
navajamo ga pa v celoti, saj razkriva usmeritve kritike ideologije, ki
jih želimo poudariti. Da ne gre za »poetiko odpovedi« in »smrt
arhitekture« smo že poudarili, a ob vseh stališčih »*contro*«, ki so se
utemeljevali od *Teorij in zgodovine*, samih izhodišč revije in do
obravnavanega spisa, so točke dvoumnih branj povsem na voljo. Tu ne
moremo mimo tega, da je že občinstvo tega političnega sporočila zelo
omejeno, kaj šele teža, ki si jo sam pripisuje. S tem ne mislimo, da je
trivialno, temveč predlaga strateško usmeritev tistim, ki si izhodišča
za razumevanje objektivnega stanja v veliki meri že delijo – recimo temu
marksistično usmerjena kultura. Do te, bi lahko rekli, Tafuri zavzema
»sektaško« stališče, kot to ob drugi priložnosti tudi sam pojmuje,
izhaja pa iz Fortinijevega vabila, da je v kritičnih časih to edina pot
do novih enotnosti. [»Pridevniki, s katerimi lahko komentirali te moje
izjave, že obstajajo in sektaški je eden od njih. Menim, da smo dolgo
trpeli zaradi pomanjkanja „sektaštva“, to je drže do ločitve, ki je
edini način za novo enotnost,« v @fortini2016cunning, 128] Do svojega
arhitekturnega občinstva, do »arhitektov levice«, pa bi vseeno
zagovarjali, da zavzema nekakšno spodbujevalno stališče. Že zaključek
*Teorij in zgodovine* je možna posledica dramatizacije protislovij
spodbuda in izziv arhitekturi. Vsekakor gre za namero, ki jo
upravičeno tudi tu prepoznamo: možnost odčarane kritike, ki jo, tako
ali drugače, arhitekturna praksa »vzame na znanje« in, tako ali
drugače, prevede v delovanje. Seveda izhaja, da mora vsako takšno
dejanje ohranjati utopičnost, ampak to nikoli ni izključevalo realnih
učinkov arhitekture na svet.

Četudi so dalje omenjene »intelektualne iluzije« [»Med intelektualnimi
iluzijami, ki jih je treba najprej poraziti, je ta, ki je, zgolj z
vrednoto podobe, usmerjena v anticipacijo pogojev nekakšne arhitekture
za „osvobojeno družbo“. Kdor predlaga takšen slogan, če postavimo na
stran njihov boleč utopizem, se izogiba vprašanju, ali ta cilj lahko
dosežemo brez lingvistične, metodološke, strukturne revolucije, ki
presega preprosto subjektivno voljo ali preprosto posodobitev sintakse,«
v @tafuri1969critica, 78] in »pogoji razrednega boja« [»Refleksija o
arhitekturi kot kritika konkretne ideologije, ki jo „izvaja“ arhitektura
sama, ne more drugega, kot iti dlje in doseči specifično politično
dimenzijo v kateri je sistematična destrukcija mitologij, ki vzdržujejo
razvoje, le eden od ciljev: in samo prihodnji pogoji razrednega boja
bodo dali vedeti, če je to kar si zastavljamo avantgardna ali
retrogardna naloga,« v @tafuri1969critica, 79] danes v veliki meri
neprepoznavne »dnevnopolitične« kategorije, menimo, da imajo za
arhitekturno teorijo – kot kategorije razreda, produkcijskih odnosov,
kapitalističnega razvoja in »dialektike« intelektualnih poklicev, ki se
ravno z uveljavljanjem svoje avtonomije določajo do njih – tudi
»dolgotrajnejše« posledice, namreč da so predlog ene
kritično-zgodovinske ali analitične prakse.

Zgodovinska in materialistična obravnava arhitekture bo po našem mnenju
toliko bolj ključna v dobi, ko bo prevladujoča ideologija svoj čas
pojmovala ravno kot »konec zgodovine« in »konec ideologije«. 

# Predmet kritike po koncu zgodovine

> [L'aura] non c'è, è andata via
> 
> – F\. Neviani, *Laura non c'è*, 1996

> [Avre] ni več, odšla je drugam
> 
> – M\. Rudan, *Laure ni več*, 1998

Rekontekstualizacija je lahko neskončno opravilo. Če jo vzamemo za pogoj
aktualizacije, se poraja vprašanje o posplošljivosti teoretske niti in
njenih zaključkov, ki smo jih zasledovali. Natančneje, če smo
epistemološke in zgodovinske pogoje teorije izpeljevali na podlagi
»italijanske situacije« in če smo dokazovali distinktivnost in bistven
doprinos teorije v razmerju do te situacije tudi na podlagi njene
konceptualne strogosti do bistvenih kategorij modernističnega
industrijskega kapitala in razredne sestave, se poraja vprašanje o
italijanskosti in modernosti teh doprinosov. Enostavneje rečeno, poraja
se vprašanje o relevantnosti tafurijanske kritike arhitekturne
ideologije ter zgodovinskega in materialističnega pojmovanja
arhitekture, ki ga smatramo za bistven temelj te kritike, za vedno
nejasno historizirano »današnjo in tukajšnjo družbenopolitično
situacijo«. Ne gre pa zgolj za nalogi interno vzniklo zagonetko: če je
eden poglavitnih ciljev kritike arhitekturne ideologije možnost
historiziranja lastnega cikla, potem mora aktualizacija te kritike prav
tako biti svojevrstno historiziranje aktualnega.

Najprej mora biti jasno, da obravnavano gradivo »samo sebe« vsekakor
»razume« za globalno posplošljivo in v razpravi, v kolikor opisuje krizo
modernizma in simptome njegovega konca, vsekakor prepoznamo tudi naše
predstave o današnjem stanju v arhitekturi in upali bi si trditi, da, če
ne analiza, vsaj izhodiščni sentiment, si te predstave delijo vsi, ki si
zastavljajo vprašanje o vlogi in učinkih svojega disciplinarnega
početja. Kot smo že omenili se kritika arhitekturne ideologije kot
disciplinarni projekt loti vseh globalnih alternativ, ki so imeli tudi
svoj arhitekturni izraz. Že v »glavnem« eseju so predmet vse evropske
zgodovinske avantgarde, weimarska socialdemokracija in Le Corbusier.
Nadaljnje delo *Inštituta za kritično in zgodovinsko analizo* bo
obravnavalo tudi arhitekturni projekt ameriškega *New Deala*, enklavo
Rdečega Dunaja, ter arhitekturo Oktobrske revolucije in NEPa. Seveda,
saj bistvena kategorija *plana*, kot ga pojmujejo Tafuri in operaisti,
ni seštevek aspiracij vodilnih osebnosti vsakega »gibanja«, ampak se
tako rekoč vsem tem aspiracijam »prikrade« kot zahrbtna strukturajoča
nujnost, ki jo vzpostavlja družbena abstrakcija. (Seveda pa je iz
njihove ločene obravnave jasno tudi, da so prav tako priznane vse
potrebne specifike.)

Marsikdo bi lahko pogrešal tovrstno analizo razvoja modernizma na
primeru Jugoslavije, na kateri se, kot tretji globalni alternativi
znotraj (ali izven) hladne vojne, pasivno gradi mit (ne samo
arhitekturni) nekakšnega Atlantisa 20\. stoletja, ki je dobrodošlo
umeščen v splošno modernistično nostalgijo. [V mislih imamo dogodke kot
so @stierli2018concrete; in novo poglavje @frampton2020yugoslavia]
Vsekakor enostavna in površinska »primerjalna zgodovina« ne odpravi
potrebe po resnih kritično-zgodovinskih obravnavah modernizma
neuvrščenih ali modernizma tržnega socializma, a nekaj dostopnih (in
omejenih na Slovenijo) podobnosti bi vseeno našteli. Prvič podoben
značaj italijanske Rezistence in Osvobodilne fronte – obe pod hegemonijo
ustrezne komunistične partije, a vseeno dovolj široko zastavljeni, da
privzemata ljudski in kulturni značaj – in podobna zaostalost gradbenega
sektorja pomeni vsaj začetno iskanje primerne nove popularne in
realistične arhitekturne sintakse. Iniciativo na specifičnem področju
analize likovne umetnosti NOB je že prevzela slovenska umetnostna
zgodovina in zdaj tudi začela zbiranje prvih gradiv za analizo
arhitekture mreže zadružnih domov. [gl. @malesic2021skupno] Prav tako
je sprotno dokumentirana, a nikakor poglobljeno analizirana v odnosu do
arhitekture, razprava, kjer so teme strukturna funkcije stanovanjske
gradnje in možnosti njene umestitve v produkcijske in konsumpcijske
cikle vedno prisotne. Parametri teh razprav bodo vedno razpoložljivost
delovne sile (ki bo, kakor v Italiji, strukturno prihajala iz juga
države), organska sestava, racionalizacija in industrializacija
gradbišča ter integralno vključevanje inženirskih poklicev v celostno
načrtovanje mest (ter s tem cikla produkcija-distribucija-konsumpcija).
Navedemo lahko, na primer, *Program 80*, interesne skupnosti za urbani
razvoj, komunalno opremo in zemljiško ureditev Šiške, ki je združevala
gospodarske, politične in samoupravne akterje pod enotnim 10-letnim
investicijskim planom:

> Pri projektiranju sosesk »Programa 80« velja načelo »integralnega
> projektiranja«, ki poteka paralelno znotraj organizacijske sheme
> projektnih timov, ki jih za programirano obdobje gradnje sosesk v
> ureditvenem območju angažira »Program 80«. Zazidalni načrti in vsi
> ostali načrti kot detajlna urbanistična in investicijsko-tehnična
> dokumentacija so močno povezani s samim gradbenim procesom. Zato smo
> proces projektiranja tesno povezali s tehnološkim procesom
> organizacije in nastajanjem naselja (gradbena tehnologija). Glavne
> oblike racionalnega projektiranja združujemo v naslednjih pojmih:  
> 
> tehnološko projektiranje (pri izdelavi projekta je treba že upoštevati
> tehnologijo gradnje, ki bo uporabljena);
> 
> industrijsko oblikovanje (projektiranje), ki jemlje v vsakem primeru
> kot izhodišče funkcijo elementa, proizvoda, objekta ter pomeni v
> bistvu metodo najbolj dosledne kompleksne racionalizacije;
> 
> metoda limitiranja cen (zasnova na konkretnih izbranih projektih in na
> preizkušeni tehnologiji gradnje, ki je proučena v detajle);
> 
> metoda projektiranja v sistemu (metoda predstavlja organski del glavne
> naloge, tj\. graditev v sistemu in strnitev projektnega in gradbenega
> procesa v neizmenično odvisnost).
[@1973program, priloga; gl. tudi *Progetto 80*, italijanska petletka,
ki je tudi obroben predmet razprave v @tafuri1970lavoro]

Sami po sebi (sploh pa v neurejeni obliki) se ti »faktoidi« za
arhitekturno razpravo ne zdijo bistveni, a glede na to, da je slovenski
modernizem v splošnem nemisljiv izven projektov določenih na strukturni
ravni, bo manko razmisleka o strukturni povezanosti »realizirane
utopije« in povojnega razrednega kompromisa onemogočal pomenljive
ugotovitve o vzrokih današnjega (slovenskega) statusa arhitekture in
pogojih arhitekturnega dela nasploh. Da se na jugoslovanskem
demokratičnem planiranju prav tako kopičijo in stopnjujejo protislovja
med subjektivnimi cilji in objektivnimi pogoji, je jasno, če ne drugače
iz postopnega uvajanja (ali prevlade) tržnih mehanizmov in, na primer na
področju upravljanja prostora, dvoumnega statusa družbene lastnine in iz
tega izhajajočega naraščajočega konflikta med nosilci lastninskih pravic
in nosilci upravljavskih. V tem smislu slovenska razprava o prostorskem
razvoju – tako v strokovnih publikacijah (*AB* ali *Sinteza*) kot v
»oporečniškem« tisku (*Mladina* ali *Tribuna*) – po svoji problemski
tematiki prav nič ne zaostaja za kapitalističnim zahodom. 

Je pa res, da razvojna protislovja jugoslovanskega modela zaostajajo za
zahodnim v smislu, da jih jugoslovanska razvojna ideologija ni bila
zmožna eksternalizirati. Ideologija objektivnosti racionalizacije
nastane s kapitalističnim razvojem in znotraj jugoslovanskega modela
zgolj preživi, kjer se mora racionalizacija produkcije vedno sklicevati
na delavski razred. Temeljni element mistifikacije razrednega značaja
razvoja – zasebna lastnina kapitala – je torej zavrt, kar
realsocialističnim predlogom razvoja daje negotov in neorganski značaj.
Despotizem tovarniške organizacije, neučinkovitost zadovoljevanja
družbenih potreb in sama kulturna obravnava teh pojavov in dejavnikov
ima zato v realsocialističnih družbah neposredno političen značaj, med
tem ko se v kapitalističnih ekonomsko in politično zdi ločeno.
Privatizacija stavbnega fonda je pospremljena z opombo, da bo zaradi
privatizacije in denacionalizacije »[s]ocialna in ekonomska učinkovitost
stanovanjske oskrbe radikalno povečana«. [iz parlamentarne razprave l\.
1991 o Osnutku Stanovanjskega zakona, navedeno po @mandic1996stanovanje,
152] Jasno je kako obnovo kapitalizma spremljajo klasične ideološke
mistifikacije, ki se tičejo zgodovine in tehnologije: buržoazni družbeni
odnosi so naravni, vsak poskus njihove politizacije – to je
problematizacije zaradi spremembe ali zgolj preverbe – je umeten in
predstavlja odklon od naravnega zaključka zgodovine; in dalje, četudi je
ideologija napredka na ravni posameznih zavesti ovržena, ta vztraja
znotraj splošne ločitve političnega od ekonomskega, ki kapitalističnemu
razvoju pripisuje inherentno racionalnost in učinkovitost, ki se bo s
tehnološkim razvojem, ki naj bi prav tako izhajal iz narave kapitala,
zgolj stopnjevala.

Dozdevna apolitičnost involucije temeljev modernistične družbe ima tudi
svoj odraz na področju arhitekture (in umetnosti), ki zgodovino svoje
modernistične angažiranosti, in posledičnega razočaranja, korigira z
»vrnitvijo k estetskemu« in ponovnemu pojmovanju arhitekture kot
umetnosti – vsekakor menimo, da gre za eno izmed arhitekturi lastnih
refleksij o njenem »izgonu« v nadzidavo. Toda redefinicijo arhitekture,
ki se je v osemdesetih letih 20\. stoletja vršila v strokovnih
publikacijah, bi podcenjevali, če ne bi v njej prepoznali tudi nov
program arhitekture kot družbene skupine in poskus produktivnega vstopa
v novo poslovno in pravno realnost. Da je arhitektura pravzaprav
umetnost, je imelo tudi družbeno-ekonomske posledice, ki jih je
predvidela pravna novost *Zakona o samostojnih kulturnih delavcih*
sprejetega leta 1982. Smisel zakona, iz stališča njegovih pobudnikov in
zagovornikov, je primarno ureditev pravno-formalnih možnosti za
ustanovitev zasebnih galerij, ki naj bi naslovile tudi slab (oziroma vse
slabši) socialni položaj svobodnih umetnikov in umetnic. [epizodo
gospodarskega »osamosvajanja« umetnikov povzemamo po
@kocjancic2019zacetki, 29-33] Zakon je umetnikom združenim v trajne ali
začasne delovne skupnosti – prva bo Equrna, trajna delovna skupnost
samostojnih kulturnih delavcev Ljubljana in prva zasebna galerija, kmalu
za tem pa tudi Delovna skupnost samostojnih arhitektov, DESSA –
dovoljeval zasebno razpolaganje z njihovimi deli in storitvami, to je
podeljeval vse pravice, ki v buržoaznih družbah pripadajo zasebnim
akterjem na prostem trgu. Da bi zakon veljal tudi za arhitekturno
dejavnost, je bila potrebna tudi potrditev, da je tudi arhitektura
umetniška dejavnost. Uradno takšno potrditev, kakor je nam znano, dobi
na 28\. seji Sveta za kulturo pri Predsedstvu Republiške konference
Socialistične zveze delovnega ljudstva Slovenije, kjer je obravnavano
*Gradivo o aktualnih vprašanjih arhitekture na Slovenskem*, ki so ga
pripravili arhitekti. [gradivo in razprava okoli njega je objavljena v
@1982aktualna]

Splošna tafurijanska »shema« arhitekturnega avtobiografskega
napovedovanja lastne usode ravno preko pozitivnih predlogov izvzemanja
iz te usode se zdi na primeru postmoderne rekvalifikacije dodatno
dopolnjena:

> Po tem, ko so ideološko predvideli železni zakon Plana, se arhitekti,
> nezmožni zgodovinskega branja opravljene poti, upirajo skrajnim
> posledicam procesov, ki so jih sami pomagali sprožiti.
[@tafuri1969critica, 77]

Če je vstop arhitekturnega projekta v razsežnost kapitalističnega plana
označeval objektivno ublagovljenje arhitekture, je nadaljnje estetsko
»raziskovanje« tega ublagovljenja in spremljajoče iskanje novih
»zavezništev« onkraj industrijskega kapitala napovedovalo prekarizacijo
arhitekturne dejavnosti. Specifični razvoj slovenske arhitekturne
prekarizacije, v kolikor je postmoderni obrat k trgu moral potekati s
samostojnim političnim delovanjem v samoupravnih organizacijah, ni zgolj
lokalna »zanimivost«, ampak postane eden jasnejših primerov globalnosti
sprememb, ki jih je od arhitekture kot institucije terjala postmoderna
reorganizacija kapitala. 

Stališče iz katerega Tafuri historizira vse postmoderne obrate je
kritično stališče do modernizma. Zanj ima postmoderni obrat zelo jasne
korenine v krizi modernizma. Ob priložnosti, ko se nova arhitekturna
stopnja že samorazume kot »post«, ta pojem zavrača kot pomirjujoč
diskurz, ki se slepi glede preseganja protislovij modernizma.
Hipermodernisti, kot je označil Portoghesija ter »opinion makers«
Jencksa in Sterna, so s tem podlegli iluziji preprostih formalnih in
pomenskih sintez. Delno posvojitev vseh razkrojevalnih učinkov
postmodernizma in toda hkratno zaupanje v njih označi za »nepopolni
nihilizem«. [@tafuri2002storia, 193] Na tej točki so v Tafurijevih (in
Cacciarijevih) besedilih že bolj izrazite ničejanske teme.
Postmodernizem vsekakor označuje tudi »krizo marksizma«, toda iz tega
nikakor ne sledi, da je prišlo do bistvenih sprememb v tafurijanskem
pojmovanju kapitalističnega razvoja. »Pozno« tafurijanskim nihilističnim
poudarkom bomo kasneje sledili v njihovi marksovski obliki, kjer so
jasna njihova produktivna teoretska raba.

Vseeno pa je treba delno upoštevati Jamesonovo alternativno
historizacijo postmodernizma. Vsekakor je pri njemu modernistični razvoj
prav tako podlaga postmodernizmu, oziroma, zanj prehod od enega k
drugemu ne predstavlja temeljnih sprememb na ravni produkcijskega
načina. Vzame pa postmoderno stanje kot izhodišče, torej ga obravnava
kot specifično kulturno logiko poznega kapitalizma. Da prepozna
strukturne spremembe kapitala kot porast financializacije, torej
»osvoboditev« kapitala od industrije, se nam vsekakor zdi ustrezno
izhodišče za obravnavo nekaterih spektakularnih in formalističnih teženj
postmoderne arhitekture. Toda vseeno smatramo Jamesonovo obravnavo
specifično arhitekture za manj odločilen del njegove teorije
postmoderne, kar je razumljivo. Sploh pa, menimo, nas nedavni
kapitalistični razvoj vrača nazaj v nekatere termine izrazito
industrijskega kapitala. V kolikor je Tafurijeva kritika arhitekturne
ideologije kritika kapitalistične modernosti, je na mestu, ravno, ko
prevladuje postmoderno stanje, določena mera pozornosti glede uporabe
pojma modernega, da ne bi v samem rezultatu opravljene analize pozabili
na njena izhodišča. Namreč danes je, menimo, v zgodovinski
jukstapoziciji prevladujoče ideologije o koncu zgodovine ravno pojem
modernega – torej moderno kot pred postmodernim – privzelo nekatere
pomene kolektivnega poskusa kapitalizmu podati cilje, ki presegajo
profit. Da danes, iz stališča Atenine sove ob neoliberalnem somraku,
kritiko ideologije arhitekture vidimo pozitivno uresničeno v
arhitekturnem postmodernizmu – analogno kot bi lahko, na primer,
operaistični kritiki planskega kapitalizma očitali ali pripisovali
pozitivno razrešitev v postfordističnem prelomu povojnega razrednega
kompromisa in posplošitev neoliberalizma – je svojevrstna ideološka
funkcija kapitalističnega razvoja. Seveda iz tega prav tako sledi, da
aktualizacija kritike arhitekturne ideologije kot kritike postmoderne
arhitekturne ideologije ne obrisuje pozitivnega predloga modernistične
nostalgije.

Kdor želi predstaviti kakršen koli pojav v njegovem razvoju, se mora
neizogibno in nujno soočiti z dilemo: ali prehiteti čas ali ostati v
ozadju. Toda »poglavitne teme našega časa« se, menimo, ponujajo pod vse
bolj enostavnima ključnima besedama »zeleno« in »digitalno«. Nikakor ne
moremo trditi, da arhitekturna kultura ti dve področji šele presenečeno
odkriva: ekologija in informacijske tehnologije tekom celotnega
povojnega razvoja modernizmu predstavljata vseprisotne izzive ali
očitke. Toda prav tako se, tekom tega razvoja, ne pojavljata kakor
enotni ali zvezni arhitekturni tendenci na ravni oblikovanja. Onkraj
številnih oblikovnih ali projektantskih principov, ki jih v tej luči
lahko privzemajo arhitekturni akterji – popis teh je na kakšen drugem
mestu vsekakor na mestu –, ti dve temi pravzaprav problemsko vse ožje
izpostavljata vprašanje položaja arhitekture znotraj najvišje
opredeljenih družbenih procesov in vprašanje arhitekturnega dela znotraj
tako opredeljene arhitekture. Enostavneje, izpostavljata vprašanje
arhitekture kot sredstva in vprašanje arhitektovih sredstev v specifični
situaciji globalne eksistencialne grožnje, ki terja ukrepanje na ravni
najvišje opredeljenih tehnik poseganja.

Učinki tako imenovane *financializacije* nepremičninskega in torej
gradbenega sektorja ter posledičen pritisk na uvajanje novih
(digitalnih) metod racionalizacije v proces oblikovanja in načrtovanja
so se – ocenjuje Peggy Deamer v eni pomembnejših zbirk
arhitekturnoteoretske obravnave digitalnega razvoja v arhitekturi
[@deamer2010introduction] – znotraj arhitekture odražali kot »subtilen
premik« od oblikovnih vprašanj do vprašanj fabrikacije in produkcijskega
procesa. S tem pa – in to je izhodišče arhitekturnoteoretskega početja,
ki ga uvaja – se zdi, da se je arhitekturna teorija umaknila v
»postkritično« in na škodo kritične obravnave parametričnega oblikovanja
in digitalne fabrikacije dala prednost zgolj »delanju stvari«.
[@deamer2010introduction, 19-20] Zbrana besedila vsekakor so kritični
prispevki do vprašanj, ki pestijo profesionalce ob vsaki disruptivni
uvedbi novih tehnologij na področju zidave: na kocki so, kot se tem
lotevajo zbrani avtorji, (pojemajoča) vloga obrtništva, učinkovitost
sodelovanja med vpletenimi poklici, napetosti med determinirajočimi
dejavniki standardizacije in oblikovnimi intencami ter status avtorstva,
avtentičnosti in odgovornosti znotraj instrumentaliziranega oblikovanja.
Toda v splošnem – kot je zapečateno s prispevkoma urednice in urednika
[@deamer2010detail; @bernstein2010models] – kritični moment te zbirke
pomeni zgolj začasno distanco, ki služi učinkovitejši profesionalni
reorientaciji za popoln sprejem vseh površinskih pomenov tehnološkega
razvoja, [iz te ocene je vsekakor potrebno izključiti Framptonov
prispevek: »[Mlajšo generacijo bi morali] opremiti z globljo in
treznejšo predstavo o mejah modernega projekta v političnem in tehničnem
smislu. Hkrati bi morali spodbujati preučevanje enako škodljivega
sodobnega Kandidskega mita o neizogibnih tržnih koristi globalizacije v
kombinaciji z namišljenim svetovnim zmagoslavjem liberalne demokracije,«
v @frampton2010intention] brez temeljitejšega prikaza dinamik, ki ta
razvoj sploh ženejo, in subjektivnih pomenov, ki jih tehnologija
privzema v razrednih družbah.

Omenjeni »igralci« na novem terenu, ki ga vzpostavlja digitalni razvoj,
so zgolj profesionalci – arhitekti, inženirji, gradbeniki – s tem, da je
predpostavljen vedno individualen arhitekt, z gradbenikom pa je vedno
mišljen gradbeni obrat kot enota. Premostitve vrzeli med oblikovno ali
načrtovalsko in izvedbeno fazo (v besedilu jih izvajajo tako imenovani
»thinkers« in »makers«), ki jih optimistično pripisujejo novim
tehnologijam, tako označujejo zgolj večjo integracijo menedžerskih nalog
različnih področij, ne pa, na primer, vrzeli med intelektualnim in
ročnim delom znotraj posameznega področja, ki jih tehnologija pravzaprav
poudarja in formalizira. Tržni imperativi so ves čas prisotni kot
objektivna danost (torej kot teren, ne igralec), kar ne spodbija zgolj
izražene namere odmika od pragmatizma, ampak tudi izključuje vse
subjektivne luknje navidezno objektivne racionalizacije. Niti naročniku
ni dovoljeno biti subjektivni »zastopnik« kapitala: v »objektivistični«
zasnovi je povezovanje in sodelovanje vseh partikularnih interesov
enačeno z usodo celotnega sistema:

> Sčasoma se bodo [digitalne informacije, ki jih nosijo nove
> reprezentacijske tehnologije (BIM),] neposredno povezale z
> računalniško nadzorovanimi fabrikacijskimi napravami in temeljito
> zapolnile vrzel namere-izvedbe ter popolnoma odpravile potrebo po
> tradicionalni gradbeni dokumentaciji.
>
> Spodbude so očitne in orodja se hitro razvijajo, toda osnovni poslovni
> modeli se morajo še naprej spreminjati, da bi omogočili nadaljnje
> izboljšave procesa. Zlasti so potrebne prilagoditve znotraj
> zgodovinsko asimetričnega odnosa med arhitekti in njihovimi naročniki
> tako, da se koristi pričakovanih izboljšav procesa uravnotežijo v prid
> projektanta. Toda če bodo arhitekti te koristi opredelili zgolj v
> terminih oblikovnih ali estetskih ciljev, bodo zamudili temeljno in
> edinstveno priložnost, ki jo prehod ponuja. Zapolnitev vrzeli
> namera-izvedba, premostitev dejanj »thinking« in »making«, bo v enaki
> meri gnana iz strani naročnikove želje po povečanju produktivnosti in
> doseganju bolj predvidljivih izidov, zato so poslovni modeli, ki se
> tesneje opirajo na sodelovanje med thinkers in makers, oblikovalci in
> gradbeniki, arhitekti in inženirji, lahko vezani na rezultate.
> Trenutni preskriptivni poslovni modeli častijo najnižje izhodiščne
> fiksne cene in standardne oblike pogodb. Pri alternativnih
> »integriranih« pristopih so tveganja in nagrade odvisni od merljivih
> rezultatov, opredeljenih na začetku projekta, uspeh ali neuspeh pa se
> pripisuje celotni ekipi [arhitektov, inženirjev, gradbenikov], ne le
> posameznemu udeležencu. Potencialnih nagrad uspeha torej ne izkorišča
> le največji porabnik (gradbenik), temveč tudi projektant, čigar
> vpogled (in iz tega izhajajoči rezultati) že od začetka narekuje
> rezultat. Vsi zmagajo, ali vsi izgubijo.
[@bernstein2010models, 196]

Za nas zbirka torej ne presega »objektivističnega« pojmovanja
tehnologije, katerega kritika je eden teoretskih temeljev klasičnega
operaizma in kasneje tafurijanske kritike arhitekturne ideologije.
Presenetljivo je v kolikšni meri sodobna arhitekturna tehnološka
govorica razpravo postavlja nazaj v okvirje že prehojene poti povojnega
modernizma. »Brezšivnost« procesa od načrtovanja do izvedbe digitalne
tehnologije zgolj simulirajo na omejeni ravni, ki jo vzpostavljajo, in
neposreden prevod te brezšivnosti na teren je zgolj stava, ki jo je
treba šele preveriti. V tej luči bodo, na primer, *Arhitekturne
industrije* [@lloyd2016industries] nove tehnologije obravnavale
razširjeno, v odnosu do širših družbenih procesov, kot *industrijo*.
Tako uredniki v uvodniku:

> Prehoda od obrtne k industrijski gradnji ni mogoče ločiti od sprememb
> družbenih razmerij in produkcijskih razmerij v gradbeništvu.
> Arhitektura je bila neločljiv del znanstvenega menedžmenta pred in po
> avantgardni projekciji idealizirane strojne estetike. Torej
> raziskovanje danes arhitekture *kot* industrije neizogibno pomeni
> upoštevati delitev dela, upoštevati delo tistih, ki sodelujejo pri
> gradnji in tudi prepoznati, da arhitekti niso zgolj »avtorji«, temveč
> tudi »delavci«. Prepoznati to znotraj discipline zahteva kritično
> prevpraševanje položaj in odnos arhitekturnega poklica do drugih
> elementov znotraj delitve dela.
[@lloyd2016industries, 6]

V tej zbirki je vsebovano tudi poročilo kjer je nova tehnologija
preverjena »na terenu«, natančneje na gradbišču hotela v peti gradbeni
fazi s 140 delavci, vrednost te javno-zasebne investicije je bila 50
milijonov funtov. [@beech2016site, 305] Kjer se mora povečana
učinkovitost medstrokovne komunikacije izkazati niz hierarhijo, so
ugotovitve nasprotne optimizmu »thinker-makerjev«. Težave nastopijo
predvsem med prenosom iz idealno izračunanih zaporedij in urnika
gradbenih in obrtniških del v realne razmere. Avtorji izpostavljajo, da
se je potek del, kljub popolni posvojitvi BIM tehnik s strani
projektantskega obrata in načrtovalca ter izvajalca strojnih ter
elektroinštalacij, nenehno zanašal na »neumne« tehnologije tiskanih in
ročno dopolnjenih dokumentov ter na sprotne ustne koordinacije med
različnimi delovnimi skupinami znotraj katerih so neformalna znanja –
med drugim tudi znanje pakistanščine in vzhodnoslovanskih jezikov –
ostajala ključna za izvajanje korekcij. [@beech2016site, 307] Vsekakor
iz tega, menimo, ne sledi, da gradbišče absolutno uhaja kakršni koli
digitalni racionalizaciji: del napak se z razvojem na ravni programske
opreme lahko odpravi – na primer natančnejša programska opredelitev
odnosa med zidanimi stenami in stenskimi odprtinami –, prav tako napake
v zvezi z razporejanjem urnika del, če ne drugače z upoštevanjem
časovnih »rezerv«. Toda ravno menedžerske tehnologije v svojem realnem
delovanju, še posebej ob svojih mejah, razkrivajo odločilnejša področja
njihove disruptivnosti: značaj digitalnih rešitev se razkriva ob realnih
problemih, ki jih naslavlja. Nekateri so povsem »tehnični« in so v
gradbenem sektorju že dolgo naslavljajo. Tradicionalni problem gradbene
industrije, iz stališča kapitala, je gradbeno dejavnost približati
zanesljivosti ostalim fiksnim industrijam. Te imajo tehnični
»privilegij«, da lahko sebi ustrezno delovno okolje, to je prostorske
pogoje delovnega procesa, izgradijo. Predmet gradbene industrije pa je
ravno ta gradnja, kar pomeni, da nikoli ne more potekati znotraj
nadzorovanih prostorskih pogojev: na severni polobli je po svoji naravi
sezonska in podvržena vremenskim presenečenjem. Tradicionalno je visoko
specializirana, končni izdelek je kompleksen produkt ločenih
podindustrij in obrti. Dela izvaja po naročilu, kar pomeni, da ne more
kopičiti zalog. V povezavi s tem in s praktičnimi prostorskimi
omejitvami – gradbeni obrat je, v primerjavi z enostavnejšimi
industrijami, relativno bolj vezan na lokalni trg – je odvisna od
sezonske delovne sile. [Te specifike obravnavajo že weimarski
socialdemokratski arhitekti, gl. @wagner1987socialization]

Te omejitve naslavljajo že modernistična vpeljava strojev,
standardizacija gradbenih in montažnih elementov ter spremljajoče
organizacijske in načrtovalske tehnike (katerih sestavni del je
zgodovinsko vsekakor tudi arhitekturna disciplina bila). So pa te
»pomanjkljivosti« lahko tudi funkcionalno izkoriščene, kot smo videli
pri Tafurijevi analizi vloge povojne gradbene industrije v razmerju do
prvakov italijanske industrije, kjer je nizka organska sestava
gradbenega kapitala lahko anticiklični dejavnik.

Nove digitalne tehnologije pa, menimo, predstavljajo še nadaljnji korak
k poskusu vzpostavitve laboratorijskih pogojev znotraj gradbenega cikla
in nasproti vsem subjektivnim kontingencam delovnega procesa (končno)
postavljajo enoten »objektivni skelet«. Da idealno zaporedje gradbenih
in obrtniških del ter njihov urnik, ki ga prek simulacije vzpostavi
program, v realnosti neizogibno propade, ni absolutno odločilno za
uporabnost te tehnologije. Odločilnejša je funkcija merila, ki jo
tehnologija predstavlja investitorju. Nasproti tempu in postopku, ki je
do sedaj v domeni delavca, predstavi neodvisen in racionaliziran
asddsa (intenzificiran) ideal postopka. Šele ob tem merilu vse rezerve
postanejo »človeške napake« in, kot reden pojav, »nepokorščina«. V tem
smislu BIM ne predstavlja kvalitativne ali zgodovinske novosti, zato
menimo, da je Panzierijeva kritika tehnologije – seveda kritika
nevtralnosti in objektivnosti tehnologije v razredni družbi – v tem
primeru še vedno veljavna. Kot smo rekli je mehanizacija gradbišča,
četudi gradbena industrija tradicionalno upočasnjeno uvaja
tehnologijo, že »star« pojav. Novost je stopnja mehanizacije in
spremenjen značaj dela, ki pripada tej stopnji, ter, za arhitekturo,
predvsem zaporedna stopnja cikla na katero se osredotoča.

Če se vrnemo na poročilo, je potrjeno, da uvedba novih digitalnih
tehnologij je zmanjšala število delovnih ur na stopnji same gradnje,
torej skrajšala čas oziroma povečala učinkovitost gradnje. Toda povečal
se je obseg odgovornosti in čas trajanja vseh načrtovalskih stopenj, ki
jih program zdaj združuje v eno kontinuirano stopnjo. [@beech2016site,
308] In na ravni tehnologije je predvideno, da se delo na modelu ne
konča z začetkom ali koncem gradnje, mišljeno je, da model simulira
celoten »življenjski cikel« stavbe. Povečan pritisk, ki izhaja iz
povečane odgovornosti, ki je naložena načrtovalski stopnji, in iz
povečanega deleža investicije, ki jo stopnja predstavlja, v nobenem
smislu ne namiguje na »nadgradnjo« stroke, kot je, na primer,
optimistična Deamer. [gl. @deamer2010detail, 86-88]

Svoj optimizem izpeljuje iz potenciala, ki ga prepozna v »krizi
detajla«. Vsaka temeljna sprememba v produkcijskih pogojih, uvaja, se v
kulturi kaže kot tesnoba glede ekspresivnosti ter družbenega in
ekonomskega statusa detajla. V parametrizaciji sredstev detajliranja
vidi razrešitev serije problemov, ki so jih, našteva, opredelili Ruskin,
Loos, Semper, Otto Wagner in nazadnje fordizem 20\. stoletja. Ruskinova
moralistična obsodba ločitve načrtovanja detajla od njegove izvedbe in
iz tega izhajajoča odtujitev obrtnika od svojega predmeta dela ter
posledični (profesionalni) antagonizem med obrtnikom in arhitektom je
razrešen z avtomatsko in mehansko podprto enotnostjo načrtovanja in
izvedbe. Identiteta dejanja risanja s strojnim navodilom sublimira
ločeni vlogi obrtnika-arhitekta v enotno vlogo. Ta prav tako razrešuje
Loosovo oceno, sicer nasprotno Ruskinovem moralizmu, problema ločenosti
arhitektove (umetnikove) težnje po avtobiografskem izrazu od neosebne,
proceduralne in po naravi »moderne« obrtniške dejavnosti. Loosov
»arhitekt brez svinčnika« je uresničen v proceduralni logiki s katero
zdaj upravlja. Kako nova tehnologija razrešuje nadaljnje stopnje –
Semperjev model primata detajla, ki naj bo izhodišče procesa, Wagnerjeva
napoved specializacije in kompozicijske neodvisnosti ter fordistični
problem vertikalne hierarhizacije in fragmentacije – je še bolj dvoumno
razdelano. Vsekakor bi lahko rekli, da način dela, ki ga predvideva
parametrično detajliranje, predstavlja težnjo poenotenja z vsemi
nadaljnjimi postopki. Toda ravno to poudarja omejenost tehnologije.
Odločilnost izhodiščnega parametra je v popolnem skladu z uporabnostjo
novega orodja (poenostavljanje in poenotenje procesa) a hkrati
spodkopava potencialno (spekulativno) uporabnost, ki jo obljublja,
oziroma mu jo pripisujemo (oblikovalčeva svoboda, ki izhaja iz
obvladovanja izdelovanja in sestave). Nikakor nimamo v mislih
izključevanja procesu zunanjih estetskih teženj: pod dvomom je
upravičenost razumevanja arhitektove vloge kot odločilne pri tem
procesu. Revitalizacija detajla pod novimi produkcijskimi pogoji,
izpeljuje Deamer, restrukturira delovni proces s posledicami v
subjektiviteti arhitektov in oblikovalcev. Popredmetenje procesa v
kompleksno orodje naj bi izenačevalo vstopne pogoje vseh, ki uporabljajo
orodje. Avtomatizacija vsekakor odpravlja pogoje starih delovnih
razmerij mojster-vajenec in prek osvobajanja od vsebine dela enači
različne udeležence v procesu, toda prav tako formalizira in poudarja
nove hierarhije. Da je privzemanje novih menedžerskih vlog videno kot
razkroj hierarhij, razkriva zgolj ideološko pojmovanje predmeta, ki se
»menedžerira«. Da za delovnimi sredstvi in gradivom, ki jih program
upravlja, ni prepoznana – oziroma se spekulira, da bo izginila – nujna
aktivacija človeške delovne sile, ni zgolj funkcija profesionalne
arhitekturne diferenciacije do ostalega procesa, temveč zakriva tudi
lasten značaj objekta znotraj procesa. Optimizem glede reorganizacije,
ki jo napovedujejo nova orodja, sloni na zaupanju, da bo arhitektura
subjekt, ne objekt te reorganizacije. To je razvidno tudi v avtoričini
oceni posledic demografskih sprememb – te se, dodajamo, postavljajo že
šestdeset let in njihova vsakokratna uresničitev privzema prav nasproten
značaj od optimističnega pojmovanja –, ki bodo razkrojile identitete
»šefa« in »osebja« ter vzpostavile novega svobodnega subjekta sodobne
produkcije – tako imenovani »neoprofesionalci« –, ki bo prosto stopal v
razmerja s sebi enakimi. [@deamer2010detail, 87-88]

Tehnološki optimizem, »objektivistične« – če vztrajamo na Panzierijevem
pojmovanju – interpretacije tehnološkega napredka in obravnava
tehnologije v idealizirani in čisti formi imajo svojo vlogo tudi znotraj
arhitekturne obravnave okoljske krize. Oziroma je pravilneje reči, da
primarno učinkujejo znotraj splošne (politične in ekonomske) obravnave
te krize, parametre katere privzema arhitektura (ali pa so ji te
naloženi). Problem se namreč izkaže že ob definicijah vseh parametrov
krize. S tem ne mislimo na dvome o obstoju ali o človeškem izvoru
okoljskih sprememb – ti morajo ob njihovi očitnosti v vsakdanjem
življenju biti izključeni iz vsake resne razprave – temveč o obstoju in
o človeškem izvoru družbenih sil, ki te spremembe začnejo in
stopnjujejo, torej o historičnosti okoljske krize.

Neoliberalno politično obzorje izključuje vsak neposreden poseg ali
regulacijo družbene produkcije. V poročilih IPCC – mednarodni panel za
podnebne spremembe, znanstveni in politični organ z mandatom OZN, da
koordinira globalni odziv na podnebne spremembe – lahko zato zaznamo
oster kontrast med znanstvenimi izsledki o nepopravljivih spremembah
vseh planetarnih podsistemov, ki vzdržujejo človeško življenje kot ga
poznamo, in relativno neambicioznimi predlogi politik, ki se omejujejo
predvsem na tržne spodbude. Podnebne spremembe so tako pojmovane kot
»neuspehi trga« in eksternalizirani stroški zasebne produkcije ali
potrošnje. Ukrepi se torej osredotočajo na internalizacijo teh stroškov
preko tržnih mehanizmov. Na eni strani to ustvari nekakšno tržišče (in
možnost špekulacije) z ogljikom, kjer lahko industrija svoj ogljični
odtis odkupi z zelenimi programi, ki večinoma niso strogo opredeljeni.
Na drugi strani pa se kaže kot potrošniška izbira, ponavadi za višjo
ceno. Neoliberalna ideologija torej predlaga zelo omejeno vizijo
mogočega.

Nikakor nismo edini, ki odkrivamo pasti, marginalne vloge in neželene
učinke povratne zanke vseh usmeritev, ki jih ponuja politično in
arhitekturno obzorje na tej točki kapitalističnega razvoja. Ravno
svetovna gospodarska kriza 2008 in vse obsežneje izkustveno zaznane ter
vse natančneje teoretsko modelirane družbeno pogojene okoljske spirale
smrti so odpravile zadnje ostanke legitimnosti občutka »konca zgodovine«
in ponovno uvedle nekatere modernistične teme planiranja, možnosti
globalnih alternativ in naslavljanja temeljev družbenih konfliktov. S
tem pa so znotraj arhitekture odpravljeni, vsaj na njenih družbeno
najbolj izpostavljenih ravneh, do sedaj pomirjujoči umiki v pragmatizem
in lokalno samoraziskovanje, ki so bili značilni za dolgo »Bilbao
desetletje«. Tega nikakor ne moremo razumeti kot negativen razvoj, saj
obdobjem splošnih kulturnih in političnih rekonfiguracij pripisujemo
dovzetnost za iskreno preizpraševanje določil institucij discipline, če
ne tudi nove teoretske in praktične preboje.

»Ali je arhitektura po svoji naravi ekstraktivna? Ali je predestinirana
biti instrument družbene krivičnosti in soudeleženka pri izumrtju
človeštva? Ali si je mogoče zamisliti arhitekturo, ki se ne zanaša na
prikrite stroške in eksternalije? Če se hočemo izogniti tej usodi, je
najprej potrebno preoblikovati samo arhitekturo.«
[@spacecaviar2021nonextractive, 256] Tu prepoznamo morda najskrajnejše
meje arhitekturne zavesti. Gre za kuriran raziskovalni in razstavni
program Josepha Grimaja in skupine *Space Caviar*, navedena zbirka
besedil, [opozarjamo na izvrstno recenzijo zbirke, gl.
@rogan2022picking] ki ji bo sledil tudi drugi del, vzpostavlja
»teoretski okvir« programa. V uvodniku [@grima2021design] so vprašanja
in izhodišča projekta še najnatančneje opredeljena. Ali si je v znotraj
razpršenega in odprtega sistema, ki se hrani s svojim okoljem v
eksponentno rastoči meri, možno zamisliti model blaginje, ki nima
eksternaliziranih stroškov? Nikakor ni arhitektura sama zmožna ponuditi
odgovora, je pa, nadaljuje, odgovorna sodelovati v razpravi o materialni
prihodnosti, saj je zgodovinsko imela bistveno vlogo pri dejavnostih, ki
so prispevale k trenutnemu stanju. In vse pasti so trezno priznane:

> Kaj mislimo z neekstraktivno arhitekturo? Enostavno rečeno, nič
> drugega kot pristop k oblikovanemu okolju, ki sprejema popolno
> odgovornost zase in katerega uspešnost ne sloni na ustvarjanju
> eksternalij drugje – ne glede na to ali je ta »drugje« časovno ali
> prostorsko oddaljen. To ne pomeni nujno planetarnega moratorijuma na
> rudarjenje – navsezadnje arhitekti verjetno niso najbolj usposobljeni
> za tak poziv, še manj za njegovo uvedbo –, vendar pa pomeni
> upoštevati, da je nepremišljeno izčrpavanje omejenih virov nevzdržno,
> in predlagati alternativo tistemu, kar drugje ni upoštevano. Ne
> zagovarja živeti z manj, vendar postavlja pod vprašaj ali je rast kot
> cilj sama po sebi dejansko proizvaja blaginjo. Ne obrača se
> nostalgično v preteklost, vendar pa preizprašuje, če je več
> tehnologije vedno rešitev. Vsekakor postavlja pod vprašaj kult
> osebnosti »arhitekta-zvezdnika« enaindvajsetega stoletja, ki je
> podedovan iz evropskega modernizma, in definitivno zavrača temelje
> družbenega in okoljskega izkoriščanja, na katerih sloni. Skratka,
> končni cilj neekstraktivne arhitekture je uporabiti sredstva, ki jih
> ima na voljo projektant, za ustvarjanje alternativnega »načina biti«
> na terenu, ki ni odvisen od kopičenja ekosistemskih razpok na
> planetarni ravni – kar Bellamy Foster opredeljuje kot metabolične
> vrzeli.
[@grima2021design, 14-16]

Toda ravno v najskrajnejših razdelavah problemskega terena, znotraj
katerega se giblje arhitekturna kultura, so ideološka pojmovanja
discipline in družbe v kateri deluje najbolj očitna. Rekli bi celo, da
so presenetljiva, saj menimo, če resno vzamemo objektivno grožnjo kot je
opisana – in sami jo jemljemo resno ter resno jemljemo, da je z druge
strani iskreno ugotovljena in priznana –, da je potreben poseben napor
oklepati se neaktualnih pojmovanj discipline. Namreč zgodba, ki jo Grima
plete skozi uvod, govori o vasi Baratti na italijanski obali. Da uvede
pojem ekstrakcije, sledi zgodovini tega najprej etruščanskega
pristanišča, ki se nahaja strateško blizu okoliških virov mineralov in
gozdov, ki so poganjali metalurško dejavnost. Rimljani so to dejavnost
in torej to vlogo mesta še dodatno poudarili in šele v zgodnjem
dvajsetem stoletju, pravi, je arheološka dejavnost dobila natančnejši
vpogled v njen obseg. Odkrili so, da okoliški hribi (nekateri do dvajset
metrov visoki) niso naravni, temveč so kupi žlindre, ki je nekoristen
stranski produkt taljenja rud. Obseg rimske industrije je v svoji
petstoletni zgodovini trajno spremenil krajino, skrčil gozdove in celo
konfiguriral zemeljsko površje. Z dvajsetim stoletjem, nadaljuje, dobi
zgodba Barattija ironičen obrat: oboroževalna industrija fašistične
Italije začne etruščanske in rimske industrijske odpadke taliti v
sodobnejših pečeh z večjim izkoristkom. Tako ustvari nekakšno zvezno
dolgo zgodovino evropskega ekstraktivizma, ki se sicer stopnjuje, a v
svojih bistvenih določilih ostaja enoten način obravnave narave.
[@grima2021design, 8-10]

Kar neekstraktivna arhitektura predlaga je »zapreti brezno med arhitekti
in materialnim svetom, ki ga oblikujejo« s kompleksnejšim osredotočanjem
na materialnost svojih početij. Modernistični brezbrižni rabi betona, ki
je neločljiva od eksternaliziranih stroškov, ki jih ustvarja, zoperstavi
Albertijev traktat *De Re Aedificatoria* in poudari, da je del traktata
posvečen materialom in njihovim izvorom ter da je postulirana kar
najdaljša trajnost stavb. [@grima2021design, 18]

Grimajev materializem materialov in uporaba zgodovine naturalizirata
zgodovino človeškega občevanja z naravo od rimske metalurgije do
industrijskega kapitala. S tem onemogoča pomenljivo določitev družbenih
pogojev krize in torej družbenih pogojev njene odprave. Kljub sklicu na
»metabolično vrzel« – pojem, ki ga Foster oblikuje po Marxovih
naravoslovnih zapiskih kjer je, na podlagi raziskav znanstvenikov prsti,
historizirana pozitivna povratna zanka med upadom rodovitnosti prsti in
industrializacijo tekom tako imenovane druge agrarne revolucije v letih
1830–1880 [gl. @foster1999marx] – je obzorje razprave, ki ga
vzpostavlja z »materializmom stvari« – če uporabimo termine Fortinijeve
polemike –, omejeno na govor o arhitekturni krivdi ali odnosu *do* tega
stanja. Spominjajo na razpravo, v katero je Fortini posegel proti
humanističnim materialistom – in menimo, da očitek še vedno velja:

> Želite razsvetliti odnos med predmetom in uporabnikom, med objektom in
> proizvajalcem? Zakaj bi torej zakrivali človeški izvor objektov? Če bi
> bili dovzetni za Marxove citate, bi lahko nekatere prijatelje spomnil
> na tretjo tezo o Feuerbachu, kjer so razsvetljenske in paternalistične
> posledice te napake že predvidene vse do sodobnih križarskih pohodov
> arhitektov in *designers* ter sociološke tesnobe tistih, ki
> prestrašeno obsojajo sodobnega človeka potopljenega v močvirju blag,
> da bi jih spomnilo, da vse to predpostavlja človekovo stanje kot tudi
> blago.
[@fortini2016cunning, 125-126]

Kevin Rogan v recenziji zbirke poprime za njene provokativne namene in
jih dopolni v iskrenejšo obliko, ki se je zbirka izogiba: »Nova knjiga
zagovarja, da je arhitekturo obupno potrebno popraviti. Toda kaj pa, če
jo je namesto tega treba pokvariti?« Invokacija Albertija parametre
razprave napravi očitne in opravičuje tudi samozadane politične meje, ki
pa jih razume kot možnosti: arhitektura je bdela nad zgodovino in
vprašanje njenega »sodelovanja« se je ves čas postavljalo kot moralna
dilema, ki si jo lahko vsak arhitekturni agent neodvisno zastavi. Ne gre
nam toliko za očitek moralizma, ki je vsekakor minimalni pogoj zaznave
problema, kot pa za poudarek, da je ključni problem ravno to, da se
prepoznani škodljivci – fosilne, gradbene, turistične industrije – ne
odzivajo na moralne predloge.

Menimo, da prispevek Swarnabha Ghosha znotraj iste zbirke
[@ghosh2021toward] predstavlja možnosti preseganja tradicionalnih
okvirjev znotraj katerih napredna arhitekturna kultura še vedno vztraja.
Podlaga prispevku je mikrozgodovina sodobne indijske gradbene industrije
in prek marksisitčnega in feminističnega teoretskega obzorja predlaga
kompleksnejšo razpravo o arhitekturi, njenih družbenih razmerjih,
institucijah in teoretskih praksah. Uvodoma začne z ameriško skupino
*Who Builds Your Architecture?* (WBYA?), ki se je po svetovni
gospodarski krizi lotila lociranja arhitekture in gradbenih delavcev
znotraj istega sistema, ki ju je zgodovinski razvoj postopoma vse bolj
oddaljeval. V tem smislu gre torej tudi za zgodovinsko obravnavo delitve
dela v katero je nazadnje disruptivno posegla tudi digitalna
tehnologija. Zanimiva je Ghoshova ocena razvoja napredne arhitekturne
kulture po letu 2008 z razkrojem legitimnosti neoliberalnih ideologij se
znova pojavijo »okosteneli družbeni boji«, pozivi k novim vrstam
solidarnosti in oživitev »govora o kapitalizmu«. [@ghosh2021toward,
158-159] Ta razvoj je spodkopal tudi absolutno optimistična pojmovanja
globalizacije in obrnil kritično pozornost k vlogi globalnega arhitekta
– osvobojeni »neoprofesionalci« – pri kršenju človekovih pravic na
gradbiščih ikoničnih arhitektur Arabskega zaliva. Aktivistične skupine
kot so WBYA? in sorodne, nadaljuje, so pomembno prispevale k
razsvetlitvi izkoriščevalskih odnosov na katerih arhitektura temelji, a
so se omejili zgolj na visokoizpostavljene zvezdniške projekte
arhitekturnih birojev globalnega severa, ki so delovali večinoma na
področju Arabskega zaliva. S tem pa še niso mogli sistematično raziskati
specifike sodobne gradbene industrije iz specifičnega stališča
arhitekturne discipline. Torej prehod od »govora o kapitalizmu«, pravi,
do dejanske »kapitalkritik« tako ostaja nedokončan. Pomembno se nam zdi,
kar želi izpostaviti z zoperstavljanjem »govora o kapitalizmu« in
»kapitalkritik«. Dvojico si sposoja od teoretičarke Nancy Fraser, ki s
prvim označuje splošno obnovo zanimanja za kapitalizem kot predmet
razprave in kritike, ki nastopi ob (vsaki) krizi. A zanjo to
predstavlja šele »retorično stopnjo«, šele željo po sistematični
kritiki in ne še bistvenega prispevka k njej. »Kapitalkritik« pa so
sistematične teorije kapitalizma, ki so zgodovinsko predstavljale
preboje pri njegovem razumevanju. Vsaka generacija ima torej
*teoretski izziv* preseči lastno »družbeno amnezijo« in prispevati k
tej tradiciji za lastne potrebe. [gl. @fraser2014behind, 55-56]

Vprašanja, ki jih aktivisti odpirajo, nadaljuje Ghosh, nujno potrebujejo
novo teoretsko konceptualizacijo arhitekture in gradnje kot »notranje
povezanih« delov iste celote. Tu se nanaša na »filozofijo notranjih
razmerij« Bertella Ollmana, ki razmerja, znotraj katerih se katera koli
stvar nahaja, obravnava kot bistvene dele tega kar ta stvar je, tako da
pomenljive spremembe katerih koli od teh razmerij razume kot tudi
kvalitativno spremembo sistema, katerega del ta stvar je. [gl.
@ollman2015marxism, 10] Vsaka stvar je tako stopnja sebe in hkrati
odnos. Posledice tega Ghosh uporabi pri obravnavi arhitekture in gradnje
kot sokonsititutivni ene do druge, kar nam – v luči videza ločenega
razvoja intelektualnega in ročnega dela kot ga prikazuje tehnološki
razvoj – predvsem predstavlja teoretsko preseganje miselne zapore, ki jo
predstavlja upanje, da bo »arhitektura subjekt, ne objekt plana,« ki se
zgodovinsko ponavlja. Hkrati pa vsak del, tako pojmovan, predstavlja
dimenzijo celote, ki ni omejena. Torej lahko posamezen del, sicer
»enostransko«, predstavlja celo razsežnost celote:

> Filozofija notranjih odnosov gre pri obravnavi relacijskih delov
> [celote] še dlje, saj svoje relacijske dele, ko se razširijo do svojih
> skrajnih meja, obravnava kot številne različice – čeprav enostranske –
> celote. Enostranskost je rezultat tega, kje začnemo preučevati
> interakcije in spremembe, ki se dogajajo, in njene vloge pri določanju
> reda, vidnosti in relativne pomembnosti preostalega, kar pride na
> plan. To tudi pomeni, da lahko le z »dovoljšnjim« preučevanjem
> (količina, ki se razlikuje od primera do primera) pomembnejših
> odnosov, ki sestavljajo katero koli celoto, upamo, da bomo ustrezno
> razumeli, kaj je celota, kako deluje, kam teži in kako lahko nanjo
> vplivamo.
[@ollman2015marxism, 10]

Za Ghosha to pomeni, da je (sistematičen, oziroma, ki se ne omejuje na
najvidnejše pojave) teoretski prikaz partikularnega in torej
specifičnega razvoja delitve dela znotraj (iz stališča) arhitekture eden
nujnih prikazov obče krivulje kapitalističnega razvoja, ki povratno
dodatno opredeli status partikularnega. Za nas pa takšen pristop
predstavlja primer razdelave teze, da če že želimo povedati kaj o
arhitekturi, moramo govoriti o svetu in obratno, če želimo povedati kaj
o svetu, moramo govoriti o arhitekturi.

***

Čeprav se ne navezuje na tafurijansko kritiko arhitekturne ideologije –
jasno pa je, da si delita teoretsko obzorje –, menimo, da razmerje, ki
ga Ghosh vzpostavi med arhitekturnim aktivizmom in arhitekturno teorijo,
teži k sklepu kroga, ki zajema tudi tafurijanske spise v *Contropiano*.
Na praktičnem »terenu« ob objektivnem razvoju različne arhitekturne
skupine – v tem primeru gre za WBYA?, potrebno pa je omeniti še vsaj
*The Architecture Lobby* in, na primer, *Architectural Workers United*,
pojav katerih prav tako označuje vzorec razkroja neoliberalnih
arhitekturnih optimizmov – postavijo zahtevo *solidarnosti*
arhitekturnega dela z ostalimi delavci v industriji. Ghoshevo
pogojevanje napredka te zahteve s kritično-zgodovinskim teoretskim
projektom, sami povezujemo s *pesimistično* tafurijansko kritiko
zgodovinskega vzorca arhitekturnega upanja, »da bo subjekt, ne objekt
plana«. Menimo namreč, da se pogoji preseganja ali prenove kritike
arhitekturne ideologije gibljejo okoli pojmov *solidarnosti* in
*pesimizma*, ki sta nenehno podtalno prisotna v Tafurijevih spisih.
Podtalno, ker nista nikoli dobesedno uporabljena, sta pa dobesedno
prisotna v različnih Benjaminovih spisih, ki so na številne načine
podlaga kritiki arhitekturne ideologije. Na primer »izplen«, ki ga
Benjamin ponuja v zaključku *Avtor kot proizvajalec*:

> Solidarnost izvedencev s proletariatom je vedno lahko le posredna.
> Aktivisti in predstavniki nove stvarnosti se lahko vedejo, kakor
> hočejo: ne morejo odpraviti dejstva, da intelektualca niti
> proletarizacija skoraj nikoli ne privede do proletarca. Zakaj? Ker mu
> je meščanski razred v obliki izobrazbe dal na pot proizvajalno
> sredstvo, za katerega je na podlagi privilegija izobrazbe solidaren z
> meščanskim razredom, še bolj pa je ta razred solidaren z njim.
[@benjamin2009avtor, 335]

»Revolucionarnost« intelektualca se torej predvsem pojavlja kot »izdaja
svojega izvornega razreda«. To pa za Benjamina ne pomeni zgolj
destruktivne vloge. Pomembno je, da govori o *izvedencih*, ki so
specifična oblika intelektualnih vlog, kot jih kapitalistični razvoj
opredeli tudi v intelektualnih sektorjih, to je znotraj strokovnih
disciplin. Intelektualec torej ne potrebuje zavzemati pokroviteljskih
vlog do proletariata, to je univerzalnega subjekta, ki ga sistem
vzpostavlja kot del sebe, kot absolutne »negativnosti«. Kar »terja« od
intelektualca je zgolj eno: »naj *misli*, naj premišljuje o svojem
položaju znotraj proizvodnih procesov,« [@benjamin2009avtor, 334] in
nadaljuje s predlogi vprašanj: »Mu je uspelo spodbujati podružbljanje
duhovnih proizvajalnih sredstev? Vidi poti, kako naj bi se duhovni
delavci sami organizirali znotraj proizvodnih procesov? Je podal
predloge za spremembo funkcije romana, drame ali pesmi?«
[@benjamin2009avtor, 336; Vprašanja ponovno zastavi Tafuri v
-@tafuri1988architecture] 

Tej *solidarnosti* do partikularnih interesov je komplementaren trezen
*pesimizem* glede celote. Ko se predlaga »trezno« razumevanje položaja
intelektualnega dela, je impliciten tudi predlog teoretskega horizonta,
to je marksovskih kategorij in zgodovinske ter materialistične obravnave
predmeta. »Organizacijo pesimizma« Benjamin predlaga v drugem, nekaj let
zgodnejšem spisu. V svojem spisu o nadrealizmu [zdaj v
@benjamin1997surrealism] kritizira optimizem buržoaznih strank in strank
socialdemokracije, ki temelji na ideologiji linearnega napredka. Temu
nasprotuje s svojim pojmovanjem pesimizma, ki nikakor ne pomeni
kontemplativnega ali fatalističnega solipsizma, temveč z »organiziranim
pesimizmom« – zveza privzeta od sopotnika nadrealistov Pierreja Navilla
– misli na pripravljenost z vsemi sredstvi na »prihod najhujšega«. Kar
»najhujše« v tem besedilu neposredno označuje je medvojna »kriza
inteligence«, ki jo povezuje s kapitalističnim razvojem, porastom
fašizma. Naloga intelektualca je, zaključuje v nadrealističnem slogu,
biti »budilka, ki v vsaki minuti zazvoni za šestdeset sekund«.
[@benjamin1997surrealism, 56] Ta urgentnost se, menimo, prevaja v
tafurijansko vztrajanje pri nujnosti kritike arhitekture kot kritike
arhitekturne ideologije in zavračanje pomirjujočih koncesij arhitekturi.
»Dramatizacija protislovij«, kot svojo nalogo opredeli v zaključku
*Teorij in zgodovine*, ni enostavno predlog podan »izven« arhitekture,
ampak je vezan na sam razvoj arhitekturne ideologije. Pozitivni moment
solidarnosti in negativni pesimizma je znotraj Tafurijevih besedil,
menimo, najbolje izražen v drugem prispevku k reviji *Contropiano*:

> Ne verjamemo v ponavljajoče se izume novih *zaveznikov* delavskega
> razreda. Toda bilo bi samomorilsko, če ne bi prepoznali, da so prav
> smeri kapitalističnega razvoja tiste, ki za lastne namene ponovno
> sestavijo praviloma vse bolj homogeno delovno silo, ki je sposobna
> delovati pod znakom neposrednih interesov delavskega razreda. [...]
> Toda to ni mogoče, če se ne premaga vsako reakcionarno iluzijo, vsak
> predlog, ki želi ponovno vzpostaviti profesionalno *dostojanstvo* tem
> »degradiranim« intelektualcem. Konkretno pokazati na reakcionarnost
> vsakega diskurza, ki želi ponuditi »alternativne« perspektive
> intelektualnemu delu, pomeni torej prepoznati, da so zgolj *znotraj*
> objektivnih vlog, ki jih vsiljuje gospostvo razvoja, pogoji za uporabo
> boja intelektualnih slojev, ki so neposredno vpiti v produkcijo, v
> celostnem napadu na plan kapitala: kar v bistvu pomeni razširiti
> politično rabo boja *za* mezdo na vse širše družbene sloje. [...] In
> zato edini cilj h kateremu mora stremeti kritika ideologije ali
> kritika plana ni drugega kot nova oblika razredne organizacije, ki je
> sposobna premagati, zdaj brez utopije, brez takoj preverljivih
> »alternativnih modelov«, brez subjektivnih pričakovanj konkretno
> realnost programirane reorganizacije kapitala.
[@tafuri1970lavoro, 281]

Iz razvoja sodobne napredne arhitekturne kulture v luči obnove
tafurijanske kritike ideologije (ter povratno), sklepamo shemo: že z
zavzemanjem parcialnega *arhitekturnega* stališča – in skrajnost tega je
zavzemanje parcialnega stališča *arhitekturnega dela* znotraj
arhitekture – arhitekt zavzema univerzalno stališče. Vprašanje torej ni,
kaj lahko arhitekt doprinese razrednemu boju, temveč kaj si lahko iz
njega pribori. V to kritika ideologije ne vstopa s pozitivnimi predlogi,
saj je razvoj partikularnega stališča arhitekturnega dela lahko stvar
zgolj objektivnega razvoja – in tudi je. Kritika ideologije vstopa
izrazito »negativno«, z demistifikacijo preseženih miselnih shem, s
katerimi arhitekturna praksa oblači svoje interese in razproda svojo
avtonomijo. V tem smislu kritika ideologije ne pomeni razsvetljevanja,
kaj vse je »lažno« pri arhitekturni zavesti, temveč kaj vse je
»resnično«. Izguba »avre«, razpad vsake skladnosti subjektivnih in
objektivnih pomenov arhitekture, sama po sebi ne predstavlja negativnega
ali pozitivnega razvoja stanja. Njen trezen sprejem, tako pri Benjaminu
kot pri Tafuriju, potencialno odpira številne možnosti samospreminjanja,
tragično iskanje novih (objektivno nemogočih) sintez pa obzorje
arhitekturne dejavnosti zapira v »brezizhodno kapsulo«. Problem, da
kritika arhitekturne ideologije ne ponuja pozitivnega projekta je torej
problem »domišljije« arhitekturnega proizvajalca; problem, ki ga ravno
kritika ideologije prepozna in določi. Preseganje kritike ideologije
lahko pomeni zgolj sprejem delov realnosti, ki jih uspe demistificirati.
In na točki prevoda spoznanja v delovanje lahko govorimo o vstopu
kritike v ideologijo, o ideološki rabi kritike ideologije, kar je v
praksi, kot je opredeljeno zgoraj v zaključku *Intelektualnega dela in
kapitalističnega razvoja*, oblikovanje lastnih institucij in njihovo
praktično zoperstavljanje prevladujočim idejam.

S tem smo se vrnili v uvodne termine *Teorij in zgodovine*:

> Vso orožje kritika, ki se zavzema za revolucijo, je uperjeno proti
> staremu redu, koplje v njegova protislovja in hipokrizije, ustvarja
> novo ideološko prtljago, ki lahko privede celo do ustvarjanja mitov:
> saj so za vsako revolucijo miti nujna in nepogrešljiva idejna sila za
> izsiljevanje položajev. Toda ko revolucija – in ni dvoma, da so se
> umetniške avantgarde 20\. stoletja borile za revolucijo – doseže svoje
> cilje, se podpora, ki jo je kritika prej našla v svoji popolni
> zavezanosti revolucionarni stvari, izgubi.
[@tafuri1968teorie, 9]

Namenoma smo zgoraj uporabili izraz, da sodobni razvoj na področju
arhitekturnega aktivizma in arhitekturne kritične teorije zgolj »teži k
sklepu kroga« in kroga ne sklene, saj ne mislimo, da se ponujajo kakšne
sklepne sinteze. Nasprotno, napredna arhitektura in kritika arhitekturne
ideologije na nobeni točki ne izčrpata svojih predmetov. Prav tako pa ne
želimo miselno odpraviti pravega pomena tega neskladja z enostavnimi
oznakami o »plesu protislovij«. Kritika arhitekturne ideologije ni
abstraktna razvrstitev splošnih konceptov nemoči, subjekta in delovanja
v družbo, ki je na ravni prispodobe predstavljena v arhitekturnih
terminih. Je specifična in natančna preveritev in vsaj predlog analize
konkretnih določil arhitekturnega dela v družbah kjer prevladuje
kapitalistični proizvodni način. Predpogoj tega je sploh historizacija,
to je zgodovinsko in materialistično pojmovanje, sedanjosti in
arhitekture. Jasno je kako je Tafurijevo historiziranje arhitekture
nastopilo kot kritika operativni zgodovini: ponavljamo, da ni šlo za
kritiko resničnostne vrednosti dejstev predstavljenih skozi operativno
zgodovino – ni šlo za kritiko na podlagi nasprotujočih si
pozitivističnih ciljev. Nikakor ne moremo reči, da »marksizem«, ki smo
mu sledili znotraj tafurijanske misli, arhitekturi umetno vsiljuje teme
in vidike, ki so ji sicer tuji. Ravno arhitektura odpira na specifično
(in edinstveno) arhitekturni način – kot arhitekturna ideologija primata
nad celotnim področjem zidave, a prav tako kot napet institucionalni
»vmesnik« med intelektualnim in ročnim, kulturnim in tehničnim delom –
»družbene teme«, katerih obravnava iz univerzalnega stališča ne bi
zapopadla možnosti vplivanja na njihove spremembe. Historizirati
(materialistično) se nenazadnje tiče možnosti mišljenja statusa
arhitekture v svetu pri čemer je zavzeto stališče, da pomeni, statusi in
odnosi, ki jih arhitektura nosi, nastajajo, se razvijajo in opuščajo
tekom te zgodovine. Torej, da niso univerzalni in naravni; so seveda
zgodovinsko »pogojeni«, ampak zgodovino, nenazadnje, »delajo« ljudje. 

---
# vim: spelllang=sl spell
...

