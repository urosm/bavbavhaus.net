SCRIPT_NAME = "inline_lua.lua"
os.setlocale("C")

return {
  { CodeBlock = function(el)
    if el.classes[1] == "lua" and el.classes[2] == "inline" then
      return load(el.text)()
    end
  end }
}
