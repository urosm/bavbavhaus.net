---
title: "Zgodovina estetike"
...

## Kant, *Kritika razsodne moči*

<!--
3. kritika: Kaj lahko upam?
-->

> Preiskovanja zmožnosti okusa, in sicer kot estetske razsodne moči, se tu nisem
> lotil zaradi oblikovanja in kulture okusa (kajti le-ta bo tako kot doslej tudi
> v prihodnje nadaljevala svojo pot tudi brez takih raziskovanj). Lotil sem se
> ga zgolj s transcendentalno namero, zato upam, da bo, kar zadeva
> pomanjkljivost prvega smotra, presojano z uvidevnostjo. Kar pa zadeva zadnjo
> namero, mora biti pripravljena za najstrožji preizkus. A tudi tu je, tako vsaj
> upam, skrajna težava, ki spremlja reševanje problema, ki je po naravi tako
> zamotan, lahko opravičilo za nekatere nejasnosti v njegovi razrešitvi, ki se
> jim ni bilo mogoče popolnoma izogniti. Pod pogojem seveda, da je bilo dovolj
> jasno pokazano, da je bilo načelo pravilno podano, čeprav nemara način, kako
> je iz njega izpeljan fenomen razsodne moči, nima vse tiste razločnosti, ki jo
> lahko upravičeno zahtevamo drugje, se pravi, od spoznanja s pojmi, in ki sem
> jo tudi sam, kot menim, dosegel v drugem delu tega spisa [@kant1999kritika3,
> 10].

Zakaj je Kant dal posebno pozornost lepemu? 

  - Ker je hotel upravičit upanje človeštva na boljše. Človeštvo je zmožno delovat
    na osnovi svojega smotra.

- 3 apriorne zmožnosti. čisti um za spoznanje, praktični um za ravnanje in
  razsodna moč za razsojanje

- razsodna moč potrebuje pojem po katerem se lahko ravna, ampak ne sme bit
  objektiven. to je pojem lepega, estetska načela

  estetska presoja utemeljuje naše upanje na boljše. pojem lepega uveljavlja
  apriorno veljavnost.

- specifike okusa za lepo

transcendentno

: onkraj možnega izkustva

transcendentalen

: možnost apriornih absolutno veljavnih konstitutivnih sodb

Del polja kjer je možno spoznanje so tla. Del tal na katerm so ti pojmi
zakonodajni, je področje ditio teh pojmov in spoznavnih zmožnosti, ki jim
pripadajo. izkustveni pojmi imajo sicer svoja tla v naravi kot skupku vseh
predmetov čutov, nimajo pa področja ampak le svoje prebivališče.

Zakonodajo s pojmi narave izvaja razum in je teoretična.

najstvo -> nujnost, svobodo in naravo je potrebo povezat

: Sistematična enotnost zmožnosti čudi [@kant1999kritika3, 38].

Vse zmožnosti čudi          Spoznavne zmožnosti  Načela a\ priori  Aplikacija na
--------------------------- -------------------- ----------------- --------------
Spoznavna zmožnost          Razum                Zakonitost        Naravo
Občutje ugodja in neugodja  Razsodna moč         Smotrnost         Umetnost
Želelna zmožnost            Um                   Končni smoter     Svobodo


### 4 momenti razlage lepega 

- kvaliteta, realnost
- kvantiteta
- relacija, odnos
- modalnost, vprašanje nujno ali mogoče

- lepo je nekaj kar ugaja brez vsakega interesa
- lepo je kar ugaja obče brez pojma (brez razlage)
- lepota je forma smotrnosti predmeta, kolikor je zaznana, na da bi bil
  predstavljen smoter (neko delo je smotrno, brez da bi rabili navest/predstavit
  nek smoter)
- lepo je to kar je brez pojma spoznano kot predmet nujnega ugajanja 

Lepo je to kar zagotavlja možnost teleologije. smoter, upanje na boljše

- umetnost sili k družabnemu sporočanju
- Umetnost ne sme videt namerna. Mora bit videt kot naravna


- to omogoča genij. genij je talent (naravni dar=, ki umetnosti daje pravilo.
  ker pa sodi talent kot umetnikova prirojena produktivna zmoznost sama k
  naravi, bi se lahko izrazili tudi da je genij prirojena zasnova čudi
  (ingenium) s katero daje narava umetnosti pravila

Protislovje pojmovanja umetnosti:

Umetniško delo je povzročeno s strani narave.


*Vrt*.

gl. razprave o vrtu. Vsak filozof je pustil eno besedilo o vrtu

- francoski vrt: racionaliziran, geometriziran, ločnica med naravo in kulturo je
  poudarjena (racionalistični ideal)
- angleški: odraža karakter lastnika, ni vidne meje med vrtom (kulturo) in
  naravo (empirični ideal)
- nizozemski: dobesedno pol pol
- nemški: razpravljali o vrtu, en lasten primer je v Kasslu, grad Herkules

- genij je edini ki daje umetnosti pravila
- vsi ostali sledijo geniju, posnemajo genij
- lepe umetnosti so povzročene samo s strani genija (gl. stran v knjigi)

- lastnost genija: izvornost, eksemplaričnost, da sam ne ve kako je prišel do
  ideje zanj, ne more proizvajati poljubno ali načrtno

- tisti ki prepoznajo genija so tudi redki. tudi prepoznati genija je naravni
  dar (talent)

razlika med kantom in heglom

: hegel ceni kanta ampak ga kritizira, še posebej pri estetiki. narava ni lepa,
lepota pripada umetnosti. narava je nekaj drugotnega. heglovo: narava posnema
umetnost

vpr iz publike: zakaj kant rabi genija tudi na drugi strani (na strani kjer
moraš bit genij za prepoznanje genija)

odg: ker mora nekdo prepoznat (genij ne sme prepoznat).

### &sect; 83. O zadnjem smotru narave kot teleološkem sistemu


: kant naj bi dokazal da je človek zadnji smoter narave. kaj naj si človek potem
izbere za smoter, če je zadnji smoter?

    za cilj zadnjega smotra narave si lahko izberemo:

    - srečo (čim bolj jo dosegamo, tako manj je učinkovita)
    - kulturo 

    gl. kantov zagovor razredne družbe. napredek je možen samo z izrazito
    razredno družbo. vojna je smer k kulturnem napredku

    Vprašanje kaj lahko upam? Kant je upal na vojno. To kar je upal se tudi
    dogaja.

## Schiller

- (Značilno za modernizem je zavračanje prihodnje generacije.)
- Posebno pozornost bodo dali *Pismom o estetski vzgoji človeka*.
- Osnovno o pismih: vse težave in probleme, ki jih ima razsvetljenstvo, lahko
  rešimo z estetsko vzgojo. S tem se vzporeja vsem, ki v umetnosti vidijo
  rešitev človeštva.
- Schiller izhaja iz Kanta na točki kjer Kant umetnosti pripisuje civilizacijsko
  sposobnost.
- Schiller maja 1789 predava svetovno zgodovino na Univerzi v Jeni. To je
  obdobje prvega razčaranja (novice iz Pariza).

- Kant, Spor fakultet
  - višje fakultete potrebujejo kritiko nižjih
  - pojem dogodka
  - dogodek pokaže, da smo zmožni napredka k boljšemu
  - Kant stavi na spomin (na dogodek)
- Buerger
  - Historična avantgarda ni uspela, ker ni uspela svetovna revolucija
  - Namesto celostne umetnine, ki izraža kako celostna je družba, stavi na
    okruške. Lepota celostne umetnine je reakcionarna.

### Rancière, trije režimi umetnosti

Po Rancièrju obstajajo [trije režimi umetnosti](delitev_cutnega.md):



- Ranciere, obstajajo trije režimi umetnosti:
  - mimetični/kultni, ki priklicuje v realnost vse kar čutimo s čutili in
    prisotnost boga
  - reprezentacijski režim, ki reprezentira, da je vse urejeno, harmonično,
    celovito in služi vsaki oblasti
  - estetski režim pri katerem gre za okruške, ponižano, kar se ne sme
    pojavljati v reprezentacijskem režimu
- umik iz delovanja, *far niente*
- ne početi nič je dostop do resnice same

- (Thompson, zgodovina ure)
- (gl. estetski marksizem)

- Ranciere kaže kako je Schiller aktualen avtor.
- Schiller povzdigne "igro" v temeljni način bivanja. "Živeti *igraje*."
- Erjavec: "Skozi vse dvajsesto stoletje obstaja segment avantgardne umetnosti,
  ki je dovolj specifičen, da si zasluži posebno oznako -- estetska avantgardna
  umetnost."
- Schiller prvi uporabi tak pojem "estetskega", ki zajema (je povezano) tudi s
  političnim.
- Erjavec: Spekter od *umetniških* do *estetskih* avantgard.
- Problem estetskih avantgard, spust avantgarde med ljudi, v življenje prvi
  izrazi K. Greenberg. Greenberg razglasi abstraktni ekspresionizem za vrh
  umetnosti. (Umetnost se je morala zavedati svoje dvodimenzionalnosti --
  abstrakcija -- in pridobiti izraz -- ekspresionizem.)
- (Schiller, 15\. pismo je najpomembnejše.)

- Kriticizem kritizira zato, da bi naredil razsvetljenstvo kompetentno za
  obravnavo v znanosti, etiki in estetiki.

- Schillerjevo opravičevanje?
- Marcusejevo opravičevanje:
  - V okoliščinah, ko se da žalostno stvarnost spremeniti le s pomočjo radikalne
    politične prakse, je treba ukvarjanje z estetiko utemeljiti. nesmiselno bi
    bilo zanikati, da ima pri tem ukvarjanju svoj delež obup: umik v svet
    fikcije, kjer se dane okoliščine spreminjajo in presegajo zgolj na območju
    imaginacije. (Trajnost umetnosti 1977) -- opravičevanje kot del obupa nad
    možnostjo osvoboditve
  - Kulturni pesimizem: kultura ni del tega sveta, ampak njegovo nasprotje.
    Nasprotje civilizacije.
- Eagletonovo opravičevanje:
  - Dosti je oblik radikalnega kulturnega preizpraševanja z neprimerno večjo
    politično težo kot tako visiko teoretsko delo; toda globlje razumevanje
    stanja tistih mehanizmov, ki vzdržujejo zdajšnjo politično hegemonijo, je
    predpostavk, nujno potrebna za učinkovito politično akcijo, in to tu je ena
    vrsta vpogleda, ki po mojem mnenju lahko pride na dan iz proučevanja
    estetskega. Saj ne da bi tak projekt pomenil vse, ampak gledati nanj zviška
    pa menda tudi ni treba. (Ideologija estetskega, 1990) -- opravičevanje iz
    resignacije in kljubovanja resignaciji
  - "Zgodovina nas bo še poklicala na dan" (Eagleton in Lukacs)
- (Paranoja je ključen pojem za drugo fazo nadrealizma. Paranojo je potrebno
  utemljit kot način delovanja umetnosti (ker krokodil dejansko je pod
  posteljo)).
- Kreftov point: zgodba o debati o spopadu na kulturni levici pomeni, da je
  vprašanje estetike politično vprašanje.
- Postvarjenje (reification)


- Schillerjeva teorija države? (estetika države)
  
  - dinamična država pravic
  - etična država dolžnosti
  - estetska država lepega občevanja

- Estetika vsakdanjega življenja

  - Alltag (Max Weber) -- rutina
  - Lebenswelt (Huserl) -- svet v katerem smo pred vsako refleksijo (vpetost v
    svet ima prednost pred refleksijo)



---
lang: sl
references:
- type: book
  id: kant1999kritika3
  author:
  - family: Kant
    given: Immanuel
  title: "Kritika razsodne moči"
  translator:
  - family: Riha
    given: Rado
  publisher-place: Ljubljana
  publisher: ZRC
  issued: 1999
  language: sl
# vim: spelllang=sl
...
