-- counts words in a document

words = 0

function Pandoc(el)
    -- skip metadata, just count body:
    el.blocks:walk({
        Str = function(el)
            if el.text:match("%p") then return end
    	    words = words + 1
        end
    })
    print("[INFO] Wordcount: " .. words .. " words in body")
end