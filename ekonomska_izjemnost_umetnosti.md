---
title: "Ekonomska izjemnost umetnosti"
description: |
  Ob vseh teorijah o pogojenosti umetnosti znotraj kapitalizma, o integraciji umetnosti v kapitalistične mehanizme, ostaja *očitna* ekonomska izjemnost umetnosti neteoretizirana.
  Drugače rečeno, obstajajo številne teoretizacije odnosa med umetnostjo in kapitalizmom, ki ne obravnavajo odnosa med umetnostjo in kapitalom.
...

Razprave o povezavi med umetnostmi in kapitalizmom, o podrejenosti umetnosti, umetniškega ustvarjanja in umetniških del mehanizmom kapitalistične družbe so se večkrat omejevale na vprašanja okoli položaja umetnosti na trgu oziroma o statusu umetniškega dela kot blaga.
To je omejeno iz vidika implikacije kaj sploh je kapitalizem.
Ali gre samo za trg?
Prav tako je omejeno iz vidika kaj so vsi možni pomeni integracije ali avtonomije umetnosti od kapitala.

> Instead of theorising art's relationship to capitalism through the concepts of commodification, culture industry, spectacle and real subsumption, all of which have a superficial ring of truth, the key to understanding art's relationship to capitalism must be derived from questioning whether art has gone through the transition from feudalism to capitalism.
[@beech2015art, 8]

> [T]he question of whether art conforms to the capitalist mode of production cannot be determined simply by observing certain capitalistic elements at work in the production of circulation of art but depends entirely on whether art embodies the social relations in which the capitalist subjugates production through the ownership of the means of production and the payment of wages to purchase labour power.
[@beech2015art, 9]


## Odsotnost marksistične ekonomije umetnosti


### Postvarenje

Družben obrat znotraj marksizma, ki se začne z Lukácsem, razloži odsotnost marksistične ekonomije umetnosti.


### Kulturna industrija

> [Adorno's utter rejection of jazz] leaves Frankfurt School aesthetics vulnerable to the charge that the perceived uniformity of mass culture is projected onto it by unsympathetic commentators rather than imposed by and inflexible system.
> According to Bernard Gendron, Adorno 'failed to appreciate the fact that ... I do not buy records like I buy cans of cleanser'.
> The underlying problem, here, is not Adorno's elitism or even his revolutionary militancy but the methodological decision to focus the analysis of culture's relationship to capitalism on the properties of the works themselves.
> While each can of Heinz tomato soup is required to taste the same, each film by Pixar and each song by The Rolling Stones is required to be different, so the application of technology and rationalization to culture uses processes of standardisation to produce goods that exhibit specific or unique qualities. [@beech2014art, 228]

Kaj je tu smiselno primerjati?
Konzerva paradižnikove juhe mora biti enaka podobno kot morajo biti zgoščenke enake.


### Spektakel

@TODO

***


## Umetnost in produktivni kapital

Lahko ugotovimo, da se je umetniška produkcija, produkcija vizualnih umetnin, med 19. in med 20. stoletjem preoblikovala vzporedno s preoblikovanji produkcije znotraj kapitalistične industrijske produkcije.
A na preoblikovanje umetniške produkcije ne pomeni nujno vstop umetnosti v kapitalistični produkcijski proces.
Obstajajo tudi druge -- družbene, kulturne, psihološke, tehnološke, ideološke -- sile, ki bi lahko povzročile, da umetnost prevzame kapitalistične produkcijske procese.
Kar Beecha zanima je, ali je takšno preoblikovanje bilo posledica vključitve umetnosti v kapitalistično ekonomijo.
Pri tem vprašanju je ključen odnos med umetnostjo in produktivnim kapitalom. [@beech2014art, 241]

Eden izmed Beechevih poudarkov pri njegovi kritiki tez o poblagovljenju je razlika med kapitalistično produkcijo blag.


## Mesto arhitekture znotraj Beechove ekonomske izjemnosti umetnosti

Svojo obravnavo odostnosti marksistične ekonomije umetnosti Beech naslanja na strogo ločitev med temo družbe in temo ekonomije.
Avtohtona tema ekonomije naj bi se tako v zahodnem marksizmu pojavljala le kot prispodoba ali homologija.

Beechev formalizem pri ločevanju teme družbe in teme ekonomije bi lahko izpodbijali tudi na splošni točki umetnosti.

A na specifični točki arhitekture lahko izpodbijamo tako:

- Posledice ekonomskih mehanizmov na področju arhitekture ne delujejo le kot prispodoba.
- Pravzaprav najprej delujejo stvarno.
- Gradnja je downstram industriji.
- Elementi industrijske proizvodnje (specializacije, blaga) so elementi s katerimi arhitektura rokuje.
- Arhitektura v neki točki dobi nalogo (dobi ali prevzame) postaviti se upstream industriji.
- Tu je element ideologije.


---
lang: sl
references:
- type: book
  id: beech2015art
  author:
  - family: Beech
    given: Dave
  title: "Art and value: art's economic exceptionalism in classical, neoclassical and Marxist economics"
  title-short: "Art and value"
  publisher-place: Leiden
  publisher: Brill
  issued: 2015
  language: en
...
