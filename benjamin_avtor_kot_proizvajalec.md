---
title: Walter Benjamin, Avtor kot proizvajalec
...


---
references:
########################################################################
- type: chapter
  id: benjamin2016avtor
  author:
  - family: Benjamin
    given: Walter
  title: "Avtor kot proizvajalec"
  container-title: "Usoda in značaj"
  publisher-place: Ljubljana
  publisher: Beletrina
  issued: 2016
  page: 81-96
  language: sl
########################################################################
...
