---
title: "Intelektualno delo in kapitalistični razvoj"
description: |
  Naiven prevod in komentarji k Tafurijevem članku ["Lavoro intelettualle e sviluppo capitalistico"]{lang=it}. [@tafuri1970lavoro, [op. a.]]
...

Odvečnost arhitekture v razmerju do kapitalističnega razvoja je znana tema tafurijanske kritike arhitekturne ideologije.
Manj ozaveščena pa je skrajnejša razdelava te odvečnosti kot jo Tafuri razvije v članku *Lavoro intellettuale e sviluppo capitalistico*.
[@tafuri1970lavoro]
Kapitalistični razvoj, oziroma *Plan*, tu pridobi status popolnoma avtonomnega akterja:

> Gospostvo totalnega kapitala se tako uresničuje izven vsakršne logike, ki je tuja njegovim lastnim neposrednim mehanizmom, brez vsakršne zunanje utemeljitve za vse večjo realizacijo konkretnih instrumentov poseganja, z najbolj absolutno neodvisnostjo od kakršnega koli abstraktnega "etičnega" cilja, od kakršne koli teleologije, od kakršnega koli "najstva".
[@tafuri1970lavoro, 247]

Tu ne gre več za to, da je arhitektura izgubila ideološko funkcijo, temveč je sama funkcija ideologije presežena.
Tafurijev *Plan* je absolutno tuj kakršnemu koli upravljanju v oblikah, s sredstvi, ki jih je arhitektura do tedaj razvila.
Ključni razvoj v to smer za Tafurija predstavlja izum računalnika, oziroma razdelava umetnih jezikov, ki so uporabni znotraj sistema računalnikov.
Zanj so pogoj avtonomije *Plana* dinamični modeli nadzora, ki zagotavljajo lastne mehanizme vrednotenja in s tem odpravljajo diskurzivne funkcije arhitekta kot intelektualca ter ustvarjajo nove splošne discipline, ki izhajajo iz *Plana*.
[@tafuri1970lavoro, 267]

> tehnike odločanja, s prilaščanjem ogromnega potenciala, ki je v elektronskem računalniku, težijo k ustvarjanju specifičnih *jezikov plana* in posledično novih splošnih disciplin, ki *izhajajo iz plana*.
[@tafuri1970lavoro, 267]

***

::: alts

> Posebna vrzel med razvojnimi procesi, prestrukturiranji, ki jih ti vsiljujejo
> kapitalistični delitvi dela, in intelektualnimi posredovanji, ki sledijo iz
> teh procesov, še ni v celoti odpravljena. Tako zelo, da lahko rečemo, da v
> trenutnem stanju splošna sestava, s katero se predstavlja intelektualno delo,
> nima enotnih konotacij. To je posledica predvsem vse globljih sprememb vseh
> institucionalnih vlog spričo realnosti razvoja.

::: {lang=it}

> /241/Non risulta ancora del tutto saldata la singolare divaricazione
> esistente fra i processi di sviluppo, le ristrutturazioni che questi impongono
> alla divisione capitalistica del lavoro, e le mediazioni intellettuali che
> conseguono a quei processi. Tanto, che può dirsi che allo stato attuale la
> configurazione generale con cui si presenta il lavoro intellettuale, risulti
> priva di connotazioni unitarie. Il che consegue, principalmente, al sempre più
> profondo mutamento di tutti i ruoli istituzionali di fronte alla realtà dello
> sviluppo.

:::

:::

::: alts

> Intelektualno delo in razvoj torej: dve področji delovanja, ki sta se že
> zdavnaj uravnotežili kot ideologija na eni in strukturna realnost na drugi
> strani. Vsa "tragika" buržoaznega Kultur, tesnoba, ki se doživlja, ko smo
> priča kako je razlaščena vsake progresivne funkcije, ob potrditvi
> neučinkovitosti njene biti na svetu, ob njenem prepoznanju kot utopije
> naivnega razuma, se je preobrnila v intelektualno delo kot pozitivno utopijo,
> kot model dialektičnega razvoja: z eno besedo, kot "dialektično formo", ki, ob
> spoznanju inherentne negativnosti sistema, v poskusu globalnega gospostva nad
> prihodnostjo *projektira* svojo integracijo.

::: {lang=it}

> Lavoro intellettuale e sviluppo, dunque: due settori di intervento che da
> tempo hanno finito di compensarsi fra loro come ideologia da un lato, e realtà
> strutturale, dall'altro. Tutto il "tragico" della Kultur borghese, l'angoscia
> sperimentata nel veder espropriata quella Kultur di ogni funzione progressiva,
> nel verificare l'ineffettualità del suo essere al mondo, nel riconoscerla come
> utopia della ragione ingenua, si sono rovesciati in un lavoro intellettuale
> come utopia positiva, come modello di sviluppo dialettico: come "forma
> dialettica", in una parola, che, riconoscendo l'inerenza della negatività al
> sistema, ne *progetti* l'integrazione in un tentativo di dominio globale del
> futuro.

:::

:::

::: alts

> Neproduktivnost intelektualnega dela je tista *krivda*, ki bremeni kulturo
> 19\. stoletja in ki jo napredne ideologije *morajo* preseči. Prevračanje
> ideologije v utopijo postane torej kategorični imperativ. Da bi preživela, se
> mora ideologija zanikati kot taka, razbiti mora svoje kristalizirane forme, se
> celovito projicirati v "konstrukcijo usode": samokritika ideologije je tako
> projekt gospostva *realizirane ideologije* nad formami razvoja.

::: {lang=it}

> È l'improduttività del lavoro intellettuale la *colpa* che la cultura
> ottocentesca sente pesare su di sé, e che le ideologie avanzate *debbono*
> superare. Rovesciare l'ideologia in utopia diviene allora un imperativo
> categorico. Per sopravvivere, l'ideologia deve negarsi in quanto tale, rompere
> le proprie forme cri/242/stallizzate, proiettarsi integralmente nella
> "costruzione del destino": l'autocritica dell'ideologia è, a questa stregua,
> progetto di un dominio dell'*ideologia realizzata* sulle forme dello sviluppo.

:::

:::

::: alts

> Weber, Scheler, Pareto, Mannheim: na začetku 20\. stoletja je razkrinkanje
> *idolov*, ki ovirajo vzlet globalne racionalizacije produktivnega univerzuma
> in njegovega družbenega gospostva prepoznano kot nova zgodovinska naloga
> intelektualca. Weberjanska Wertfreiheit je "manifest" najbolj radikalnega
> zavračanja vsakega kompromisa med znanostjo in ideologijo. Znanstveni statut
> ima samo en *treba*, tega o "samokontroli", ki je "edino sredstvo za
> preprečiti zavajanja, razločiti z natančnostjo logično-komparativistični odnos
> realnosti z idealnimi tipi v logičnem smislu, od vrednotenja realnosti na
> podlagi idealov."[^1]

::: {lang=it}

> Weber, Scheler, Pareto, Mannheim: agli inizi del XX secolo lo smascheramento
> degli *idola* che ostacolano il decollo di una razionalizzazione globale
> dell'universo produttivo e del suo dominio sociale, è identificato come il
> nuovo compito storico dell'intellettuale. La Wertfreiheit weberiana è il
> "manifesto" del rigetto più radicale di ogni compromesso fra scienza e
> ideologia. Lo statuto scientifico ha un solo *dovere*, quello
> dell'"autocontrollo", che è "il solo mezzo per prevenire gli inganni,
> distinguere con precisione la relazione logico-comparativa della realtà con
> tipi ideali in senso logico, dalla valutazione della realtà in base a
> ideali"[1].

:::

:::

[^1]: Max Weber, *Il metodo delle scienze storico-sociali*, Torino 1958, str.
    119, in *Il lavoro intellettuale come professione*, Torino 1966\. Za
    posplošitev webrovske kritike ideologije gl. K. Loewenstein, *Beiträge zur
    Staatssoziologie*, Tübingen 1961 (zlasti esej *Ueber dar Verhältnis von
    politischen Ideologien und politischen Institutionen*, že v "Zeitschrift für
    Politik" 1955).

    V delih Pareta, Maxa Schelerja, Webra kritika ideologije -- izražena v
    različnih strujah in z različno stopnjo samozavedanja -- ne pomeni zgolj
    odčarane zaveze *dejstvu* realnosti, v vsej njeni neusmiljeni pogojenosti,
    temveč pomeni tudi prisvajanje tipično marksovskega instrumenta boja z
    njegovim preobračanjem v samega sebe. Reducirati marksizem na ideologijo je
    potem omogočilo tej vrsti kritike uperiti se v teoretske temelje
    historičnega materializma, ki je bil bolj ali manj dobronamerno napačno
    razumljen. Treba je opomniti, da pri Paretu ideologija -- "popačeno
    mišljenje" -- ni drugega kot način biti dejanja in "neznanstvenega
    teoretiziranja": še vedno smo v okolju obnove "predsodka" prepoznanega kot
    takega. (Konec koncev, končna obnova *iracionalnosti* ideologije ni tuja
    niti Webru).

<!--

[^1]: Max Weber, *Il metodo delle scienze storico-sociali*, Torino 1958, p. 119,
    e *Il lavoro intellettuale come professione*, Torino 1966\. Per una
    generalizzazione della critica weberiana dell'ideologia cfr. K. Loewenstein,
    *Beiträge zur Staatssoziologie*, Tübingen 1961 (in particolare il saggio
    *Ueber dar Verhältnis von politischen Ideologien und politischen
    Institutionen*, già in "Zeitschrift für Politik" 1955).

    Nelle opere di Pareto, di Max Scheler, di Weber, la critica dell'ideologia
    -- articolata in diversi filoni e con un variabile grado di autocoscienza --
    non ha solo il significato di un disincantato aderire al *dato* della
    realtà, in tutta la sua spietata condizionatezza, ma ha anche quello di
    appropriarsi di uno strumento di lotta tipicamente marxiano, rovesciandolo
    su se stesso. La riduzione del marxismo a ideologia ha poi permesso a tale
    tipo di critica di appuntarsi contro le basi teoriche di un materialismo
    storico frainteso con più o meno buona fede. Si noti, comunque, che in
    Pareto l'ideologia -- il "pensiero distorto" -- non è che un modo d'essere
    dell'azione e della "teorizzazione non scientifica": siamo ancora
    nell'ambito di un recupero del "pregiudizio", riconosciuto come tale. (Del
    resto, il finale recupero dell'*irrazionalità* dell'ideologia non è estraneo
    neanche a Weber).

-->

::: alts

> Dejstva, da ima ta Wertfreiheit za Webra dramatičen pomen, ne gre zanemariti.
> Intelektualec, s tem ko se odpove vrednostni sodbi, *odločno* sprejme lastno
> najstvo: sprejemanje je primarno prepoznanje iracionalnega, ki v je sistemu,
> negativnega, ki je v njem, in ki ga je, kot neločljivo od pozitivnega, treba
> sprejeti kot takega. Problem -- za Webra, za Keynesa, za Schumpetra, za
> Mannheima -- je v instrumentih, ki so sposobni funkcionirati *hkrati*
> pozitivno in negativno (kapital in delavski obraz dela), ki niso sposobni
> razločiti teh dveh pojmov, ki *realizirajo* njuno komplementarnost. Pri vseh
> prevladuje tema prihodnosti, v katero je celotna sedanjost projicirana,
> "racionalnega" gospostva prihodnosti, odprave *tveganja*, ki ga prinaša[^2].

::: {lang=it}

> Il fatto che quella Wertfreiheit abbia per Weber un significato drammatico non
> va ignorato. L'intellettuale, impedendosi il giudizio di valore, accetta
> *virilmente* il proprio dover essere: l'accettazione è in primo luogo
> riconoscimento dell'irrazionale che è nel sistema, del negativo che è in esso,
> e che, in quanto inseparabile dal positivo, va assunto in quanto tale. Il
> problema -- per Weber, per Keynes, per Schumpeter, per Mannheim -- è negli
> strumenti capaci di far funzionare *insieme* positivo e negativo (capitale e
> faccia operaia del lavoro), di non permettere una divaricazione dei due
> termini, di *realizzare* la loro complementarietà. Per tutti loro, il tema
> dominante è /243/quello di un futuro in cui l'intero presente venga
> proiettato, di un dominio "razionale" del futuro, di una eliminazione del
> *rischio* che esso comporta[2].

:::

:::

[^2]: Gl. Antonio Negri, *La teoria capitalistica dello stato nel 29: John M.
    Keynes*, v "Contropiano" 1 1968, str. 3 in dalje.

::: alts

> Zato je Mannheim primoran ponuditi mistificirano različico delovanja in
> realnosti utopije[^3].

::: {lang=it}

> È per questo che Mannheim è obbligato a offrire una versione mistificata del
> funzionamento e della realtà dell'utopia[3].

:::

:::

[^3]: Sklicujemo se predvsem na Mannheimovo razlikovanje med "progresivno
    mislijo" in "konzervativno mislijo". Mannheim piše: "Večji del integracij,
    ki jih progresivna misel postavlja pred posamezna dejstva izvirajo iz
    racionalne utopije in vodijo k strukturni viziji totalitete, ki je v
    postajanju". Ideologija se lahko usmeri tudi k "objektom ki so tuji
    realnosti in ki presegajo sedanjo eksistenco", vendar kljub temu, nadaljuje
    Mannheim: "prispevajo k utrjevanju obstoječega reda. Takšna nedosledna
    usmeritev postane utopistična šele, ko si prizadeva porušiti vezi
    obstoječega reda". Namreč "v vsakem zgodovinskem obdobju so obstajale ideje,
    ki so presegale obstoječi red, vendar niso izpolnjevale funkcije utopij:
    bolj so vzpostavljale, kolikor so bile harmonično in organsko vključene v
    prevladujočo vizijo epohe in niso namigovale na revolucionarne možnosti,
    najbolj ustrezne ideologije obdobja". Karl Mannheim, *Das Konservative
    Denken*, in "Archiv für Sozialwissenschaft und Sozialpolitik" 1927\. Toda
    gl. tudi od Mannheim, *Ideologie und Utopie*, Frankfurt a. M. 1952^3, kjer
    je teorija utopije v celoti izpostavljena kot težnja, ki je sama po sebi
    neuresničljiva, sposobna porišiti omejitve obstoječe realnosti "da bi jo
    prepustila svobodnemu razvoju v smeri sledečega reda" (*Ideologia e utopia*
    nav. d., str. 201). Pomembno je enačenje revolucionarnega momenta z
    ideološko strukturo utopije: kritika ideologije se razkrije kot funkcionalna
    za teorijo razvoja v skladu z usmeritvijo, ki se na več načinov razlikuje od
    webrovske.

<!--

[^3]: Ci riferiamo, in particolare, alla distinzione, tracciata dal Mannheim,
    fra "pensiero progressivo" e "pensiero conservatore". "La maggior parte
    delle integrazioni che il pensiero progressivo mette di fronte ai fatti
    particolari -- egli scrive -- scaturiscono dall'utopia razionale e conducono
    a una visione strutturale della totalità che è diviene". L'ideologia può
    anche essa orientarsi verso "oggetti che sono estranei alla realtà e che
    trascendono l'esistenza attuale", ma nondimeno essa "concorre al
    consolidamento dell'ordine esistente. Un tale orientamento incongruente --
    continua Mannheim -- diviene utopistico solo quando tende a rompere i legami
    dell'ordine esistente". Infatti "in ogni periodo della storia vi sono state
    idee trascendenti l'ordine esistente, ma esse non assolvevano la funzione
    delle utopie: esse costituivano piuttosto, nella misura in cui erano
    armoniosamente e organicamente integrate con la visione prevalente
    dell'epoca e non suggerivano possibilità rivoluzionarie, le ideologie più
    adeguate del periodo". Karl Mannheim, *Das Konservative Denken*, in "Archiv
    für Sozialwissenschaft und Sozialpolitik" 1927\. Ma cfr. anche, di Mannheim,
    *Ideologie und Utopie*, Frankfurt a. M. 1952^3, trad. it. Bologna 1952^2, in
    cui è compiutamente esposta la teoria dell'utopia come tendenza in sé
    irrealizzabile, capace di rompere i confini della realtà esistente "per
    lasciarla libera di svilupparsi nella direzione dell'ordine successivo"
    (*Ideologia e utopia* cit., p. 201). È significativa l'identificazione del
    momento rivoluzionario con la struttura ideologica dell'utopia: la critica
    dell'ideologia si rivela qui funzionale alla teoria dello sviluppo, secondo
    una linea per più versi divergente da quella weberiana.

-->

::: alts

> Ideologi, za Mannheima, niso nič drugega kot "razred učenih", ki delujejo kot
> freischwebende Intellektuallen, kot "misleci upravičevanja": njihova dejavnost
> teži k konsolidaciji obstoječega, k podružbljanju vprašanj razvoja[^4].

::: {lang=it}

> Gli ideologhi, per Mannheim, sono nient'altro che una "classe di colti" che
> fungono da freischwebende Intellektuallen, da "pensatori da giustificazione":
> il loro mestiere tende al consolidamento dell'esistente, alla socializzazione
> dei temi dello sviluppo[4].

:::

:::

[^4]: K. Mannheim, *Das konservative Denken* nav. d.

::: alts

> Nasprotno pa za "progresivno misel", "vsaka posamezna stvar prejme svoj zadnji
> pomen zgolj kot neka druga stvar, ki je pred njo ali za njo, iz *utopije
> prihodnosti* ali iz norme nad bitjo, medtem ko je pomen posameznega v
> konzervativne misli izpeljan iz neke stvari za njo, iz preteklosti ali iz tega
> kar predobstaja, četudi samo v zarodku."[^5]

::: {lang=it}

> Al contrario, per il "pensiero progressivo", "ogni singola cosa riceve il suo
> ultimo significato solo da qualche altra cosa che è davanti ad essa o che sta
> sopra di essa, da un'*utopia del futuro* o da una norma al di sopra
> dell'essere, mentre la significazione del particolare viene dedotta nel
> pensiero conservatore da qualche cosa che sta dietro di essa, dal passato o da
> ciò che preesiste almeno in embrione"[5].

:::

:::

[^5]: Prav tam.

::: alts

> Utopija ni torej nič drugega kot "strukturna vizija totalitete, ki je in ki
> postaja",[^6] transcendenca čiste "danosti", orientacijski sistem, ki stremi
> "pretrgati vezi obstoječega reda," da bi jih ponovno zavzel na višji in
> drugačni ravni.[^7] Za Webra in za Mannheima je kritika ideologije eden od
> dinamičnih faktorjev razvoja. Za oba -- kot za Keynesa -- je edina prepoznavna
> realnost *dinamika razvoja*. Mannheimova *utopija*, onkraj potrditev njenega
> avtorja, je prefiguracija končnih in globalnih modelov, v smislu dane
> realnosti: "kritika konzervativne misli" tako postane nujnost, instrument
> usmerjen v osvoboditev dinamičnega delovanja sistema. Nenehno rušenje
> ravnovesja bo treba preobrniti v antiideološko "znanstveno politiko", v
> racionalno rešitev konfliktov, ki jih ustvarja isti razvoj, samo po tem bo
> inherenca teh konfliktov v realnem dialektičnem procesu prepoznana[^8].

::: {lang=it}

> L'utopia è quindi nient'altro che "visione strutturale della /244/totalità che
> è e diviene"[6], trascendimento del puro "dato", sistema di orientamento teso
> a "rompere i legami dell'ordine esistente" per riconquistarli a un più alto e
> diverso livello[7]. Per Weber e per Mannheim, la critica dell'ideologia è uno
> dei fattori dinamici dello sviluppo. Per entrambi -- come per Keynes --
> l'unica realtà individuabile è la *dinamica dello sviluppo*. L'*utopia* del
> Mannheim, al di là delle affermazioni del suo autore, è prefigurazione di
> modelli finali e globali, nel senso della realtà data: la "critica al pensiero
> conservatore" diviene quindi una necessità, uno strumento volto a liberare il
> funzionamento dinamico del sistema. La rottura costante dell'equilibrio si
> dovrà rovesciare in una "politica scientifica", anti-ideologica, in una
> soluzione razionale dei conflitti generati dallo stesso sviluppo, solo dopo
> aver però riconosciuto l'inerenza di quei conflitti al processo dialettico del
> reale[8].

:::

:::

[^6]: Prav tam.

[^7]: Pomembno je poudariti, da se za Mannheima utopija, ko je enkrat potrjena,
    na novo preoblikuje v ideologijo: med ideologijo in utopijo torej vzpostavi
    dialektično razmerje, ki bi lahko spodbudilo, tudi znotraj lastnega
    diskurza, refleksijo o globoko strukturnem značaju same utopije. Zelo je že
    jasno, da so Mannheimove teze poskus odgovoriti na jasno razlago
    funkcionalnosti utopije v *Nemški ideologiji*, v samem *Manifestu
    komunistične stranke*, in v Engelsovem *Znanstveni in utopični socializem*.
    Kakorkoli pa je treba razlikovati ta povsem odločno antimarksistični odziv
    od utopičnih branj Marxa, ki prav tako, v evropski socialdemokratski praksi,
    "realizirajo" "progresivne" funkcije Mannheimove utopije. Z drugimi
    besedami, teorija "revolucionarnega skoka" kot *nujnega* zaradi utopičnih
    razlogov, se izkaže za intimno povezano z reformistično politično prakso:
    kar je zlahka preverljivo s pozorno analizo zgodovine zadnjih petdesetih
    let. To nakazuje, da je vsaka utopična teorija komunizma ne zgolj nemočna,
    temveč tudi objektivno zavajajoča.

<!--

[^7]: È importante notare che per Mannheim l'utopia, una volta affermatasi, si
    trasforma di nuovo in ideologia: fra ideologia e utopia egli stabilisce
    quindi una relazione dialettica, che avrebbe potuto far riflettere, anche
    all'intero del suo discorso, sul carattere profondamente strutturale
    dell'utopia stessa. È sin troppo chiaro che le tesi del Mannheim sono un
    tentativo di risposta alla chiara lettura della funzionalità dell'utopia
    contenuta nell'*Ideologia tedesca*, nello stesso *Manifesto del Partito
    Comunista*, e nell'*Evoluzione del socialismo, dall'utopia alla scienza* di
    Engels. Va comunque distinta tale risposta tutta decisamente antimarxista,
    dalle letture utopiche di Marx, che pure, nella prassi socialdemocrazia
    europea, "realizzano" la funzione "progressista" dell'utopia del Mannheim.
    In altre parole, la teoria del "salto rivoluzionario", visto come
    *necessario* su basi utopiche, si rivela intimamente legato alla prassi
    politica riformista: come è facilmente verificabile a un'attenta analisi
    della storia degli ultimi cinquanta anni. Il che dovrebbe insegnare a
    giudicare non solo impotente, ma oggettivamente mistificante, ogni teoria
    utopica del comunismo.

-->

[^8]: Cfr. K. Mannheim, *Wissenssoziologie*, v *Handwörterbuch der Soziologie*,
    Stuttgart 1931; in Max Weber, *Gesammelte politische Schriften*, Tübingen
    1958^2.

::: alts

> Še obstoječe protislovje v Mannheimovi misli -- utopija kot model, ki je
> povsem zatopljen v realni dinamiki politično-ekonomskih procesov, in njen
> značaj eksperimentalnega pričakovanja projiciranega v prihodnost -- je v
> ozračju vseh intelektualnih del avantgard začetka 20\. stoletja.

::: {lang=it}

> La contraddizione ancora esistente nel pensiero di Mannheim -- l'utopia come
> modello interamente calato nella dinamica reale dei processi
> politico-economici, e il suo carattere di anticipazione sperimentale
> proiettata nel futuro -- è nel clima di tutto il lavoro intellettuale di
> avanguardia del primo '900.

:::

:::

::: alts

> Pri Keynesu in Webru pa je pot že začrtana. Utopija mora "delati" na terenu
> programiranja, opustiti mora teren svoje splošne ideologije: Mannheim
> predstavlja zavest še vedno nevarne vrzeli -- edina resnična "nevarnost", ki
> ogroža proces nenehnega prestrukturiranja realnega -- med racionalnostjo
> projekta, njegovimi izvedbenimi instrumenti in družbeno zavestjo o *nujnosti*
> razvoja.

::: {lang=it}

> Ma con Keynes e Weber la strada è già segnata. L'utopia deve "lavorare" sul
> terreno della programmazione, deve abbandonare il terreno della sua ideologia
> generale: Mannheim esprime la coscienza di uno scarto ancora pericoloso --
> l'unico vero "pericolo" che minacci il processo di perpetua
> ristrut/245/turazione del reale -- fra la razionalità del progetto, i suoi
> strumenti di attuazione, e la coscienza sociale della *necessità* dello
> sviluppo.

:::

:::

::: alts

> Toda on prav tako dela znotraj weberjanskih hipotez intelektualnega dela, ki
> oddaljujejo vsako negativno utopijo; v tej luči je njegova kritika ideologije
> posledica namere, da politični nadzor dinamike sistema napravi znanstven.

::: {lang=it}

> Ma anche lui lavora all'interno dell'ipotesi weberiana di un lavoro
> intellettuale che allontani ogni utopia negativa; sotto tale luce la sua
> critica all'ideologia è conseguente all'intento di rendere scientifico il
> controllo politico sulla dinamica del sistema.

:::

:::

::: alts

> Na ta način se weberjansko *odčaranje*, njegova negacija "vrednote" kot merila
> presoje, resnično postavlja -- to je pozorno opazil Cacciari -- kot zadnja
> posledica negativne in "škandalozne" ničejanske potrditve subjekta. "Weber
> odpravi ta 'škandal': subjekt zdaj ne more biti nič več drugega kot
> subjekt-funkcija, *intelektualec*, sicer se mora vrniti k 'starim cerkvam',
> starim iluzijam in mistifikacijam o 'bogu, ki nam govori', o 'bogu v meni',
> skratka: o substancialnosti ega ... *Odpraviti vsak reduktivni proces pomeni
> predvsem narediti Ego to kar je in mora biti v nepovratnem kontekstu svoje
> 'usode'*".[^9]

::: {lang=it}

> In tal modo, il *disincantamento* weberiano, la sua negazione del "valore"
> come metro di giudizio, si pone veramente -- l'ha acutamente osservato
> Cacciari -- come conseguenza ultima della negativa e "scandalosa" affermazione
> nietzschiana del Soggetto. "Weber toglie questo 'scandalo': il soggetto non
> può ormai che essere il soggetto-funzione, l'*intellettuale* appunto, o
> altrimenti si deve far ritorno alle 'vecchie chiese', alle vecchie illusioni e
> mistificazioni del 'dio che ci parla', del 'dio in me', insomma: della
> sostanzialità dell'Ego ... *Abolire ogni processo riduttivo è, anzitutto, fare
> dell'Ego ciò che è e deve essere nel contesto irreversibile del suo
> 'destino'*"[9].

:::

:::

[^9]: Massimo Cacciari, *Sulla genesi del pensiero negativo*, v "Contropiano" 1
    1969, str. 186--187\. Izjemnega pomena je zveza, ki jo je Cacciari nedvoumno
    vzpostavil med Nietzschejevo kritiko vrednot in Webrovo *uporabo* njihove
    negacije. "Nietzschejeva implicitna in eksplicitna kritika ideje
    Vergeistunga je namenjena prav poudarjanju *izginjanja* Geista iz splošnega
    procesa racionalizacije, ali bolje, kako je treba ta Vergeistung razumeti
    kot funkcionalen za *materialno življenje*, za ohranjanje kapitalističnega
    sistema *v* procesu. Ta Geist ni več Kultur, ki bi bila odtujena ali
    neposredno nasprotna procesom sistema [...] ampak je racionalizacija
    sistema, njegovega vseobsegajočega *obstoja*". Nietzschejeva *Vesela
    znanost* je povsem v tem: v priznavanju, kot dejavno in dejansko načelo,
    obstoja v vseh svojih protislovjih. Nietzschejeva *streznitev* je torej pred
    Webrovim sprejemanjem "usode": tako za Nietzscheja kot za Webra,
    "emancipirati ideologijo sistema od problematike 'vrednot' pomeni ponovno
    odkriti pravo znanstveno miselnost, tudi in prav tedaj, ko kaže, da je
    *prav* sistem tisti, ki se osvobaja od vrednot, ki *hoče-more*, in da je
    torej ideologija *resnična* samo do mere do katere je skladna in strukturno
    funkcionalna v tem materialnem procesu, ter do mere do katere kritizira in
    nasprotuje temu kar ta proces postavlja v dvom ali krizo" (prav tam, str.
    183).

<!--

[^9]: Massimo Cacciari, *Sulla genesi del pensiero negativo*, in "Contropiano" 1
    1969, pp. 186--187\. Di estrema importanza è il nesso inequivocabilmente
    fissato da Cacciari fra la critica nietzschiana dei valori e l'*uso*
    weberiano di quella negazione. "La critica, implicita ed esplicita in tutto
    Nietzsche, dell'idea di Vergeistung -- scrive Cacciari (op. cit., p. 182) --
    vuole appunto mettere in rilievo la *sparizione* del Geist dal processo
    generale di razionalizzazione, o, meglio, come quella Vergeistung vada
    intesa in quanto funzionale alla *vita materiale*, alla conservazione *nel*
    processo, del sistema capitalista. Quel Geist non è più Kultur estranea od
    opposta addirittura al processo del sistema ... ma è razionalizzazione del
    sistema, della sua *esistenza* onniinglobante". La *Gaia scienza*
    nietzschiana è tutta qui: nel riconoscersi come principio attivo, fattivo,
    di un esistere accettato nell'interezza delle sue contraddizioni. Il
    *disincantamento* di Nietzsche è quindi a monte dell'accettazione weberiana
    del "destino": sia per Nietzsche che per Weber, "emancipare l'ideologia
    *del* sistema dalla problematica dei 'valori' è ritrovare la vera mentalità
    scientifica anche, e proprio, allorché vedono che è *questo* sistema che si
    libera dai valori, che *vuole-potere*, e che quindi l'ideologia è *vera*
    soltanto nella misura in cui è coerente e strutturalmente funzionale a
    questo processo materiale, e nella misura in cui critica e si oppone a
    quanto mette in dubbio o in crisi di tale processo" (ibidem, p. 183).

-->

::: alts

> Če je subjekt zdaj sistem, je svoboda od vrednot zdaj svoboda od *lastne*
> subjektivnosti. Relativnost vrednot ne sme biti objekt nove "svete znanosti":
> desakralizacija intelektualne dejavnosti je zgolj nujna premisa za pravilno
> opravljanje te dejavnosti v procesu samoracionalizacije *tega* subjekta.
> *Izguba avre*, za Benjamina, predstavlja natančno to: integracijo
> subjektivnega momenta v celosten mehanizem racionalizacije; toda istočasno
> opredelitev nekakšne "etike racionalizacije", ki je vsa obrnjene vase. Procesi
> koncentracije kapitala, njegova socializacija, nenehno višanje njegove
> organske sestave takšno etiko narekujejo kot nujnost. Ta se ne predstavlja več
> kot *zunanja vrednota*, izvzeta je iz relativnosti ideoloških iznajdb. Etiko
> razvoja bo treba realizirati *skupaj* s samim razvojem, znotraj njegovih
> procesov: obljuba *osvoboditve od stroja* mora izhajati iz zamisli natančno
> nadzorovanje prihodnosti.[^10]

::: {lang=it}

> Se il soggetto è ora il Sistema, la libertà dal valore è libertà dalla
> *propria* soggettività. La relatività del valore non deve essere oggetto di
> nuove "scienze sacre": la desacralizzazione dell'attività intellettuale è solo
> la necessaria premessa per il funzionamento corretto di quell'attività, nel
> processo di autora/246/zionalizzazione di *quel* Soggetto. La *caduta
> dell'aura*, per Benjamin, esprime esattamente questo: integrazione del momento
> soggettivo nel meccanismo complessivo della razionalizzazione; ma nello stesso
> tempo individuazione di un'"etica della razionalizzazione", tutta rivolta su
> se stessa. I processi di concentrazione del Capitale, il suo stesso
> socializzarsi, il costante elevarsi della sua composizione organica, impongono
> come necessità una tale etica. Questa non si presenta più come *valore
> esterno*, è sottratta alla relatività dell'invenzione ideologica. L'etica
> dello sviluppo dovrà realizzarsi *insieme* allo sviluppo stesso, all'interno
> dei suoi processi: la promessa di *liberazione dalla macchina* deve scaturire
> da una immaginazione del futuro accuratamente controllata[10].

:::

:::

[^10]: V tem smislu v Marcusejevem utopizmu -- vsaj v Marcuseju *Uma in
    revolucije* in *Erosa in civilizacije* -- ni težko videti poskusa etične
    odrešitve, ki temelji povsem v progresistični funkcionalnosti negacije. S
    povsem drugačno intelektualno globino najdemo to antiheglovsko ponovno
    odkritje negativnosti pri Blochu (gl. Ernst Bloch, *Geist der Utopie*,
    München 1918; in *Freiheit und Ordnung. Abriss der Sozialutopien*, Berlin
    1947). Etična vrednost utopije, kot poslednjega humanističnega odrešenja, je
    tudi v središču dela Martina Buberja, *Pfade in Utopia*, Heidelberg 1950;
    ital. prev. *Sentieri in Utopia*, Milano 1967.

<!--

[^10]: In tal senso non è difficile vedere nell'utopismo marcusiano -- almeno
    del Marcuse di *Ragione e rivoluzione* e di *Eros e civiltà* -- un tentativo
    di riscatto etico tutto fondato sulla funzionalità progressista della
    negazione. Con ben altra profondità intellettuale tale riscoperta
    antihegeliana della negatività è in Bloch (cfr. Ernst Bloch, *Geist der
    Utopie*, München 1918; e *Freiheit und Ordnung. Abriss der Sozialutopien*,
    Berlin 1947). Il valore etico dell'utopia, come ultimo riscatto umanistico,
    è anche al centro dell'opera di Martin Buber, *Pfade in Utopia*, Heidelberg
    1950; trad. it. *Sentieri in Utopia*, Milano 1967.

-->

::: alts

> Ves *oskrunjajoč* [dissacrante] boj evropske intelektualne avantgarde v prvih
> dveh desetletjih 20\. stoletja se postavlja na raven tega priznavanja novih
> funkcij intelektualnega dela: razvoj, ki se kaže kot dinamičen in dialektičen,
> zahteva *plan* proti nenehni nevarnosti notranjega izbruha. V tem smislu se te
> avantgarde, bolj kot rušijo stare rede in bolj kot poudarjajo skladnost
> realnega kot "kraljestvo absurda", objektivno preobrne v ideološke
> anticipacije, v *delne utopije* plana.

::: {lang=it}

> Tutta la lotta *dissacrante* delle avanguardie intellettuali europee nei primi
> due decenni del nostro secolo si pone al livello di tale riconoscimento delle
> nuove funzioni del lavoro intellettuale: lo sviluppo, rivelatosi in quanto
> dinamica e dialettica, chiede un *piano* contro il costante pericolo di una
> deflagrazione interna. È in questo senso che quelle avanguardie, quanto più
> demoliscono i vecchi ordinamenti e quanto più sottolineano la consistenza del
> reale come "regno dell'assurdo", si rovesciano oggettivamente in anticipazioni
> ideologiche, in *utopie parziali* di piano.

:::

:::

::: alts

> Ideologija je zdaj enkrat za vselej *dana* v formi dialektike, ki *temelji na
> negativnem*, ki protislovje naredi v element gonila razvoja, ki realnost
> sistema prepoznava *v prisotnosti protislovij*. Takšna dialektika nima več
> potrebe po nenehnem nazadovanju v ideologijo. Ker ne vzpostavlja abstraktne
> sheme vedenja, temveč brani, istočasno, realne temelje kapitalističnih
> produkcijskih razmerij in strategijo plana kapitala, upošteva vsak utopistični
> model in vsako možnost razvoja ideologije same. Iz ideološkega stališča, vsaka
> razdelava opravljena v institucionalnem sistemu vrednot ni nič drugega kot
> zgolj in samo "ponavljanje". Iz stališča sistema ni možnosti "napredka"
> samozavedanja sistema. Ideologija ne more drugega kot ponovno stopiti na že
> prehojene stopinje, da bi ponovno odkrila, znova in znova, svojo najvišjo
> formo v formi *posredovanja*. Kvečjemu lahko obstajajo "tehnični" napredki
> disciplinarnih prevodov ideologije -- toda zgodovina literarnih in umetniških
> neoavantgard bi lahko v tem pogledu moderne vestalke disciplinarne
> *zavezanosti* veliko naučila. Pravi problem je vedeti do katere točke ta
> nenehna reprodukcija ideologije na sebi še vedno ohranja bistvene vloge, ki
> jih je odkrila v fazi vzela in stabilizacije buržoazno-kapitalističnega
> sistema.

::: {lang=it}

> L'ideologia è ormai *data* una volta per tutte nella forma di una dialettica
> che *si fondi sul negativo*, che faccia della contraddizione l'elemento
> propulsore dello sviluppo, che riconosca la realtà del sistema *a partire
> dalla presenza della contraddizione*. Una tale dialettica non ha più alcun
> bisogno di regredire continuamente a ideologia. Non costituendo uno schema
> astratto di comportamento, ma definendo, contemporaneamente, i reali
> fondamenti dei rapporti di produzione capitalistici e la strategia del piano
> del capitale, essa fa ragione di ogni modello utopistico e di ogni possibilità
> di sviluppo dell'ideologia stessa. /247/Dal punto di vista ideologico,
> ogni elaborazione compiuta in un sistema istituzionale di valori non è che un
> puro e semplice "ripetere". Non c'è possibilità di un "progresso"
> dell'autocoscienza del sistema dal punto di vista del sistema. L'ideologia non
> può che ripercorrere le tappe già superate per riscoprire, sempre e di
> continuo, la forma più alta di se stessa nella forma della *mediazione*. Al
> massimo, potranno esistere progressi "tecnici" nelle traduzioni disciplinari
> dell'ideologia -- ma la vicenda delle neoavanguardie letterarie e artistiche
> avrebbe molto da insegnare al proposito alle moderne vestali dell'*impegno*
> disciplinare. Il vero problema è sapere fino a che punto tale continuo
> riprodursi dell'ideologia su se stessa conservi ancora i ruoli essenziali da
> essa ricoperti nella fase di decollo e stabilizzazione del sistema
> borghese-capitalistico.

:::

:::

::: alts

> Tudi v svoji najvišji formi, ki se konkretizira v utopiji, se zdi, da sta
> ideologija in razviti kapitalistični sistem v protislovju. V tej fazi nima
> smisla enostavno utemeljevati inherentnosti negacije znotraj sistema: problem,
> ki se postavlja, je povsem "tehničen", usmerjen povsem v opredelitev realnih,
> konkretnih načinov, znotraj ekonomsko-produktivne baze, ki *"negativnost" --
> delavsko negacijo -- dejansko napravi v funkcionalno* "nujnost", ki je
> notranja procesu sistema[^11].

::: {lang=it}

> Anche nella sua forma più alta, quella che si concreta nell'utopia, ideologia
> e sistema capitalistico sviluppato appaiono in contraddizione. Non serve più a
> nulla, in tale fase, fondare semplicemente l'inerenza della negazione al
> sistema: il problema che si pone è tutto "tecnico", tutto rivolto
> all'individuazione dei modi reali, concreti, interni alla base
> economico-produttiva, che *facciano effettivamente funzionare la "negatività"*
> -- la negazione operaia -- come "necessità" intrinseca al processo del
> sistema[11].

:::

:::

[^11]: V zvezi s tem so značilne teorije Abendortha in Dahrendorfa o nujnem
    vključevanju konkurence v tovarno in družbo. Gl. Ralf Dahrendorf, *Soziale
    Klassen und Klassenkonflikt in der industriellen Gesellschaft*, Stuttgart
    1957; ital. prev. po popravljeni in razširjeni angleški izdaji, Bari 1970;
    in W. Abendorth, *Antagonistiche Gesellschaft und politiche Demokratie*,
    Neuwied-Berlin 1967. 

<!--

[^11]: Tipiche, al proposito, le teorie di Abendroth e di Dahrendorf, sulla
    necessaria integrazione della competizione nella fabbrica e nella società.
    Cfr. Ralf Dahrendorf, *Soziale Klassen und Klassenkonflikt in der
    industriellen Gesellschaft*, Stuttgart 1957; trad. it., dall'edizione
    inglese riveduta e ampliata, Bari 1970; e W. Abendorth, *Antagonistiche
    Gesellschaft und politiche Demokratie*, Neuwied-Berlin 1967.

-->

::: alts

> Nič več Hegel, ampak Keynes, nič več neučinkovita ideologija plana, ampak plan
> v konkretnosti svojega razvoja, nič več ideologija New Deala ampak
> postkeynesijanska kritika prevedena v anticiklične tehnike: ideologija se
> konkretizira, se očisti vsake utopistične sledi, se znajde neposredno v
> posameznih področjih delovanja. Kar je tako kot bi rekli, da sama sebe
> potlači.

::: {lang=it}

> Non più Hegel ma Keynes, non l'ideologia ineffettuale del piano ma il piano
> nella concretezza del suo sviluppo, non l'ideologia del New Deal ma la critica
> post-keynesiana tradotta in tecniche anticicliche: l'ideologia si fa concreta,
> si depura di ogni tratto utopistico, si cala direttamente nei singoli settori
> di intervento. Che è come dire sopprime se stessa.

:::

:::

::: alts

> Govorjenje s strani buržoazije o "krizi ideologije" prikriva prav to realnost:
> toženje nad krizo je samo pokazatelj nore nostalgije za tradicijo, ki temelji
> na neučinkovitosti Kultur.

::: {lang=it}

> Il vociare di parte borghese sulla "crisi delle ideologie" nasconde
> esattamente tale realtà: la lamentatio sulla crisi è solo indice di un'insana
> nostalgia per la tradizione basata sull'ineffettualità della Kultur.

:::

:::

::: alts

> Plan na eni strani teži k identifikaciji z institucijami, ki vzdržujejo njegov
> obstoj, na drugi strani pa se postavlja kot specifična institucija njega
> samega. Gospostvo totalnega kapitala se tako uresničuje izven vsakršne logike,
> ki je tuja njegovim lastnim neposrednim mehanizmom, brez vsakršne zunanje
> utemeljitve za vse večjo realizacijo konkretnih instrumentov poseganja, z
> najbolj absolutno neodvisnostjo od kakršnega koli abstraktnega "etičnega"
> cilja, od kakršne koli teleologije, od kakršnega koli "najstva".

::: {lang=it}

> Il Piano tende da un lato a identificarsi con le istituzioni che ne sostengono
> l'esistenza, dall'altro a porsi come speci/248/fica istituzione esso
> stesso. Il dominio del capitale complessivo si realizza così al di fuori di
> ogni logica estranea ai propri diretti meccanismi, privo di giustificazoni
> esterne alla realizzazione crescente degli strumenti concreti di intervento,
> nella più assoluta indipendenza da ogni astratto fine "etico", da ogni
> teleologia, da ogni "dover essere".

:::

:::

::: alts

> Vendar pa obstaja skupni element, ki povezuje intelektualna pričakovanja prvih
> desetletij 20\. stoletja. To kar teorije Webra, Maxa Schelerja ali Mannheima
> potrdijo kot "nujen" preskok metod v strukturi intelektualnega dela, kar
> Keynes in nato Schumpeter vrneta na področje ekonomskega plana, ki
> predpostavlja jasno delovanje celotnega kapitala, kar ideologije avantgard
> predstavijo kot predlog družbenega vedenja, je vsekakor transformacija
> tradicionalne ideologije, praktično opredeljene, v utopijo kot prefiguracijo
> abstraktnega končnega momenta razvoja, ki sovpada z globalno racionalizacijo,
> z nalogo *realizacije dialektike*.

::: {lang=it}

> Esiste comunque un elemento comune che lega fra loro le anticipazioni
> intellettuali dei primi decenni del '900\. Ciò che le teorie di Weber, di Max
> Scheler o di Mannheim sanciscono come "necessario" salto di metodo nella
> struttura del lavoro intellettuale, che Keynes e poi Schumpeter riconducono
> nell'ambito di un piano economico che presuppone l'azione articolata del
> capitale complessivo, che le ideologie dell'avanguardia introducono come
> proposta di comportamento sociale, è decisamente la trasformazione
> dell'ideologia tradizionale, praticamente definita, in utopia come
> prefigurazione di un astratto momento finale dello sviluppo, coincidente con
> una razionalizzazione globale, con una compiuta *realizzazione della
> dialettica*.

:::

:::

::: alts

> To se morda ne zdi povsem točno, kar se tiče Webra ali Keynesa. Dejansko, to,
> kar je pri njima še utopija, je zgolj ostanek, ki je v celoti preoblikovan v
> dinamični model, potem ko je kapital razrešil problem ustvarjanja novih
> institucij, ki so sposobne lastna notranja protislovja uporabiti kot pogonske
> elemente razvoja. Ni slučajno, da se ekonomski modeli zdaj konfigurirajo tako,
> da *izhajajo iz krize* in ne, abstraktno, *proti* krizi; niti moderne
> ideologije "protislovij" niso tuje temu procesu pozitivne realizacije
> dialektike.

::: {lang=it}

> Ciò può sembrare non del tutto esatto per quanto riguarda un Weber o un
> Keynes. In effetti, quanto è ancora in essi di utopia è solo un residuo che
> agisce per trasformarsi compiutamente in modello dinamico, una volta che il
> capitale avrà risolto il problema della creazione delle nuove istituzioni
> capaci di far funzionare le proprie interne contraddizioni come elementi
> propulsivi dello sviluppo. Non casualmente, i modelli economici si
> configurano, ora, *a partire dalla crisi* e non, astrattamente, *contro* la
> crisi; né le moderne ideologie "della contraddizione" sono estranee a tale
> processo di realizzazione positiva della dialettica.

:::

:::

::: alts

> Zatorej se, v obravnavanem trenutku, poudarja notranja razdrobljenost
> funkcionalne vizije intelektualnega dela, ko ta prehaja v sfero produktivnega
> dela. Problem njegove umeščenosti v cikle ali njegovega programiranja ostaja
> še vedno popolnoma odprt: toda gotovo je, da se intelektualno delo, ki ima
> pogum, da se prepozna in deluje kot kapitalistična znanost, objektivno loči od
> nazadnjaških vlog čisto ideološkega dela. Odslej je sinteza nemogoča. Sama
> utopija označuje, z lastno smerjo pohoda, zaporedne stopnje lastnega izumrtja.

::: {lang=it}

> Risulta pertanto, nel momento considerato, un'accentuazione del frazionamento
> interno alla visione funzionale del lavoro intellettuale, man mano che questo
> si cala nella sfera del lavoro produttivo. Rimane ancora del tutto aperto il
> problema della sua collocazione nei cicli o nella loro programmazione: ma è
> certo che il lavoro intellettuale che ha il coraggio di riconoscersi e
> funzionare come scienza capitalistica si separa oggettivamente dai ruoli
> arretrati del lavoro puramente ideologico. Da ora in poi la sintesi è
> impossibile. L'uto/249/pia stessa segna, con la propria direzione di
> marcia, le tappe successive della propria estinzione.

:::

:::

::: alts

> Gre za razcep, ki bo v prihodnosti samo še bolj izrazit, saj bo vrzel med
> tistimi institucijami, ki realizirajo tehnike plana in tistimi, ki nadzorujejo
> njegovo dinamiko vse večja.

::: {lang=it}

> E si tratta di una scissione destinata ad acuirsi in seguito, man mano che fra
> le istituzioni che realizzano le tecniche di piano e quelle che ne controllano
> la dinamica si apre una divaricazione crescente.

:::

:::

::: alts

> Vse te tendence je treba razumeti v konkretni zgodovinski realnosti, ki je
> sledila 1917 in Versajski pogodbi, v globoki protislovnosti, ki pretresa
> evropski in ameriški kapitalizem. Bilo bi povsem nezgodovinsko predstavljati
> si povsem razvito in delujočo kapitalistično zavest, v smislu projekta, ki
> teži k stvaritvi novih institucij za kapital, ki se zaveda, da se mora
> preoblikovati v družbeni kapital in ki mora kot tak neposredno upravljati z
> lastnimi cikli, lastnimi krizami, lastnim razvojem.

::: {lang=it}

> Tali tendenze vanno tutte colte, nella concreta realtà storica che segue al
> '17 e al trattato di Versailles, nella profonda contraddittorietà che scuote
> il capitalismo europeo e americano. Sarebbe del tutto antistorico figurarsi
> una coscienza capitalistica tutta dispiegata e operante, nel senso di un
> progetto teso alla creazione di nuove istituzioni per un capitale consapevole
> di doversi trasformare in capitale sociale e di dover gestire direttamente, in
> quanto tale, i propri cicli, le proprie crisi, il proprio sviluppo.

:::

:::

::: alts

> Toda šlo bi za lažna objektivnost, če bi očitna neravnovesja, politične
> zaostalosti, artikulacije in notranjo razpravo, ki zaznamuje 20\. leta 20\.
> stoletja, označili za slepe blodnje, ki jih prekine le nekaj preroških in
> genialnih splošnih prefiguracij.

::: {lang=it}

> Ma sarebbe falsa oggettività sottolineare gli evidenti squilibri, le
> arretratezze politiche, l'articolazione e il dibattito interno, che
> caratterizzano gli anni '20, come un cieco vaneggiamento, rotto solo da alcune
> profetiche e geniali prefigurazioni complessive.

:::

:::

::: alts

> S stališča naše analize je kakorkoli treba omeniti, da je, prav v tem
> kritičnem trenutku, vloga intelektualcev konfigurirati utopije za namene
> redimenzioniranja taistega kulturnega dela. Na polju intelektualnega dela gre
> predvsem za zavzemanje stališča okoli ene stvari: okoli teme *političnosti*
> ali nepolitičnosti tega dela znotraj avantgardne perspektive.

::: {lang=it}

> Dal punto di vista della nostra analisi, è comunque da rilevare che proprio in
> quel frangente critico, il ruolo degli intellettuali è di configurare utopie
> per lo più rivolte a ridimensionare lo stesso lavoro culturale. Nel campo del
> lavoro intellettuale si tratta di prendere partito su una cosa,
> principalmente: sul tema della *politicità* o meno di quel lavoro stesso,
> nella prospettiva delle avanguardie.

:::

:::

::: alts

> Poskusimo soočiti dve navidezno nezdružljivi stališči. Šklovski leta 1926, ko
> zagovarja absolutno avtonomijo literature kot "besedne umetnosti", ki je ni
> mogoče zvesti na zakone, ki so tuji njenemu lastnemu konstruiranju, piše: "Mi
> futuristi našo umetnost povezujemo s Tretjo internacionalo. Toda to, tovariši,
> je predaja vseh položajev! Je Belinski-Vengerov, je *Zgodovina ruske
> inteligence*!"[^12] Nadaljeval je s še bolj eksplicitnim zavzemanjem stališča
> proti "angažirani", agitacijski, propagandni umetnosti, ki jo je zagovarjal
> Majakovski in LEF: "Ne želim zagovarjati umetnosti v imenu umetnosti, temveč
> propagando v imenu propagande [...] agitacija, ki se izvaja v operi, v filmu,
> na razstavah je neuporabna: konča se tako, da pokonča samo sebe. V imenu
> agitacije, odstranite jo od umetnosti!"[^13]

::: {lang=it}

> Proviamo a confrontare fra loro due prese di posizione apparentemente
> inconciliabili. "Noi futuristi -- scrive Šklovskij nel '26, difendendo
> l'assoluta autonomia della letteratura come 'arte verbale', irriducibile a
> ragioni estranee al suo stesso costruirsi[12] -- colleghiamo la nostra arte
> con la Terza Internazionale. Ma questo, compagni, è un arrendersi a
> discrezione! È un Belinskij-Vengerov, è la *Storia dell'intelligencija
> russa!*". Proseguendo poi, in modo ancora più esplicito, nel prendere
> posizione contro l'arte "impegnata", di agitazione, di propaganda, propugnata
> da Majakovskij e dal LEF: "Non voglio /250/difendere l'arte in nome
> dell'arte, bensì la propaganda in nome della propaganda ... L'agitazione
> svolta in un'opera cantata, nei film, con le mostre, è inutile: finisce per
> divorare se stessa. In nome dell'agitazione, toglietela dall'arte!"[13].

:::

:::

[^12]: Viktor Šklovskij, *Ulla, ulla Marziani!*, v *Chod Konja*, Moskvà-Berlin
    1926; ital. prev. *La mossa del cavallo*, Bari 1967, str. 35--38.

[^13]: Prav tam, str. 41.

::: alts

> André Breton leta 1920 v Drugem manifestu nadrealizma, po tem ko spozna, da
> misel "ne more drugega kot nihati med zavestjo o svoji popolni avtonomiji in
> tej o svoji strogi odvisnosti", se poglobi v to protislovje, ki je dano kot
> nujno in neizogibno, ter se odloči za literaturo, ki je "nepogojena in
> pogojena, utopistična in realistična, ki vidi svoj namen samo v sebi sami in
> ne želi drugega kot služiti."[^14]

::: {lang=it}

> Nel 1920 André Breton, nel Secondo manifesto surrealista, dopo aver
> riconosciuto che il pensiero "non può far altro che oscillare fra la coscienza
> della sua perfetta autonomia e quella della sua rigorosa dipendenza", si
> immerge in questa contraddizione data per necessaria e ineluttabile, optando
> per una letteratura "incondizionata e condizionata, utopistica e realistica,
> che veda il suo scopo solo in se stessa e non voglia nient'altro che
> servire"[14].

:::

:::

[^14]: Odlomek je iz drugega nadrealističnega manifesta.

<!-- napaka pri letnici za manifest -->

::: alts

> Enzensberger je v zvezi s citiranim Bretonovim odlomkom pripomnil, da so
> "nadrealisti v svoj program povzdignili kvadraturo kroga"[^15]. Toda ta
> program ni bil značilen za nadrealizem: ta ni storil drugega kot jasno izrazil
> prizadevanja celotnega območja intelektualnih avantgard, ki so odločile
> uporabiti politični teren za obvarovanje zadnje fronte na kateri je možno
> ubraniti intelektualno delo v svojih institucionalnih oblikah.

::: {lang=it}

> Enzensberger ha osservato, a proposito del passo citato di Breton, che "i
> surrealisti elevarono a loro programma la quadratura del cerchio"[15]. Ma
> quel programma non è specifico del Surrealismo: questo non ha fatto che
> rendere esplicite le aspirazioni di tutta l'area delle avanguardie
> intellettuali che avevano scelto di fruire del terreno politico per
> salvaguardare un'ultima frontiera su cui attestare la difesa del lavoro
> intellettuale nelle sue forme istituzionali.

:::

:::

[^15]: Hans Magnus Enzensberger, *Gemeinplätze, die Neuste Literatur
    betreffend*, "Kursbuch" 15, Frankfurt a. M. 1968, ital. prev. v
    Enzensberger, Michel, Schneider, *Litteratura e/o rivoluzione*, Milano 1970,
    št. 14.

::: alts

> Ne gre za reakcionaren ideološki projekt proti "naprednemu", kar se srečuje v
> dveh izjavah Šklovskega in Bretona. Formalizem in nadrealizem se v bistvu
> strinjata v njunem zagovoru "profesionalnosti" intelektualnega dela. Samo da
> prvi, z večjo lucidnostjo in pogumom, priznava svoj tavtološki značaj, svojo
> lastno neaktualnost (vsaj do leta 1926), medtem ko se druga odloči sebe
> povzdigniti v simbol intelektualne "slabe vesti".

::: {lang=it}

> Non sono un progetto ideologico reazionario contro uno "avanzato" che si
> scontrano nelle due dichiarazioni di Šklovskij e di Breton. Formalismo e
> Surrealismo concordano, in sostanza, nella loro difesa della "professionalità"
> del lavoro intellettuale. Solo che il primo, con maggiore lucidità e coraggio,
> riesce a confessare il proprio carattere tautologico, la propria inattualità
> (fino al '26, almeno), mentre il secondo sceglie di elevare se stesso a
> emblema della "cattiva coscienza" intellettuale.

:::

:::

::: alts

> Vendar je prav tako pomembno omeniti, da kjer se formalizem -- in z njim
> abstraktne avantgarde v vseh sektorjih vizualne in literarne komunikacije --
> oblikuje kot šola *dela z jezikom*, nadrealizem -- in z njim vse "angažirane"
> avantgarde -- ne upoštevajo sistematike intelektualnega dela in se težijo
> predstavljati kot politična *intervencija* tout-court.

::: {lang=it}

> Ma è ugualmente importante notare che laddove il Formalismo -- e con lui le
> avanguardie astratte, in tutti i settori della comunicazione visiva e
> letteraria -- si configura come scuola di *lavoro sul linguaggio*, il
> Surrealismo -- e con lui tutte le avanguardie "impegnate" -- ignora la
> sistematicità /251/del lavoro intellettuale e tende a porsi come
> *intervento* politico tout-court.

:::

:::

::: alts

> Z drugimi besedami, soočamo se z dvema tendencama, ki se nadaljujeta do danes
> in pri tem sledita dvema različnima in komplementarnima smerema:

::: {lang=it}

> In altre parole, siamo di fronte a due tendenze che si perpetueranno, fino a
> oggi, seguendo due direzioni diverse e complementari:

:::

:::

::: alts

> 1. na eni strani gre za smer, kjer intelektualno delo sebe priznava v svojem
> bistvu prav kot *delo* in torej neizterljivo za revolucionarno gibanje;
> avtonomija tega dela je izrazito prepoznana kot *relativna*: le politični
> ali ekonomski naročnik bo lahko podal *smisel* čisti vzpostavitvi
> racionalne platforme, ki jo zagotavlja
> interna kritika, ki se opravlja na disciplinah.

::: {lang=it}

> 2. da un lato è il riconoscersi, da parte del lavoro intellettuale,
> essenzialmente come *lavoro* appunto, non recuperabile, quindi, a un
> movimento rivoluzionario; l'autonomia di tale lavoro è esplicitamente
> riconosciuta come *relativa*: è solo il committente politico o economico
> che potrà dettare un *senso* al puro allestimento di una piattaforma
> razionale, assicurata dalla critica interna condotta sulle discipline.

:::

:::

::: alts

> 2. na drugi gre za intelektualno delo, ki se zanika kot tako in se postavlja
> kot čista *ideologija*; ki želi politično organizacijo ali nadomestiti, ali
> slaviti, ali jo kritizirati od znotraj. Vedno pa s ciljem *izstopiti* iz
> produktivnega dela in se mu zoperstaviti kot njegova *kritična zavest*.

::: {lang=it}

> 2. dall'altro lato è un lavoro intellettuale che nega se stesso come tale, e
> si pone come pura *ideologia*; che vuole sostituirsi all'organizzazione
> politica, o celebrarla, o criticarla dall'interno. Sempre, comunque, con
> l'obiettivo di *uscire* dal lavoro produttivo, e di porsi di fronte a
> questo come sua *coscienza critica*.

:::

:::

::: alts

> Tu se postavlja problem posredovanja med tema dvema držama: to je velika tema
> Benjamina, konstruktivistične umetnosti in arhitekture, socialdemokratskih
> tehnik upravljanja mesta, urbanistične utopije srednjeevropske kulture 30\.
> let[^16].

::: {lang=it}

> Si pone quindi il problema della mediazione fra tali due atteggiamenti: è il
> grande tema di Benjamin, dell'arte e dell'architettura costruttiviste, delle
> tecniche di gestione socialdemocratica della città, delle utopie urbanistiche
> della cultura mitteleuropea degli anni '30[16].

:::

:::

[^16]: To temo smo že naslovili (gl. M. Tafuri, *Per una critica dell'ideologia
    architettonica*, v "Contropiano" 1 1969, str. 31--79), toda glej tudi: B.
    Miller-Lane, *Architecture and Politics in Germany 1918--1945*, Harvard
    University Press, 1968; in E. Collotti, *Il Bauhaus nell'esperienza
    politico-sociale della repubblica di Weimar*, v "Controspazio" 4-5 1970,
    str. 8--15.

<!--

[^16]: Abbiamo già affrontato tale tema (cfr. M. Tafuri, *Per una critica
    dell'ideologia architettonica*, in "Contropiano" 1 1969, pp. 31--79), ma si
    veda anche: B. Miller-Lane, *Architecture and Politics in Germany
    1918--1945*, Harvard University Press, 1968; e E. Collotti, *Il Bauhaus
    nell'esperienza politico-sociale della repubblica di Weimar*, in
    "Controspazio" 4-5 1970, pp. 8--15.

-->

::: alts

> Končni pomen teh gibanj je samo en. Intelektualne avantgarde morajo zdaj
> zavzeti področje, od katerega so se do zdaj skrbno oddaljevale: področje
> *dela*. Ker ni več mogoče vzdrževati institucionalne distance do produktivnega
> dela, ki je v preteklosti zagotavljala *svetost* intelektualnega raziskovanja,
> ne preostane drugega kot prostovoljna poteza, ki implicira uničenje lastnih
> klasičnih vlog. Benjaminova "izguba avre" ni zgolj posledica posplošitve novih
> načinov produkcije, ampak tudi plod zavestne odločitve, v katero se je
> spustila vsa volja do preživetja, ki je vsebovana v halucinirani
> protiinstitucionalni bitki, ki jo je vodilo negativno mišljenje.

::: {lang=it}

> Il significato ultimo di tali movimenti è uno solo. Le avanguardie
> intellettuali debbono ora occupare l'area dalla quale si erano tenute fino ad
> allora attentamente lontane: quella *del lavoro*. Non essendo più possibile
> mantenere la distanza istituzionale dal lavoro produttivo che aveva assicurato
> nel passato la *sacralità* della ricerca intellettuale, non rimane che
> compiere volontariamente un passo che implica la distruzione dei propri
> classici ruoli. La benjaminiana "caduta dell'aura" non è solo indotta della
> generalizzazione dei nuovi modi di produzione, ma è anche il frutto di una
> scelta cosciente, dentro la quale si è calata tutta la volontà di
> sopravvivenza contenuta nell'allucinata battaglia antiistituzionale intrapresa
> dal pensiero negativo.

:::

:::

::: alts

> Prav tako ne smemo podcenjevati *zahteve*, ki jo je najnaprednejši kapital
> postavljal intelektualcem 20\. in 30\. let 20\. stoletja. Rathenau in Ford
> izrecno izrazita svoje zahteve. Ne zahtevata, da intelektualci neposredno
> vstopijo v nadzorovanje ciklov produkcije, zahtevata pa, da njihov prispevek
> jasno in eksplicitno "podeli pomen" samemu ciklu. Z vstopom v sfero dela, se
> ideološka produkcija hkrati privlači in odbija. Na eni strani je poklicana k
> produkciji globalnih modelov: racionalizacija ciklov ne sme biti ločena od
> njihove socializacije; inovativni modeli morajo odgovarjati na zahteve, ki
> sočasno tvorijo moment notranjega prestrukturiranja ciklov in moment
> cirkulacije blaga. Na drugi strani je omejena na vlogo uvrednotevanja
> konstantnega kapitala: njegov poseg mora neposredno zarezati v socializacijo
> konsumpcije. Z zamejevanjem vpadnega polja intelektualnega dela na "družben"
> moment, ne počne drugega kot lastnosti svojih tradicionalnih vlog premika na
> drugačno raven. Zbrana znotraj sfere produktivnega dela -- toda same še ne
> spremenjena v produktivno delo -- je intelektualna razdelava usmerjena v
> "funkcionalizacijo" ideološke produkcije, ne v njeno opustitev tout-court.

::: {lang=it}

> /252/Né bisogna sottovalutare la *domanda* rivolta agli intellettuali
> da parte del Capitale più avanzato degli anni '20--'30\. Rathenau e Ford
> avanzano esplicitamente le loro richieste. Non si pretende che l'intellettuale
> entri direttamente nel controllo dei cicli di produzione, ma si pretende che
> il suo contributo si ponga come chiara e esplicita "attribuzione di senso" al
> ciclo stesso. Entrando nella sfera del lavoro, la produzione ideologica è
> contemporaneamente attratta e respinta. Da un lato ad essa è chiesta la
> produzione di modelli globali: la razionalizzazione dei cicli non deve
> disgiungersi dalla loro socializzazione; i modelli innovativi debbono
> rispondere a esigenze che fanno contemporaneamente parte dei momenti di
> ristrutturazione interna ai cicli e dei momenti di circolazione della merce.
> Dall'altro lato essa è confinata nel ruolo di valorizzatrice del capitale
> costante: il suo intervento deve incidere direttamente nella socializzazione
> dei consumi. Delimitando il campo di incidenza del lavoro intellettuale al
> momento "sociale", non si fa che spostare a un livello diverso le proprietà
> dei suoi ruoli tradizionali. Accolta dentro la sfera del lavoro produttivo --
> ma non ancora trasformata essa stessa in lavoro produttivo -- l'elaborazione
> intellettuale viene spinta a "funzionalizzare" la produzione ideologica, non
> ad abbandonarla tout-court.

:::

:::

::: alts

> Antiinstitucionalni moment avantgardne inteligence se je razkril kot
> funkcionalen za uničenje izčrpanih vlog: vso njeno rušilno delo se zdaj pojavi
> kot vzpostavitev jasne platforme s katere je treba ponovno začeti odkrivati
> nove "zgodovinske naloge" intelektualnega dela.

::: {lang=it}

> Il momento antiistituzionale dell'intelligencija di avanguardia si è rivelato
> funzionale alla demolizione dei ruoli esauriti: tutta la sua opera demolitoria
> si manifesta ora come allestimento di una piattaforma sgombra, da cui
> ripartire per scoprire i nuovi "compiti storici" del lavoro intellettuale.

:::

:::

::: alts

> Prav tako ne smemo podcenjevati, da imajo te zgodovinske naloge hkrati dva
> sogovornika in dve smeri pohoda: prva je mističen pogovor s kapitalom, ki ga
> berejo kot tehnološko abstrakcijo ali kot univerzalno produktiven subjekt;
> druga je mističen pogovor z množicami, ki jih bere kot enako abstrakten
> subjekt družbene cirkulacije produkcije.

::: {lang=it}

> Né va sottovalutato che tali compiti storici abbiano contemporaneamente due
> interlocutori e due direzioni di marcia: una prima è il colloquio mistico con
> il capitale, letto come astrazione tecnologica o come soggetto produttivo
> universale; una seconda è il colloquio mistico con le masse, lette come
> soggetto ugualmente astratto della circolazione sociale della produzione.

:::

:::

::: alts

> Ideologija nase prevzame nalogo združiti subjekt in objekt produkcije, nalogo
> preseči vse protislovne momente, da bi predstavila modele funkcionalne
> integracije kapitala in dela, produkcijskih načinov in produkcijskih odnosov:
> pretvori se, z drugimi besedami, -- predstavljena je bodisi kot ideologija, ki
> je povsem spuščena v delovni proces, bodisi kot abstrakten projekt
> socializacije dela -- v *kapitalistično-industrijsko utopijo*. Na tej točki je
> nujno potrebno opozoriti, da se vsa opravljena analiza le obrobno dotika
> kapitalističnega stališča. Zgoraj poudarjeno sovpadanje med intelektualnimi
> avantgardami in naprednim kapitalom ni pokazatelj splošnih pojavov: taisto
> sovpadanje, celo, se je zgodovinsko razkrilo za precej omejeno in začasno ter
> vztrajno v najbolj obstranskih sektorjih razvoja.

::: {lang=it}

> L'ideologia si assume in proprio il compito di unificare soggetto e oggetto
> della produzione, di saltare tutti i momenti di contraddizione per presentare
> modelli di funzionamento integrato di capitale e lavoro, di modi e rapporti di
> produzione: si trasforma, in altre parole -- sia che si presenti come
> ideologia tutta calata nel processo lavorativo che come progetto astratto
> /253/di socializzazione del lavoro -- in *utopia capitalistico-industriale*. A
> questo punto è necessario avvertire che tutta l'analisi compiuta non investe
> che marginalmente il punto di vista capitalista. La convergenza sopra
> sottolineata di avanguardie intellettuali e capitale avanzato non è un indice
> di fenomeni generalizzati: quella stessa convergenza, anzi, si rivela
> storicamente alquanto limitata e provvisoria, e insistente su settori
> altamente marginali dello sviluppo.

:::

:::

::: alts

> Zato je pomembno dojeti tako subjektivni značaj sprejetih odločitev
> intelektualnega dela, kot njeno nenehno marginalizacijo v naročju
> kapitalističnega razvoja. Utopija postane funkcionalna razvoju kot rezervat
> tendenčnih modelov in kot ideološko orožje za pridobivanje konsenza.

::: {lang=it}

> È quindi importante cogliere sia il carattere soggettivo delle scelte compiute
> dal lavoro intellettuale, che la costante emarginazione compiuta in seno allo
> sviluppo capitalista. L'utopia diviene funzionale allo sviluppo come area di
> riserva di modelli tendenziali e come arma ideologica per estrazione del
> consenso.

:::

:::

::: alts

> Jasno je, da ti dve usmeritvi vstopita v krizo vsakič, ko se od tendenčnih
> modelov zahteva realna preveritev njihovih ciljev in vsakič, ko se samo
> upravljanje s konsenzom razkrije kot orožje, ki je neustrezno za cilje
> razvoja.

::: {lang=it}

> È chiaro che tali due direzioni entrano in crisi ogni volta che ai modelli
> tendenziali si chiederà una verifica reale dei loro obiettivi e ogni volta che
> la stessa gestione del consenso si rivelerà come un'arma poco adeguata ai fini
> dello sviluppo.

:::

:::

::: alts

> Med prizadevanjem po absolutni avtonomiji in prostovoljnim samoizničenjem v
> misijah, ki so "v službi razreda", se je sodobna ideologija je dokončno
> odločila, v večini primerov in s presenetljivo stanovitnostjo vedenja,
> ustaliti se na robu britve med obema izbirama.

::: {lang=it}

> Fra l'aspirazione all'autonomia assoluta e il volontario autoannullamento in
> missioni "al servizio della classe", l'ideologia contemporanea è finita per
> scegliere, nella maggior parte dei casi e con una costanza di comportamento
> veramente sorprendente, di stabilizzarsi sul filo del rasoio comune ai due
> campi decisionali.

:::

:::

::: alts

> Avtonomija intelektualne raziskave odgovarja nadaljevanju projekta obnove
> Subjektivnosti, ki jo je kapitalistična delitev dela razlastila. Intelektualno
> delo kot "razredni servis" in projekt povrnitve -- posredno -- te iste
> Subjektivnosti razlaščenemu razredu. Menimo, da ni treba poudarjati "bede" teh
> dveh ideoloških usmeritev, katerih objektivno komplementarnost razkrivamo.
> Literatura in umetnost kot instrumenta obnove Totalitete in kot njen prenos v
> nov izvoljen zgodovinski Subjekt -- delavski razred -- sta dela načrta, ki se
> objektivno postavlja na retrogardo kapitalističnega razvoja, četudi s tem
> pokrije natančne naloge.

::: {lang=it}

> L'autonomia della ricerca intellettuale risponde al perpetuarsi del progetto
> di recupero della Soggettività, espropriata dalla divisione capitalistica del
> lavoro. Il lavoro intellettuale come "servizio di classe" è progetto di
> restituzione -- per via indiretta -- di quella stessa Soggettività alla classe
> espropriata. Non c'è bisogno, pensiamo, di sottolineare la "miseria" di tali
> due direzioni ideologiche, di cui stiamo scoprendo l'oggettiva
> complementarietà. La letteratura e l'arte come strumenti di recupero della
> Totalità, e come travaso di questa nel nuovo Soggetto storico per elezione --
> la classe operaia -- sono parte di un disegno che si pone oggettivamente alla
> retroguardia dello sviluppo capitalistico, anche se in questo copre compiti
> precisi.

:::

:::

::: alts

> Kar je precej bolj zanimivo je, kako se ti dve izbiri (ali kompromis, ki
> posreduje med njima) realizirata.

::: {lang=it}

> Ciò che maggiormente interessa, piuttosto, è il modo di realizzarsi di tali
> due scelte (o del compromesso che le media).

:::

:::

::: alts

> Avtonomija formalne konstrukcije, pri vseh zgodovinskih avantgardah, ne
> vztraja več na projektu nadzora vsakdanjega izkustva skozi Formo. Zdaj smo
> pripravljeni sprejeti, da je izkušnja ta, ki obvladuje subjekt in ga
> vzpostavlja: problem je bolj *planiranje izginjanja subjekta*, izničevanje
> tesnobe, ki izhaja iz patetičnega (ali smešnega) upiranja posameznika proti
> strukturam gospostva, ki ga držijo blizu, kazanje na univerzalno pacifikacijo
> -- raj na zemlji je realiziran z "izginotjem tragičnega"[^17] --, ki se prav
> prostovoljno in ponižno podreja tem strukturam gospostva, kot na obljubljeno
> deželo.

::: {lang=it}

> L'autonomia della costruzione formale, in tutte le avanguardie storiche, non
> insiste più sul progetto di controllo dell'esperienza quotidiana tramite la
> Forma. Ora si è disposti ad accet/254/tare che è l'esperienza che
> domina il soggetto e lo fonda: il problema è, piuttosto, *pianificare la
> scomparsa del soggetto*, annullare l'angoscia che deriva dal patetico (o
> ridicolo) resistere dell'individuale di fronte alle strutture di dominio che
> lo stringono da presso, indicare come terra promessa della pacificazione
> universale -- il paradiso in terra è realizzato dalla "scomparsa del
> tragico"[17] -- esattamente il volontario e mansueto sottomettersi a quelle
> strutture di dominio.

:::

:::

[^17]: Izjemno zanimivo je slediti odzivom naprednih teoretikov kapitalistične
    buržoazije in "levega komunizma" 20\. in 30\. let na spoznanje o *razkroju
    Tragedije*. Za slednje je intelektualno nasprotovanje kapitalističnemu
    uničenju buržoaznega Geista, utopije Forme, "problematičnega" humanizma
    enako težnji po natančni obnovi teh neuporabljenih ideoloških instrumentov
    in njihovem predajanju -- z nadaljnjo utopijo -- v roke proletariata.
    *Marksistični humanizem* se tako razkrije kot projekt širitve na delavski
    razred tiste "Forme-Utopije buržoazne biti, ki je Tragedija": buržoazni
    junak je sprevržen v *kolektivnega junaka*. Linije tega procesa so zelo
    jasne v misli mladega Lukácsa kot tudi v misli marksističnega Lukácsa: toda
    tudi na nekaterih straneh Korscha, Löwitha in -- kot čista ideologija --
    Bertolta Brechta. V zvezi s tem gl. A. Asor Rosa, *Il giovane Lukács teorico
    dell'arte borghese*, v "Contropiano" 1 1968, str. 59 in dalje; in L.
    Bedeschi, *Alienazione e feticismo nel pensiero di Marx*, Bari 1968 (in
    particolare l'appendice, str. 177 in dalje).

<!--

[^17]: È di estremo interesse seguire le reazioni alla presa di coscienza della
    *dissoluzione della Tragedia*, da parte dei teorici avanzati della borghesia
    capitalista, e da parte del "comunismo di sinistra" degli anni '20-'30\. Per
    quest'ultimo, l'opposizione intellettuale alla distruzione capitalista del
    Geist borghese, dell'utopia della Forma, dell'umanesimo "problematico", si
    identifica con il tendere verso il recupero puntuale di tali strumenti
    ideologici dismessi, consegnandoli -- con un'ulteriore utopia -- nelle mani
    del proletariato. L'*umanesimo marxista* si rivela così come il progetto di
    estensione alla classe operaia di quella "Forma-Utopia dell'essere borghese
    che è la Tragedia": l'eroe borghese si rovescia nell'*eroe collettivo*. La
    linea di tale processo sono chiarissime nel pensiero del giovane Lukács come
    in quello del Lukács marxista: ma anche in alcune pagine di Korsch, di
    Löwith, e -- come pura ideologia -- in Bertolt Brecht. Cfr. al proposito, A.
    Asor Rosa, *Il giovane Lukács teorico dell'arte borghese*, in "Contropiano"
    1 1968, pp. 59 ss.; e L. Bedeschi, *Alienazione e feticismo nel pensiero di
    Marx*, Bari 1968 (in particolare l'appendice, pp. 177 ss.).

-->

::: alts

> Odrešitve ni več v "uporu", ampak v popolni predaji. Le človeštvo, ki je
> ponotranjilo, napravilo svoje, vpilo ideologijo dela in razvoja, ki ne vztraja
> produkcijo ali Plan obravnavati kot nekaj *drugega od sebe* ali kot enostavne
> instrumente, *ki se prepozna kot del splošnega Plana* in ki kot tako dokončno
> sprejme *funkcionirati* kot zobnik globalnega stroja, se lahko odreši
> "izvornega greha". Da ne gre za to, da je človek izdelal sistem sredstev ne da
> bi lahko nadzoroval "upor predmetov", ki se je sprožil proti izumitelju --
> tako sta Löwith in [mladi Lukács](mladi_lukacs.md) brala odtujenost v Marxu
> --, temveč bolj za "hudičevo" vztrajanje "človeka", da ostane takšen, da se
> umesti kot "nepopolni stroj" v produktivnem univerzumu, ki kot dosledno držo
> dopušča zgolj čisto *tišino*[^18].

::: {lang=it}

> La salvezza non è più nella "rivolta", ma nella resa senza discrezione. Solo
> un'umanità che abbia introiettato, fatto propria, assorbito, l'ideologia del
> lavoro e dello sviluppo, che non persista nel considerare la produzione o il
> Piano come *altro da sé* o come semplici strumenti, *che si riconosca come
> parte di un Piano complessivo*, e che come tale accetti fino in fondo di
> *funzionare* come ingranaggio di una macchina globale, può riscattare la
> propria "colpa originaria". Che non è l'aver prodotto un sistema di mezzi
> senza saper controllare la "rivolta degli oggetti" che si scatena contro
> l'inventore -- così Löwith e il giovane Lukács leggono l'alienazione marxiana
> -- ma che è piuttosto il "diabolico" insistere dell'"uomo" a rimanere tale, a
> porsi come "macchina imperfetta" in un universo produttivo che ammette come
> atteggiamento coerente il solo e puro *silenzio*[19].

:::

:::

[^18]: "Obča proletarizacija ostaja torej poslednja velika podlaga enotnega
    videza človeškega stanja: prekleta ironija kapitalistične družbe jo obsoja
    na nepopravljivo distanco od *realne* proletarizacije prav v trenutku, ko se
    odvija *formalna* proletarizacija. Ko je celotna družba oblikovana po vzoru
    tovarne, ko se zdi, da je forma Identitete realizirana in kroži po celotnem
    sistemu, se razkrije, da gre za mišljenju odvratno podobo: ki mora vedno
    iskati, okraj samega sebe, meje različnosti in živeti za to različnost.
    Pripravljeno se je celo žrtvovati za to, se imeti za blago v obtoku, za
    čisto funkcijo v obči cirkulaciji blag. Na eni strani obče izkoriščanje
    delavcev, sintetična forma, ki je prvič v zgodovini postala realna; na drugi
    pa nobene forme, zgolj del cirkulirajočega kapitala. (N. Licciardello,
    *Proletarizzazione e utopia*, v "Contropiano" 1 1968, str. 109). Na tem
    mestu lahko le bežno namignemo na funkcijo, ki so jo imele umetniške
    avantgarde 20\. stoletja in na teoretsko misel, ki jih je spremljala pri
    konkretizaciji in vizualizaciji *formalne proletarizacije* o kateri govori
    Licciardello: utopija Avantgarde je prav v projektirani obnove nekega
    "enotnega videza človeškega stanja", ki ga zagotavlja intelektualno delo, ki
    *se reši* samo, ko uniči lastne temelje.

<!--

[^18]: "La proletarizzazione generale rimane allora l'ultimo grande sostrato
    dell'apparenza unitaria della condizione umana: una maledetta ironia della
    società capitalista la condanna a una irrimediabile distanza dalla
    proletarizzazione *reale*, proprio nel momento in cui una proletarizzazione
    *formale* è in atto. Quando tutta la società modellata sulla fabbrica,
    quando la forma dell'identico sembra essersi realizzata e circola in tutto
    il sistema, si scopre /255/che questa è una sembianza ripugnante al
    pensiero: che esso deve sempre cercare, al di là di se stesso, un margine di
    diverso, e vivere *per* quel diverso. È disposto persino al sacrificio di sé
    per ciò, a considerare merce circolante, funzione pura nella circolazione
    generale delle merci. Da un lato una forma sintetica, divenuta realtà per la
    prima volta nella storia, che è lo sfruttamento operaio generale; dall'altro
    niente forma, soltanto un pezzo di capitale circolante" (N. Licciardello,
    *Proletarizzazione e utopia*, in "Contropiano" 1 1968, p. 109). In questa
    sede si può solo fuggevolmente accennare alla funzione che hanno avuto le
    avanguardie artistiche del '900, e il pensiero teorico che le accompagna,
    nel rendere concreta, visibile, la *proletarizzazione formale* di cui parla
    Licciardello: l'utopia dell'Avanguardia è proprio nel progettato recupero di
    un'"apparenza unitaria della condizione umana", assicurata da un lavoro
    intellettuale che *si salva* solo nel momento in cui devasta le proprie
    fondamenta.

-->

::: alts

> Kar pri tej ideologiji brezpogojnega konsenza v univerzum kapitala osupne, ni
> toliko njegova literarnost, kot njegov naivni radikalizem. Ne obstaja spis, ki
> se zavzema za *mehanizacijo univerzuma*, ki ne bi osupnil, ko bi soočili te
> literarne, umetniške, kinematografske "manifesti" s cilji za katere se zdi, da
> jih predlagajo. Poziv k temu, da *postanemo stroj*, k univerzalni
> proletarizaciji, k prisilni produkciji preveč eksplicitno razkriva ideologijo
> Plana, da ne bi vzbudil sumov o svojih realnih namenih.

::: {lang=it}

> /255/Ciò che colpisce, in tale ideologia del consenso incondizionato
> all'universo del capitale, non è tanto la sua letterarietà, quanto piuttosto
> il suo ingenuo radicalismo. Non esiste scritto a favore della *meccanizzazione
> dell'universo* che non lasci sbalorditi qualora si confrontino quei
> "manifesti" letterari, artistici, cinematografici, con i fini che essi
> sembrano proporsi. L'invito a *farsi macchina*, alla proletarizzazione
> universale, alla produzione forzata, rivelano troppo esplicitamente
> l'ideologia del Piano, per non destare sospetti sulle loro reali intenzioni.

:::

:::

::: alts

> "Negativno mišljenje" je svoj projekt preživetja opredelila v zavračanju
> heglovske dialektike, v obnovi posameznih protislovij, ki jih ta odpravlja.
> "Pozitivno mišljenje" ne dela drugega, kot simetrično preobrne to negacijo
> samo vase. Negativno je razkrito kot tako, tudi v svoji "neizbežnosti".
> Odpoved njej je le prvi pogoj, ki dopušča nadaljevanje intelektualnih
> disciplin, obnovo (za ceno uničenja same "avre" intelektualnega dela)
> tradicije njihove "svete" odtujenosti od sveta in ponovno vzpostavitev, čeprav
> minimalnega, razloga za preživetje.

::: {lang=it}

> Il "pensiero negativo" aveva enunciato il proprio progetto di sopravvivenza
> nel rifiuto della dialettica hegeliana, nel recupero delle singole
> contraddizioni da questo eliminate. Il "pensiero positivo" non fa altro,
> rovesciando simmetricamente su se stesso quella negazione. Il negativo è
> scoperto come tale, anche nella sua "ineluttabilità". La rinuncia di fronte a
> esso è solo la condizione prima che permette di perpetuare le discipline
> intellettuali, di recuperare (a costo di dover distruggere la stessa "aura"
> del lavoro intellettuale) la tradizione della sua "sacra" estraneità al mondo,
> di riproporsi una ragione, sia pur minima, di sopravvivenza.

:::

:::

::: alts

> Poraz Razuma je zdaj predpostavljen kot realizacija zgodovinske naloge Razuma
> samega: cinizem intelektualnega dela se razkaže sam ter igra lastne karte na
> dvoumni meji samoironije.

::: {lang=it}

> La disfatta della Ragione è ora assunta come la realizzazione del compito
> storico della Ragione stessa: il cinismo del lavoro intellettuale esibisce se
> stesso, giocando le proprie carte al limite ambiguo dell'autoironia.

:::

:::

::: alts

> Dokazati brez možnosti ugovora, da ni druge poti kot izničenje človeškega
> subjekta v Subjekt razvoja: ta aksiom si prizadeva odrešiti ideologijo kot
> poslednji *kulturni projekt*. "Osvoboditev od vrednote", izginotje Geista iz
> splošnega procesa racionalizacije, nevtralizacija vsakega projekta *etičnega*
> upravičevanja logike sistema so se že zgodile: zdaj se morajo predstaviti s
> silo *danega*, kvečjemu lahko pokažejo svojo *učinkovitost*. Zato je treba
> vsak ostanek "vrednote" napasti z nasiljem oskrunitve. Boj proti *človeku* je
> pogojen s smermi razvoja: samo, če bo slednji naletel na ovire -- zaradi
> viskoznosti "predsodkov", ki delujejo na družbeni ravni -- na poti posplošitve
> svojega gospostva, se bo lahko ponovno vzpostavila mitologija *človeškega*.
> Toda to bo morala biti cinična in regresivna mitologija, ki je funkcionalna
> zgolj za preseganje šibkih in nadležnih uporov[^19].

::: {lang=it}

> Dimostrare senza possibilità di appello che non c'è altra via che annullare il
> soggetto umano nel Soggetto dello sviluppo: questo assioma tende a salvare
> l'ideologia come ultimo *progetto culturale*. La "liberazione dal valore", la
> sparizione del Geist dal processo generale di razionalizzazione, la
> neutralizzazione di /256/ogni progetto di giustificazione *etica* della
> logica del sistema, sono già avvenute: debbono ora porsi con la forza del
> *dato*, possono, al massimo, dimostrare la loro *efficienza*. È per questo che
> ogni residuo di "valore" deve essere attaccato con la violenza della
> dissacrazione. La lotta contro l'*uomo* è condizionata dalle direzioni dello
> sviluppo: solo se quest'ultimo trova ostacoli -- dovuti alla vischiosità dei
> "pregiudizi" agenti a livello sociale -- nel corso della generalizzazione del
> suo dominio, potrà riproporsi una mitologia dell'*umano*. Ma si dovrà trattare
> di una mitologia cinica e regressiva, funzionale solo a far saltare resistenze
> deboli quanto fastidiose[19].

:::

:::

[^19]: Kar lahko zasledimo v pesmimizmu Lévi-Straussa, ki ga popolnoma obvladuje
    Absolut *človeškega*, ki s svojim neujemanjem z realnim proizvaja
    strukturalistično perspektivo nekakšne *rekonstrukcije-za-nič*, nekakšnega
    *izhoda iz sveta*, ki ga izzove "izdaja" sveta samega. "Kakšen pomen ima
    delovanje, če misel, ki vodi delovanje, pripelje do odkritja odsotnosti
    pomena? [...] Svet se je začel brez človeka in so bo končal brez njega.
    Institucije, šege in navade, ki sem jih celo življenje popisoval in poskušal
    razumeti, so le bežen razcvet stvarstva v razmerju do katerega nimajo
    nobenega pomena [...] Kar zadeva stvaritve človeškega duha, pa je njihov
    pomen zgolj v odnosu do človeka in bo zamegljen, ko bo ta izginil" (Claude
    Lévi-Strauss, *Tristi tropici*, Milano 1965^2, str. 402--403). <!-- obstaja
    slo -->

<!--

[^19]: Il che può essere colto nel pessimismo di un Lévy-Strauss, tutto dominato
    da un Assoluto dell'*umano* che, nel suo non coincidere con il reale,
    produce la prospettiva strutturalista di un *ricostruire-per-il nulla*, di
    un'*uscita dal mondo* provocata dal "tradimento" del mondo stesso. "A che
    serve agire se il pensiero che guida l'azione conduce alla scoperta
    dell'assenza di senso? ... Il mondo è cominciato senza l'uomo e finirà senza
    di lui. Le istituzioni, gli usi e i costumi che per tutta la vita ho
    catalogato e cercato di comprendere, sono un'efflorescenza passeggera d'una
    creazione in rapporto alla quale essi non hanno alcun senso ... Quanto alle
    creazioni dello spirito umano, il loro senso non esiste che in rapporto
    all'uomo e si confonderanno nel disordine quando egli sarà scomparso"
    (Claude Lévy-Strauss, *Tristi tropici*, Milano 1965^2, pp. 402--403).

-->

::: alts

> Zgodovina modernega sveta iz stališča delavca je še vsa za napisati. Ne vemo,
> torej, koliko je na spremembe znanosti programiranja in njene politično rabo
> konkretno vplival razredni boj, negativna izkušnja razširjene reprodukcije,
> nasilno zatiranje neznanega delavca -- variablinega kapitala -- v fašističnih
> deželah, ali dvoumni rezultati sovjetskih petletk.

::: {lang=it}

> È ancora tutta da scrivere una storia del mondo moderno dal punto di vista
> operaio. Non sappiamo, quindi, quanto, nei mutamenti della scienza della
> programmazione e nel suo uso politico, abbiano concretamente pesato le lotte
> di classe, la negativa esperienza, ai fini della riproduzione allargata, della
> soppressione violenta dell'incognita operaia -- del capitale variabile -- nei
> paesi fascisti, o gli ambigui risultati dei piani quinquennali sovietici.

:::

:::

::: alts

> Gotovo pa je, da odkritje -- tipično za "napredno" sociologijo 50\. let --
> neizogibnosti konflikta osmisli vsako utopijo, ki temelji na popolni in mirni
> integraciji razreda v procese razvoja. Nasproti optimističnim tezam Talcotta
> Parsonsa, ki jih je realnost delavskega boja natančno zanikala, je priznanje
> *univerzalnosti* konflikta: za Dahrendorfa, ne gre več za zastavljanje
> optimalnih form razvoja brez napetosti, ampak za uresničevanje razvoja *preko*
> napetosti, preko mehanizmov regulacije konfliktov, ko so slednji enkrat
> izolirani znotraj posamezne tovarne, posamezne skupine moči, posamezne
> družbene strate, posamezne institucije. Vendar ne brez formulacije novih
> utopij: predvsem družbene mobilnosti, kot osi okoli katere se vrtijo posamezni
> mehanizmi regulacije in ukrepov[^20].

::: {lang=it}

> È certo però che la scoperta -- tipica della sociologia "avanzata" degli anni
> '50 -- dell'ineliminabilità del conflitto, fa ragione di ogni utopia basata
> sulla totale e pacifica integrazione della classe nei processi di sviluppo.
> Contro le tesi ottimistiche di un Talcott Parsons, puntualmente smentite dalla
> realtà delle lotte operaie, è il riconoscimento della *universalità* del
> conflitto: per Dahrendorf, non si tratta più di prefigurare le forme ottimali
> di uno sviluppo privo di tensioni, ma di realizzare lo sviluppo *attraverso*
> le tensioni, attraverso meccanismi di regolazione dei conflitti, una volta
> isolati questi ultimi all'interno della singola fabbrica, dei gruppi di
> potere, delle stratificazioni /257/sociali, delle singole istituzioni.
> Non senza, però, la formulazione di nuove utopie: prima fra tutte la mobilità
> sociale, come asse intorno a cui far ruotare quei meccanismi di regolazione e
> contenimento[20].

:::

:::

[^20]: Gl. R. Dahrendorf, *Soziale Klassen und Klassenkonflikt*, nav. d., II.
    del, str. 253 in dalje, kjer so "institucionalizacija razrednega konflikta,
    razvoj "industrijske demokracije", organizacija sindikatov, družbena
    mobilnost izrecno navedeni kot instrumenti regulacije razrednega boja. Glej
    tudi Dahrendorfovo kritiko Talcott Parsonsovo *teorijo integracije* (str.
    257 in dalje). Gl. tudi L. A. Coser *Social Conflict and Social Change*, v
    "British Journal of Sociology" 1957; in *The Function of Social Conflict*,
    London 1956, ital. prev. *Le funzioni del conflitto sociale*, Milano 1967\.
    Verjetno ni potrebno poudarjati, da so tako teorije integracije kot te o
    regulaciji konfliktov propadle spričo pomasovljenja in homogenizacije
    delavskega boja od '56 do danes.

<!--

[^20]: Cfr. R. Dahrendorf, *Soziale Klassen und Klassenkonflikt* cit., parte II,
    pp. 253 ss., dove "l'istituzionalizzazione del conflitto di classe", lo
    sviluppo della "democrazia industriale", l'organizzazione dei sindacati, la
    mobilità sociale, sono esplicitamente indicati come gli strumenti di
    regolazione della lotta di classe. Vedi anche la critica di Dahrendorf alla
    *teoria dell'integrazione* di Talcott Parsons (pp. 257 e ss.). Cfr. anche L.
    A. Coser, *Social Conflict and Social Change*, in "British Journal of
    Sociology" 1957; e *The Functions of Social Conflict*, London 1956, trad.
    it. *Le funzioni del conflitto sociale*, Milano 1967\. Non c'è forse bisogno
    di sottolineare che tanto le teorie dell'integrazione che quelle del
    regolamento dei conflitti -- isolati questi ultimi nei singoli settori della
    produzione e della circolazione -- sono crollate di fronte alla
    massificazione e all'omogeneizzazione delle lotte operaie dal '56 a oggi.

-->

::: alts

> Vendar ameriški in evropski teoretiki med 50\. in 60\. ostajajo trdno
> prepričani, da je vsaka možna *uporaba* variabilnega dela svoj specifičen
> instrument nenehnega prestrukturiranja fiksnega kapitala in organske sestave
> kapitala. Znanstvena produkcija vstopi *neposredno* v procese razširjene
> akumulacije, ni več zunanja njim: je njihov pogoj in sredstvo nadzora. Procesi
> avtomatizacije, tehnike znanstvenega napovedovanja prihodnosti, tehnična in
> politična izkušnja programiranja in njegova radikalna prenova so del
> likvidacije teorij *ekvilibrija*. Cikel in krize so ponovno neločljivo
> povezani: osrednjega pomena za to funkcionalno povezavo je intelektualna
> produkcija. Klasično zoperstavljanja abstraktnega in konkretnega dela začne
> izgubljati svojo funkcionalnost v razširjenem obsegu. Kar sledi je, da se same
> tradicionalne forme pridobivanja in upravljanja konsenza radikalno spremenijo.
> Konsenz zdaj ne zadeva več disciplinarne vsebine, temveč ideološke discipline
> same: postavljene v krizo glede na njihovo parcialno funkcijo znotraj tehnik
> znanstvenega napovedovanja prihodnosti razkrijejo svojo realno funkcijo kot
> institucionalna retrogarda sistema.

::: {lang=it}

> Rimane comunque fermo, nei teorici americani ed europei fra il '50 e il '60,
> che ogni possibile *uso* della variabile operaia ha come suo strumento
> specifico la ristrutturazione continua del capitale fisso e della composizione
> organica del capitale. La produzione scientifica entra *direttamente* nei
> processi di accumulazione allargata, non è più esterna ad essi: ne è
> condizione e mezzo di controllo. I processi di automazione, le tecniche di
> previsione scientifica del futuro, l'esperienza delle tecniche e delle
> politiche di programmazione e il loro radicale rinnovamento, fanno tutt'uno
> con la liquidazione delle teorie dell'*equilibrio*. Ciclo e crisi si
> ripresentano indissolubilmente legati: la produzione intellettuale è cardinale
> di tale connessione funzionale. La contrapposizione classica di lavoro
> astratto e lavoro concreto comincia a perdere la sua funzionalità in una
> dimensione allargata. Il che comporta che le stesse forme tradizionali di
> estrazione e gestione del consenso cambino radicalmente. Ora il consenso non
> riguarda più il contenuto disciplinare, ma le discipline ideologiche in sé:
> messe in crisi per quanto riguarda la loro funzione particolare all'interno
> delle tecniche di previsione scientifica del futuro, esse rivelano la loro
> reale funzione in quanto retroguardia istituzionale del sistema.

:::

:::

::: alts

> V tem pogledu ni več pomembno na kakšen način se artikulirajo, kakšna je
> kakovost ali pomen njihovih komunikacij. Bolj pomembno je samo dejstvo, da
> obstajajo, da vztraja rezervna intelektualna armada, ki je popolnoma
> izključena iz vsakega tipa produktivnega procesa in ki privzema nalogo
> reprodukcije institucionalnih form, *katere koli*, ideologije. Namreč že lahko
> vidimo, da nova naloga, ki je zaupana temu sektorju intelektualnega dela -- ki
> je ves usmerjen v fazo priprave in v "osnovno izobrazbo" splošne delovne sile
> -- ni več niti *vzgajanje konsenza*, temveč *izvajanje disenza*: pod pogojem,
> da ta disenz vztraja prav na preizkušenem terenu ideoloških institucij.

::: {lang=it}

> Non è più importante, sotto tale riguardo, in che modo esse si articolino, la
> qualità o il senso delle loro comunicazioni. Vale piuttosto il solo fatto che
> esse sussistano, che permanga un esercito intellettuale di riserva estromesso
> completamente da ogni tipo di processo produttivo, che si assuma il compito di
> /258/riprodurre, *come che sia*, le forme istituzionali dell'ideologia.
> Anzi, si può già intravedere che il compito nuovo affidato a tale settore del
> lavoro intellettuale -- tutto rivolto alla fase di preparazione e "educazione
> generale" di una forza-lavoro generica -- non sia neanche più l'*educazione al
> consenso*, bensì l'*esercitazione al dissenso*: purché tale dissenso insista,
> appunto, sul terreno sperimentato delle istituzioni ideologiche.

:::

:::

::: alts

> Vprašanje "alternativnih vsebin" znanosti in disciplin ali "alternativnih
> načinov" uporabe istih je, še preden postanejo slogan delno propadlih
> sektorjev političnih organizacij delavskega razreda, zahteva obrobij
> avantgarde kapitala.

::: {lang=it}

> Il tema dei "contenuti alternativi" delle scienze e delle discipline, o dei
> "modi alternativi" di uso delle stesse è, prima ancora che uno slogan già in
> partenza fallimentare di un settore delle organizzazioni politiche della
> classe operaia, un'esigenza delle frange di avanguardia del capitale.

:::

:::

::: alts

> Prav tako nam ne more uiti, da so "alternativna kultura", "operativna kritika"
> ali "kritično poučevanje" vsi členi ene in iste verige. Še vedno smo znotraj
> preobrnjene rabe kritike ideologije: znotraj procesa izpopolnjevanja
> instrumentov poseganja, rekompozicije tradicionalnih vsebin, ki se vršijo v
> funkciji afirmacije *realnosti razvoja*.

::: {lang=it}

> Né può sfuggire che "cultura alternativa", "critica operativa", o
> "insegnamento critico", siano tutti anelli di una unica catena. Siamo ancora
> all'interno di un uso rovesciato della critica dell'ideologia: all'interno di
> un processo di affinamento degli strumenti di intervento, di ricomposizione
> dei contenuti tradizionali, compiuta in funzione di un'affermazione della
> *realtà dello sviluppo*.

:::

:::

::: alts

> Ves razvoj je sam po sebi radikalno samopreseganje, nenehno revolucioniranje
> *sebe*. Dialektika in tradicija, ki sta dosegli pomembno visoko raven samega
> razvoja, se morata ne le uravnovešate, temveč popolnoma združiti. Če v
> realnosti materialnih procesov *negativnega* -- "upor" razreda -- ni mogoče
> vpiti, ampak samo funkcionalizirati (to izrecno poudarjajo ne le
> keynesijanske, temveč tudi postkeynesijanske teorije), je na ideološki ravni
> funkcionalizacija disenza uresničljiva že z razmejitvijo območja za izvajanje
> negacije. V ta namen ni nič primernejšega kot, da kritično vajo zmrznemo
> znotraj interdisciplinarnih polj.

::: {lang=it}

> Tutto lo sviluppo è di per sé autosuperamento radicale, rivoluzionamento
> continuo *in se stesso*. Dialettica e tradizione, giunte a livelli
> notevolmente elevati dello sviluppo stesso, debbono non solo compensarsi, ma
> unificarsi totalmente. Se nella realtà dei processi materiali il *negativo* --
> la "resistenza" della classe -- non può essere assorbito, ma solo
> funzionalizzato (non solo le teorie keynesiane ma anche quelle post keynesiane
> sono esplicite al proposito), al livello ideologico la funzionalizzazione del
> dissenso è realizzabile già delimitando un'area per l'esercizio della
> negazione. A tal fine non v'è nulla di più adeguato che congelare l'esercizio
> critico all'interno dei campi disciplinari.

:::

:::

::: alts

> Ni rečeno, da to operacijo izvrši prav kapital sam. Izjemno funkcionalno v ta
> namen je izkoriščanje sil radikalne ali paramarksistične "levice", vsekakor pa
> tradicij, ki so marksizem reducirali na ideologijo. Razmnoževanje
> samopredlaganih poskusov, še posebej v zadnjih desetih letih, oblik
> "revolucionarnega filma", "protestniške umetnosti", "alternativnega
> urbanističnega upravljanja" ali "antiinstitucionalne sociologije" ni slučaj.
> Vse prelahko se je pred takšnimi patetičnimi intelektualnimi iluzijami (ali, v
> najslabšem primeru, političnem oportunizmu) nasmehniti: toda nedvomno je
> izjemno jasna *razrodaja ideologije*, ki se, v posebej napredni fazi
> kapitalističnega razvoja, izvaja z namenom disciplinarne preureditve in v
> funkciji korenitega revolucioniranja tehnik plana.

::: {lang=it}

> Né è detto che tale operazione debba essere fatta in proprio dal capitale.
> Estremamente più funzionale a tal fine è l'utilizzazione delle forze della
> "sinistra" radicale o paramarxista, e comunque della tradizione che ha ridotto
> il marxismo a ideologia. Non è casuale il proliferare dei tentativi di
> autoproposizione, negli ultimi dieci anni in particolare, di forme di "cinema
> rivoluzionario", di "arte di contestazione", di "gestione urbanistica
> alternativa", di "sociologia antiistituzionale". È sin troppo facile sorridere
> di fronte a tali patetiche illusioni /259/intellettuali (o di
> opportunismo politico, nei casi peggiori): ma è indubbio che esse evidenziano
> clamorosamente la *svendita dell'ideologia* che, in una fase particolarmente
> avanzata dello sviluppo capitalista, viene compiuta in vista di un
> riassestamento disciplinare, e in funzione di un decisivo rivoluzionamento
> delle tecniche di piano.

:::

:::

::: alts

> V samem trenutku, ko je racionalizacija povzdignjena v moment dinamičnega
> razvoja celotnega kapitala, se postavlja potreba po drugačni uporabi
> discipline, ki predseduje razvoju in razdelavi metod uvrednotenja in
> programiranja.

::: {lang=it}

> Nel momento stesso in cui la razionalizzazione è elevata a momento dello
> sviluppo dinamico del capitale complessivo, si pone l'esigenza di una diversa
> utilizzazione delle discipline che presiedono al controllo e all'elaborazione
> dei metodi di valorizzazione e di programmazione.

:::

:::

::: alts

> To kar se zdaj predstavlja kot subjekt razvoja je globalnost plana. Vsi
> posamezni momenti njega, uporabljeni v prvem momentu za začetek delnega
> nadzora -- od analize strukture ologopolistične trgovine, do razdelave
> anticikličnih teorij, do tehnik planskega poseganja v distribucijo in
> konsumpcijo, končno do samih tehnik racionalizacije posameznih produkcijskih
> sektorjev -- morajo biti na novo preučeni, podvrženi kritiki, umeščeni v nov
> kontekst.

::: {lang=it}

> Ciò che si presenta ora come soggetto dello sviluppo è la globalità del Piano.
> Tutti i momenti particolari di esso, utili in un primo momento per avviare
> processi di controllo parziale -- dalle analisi sulla struttura del mercato
> oligopolistico, all'elaborazione delle teorie anticicliche, alle tecniche di
> intervento pianificato sul momento sociale della distribuzione e del consumo,
> fino alle stesse tecniche di razionalizzazione di singoli settori produttivi
> -- debbono essere di nuovo vagliati, sottoposti a critica, inseriti in un
> nuovo contesto.

:::

:::

::: alts

> Na kocki je, iz kapitalističnega stališča, globalen nadzor tehnik znanstvenega
> napovedovanja prihodnosti: samo dinamično pojmovanje razvoja zahteva takšen
> metodološki skok.

::: {lang=it}

> È in gioco, dal punto di vista capitalistico, il controllo globale delle
> tecniche di previsione scientifica del futuro: la stessa concezione dinamica
> dello sviluppo richiede tale salto metodologico.

:::

:::

::: alts

> V tem kontekstu postaja očitna vse večja pomembnost pripisana "kritičnemu"
> momentu. Ne gre več za tradicionalno funkcionalizacijo kritike, ki je v
> meščanski misli od vedno privzemala nalogo, da razdelave in discipline
> pripelje do najvišje dovoljene ravni. Z drugimi besedami, ne gre več za
> enostavno "pozitivno" rabo kritike, za njeno takojšnjo podreditev
> odstranjevanju ovir, ki jih razvoj srečuje na svoji poti. Disciplinarno
> prilagajanje smerem razvoja, nenehna in neprestana prenova disciplin in
> institucij je do nedavnega bila naloga kritike, ki je sama bila povzdignjena v
> status posebne "discipline". Zdaj je za spremljanje in napovedovanje
> dinamičnega planiranja razvoja nujno potrebno iz kritike, ki se izvaja *nad*
> disciplino in *nad* posameznimi tehnikami, preiti h kritiki *samih* disciplin.

::: {lang=it}

> In questo quadro si fa evidente l'importanza crescente attribuita al momento
> "critico". Né si tratta più della tradizionale funzionalizzazione della
> critica, che, da sempre nel pensiero borghese, ha assunto il compito di
> spingere elaborazioni e discipline ad attestarsi al livello più alto loro
> consentito. Non si tratta più, in altre parole, di un semplice uso "positivo"
> della critica, dell'asservimento immediato della critica a rimuovere gli
> ostacoli che lo sviluppo incontra sul suo cammino. L'adeguamento disciplinare
> alle direzioni dello sviluppo, il rinnovamento continuo, incessante, di
> discipline e istituzioni, è stato fino a qualche tempo fa il compito di una
> critica elevata essa stessa a "disciplina" specifica. Ora è necessario, per
> seguire e anticipare la pianificazione dinamica dello sviluppo, passare da una
> critica che si esercita *sulla* disciplina e *sulle* tecniche particolari, a
> una critica *delle* discipline.

:::

:::

::: alts

> Ni slučaj, da je med prvimi instrumenti, ki so bili podvrženi tej novi vrsti
> kritike, tehnika *interdisciplinarnosti* (tehnika, ki je bila celo
> povzdignjena v mit tekom razprave o tehnikah programiranja tekom 60\. let).

::: {lang=it}

> Non è casuale che fra i primi strumenti a essere sottoposti /260/a tale
> nuovo tipo di critica sia la tecnica dell'*interdisciplinarietà* (una tecnica
> elevata addirittura a mito, nel corso del dibattito sulle tecniche di
> programmazione nel corso degli anni '60).

:::

:::

::: alts

> Interdisciplinarna metoda predpostavlja učinkovitost posameznih področij
> planskih intervencij -- ekonomsko, sociološko, teritorialno --, ki jih je
> mogoče zlagati skozi začasno prekinitev ekskluzivnosti disciplin. Ona sama je
> plod prvotne *kritike disciplin*: in sicer kritike, ki je izpodbijala
> totalizirajoče namere posameznih intervencijskih tehnik. (Dovolj je pomisliti
> na resno razpravo, ki se je odvijala v Italiji in Franciji okoli načinov
> programiranja ekonomsko-formalne ureditve teritorija, ali na polemike
> radikalne sociologije ZDA proti sektorski naravi intervencij na urbani ravni).
> Poleg tega je treba dodati, da je sektorsko programiranje -- in prav zaradi
> zaprtega kroga, ki se sklene med tehniko intervencije in ciljem ponovnega
> uravnoteženja same intervencije -- v temelju zavezano *statičnim* modelom, ki
> sledijo logiki, ki je osredotočena na odpravo "neravnovesij".

::: {lang=it}

> Il metodo interdisciplinare presuppone l'efficacia dei singoli campi di
> intervento pianificato -- economico, sociologico, territoriale -- componibili
> attraverso la rottura provvisoria dell'esclusività delle discipline. Esso
> stesso è il frutto di un'iniziale *critica delle discipline*: ed esattamente
> di una critica che ha contestato le pretese totalizzanti delle singole
> tecniche di intervento. (Basterebbe pensare al serrato dibattito svoltosi in
> Italia e in Francia intorno ai modi di programmazione dell'assetto
> economico-formale del territorio, o alle polemiche della sociologia radicale
> statunitense contro la settorialità degli interventi al livello urbano). Al
> che va aggiunto che la programmazione settoriale -- e proprio per il circolo
> chiuso che in essa viene a formarsi fra tecnica di intervento e il fine
> riequilibratore dell'intervento stesso -- è obbligata ad agire in base a
> modelli *statici*, seguendo una logica accentrata sulla rimozione degli
> "squilibri".

:::

:::

::: alts

> Prehod od uporabe statičnih modelov do razdelave dinamičnih je bil prvi korak
> kapitalistične posodobitve uporabe tehnik plana: preseganje nadaljnjih faz, da
> bi dosegli popolno razpoložljivost pri realizaciji celostnega upravljanja s
> planskim razvojem, je cilj, ki ga v tem trenutku zasleduje znanost Plana.

::: {lang=it}

> Passare dall'utilizzazione di modelli statici all'elaborazione di modelli
> dinamici è stato il primo passo dell'aggiornamento capitalistico nell'uso
> delle tecniche di piano: superare ulteriormente tale fase, per raggiungere una
> piena disponibilità nella realizzazione di una gestione complessiva dello
> sviluppo pianificato, è l'obbiettivo che in questo momento la scienza del
> Piano va proseguendo.

:::

:::

::: alts

> Plan se, od enostavnega "momenta" razvoja, uveljavlja (se je uveljavil) kot
> nova politična institucija[^21]. V tem smislu je treba radikalno preseči
> izključno in preprosto interdisciplinarno -- ki je neuspešna tudi na praktični
> ravni -- izmenjavo.

::: {lang=it}

> Da semplice "momento" dello sviluppo, il Piano si va imponendo (si è imposto)
> come nuova istituzione politica[21]. È /261/in questo senso che il puro
> e semplice scambio interdisciplinare -- fallito anche a livello pratico -- va
> radicalmente superato.

:::

:::

[^21]: Nedavna zahteva Saracena, da bi presegli programe programe, ki jih je sam
    opredelil *kot objektivne*, v prid programiranega delovanja *občega* tipa,
    je že del vizije plana, ki odpravlja vso shematičnost in sektorsko naravo
    teorij plana, ki so bile razdelane v 50\. in 60\. letih. "Če je
    programiranje splošnega značaja", piše Saraceno, "je njegov osnovni cilj,
    povsem drugačen [od obsežnih projektov, ki pokrivajo določene sektorje
    javnega delovanja], sestaviti v sistem vse ukrepe, ki jih je treba izvesti
    na javnem področju; tako postane procedura, kjer se primerjajo stroški
    različnih predlogov Vladnega delovanja, kot tudi celota teh stroškov s
    celoto razpoložljivih sredstev tako, da se omogoči izbira načina izvajanja
    ukrepov in kriterijev za zbiranje sredstev. S sprejetjem podobne procedure,
    bi bilo bolj primerno govoriti o programirani *družbi* in ne o programirani
    *ekonomiji*" (Pasquale Saraceno, *La programmazione negli ani '70*, Milano
    1970, str. 28). Treba je pripomniti, da *obči program* o katerem govori
    Saraceno ne predstavlja splošne dejavnosti: njegova "edina naloga je, da od
    časa do časa -- v presledkih, ki verjetno ne presegajo enega leta -- sporoči
    stanje sistema" (prav tam, str. 32). V zvezi s tem je pomembna zahteva po
    novih institucijah, ki so sposobne realizirati koordinacijo: pozitivno
    vrednotenje metode, ki se jo zasleduje v formulaciji *Progetto 80*, potrjuje
    ugotovljen trend. "Kaj je dejansko *Progetto 80*?" se sprašuje Saraceno
    (str. 52): "Gre za sistematični pregled nacionalnih problemov, ki so *v tem
    trenutku* ocenjeni za najpomembnejše, kot tudi novih institucij, ki naj bi
    bile sposobne, bolje od obstoječih, začeti iskati rešitve za te probleme; če
    bi naša javna sfera že bila urejena v prej razjasnjen *sistem*, bi
    pripravljavci tega dokumenta lahko izdelali tako imenovani
    *Program-verifikacija*". Čeprav tudi Saracenove tehnične perspektive niso
    brez utopičnih ostankov -- glej njegov poziv k "redu, v skladu s katerim se
    družbene sile *moralno* držijo procesa uporabe virov zahtevanih za reševanje
    problemov" (prav tam, str. 26, poudarek naš) -- se njegova kritika
    petletnega programa '66--'70 drži institucionalnega preoblikovanja nadzora
    razvoja, ki je pravilno prepoznan v zapisu Sandra Mattiuzzija in Stefanie
    Potenza, *Programmazione e piani territoriali: l'esempio del Mezzogiorno*, v
    "Contropiano", 1969, str. 685--717\. Da so Saracenova stališča del obsežnega
    prestrukturiranja, ki poteka znotraj praks in teorije programiranja,
    dokazuje cela vrsta stališč, ki *Plan kot institucijo* podpirajo kot "stalno
    in v celoti izvajano politiko". Gl. Giorgio Ruffolo, *Progetto 80: scelte,
    impegni, strumenti*, v "Mondo economico" 1 1969.

<!--

[^21]: L'istanza recentemente avanzata da Saraceno di superare i programmi da
    lui definiti *ad obbiettivo*, a favore di un'azione programmata di tipo
    *generale*, rientra già in una visione del piano che fa ragione di tutti gli
    schematismi e della settorialità delle teorie del piano elaborate negli anni
    '50--'60\. "Se la programmazione -- scrive Saraceno -- è di carattere
    *generale*, essa ha in sostanza lo scopo, del tutto diverso [rispetto ai
    progetti di vasta portata che coprono determinati settori dell'azione
    pubblica], di comporre in un sistema tutte le azioni da intraprendere nella
    sfera pubblica; essa diviene così una *procedura* mediante la quale si
    confrontano costi delle varie azioni proposte all'attività di Governo,
    nonché il complesso di tali costi con il complesso delle risorse
    prevedibili, così da rendere possibile la scelta della modalità delle azioni
    da svolgere e dei criteri con cui raccogliere dette risorse. Con l'adozione
    di una simili procedura, sarebbe più appropriato parlare di una *società*
    programmata e non di una *economia* programmata" (Pasquale /261/Saraceno,
    *La programmazione negli anni '70*, Milano 1970, p. 28). Va notato che il
    *programma generale* di cui parla Saraceno non costituisce affatto
    un'attività generale: esso ha "il solo ufficio di rendere noto di tempo in
    tempo -- probabilmente a intervalli non superiori ad un anno -- lo stato del
    sistema" (ibidem, p. 32). È significativa al proposito la richiesta di nuove
    istituzioni capaci di realizzare il coordinamento: la valutazione positiva
    del metodo seguito nella formulazione del *Progetto 80* conferma la linea di
    tendenza rilevata. "Che cosa è infatti il *Progetto 80*? -- si chiede
    Saraceno (p. 52) -- È una rassegna sistematica dei problemi nazionali che in
    *questo momento* sono giudicati di massimo rilievo, nonché dei nuovi
    istituti che meglio di quelli esistenti si ritiene possano avviare a
    soluzione quei problemi; se la nostra sfera pubblica fosse già ordinata in
    un *sistema* nel senso prima chiarito, i redattori di quel documento
    avrebbero prodotto quello che si è denominato un *Programma-verifica*".
    Malgrado che anche le prospettive tecniche di Saraceno non siano prive di
    residui utopici -- si veda la sua perorazione per "un ordinamento in virtù
    del quale le forze sociali aderiscono *moralmente* al processo di utilizzo
    di risorse richieste per la soluzione [dei] problemi" (ibidem, p. 26 corsivo
    nostro) -- la sua critica al programma quinquennale '66--'70 aderisce alla
    trasformazione istituzionale del controllo dello sviluppo, correttamente
    individuata nella nota di Sandro Mattiuzzi e Stefania Potenza,
    *Programmazione e piani territoriali: l'esempio del Mezzogiorno*, in
    "Contropiano", 1969, pp. 685--717\. Che le opinioni del Saraceno siano parte
    di una vasta ristrutturazione in atto nella prassi e nella teoria della
    programmazione, è provato da tutta una serie di prese di posizione a favore
    del *Piano come istituzione* come "politica continuamente e completamente
    esercitata". Cfr. Giorgio Ruffolo, *Progetto 80: scelte, impegni,
    strumenti*, in "Mondo economico" 1 1969\.

-->

::: alts

> Horst Rittel je z jasnostjo izpostavil posledice "teorije odločanja", ko se ta
> enkrat umesti v samoprogramiranje kibernetskih sistemov (in logično je, da
> jemljemo za samoumevno, da ta raven racionalizacije v dobršnem delu še vedno
> predstavlja tendenčni model).

::: {lang=it}

> Horst Rittel ha esposto con chiarezza le implicazioni della "teoria delle
> decisioni", una volta che questa sia inserita nell'autoprogrammazione dei
> sistemi cibernetici (ed è logico che diamo per scontato che tale livello di
> razionalizzazione rappresenti ancora, in buona parte, un modello tendenziale).

:::

:::

::: alts

> Rittel piše[^22], da "vrednostnih sistemov ni mogoče več šteti za dolgoročno
> stabilne. To, kar si lahko želimo, je odvisno od tega kaj lahko omogočimo in
> to kar lahko omogočimo je odvisno od tega kaj si želimo. Cilji in funkcije
> koristnosti niso neodvisne spremenljivke. Z odločevalsko sfero so v
> medsebojnem odnosu. Reprezentacije vrednosti je mogoče nadzorovati znotraj
> širokih meja. Absurdno je želeti oblikovati toge modele odločanja pred
> negotovostjo alternativnih prihodnjih razvojev". Teorija odločanja mora
> zagotavljati fleksibilnost "sistemov, ki sprejemajo odločitve": in jasno je,
> da v igri ni več vrednostnih kriterijev, ki bi se lahko primerjali s temi od
> frankfurtske sociološke šole. Vprašanje na katerega mora odgovoriti napredna
> raven programiranja je: "kateri vrednostni sistemi so obče skladni in
> zagotavljajo možnost prilagajanja in s tem preživetja"?[^23]

::: {lang=it}

> I sistemi di valori -- scrive Rittel[22] -- non possono essere più considerati
> stabili per lunghi periodi. Ciò che si può volere dipende da quel che può
> essere reso possibile, e ciò che deve essere reso possibile dipende da quel
> che si vuole. Fini e funzioni di utilità non sono grandezze indipendenti. Esse
> stanno in rapporto d'implicazione con l'ambito decisionale. Rappresentazioni
> di valore sono controllabili entro ampi limiti. Di fronte all'incertezza degli
> sviluppi alternativi futuri è assurdo voler costruire modelli decisionali
> rigidi, che forniscano strategie per /262/lunghi periodi". La teoria
> delle decisioni deve assicurare la flessibilità dei "sistemi che prendono
> decisioni": ed è chiaro che qui non è più in gioco un criterio di valore
> confrontabile con quelli della scuola sociologica di Francoforte. La domanda
> cui un livello avanzato di programmazione deve rispondere è: "quali sistemi di
> valori sono in generale coerenti e garantiscono possibilità di adattamento e
> quindi di sopravvivenza"?[23]

:::

:::

[^22]: Horst Rittel, [*Überlegungen zur wissenschaftlichen und politischen
    Bedeutung der Entscheidungstheorien*]{lang=de}, relazione dello
    Studiengruppe für Systemforschung, Heidelberg, str. 22 ss., zdaj v zborniku
    v uredništvu H. Krauch, W. Kunz, H. Rittel, [Forschungsplanung]{lang=de},
    München, Oldenbourg Verlag, 1966, pp. 110--129.

[^23]: Prav tam.

::: alts

> Zato je, nujno "odprta", struktura plana tista, ki ustvarja lastne sisteme
> vrednotenja: nič več ne ostane zunaj plana. Spričo tega ves hegelianizem
> "kritičnih teorij družbe" privzame zgodovinsko nalogo potrditi, z preverjanjem
> lastne objektivne impotence, izumrtje vsakega nasprotovanje med "vrednoto" in
> planom.

::: {lang=it}

> È quindi la struttura, necessariamente "aperta", del piano, che genera i
> propri sistemi di valutazione: nulla rimane più all'esterno del piano. Di
> fronte ad esso, tutto l'hegelismo delle "teorie critiche della società" si
> assume il compito storico di sancire, con la verifica della propria oggettiva
> impotenza, l'estinzione di ogni contrapposizione fra "valore" e piano.

:::

:::

> Obuditev Weber-Scheler-Mannheimove struje s strani ameriških teoretikov "konca
> ideologije" je vse prej kot naključna. Če je Lippmann še lahko predstavljal
> nujno potrebnost deideologizacije političnega življenja kot element
> antikonfliktne stabilizacije, Bell, Lane ali Lipset svojo kritiko znova
> usmerijo na področje teme razvoja[^24]. Primat racionalizacije, ki temelji na
> izključno pragmatični osnovi, ki ga Bell pripisuje *New Dealu*, ali Lipsetovo
> enačenje demokracije z "dobro družbo v delovanju", priznavata, da je poskus
> odpraviti vsako vrzel med utopijo in razvojnimi procesi realen[^25].

::: {lang=it}

> È tutt'altro che casuale la ripresa del filone Weber-Scheler-Mannheim da parte
> dei teorici americani della "fine delle ideologie". Se il Lippmann poteva
> ancora presentare la necessità di una de-ideologizzazione della vita politica
> come elemento di stabilizzazione anticonflittuale, il Bell, il Lane o il
> Lipset riconducono la loro critica nell'ambito della tematica dello
> sviluppo[24]. Il primato di una razionalizzazione fondata su basi
> esclusivamente pragmatiche, letto dal Bell nel *New Deal*, o
> l'iden/263/tificazione fatta dal Lipset della democrazia con la "buona
> società in azione", riconoscono come reale il tentativo di eliminare ogni
> scarto fra utopia e processi di sviluppo[25].

:::

[^24]: Gl. Daniel Bell, *The End of Ideology*, Glencee, Ill., 1960; Robert E.
    Lane, *Political Ideology. Why the American Common Man Believes What He
    Does*, London-New York 1962; Seymour Martin Lipset, *Political Man*, New
    York 1960, ital. prev. *L'uomo e la politica*, Milano 1963\. Toda glej tudi
    J. Fartganis, *American Politics and the End of the Ideology*, v *The New
    Sociology. Essays in Social Science and Social Theory, in Honor of C. Wright
    Mills*, New York 1964, str. 268 in dalje. Opozoriti je treba, da Bell teze o
    "koncu ideologije" podpira kot perspektivo, ki teži realizirati dobo
    "znanstvene politike": namreč, za avtorja nenehno zakrivanje konkretnih
    zahtev pod ideološko tančico (kar se posledično izraža v nevarnih čustvenih
    nabojih) vodi -- kot v zgodnjih letih New Deala -- v zaostrovanje
    konfliktov, ki so vse prej kot neizogibni. Bellova intelektualistična
    utopija -- ki sta jo povzela Lane in Lipset -- se tako izide v tehnokratski
    mit in iluzijo popolne družbe konsenza, ki je utemeljena na znanstvenosti
    delovanja in odločanja ter na posvečenju Welfare State s strani
    intelektualcev. V tej tezi je gospostvo racionalizacije tako projicirano v
    novo industrijsko utopijo. Gl. tudi Sergio Cotta, *Sulla fine
    dell'ideologia*, v *Ideologia e filosofia*, Brescia 1967, in odgovor Wright
    Mills, ki tezam o smrti ideologije očita, da so: "surtout l'idéologie d'une
    mort: celle de la reflexion politique en tant que facteur public" (C. Wright
    Mills, *Mort des idéologie, Lettre à la nouvelle gauche*, in "Les lettres
    nouvelles" 1961).

    Toda gl. tudi Jean Meynaud, *Destino delle ideologie*, Bologna, Cappellii,
    1964.

<!--

[^24]: Cfr. Daniel Bell, *The End of Ideology*, Glencee, Ill., 1960; Robert E.
    Lane, *Political Ideology. Why the American Common Man Believes What He
    Does*, London-New York 1962; Seymour Martin Lipset, *Political Man*, New
    York 1960, trad. it. *L'uomo e la politica*, Milano 1963\. Ma si veda anche
    J. Fartganis, *American Politics and the End of the Ideology*, in *The New
    Sociology. Essays in Social Science and Social Theory, in Honor of C. Wright
    Mills*, New York 1964, pp. 268 ss. Si noti che la tesi della "fine delle
    ideologie" è sostenuta dal Bell come sostegno a una prospettiva tesa a
    realizzare l'era di una "politica scientifica": per l'autore, infatti, il
    continuo coprire istanze concrete sotto velami ideologici (a loro volta
    risolte in pericolose cariche emozionali) comporta -- come nei primi anni
    del New Deal -- l'acutizzarsi di conflitti tutt'altro che inevitabili.
    L'utopia intellettualista del Bell -- ripresa dal Lane e dal Lipset --
    sfocia quindi nel mito tecnocratico e nell'illusione di una perfetta società
    del consenso fondata sulla scientificità dell'azione e delle decisioni, e
    sulla consacrazione del Welfare State da parte degli intellettuali. Il
    dominio della razionalizzazione è quindi proiettato, in questa tesi, in una
    nuova utopia industriale. Cfr. anche Sergio Cotta, *Sulla fine
    dell'ideologia*, in *Ideologia e filosofia*, Brescia 1967, e la risposta di
    Wright Mills, che accusa la tesi della morte delle ideologie di essere
    "surtout l'idéologie d'une mort: celle de la reflexion politique en tant que
    facteur public" (C. Wright Mills, *Mort des idéologie, Lettre à la nouvelle
    gauche*, in "Les lettres nouvelles" 1961).

    Ma cfr. anche Jean Meynaud, *Destino delle ideologie*, Bologna, Cappellii,
    1964.

-->

[^25]: Gl. S. M. Lipset, *Political Man* nav. d. Zaostalost teh socioloških
    teorij je mogoče meriti z njihovim neupoštevanjem učinkovitosti konflikta
    kot dinamičnega dejavnika razvoja. Vso Lipsetovo delo temelji na slavljenju
    realizirane utopije: *dobra družba v delovanju* je dana kot vse navzoča,
    tako kot je tudi družbeno-politično ravnovesje dano kot realizirano. Samo
    ideologija, torej, vnaša fiktivne prelome: z njeno odpravo bo sistem postal
    popoln.

<!--

[^25]: Cfr. S. M. Lipset, *Political Man* cit. L'arretratezza di tali teorie
    sociologiche è misurabile nel loro non tener conto dell'effettualità del
    conflitto come fattore dinamico dello sviluppo. Tutto il lavoro del Lipset
    si fonda sulla celebrazione dell'utopia realizzata: *la buona società in
    azione* è data come tutta presente, così come è dato per realizzato
    l'equilibrio sociopolitico. Solo *l'ideologia*, dunque, introduce fittizie
    fratture: con la sua eliminazione il sistema sarà reso perfetto.

-->

::: alts

> Habermas je prepoznal, da ima kritika ideologije "reducirana v pozitivističnem
> smislu" nalogo razkriti "vsako teorijo, ki se na prakso nanaša drugače kot na
> potenciranje in izpopolnjevanje tokov tehničnega delovanja"[^26] kot
> dogmatično. In vendar je povedno, da takoj zatem ne razbere funkcionalnosti
> tovrstne kritika za razvojne procese. "Priprava racionalnega vedenja skozi
> kritiko ideologije racionalnost priporoča kot odlično, če ne celo izključno,
> sredstvo za realizacijo vrednot, saj zagotavlja 'učinkovitost' ali
> 'ekonomičnost' ravnanja. Oba izraza razkrivata, da je spoznavni interes
> empiričnih znanosti tehnični interes. Iz njih je razvidno, da je
> racionalizacija že od samega začetka umeščena znotraj mej sistema družbenega
> dela in da označuje prav moč razpolaganja z objektivnimi in objektiviziranimi
> procesi. Določil te racionalnosti zatorej po svoje ni mogoče pojmovati kot
> vrednote in jih je mogoče upravičiti le znotraj pozitivističnega
> samorazumevanja kot, da bi bile vrednote. Kritika ideologije katere edini cilj
> je afirmacija tehnične racionalnosti se ne izogne tej dilemi: racionalnost
> želi kot vrednoto, ker ima ta v primerjavi z ostalimi vrednotami to prednost,
> da je implicitna v racionalnih postopkih samih"[^27].

::: {lang=it}

> Habermas ha riconosciuto come la critica dell'ideologia "ridotta in senso
> positivistico" abbia il compito di rivelare come dogmatica "ogni teoria, che
> sia riferita alla prassi in modo diverso da quello del potenziamento e
> perfezionamento di raggi d'azione tecnici"[26]. È però indicativo che subito
> dopo egli non legga la funzionalità di tale tipo di critica ai processi di
> sviluppo. "La preparazione del comportamento razionale tramite la critica
> dell'ideologia raccomanda la razionalità come mezzo eccellente, se non
> addirittura esclusivo, per realizzare i valori, perché garantisce 'efficienza'
> o 'economia' del procedere. Le due espressioni rivelano che l'interesse
> cognitivo delle scienze empiriche è un interesse tecnico. Esse fanno capire
> che la razionalizzazione è fin dall'inizio collocata entro i confini del
> sistema del lavoro sociale, e denota appunto il poter disporre su processi
> oggettivi e oggettivizzati. Le determinazioni di questa razionalità, non
> possono perciò essere concepite a loro volta come valori, e possono essere
> giustificate solo all'interno dell'autocomprensione positivistica come fossero
> valori. Una critica dell'ideologia, il cui unico fine sia l'affermazione della
> razionalità tecnica, non sfugge a questo dilemma: essa vuole la razionalità
> come valore, perché essa ha, rispetto agli altri valori, il vantaggio di
> essere implicita negli stessi procedimenti razionali"[27].

:::

:::

[^26]: Jűrgen Habermas, *Theorie und Praxis*, Neuwied, Luchterhand, 1963, zdaj v
    *Teoria e prassi nella società tecnologica*, Bari 1969, str. 87.

[^27]: J. Habermas, nav. d., str. 92\. "Zatorej v kritiki ideologije," nadaljuje
    Habermas (str. 63), "ki vse to realizira najmanj potihoma, živi delček
    angažiranega razuma v protislovju s kriteriji na podlagi katerih kritizira
    dogmatizem". Cilj takšnega angažiranega razuma naj bi torej bila
    organizacija družbe v kateri "v imenu evolutivnosti, hipostazirana
    tehnologija celo narekuje, področjem, ki jih je uzurpirala praksam, natančno
    svoj sistem vrednot" (prav tam). V zvezi s predstavljenimi alternativami se
    Horkheimerjeva in Adornova tema oživi v še bolj mistificirani različici:
    "racionalizacije zgodovine zatorej se ne more spodbujati s povečano močjo
    nadzora manipuliranih ljudi, temveč zgolj z naprednejšo ravnjo refleksije, z
    zavestjo človeških agentov, ki napredujejo k emancipaciji" (prav tam, str.
    98).

<!--

[^27]: J. Habermas, op. cit., p. 92\. "Perciò -- continua Habermas (p. 63) --
    nella critica dell'ideologia, che realizza tutto ciò almeno tacitamente,
    vive una particella di ragione impegnata, in contraddizione con i criteri in
    base ai quali essa critica il dogmatismo". Il fine di tale ragione impegnata
    sarebbe quindi un'organizzazione della società in cui, "in nome
    dell'evolutività, una tecnologia ipostatizzata detta perfino il sistema dei
    valori, e precisamente il proprio, agli ambiti usurpati della prassi"
    (ibidem). Il tema di Horkheimer e Adorno rivive in una versione ancor più
    mistificante rispetto alle alternative avanzate: "una razionalizzazione
    della storia perciò non può essere promossa /264/da un potere di controllo
    ampliato di uomini manipolanti, bensì soltanto grazie a un livello più
    avanzato di riflessione, una coscienza di uomini agenti progrediente
    nell'emancipazione" (ibidem, p. 98).

-->

::: alts

> Vsa kritika, ki so jo naslovili Habermas in zadnji epigoni Frankfurtske šole,
> onemogoča analizo vprašanja vrednot iz stališča, ki se izmika njihovi
> "družbenosti". Kritika se še enkrat izvaja na procesih racionalizacije kot
> takih, ki se berejo v njihovi nadzgodovinski univerzalnosti in so, kar je
> najpomembnejše, prikrajšani vsake razredne konotacije. Habermas omejitev
> vrednote na čisti znak same sebe, njeno izginotje znotraj sektaške narave
> tehnik odločanja, teorij iger kot instrumentov nadzora antagonističnih gibanj,
> sistem avtomatizacije pri določanju programov kritizira v imenu *utopije
> emancipacije*, ki analizo vrača nazaj na zaostalo raven Marcuseja[^28]. Res je
> tudi, da sta celo pri Popperju plan in vrednota predstavljena kot ločena,
> vendar oba nujno potrebna. Racionalnost plana sama po sebi ni vprašljiva:
> postavljena je kot čista *nujnost*. Slepa vera v racionalnost, po drugi
> strani, je predstavljena kot kot "odločitev", prav tako nujno potrebna, za
> nadzor družbenega vedenja spričo plana[^29]. Procesi komunikacije so tisti, ki
> jih je treba znanstveno razsvetliti: cilj takšne operacije "formacije"
> politične volje je izrecno opredeljen v družbeno-tehnični razgradnji oblik
> gospostva. 

::: {lang=it}

> /264/Tutta la critica rivolta da Habermas e dagli ultimi epigoni della
> scuola di Francoforte, si impedisce di analizzare la questione dei valori da
> un punto di vista che prescinda dalla loro "socialità". La critica si esercita
> ancora una volta sui processi di razionalizzazione in quanto tali, letti nella
> loro universalità metastorica, e privati, ciò che più conta, di ogni
> connotazione di classe. La riduzione del valore a puro segnale di se stesso,
> la sua scomparsa all'interno della settarietà delle tecniche decisionali,
> delle teorie dei giochi come strumenti di controllo dei movimenti antagonisti,
> dei sistemi di automazione nella determinazione dei programmi, sono criticate
> da Habermas in nome di una *utopia dell'emancipazione*, che riporta l'analisi
> ai livelli arretrati di un Marcuse[28]. È ben vero, peraltro, che persino in
> Popper piano e valore si presentano scissi ma entrambi necessari. La
> razionalità del piano non è in discussione in se stessa: essa è posta come
> pura *necessità*. L'atto di fede nella razionalità, invece, è presentato come
> "decisione", altrettanto necessaria, per il controllo del comportamento
> sociale di fronte al piano[29]. Sono i processi di comunicazione che vanno
> illuminati scientificamente: il fine di tale operazione di "formazione" della
> volontà politica è esplicitamente individuato nella dissoluzione sociotecnica
> delle forme del dominio.

:::

:::

[^28]: Utopija emancipacije objektivno igra lastno, povsem obrobno, vlogo pri
    spodbujanju nekaterih družbenih rešitev: pri odpravi nekaterih posebej
    vidnih izkrivljanj, ki niso funkcionalni za dinamiko razvoja. To je posebej
    očitno v kritiki urbane forme privržencev Inštituta Sigmunda Freuda v
    Frankfurtu: gl. A. Mitscherlich, *Die unwirtlichkeit unserer Städte*,
    Frankfurt a. M. 1965, ital. prev. Torino, Einaudi, 1968; in M. Berndt, A.
    Lorenzer, K. Horn, *Architektur als ideologie*, Frankfurt a. M. 1968, ital.
    prev. Bari, Laterza, 1969.

<!--

[^28]: L'utopia dell'emancipazione svolge oggettivamente il proprio ruolo, tutto
    marginale, nello stimolare soluzioni sociali particolari: nell'eliminazione
    di alcune distorsioni particolarmente vistose, e non funzionali alla
    dinamica dello sviluppo. Ciò è particolarmente evidente nella critica alla
    forma urbana degli adepti al Sigmund Freud Institut di Francoforte: cfr. A.
    Mitscherlich, *Die unwirtlichkeit unserer Städte*, Frankfurt a. M. 1965,
    trad. it. Torino, Einaudi, 1968; e M. Berndt, A. Lorenzer, K. Horn,
    *Architektur als ideologie*, Frankfurt a. M. 1968, trad. it. Bari, Laterza,
    1969.

-->

[^29]: Gl. K. R. Popper, *The Open Society and its Enemies*, London 1962, toda
    glej tudi H. Albert, *Ethik und Metaethik*, v "Archiv für Philosophie" XI
    1961, str. 59 in dalje, in J. Habermas, nav. d., str. 98 in dalje.

::: alts

> Tako v prvi plan pride "podružbljanja plana" kot moment refleksije plana nad
> samim seboj: negacija se preoblikuje v stalen nadzor "pozitivnega".

::: {lang=it}

> Torna così in primo piano il tema della "socializzazione del piano" come
> momento di riflessione del piano su se stesso: la negazione si trasforma in
> controllo costante del "positivo".

:::

:::

::: alts

> Primer prestrukturiranja intelektualnega dela v ločene vloge in ponovnega
> združevanja disciplin v napredne tehnike programiranja je formacija
> "pogovornih jezikov", ki se uporabljajo v pogovorih med računalniškimi sistemi
> povezanimi v komunikacijsko omrežje (information network) in v pogovorih med
> človekom in računalnikom, ki jih omogočajo tehnološki razvoji na polju
> terminalov, prenosa podatkov na telefonskih linijah in projektiranja
> večopravilnih sistemov (time-sharing)[^30].

::: {lang=it}

> Un esempio di ristrutturazione del lavoro intellettuale in ruoli
> differenziati, e di riunificazione delle discipline in tecniche avanzate di
> programmazione, è la formazione dei "linguaggi /265/conversazionali",
> in uso nei dialoghi tra sistemi di calcolo connessi tra loro da una rete di
> linee di comunicazione (information network), e nei dialoghi tra uomo e
> calcolatore, permessi dagli sviluppi tecnologici nel campo dei terminali,
> nella trasmissione dei dati su linee telefoniche e nella progettazione di
> sistemi ad accessi multipli (time-sharing)[30].

:::

:::

[^30]:
O teh komunikacijskih tehnikah gl. Mauro M. Pacelli, *I linguaggi conversazionali e la loro influenza nella ricerca e nell'industria*, v *Linguaggi nella società e nella tecnica*, Zbornik konference, ki jo je podprla družba Olivetti (Torino, 14--17 oktober 1968), Milano 1970, str. 433 in dalje, in nadaljnji eseji, ki jih vsebuje zvezek: Adriaan Van Wijngaarden, *On the Boundary between Natural and Artificial Languages*, str. 165 in dalje.; S. Kostantinovič Šaumjan, *Linguistic models as artificial languages simmulating natural languages*, str. 269 in dalje; René Moreau, *Languages naturels et languages de programmation*, str. 303 in dalje; Ole-Johan Dahl, *Decomposition and classification in programming languages*, str. 371 in dalje; Alfonso Caracciolo di Forino, *I linguaggi speciali di programmazione come strumenti avanzati di pensiero*, str. 385 in dalje; Ivan E. Sutherland, *Pictures Languages*, str. 423 in dalje.
Zbornik je pomemben, ker je v njem zbrana izjemna dokumentacija o različnih tehnikah programskih jezikov, ki dopuščajo neposredno primerjavo med prispevki v zvezi s semantiko "naravnih" jezikov (gl. tudi eseje, ki so vsebovani v prvem delu knjige) in tistimi, ki so bili razdelani ali so v razdelavi za neposredno uporabo "programskih jezikov" v kibernetiki.

<!--

[^30]: Su queste tecniche di comunicazione cfr. Mauro M. Pacelli, *I linguaggi
    conversazionali e la loro influenza nella ricerca e nell'industria*, in
    *Linguaggi nella società e nella tecnica*, Atti del convegno promosso dalla
    Olivetti (Torino, 14--17 ottobre 1968), Milano 1970, pp. 433 ss., e gli
    ulteriori saggi sul tema contenuti nel volume: Adriaan Van Wijngaarden, *On
    the Boundary between Natural and Artificial Languages*, pp. 165 ss.; S.
    Kostantinovič Šaumjan, *Linguistic models as artificial languages
    simmulating natural languages*, pp. 269 ss.; René Moreau, *Languages
    naturels et languages de programmation*, pp. 303 ss.; Ole-Johan Dahl,
    *Decomposition and classification in programming languages*, pp. 371 ss.;
    Alfonso Caracciolo di Forino, *I linguaggi speciali di programmazione come
    strumenti avanzati di pensiero*, pp. 385 ss.; Ivan E. Sutherland, *Pictures
    Languages*, pp. 423 ss. L'importanza del volume è nel suo raccogliere una
    notevole documentazione sulle varie tecniche dei linguaggi di
    programmazione, permettendo un confronto diretto fra i contributi relativi
    alla semantica dei linguaggi "naturali" (cfr. anche i saggi contenuti nella
    prima sezione del libro), e quelli elaborati o in corso di elaborazione per
    un diretto uso dei "linguaggi di programmazione" stessi nella cibernetica.

-->

::: alts

> Neposredna izmenjava sporočil med menedžerjem in strojem ponovno vstopi v
> okvir raziskave, ki je namenjena razdelavi podatkov, ki jih ni mogoče v celoti
> formalizirati: dialog v tem primeru dopušča upoštevanje vseh nepredvidenih
> spremenljivk, ki se lahko razkrijejo med delovanjem, in same zapletenosti
> problema, ki je zastavljen računalniku. Kar nas v tem delovanju zanima je
> nujnost vpeljave "pogovornih modelov", ki temeljijo na kodah, ki upoštevajo
> *strukturo dialoga*: tako se preseže preprosta formulacija "problemsko
> usmerjenih jezikov", ki jo je izvorno spodbujala možnost uporabe elektronskih
> računalnikov in splošno programirljivih strojev[^31].

::: {lang=it}

> Lo scambio di messaggi diretti fra il manager e la macchina rientra nel quadro
> di una ricerca volta a elaborare dati non completamente formalizzabili: il
> dialogo permette in tal caso di tener conto di tutte le variabili impreviste
> che possono presentarsi nel corso dell'operazione, e della complessità stessa
> del problema posto al calcolatore. Ciò che ci interessa, in tale operazione, è
> la necessità di introdurre "modelli di conversazione" basati su codici che
> tengano conto della *struttura del dialogo*: viene così superata la semplice
> formulazione dei "linguaggi orientati verso problemi", stimolata in origine
> dalle possibilità di uso dei calcolatori elettronici, e in genere delle
> macchine programmabili[31].

:::

:::

[^31]: A. Caracciolo di Forino, *I linguaggi speciali di programmazione* nav.
    d., str. 385.

::: alts

> Kvantifikacija napovedi v najnaprednejših tehnikah planiranja zatorej ne
> zanemarja subjektivnih sprememb in notranjih spremenljivk danega problema:
> pogoj za to je nadzor nad tako imenovanimi "ne numeričnimi" računskimi
> procesi. Razvoj "programskih jezikov" tako napravi odločilen skok od samih
> lingvističnih disciplin ter jih preoblikuje iz analitičnih tehnik v pripomočke
> za ustvarjanje cele vrste visoko formaliziranih jezikov[^32]. Ob opisovanju
> nekaterih "programskih jezikov za splošno rabo" -- jezikov, ki lahko
> obdelujejo vse vrste podatkov, "kot tudi dopuščajo lokalne opredelitve novih
> podatkovnih struktur" -- Alfonso Caracciolo opomni, da je ta tip jezikov
> "posebej pomemben, ker vzpostavlja pomemben korak k ustvarjanju zmogljivih
> lingvističnih sistemov, ki so opremljeni z lokalnim mehanizmom samorazširjanja
> in so zato bližje našim naravnim jezikom, ki imajo sicer neomejeno, toda na
> žalost nesistematično in v resnici pogosto dvoumno, moč razširitve"[^33].

::: {lang=it}

> La quantificazione delle previsioni, nelle più avanzate tecniche di
> pianificazione, non prescinde, dunque, dalle modificazioni soggettive e dalle
> variabili interne al problema dato: condizione di ciò è il controllo dei
> processi di calcolo cosiddetti "non numerici". Lo sviluppo dei "linguaggi di
> programmazione" fa dunque compiere un salto decisivo alle stesse discipline
> linguistiche, trasformandole da tecniche analitiche, in /266/ausilio
> per la creazione di tutta una serie di linguaggi altamente formalizzati[32].
> Descrivendo alcuni "linguaggi di programmazione di uso generale" -- di
> linguaggi, cioè in grado di trattare qualsiasi tipo di dati, "nonché di
> ammettere definizioni locali di nuove strutture di dati" -- Alfonso Caracciolo
> nota che questo tipo di linguaggio "è ... particolarmente importante perché
> costituisce un notevole passo avanti verso la creazione di potenti sistemi
> linguistici, di lingue, cioè dotate di un meccanismo locale di
> auto-estensione, e pertanto molto più vicine ai nostri linguaggi naturali, che
> hanno un potere di estensione illimitato, ma sfortunatamente non sistematico e
> in verità spesso assai ambiguo"[33].

:::

:::

[^32]: Gl. J. O. Dahl, *Decomposition and Classification* nav. d., in A.
    Caracciolo di Forino, nav. d., str. 36.

[^33]: A. Caracciolo di Forino, nav. d., str. 386--387

::: alts

> Novim vejam matematike, ki so ustanovljene za proučevanje dinamičnih sistemov
> -- teorija avtomatov --, ustreza torej nova tehnika za opredeljevanje in
> analiziranje teh sistemov s pomočjo tako imenovanih *simulacijskih jezikov*.

::: {lang=it}

> Ai nuovi rami della matematica istituita per lo studio dei sistemi dinamici --
> la teoria degli automi -- corrisponde dunque una nuova tecnica per definire e
> analizzare quei sistemi, mediante i così detti *linguaggi di simulazione*.

:::

:::

::: alts

> Seveda nas ne zanima vstopiti v specifično analizo umetnih in programskih
> jezikov: sklic služi vrednotenju dosežene ravni splošne tehnike programiranja,
> kar potrjuje kar smo zgoraj potrdili glede usode tradicionalnih disciplin in
> vloge interdisciplinarnosti za namene kvantifikacije razvoja[^34].

::: {lang=it}

> Non ci interessa, evidentemente, entrare nell'analisi specifica relativa ai
> linguaggi artificiali e di programmazione: l'accenno fatto ci serve per
> valutare il livello raggiunto nelle tecniche di programmazione generale,
> verificando quanto sopra abbiamo affermato circa il destino delle discipline
> tradizionali e il ruolo dell'interdisciplinarietà ai fini della
> quantificazione dello sviluppo[34].

:::

:::

[^34]: "Vendar je možno tudi konstruirati jezike na še višji ravni, ki omogočajo
    določitev mehaničnega postopka glede na rezultate, ki jih je treba doseči,
    namesto, da bi določili postopke, ki jih je treba opraviti [...] Tovrstni
    jeziki predstavljajo [...] tipičen primer jezika, ki lahko opredeli problem"
    (prav tam, str. 392)

<!--

[^34]: "Ma è anche possibile costruire dei linguaggi a livello ancora superiore che
consentono di specificare una lavorazione meccanica in termini risultati da
ottenere anziché specificando le operazioni da compiersi ... Linguaggi del
genere rappresentano ... un esempio tipico di linguaggi in grado di definire
un problema" (ibidem, p. 392).

-->

::: alts

> Naštete lingvistične inovacije dejansko imajo nekaj skupnih značilnosti:

::: {lang=it}

> Nelle innovazioni linguistiche elencate si danno infatti alcune
> caratteristiche comuni:

:::

:::

::: alts

> 1. z njimi upravljajo sistemi samopreverjanja, ki so vezani zgolj na
>    maksimalno uvrednotenje tehnik plana: lahko, torej, popolnoma zanemarijo
>    vprašanja vrednot;

::: {lang=it}

> 1. esse sono rette da sistemi di autoverifica legati unicamente alla massima
>    valorizzazione delle tecniche di piano: possono quindi prescindere
>    completamente da questioni di valore;

:::

:::

::: alts

> 2. postavljajo se kot instrumenti, ki so v veliki meri neodvisni od
>    specifičnih značilnosti sistemov, ki so za vključiti v programiranje: vsa
>    njihova prizadevanja so usmerjena v opredeljevanje splošnih instrumentov
>    plana;

::: {lang=it}

> 2. si pongono come strumenti in buona parte indipendenti dalle specifiche
>    caratteristiche dei sistemi da investire con la programmazione: tutto il
>    loro sforzo è volto a definire strumenti generali di piano;

:::

:::

::: alts

> 3. preskakujejo interdisciplinarno raven prav zato, da se postavijo kot
>    sistemi, ki lahko delujejo neodvisno od posameznih polj uporabe.

::: {lang=it}

> 3. esse saltano il livello interdisciplinare, proprio ponendosi /267/come
>    sistemi atti a funzionare indipendentemente dai singoli campi di
>    applicazione.

:::

:::

::: alts

> Vse to lahko izrazimo tudi drugače: tehnike odločanja, s prilaščanjem
> ogromnega potenciala, ki je v elektronskem računalniku, težijo k ustvarjanju
> specifičnih *jezikov plana* in posledično novih splošnih disciplin, ki
> *izhajajo iz plana*. Kar pojasnjuje tudi nekatere pojave, ki spremljajo te
> procese. Nedvomno je, da so ogromne razvoje moderne lingvistike spodbudile
> nove potrebe, ki so jih narekovale tehnike strojne komunikacije. Tako kot je
> res, da je prav kmalu potrjena nova delitev nalog. Semantični analizi je bila
> odrejena preiskava tega, kar je potrebno izključiti iz formaliziranih
> programskih jezikov: *dvoumnost* "naravnega" jezika; tehnikam ustvarjanja
> umetnih jezikov je odrejena edina naloga *inovacije*, ki je dopustna na ravni
> komunikacije: natančneje ta, ki je usmerjena v učinkovito instrumentalizacijo
> tehnik odločanja.

::: {lang=it}

> Possiamo esprimere tutto ciò in un altro modo: le tecniche decisionali,
> nell'appropriarsi delle enormi potenzialità contenute nel calcolatore
> elettronico, tendono a creare gli specifici *linguaggi del piano*, e,
> conseguentemente, le nuove discipline generali elaborate a *partire dal
> piano*. Il che spiega anche alcuni dei fenomeni che si accompagnano a tali
> processi. È indubbio che gli enormi sviluppi della linguistica moderna siano
> stati stimolati dalle nuove necessità dettate dalle tecniche di comunicazione
> con le macchine. Così come è vero che ben presto si è verificata una divisione
> dei compiti. All'analisi semantica si è riservata l'indagine su ciò che è
> necessario espellere dai linguaggi formalizzati di programmazione:
> l'*ambiguità* del linguaggio "naturale"; alle tecniche di creazione dei
> linguaggi artificiali è riservato l'unico compito di *innovazione* ammissibile
> al livello della comunicazione: quello, appunto, rivolto alla strumentazione
> efficiente delle tecniche decisionali.

:::

:::

::: alts

> Semantična analiza jezika pa je v zameno spodbudila vzpon -- ali bolje,
> ponovni vzpon -- ideologije umetniško-literarne avantgarde. Zahtevo, že
> avantgard in kasneje neoavantgard v 60\., da se predstavljajo kot *delo na
> besedi*, kot kritično eksperimentiranje z artikulacijo jezika, je zato treba
> meriti z realnostjo konkretnega in produktivnega dela na novih možnostih
> programirane komunikacije[^35].

::: {lang=it}

> L'analisi semantica del linguaggio, a sua volta, ha stimolato il sorgere -- o,
> meglio, il risorgere -- di un'ideologia dell'avanguardia artistico-letteraria.
> La pretesa, già delle avanguardie e poi, negli anni '60, delle
> neo-avanguardie, di porsi come *lavoro sulla parola*, come sperimentazione
> critica dell'articolazione del linguaggio, va quindi misurata con la realtà
> del concreto e produttivo lavoro sulle nuove possibilità di comunicazione
> programmata[35].

:::

:::

[^35]: O funkciji neoavantgarde glej eseje Franca Fortinija (*Le due
    avanguardie*), Giannija Scalie (*La nuova avanguardia, o della "miseria
    della poesia*) in E. Sanguinetija (*Avanguardia, società impegno*) v zbirki
    *Avanguardie e neoavanguardie*, Milano, Sugar, 1966.

<!--

[^35]: Si veda, sulla funzione delle neoavanguardie, i saggi di Franco Fortini (*Le
due avanguardie*), di Gianni Scalia (*La nuova avanguardia, o della
"miseria" della poesia*) di E. Sanguineti (*Avanguardia, società impegno*),
nel vol. *Avanguardie e neoavanguardie*, Milano, Sugar, 1966.

-->

::: alts

> Vendar to ni dovolj: poetika nedoločenosti, *odprtega dela*, dvoumnosti,
> povzdignjene v institucijo, dejansko vztrajajo -- v večini primerov -- prav na
> poljih, ki so jih definirale nove tehnike komunikacije človeka-stroja: v tem
> smislu je primer glasbe ex machina zgolj najbolj ekspliciten primer.

::: {lang=it}

> Ma non basta: la poetica dell'indeterminazione, dell'*opera aperta*,
> dell'ambiguità elevata a istituzione, insiste infatti -- nella gran parte dei
> casi -- esattamente sui campi definiti dalle nuove tecniche di comunicazione
> uomo-macchina: il caso della musica ex machina è solo l'esempio più esplicito
> in tal senso.

:::

:::

::: alts

> Ni več ideologija tista, ki bi, po tem ko je prevzela utopistične konotacije,
> nakazovala nepredvidene smeri delovanja za tehnike programiranja. Delo na
> komunikacijskih materialih -- tu mislimo na vse vizualne, literarne in
> glasbene tehnike ali njihove izpeljanke -- ni več odgovorno za anticipiranje
> smeri razvoja: preseganje *končnih modelov* v tehniki napovedovanja ukine
> utopično vlogo novih ideologij.

::: {lang=it}

> Non è più l'ideologia che, dopo aver assunto connotati utopistici, indica
> linee di lavoro impreviste alle tecniche di programmazione. Al lavoro sui
> materiali della comunicazione -- ci riferiamo a tutte le tecniche di
> comunicazione visiva, letteraria, /268/musicale, o alle loro
> contaminazioni -- non spetta più l'anticipazione delle linee di tendenza
> relative allo sviluppo: il superamento dei *modelli finali*, nella tecnica
> previsionale, estingue il ruolo utopistico delle nuove ideologie.

:::

:::

::: alts

> Tudi tu se pojavi simptomatičen proces. Izrinjena iz razvoja se ideologija
> obrne proti razvoju samemu: v formi oporečništva poskuša, torej, lastno
> ekstremno obnovo. Ker se ne more več predstavljati kot utopija -- četudi v tem
> smislu še vedno vztraja veliko poskusov: toda njihove konotacije, ki jih
> spodbuja popolni cinizem, ne morejo nikogar preslepiti -- se ideologija
> preobrne k nostalgični kontemplaciji svojih zastarelih vlog, ali k
> samooporekanju: in dobro vemo kako je samooporekanje, od Baudelaira in
> Rimbauda ter kasneje, instrument preživetja.

::: {lang=it}

> Anche qui si manifesta un processo sintomatico. Espulsa dallo sviluppo,
> l'ideologia si rivolta contro lo sviluppo stesso: sotto la forma della
> contestazione tenta, cioè, il proprio recupero estremo. Non potendo più porsi
> come utopia -- anche se permangono non pochi tentativi in tal senso: ma i loro
> connotati, informati al più totale cinismo, non possono ingannare nessuno --
> l'ideologia si rovescia in contemplazione nostalgica dei propri ruoli
> superati, o in autocontestazione: e ben sappiamo come, per l'ideologia
> moderna, da Baudelaire e Rimbaud in poi, l'autocontestazione sia strumento di
> sopravvivenza.

:::

:::

::: alts

> Kakorkoli pa ostaja nespremenjeno, da je vsaka pot *zunanje* razdelave za
> intelektualno delo še vedno izključena. Edino iluzijo *zunanjega* dela lahko
> poda upor proti intelektualnemu delu samemu, ki se izraža kot *sakralizacija
> njegove impotence*: natanko to kar nezavedno počne večina nedavne umetniške
> produkcije. Onkraj tega je skok nazaj, je "pogum govoriti o vrtnicah", je
> brodolom v "veselem času" buržoaznega Kultur: ideologija kot "sublimna"
> nekoristnost. Toda ni slučaj, da se zgodovinska usoda formalizmov vedno
> zaključi z "oglaševalsko" izrabo *dela na formi*.

::: {lang=it}

> Rimane comunque fisso che ogni strada di elaborazione *esterna*, per il lavoro
> intellettuale, resta preclusa. L'unica illusione di lavoro *esterno* può
> essere data dalla rivolta contro il lavoro intellettuale stesso, espresso
> dalla *sacralizzazione della sua impotenza*: esattamente ciò che tanta
> produzione artistica recente fa inconsapevolmente. Al di là di ciò è il salto
> all'indietro, è il "coraggio di parlar delle rose", il naufragio nel "tempo
> felice" della Kultur borghese: l'ideologia come "sublime" inutilità. Ma non è
> un caso che il destino storico dei formalismi si concluda sempre
> nell'utilizzazione "pubblicitaria" del *lavoro sulla forma*.

:::

:::

::: alts

> Znašli smo se torej pred skrajno pomembnim pojavom. Racionalizacija ne služi
> več jezikom, ki *govorijo* o razvoju -- to nalogo še vedno opravlja, seveda,
> vendar gre le za obrobno funkcijo -- temveč vse bolj gospoduje jeziku *samega*
> razvoja. Z ustvarjanjem kod, ki so vse že *znotraj* tehnoloških struktur
> programiranja, se izključi vsaka možnost "etičnega" opravičila, z *zunanjimi*
> jeziki, programiranja samega. Ustvarjanje formaliziranih programskih jezikov
> odpravlja utopijo nadzorovanja *ciljev* plana racionalizacije s pomočjo
> instrumentov komunikacije, ki ne privzemajo njegovih značilnosti in pomenov.
> Projekt nadzora razvoja skozi intelektualno delo, ki naj bi mu zagotovil
> teleologijo -- Benjaminov projekt, v bistvu -- je zvrnjen v vice utopije,
> neuporaben zaradi nenehnega procesa marginalizacije "pomena", ki mu te
> programski jeziki sledijo. 

::: {lang=it}

> Ci troviamo quindi di fronte a un fenomeno estremamente significativo. La
> razionalizzazione non si serve più di linguaggi che *parlano* dello sviluppo
> -- questo è un compito ancora svolto, certo, ma solo con funzioni marginali --
> ma tende a dominare il linguaggio *dello* sviluppo. Creando codici già tutti
> *dentro* le strutture tecnologiche della programmazione, si elimina ogni
> possibilità di giustificare "eticamente", con linguaggi *esterni*, la
> programmazione stessa. La creazione dei linguaggi formalizzati di
> programmazione fa cadere l'utopia di un controllo dei *fini* del piano di
> razionalizzazione mediante strumenti di comunicazione che ne assumano le
> caratteristiche e le connotazioni. Il progetto di controllo dello sviluppo
> tramite un lavoro intellettuale teso a fornirlo di teleologia -- il progetto
> di un Benjamin, in sostanza -- viene respinto nel limbo dell'utopia,
> inutilizzabile da parte del continuo processo di /269/emarginazione del
> "significato", che quei linguaggi di programmazione perseguono.

:::

:::

::: alts

> Vse to je le znak razširitve celotnega kapitalističnega gospostva nad
> univerzumom, ki ga oblega plan: torej tudi nad univerzumom "družbene"
> komunikacije.

::: {lang=it}

> Tutto ciò è solo un segno dell'estensione del dominio capitalistico
> complessivo sull'universo investito dal piano: anche, quindi, sull'universo
> della comunicazione "sociale".

:::

:::

::: alts

> Formalna uporaba|izraba jezika razvoja|razvojnega jezika ima|dobi poseben
> pomen v trenutku, ko je projeciran na mesto. Moderni urbanizem -- kot utopični
> poskus obvarovanja mestne *forme*, oziroma principa forme, ki je vpet v
> dinamiko urbanih struktur  -- ni uspel realizirati svojih strukturnih modelov.
> Toda na območju urbanih in teritorialnih struktur ves prispevek zgodovinskih
> avantgard živi s neko svojo posebno duhovitostjo: mesto kot oglaševalska in
> samooglaševalska struktura, hkrati kot kanali komunikacije, postane neke vrste
> stroj, ki zagotovo ne deluje kot produkcijski stroj, toda zagotovo je dejaven
> kot oddajnik nenehnih sporočil, kodificiranih *v formi* programskih jezikov.
> (Nedoločeno je podano v svoji specifični formi, ponudi se kot edino možno
> določilo urbane celote: spomnimo se na obsežno teoretiziranje, v 60\. letih, o
> temi teritorija kot "magnetskega polja")[^36].

::: {lang=it}

> L'utilizzazione formale del linguaggio dello sviluppo ha un significato
> particolare nel momento in cui questo è proiettato sulla città. L'urbanistica
> moderna -- in quanto utopistico tentativo di salvaguardare una *forma* per la
> città, anzi un principio di forma calato nella dinamicità delle strutture
> urbane -- non ha potuto realizzare i propri modelli strutturali. Ma
> nell'ambito delle strutture urbane e territoriali tutto il contributo delle
> avanguardie storiche vive con una sua particolare pregnanza: la città come
> struttura pubblicitaria e autopubblicitaria, come insieme di canali di
> comunicazione, diviene una sorta di macchina, certo non funzionante come
> macchina di produzione, ma sicuramente attiva in quanto emittente di
> incessanti messaggi codificati *nella forma* dei linguaggi di programmazione.
> (L'indeterminato si dà nella sua forma specifica, si offre come unica
> determinatezza possibile per l'insieme urbano: si pensi al vasto teorizzare,
> negli anni '60, sul tema del territorio come "campo magnetico")[36].

:::

:::

[^36]: Gl. poročilo G. De Carla na konferenci INU v Trstu (september 1965) in C.
    Alexander, *Notes on the Synthesis of Form*, Harvard College 1964, ital.
    prev. Milano, Il Saggiatore, 1967.

::: alts

> Tako se uteleša poskus *oživiti* jezik razvoja: napraviti ga konkretno
> izkustvenega v vsakdanjem življenju.

::: {lang=it}

> In tal modo prende corpo il tentativo di *far vivere* il linguaggio dello
> sviluppo: di renderlo concretamente sperimentabile nella vita quotidiana.

:::

:::

::: alts

> Ker na tem mestu ne moremo drugega kot navesti namige, je treba poudariti, da
> ustvarjanju jezika plana nenehno spremlja vzporedna teoretizacija, kateri
> enotni struji lahko sledimo od splošne znanosti o znakih -- Piercova semiotika
> -- do najnovejših razdelav *tehnološke estetike*.

::: {lang=it}

> Per quanto in questa sede non si potranno che fare degli accenni, va rilevato
> che alla creazione del linguaggio del piano si è accompagnata costantemente
> una teorizzazione parallela, la cui linea unitaria è tracciabile a partire
> dalla scienza generale dei segni -- la semiotica del Peirce -- fino alle più
> recenti elaborazioni dell'*estetica tecnologica*.

:::

:::

::: alts

> Poziv je vse prej kot akademski. Raziskovanje področij, ki zadevajo gramatiko,
> logiko in semiotiko so omogočili prispevki Wittgensteina, Carnapa in Fregeja;
> Piercev metodološki pragmatizem prispe do prikaza pogojev manipulativnosti
> čistega znaka, ki je povsem prikrajšan vsake označevalne implikacije, je brez
> vsakršne semantične reference. *Natančna teorija znakov* postane nujna v
> trenutku, ko se odkrije nujnost komunikacijskih shem, ki so neodvisne od
> subjektivnih interpretacij. Vstop informacijskih teorij v vrednotenje količin
> informacij, ki so vsebovane v strukturiranih nizih znakov, resda izhaja -- na
> primer pri Benseju[^37] -- iz nujnosti obravnave estetske komunikacije kot
> obdarjene s pomenom, ki popolnoma sovpada s fizično realnostjo dela, toda jo
> nedvoumno vodi nazaj v matematično opisljivo logiko brez možnosti napak.
> "Konstruktivnost" estetske komunikacije je, v tem smislu, možno zvesti na
> serijo alternativnih izbir, ki ustrezajo delovanju binarnih računalnikov[^38].

::: {lang=it}

> Il richiamo è tutt'altro che accademico. La ricerca delle aree di pertinenza
> della grammatica, della logica e della semiotica è permessa dai contributi di
> Wittgenstein, Carnap e Frege; il pragmatismo metodologico del Peirce giunge a
> indicare le condizioni di manipolabilità del segno puro, del tutto privo
> /270/di ogni implicazione designativa, di ogni riferimento semantico. Una
> *teoria esatta dei segni* diviene necessaria nel momento in cui si scopre la
> necessità di schemi di comunicazione indipendenti da interpretazioni
> soggettive. L'ingresso della teoria dell'informazione nella valutazione dei
> quanti di informazione contenuti in insiemi strutturati di segni, parte sì --
> in Bense, ad esempio[37] --, dalla necessità di considerare la comunicazione
> estetica come dotata di un significato che coincida completamente con la
> realtà fisica di un'opera, ma si riconduce senza possibilità di equivoco a una
> logica descrivibile matematicamente. La "costruttività" della comunicazione
> estetica è riducibile, in tal senso, a una serie di scelte alternative, del
> sito corrispondenti al funzionamento dei calcolatori numerici a due stati[38].

:::

:::

[^37]: Gl., od Maxa Benseja, *Aesthetica*, Baden-Baden, Agis Verlag, 1965;
    *Semiotik, allgemeine Theorie der Zeichen*, ibidem, Agis Verlag, 1967;
    *Estetica e programmazione*, Roma, Silva, 1969\. Odlična analiza Bensejevih
    del, ki se povsem drži splošnih smeri interpretacije, ki jo predlagamo, se
    nahaja v diplomi G. Pasqualotto (Fakulteta za književnost in literaturo
    Univerze v Padovi 1969), ki bo kmalu izšla pri založbi Officina.

<!--

[^37]: Cfr., di Max Bense, *Aesthetica*, Baden-Baden, Agis Verlag, 1965;
    *Semiotik, allgemeine Theorie der Zeichen*, ibidem, Agis Verlag, 1967;
    *Estetica e programmazione*, Roma, Silva, 1969\. Un'eccellente analisi
    dell'opera di Bense, in tutto aderente alle linee generali d'interpretazione
    da noi avanzata, è nella tesi di laurea di G. Pasqualotto (Facoltà di
    Lettere e Filosofia dell'Università di Padova 1969), di prossima
    pubblicazione per le edizioni Officina.

-->

[^38]: Gl. Max Bense, *Aesthetica* nav. d., I. del, str. 16 in dalje, in A.
    Moles, *Theorie de l'information et perception esthétique*, Paris,
    Flammarion, 1965: ital. prev. Milano, Lerici, 1969.

::: alts

> Študije, ki jih je vodil Norbert Wiener, o projektih servomehanizmov za
> vojaške enote, izvirajo iz leta 1943: na podlagi teh študij, kot je znano, se
> je začela razdelava avtomatskih mehanskih modelov, ki temeljijo na splošni
> teoriji samoupravnih sistemov. Izjemno pomenljivo je torej, da je uporaba
> principa entropije ali nepovratnosti komunikacije -- temeljnega pomena za
> zagotavljanje učinkovitosti in logične skladnosti postopkov samoupravnih
> sistemov -- bila predvidena v letih med 20\. in 30\. leti 20\. stoletja na
> področju lingvistične analize: informacijska teorije je pri določanju
> *formalnih pomenov*, ki jih je opravljal ruski formalizem, že povsem
> prisotna[^39].

::: {lang=it}

> Gli studi diretti da Norbert Wiener intorno a progetti di servomeccanismi
> applicabili a unità belliche, sono del 1943: da quegli studi, come è noto, ha
> preso avvio l'elaborazione di modelli meccanici automatici, in base a una
> teoria generale dei sistemi autogovernati. È estremamente significativo,
> quindi che l'applicazione del principio dell'entropia, o dell'irreversibilità
> della comunicazione -- fondamentale per assicurare efficacia e coerenza logica
> di procedimento ai sistemi autogovernati -- sia stato anticipato, negli anni
> fra il '20 e il 30, in sede analisi linguistica: la teoria dell'informazione è
> già tutta presente nella determinazione dei *significati formali* compiuta dal
> formalismo russo[39].

:::

:::

[^39]: O tej temi gl. Victor Erlich, *Russian Formalism*, The Hague, Monton,
    1954, ital. prev. Milano, Bompiani, 1966.

::: alts

> V Bensejevi estetiki -- v elektronski glasbi kakšnega Stockhausena ali
> Xenakisa, v "programirani umetnosti", v nekaterih eksperimentih fonetične
> poezije -- je razmerje ideologija-znanost programiranja natančno obrnjeno.
> Splošna teorija znakov na silo vstopi v estetiko in v formalno raziskavo:
> absolutno sovpadanje znaka s samim seboj, absolutno izginotje kakršnega koli
> zunanjega "pomena" iz njega, objektivnost umetniškega fenomena, ne več
> *projekt* temveč čisto objektivno *dejstvo*, ocenljivo z binarnimi sistemi, ni
> zgolj kibernetična hipostaza, temveč tudi zadnji poskus *emancipacije od nje*.

::: {lang=it}

> Nell'estetica di Bense -- nella musica elettronica di uno Stockhausen o di uno
> Xenakis, nell'"arte programmata", in alcuni esperimenti di poesia fonetica --,
> il rapporto ideologia-scienza della programmazione è puntualmente invertito. La
> teoria generale dei segni entra di peso nell'estetica e nella ri/271/cerca
> formale: l'assoluta coincidenza del segno con se stesso, la sparizione assoluta
> da esso di qualsiasi "significato" esterno, l'oggettualità del fenomeno
> artistico, non più *progetto* ma puro *fatto* oggettivo, valutabile con sistemi
> binari, non sono solo una ipostasi della cibernetica, ma anche l'ultimo
> tentativo di *emancipazione da essa*.

:::

:::

::: alts

> *Bolj matematično* branje umetniških sistemov komunikacije teži k obnovi
> tradicionalnih funkcij umetnosti. Odčarano sprejemanje popolne tehnizacije
> estetskih procesov je v temelju razžaljenja avtonomije teh procesov od
> realnega[^40]; programski jezik abstrahira od samega sebe, postane *možnost*
> za semantični zdrs, postavi se kot formalizacija na kvadrat[^41]. Kdor še ni
> opazil, opozarjamo, da analiza tehnološke estetike v celoti dokazuje, kar smo
> zgoraj pritrjevali o marginalni funkciji ideologija znotraj univerzuma plana.

::: {lang=it}

> La lettura *more mathematico* dei sistemi di comunicazione artistica tende a
> recuperare le funzioni tradizionali dell'arte. L'accettazione disincantata
> della totale tecnicizzazione dei processi estetici è a fondamento di una
> esasperazione dell'autonomia di quei processi dal reale[40]; il linguaggio di
> programmazione astrae da se stesso, diviene *occasione* di slittamento
> semantico, si pone come formalizzazione al quadrato[41]. Per chi non se ne
> fosse accorto, avvertiamo che l'analisi dell'estetica tecnologica dimostra in
> pieno quanto sopra abbiamo affermato sulla funzione marginale dell'ideologia
> all'interno dell'universo del piano.

:::

:::

[^40]: Bense sam opozarja, da hegeljanska dialektika primerljiva s kibernetično
    dinamiko: obe vodita k pojmu zavesti, ki se nanaša zgolj na *Funktion* (M.
    Bense, *Die Aktualität der Hegelschen Aesthetik*, v *Aesthetica* nav. d.,
    III\. del, str. 196 in dalje. Gl. tudi Leo Apostel, *Materialismo dialettico
    e metodo scientifico*, Bruxelles 1960, ital. prev. Torino, Einaudi, 1968).
    Vendar je treba opozoriti, da vsa tehnološka estetika teži k abstraktni
    objektivizaciji lingvističnega sredstva ter znanstveno legitimira trend
    zgodovinskih in sodobnih avantgard (od Mallarméja do elektronske glasbe).
    Znotraj tega trenda je pojasnjeno zakaj izničenje semantične vrednosti
    estetske komunikacije svojo pot išče v tehnološki formalizaciji. *Znak*,
    čisto *bistvo znaka*, se nanaša zgolj na zakone lastne strukture; njegova
    utemeljenost na informacijski teoriji teži k preseganju -- z vključevanjem
    vase -- merljivosti informacije same, njene izvenlingvistične narave. Kar
    ostane je gola tavtološka afirmacija formaliziranega univerzuma znakov, ki
    svojo "vrednoto" iščejo v *razkazovanju lastne tujosti* kateremu koli
    sistemu vrednot. 

<!--

[^40]: Lo stesso Bense avverte che la dialettica hegeliana è accostabile alla
    dinamica cibernetica: entrambe conducono a un concetto di coscienza riferito
    unicamente alla *Funktion* (M. Bense, *Die Aktualität der Hegelschen
    Aesthetik*, in *Aesthetica* cit., teil III, pp. 196 ss. Cfr. anche Leo
    Apostel, *Materialismo dialettico e metodo scientifico*, Bruxelles 1960,
    trad. it. Torino, Einaudi, 1968). Va comunque osservato che tutta l'estetica
    tecnologica tende all'oggettivazione astratta del mezzo linguistico,
    legittimando scientificamente la linea di tendenza delle avanguardie
    storiche e contemporanee (da Mallarmé alla musica elettronica). Entro tale
    tendenza si spiega perché l'annullamento del valore semantico della
    comunicazione estetica cerchi la sua strada in una formalizzazione
    tecnologica. Il *segno*, il puro *esserci del segno*, rimanda unicamente
    alle leggi della propria struttura; il suo fondarsi sulla teoria
    dell'informazione tende a superare -- inglobandola in sé -- la misurabilità
    dell'informazione stessa, la sua natura extralinguistica. Ciò che rimane è
    la nuda affermazione tautologica di un universo formalizzato di segni, che
    cercano il proprio "valore" nella *esibizione della propria estraneità* a
    qualsiasi sistema di valori.

-->

[^41]: Gl. M. Bense, nav. d., II, str. 140 in dalje.

::: alts

> *Tehnološka umetnost*, ki se postavlja pod znak informacijske teorije,
> odkrije, da je opustitev vsake utopije, vsake tendence usmerjene v "projekt"
> edina možnost preživetja: razkrije se kot čista znakovnost, puščava pomenov,
> programirana reprodukcija semantičnih praznin.

::: {lang=it}

> Ponendosi sotto il segno della teoria dell'informazione, l'*arte tecnologica*
> scopre come unica possibilità di sopravvivenza l'abbandono di ogni utopia, di
> ogni tendenza verso un "progetto": si rivela come segnicità pura, deserto del
> significato, riproduzione programmata di vuoti semantici.

:::

:::

::: alts

> Kljub vsemu je tudi to ideološki projekt: vsekakor reduciran do minimuma, toda
> vseeno ga lahko ocenimo kot takega. Objektivnost umetnosti -- ki je z
> Benjaminom in konstruktivističnimi avantgardami bila v temelju zadnje
> pozitivne buržoazne utopije -- se zdaj razkrije kot zgolj pretveza za kakršno
> koli nadaljevanje biti: četudi ta bit sovpada z nenehnim govorjenjem o lastnem
> izumrtju njene reprezentacije, njene prisotnosti[^42]. 

::: {lang=it}

> Malgrado tutto, anche questo è un progetto ideologico: ridotto al minimo, certo,
> ma tuttavia valutabile solo in quanto tale. L'oggettualità dell'arte -- che con
> Benjamin e le avanguardie costruttiviste era a fondamento dell'ultima utopia
> positiva borghese -- si rivela ora come mero pretesto per conti/272/nuare
> comunque ad essere: anche se ciò comporta che tale essere coincida con il
> continuo parlare della propria estinzione di rappresentarla, di renderla
> presente[42].

:::

:::

[^42]: Samorazkrojevanje kot pogoj preživetja -- obstajanje v nenehnem zatiranju
    -- je le specifičen način vsake avantgarde na polju lingvistične in vizualne
    komunikacije. Tako se *negacija pomena* razkrije kot avtentičen "pomen"
    estetskega raziskovanja; to je onkraj patetične utopije o univerzalni obnovi
    skozi estetiko najbolj naivnih teoretikov neoavantgard.

<!--

[^42]: L'autodissolversi come condizione di sopravvivenza -- l'esserci come
    permanente sopprimersi -- è del resto il modo specifico di ogni avanguardia
    nel campo della comunicazione linguistica e visiva. La *negazione del
    significato* si rivela così come l'autentico "significato" della ricerca
    estetica; ciò al di là delle patetiche utopie di rigenerazione universale
    attraverso l'estetica, dei più ingenui teorici delle neoavanguardie.

-->

::: alts

> Naša teza je potrjena. Ideologija se reproducira na sami sebi. Njeno
> slavljenje plana in njegovih tehničnih form se zakriva pod patetično
> reafirmacijo avtonomije, ki nikogar ne zavaja: razlogi za njeno preživetje so
> povezani z viskoznostjo ideoloških institucij, ne z njihovo dejansko
> funkcionalnostjo.

::: {lang=it}

> Risulta confermata la nostra tesi. L'ideologia si riproduce su se stessa. Il
> suo celebrare il piano e le sue forme tecniche, è dissimulato sotto una
> patetica riaffermazione di autonomia, che non inganna nessuno: le ragioni
> della sua sopravvivenza sono legate alla vischiosità delle istituzioni
> ideologiche, non alla loro effettiva funzionalità.

:::

:::

::: alts

> Enkrat, ko so te procesi ugotovljeni, je treba pojasniti vztrajnost in nenehno
> reprodukcijo povsem ideološkega intelektualnega dela, ki se pojavlja na
> objektivno najbolj zaostalih ravneh kapitalističnega razvoja: in sicer na
> toliko bolj zaostalih ravneh kolikor bolj se to ideološko delo predstavlja kot
> alternativna uporaba discipline v funkciji antikapitalizma.

::: {lang=it}

> Una volta acclarati tali processi, va spiegato il persistere e il continuo
> riprodursi di un lavoro intellettuale puramente ideologico, attestato ai
> livelli oggettivamente più arretrati dello sviluppo capitalista: a livelli
> tanto più arretrati, anzi, quanto più quel lavoro ideologico si presenta come
> uso alternativo delle discipline in funzione anticapitalista.

:::

:::

::: alts

> Ko smo to že omenili, lahko pustimo ob strani najbolj očiten vidik te drže, to
> je tisti, ki je usmerjen v interno kritiko disciplin, namenjeno njihovi
> premestitvi v globalne procese razvoja.

::: {lang=it}

> Avendone già accennato, possiamo mettere da parte l'aspetto più evidente di
> tale atteggiamento, quello rivolto, cioè, a una critica interna delle
> discipline, finalizzata alla loro ricollocazione nei processi globali di
> sviluppo.

:::

:::

::: alts

> Vendar pa ni slučaj, da reafirmacija ideologije in "obnova vrednot" v razmerju
> do intelektualnega dela vse bolj poteka skozi prilaščanje posameznih Marxovih
> razdelav, ločenih od njihovega konkretnega konteksta kritike politične
> ekonomije in kritike filozofije. Obnova "mladega Marxa" predstavlja
> privilegiran moment preobrata ideologije v "znanost", pravzaprav, v splošno
> teorijo epistemologije.

::: {lang=it}

> Non è comunque a caso che la riaffermazione dell'ideologia e il "recupero di
> valore" relativo al lavoro intellettuale, passino sempre più attraverso
> l'appropriazione di singole elaborazioni marxiane, scorporate dal contesto
> concreto della critica dell'economia politica e della critica della filosofia.
> Il recupero del "Marx giovane" rappresenta il momento privilegiato del
> rovesciamento dell'ideologia nella "scienza", anzi, nella teoria generale
> dell'epistemologia.

:::

:::

::: alts

> V zvezi s tem bi lahko govorili o *preudarnosti ideologije*. Ker se zanika kot
> "realnost", izpodbija lastno veljavnost kot forma zavesti, se lahko vedno
> znova obnovi kot zadnji produkt progresivnega in radikalnega "očiščenja".
> Althusser piše[^43]: "Ideologija je sistem (ki poseduje lastno logiko in
> lastno togost) reprezentacij (podobe, miti, ideje ali koncepti, odvisno od
> primera), ki ima v dani družbi določeno eksistenco in določeno zgodovinsko
> funkcijo. Ne da bi se spuščali v problem razmerja, ki jih ima neka znanost do
> svoje (ideološke) preteklosti, lahko rečemo, da se ideologija kot sistem
> reprezentacij razlikuje od znanosti po tem, da v njej praktično-družbena
> funkcija prevladuje nad teoretsko (ali funkcijo znanja)".

::: {lang=it}

> Potremmo parlare, al proposito, di un'*astuzia dell'ideologia*. Negandosi come
> "realtà", contestando la propria validità come forma di conoscenza, essa può
> sempre recuperarsi come prodotto ultimo di una "depurazione" progressiva e
> radicale. "Un'ideologia -- scrive Althusser[43] -- è un sistema (che
> pos/273/siede la propria logica e il proprio rigore) di rappresentazioni
> (immagini, miti, idee o concetti, secondo i casi) dotate di un'esistenza e di
> una funzione storica nell'ambito di una data società. Senza entrare nel
> problema dei rapporti che una scienza ha col suo passato (ideologico), diciamo
> che l'ideologia come sistema di rappresentazioni si distingue dalla scienza
> per il fatto che in essa la funzione pratico-sociale prevale sulla funzione
> teorica (o funzione di conoscenza)".

:::

:::

[^43]: L. Althusser, Pour Marx, ital. prev. Roma, Editori Riuniti, 1967, str.
    207.

::: alts

> Tako lahko Vacca zaključi s kritiko zlitja, pri Althusserju, teoretske
> demistifikacije ideologije socialističnega humanizma in stalnosti buržoazne
> ideologije, ki je reducirana na instrument praktično-politične
> intervencije[^44].

::: {lang=it}

> Così che il Vacca può concludere criticando la confluenza, in Althusser, di
> demistificazione teorica dell'ideologia dell'umanesimo socialista e di
> permanenza dell'ideologia borghese, ridotta a strumento di intervento
> pratico-politico[44].

:::

:::

[^44]: "Zato poleg 'resnice' teoretske demistifikacije ideologije (v tem
    primeru) socialističnega humanizma sobiva humanistična ideologija in
    opravlja praktično nalogo, ki je tudi pozitivna. Teoretska praksa se opira
    na 'resnice', ki niso povezane s historično prakso; slednja pravilno posvoji
    in uporabno ohranja ideologije, ki so v teoretski praksi preprečene. Tako s
    pritrditvijo trajnosti ideologije zaradi njene možnosti, da v vsakokratni
    družbi opravlja praktično-politične naloge, Althusser nasprotuje temeljnemu
    *materialističnemu* prizadevanju historičnega materializma, to je znanstveno
    utemeljiti politično prakso" (G. Vacca, *Louis Althusser: materialismo
    storico e materialismo dialettico*, v "Angelus novus", 12-13 1968, zdaj v
    *Marxismo e analisi sociale*, Bari, De Donato, 1969, str. 45--67: navedek je
    na str. 67).

<!--

[^44]: "Dunque -- scrive il Vacca -- accanto alla 'verità' della
    demistificazione teorica dell'ideologia (in questo caso) dell'umanesimo
    socialista, convive l'ideologia umanistica ed assolve ad un ufficio pratico
    anche positivo. La pratica teorica attinge 'verità' irrelate alla pratica
    storica; quest'ultima adopera a buon diritto e perpetua utilmente ideologie
    vanificate in sede di pratica teorica. Affermando la permanenza
    dell'ideologia -- egli aggiunge -- per la sua possibilità di assolvere, in
    qualunque società, ad uffici pratico-politici, Althusser contraddice così
    una fondamentale aspirazione *materialistica* del materialismo storico,
    quella di fondare scientificamente la pratica politica" (G. Vacca, *Louis
    Althusser: materialismo storico e materialismo dialettico*, in "Angelus
    novus", 12-13 1968, ora in *Marxismo e analisi sociale*, Bari, De Donato,
    1969, pp. 45--67: la citazione è a p. 67).

-->

::: alts

> Toda to se ne zdijo osrednja vozlišča novih mistifikacij o pozitivni vlogi
> kritike ideologije. Korenine problema so povsem v relativni potrditvi
> "irealnosti" ideologije. Razlika med *irealnostjo* in *neučinkovitostjo* je
> pravzaprav subtilna. Če damo prednost prvi razlagi je skoraj neizogibno, da
> prispemo naravnost do zaključka o *razpoložljivosti* ideologije: sledi --
> dobro pazite --, da je razpoložljivost toliko večja, kolikor bolj radikalna in
> globoka je znanstvena "kritika" ideologije same, ki jo preoblikovano v
> pozitivno teorijo "ponovno umesti" v svet.
>  
> ::: {lang=it}

Ma non ci sembra che sia qui il nodo centrale delle nuove mistificazioni sul
ruolo positivo della critica dell'ideologia. La radice del problema è tutta
nell'affermazione relativa all'"irrealtà" delle ideologie. La differenza fra
*irrealtà* e *ineffettualità* è infatti sottile. Privilegiando la prima
interpretazione è quasi inevitabile giungere direttamente a concludere con la
*disponibilità* dell'ideologia: e si tratta -- si badi bene -- di una
disponibilità tanto maggiore quanto più radicale e profonda è stata la
"critica" scientifica dell'ideologia stessa, che la "restituisce" al mondo
trasformata in teoria positiva.

:::

:::

::: alts

> "Kritika" -- in seveda ne samo pri Althusserju, ki smo ga navedli zgolj kot
> patološki primer posplošene obnove intelektualnega dela kot univerzalno
> "potrebnega" momenta -- se tako predstavi kot popolnoma obrnjena v razmerju do
> specifičnih ciljev, ki so ji pripisani v *Nemški ideologiji* ali v *Bedi
> filozofije*. Kar na čisto teoretski ravni seveda ni tako resno, kot pa
> pokazatelj politične tendence.

::: {lang=it}

> La "critica" -- e non solo, certo, in Althusser, che abbiamo citato unicamente
> come caso patologico del generalizzato recupero di un lavoro intellettuale
> come momento universalmente "necessario" -- si presenta quindi completamente
> rovesciata rispetto ai fini specifici ad essa attribuiti nell'*Ideologia
> tedesca* o nella *Miseria della Filosofia*. Il che non è, certo, tanto grave
> sul piano puramente teorico, quanto come indice di una tendenza politica.

:::

:::

::: alts

> Na eni strani razvrednotenje, depreciacija, marginalizacija ideologije v
> klasičnem pomenu besede. Na drugi strani njena ponovna vpeljava, njena obnova,
> njena ponovna funkcionalizacija kot implicitni moment v konkretnih
> disciplinah: kar nas bolj zanima, njena funkcionalizacija kot *implicitno*
> jedro (ki se ne razkrije kot tako, torej, povsem prevedeno v *tehniko*
> intervencije) disciplin plana. "Kritika" se v tem procesu predstavlja v dveh
> svojih povsem komplementarnih obrazih: prvič kot razkritje *bede ideologije*,
> drugič kot magični reagent, ki v *tehnikah plana* raztopi očiščene ostanke
> intelektualnega dela.

::: {lang=it}

> /274/Da un lato la svalutazione, il deprezzamento, la messa al margine
> delle ideologie nel senso classico del termine. Dall'altro la loro
> riintroduzione, il loro recupero, la loro rifunzionalizzazione come momento
> implicito nelle discipline concrete: per quanto più ci interessa, la loro
> funzionalizzazione come nucleo *implicito* (non rivelato come tale, quindi,
> tutto tradotto in *tecnica* di intervento) alle discipline del Piano. La
> "critica" si presenta in tale processo nelle sue due facce del tutto
> complementari: una prima volta come rivelazione della *miseria
> dell'ideologia*, una seconda volta come reagente magico che risolve in
> *tecniche di piano* i residui purificati del lavoro intellettuale.

:::

:::

::: alts

> Smo točno znotraj tega pomena ideološke kritike, pri katerem vztrajajo zadnji
> epigoni frankfurtske sociološke šole.

::: {lang=it}

> Siamo esattamente all'interno di quell'accezione della critica ideologica che
> gli ultimi epigoni della scuola sociologica di Francoforte propongono con
> insistenza.

:::

:::

::: alts

> Četudi ni v celoti priznano, ideološke deformacije, ki določajo politične
> odločitve večjega dela organizacij delavskega gibanja -- in celo precejšnjega
> dela izvenparlamentarne opozicije --, niso nič drugega kot razvoj utopij
> Linkskomunismusa ali "sociološke levice". Neprestano se znajdemo pred pojavom
> s katerim se je treba soočiti: kapital iz sebe ideologijo izloča kot
> "nevarnost" za učinkovanje njegovih procesov racionalizacije, "razredne"
> organizacije in intelektualci pa si te razprodane instrumente prisvajajo.

::: {lang=it}

> Per quanto in modo non del tutto confessato, le deformazioni ideologiche che
> determinano le scelte politiche di gran parte delle organizzazioni del
> movimento operaio -- e persino di consistenti frange dell'opposizione extra
> parlamentare -- sono nient'altro che lo sviluppo delle utopie del
> Linkskomunismus o della "sinistra sociologica". Ci troviamo costantemente di
> fronte a un fenomeno da fronteggiare: il capitale espelle da sé l'ideologia
> come "pericolo" per l'efficienza dei propri processi di razionalizzazione,
> mentre le organizzazioni e gli intellettuali "di classe" si appropriano di
> quello strumento svenduto.

:::

:::

::: alts

> "Poslanstva" buržoaznega intelektualca je tako ponovno strnjeno v "razrednem
> intelektualcu". Marksizem mladega Lukàcsa, ki je obnovil hegeljansko branje
> Marxa, je vse prej kot osamljen primer: opredeljuje stalen način delovanja, ki
> je toliko bolj resen, kolikor bolj je uspešen pri prodiranju v uradne politike
> delavskih strank. Intelektualno delo kot "projekt" "socialističnega
> humanizma", boj, ki ga posredujejo subjektivne emancipacije, enačenje
> socializma in globalnega plana razvoja, ki se uveljavlja in nadzoruje na ravni
> družbe, reprodukcija ideologije dela, težnja -- ki jo posredujejo
> intelektualci -- po ponovni osvojitvi *človeške totalitete* in končno projekt
> *alternativnih* tehnik in znanosti (ali alternativne uporabe znanosti in
> tehnik): mnogi ideološki instrumenti, ki so bili vzpostavljeni za legitimacijo
> smeri razvoja "socializma v eni državi", zdaj delujejo v organiziranem
> delavskem gibanju[^45]. Ideologizacija marksizma se torej s kapitalističnega
> stališča predstavlja kot instrument nasprotne strani za upravljanje rešitve
> zaostalih problemov razvoja: v tem smislu se objektivno postavlja kot eden
> nadzidavnih sredstev "realizacije dialektike", funkcionalizacije lastne
> negativnosti.

::: {lang=it}

> La "missione" dell'intellettuale borghese viene così riassunta in proprio
> dall'"intellettuale di classe". Il marxismo del giovane Lukács, nel suo
> recupero di una lettura hegeliana di Marx, è tutt'altro che un caso isolato:
> esso definisce un modo di azione costante, tanto più grave quanto più esso è
> riuscito a penetrare nelle politiche ufficiali dei partiti operai. Il lavoro
> intellettuale come "progetto" dell'"umanesimo socialista", la lotta mediata da
> emancipazioni soggettive, l'identificazione di socialismo e piano globale di
> sviluppo, promosso e controllato a livello sociale, la riproduzione
> dell'ideologia del lavoro, la riconquista tendenziale -- mediata dagli
> intellettuali -- della *totalità umana*, il progetto, infine, di scienze e
> tecniche *alternative* (o l'uso alternativo delle scienze e delle tecniche):
> non pochi degli strumenti ideologici messi in opera /275/come
> legittimazione delle linee di sviluppo del "socialismo in un paese solo", sono
> oggi operanti in seno al movimento operaio organizzato[45]. L'ideologizzazione
> del marxismo si presenta quindi, dal punto di vista capitalistico, come
> strumento per far gestire alla controparte la soluzione dei problemi arretrati
> dello sviluppo: in tal senso essa si pone oggettivamente come uno dei mezzi
> sovrastrutturali di "realizzazione della dialettica", di funzionalizzazione
> del proprio negativo.

:::

:::

[^45]: Mario Tronti je v za nas temeljnem eseju zapisal: "Pred nami ni več
    velikih abstraktnih sintez buržoazne misli, ampak kult najvulgarnejše
    empirije kot prakse kapitala; ne več logični sistem vednosti, znanstveni
    principi, ampak neurejena masa zgodovinskih dejstev, posamičnih izkušenj,
    velikih izvršenih dejanj, ki jih nihče nikoli ni *mislil*. Znanost in
    ideologija se znova združujeta in si nasprotujeta, toda ne več v
    sistematizaciji idej za večno, temveč v vsakdanjih dogodkih razrednega boja
    [...] Ves funkcionalni aparat buržoazne ideologije je kapital predal v roke
    uradno priznanega delavskega gibanja. Kapital ne upravlja več z lastno
    ideologijo, z njo upravlja delavsko gibanje [...] Zato trdimo, da je danes
    kritika ideologije naloga interna delavskemu stališču in se tiče kapitala
    samo v drugi instanci" (M. Tronti, *Marx, forza-lavoro, classe operaia*, v
    *Operai e capitale*, Torino 1966, str. 171\. Toda gl. tudi str. 177 in
    dalje).

<!--

[^45]: "Di fronte a noi -- ha scritto Mario Tronti, in un saggio per noi
    fondamentale -- non più le grandi sintesi astratte del pensiero borghese, ma
    il culto della più volgare empiria come prassi del capitale; non più il
    sistema logico del sapere, i principi della scienza, ma una massa senza
    ordine di fatti storici, di esperienze staccate, di grandi azioni compiute
    che nessuno mai ha *pensato*. Scienza e ideologia di nuovo si fondono e si
    contraddicono, non più però in una sistemazione delle idee per l'eterno, ma
    negli eventi quotidiani della lotta di classe ... Tutto l'apparato
    funzionale dell'ideologia borghese è stato consegnato dal capitale nelle
    mani del movimento operaio ufficialmente riconosciuto. Il capitale non
    gestisce più la propria ideologia, la fa gestire al movimento operaio ...
    Ecco perché diciamo che oggi la critica dell'ideologia è un compito interno
    al punto di vista operaio, che solo in seconda istanza riguarda il capitale"
    (M. Tronti, *Marx, forza-lavoro, classe operaia*, in *Operai e capitale*,
    Torino 1966, p. 171\. Ma cfr. anche alle pp. 177 ss.).

-->

::: alts

> Iz stališča intelektualnega dela pa ima specifičen pomen: dopušča nadaljevanje
> "zgodovinskih" nalog, ki jih je kapitalistična racionalizacija istemu
> intelektualcu odvzela. Vsa reakcionarnost *socialističnega humanizma* kot
> "projekta" družbene realizacije "vrednote" se razkrije, ko sledimo temu
> projektu nazaj do teze, ki v *marksistični alternativi* želi takojšen preobrat
> teženj kapitala, ki se izvaja na podlagi najvišjih ravni buržoaznih razdelav.

::: {lang=it}

> Dal punto di vista del lavoro intellettuale, essa ha un significato specifico:
> permette la perpetuazione di compiti "storici" di cui la razionalizzazione
> capitalista ha espropriato l'intellettuale stesso. Tutta la reazionarietà
> dell'*umanesimo socialista* in quanto "progetto" di realizzazione sociale del
> "valore", si scopre quando si fa risalire quel progetto alla tesi che vuole
> nell'*alternativa marxista* il ribaltamento immediato delle linee di tendenza
> del capitale, attuato sulla base dei livelli massimi delle elaborazioni
> borghesi.

:::

:::

::: alts

> Tako sovpadata dve tipični utopiji: ni slučaj, da odrešitev utopije zavzame
> formo predloga "pozitivne alternative".

::: {lang=it}

> Due tipiche utopie vengono quindi fatte coincidere: non è a caso che il
> riscatto dell'utopia prenda forma nelle proposizioni dell'"alternativa
> positiva".

:::

:::

::: alts

> Ne bi bilo potrebno razpravljati o tako samoumevnih temah, ki se jih zlahka
> demistificira, če ne bi konkretno delovale kot instrumenti deviacije
> razrednega boja in če ne bi vzpostavljale ravni ideoloških zahtevkov na
> katerih številni obrobki intelektualnega dela tvegajo, da bodo postali zopet
> funkcionalni v trenutku socializacije plana. Vseeno pa je treba opozoriti, da
> projekt "emancipacije" skozi revolucioniranje znanosti in tehnike vztraja (in
> to prav kot *projekt*) na specifičnem področju buržoazne utopije. Zavračanje
> odčaranja nad možnostjo utopije se tako prevede v ideologijo sodelovanje
> razreda pri razvoju, kot njegov "ideološki motor": navsezadnje za preskok
> ovire objektivne tujosti neposrednih razrednih interesov do razvoja ni drugega
> instrumenta kot ideološka penetracija.

::: {lang=it}

> Non ci sarebbe bisogno di discutere temi così scontati e così facilmente
> demistificabili, se essi non agissero concretamente come strumenti di
> deviazione della lotta di classe, e se non costituissero dei livelli di
> rivendicazione ideologica su cui rischiano di attestarsi le molte frange del
> lavoro intellettuale ancora funzionali ai momenti di socializzazione del
> piano. Ciò che va comunque rilevato è che il progetto di "emancipazione"
> tramite il rivoluzionamento della scienza e della tecnica, insiste (e proprio
> in quanto *progetto*) sull'area specifica dell'utopia /276/borghese. Il
> rifiuto del disincantamento sulle possibilità dell'utopia, si traduce così in
> ideologia della partecipazione della classe allo sviluppo, come suo "motore
> ideologico": e del resto per saltare a piè pari l'ostacolo dell'estraneità
> oggettiva dei diretti interessi di classe allo sviluppo, non c'è altro
> strumento che la penetrazione ideologica.

:::

:::

::: alts

> Utopija in plan sta ponovno ločena: slednjemu gre vsa operativnost, prvi pa je
> odrejena vloga, ki jo lahko opredelimo kot "retorična". Načrt, ki ga je
> izračunal razvoj, nima več potrebe po ustvarjanju lastnih ideologij in
> legitimacij. Te so, kot vrednostni kriteriji, ovire v razmerju do
> esencialnosti tehnik odločanja: v kolikor ostajajo marginalno uporabnee, jih
> je treba izročiti opoziciji znotraj sistema ter jo pustiti v iluziji, da se je
> polastila orožja iz rok nasprotnikov.

::: {lang=it}

> Utopia e piano si presentano di nuovo disgiunti: al secondo va tutta
> l'operatività, alla prima è riservato un ruolo che potremmo definire
> "retorico". Il disegno calcolato dello sviluppo non ha più bisogno di generare
> da solo le proprie ideologie e le proprie legittimazioni. Queste, in quanto
> criteri di valore, sono intralci rispetto all'essenzialità delle tecniche
> decisionali: in quanto rimane in loro un'utilità marginale, esse vanno
> consegnate all'opposizione interna al sistema, lasciandole anche l'illusione
> di aver strappato armi già in mano all'avversario.

:::

:::

::: alts

> Seveda pa skrbi ne vzbuja taktični vidik tega procesa. Kar je treba enkrat za
> vselej razsvetliti je objektivna narava te razprodaje ideologije kot
> strukturnega momenta v razdelavi dinamičnih modelov, ki omogočajo
> makroekonomsko in makrodružbeno planiranje. Z drugimi besedami, prepoznati, da
> prilaščanje ideologije s strani razrednih organizacij ni toliko rezultat
> prepoznavanje sebe kot "kritične funkcije" razvoja -- utopično "tveganje" je
> dejansko vse bolj integrirano v samo razdelavo dinamičnih modelov, ki
> posredujejo celotno kapitalistično gospostvo --, temveč posledica obnove
> ideološko-institucionalne prtljage, ki teži ohromiti razredna gibanja.

::: {lang=it}

> Naturalmente non è l'aspetto tattico di tale processo a destare
> preoccupazioni. Ciò che va messo in chiaro, e una volta per sempre, è la
> natura oggettiva di tale svendita dell'ideologia come momento strutturale
> nell'elaborazione dei modelli dinamici che permettono la pianificazione
> macroeconomica e macrosociale. Bisognerà cioè riconoscere che l'appropriazione
> delle ideologie da parte delle organizzazioni di classe non è tanto il
> risultato di un riconoscersi come "funzione critica" dello sviluppo -- il
> "rischio" utopico, infatti, risulta sempre più integrato nella stessa
> elaborazione dei modelli dinamici che mediano il dominio capitalistico
> complessivo --, ma è piuttosto la conseguenza del recupero di un bagaglio
> ideologico-istituzionale che tende a paralizzare i movimenti di classe.

:::

:::

::: alts

> V zvezi s strukturo intelektualnega dela je treba opozoriti, da ta obnova
> predstavlja zadnjo rešilno bilko za obrambo privilegijev, ki so na poti k
> izumrtju. Preobrniti ves utopični naboj, vso "projektantsko" voljo, vso etično
> vsebino buržoaznega najstva v *socialistično* hipotezo v bistvu pomeni
> zaščititi institucionalne vloge intelektualca: pravzaprav vloge, ki so najbolj
> tuje procesom homogenizacije razreda: saj gre vedno za intelektualni statut,
> ki se skrbno oddaljuje od produktivnega dela, kadarkoli okoliščine to
> omogočajo.

::: {lang=it}

> Rispetto alla struttura del lavoro intellettuale, va osservato che quel
> recupero rappresenta l'ultima àncora di salvezza per la difesa di privilegi in
> corso di estinzione. Ribaltare tutta la carica utopica, tutta la volontà
> "progettante", tutti i contenuti etici del dover-essere borghese, nell'ipotesi
> *socialista*, significa in sostanza salvaguardare i ruoli istituzionali
> dell'intellettuale; anzi, i ruoli più estranei ai processi di omogeneizzazione
> della classe: dato che si tratta, sempre, di uno statuto intellettuale attento
> a riprendere le proprie distanze dal lavoro produttivo, ogni qualvolta la cosa
> è permessa dalle circostanze.

:::

:::

::: alts

> Zato so pred marskovsko kritiko ideologije nove naloge. Dejansko se ta kritika
> zdi vedno manj funkcionalna kot neposreden napad na instrumente kapitalistične
> organizacije: pri novih ravneh, ki jih dosega celoten plan, in še bolj pri
> tendencah, po katerih se giblje dinamika razvoja, se takšna politična naloga
> zdi kot, da se bije na zaostalih področjih, ali vsaj na področjih za katera je
> pričakovati, da bodo postala omejena. Poleg vloge, ki jo kritika ideologije
> lahko razvije pri historizaciji sedanje situacije, ostane zdaj jasno, da je
> njena najbolj pravilna točka uporabe to temeljno vozlišče, ki označuje prehod
> buržoazne ideologije v socialistični "projekt".

::: {lang=it}

> Esistono quindi compiti nuovi per una marxiana critica dell'ideologia. Sempre
> meno, infatti, tale critica appare fun/277/zionale a un attacco diretto
> contro gli strumenti di organizzazione capitalistica: i nuovi livelli
> raggiunti dal piano complessivo, e ancor più le linee di tendenza lungo le
> quali si va muovendo la dinamica dello sviluppo, fanno apparire un simile
> compito politico come battaglia svolta su aree arretrate, o almeno su aree che
> è prevedibile si restringano sempre più. A parte il ruolo che la critica
> dell'ideologia può svolgere nello storicizzare la situazione presente, risulta
> ormai evidente che il suo punto di applicazione più proprio è il nodo
> fondamentale che segna il passaggio dell'ideologia borghese nel "progetto"
> socialista.

:::

:::

::: alts

> Reducirati ves marksizem na utopijo, ga uporabiti kot instrument napovedovanja
> (in, pogosto ter prostovoljno, napovedovanja, ki vztrajajo celo na posameznih
> disciplinarnih področjih), posredovati njegovo revolucionarnost z neko
> sociološko rabo ali skozi humanitarno ideologijo: to so že dolgo časa -- vse
> od nesporazumov, ki jih je nazadnje sprožil celo Engels -- koraki, ki so
> nevtralizirali formiranje ustrezne razredne organizacije. Niti ni slučajno, da
> so med nosilci teh mitologij natančno preoblikovanih v politične prakse od
> vedno intelektualci, ki v socializmu iščejo vloge, ki jim jih je
> kapitalistična organizacija razvoja odrekla[^46].

::: {lang=it}

> Ridurre tutto il marxismo a utopia, servirsi di esso come strumento di
> anticipazioni (e, spesso e volontieri, di anticipazioni che insistono
> addirittura su singole aree disciplinari), mediarne la rivoluzionarietà con un
> uso sociologico o con ideologie umanitarie: questi sono stati, da tempo --
> dagli equivoci provocati dall'ultimo Engels, addirittura -- i passaggi che
> hanno neutralizzato il formarsi di un'organizzazione adeguata della classe. Né
> è un caso che fra gli alimentatori di tale mitologie, puntualmente trasformate
> in prassi politica, siano sempre intellettuali che cercano nel socialismo
> ruoli loro negati dall'organizzazione capitalistica dello sviluppo[46].

:::

:::

[^46]: Ni se slabo spomniti na: "Komunizem za nas ni *stanje*, ki naj bo
    vzpostavljeno, *ideal*, po katerem naj se ima ravnati dejanskost. S
    komunizmom pojmujemo *dejansko* gibanje, ki odpravlja zdajšnje stanje." (K.
    Marx, *Nemška ideologija*, v "Karl Marx, Friedrich Engels: Izbrana dela: II.
    zvezek", Ljubljana, Cankarjeva založba, 1976, str. 41).

<!--

[^46]: Non è male ricordarlo: "Il comunismo non è per noi uno *stato di cose*
    che debba essere instaurato, un *ideale* al quale la realtà dovrà
    conformarsi. Chiamiamo comunismo il movimento reale che abolisce (aufhebt)
    lo stato di cose presente" (K. Marx, *L'ideologia tedesca*, Roma, Editori
    Riuniti, 1958, p. 32).

-->

::: alts

> Napad na te mitologije mora biti neločljivo združen z analizo objektivnih
> vlog, ki so vse notranje kapitalističnemu planu, iz katerih je potrebno
> izhajati, da bi v politični praksi odpravili objektivne pogoje same, ki jih
> nalaga razvoj in ki se jim lahko izognemo le z mistifikacijo funkcije
> intelektualnega dela.

::: {lang=it}

> L'attacco che va sferrato contro tali mitologie deve unirsi indissolubilmente
> all'analisi dei ruoli oggettivi, tutti interni al piano capitalista, da cui è
> necessario partire per rovesciare in prassi politica le stesse condizioni
> oggettive che lo sviluppo impone, e cui non è dato sottrarsi che mistificando
> la funzione del medesimo lavoro intellettuale.

:::

:::

::: alts

> Zlahka lahko sklepamo -- tudi ker je že neposredno preverljivo -- da bodo
> procesi, ki jih povzroča širitev kapitalističnega gospostva nad globalnim
> razvojem, poenostavili in reducirali hierarhijo vlog povezanih z
> intelektualnim delom ter jasno ločili, od homogenizirane strukture funkcij
> nadzora in upravljanje posameznih faz ali posameznih sektorjev razvoja, vloge
> povezane z analizo in razdelavo ter dopustili minimalno potrebno rezervo za
> ideološko kontemplacijo.

::: {lang=it}

> È facilmente ipotizzabile -- anche perché è già direttamente verificabile --
> che i processi indotti dall'espansione del dominio capitalistico sullo
> sviluppo globale, semplificherà e ridurrà la gerarchia dei ruoli relativi al
> lavoro intellettuale, scindendo nettamente, dalla struttura omogeneizzata
> delle funzioni di controllo e gestione delle singole fasi o dei singoli
> settori dello sviluppo, i ruoli relativi all'analisi e all'elaborazione, e
> lasciando /278/in vita una riserva minima di necessaria contemplazione
> ideologica.

:::

:::

::: alts

> Skupku, ki je neposredno povezan s politično-ekonomskim področjem odločanja,
> se zoperstavlja sistem množičnega nadzora: proces, ki je vse prej kot neznan
> zgodovini kapitalistične organizacije produkcije, toda ki se zagotovo
> predstavlja v kvantitativno novih vidikih.

::: {lang=it}

> Alla fascia direttamente connessa con l'area decisionale politico-economica,
> si contrappone un sistema di controlli di massa: processo tutt'altro che
> ignoto nella storia dell'organizzazione capitalistica della produzione, ma che
> sicuramente va presentando aspetti quantitativamente nuovi.

:::

:::

::: alts

> "Dokument Pirelli" in "Projekt 80" sta popolnoma usklajena pri orisovanju
> perspektive povečanja procesov sektorske diverzifikacije in "dinamizma v
> artikuliranju podjetnih struktur in entitet", da bi nas te privedli do ravni
> držav, kot so ZDA, ki so že dosegle "prag programirane in permanentne
> inovacije". Toda hipoteza o izjemno koncentriranem kapitalu in z zelo visoko
> organsko kompozicijo je še vedno *model*[^47]. Trenutno zaznavni trendi
> kažejo, da se ob in *znotraj* perspektive cikla v nenehni in programirani
> dinamični situaciji v Italiji in Evropi odvija splošno prestrukturiranje
> delitve dela, ki v prvi vrsti pomeni krepitev sektorja, ki mu tradicionalno
> pravimo terciarni[^48].

::: {lang=it}

> Il "documento Pirelli" e il "Progetto 80" concordano perfettamente nel delineare come prospettiva l'allargamento dei processi di diversificazione settoriale e un "dinamismo nell'articolazione delle strutture e delle entità aziendali", tali da porsi al livello dei paesi, come gli Stati Uniti, che hanno già raggiunto la "soglia dell'innovazione programmata e permanente".
> Ma l'ipotesi di un capitale estremamente concentrato e ad altissima composizione organica è ancora un *modello*[47].
> Le linee di tendenza attualmente rilevabili indicano che accanto e *dentro* la prospettiva di un ciclo in perpetua e programmata situazione dinamica, in Italia e in Europa si sta attuando una ristrutturazione generale della divisione del lavoro che vede al primo posto il potenziamento del settore tradizionalmente detto terziario[48].

:::

:::

[^47]: Gl. "Mondo economico" 8 1970 (28 februar). V "documento Pirelli" je jasno
    pojasnjen cilj med izvozom povečati "kvoto produktov z visoko tehnološko
    vsebino in kvoto produktov tako imenovanih novih industrij". Pospešek ritma
    pridobivanja tehnično-znanstvenih inovacij in poudarjanje "dinamizma v
    artikuliranju podjetnih struktur in entitet" -- temelji dokumenta Pirelli --
    pomenita temeljito prestrukturiranje univerzitetnih in šolskih institucij na
    sploh v smislu prenovljene strukture kapitalistične delitve dela.

<!--

[^47]: Cfr. "Mondo economico" 8 1970 (28 febbraio). Nel "documento Pirelli" è
    chiaramente esplicitato l'obbiettivo di accrescere, tra le esportazioni, "la
    quota di prodotti ad alto contenuto tecnologico e la quota dei prodotti di
    industrie cosiddette nuove". L'accelerazione dei ritmi di acquisizione delle
    innovazioni tecnico-scientifiche e l'accentuazione del "dinamismo nei
    processi di articolazione delle strutture e delle entità aziendali" --
    cardini del documento Pirelli -- comportano una profonda ristrutturazione
    delle istituzioni universitarie e scolastiche in generale, nel senso di una
    rinnovata struttura della divisione capitalistica del lavoro.

-->

[^48]: V zvezi s glej okvirne napovedi glede strukture zaposlovanja, ki so
    vsebovane v *Progetto 80*.

<!--

[^48]: Cfr. al proposito le previsioni di massima relative alla struttura
    dell'occupazione, contenute nel *Progetto 80*.

-->

::: alts

> Izmenljivost, raznovrstnost, pripravljenost na interdisciplinarni dialog: to
> so nove značilnosti, ki se zahtevajo od splošne intelektualne delovne sile,
> katere proces pomasovljenja je že v trenutku njegove priprave. Vrste izredno
> razkrivajočih analiz so že prepoznale politično-ekonomski načrt, ki je podlaga
> stalni rasti storitvenega sektorja in sektorja upravnega in produktivnega
> nadzora[^49].

::: {lang=it}

> Intercambiabilità, polivalenza, preparazione al colloquio interdisciplinare:
> queste le caratteristiche nuove che vengono richieste a una generica
> forza-lavoro intellettuale, il cui processo di massificazione avviene già nel
> momento della sua preparazione. Una serie di analisi estremamente rivelatrici
> hanno già individuato il disegno politico-economico che sottostà all'aumento
> costante del settore dei servizi e del controllo amministrativo e
> produttivo[49].

:::

:::

[^49]: Sklicujemo se predvsem na temeljno poročilo A. Asor Rosa na *Convegno
    sulla formazione della forza-lavoro e sbocchi professionali* [*Konferenca o
    formaciji delovne sile in profesionalnih delovnih mest*], ki sta jo v Rimu
    29--30 maja 1970 organizirala univerzitetna sekcija PCI in univerzitetna
    skupina PSIUP. V svoji analizi Asor Rosa analizira nasprotujoče si napovedi
    o funkciji šolanja in poklicnega usposabljanja znotraj kapitalističnega
    razvoja, ki sta jih pripravila ISRIL in *Progetto 80*, ter iz njih opredeli
    dve različni hipotezi: prva temelji na ekstrapolaciji podatkov, ki se
    nanašajo na aktualno stopnjo rasti nacionalne ekonomije, druga na izrecno
    ekspanzivno hipotezo. (O raziskavi ISRIL gl. N. Cacace e M. D'Ambrosio,
    *Domanda e offerta di laureati in Italia*, priloga k "Futuribili" št. 2).
    Asor Rosa s političnega stališča presega splošne perspektive boja, ki
    izhajajo iz, sicer v bistvu pravilnih, analiz, ki so jih leta 1968 razdelale
    skupina Facoltà Tecnico-scientifiche del Movimento Studentesco di Roma (gl.
    *Scuola e sviluppo capitalistico*, v "Linea di massa" 3) in skupina
    "Manifesto". Gl. zlasti R. Rossanda, M. Cini, L. Berlinguer, *Tesi sulla
    scuola*, v "Il Manifesto" 2 1970, str. 20 in dalje; Lucio Magri, *Il
    movimento studentesco: linee per la ripresa*, ibidem, 5 1970, str. 49--53.

<!--

[^49]: Ci riferiamo principalmente alla fondamentale relazione di A. Asor Rosa
    al *Convegno sulla formazione della forza-lavoro e sbocchi professionali*,
    tenuto a Roma il 29--30 maggio 1970 a cura della Sezione universitaria del
    PCI e del Gruppo universitario del PSIUP. Nella sua analisi, Asor Rosa
    /279/analizza le contrapposte previsioni sulla funzione della scolarità e
    della preparazione professionale, in seno allo sviluppo capitalistico,
    elaborate rispettivamente dall'ISRIL e dal *Progetto 80*, individuando in
    esse due ipotesi differenti: la prima, basata sulla estrapolazione dei dati
    riferiti all'attuale tasso di crescita dell'economia nazionale, la seconda,
    riferita a un'ipotesi nettamente espansiva. (Sulla ricerca dell'ISRIL cfr.
    N. Cacace e M. D'Ambrosio, *Domanda e offerta di laureati in Italia*,
    supplemento al n. 2 dei "Futuribili"). Asor Rosa supera, dal punto di vista
    politico, sia le generiche prospettive di lotta scaturite dalle pur
    sostanzialmente corrette analisi elaborate nel '68 dalla missione delle
    Facoltà Tecnico-scientifiche del Movimento Studentesco di Roma (cfr. *Scuola
    e sviluppo capitalistico*, in "Linea di massa" 3), che quelle elaborate dal
    gruppo del "Manifesto". Cfr. in particolare R. Rossanda, M. Cini, L.
    Berlinguer, *Tesi sulla scuola*, in "Il Manifesto" 2 1970, pp. 20 ss.; Lucio
    Magri, *Il movimento studentesco: linee per la ripresa*, ibidem, 5 1970, pp.
    49--53.

-->

::: alts

> Ni potrebno -- celo napačno bi bilo -- prenesti analize homogenizacije
> abstraktnega dela v tovarni v analize novih vlog intelektualnega dela. Splošna
> usposobljenost neposredno produktivne delovne sile in njena funkcionalnost v
> hitro spreminjajočih se ciklih ali v nujno potrebni delavski mobilnosti, ki se
> uporablja kot instrument nadzora in umetne prerazdelitve, odgovarja potrebam
> in procesom s strani kapitalistov, ki so še vedno v večjem delu neodvisni od
> tistih, ki oblegajo prestrukturiranje metod priprave intelektualne delovne
> sile. (Če kaj, je dejstvo, da obstaja skupna stran teh dveh produkcijskih
> procesov, element na katerega se je treba politično odzvati po tem, ko smo ga
> pahnili).

::: {lang=it}

> /279/Non è necessario -- anzi sarebbe scorretto -- trasportare
> l'analisi relativa all'omogeneizzazione del lavoro astratto in fabbrica
> nell'analisi dei nuovi ruoli del lavoro intellettuale. La qualificazione
> generica della forza-lavoro direttamente produttiva e la sua funzionalità al
> rapido mutamento dei cicli o alla necessità di una mobilità operaia usata come
> strumento di controllo e artificiale risuddivisione, risponde a esigenze e
> processi di parte capitalista ancora in gran parte indipendenti da quelli che
> investono la ristrutturazione dei metodi di preparazione della forza lavoro
> intellettuale. (Caso mai, il fatto che esista un versante comune a tali due
> facce dei processi di produzione è un elemento da far reagire politicamente,
> dopo averlo fatto precipitare).

:::

:::

::: alts

> Treba pa je upoštevati objektivno dejstvo, ki bi ga bilo nevarno napačno
> razumeti: proces, ki danes, ne samo v Italiji, oblega šolstvo v svoji celoti,
> ne smemo nikakor brati neposredno kot splošno "dekvalifikacijo"
> intelektualnega dela. Nasprotno, gre za natančno prekvalifikacijo, četudi se
> predstavlja kot razlastitev družbenih privilegijev -- realnih ali
> mistificiranih -- v različnih sektorjih kjer se opravlja intelektualno delo.

::: {lang=it}

> Va intanto tenuto ben fermo un dato oggettivo su cui è estremamente pericoloso
> equivocare: il processo che oggi investe, e non soltanto in Italia, la scuola
> nel suo complesso, non va affatto letto come diretto a una generica
> "dequalificazione" del lavoro intellettuale. Al contrario, si tratta di una
> precisa riqualificazione, anche se si presenta come esproprio di posizioni di
> privilegio sociale -- reali o mistificate -- nei vari settori in cui si
> esplica il lavoro intellettuale.

:::

:::

::: alts

> Da lahko govorimo o novi kvalifikaciji dokazuje branje, ki ne izhaja iz
> subjektivnega stališča intelektualcev, ampak iz potreb, ki jih narekuje
> razvoj. Splošnost osnovne intelektualne priprave dejansko odgovarja celi vrsti
> ciljev, ki so vsi funkcionalni za prestrukturiranje organske sestave
> kapitala[^50].

::: {lang=it}

> Che sia lecito parlare di nuova qualificazione lo dimostra una lettura che non
> parta dal punto di vista soggettivo degli intellettuali, ma dalle necessità
> dettate dallo sviluppo. La genericità della preparazione intellettuale di base
> risponde infatti a una serie di obbiettivi tutti funzionali alla
> ristrutturazione della composizione organica del capitale[50].

:::

:::

[^50]: Gl. A. Asor Rosa, nav d., in M. Cacciari, *Qualificazione e composizione
    di classe: problemi generali*, v tem istem sklopu.

<!--

[^50]: Cfr. A. Asor Rosa, relazione cit., e M. Cacciari, *Qualificazione e
    composizione di classe: problemi generali*, in questo stesso fascicolo.

-->

::: alts

> Kakorkoli pa je govorjenje o proletarizaciji intelektualnega dela tout-court
> ter preskakovanje vseh realnih posredovanj skozi katere se organizirajo nove
> funkcije nadzora nad razrednimi gibanji in napačno pričakovanje rezultatov
> procesov, ki so vse prej kot dokončni ali linearni, lahko mistificirajoče.
> Bolj točno bi bilo prepoznati, da smo v vsakem primeru v prisotnosti nenehnega
> povečevanja odtujenosti intelektualca od lastnega dela, ki se realizira toliko
> globlje, kolikor bolj se slednje označuje prav kot "delo": bolj natančno,
> celo, kot mezdno delo. Kar pa ne pomeni, da že danes v najbolj avtomatiziranih
> sektorjih, kot na primer elektrotehnika, ne obstajajo primeri neposredno
> produktivnega intelektualnega dela. Iz tega se lahko ekstrapolira splošna
> tendenca in predvidi posledica nujnosti množičnega prenosa vlog, ki so
> namenjene uvrednotenju fiksnega kapitala, k neposredno produktivnim vlogam:
> toda s pridržkom, da nenehna rast terciarnega sektorja sledi procesu, ki teži
> vso družbo napraviti funkcionirati kot uvrednotevalni stroj kapitala.

::: {lang=it}

> /280/Tuttavia, parlare di proletarizzazione del lavoro intellettuale
> tout-court, saltando tutte le mediazioni reali attraverso le quali si
> organizzano le nuove funzioni di controllo sui movimenti di classe, e
> anticipando scorrettamente risultati di processi tutt'altro che definiti o
> lineari, può risultare mistificante. È più esatto riconoscere che, in ogni
> caso, siamo in presenza di un costante aumento dell'estraneità
> dell'intellettuale ai contenuti del proprio lavoro, che si realizza tanto più
> profondamente quanto più quest'ultimo si caratterizza esattamente come
> "lavoro": più esattamente, anzi, come lavoro salariato. Il che non toglie che
> già oggi, in settori altamente automatizzati, come quello dell'elettronica,
> esistano già esempi di un lavoro intellettuale direttamente produttivo. Si può
> estrapolare da ciò una tendenza generale, e prevedere di conseguenza la
> necessità di un trasferimento di massa dei ruoli tesi alla valorizzazione del
> capitale fisso a ruoli direttamente produttivi: con l'avvertenza, però, che
> l'aumento costante del terziario di fabbrica consegue a un processo che tende
> a far funzionare l'intera società come macchina di valorizzazione del
> capitale.

:::

:::

::: alts

> Razbrati v aktualnih pogojih intelektualnega dela neko konkretno tendenco k
> materialni homogenizaciji, ki poteka skozi kapitalistične procese družbenega
> in produktivnega prestrukturiranja, pomeni v pomasovljenju in v mobilnosti
> vlog, v izgubi tradicionalnih privilegijev rezerviranih za intelektualno delo,
> v oddaljenosti -- ki poteka že v fazi šolskih in univerzitetnih priprav -- od
> vsebine lastnega dela, v odtujenosti, katero je končno še intelektualec
> *primoran* doživljati znotraj kapitalistične organizacije dela, prepoznati
> nekatere *pozitivne* pogoje iz katerih lahko ponovno izhajamo za razdelavo
> programa napada na celotni plan.

::: {lang=it}

> Leggere nelle condizioni attuali del lavoro intellettuale una concreta
> tendenza verso un'omogeneizzazione materiale, che passa attraverso i processi
> di ristrutturazione sociale e produttiva capitalistici, significa riconoscere
> nella massificazione e nella mobilità dei ruoli, nella perdita dei privilegi
> tradizionali riservati al lavoro intellettuale, nel distacco -- che avviene
> già nella fase di preparazione scolastica e universitaria -- dai contenuti del
> proprio lavoro, nell'estraneità che finalmente anche l'intellettuale è
> *obbligato* a sperimentare nei confronti dell'organizzazione capitalistica del
> lavoro, alcune delle condizioni *positive* da cui ripartire, per elaborare un
> programma di attacco al piano complessivo.

:::

:::

::: alts

> Uporaba znanosti kot notranjega faktorja produktivnega cikla, neposredno
> produktivna uporaba znanstvene raziskave, se danes, v skrajnem primeru,
> kapitalu vsiljuje kot nujen instrument dinamike cikla in kot instrument
> prerazdelitve razreda in nadzora njihovih gibanj. Toda istočasno ta uporaba
> intelektualnega dela -- edina, ki nas, zaradi političnih ciljev, v tem
> trenutku zanima -- prinaša izpolnitev obsežne in neprestane razlastitve, ki jo
> izvaja kapital, ko pod svojim *znakom* reducira tisto, čemur se je izvorno
> zoperstavljal kot konkretno delo postavljeno pred abstraktno delo delavca:
> znanost je zdaj vsa objektivizirana *znotraj* razvoja.

::: {lang=it}

> L'uso della scienza come fattore interno al ciclo produttivo, l'uso
> direttamente produttivo della ricerca scientifica, al limite, si impongono
> oggi al capitale sia come strumenti necessari alla dinamica del ciclo, che
> come strumenti di risuddivisione della classe e di controllo dei suoi
> movimenti. Ma nello stesso tempo, tale uso del lavoro intellettuale -- l'unico
> che in questo momento ci interessi ai fini politici -- porta a compimento la
> vasta e continua operazione di esproprio condotto dal capitale, nel suo
> ridurre sotto il suo *segno* quello che si contrapponeva, /281/all'origine,
> come lavoro concreto posto di fronte al lavoro astratto operaio: la scienza è
> ora tutta oggettivata *dentro* lo sviluppo.

:::

:::

::: alts

> Ne verjamemo v ponavljajoče se izume novih *zaveznikov* delavskega razreda.
> Toda bilo bi samomorilsko, če ne bi prepoznali, da so prav smeri
> kapitalističnega razvoja tiste, ki za lastne namene ponovno sestavijo
> praviloma vse bolj homogeno delovno silo, ki je sposobna delovati pod znakom
> neposrednih interesov delavskega razreda. Odprava tistega, kar je, predolgo
> časa, bil kapitalistični načrt, ki kot svoj zaključek vidi *delavski razred
> organiziran po kapitalu*: to je cilj, ki ga je treba doseči z zastavljanjem
> naloge delavskega upravljanja subjektivnih zahtev novih slojev intelektualnega
> mezdnega dela.

::: {lang=it}

> Non crediamo alle ripetute invenzioni di nuovi *alleati* della classe operaia.
> Ma sarebbe suicida non riconoscere che sono le stesse linee dello sviluppo
> capitalista a ricomporre, ai propri fini, una forza-lavoro tendenzialmente
> omogenea, che è possibile far funzionare sotto il segno degli interessi
> diretti della classe operaia. Rovesciare quello che è stato, per troppo tempo,
> il disegno capitalista, quello che vede come proprio fine *una classe operaia
> organizzata dal capitale*: questo è l'obbiettivo da raggiungere ponendosi come
> compito la gestione operaia delle rivendicazioni soggettive dei nuovi strati
> di lavoro intellettuale salariato.

:::

:::

::: alts

> Toda to ni mogoče, če se ne premaga vsako reakcionarno iluzijo, vsak predlog,
> ki želi ponovno vzpostaviti profesionalno *dostojanstvo* tem "degradiranim"
> intelektualcem. Konkretno pokazati na reakcionarnost vsakega diskurza, ki želi
> ponuditi "alternativne" perspektive intelektualnemu delu, pomeni torej
> prepoznati, da so zgolj *znotraj* objektivnih vlog, ki jih vsiljuje gospostvo
> razvoja, pogoji za uporabo boja intelektualnih slojev, ki so neposredno vpiti
> v produkcijo, v celostnem napadu na plan kapitala: kar v bistvu pomeni
> razširiti politično rabo boja *za* mezdo na vse širše družbene sloje.

::: {lang=it}

> Ma ciò non è possibile se non battendo ogni illusione reazionaria, ogni
> proposta tesa a restituire *dignità* professionale a quegli intellettuali
> "degradati". Mostrare in concreto la reazionarietà di ogni discorso che voglia
> offrire prospettive "alternative" al lavoro intellettuale, significa quindi
> riconoscere che solo *all'interno* del ruolo oggettivo imposto dal dominio
> dello sviluppo è la condizione per utilizzare la lotta dei ceti intellettuali
> assorbiti direttamente nella produzione, in un attacco complessivo al piano
> del capitale: il che significa, essenzialmente, estendere l'uso politico della
> lotta *sul* salario a strati sociali sempre più ampi.

:::

:::

::: alts

> Vmesni cilj odprave vseh posledic kapitalističnega prestrukturiranja samega
> kapitala vodi nazaj k splošni temi organizacije razreda, ki smo jo v tej
> reviji že večkrat obravnavali.

::: {lang=it}

> L'obbiettivo intermedio, il rovesciamento di tutto il costo della
> ristrutturazione capitalista sul capitale stesso, riconduce al tema generale
> dell'organizzazione di classe, a più riprese affrontato da questa rivista.

:::

:::

::: alts

> In zato edini cilj h kateremu mora stremeti kritika ideologije ali kritika
> plana ni drugega kot nova oblika razredne organizacije, ki je sposobna
> premagati, zdaj brez utopije, brez takoj preverljivih "alternativnih modelov",
> brez subjektivnih pričakovanj konkretno realnost programirane reorganizacije
> kapitala.

::: {lang=it}

> Ed è per questo che l'unico obbiettivo cui far tendere la critica
> dell'ideologia o la critica del piano, non è altro che una nuova forma di
> organizzazione della classe, capace di battere, senza più utopie, senza alcun
> "modello alternativo" immediatamente sperimentabile, senza anticipazioni
> soggettive, la concreta realtà della riorganizzazione programmata del
> capitale.

:::

:::

---
lang: sl
references:
- type: article-journal
  id: tafuri1970lavoro
  author:
  - family: Tafuri
    given: Manfredo
  title: "Lavoro intellettuale e sviluppo capitalistico"
  title-short: "Lavoro intellettuale"
  container-title: "Contropiano: materiali marxisti"
  issue: 2
  issued: 1970
  page: 241-281
  language: it
# vim: spelllang=sl,it,de
...
