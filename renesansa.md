---
title: "Renesansa"
description: |
  Neurejeni izpiski in zapiski o renesančni arhitekturi kot začetku moderne arhitekture.
...

<!--

Nenazadnje je za številne zgodovinarje konstitutivni moment moderne arhitekture "tehnični čudež", kjer se nasproti srednjeveški *mehaniki*, ki jo vodi tradicija, prvič dokaže arhitektura kot intelektualna disciplina.
[@argan1946thearchitecture, 105-107]
Brunelleschi (1377--1446) se je med gradnjo florentinske katedrale in zasnovo kupole dokazal s tehnološko novo rešitvijo problema lesenih opažev, ki v tem primeru domnevno zaradi velikih dimenzij niso bili izvedljivi, tako da je bila celotna kupola na vsaki stopnji izgradnje samonosilna in samouravnalna.
Z uvedbo novih zidarskih tehnik in aparatov, s katerimi se je odmikal od starih gradbenih praks, je "racionaliziral tehniko in načine gradbene proizvodnje, prekinil kontinuiteto kolektivne organizacije tradicionalnega gradbišča in naglo postavil v ospredje temo moderne družbene delitve dela".
Rešitev zasnove kupole na način, da je ni potrebno pred gradnjo centrirati ter med gradnjo podpirati, odpravi privilegije mojstrov saj se ne zanaša več na njihovo tradicionalno vednost ter uveljavi avtonomijo arhitekta saj zdaj edini razpolaga s *projektom*, nizom racionalnih dejanj, ki so ločeni in neodvisni od same izvedbe na gradbišču.
Ta ločitev zasnove in izvedbe prav tako omogoča, da gradbišče lažje prenese zaustavitve del in menjave delovnih ekip.
Revolucijo v proizvodnih odnosih, katere neposredno ozadje je tudi razredni boj med oslabljenimi cehi in trgovskimi rodbinami, dokazujeta dogodka povezana z izgradnjo kupole.
Po tem, ko je bil Brunelleschi zaradi neplačevanja cehovske članarine aretiran, so bili cehi in mestne oblasti zaradi njegove nepogrešljivosti na gradbišču vseeno prisiljene ga izpustiti.
Izgubo tradicionalnih funkcij starih delavcev pa dokazuje izid stavke na gradbišču, ki jo je arhitekt lahko strl z najemom povsem druge in cenejše delovne sile, kar je izsililo, da so tudi stavkajoči sprejeli slabše pogoje.
[@aureli2023architecture, 35-41]

-->

## Formiranje novega jezika

::: alts

> Brunelleschi (1377--1446), dedič velike predhumanistične tehnološke tradicije, je v ospredje mestnega življenja stopil med tekmovanjem za kupolo katedrale.
> Marca 1418, ko je bil razpisan prvi natečaj, je bil Giovanni d'Ambrogio mojster gradnje; toda modeli Arnolfa, Talentija in Giovannija di Lapo Ghini za kupolo so se zdaj zdeli anahronistični: zmaga Brunelleschija in Ghibertija -- najprej razpisanega za drugorazredni natečaj, nato združenega v podjetju za kupolo -- je pomenila dokončno izbiro florentinske kulturne politike.
> Postopna prevlada Brunelleschija nad Ghibertijem, mojstrom, ki ga je pred sedemnajstimi leti premagal na natečaju za vrata baptisterija, je zdaj dobila natančen pomen.
> Končalo se je ne le obdobje obrtnih cehov, temveč tudi obdobje enakopravnih partnerstev, ki so temeljila na kompromisnih ali splošnih "delavniških" sporazumih.
>
> Brunelleschi je s tehnološko novo rešitvijo problema reber sklepnika, tako da bi bila celotna streha samonosna v vsaki fazi dokončanja, in z uvedbo zidarskega aparata "spinapesce", ki se je odvračal od antične gradbene prakse, racionaliziral tehniko in načine gradbene proizvodnje, prekinil kontinuiteto kolektivne organizacije tradicionalnega gradbišča in naglo postavil v ospredje temo moderne družbene delitve dela.
> Brunelleschi je s tem, ko je celoten postopek projektiranja in celoten tehnološki program povzel v svojo pristojnost, zagotovil integracijo formalne racionalnosti s strukturno racionalnostjo v arhitekturno izkušnjo.
> Veliki "stroj" kupole, ki se meri z razsežnostmi pokrajine, na različne načine vpliva na podobo mesta glede na številne zaznavne situacije in se s svojimi osmimi jadri in rebri, ki določajo njegovo geometrijsko skladnost, neizpodbitno uveljavlja kot glavni mestni prostor, je simbol nove državljanske vrednosti: Alberti, ko pravi, da je "tako velika, da bi s svojo senco prekrila vse toskanske ljudi", ostro razume njen poseben pomen.
>
> Revolucija, izvedena v proizvodnih odnosih, je najusodnejši odgovor, ki ga je arhitektura lahko dala humanistični ideologiji.
> Arhitekt intelektualec se loči od kolektivne proizvodnje.
> S tem, ko zahteva avtonomijo svoje vloge, se postavi v ospredje novih vladajočih razredov; tako zelo, da lahko z njimi celo stopi v konflikt, kadar ti niso pripravljeni biti popolnoma skladni z njegovimi lastnimi izhodišči (in to pojasnjuje nasprotja med Brunelleschijem in Cosimo de' Medici).
>
> Toda kar zadeva konkretno jezikovna vprašanja, kako se lahko spričo razpršenih poskusov znotraj florentinske tradicije prejšnjih stoletij postavi problem novega jezika?
> Brunelleschi pri odgovoru na to vprašanje naredi radikalno revolucijo.
> Trije parametri, ki so jim kritiki podredili njegovo arhitekturo -- obnova antike, tehnološki izum, perspektivna prostorskost --, so glasovi enotnega likovnega koda, nove jezikovne strukture, novega načina poseganja na prizorišče človeških dejanj, ki bo več kot štiri stoletja obvladoval zahodno umetnost.
>
> Racionalnost perspektivnega pogleda, ki jo je eksperimentalno raziskal Brunelleschi v znamenitih tablah, ki prikazujejo piazze Signorije in baptisterija, vzpostavlja novo intelektualno konstrukcijo prostora.
> Celotna naravna stvarnost je zdaj podvržena strogemu kodeksu umetnih odnosov, ki lahko odnose med predmeti izpostavi kot samostojno spoznavno vrednost; perspektivna konstrukcija posledično vsakemu predmetu določi enoznačno vlogo v mreži novega normaliziranega prostora.
> To pomeni redukcijo neskončne raznolikosti vidnih oblik na nekaj kanoničnih elementov.
> Zaradi racionalizacije, ki jo zagotavlja nova "merljivost" prostora, enotna kakovost "stvari" ne predstavlja več "vrednosti", temveč oviro, nekoristen presežek, ki ga je treba odpraviti ali nadzorovati v skladu s protikorporativno polemiko, ki poteka drugje: normalizacija dekorativnih strani nenadoma naredi anahronistično iznajdljivost in ustvarjalno svobodo, ki sta bili že prej privilegij kamnosekov, kiparjev in dekoraterjev, ki so tako izgubili svoje tradicionalne funkcije.
> "Obnova klasicizma" ima natančen pomen: zagotavlja popolno razpoložljivost normaliziranega niza elementov, ki so med seboj povezani s čisto metrično gramatiko, tako da je uporaba posameznih elementov stalna, njihova individualizacija konceptualno drugotna, njihova sestava pa eksperimentalna in preverljiva.
[@tafuri1972larchitettura, 17-21]

::: {lang=it}

> Erede della grande tradizione tecnologica preumanistica, il Brunelleschi (1377--1446) si pone in posizione di primo piano nella vita cittadina in occasione del concorso per la cupola del duomo.
> Nel marzo 1418, quando si bandisce il primo concorso, è capomastro Giovanni d'Ambrogio; ma i modelli dell'Arnolfo, del Talenti e di Giovanni di Lapo Ghini per la cupola appaiono ormai anacronistici: la vittoria del Brunelleschi e del Ghiberti -- chiamati in un primo momento a un concorso di secondo grado, poi associati nell'impresa della cupola -- segna una scelta definitiva per la politica culturale fio rentina.
> Anzi, ora il progressivo prevalere del Brunelleschi sul Ghiberti, il maestro che lo aveva battuto diciassette anni prima circa nel concorso per le porte del Battistero, assume un significato preciso.
> Non è solo finita l'epoca delle corporazioni artigiane, ma anche quella delle collaborazioni paritetiche basate sul compromesso o su generici accordi "di bottega".
>
> Dando una soluzione tecnologicamente nuova al problema delle centine della volta, in modo che l'intera copertura risulti autoportante in ogni fase del suo compimento, e introducendo l'apparecchiatura muraria "a spinapesce" detratta dalla prassi costruttiva antica, Brunelleschi razionalizza la tecnica e i modi della produzione edilizia, spezza la continuità dell'organizzazione collettiva del cantiere tradizionale, fa emergere impetuosamente il tema della moderna divisione sociale del lavoro.
> Riassumendo nelle proprie competenze l'intero processo ideativo e l'intero programma tecnologico, Brunelleschi garantisce all'esperienza architettonica l'integrazione della razionalità formale con quella strutturale.
> La grande 'macchina' della cupola, misurandosi con le dimensioni paesistiche, incidendo variamente nell'immagine cittadina a seconda delle molteplici situazioni percettive, affermandosi perentoriamente come luogo urbano primario con le sue otto vele e le costolanature che ne precisano la consistenza geometrica, è il simbolo di un nuovo valore civico: l'Alberti, dicendola "ampla da coprire con sua ombra tutti i popoli toscani", ne coglie acutamente il significato specifico.
>
> La rivoluzione compiuta all'interno dei rapporti di produzione è la risposta più conseguente che l'architettura potesse dare all'ideologia umanistica.
> L'intellettuale-architetto si separa dalla produzione collegiale.
> Rivendicando l'autonomia del proprio ruolo, esso si pone all'avanguardia delle nuove classi al potere; tanto da poter persino entrare in conflitto con esse là dove queste non siano disposte ad essere conseguenti fino in fondo con le proprie premesse (e ciò spiega i contrasti fra Brunelleschi e Cosimo de' Medici).
>
> Ma rispetto alle tematiche specificamente linguistiche, di fronte agli sparsi tentativi compiuti nell'ambito della tradizione fiorentina dei secoli precedenti, in che modo può porsi il problema del nuovo linguaggio?
> Brunelleschi compie, nel rispondere a tale interrogativo, una radicale rivoluzione.
> I tre parametri cui la critica ha sottoposto la sua architettura -- il recupero dell'Antico, l'invenzione tecnologica, la spazialità prospettica -- sono le voci di un unico codice figurativo, di una nuova struttura linguistica, di un nuovo modo di intervento sulla scena delle azioni umane, destinati a dominare per più di quattro secoli l'arte occidentale.
>
> La razionalità della visione prospettica, ricercata sperimentalmente dal Brunelleschi nelle famose tavolette raffiguranti le piazze della Signoria e del Battistero, fissa una nuova costruzione intellettuale dello spazio.
> L'intera realtà naturale è ora sottoposta a un rigoroso codice di relazioni artificiali, capace di far risaltare le relazioni tra gli oggetti come valore conoscitivo in senso proprio; la costruzione prospettica, di conseguenza, fissa un ruolo univoco a ogni oggetto nel reticolo del nuovo spazio normalizzato.
> Ciò implica la riduzione dell'infinità varietà delle forme visibili a pochi elementi resi canonici.
> Di fronte alla razionalizzazione assicurata dalla nuova 'misurabilità' dello spazio, la singola qualità delle 'cose' non rappresenta più un 'valore', ma caso mai un impaccio, un sovrappiù inutile da eliminare, o, meglio, da controllare in stretta concordanza con la polemica anticorporativa condotta in altra sede: la normalizzazione dei partiti decorativi rende di colpo anacronistica l'inventività e la libertà creativa già appannaggio di scalpellini, scultori e decoratori, che vedono così perdere le loro funzioni tradizionali.
> Il "recupero del Classicismo" ha un preciso significato: esso assicura la piena disponibilità di una serie normalizzata di elementi connessi fra loro da una grammatica puramente metrica, tale da rendere costante l'uso dei singoli elementi, concettualmente secondaria la loro individualizzazione, sperimentale e verificabile il loro montaggio.

:::

:::

## Problemi renesanse

### Arhitektura in mesto

::: alts

> Pomembno je poudariti globoko realistično naravo humanistične teme mesta od samega začetka.
> Humanizem namreč spričo eshatologij in tisočletnih pričakovanj srednjega veka potrjuje konkretni, družbeno in ekonomsko pogojeni značaj mestnega življenja in razvoja mest: "perspektivno mesto" Brunelleschija in Albertija je resnično mesto z vso kompleksnostjo njegovih zgodovinskih razslojenosti, v katerem novi perspektivni "objekti" uvajajo novo človeško vedenje -- posvetno in racionalno -- veliko bolj kot predlog globalnega fizičnega prestrukturiranja.
> V *De re aedificatoria* Alberti jasno sprejema dialektiko med posamezno stavbo, v kateri se lahko humanistični racionalizem in historizem v celoti izrazita, in mestom, ki je okvir različnih zaporednih posegov.
> Pri tem je Alberti veliko bolj aristotelski, kot bi lahko sklepali po njegovi slavni opredelitvi mesta kot "velike hiše".
[@tafuri1969rinascimento, 223]

::: {lang=it}

> È importante insistere, a proposito della tematica umanistica inerente alla
> città, sul profondo carattere realistico che essa assume sin dall'inizio.
> Anzi, di fronte alle escatologie e alle aspettazioni millenarie del Medioevo,
> l'Umanesimo riafferma il carattere concreto, socialmente ed economicamente
> determinato, della vita urbana e degli sviluppi cittadini: la "città
> prospettica" del Brunelleschi e dell'Alberti è la città reale, con tutta la
> complessità delle sue stratificazioni storiche, nella quale i nuovi "oggetti"
> prospettici introducono un nuovo comportamento umano -- laico e razionale --
> assai più che una proposta di ristrutturazione fisica globale. Nel *De re
> aedificatoria* l'Alberti accetta palesemente la dialettica fra l'edificio
> singolo, nel quale il razionalismo e lo storicismo umanistici possono
> manifestarsi in pieno, e la città, quadro dei vari interventi successivi. In
> ciò l'Alberti è assai più aristotelico di quanto farebbe supporre la sua
> famosa definizione della città come "grande casa".
:::

:::

::: alts

> V resnici je Albertijev arhitekturni poseg namenjen kritični racionalizaciji
> večplastne strukture: enako nalogo opravljajo posamezni spremenljivi elementi
> mestne strukture, od obodnih zidov do nove stende. Albertijevo delo torej ni
> kodifikacija srednjeveškega urbanizma, temveč hipoteza o uporabi obstoječih
> konvencij, preoblikovana z edinim parametrom, ki ga je imel arhitekt na voljo:
> tipološko raziskavo. Prav to je značilnost humanističnih teorij, ki
> arhitekturo razumejo kot urbani pojav. Tipološko kazuistiko Francesca di
> Giorgia in Leonardove izume v urbanem merilu je treba brati prav v tem ključu:
> kot dejanja realizma, torej odločno antiutopična. Prav metodološka stalnost
> pri oblikovanju stanovanjskih tipologij ali stavb za javno rabo v
> eksperimentiranju Francesca di Giorgia ali opredelitev kompleksnih enot
> reorganizacije mestnih struktur, značilna za Leonardove raziskave, ponujata
> realistične podlage za razvoj mest. Takšni teoretiki sprejemajo politične in
> ekonomske temelje reorganizacije združevanja, vključno s strogimi razrednimi
> razlikami. Njihova naloga tehnikov, ki so na najvišji ravni vključeni v
> potekajoče preobrazbe, jim preprečuje, da bi izumljali urbanistične
> ideologije: tipološko orodje se uporablja kot najprimernejše za napredno
> racionalizacijo. To se je zgodilo kljub temu, da je kriza mecenstva, ki se je
> začela že v drugi polovici 15\. stoletja, povzročila neuspeh tudi teh
> racionalizacijskih tehnik: srečna primera Urbina in Pienze ostajata osamljena
> in neponovljiva. Šele na tej točki se je humanistični realizem kot reakcija na
> eni strani osredotočil na izdelavo modelov za edine konkretne pobude za nove
> urbane sisteme -- raziskovanje utrdb in vojaških mest se je rodilo kot
> samostojna znanost -- in se na drugi strani izognil utopičnim sanjam. Po drugi
> strani pa se izmika utopičnim sanjam. urbanistični kazuistiki Francesca di
> Giorgia je utopija še vedno tuja: pravilnost geometrijskih razporeditev,
> četudi utemeljena z eruditskimi in simbolnimi parametri, ima vrednost
> laboratorijskega vzorca, ki se lahko v primerjavi s kompleksno fenomenologijo
> krajev neskončno deformira. Filaretijeva Sforzinda pa priča o drugačnem
> odnosu. Kot polemična načelna deklaracija želi Filaretov traktat pokazati
> konkretno možnost uresničitve enotnega urbanega organizma, katerega zvezdasta
> oblika je polna astroloških in simbolnih referenc; kot težnja, ki se zaveda
> svoje ideološke narave, Sforzindova arhitekturna tipologija v fantastičnem
> smislu poudarja razdrobljenost slovničnih elementov in perspektivno
> razsežnost. Na meji med polemiko in utopijo Filaretovo delo zaznamuje začetek
> krize humanističnega "realizma". To krizo je poudarjala slavnostna in
> simbolična uporaba perspektivne kulture, značilne za rimsko okolje od Nikolaja
> V dalje. Sveti Rim kot civitas Dei ali nebeški Jeruzalem: nič ne bi moglo biti
> bolj oddaljeno od racionalne in posvetne republike Salutatija ali Brunija.
> Prav tako ni zgodovinsko nepomembno, da so v Rimu uresničili le delčke
> Albertijevega programa ali da se je ob koncu 16\. stoletja načrt Sikstine
> nagibal k nadomestitvi političnega realizma in slavnostnih namenov.
> [@tafuri1969rinascimento, 223]

::: {lang=it}

> In realtà l'intervento architettonico viene destinato dall'Alberti alla
> razionalizzazione critica dei tessuti polistratificati: ed eguale compito
> hanno i singoli elementi modificabili della struttura urbana, dal perimetro
> murario alle nuove stende. Non è quindi una codificazione dell'urbanistica
> medievale, quella dell'Alberti, bensì un'ipotesi sull'uso delle convenzioni
> vigenti, trasformate dall'unico parametro completamente in possesso
> dell'architetto: l'indagine tipologica. È proprio quest'ultima che costituisce
> il trait-d'union delle teorizzazioni umanistiche che leggono l'architettura
> come fenomeno urbano. La casistica tipologica di Francesco di Giorgio e le
> invenzioni leonardesche a scala urbana vanno lette precisamente in questa
> chiave: come atti di realismo, quindi, decisamente antiutopistici. Sono la
> costanza metodologica nella formazione delle tipologie residenziali o degli
> edifici di uso pubblico, nella sperimentazione di Francesco di Giorgio, o
> l'individuazione di unità complesse di riorganizzazione delle strutture
> cittadine, tipica delle ricerche di Leonardo, che offrono basi realistiche per
> lo sviluppo delle città. Tali teorici accettano i fondamenti politici ed
> economici in fuse di riorganizzazione, comprese le rigide distinzioni di
> classe. Il loro compito di tecnici integrati al massimo livello nelle
> trasformazioni in corso impedisce loro di inventare ideologie urbane: lo
> strumento tipologico è usato come il più adeguato per una razionalizzazione
> avanzata. Ciò malgrado la crisi della committenza -- già in atto nella seconda
> metà del '400 -- provoca il fallimento persino di tali tecniche di
> razionalizzazione: i felici casi di Urbino e Pienza rimangono isolati e
> irripetibili. È solo a questo punto che, per reazione, il realismo umanistico
> si concentra da un lato nell'elaborazione dei modelli per le uniche iniziative
> concrete di nuovi impianti urbani -- nasce, come scienza autonoma, l'indagine
> sulle fortificazioni e le città militari --, ed evade, d'altro lato, nel sogno
> utopistico. La casistica urbana di Francesco di Giorgio è ancora estranea
> all'utopia: la regolarità dei tracciati geometrici per quanto giustificata con
> parametri eruditi e simbolici, ha il valore di un campione di laboratorio
> deformabile all'infinito al confronto con la complessa fenomenologia dei siti.
> La Sforzinda filaretiana, invece, è la testimonianza di un atteggiamento
> diverso. In quanto polemica dichiarazione di principio il trattato del
> Filarete tende a dimostrare la possibilità concreta di realizzazione di un
> organismo urbano unitario, la cui figura a stella è colma di riferimenti
> astrologici e simbolici; in quanto aspirazione cosciente del proprio carattere
> ideologico, la tipologia architettonica di Sforzinda accentua, in senso
> fantastico, la disarticolazione degli elementi grammaticali e la dimensione
> prospettica. Al limite fra la polemica e l'utopia, l'opera del Filarete segna
> l'inizio della crisi del "realismo" umanistico. Crisi che viene accentuata
> dall'uso celebrativo e simbolico della cultura prospettica tipica
> dell'ambiente romano, da Niccolò V in poi. Roma sacra come civitas Dei o
> Gerusalemme celeste: nulla di più lontano dalla repubblica razionale e mondana
> del Salutati o del Bruni. Né storicamente è poco importante che del programma
> albertiano non vengano realizzati a Roma che dei frammenti, o che, alla fine
> del sec. XVI, il piano sistino tenda a compensare realismo politico e intenti
> celebrativi.

:::

:::

::: alts

> Pomembna je rešitev kontinuitete, ki papeško urbanistično politiko ločuje od
> naprednega aragonskega programa prestrukturiranja Neaplja na eni strani in
> čistega funkcionalizma nizozemskih, nemških ali skandinavskih posegov na
> drugi. Proti sporočilu mesta, proti simbolu mesta se je ponovno potrdilo
> mesto, funkcionalno za interese višje finančne buržoazije: in razvoj dvornih
> ali emblematičnih formae urbia univerzalne vrednosti na ideološki ravni ni bil
> del interesov te nove oligarhije. Strada Nuova v Genovi, Fuggerei v Augsburgu,
> nove četrti v Antwerpnu ali Københavnu so delni posegi, brez polemičnih
> razlogov glede starega tkiva, pogosto izvedeni z že nekaj časa delujočimi
> ekonomskimi mehanizmi, pri katerih je problem oblike le sekundarno vprašanje.
> A to ni dovolj: racionalizacija, ki se izvaja na sektorski ravni in na podlagi
> vrste špekulativnih mehanizmov zasebne akumulacije, naj ne bi bila model, ki
> bi ga bilo mogoče posplošiti na celotno mesto. Prav tako ne moremo trditi, da
> je struktura, ki so jo eksperimentalno razvili v kolonialnih mestih v Ameriki,
> alternativa evropskemu mestu. Shematični kvadrilogi in prostorske
> organizacije, ki se odpirajo v njihovem središču, zmanjšujejo mesto na zgolj
> strukturno podporo, ki je v celoti na voljo, vendar brez oblike: celo na
> področju urbanističnega načrtovanja je intelektualni prestiž oblike v
> kolonialnih državah ukinjen v korist neusmiljene logike izkoriščanja. To se v
> bistvu dogaja celo v najnaprednejših evropskih državah v zvezi z oblikovanjem
> sodobne organizacije kapitala: "zaton svetega", ki so se mu države
> protireformacije skušale zoperstaviti z mitologizacijo določenih mest-simbolov
> -- od Rima do Granade in Borromejskega Milana --, je v reformiranih mestih
> popolnoma sprejet. Če že, je problem v tem, kako nadomestiti funkcionalizem
> ter javno in zasebno pobudo z novo formalno strogostjo: s tem vprašanjem se je
> na najvišji ravni ukvarjal Elias Holl v Augsburgu. V primerjavi s to
> dialektiko med mitologijo in empirijo so utopije socialnega reformizma Mora
> (1516), Agostinija (1553), Patrizija, Donija (1548) in Valentina Andreae
> (1619) dokaz globoke skepse glede možne "napredne" vloge evropskega
> intelektualca. Sončna mesta, zemeljski Jeruzalem, popolne in skupnostne
> družbe, ekonomski in spolni komunizem, deizem -- stalne teme teh političnih
> sanj -- niso resnični predlogi za družbeno racionalizacijo, temveč so zadnje,
> dramatično pričevanje humanističnega racionalizma, ki ob očitnem neuspehu
> svojih družbenih programov z eshatološkimi pričakovanji sankcionira zaton
> svoje lastne civilne funkcije. Upad, ki so ga natančno zabeležili najbolj
> antiutopični teoretski tokovi 16\. stoletja. Razprave o tehnikah utrjevanja, ki
> so jih napisali Dürer (1527), Lorini (1596), Bellucci (1598), Maggi (1546), De
> Marchi (1599), Speckle (1589) in Perret (1604), ne upoštevajo tako eruditskih
> problemov vitruvijske kulture kot abstrakcij utopičnega reformizma.
> Humanistični program posvetnega mesta človeka je zdaj zamenjala politična
> realnost, ki jo je neusmiljeno izpostavil Machiavelli: na sončni zahod
> idealnega mesta odgovarja cinični realizem prinčevih "obrambnih strojnih mest"
> (Horst de la Croix, *Military Architecture and the Radial City Plan in XVI
> Century in Italy,* AB, XLII, 1960; P. Marconi, *La cittadella come macrocosmo*
> Quad. dell'Ist. di Stor. dell'Archit, 1968, št. 85-90, o simbolnih
> vztrajnostih v renesančnem urbanizmu). Bellucci bi lahko potem kruto
> ironiziral vlogo arhitektov in jih pozval, naj svoje študije omejijo na
> formalne nadgradnje: vojaški teoretik je novi znanstvenik urbanih pojavov. S
> tem je dokončno potrjen propad nadstrukturnih ideologij humanizma. Naslednja
> stoletja ne bi imela druge izbire, kot da se sprijaznijo z vlogo, ki jo je
> arhitektura imela v ozadju urbanih preobrazb, ali pa jo zakrijejo z bujnim, a
> patetičnim razcvetom izmikajoče se civilizacije podobe.
> [@tafuri1969rinascimento, 223-224]

::: {lang=it}
> Ciò che conta è la soluzione di continuità che separa la politica urbanistica
> papale dall'avanzato programma di ristrutturazione aragonese per Napoli, da un
> lato, dal puro funzionalismo degli interventi olandesi, tedeschi o scandinavi,
> dall'altro. Contro la città-messaggio, contro la città-simbolo, si riafferma
> la città funzionale agli interessi dell'alta borghesia finanziaria: e non fa
> parte degli interessi di tale nuova oligarchia l'elaborazione a livello
> ideologico di auliche o emblematiche formae urbia di valore universale. La
> Strada Nuova genovese, la Fuggerei ad Augusta, i nuovi quartieri di Anversa o
> di Copenaghen, sono tutti interventi parziali, privi di ragioni polemiche nei
> confronti degli antichi tessuti, spesso realizzati con meccanismi economici
> già operanti da tempo ed in cui il problema della forma non è che tema
> secondario. E non basta: la razionalizzazione attuata a livello di settore e
> sulla base di una serie di meccanismi speculativi di accumulazione privata,
> non vuole proporsi come modello generalizzabile all'intera città. Né può dirsi
> che la struttura elaborata sperimentalmente nelle città coloniali in America,
> costituisca l'alternativa alla città europea. Gli schematici quadrilloges e le
> organizzazioni spaziali che si aprono al loro centro, riducono la città a mero
> supporto strutturale, del tutto disponibile ma privo di forma: anche in tema
> urbanistico il prestigio intellettuale della forma è annullato nei paesi
> coloniali, a favore della spietata logica dello sfruttamento. È in fondo ciò
> che accade anche nei paesi europei più avanzati rispetto alla formazione della
> moderna organizzazione del capitale: l'"eclissi del sacro", cui i paesi dello
> Controriforma tentano di opporsi con la mitizzazione di alcune città-simbolo
> -- da Roma a Granada alla Milano borromea -- è accettata in pieno nelle città
> riformate. Il problema, qui, caso mai, è quello di compensare il
> funzionalismo, e l'iniziativa pubblica e privata, con un nuovo rigorismo
> formale: è il tema affrontato al massimo livello da Elias Holl ad Augusta. Al
> confronto di tale dialettica fra mitologia ed empirismo le utopie del
> riformismo sociale del Moro (1516), dell'Agostini (1553), del Patrizi, del
> Doni (1548), di Valentin Andreae (1619), sono testimonianze di un profondo
> scetticismo sul possibile ruolo "progressivo" dell'intellettuale europeo. Le
> città del sole, le Gerusalemmi terrene, le società perfette e comunitarie, il
> comunismo economico e sessuale, il deismo -- temi costanti di tali sogni
> politici -- non sono proposte reali di razionalizzazione sociale: in essi va
> piuttosto riconosciuta l'ultima, drammatica testimonianza del razionalismo
> umanistico, che, di fronte al fallimento palese dei propri programmi sociali,
> sancisce con le sue escatologiche aspettazioni il declino dello propria stessa
> funzione civile. Declino che viene puntualmente registrato dalla più
> antiutopistica delle correnti teoriche del '500. I trattati sulla tecnica
> fortificatoria del Dürer (1527), del Lorini (1596), del Bellucci (1598), del
> Maggi (1546), del De Marchi (1599), dello Speckle (1589), del Perret (1604),
> ignorano sia i problemi eruditi della cultura vitruviana, che le astrazioni
> del riformismo utopistico. Al programma umanistico dello laica città dell'uomo
> si è ormai sostituita la realtà politica messa spietatamente in luce dal
> Machiavelli: al tramonto della città ideale risponde il cinico realismo delle
> "città-macchine di difesa" del Principe (Horst de la Croix, *Military
> Architecture and the Radial City Plan in XVI Century in Italy*, AB, XLII,
> 1960; P. Marconi, *La cittadella come macrocosmo* Quad. dell'Ist. di Stor.
> dell'Archit., 1968, n. 85--90, sulle persistenze simboliche nell'urbanistica
> rinascimentale). Il Bellucci potrà quindi ironizzare crudelmente sul ruolo
> degli architetti, invitandoli a limitare i propri studi alle sovrastrutture
> formali: è il teorico militare il nuovo scienziato dei fenomeni urbani. Il
> tramonto definitivo delle ideologie sovrastrutturali dell'Umanesimo è così
> sancito. Ai secoli successivi non rimarrà che accettare il ruolo di
> retroguardia riservato all'architettura nei confronti delle trasformazioni
> urbane, o coprirlo con il rigoglioso ma patetico fiorire di una evasiva
> civiltà dell'immagine.
:::

:::

### Arhitektura znanost in tehnologija

::: alts
> Veliko zanimanje za tehnologijo, razširjeno v Toskani konec 14\. in v začetku
> 15\. stoletja, vrednost eksperimentalnega preverjanja naravnih zakonov, ki jo
> je humanizem pripisoval umetniški dejavnosti, spoznavna vrednost strukture
> prostora, ki je lastna perspektivni viziji, že na samem začetku dolge
> zgodovine klasicizma postavljajo temo odnosa med umetnostjo in znanostjo. Ker
> imamo s perspektivo možnost, da neskončno raznolikost narave zreduciramo na
> enoto, merljivo z enim samim merilom, in ker se znanost o perspektivi ukvarja
> z medsebojnim odnosom stvari ne glede na njihove individualne lastnosti,
> sledi, da je z novim vizualnim kodom resničnost pripravljena na kvantitativno
> in racionalno raziskovanje. Ista neskončnost, materializirana v izginjajoči
> točki, dobi končno vrednost v znanosti o perspektivi in se vrne v človeško
> izkušnjo. V tem smislu perspektiva ne reproducira realnosti, temveč jo
> rekonstruira po intelektualnih zakonih, ki jih določa globoka potreba po
> znanju in posedovanju. Vendar pa znanje ni popolno, če ne vključuje
> kakovostnega raziskovanja; prav tako posest ostaja abstraktna, če ne vključuje
> namena preoblikovati naravo v celoti, potem ko jo racionalno razišče.
> Slikarska izkušnja Paola Uccella in Piera della Francesca se torej ukvarja z
> iskanjem naključja, posebne kakovosti v geometrijski, univerzalni konstrukciji
> oblike, tako da med sestavine te konstrukcije uvršča tudi svetlobo ali
> sorazmernost barvne palete. Brunelleschijeva arhitektura in Leonardove
> arhitekturne risbe so namenjene preoblikovanju civilnega in naravnega okolja;
> prvi z uvedbo konkretne merljivosti prostora v kontinuum srednjeveškega mesta,
> drugi pa s postavitvijo problema teritorialne razširitve projektne dejavnosti.
> Prostor in čas sta tako podvržena kodu, ki ga ponazarja red človeške misli, ki
> želi -- v skladu z racionalizacijsko ideologijo višjega srednjega razreda in
> novih "knezov" -- prepoznati vednost o svetu in delovanje na svetu. V tem
> smislu se znanost in umetnost ujemata. Ne le zato, ker umetnost velja za
> najbolj usposobljeno človeško tehniko, temveč tudi zato, ker je z novim
> ugledom, ki ga je dobila vizija, sposobna ponuditi norme vedenja, konkretna
> navodila za racionalno obvladovanje zakonov kozmosa. Še vedno bi lahko
> ugovarjali, da je velik del gnoseološke vrednosti, pripisane obliki, v 15\. in
> 16\. stoletju vodil k obnovi magije. V renesansi pa je magija predpostavka za
> tehnike popolnega obvladovanja resničnosti in vse bolj eksperimentalno posega
> v naravne elemente. Kodifikacija perspektive v Albertijevem traktatu *Della
> Pittura,* Pierovih priročnikih o matematiki in perspektivi (*De abaco*, *De
> perspectiva pingendi*, *Libellus de quinque corporibus regularibus)*, *De
> divina proportione* Luce Paciolija, traktatih Pomponia Gaurica (1504) in Jeana
> Pélerina, znan kot Le Viateur (1504), konkretizira in posplošuje metode
> intelektualne konstrukcije prostora ter z vzajemno izmenjavo matematičnih
> abstrakcij, teorij glasbene harmonije in raziskovanja načinov videnja ponovno
> odkriva krožnost znanstvene in umetniške izkušnje. Poenotenje prostora je
> predpostavka in simbol želenega idealnega poenotenja sveta: nova geografska
> odkritja, razvoj kartografije, usklajevanje pobud za preoblikovanje mest, vse
> to sledi -- čeprav posredno -- novi homogenosti človeškega okolja, ki so jo
> uvedli umetniki humanizma. [@tafuri1969rinascimento, 224]

::: {lang=it}
> Il grande interesse per la tecnologia, diffuso nel tardo '300 e nel primo '400
> in Toscana, il valore di verifica sperimentale delle leggi naturali attribuito
> dall'Umanesimo all'attività artistica, il valore conoscitivo della struttura
> dello spazio, proprio della visione prospettica, pongono, alle origini stesse
> della lunga storia del Classicismo, il tema dello relazioni fra arte e
> scienza. Anzi, poiché attraverso la prospettiva si viene ad avere la
> possibilità di ridurre ad unità, misurabile secondo un'unica scala, l'infinita
> varietà della natura, e poiché la scienza prospettica si occupa delle
> relazioni delle cose fra loro prescindendo dalle loro qualità individuali, ne
> discende che attraverso il nuovo codice visivo si predispone la realtà ad
> un'indagine quantitativa e razionale. Lo stesso infinito, materializzato nel
> punto di fuga, assume nella scienza prospettica un valore finito, è riportato
> ad un'esperienza umana. In tal senso la prospettiva, lungi dal riprodurre la
> realtà, la ricostruisce secondo leggi intellettuali informate a un profondo
> bisogno di conoscenza e di possesso. La conoscenza non è però totale se non
> implica una indagine qualitativa; il possesso, ugualmente, rimane astratto se
> non' implica l'intenzione di trasformare integralmente la Natura dopo averla
> razionalmente esplorata. L'esperienza pittorica di un Paolo Uccello e di un
> Piero della Francesca si preoccupa quindi di ritrovare l'accidente, la qualità
> particolare, in seno alla costruzione geometrica, universale, della forma:
> fino ad includere fra gli ingredienti di quella costruzione la luce o la
> proporzionalità nella gamma dei colori. L'architettura di Brunelleschi, e i
> disegni architettonici di Leonardo si rivolgono a trasformare l'ambiente
> civile e naturale; la prima introducendo una misurabilità concreta dello
> spazio nel continuum della città medievale, i secondi impostando il problema
> di un'estensione territoriale dell'attività di progettazione. Lo spazio e il
> tempo sono così soggetti ud un codice esemplato sull'ordine di un pensiero
> umano che -- coerentemente all'ideologia razionalizzatrice dell'alta borghesia
> e dei nuovi "principi" -- vuole identificare conoscenza del mondo e azione sul
> mondo. È in tale senso che scienza e arte vengono fatte coincidere. E non solo
> perché l'arte è vista come la più qualificata delle tecniche umane, ma anche
> perché attraverso il nuovo prestigio dato alla visione, l'arte è in grado di
> offrire norme di comportamento, guide concrete per appropriarsi razionalmente
> delle leggi del cosmo. Si può ancora obiettare che buono parte del valore
> gnoseologico attribuito alla forma sfocia, nel '400 e nel '500, nel recupero
> della magia. Ma nel Rinascimento la magia è premessa a tecniche di pieno
> possesso del reale, e sempre più interviene sperimentalmente sugli elementi
> naturali. La codificazione della prospettiva contenuta nel trattato *Della
> Pittura* dell'Alberti, i manuali di matematica e prospettiva di Piero (*De
> abaco*, *De perspectiva pingendi*, *Libellus de quinque corporibus
> regularibus*), il *De divina proportione* di Luca Pacioli, i trattati di
> Pomponio Gaurico (1504) e di Jean Pélerin, detto Le Viateur (1504), rendono
> concreti e generalizzabili i metodi di costruzione intellettuale dello spazio,
> ritrovando la circolarità delle esperienze scientifiche ed artistiche
> attraverso il reciproco scambio di astrazioni matematiche, teorie di armonia
> musicale, indagini sui modi della visione. L'unificazione dello spazio è
> premessa e simbolo dell'agognata unificazione ideale del mondo: le nuove
> scoperte geografiche, gli sviluppi della cartografia, il coordinamento delle
> iniziative di trasformazione urbana, conseguono -- anche se indirettamente --
> alla nuova omogeneità dell'ambiente umano introdotto dagli artisti
> dell'Umanesimo.
:::

:::

::: alts
> Vendar pa identifikacija umetnosti in znanosti še vedno lahko privede do dveh
> različnih operativnih pristopov. Po eni strani lahko spodbudi
> eksperimentiranje z racionaliziranimi tehnikami v okviru arhitekture, ki je
> sama normalizirala in racionalizirala jezik z uporabo kanona naročil ter
> modularnosti pristanišč in sklopov (vendar bo treba preveriti, v kolikšni meri
> si kanoni in tehnološki izumi ne nasprotujejo). Po drugi strani pa bi lahko,
> nasprotno, spodbudila popolno zaupanje v avtonomno zmožnost vidne oblike, da
> raziskuje in gradi prizorišče človeškega dogajanja: in takrat bi se namesto
> tehnološkega eksperimentalizma morali zateči k poenostavljeni gradbeni
> tehniki, ki bi prav tako temeljila na vrsti kanoničnih struktur, povezanih s
> tipologijo stavb. Brunelleschi je že takrat nihal med tema dvema skrajnostma,
> čeprav se je zaradi zavedanja, da mora korenito spremeniti vse dosedanje
> konvencije, pri gradnji florentinske kupole in Pazzijeve kapele dosledno
> odločal za prvo pot. Poleg tega je bila sodobna delitev dela v revoluciji na
> gradbišču, ki jo je sprožil, nujen in neizogiben pogoj. Brunelleschi je kot
> razlagalec in ideolog -- v posebnem sektorju gradbene proizvodnje -- struktur
> nove trgovske buržoazije odločno prekinil s tradicionalnim korporativnim redom
> gradbenih delavcev ter celotno odgovornost za načrtovanje in celoten gradbeni
> program prenesel v roke novega intelektualca, ki je z njim postal arhitekt.
> Racionalnost in tehnološka protitradicionalnost florentinske kupole razkrivata
> njuno zgodovinsko vlogo; sta funkciji sodobne družbene delitve dela, pogoj
> arhitekturne, znanstvene in gospodarske prenove. Tehnologija je bila torej
> prav tako del novega intelektualnega univerzuma: Alberti bo to predpostavko
> teoretično sistematiziral, medtem ko so se po vsej Evropi in v italijanskih
> krogih, povezanih s starim redom, šole tradicionalnih delavcev -- od
> Lombardeschija do Gagginija in Pahra -- zdaj pripravljale na zadnji
> korporativni odpor v imenu obrtniške enotnosti cikla gradbene proizvodnje.
> Torej znanost proti obrtniškemu empirizmu. Toda tudi empirizem je del novega
> znanstvenega duha in njegovo izključevanje vodi neposredno v dogmatizem, vodi
> v sklerotično nasprotje znanosti. Prav to je Leonardov problem. Ne le zaradi
> nepristranske empiričnosti, s katero v svojih arhitekturnih risbah sintetizira
> tehnične izume in jezikovne eksperimente, temveč tudi zato, ker te risbe
> obravnava kot načine spoznavanja vidne resničnosti, teoretične eseje o
> obnašanju struktur, tridimenzionalne prereze urbanistične reorganizacije.
> [@tafuri1969rinascimento, 224]

::: {lang=it}
> L'identificazione dì arte e scienza può però ancora comportare due
> diversi atteggiamenti operativi. Da un lato può stimolare la
> sperimentazione di tecniche razionalizzate nel contesto di
> un'architettura che ha per suo conto normalizzato e razionalizzato il
> linguaggio attraverso il ricorso al canone degli ordini e alla
> modularità delle porti e degli insiemi (ma allora bisognerà verificare
> quanto canoni e invenzioni tecnologiche non si contraddicano fra loro).
> Dall'altro può stimolare, al contrario, una fiducia assoluta nelle
> capacità autonome della forma visibile ad esplorare e costruire la scena
> delle vicende umane: ed allora piuttosto che a uno sperimentalismo
> tecnologico si dovrà ricorrere ad una tecnica di costruzione
> semplificata, basata anch'essa su una serie di strutture canoniche
> legate alla tipologia edilizia. Brunelleschi oscilla già fra questi due
> estremi; anche se la coscienza di introdurre un mutamento radicale in
> tutte le convenzioni preesistenti lo portano a preferire coerentemente
> la prima via nell'impresa della cupola fiorentina e nella cappella
> Pazzi. D'altronde, nella rivoluzione del cantiere da lui provocata, la
> moderna divisione del lavoro è condizione essenziale e ineliminabile. In
> quanto interprete e ideologo -- nello specifico settore della produzione
> edilizia -- delle strutture della nuova borghesia mercantile,
> Brunelleschi rompe decisamente il tradizionale ordinamento corporativo
> delle maestranze edili, accentrando l'intera responsabilità ideativa e
> l'intero programma realizzativo nelle mani di quel nuovo intellettuale
> che, con lui, diviene l'architetto. La razionalità e
> l'antitradizionalismo tecnologico della cupola fiorentina rivelano il
> loro ruolo storico; essi sono funzioni di una moderna divisione sociale
> del lavoro, condizione, insieme, del rinnovamento architettonico, di
> quello scientifico, di quello economico. Anche la tecnologia, dunque, è
> parte del nuovo universo intellettuale: l'Alberti sistemerà teoricamente
> tale assunto, mentre in tutta Europa e negli ambienti italiani legati
> agli antichi ordinamenti, le scuole delle maestranze tradizionali -- dai
> lombardeschi, ai Gaggini, ai Pahr -- si apprestano ad opporre adesso
> l'ultima resistenza corporativa in nome dell'unità artigianale del ciclo
> di produzione edilizia. La scienza contro l'empirismo artigianale,
> dunque. Ma anche l'empiria è parte del nuovo spirito scientifico, ed
> escluderla conduce direttamente al dogmatismo, porta la scienza a
> sclerotizzarsi nel suo opposto. È proprio questo il problema di
> Leonardo. E non solo per lo spregiudicato empirismo con cui egli
> sintetizza, nei suoi disegni architettonici, invenzioni tecniche ed
> esperimenti linguistici, ma anche perché quei disegni sono da lui
> considerati modi di conoscenza della realtà visibile, saggi teorici sul
> comportamento delle strutture, sezioni tridimensionali di
> riorganizzazione urbana.
:::

::: ::: alts
> Znanost in tehnologija sta bili v Leonardovem delu še vedno povezani, tudi
> zato, ker je bila njegova celotna arhitekturna produkcija omejena zgolj na
> teoretični okus. Čeprav je to zavezništvo igralo glavno vlogo pri Francescu di
> Giorgiu in Fra' Giocondoju, je kmalu doživelo krizo: po eni strani je
> Bramantejeva revizija povzročila koncentracijo zanimanja za povsem formalne
> zakonitosti klasicizma, po drugi strani pa so tehnološke vrednote našle
> najširše polje uporabe v vojaški arhitekturi in inženirstvu na urbani in
> teritorialni ravni. Bramante, Peruzzi, Falconetto in Mormanno so bili
> znanstveniki prostorskih zakonov in sintaktičnih členitev: in znanstveniki v
> pravem pomenu besede, saj se je v novem ozračju, ki so ga ustvarili, rodil
> filološki študij antike. Fieravanti in fra Giocondo sta bila najprej teoretika
> vojaške arhitekture, nizozemski tehniki in inženirji pa so se kasneje v celoti
> posvetili problemom, povezanim s tehnikami utrjevanja, melioracijo in
> namakanjem podeželja ter opremljanjem mest in regij. [@tafuri1969rinascimento,
> 225]

::: {lang=it}
> Scienza e tecnologia, in Leonardo, sono ancora alleate, anche perché la sua
> intera produzione architettonica si limita al puro assaggio teorico. Se in
> Francesco di Giorgio e in fra' Giocondo tale alleanza gioca un ruolo primario,
> ben presto essa viene messa in crisi: da un lato la revisione bramantesca
> provoca una concentrazione degli interessi sulle leggi puramente formali del
> Classicismo, dall'altro le valenze tecnologiche trovano il loro più ampio
> campo di applicazione nell'architettura militare e nell'ingegneria a livello
> urbano e territoriale. Bramante, Peruzzi, Falconetto o il Mormanno sono gli
> scienziati delle leggi spaziali e delle articolazioni sintattiche: e
> scienziati in senso proprio, dato che è nel nuovo clima che essi determinano
> che nasce, fra l'altro, lo studio filologico dell'antico. Il Fieravanti e fra'
> Giocondo, prima, i teorici di architettura militare, i tecnici e gli ingegneri
> olandesi, in un secondo tempo, concentrano per intero la loro attenzione sui
> problemi relativi alla tecnica fortificatoria, alla bonifica e all'irrigazione
> delle campagne, all'attrezzatura delle città e delle regioni.
:::

:::

::: alts
> Slednji so zagotovo izkoristili nove prispevke znanstvenih odkritij: vojaška
> umetnost in razvoj novih obrambnih sistemov -- med drugim iznajdba bastionskega
> stebrička -- sta privedla do vse bolj kompleksnih študij gradbenih tehnik in
> prostorskih sistemov, ki so zdaj popolnoma zanemarili klasicistično
> kodifikacijo. V tem okviru so zakoni prostora razvrščeni glede na zvrsti.
> Usklajevanje perspektive, arheologija, eksperimentiranje s kompleksnimi skupki
> preprostih prostorov se uporabljajo v civilni in verski gradnji; empirizem in
> funkcionalizem sta rezervirana za vojaško arhitekturo. Tisti, ki so tako kot
> Sanmicheli poskušali posredovati med temi nasprotujočimi si študijami, so
> morali meriti njihova očitna protislovja (le Delormu je v nekem drugem smislu
> uspelo doseči integralni racionalizem: vendar ima natančna izolacija njegovih
> raziskav izrazit zgodovinski pomen). [@tafuri1969rinascimento, 225]

::: {lang=it}
> Sono certo questi ultimi a sfruttare tecnicamente gli apporti nuovi delle
> scoperte scientifiche: l'arte militare e l'elaborazione dei nuovi sistemi di
> difesa -- ricordiamo l'invenzione del frontone bastionato, fra i tanti --
> inducono studi sempre più complessi sulle tecniche costruttive e su impianti
> spaziali che prescindono ormai completamente dalla codificazione classicista.
> In tale ambito le leggi dello spazio vengono classificate per generi.
> Coordinamento prospettico, archeologismo, sperimentazione su aggregazioni
> complesse di spazi semplici, sono applicati all'edilizia civile e religiosa;
> empirismo e funzionalismo sono riservati all'architettura militare. Chi, come
> il Sanmicheli, tenta una mediazione fra tali opposte ricerche è obbligato a
> misurarne l'evidente contradditorietà (solo il Delorme, in altra accezione,
> riesce ad approdare ad un razionalismo integrale: ma l'accurato isolamento
> della sua ricerca ha un esplicito significato storico).
:::

:::

::: alts
> Vse to je kmalu povzročilo krizo v humanistični identifikaciji arhitekture in
> znanosti. Če prostora dejansko ni več mogoče v celoti povzeti v simbolični
> obliki perspektivnega pogleda, če zunaj povsem merljivega in intelektualnega
> prostora matematično-geometričnih zakonov in harmoničnega usklajevanja obstaja
> empirični prostor, ki ga ni mogoče reducirati na primere, pade sam postulat o
> absolutni homogenosti prostora: z drugimi besedami, prav tisti postulat, ki je
> omogočil, da je umetniško izkušnjo mogoče obravnavati kot konkretno raziskavo
> strukture resničnosti. [@tafuri1969rinascimento, 225]

::: {lang=it}
> Tutto ciò non tarda a far entrare in crisi l'umanistica identificazione di
> architettura e scienza. Se infatti lo spazio non può più essere interamente
> riassunto nella forma simbolica dello visione prospettica, se al di là dello
> spazio tutto misurabile e intellettuale delle leggi matematico-geometriche e
> del coordinamento armonico, esiste uno spazio empirico, ad case irriducibile,
> cade il postulato dell'assoluta omogeneità dello spazio stesso: vale a dire
> proprio il postulato che permetteva di considerare l'esperienza artistica come
> concreta indagine sulla struttura del reale.
:::

:::

::: alts
> Vendar bi bilo napačno, če bi v razpravi med vitruvijanstvom in empiričnimi
> znanostmi obe koncepciji opredelili kot antitezi. V 16\. stoletju je bila
> vitruvijevska kultura še vedno sposobna združevati veliko število različnih
> fermentov: če je res, da obstaja nasprotje med arheološkim in anahronističnim
> akademizmom Trissina in empirizmom Alvise Cornaro, je prav tako res, da so
> historizem, arheologija, civilna ideologija, tehnološki interes in znanstvene
> teme našli sintezo v komentarju Daniela Barbaro k Vitruvijevim desetim knjigam
> (1556). Za Barbaro je oblika intelektualna dejavnost in instrument znanja.
> Aristotelizem in scientizem padovanske šole se odražata v odlomku, v katerem
> izvoljeni akvilejski patriarh priznava razsežnosti, ki jih lahko doseže
> oblikovalska dejavnost pri popolnem preoblikovanju narave v kulturo:
> Občudovanja vredno je, da lahko v skupno korist vseh zberemo neukročene ljudi
> in jih podredimo bogoslužju in disciplini, varne in mirne v mestih in
> trdnjavah, nato pa z večjim nasiljem nad naravo sekamo pečine, silimo gore,
> polnimo doline, sušimo močvirja, gradimo ladje, usmerjamo reke, opremljamo
> pristanišča, gradimo mostove in premagujemo samo naravo v tistih hišah, ki jih
> premagujemo z dvigovanjem ogromnih bremen in delno zadovoljujemo prirojeno
> željo po Večnosti (D. Barbaro, *I dieci libri di M. Vitruvio...,* Benetke,
> 1556, str. 8). Arhitekt, humanist in znanstvenik, zato s svojo tehnično in
> intelektualno premočjo premaga tako običaje neotesanih ljudi kot izvorno
> grobost narave, na katero je njegov poseg nasilen. Premagovanje narave je
> pogosta tema manieristične poetike. Vendar se zdi, da tu beremo odsev dejanske
> in konkretne preobrazbe beneškega podeželja, ki jo je izvedla
> predkapitalistična aristokracija, ter nasilnega dela kulturne kolonizacije in
> gospodarskega izkoriščanja avtohtonih prebivalcev Amerike, ki so bili v novih
> mestih Cortésa in Filipa V. "nasilno podvrženi čaščenju in disciplini".
> Znanost začne razkrivati svoje razredne konotacije, in sicer prav pri
> kolonizaciji. [@tafuri1969rinascimento, 225]

::: {lang=it}
> Sarebbe però sbagliato identificare le due concezioni in antitesi nel
> dibattito fra vitruvianesimo e scienze empiriche. Nel '500 la cultura
> vitruviana è ancora capace di raccogliere in sé una grande molteplicità di
> fermenti: se è vero che esiste un contrasto fra l'accademismo archeologico ed
> anacronistico del Trissino e l'empirismo di Alvise Cornaro, è anche vero che
> storicismo, archeologia, ideologia civile, interesso tecnologico, temi
> scientifici, trovano una sintesi nel commento di Daniele Barbaro ai dieci
> libri di Vitruvio (1556). La forma, per il Barbaro, è attività intellettuale e
> strumento di conoscenza. L'aristotelismo e lo scientismo della scuola padovana
> si riflettono nel passo in cui il patriarca eletto di Aquileia riconosce le
> dimensioni cui l'attività di progettazione può giungere nel trasformare
> integralmente la natura in cultura: Mirabil cosa è il potere a comune
> beneficio raunare gli huomini rozzi, e quelli ridurre al culto e alla
> disciplina, sicuri e tranquilli nelle città e nelle fortezze; poi, con maggior
> violenza fatta alla natura, tagliare le rupi, forzare i monti, empire le
> valli, seccare le paludi, fabbricare le navi, dirizzare i fiumi, munire i
> porti, gettare i ponti, e superare la stessa natura in quelle case, che noi
> vinti siamo levando pesi immensi, e satisfacendo in parte al desiderio innato
> della Eternità (D. Barbaro, *I dieci libri di M. Vitruvio...*, Venezia, 1556,
> p. 8). L'architetto, umanista e scienziato, vince dunque, con la sua
> superiorità tecnica e intellettuale, sia i costumi degli uomini rozzi, che la
> rozzezza originaria della natura, sulla quale il suo intervento compie una
> violenza. Il superamento della natura è tema comune delle poetiche manieriste.
> Ma qui sembra di leggere un riflesso sia delle reali e concrete operazioni di
> trasformazione della campagna veneta, ad opera del patriziato precapitalista,
> sia della violenta opera di colonizzazione culturale e dello sfruttamento
> economico delle popolazioni indigene d'America, "ridotte con la violenza al
> culto e alla disciplina" nelle nuove città del Cortés e di Filippo V. La
> scienza inizia a rivelare, e proprio nella colonizzazione, i propri connotati
> di classe.
:::

:::

::: alts
> Toda obsežnost ozemeljskih sprememb, ki jih je predvideval humanizem, se je
> uresničila le občasno in s številnimi težavami. Za izpolnitev te razsežnosti
> načrtovanja bo treba počakati na velika dela melioracij in prestrukturiranja
> mest v nizozemskem 17\. stoletju: prav tako ni vseeno, da sta ohranitev
> empiričnega odnosa in majhna teža, ki jo je imel klasicistični sistem kot tak,
> pomagala nordijskim državam, da so brez pretiranih predsodkov sprejele
> prispevek novih tehničnih in znanstvenih odkritij. Klasicizem, ki je v 15\.
> stoletju samostojno naredil velik korak naprej v znanstveni misli, je kmalu
> prehitela družbena delitev dela, ki jo je potrdil. Razvoj humanističnega
> racionalizma je ločil umetnost od znanosti v trenutku, ko se je ta osvobodila
> magije. Od konca 16\. stoletja dalje se je umetnost znašla pred dvojno
> alternativo: ali se je odpovedala svoji privilegirani vlogi in iskala odnos
> odvisnosti od nove znanstvene misli, ali pa je sprejela slavilno funkcijo
> znanosti, ne brez magičnih in ezoteričnih odtenkov. Toda znanost še ni bila
> dovolj neodvisna, da bi lahko vzpostavila nove vrednote ali nova pravila
> vedenja: umetnost in arhitektura sta še vedno imeli veliko svobode. Zato je
> bila kriza v letih 1500 in prvih desetletjih 16\. stoletja povsem podzemna in
> je le redko postala zavestno dejstvo. V zanimanju za mehaniko avtomatov,
> strojev in hidravličnih instrumentov, za organske materiale ali zoomorfne
> oblike, ki je razvidno iz okrasja in grobnic Giulia Romana, Buontalentija in
> Palissyja, se jasno kaže navezava na tokove, ki danes preučujejo naravo iuxta
> propria principia: Vendar so to vedno obrobna dela, v katerih prevladuje
> skrivnostno in nerealno vzdušje, ki se bo kmalu umaknilo čistemu tehnokratizmu
> Domenica Fontane ali Simona Stevina (1548-1620), nizozemskega matematika in
> teoretika hidravlične tehnologije in novih sistemov utrdb.
> [@tafuri1969rinascimento, 225]

::: {lang=it}
> Ma la vastità delle trasformazioni territoriali preconizzata dall'Umanesimo
> non diviene realtà che in modo sporadico e con molte difficoltà. Bisognerà
> attendere le grandi opere di bonifica ce ristrutturazione urbana del '600
> olandese per vedere soddisfatta tale dimensione della progettazione: né è
> indifferente che la sopravvivenza di un atteggiamento empirico e lo scarso
> peso dato al sistema classicista come tale, abbiano aiutato i paesi nordici ad
> accettare senza pregiudizi eccessivi l'apporto delle nuove scoperte tecniche e
> scientifiche. Il Classicismo, che nel '400 aveva autonomamente fatto compiere
> un balzo in avanti al pensiero scientifico, sta per essere superato da quella
> stessa divisione sociale del lavoro che esso aveva sancito. Sono gli sviluppi
> del razionalismo umanistico che scindono l'arte dalla scienza, nel momento
> stesso che quest'ultima si libera dalla magia. Dalla fine del sec. XVI in poi
> all'arte si presenta una doppia alternativa: o l'abbandono del proprio ruolo
> privilegiato, nella ricerca di un rapporto di dipendenza dal nuovo pensiero
> scientifico, o l'accettazione di una funzione celebrativa della scienza, non
> senza inflessioni magiche ed esoteriche. Ma ancora la scienza non è tanto
> indipendente da stabilire di per sé nuovi valori o nuove norme di
> comportamento: all'arte e all'architettura sono ancora concessi molti margini
> di libertà. La crisi quindi, per tutto il '500 e i primi decenni del '600, è
> tutta sotterranea e di rado diviene fatto cosciente. Nell'interesse per la
> meccanica degli automi, per le macchine e gli strumenti idraulici, per i
> materiali organici o per le forme zoomorfe, evidente nelle decorazioni e nelle
> grotte di Giulio Romano, del Buontalenti, del Palissy, è chiaro il riferimento
> alle correnti che ormai studiano la natura iuxta propria principia: ma si
> tratta sempre di opere marginali, dominate da un clima misteriosofico e
> irreale, che cederà ben presto il passo alla pura tecnocrazia di un Domenico
> Fontana o di un Simon Stevin (1548--1620), il matematico olandese teorico
> della tecnica idraulica e dei nuovi sistemi di fortificazione.
:::

:::

::: alts
> Nove eksperimentalne znanosti Keplerja, Galileja in pozneje Huygensa ni treba
> več slaviti. Z eksperimentiranjem zavrača abstraktno idealizacijo: ustrezno
> slavimo le tisto, kar je stabilno in utrjeno, ne pa tistega, kar se želi
> nenehno spreminjati. Vendar pa je arhitektura 16\. stoletja v povezavi z
> rojstvom modernega znanstvenega odnosa nepomembna naloga. V času, ko niti
> astronomska odkritja niti sistematične raziskave obnašanja teles niso začele
> rušiti antropocentričnih in geocentričnih dogem in preden so analize Bruna,
> Patrizija, Telesia in Scaligerja podvrgle tradicionalni koncept prostora
> radikalni kritiki, so arhitekturne raziskave dialektike prostorov, o njihovih
> presečiščih in metamorfozah, o njihovih anamorfnih krčenjih, značilnih za
> Bramanteja, Peruzzija, Michelangela, Serlia in pozneje Delorma, Palladia in
> Montana, nas z vizualno vzgojo pripravljajo na sprejemanje relativnosti in
> nedoločenosti, ki ju bo znanstvena misel konkretno uvedla na vse ravni z njo
> povezanega življenja. Tega temeljnega prispevka manierizma ne moremo
> zamolčati: bolj kot v čistem odkrivanju "bogastva dvoumnosti" moramo v njem
> iskati njegov prispevek v civilnem smislu. [@tafuri1969rinascimento, 225-226]

::: {lang=it}
> La nuova scienza sperimentale di Keplero, di Galileo, più tardi di Huygens,
> non ha più bisogno, a rigore, di essere celebrata. Divenendo sperimentale,
> essa respinge un'astratta idealizzazione: si può celebrare adeguatamente solo
> ciò che è stabile e consolidato, non ciò che è disposto a rivoluzionare di
> continuo se stesso. Esiste purtuttavia, per l'architettura del '500, un
> compito non marginale, in relazione alla nascita del moderno atteggiamento
> scientifico. Quando ancora né le scoperte astronomiche né l'indagine
> sistematica sul comportamento dei gravi avevano iniziato a demolire i dogmi
> antropocentrici e geocentrici, c prima ancora che le analisi del Bruno, del
> Patrizi, del Telesio, dello Scaligero avessero sottoposto a critica radicale
> il concetto tradizionale di spazio, le ricerche architettoniche sulla
> dialettica degli spazi, sulle loro intersezioni e metamorfosi, sulle loro
> contrazioni anamorfiche, tipiche del Bramante, del Peruzzi, di Michelangelo,
> del Serlio, e, più tardi, del Delorme, del Palladio, del Montano, preparano ad
> accettare, mediante l'educazione visiva, la relatività e l'indeterminatezza
> che il pensiero scientifico introdurrà concretamente in tutti i livelli della
> vita associata. Tale apporto fondamentale del Manierismo non può essere
> passato sotto silenzio: in esso, più che nella pura scoperta della "ricchezza
> dell'ambiguità", è da cercare il suo contributo in senso civile.
:::

:::

### Arhitektura in ideologija

::: alts
> Globok ideološko-logični značaj klasicističnega humanizma se izraža v dveh
> osnovnih postulatih, ki sta bila implicitna v Brunelleschijevi arhitekturi in
> sta postopoma postala teoretični načeli misli o umetnosti in filozofske
> spekulacije: identiteta narave in razuma na eni strani ter opredelitev druge,
> popolnejše narave v klasicizmu na drugi strani. Brunelleschijev racionalizem
> in historizem se skupaj s humanizmom Masaccia in Donatella bereta kot odsev
> novega posvetnega, konkretnega duha florentinskega višjega srednjega razreda
> 14\. in zgodnjega 15\. stoletja, usmerjenega v kvantifikacijo kakovostnih
> vrednot (A. Hauser, *Storia sociale dell'arte*, Torino 1956, II; F. Antal, *La
> pittura fiorentina e il suo ambiente sociale nel Trecento e nel primo
> Quattrocento,* Torino 1960). Vendar pa je Garin upravičeno opozoril, da je na
> začetku 15\. stoletja obstajal razkorak med družbenopolitičnimi in
> gospodarskimi dogodki ter umetniškim razvojem: sezona največjega sijaja
> toskanskega meščanstva je bila že ob sončnem zahodu, ko se je začel
> uveljavljati florentinski humanizem (E. Garin, *Medioevo e Rinascimento,*
> Bari, 1966^3). To ni protislovje: Brunelleschi je bil tako kot Alberti ali
> Salutati intelektualec v sodobnem smislu, pravzaprav prvi arhitekt, ki so ga
> tako imenovali (ni naključje, da je bil tudi prvi, ki je bil deležen
> biografije: psevdo-Manetti). Zato ne čaka na figurativne programe od zunaj,
> temveč jih določa avtonomno, pri čemer spodbuja nov način videnja in življenja
> v svetu z racionalnostjo, univerzalnostjo in konkretno sekularnostjo
> perspektivnih struktur. Njegova arhitektura je torej ideološka, saj predlaga
> poseganje v svet človeštva in njegovo spreminjanje s sprejemanjem lekcij
> zgodovine in arbitrarnim izbiranjem njenih prispevkov. Brunelleschi se je
> lahko enakovredno pogovarjal z antiko, hkrati pa si je z uvajanjem svojih
> prostorskih objektov v srednjeveško mesto kot odmevnikov racionalnih vrednot
> lahko dovolil priznati lastno kulturno kontinuiteto z nekaterimi izrazi
> toskanske romanike. Zgodovina zanj še ni bila zbirka abstraktnih kanonov (G.
> C. Argan, *Brunelleschi,* Milano, 1955; L. BenevoLo, *Storia dell'architettura
> del Rinascimento,* Bari, 1968; M. Tafuri, *Teorie e storia dell'architettura,*
> Bari, 1968). Če je Brunelleschi še vedno našel meščansko klientelo, ki je bila
> sposobna sprejeti njegove civilne predloge, pa razkorak med intelektualci in
> resničnostjo ni bil dolg. Albertijev že filološki historizem in univerzalizem
> sta bila ideološka kompenzacija za kulturno krizo, ki jo je bilo nujno
> občutiti. Ni naključje, da sta protoskanska sodišča v Milanu in Rimu poskušala
> izničiti vsebino humanistične revolucije in njene nevarne libertarne primere
> prevesti v retorični univerzalizem. Zgodovina zdaj postane vir novih načel
> avtoritete. Vitruvijevo besedilo, ki je bilo od konca 15\. stoletja dalje
> večkrat prevedeno in komentirano, je dobilo vrednost, ki jo je imel Aristotel
> za novosholastiko. K temu se je pridružila še kriza pokroviteljstva.
> Neofevdalizem šestnajstega stoletja je arhitektu odrekel promocijsko vlogo in
> mu kot polje svobode dodelil abstraktno oblast nad oblikami. To povzroči krizo
> v odnosu med arhitekturo in naravo ter v odnosu med umetniško prakso in
> zgodovino. Podobno kot je antiintelektualistični in skeptični naturalizem
> Pomponazzija ali Montaigna, se je tudi v arhitekturi s prvimi izkušnjami
> manierizma začelo spreminjati ravnovesje, ki ga je odlično izražal Bramantejev
> Palazzo Caprini, med delom človeka (arhitekturna racionalnost) in delom narave
> (rustikalni, antropomorfni in zoomorfni dekorativizem). Narava ni več
> asimilirana z Razumom; zdaj je bogata s skrivnostnimi, simbolnimi,
> nerazumskimi vrednotami. Ohrmuschelstil, nordijski Rollwerk, grozljivost
> Giulia Romana, okrasje Fontainebleauja, Serliovi portali so v ozračju
> arhitekturnega vitalizma nekje med ironijo, čistim čutnim hedonizmom in
> neformalnim antiklasicizmom. Nove fermentacije in stališča kontestacije se
> pomikajo proti limbu najbolj antideološkega scenografskega izmikanja. Hkrati
> je zgodovini odvzeta njena idealna vrednost in je zreducirana na instrument
> oblikovanja. Vignolinih *pet redov* je glavni dokument te dehistorizacije
> umetniškega ustvarjanja, ki je bila povezana z antihistorizmom
> protireformacije in še bolj z antihistorizmom neo-fevdalnih dvorov, predvsem
> dvora Farnese (F. Zeri, *Pittura e Controriforma*, Torino, 1957).
> Intelektualni temelji renesančne arhitekture, od historizma do novoplatonske
> ideologije, ki je bila osnova teorij posnemanja in enotnosti prostora, so bili
> tiho opuščeni. Cornarov čisti funkcionalizem je bil v odprtem nasprotju z
> znanstvenim intelektualizmom, Borromejevi predpisi, ki so izhajali iz razprav
> tridentinskega koncila, pa so arhitekta postavljali pred vnaprej določene
> programe, ki jim je lahko dal le primeren formalni videz. Ideologizem je
> zamenjala ciceronska teorija okrasja, Albertijevo junaško afirmacijo človeka
> je zamenjala obnovitev aristotelske retorike: Jezus v Rimu, Sveti Mihael v
> Münchnu ali preobrazbe Svetega Petra po Michelangelu so najočitnejši primeri
> tega. Arhitekti se v takšnih razmerah odzivajo z dvema temeljnima stališčema.
> Na eni strani so apokaliptiki, razočarani protestniki, predvsem pa
> Michelangelo: vendar njihova osebna drama ne najde izhoda, njihova kritika
> ostaja skoraj neproduktivna. Na drugi strani so integrirani, tisti, ki seveda
> sprejemajo izgubo vloge civilne avantgarde, ki jo je utrpel arhitekt. To so
> Antonio da Sangallo mlajši, Vignola, rimska šola s konca 16\. stoletja. Edini,
> ki je poskušal oblikovanju povrniti novo vrednost v okviru zahtev posebnih
> strank, kot je bila beneška aristokracija, je bil Palladio. Zgodovinopisno
> ideologijo je pripeljal do skrajnih posledic in realistično razrušil vse
> mitologije, ki so se nad njo nakopičile. [@tafuri1969rinascimento, 226-227]

::: {lang=it}
> Il profondo carattere ideo- logico dell'Umanesimo classicista si esprime
> attraverso due postulati di fondo, impliciti nell'architettura brunelleschiana
> e man mano resi principi teorici dal pensiero sull'arte e dalla speculazione
> filosofica: l'identità di Natura e Ragione da un lato, l'individuazione nella
> Classicità di una seconda e più perfetta Natura dall'altro. Il razionalismo e
> lo storicismo del Brunelleschi sono stati letti, insieme all'Umanesimo di
> Masaccio e Donatello, come i riflessi del nuovo spirito laico, concreto,
> rivolto a quantificare i valori qualitativi, dell'alta borghesia fiorentina
> del '300 e del primo '400 (A. Hauser, *Storia sociale dell'arte*, Torino,
> 1956, II; F. Antal, *La pittura fiorentina e il suo ambiente sociale nel
> Trecento e nel primo Quattrocento*, Torino, 1960). Ma giustamente il Garin ha
> sottolineato come fra eventi socio-politici ed economici e sviluppi artistici,
> esista, agli inizi del '400, una sfasatura: la stagione di massimo splendore
> della borghesia toscana è già al tramonto quando l'Umanesimo fiorentino inizia
> ad affermarsi (E. Garin, *Medioevo e Rinascimento*, Bari, 1966^3). Questa non
> è una contraddizione: il Brunelleschi, come l'Alberti o il Salutati, è un
> intellettuale in senso moderno, anzi il primo architetto cui si possa
> attribuire tale appellativo (non è casuale che sia anche il primo a
> beneficiare di una biografia: quella dello pseudo-Manetti). In quanto tale
> egli non attende dall'esterno i programmi figurativi, ma li determina
> autonomamente, promuovendo un nuovo modo di vedere e vivere nel mondo, tramite
> la razionalità, l'universalità, le concreta laicità delle strutture
> prospettiche. La sua architettura è quindi ideologica in quanto si propone di
> intervenire e modificare il mondo degli uomini accogliendo la lezione della
> storia e selezionandone arbitrariamente gli apporti. Brunelleschi può
> colloquiare da pari a pari con l'antico, ma contemporaneamente, introducendo
> nella città medievale i suoi oggetti spaziali come riverberatori di valori
> razionali, si può permettere di riconoscere la propria continuità culturale
> con alcune espressioni del Romanico toscano. La storia, per lui, non è ancora
> un serbatoio di astratti canoni (G. C. Argan, *Brunelleschi*, Milano, 1955; L.
> BenevoLo, *Storia dell'architettura del Rinascimento*, Bari, 1968; M. Tafuri,
> *Teorie e storia dell'architettura*, Bari, 1968). Se Brunelleschi trova ancora
> una committenza borghese capace di recepire le sue proposte civili, il
> distacco fra intellettuali e realtà non tarderà a verificarsi. Lo storicismo
> già filologico e l'universalismo dell'Alberti sono le compensazioni
> ideologiche di una crisi culturale sentita in modo impellente. Non è casuale
> che le corti antitoscane di Milano o di Roma tentino di affossare i contenuti
> della rivoluzione umanistica, traducendone le pericolose istanze libertarie in
> retorici universalismi. La storia diviene ora fonte di nuovi principi di
> autorità. Il testo di Vitruvio, tradotto e commentato più volte dal tardo '400
> in poi, assume il valore che ha Aristotele per la neoscolastica. A ciò si
> unisce la crisi della committenza. Il neofeudalesimo cinquecentesco nega
> all'architetto un ruolo promozionale, assegnandogli, come campo di libertà,
> l'astratto dominio delle forme. Ciò provoca la crisi delle relazioni fra
> architettura e natura, e quella delle relazioni fra prassi artistica e storia.
> In modo simile a quanto viene elaborato dal naturalismo antintellettualista e
> scettico di un Pomponazzi o di un Montaigne, anche in architettura
> l'equilibrio -- perfettamente espresso dal bramantesco palazzo Caprini -- fra
> opera dell'uomo (razionalità architettonica) e opera naturale (rustico,
> decorativismo antropomorfico e zoomorfico) comincia ad alterarsi con le prime
> esperienze del Manierismo. La Natura non è più assimilata dalla Ragione; essa
> è ora ricca di valori misterici, simbolici, antirazionali. L'Ohrmuschelstil,
> il Rollwerk nordico, l'orrido di Giulio Romano, le decorazioni di
> Fontainebleau, i portali del Serlio, sono nel clima di un vitalismo
> architettonico a metà fra l'ironia, il puro edonismo sensistico e un informale
> anticlassicismo. Fermenti nuovi e atteggiamenti di contestazione scivolano
> verso il limbo della più antideologica evasione scenografica. Nello stesso
> tempo la Storia viene privata di valore ideale e ridotta a strumento di
> progettazione. I *Cinque ordini* del Vignola sono il documento principe di
> questa destoricizzazione del fare artistico, che è stato messo in relazione
> con l'antistoricismo della Controriforma e, ancor più, con quello delle corti
> neofeudali: prima fra tutte quella farnesiana (F. Zeri, *Pittura e
> Controriforma*, Torino, 1957). Le basi intellettuali dell'architettura del
> Rinascimento, dallo storicismo all'ideologia neoplatonica che era stata alle
> basi delle teorie dell'imitazione e dell'unità dello spazio, vengono
> tacitamente messe da parte. Il funzionalismo puro del Cornaro polemizza
> apertamente con l'intellettualismo di stampo erudito e i precetti del
> Borromeo, scaturiti dalle discussioni del Concilio Tridentino, tendono a porre
> l'architetto di fronte a programmi prefissati cui dare solamente un'acconcia
> veste formale. All'ideologismo si sostituisce la teoria ciceroniana del decor,
> all'albertiana affermazione eroica dell'uomo il recupero della retorica
> aristotelica: il Gesù a Roma, il S. Michele a Monaco, o le trasformazioni del
> S. Pietro dopo Michelangelo ne sono le più evidenti testimonianze. In tale
> situazione gli architetti rispondono con due atteggiamenti fondamentali. Da un
> lato sono gli apocalittici, i contestatori frustrati, primo fra tutti
> Michelangelo: ma il loro dramma personale non trova sbocchi, la loro critica
> rimane pressoché improduttiva. Dall'altro sono gli integrati, coloro che
> accettano per naturale adesione la perdita del ruolo di avanguardia civile
> subita dall'architetto. Sono Antonio da Sangallo il Giovane, il Vignola, la
> scuola romana del tardo '500. L'unico a tentare di restituire un valore nuovo
> alla progettazione, all'interno delle richieste di una committenza particolare
> come quella dell'aristocrazia veneta, è Palladio. L'ideologia storicistica è
> infatti da lui portata alle estreme conseguenze, nella realistica demolizione
> di tutte le mitologie che su di essa si erano accumulate.
:::

:::

::: alts
> Posebno poglavje bi bilo treba nameniti razmerju med arhitekturo in ideologijo
> protestantske reformacije. Tudi tu lahko prepoznamo dve poti arhitektov, ki
> ustrezata dvema dušama protestantizma. Na eni strani skepsa glede človeških
> vrednot, pojmovanje minljivosti narave in sveta, senzualistični in
> iracionalistični fermenti, značilni za Kalvinove in Luthrove spise: tu pa sta
> nemirna horror vacui Knorpelwerka in antihumanistična groza Dietterlina. Po
> drugi strani pa je poudarek na strogosti, vrednosti introspekcije in intimne
> izkušnje spodbudil strogost številnih protestantskih arhitektov: Salomona de
> Brossa (med drugim avtorja templja v Charentonu) v Franciji, Eliasa Holla v
> Augsburgu, H. de Keyserja na Nizozemskem. Na patetičnost podob in okultna
> prepričanja rimske Cerkve so se te izkušnje odzvale s puristično ideologijo,
> ki pa je bila tudi zadnja rešilna bilka za nekatere globoko izmučene katoliške
> intelektualce, kot sta bila Ammannati iz rimskega kolegija ali Herrera iz
> katedrale Escorial in Valladolid. Vendar je bil do konca 16\. stoletja visok
> moralni in civilni mandat, ki ga je humanizem prenesel na arhitekturo,
> popolnoma ogrožen, prizadevanja za individualno odrešitev pa so pričala o
> propadu iluzije iz 15\. stoletja o avtonomni in dejavni vlogi intelektualcev,
> razumljenih kot vodilni razred, in hkrati o krizi kozmopolitskega poslanstva
> italijanskega renesančnega intelektualca. [@tafuri1969rinascimento, 227]

::: {lang=it}
> Un capitolo a parte va riservato alle relazioni fra l'architettura e
> l'ideologia della Riforma protestante. Anche qui si possono riconoscere due
> vie battute dagli architetti, che corrispondono alle due anime del
> protestantesimo. Da un lato, lo scetticismo sui valori dell'uomo, il concetto
> della deperibilità della natura e del mondo, i fermenti sensualistici e
> irrazionalisti tipici degli scritti di Calvino e Lutero: ed ecco l'inquieto
> horror vacui del Knorpelwerk e l'orrido antiumanistico del Dietterlin.
> Dall'altro, l'accento posto sull'austerità, sul valore dell'introspezione e
> dell'intima esperienza stimolano il rigorismo di molti architetti protestanti:
> di Salomon de Brosse (autore, fra l'altro, del tempio di Charenton) in
> Francis, di Elias Holl ad Augsburg, di H. de Keyser in Olande. Al patetismo
> delle immagini e alle persuasioni occulte della Chiesa di Roma, queste
> esperienze rispondono con un'ideologia purista, che, tuttavia, è anche
> l'ultima ancora di salvezza per alcuni intellettuali cattolici profondamente
> tormentati come "Ammannati del Collegio Romano o l'Herrera dell'Escorial e
> della cattedrale di Valladolid. Comunque, alle fine del '500 l'alto mandato
> morale e civile che l'Umanesimo aveva trasmesso all'architettura è del tutto
> compromesso, e gli sforzi tesi a riscatti individuali testimoniano il crollo
> dell'illusione quattrocentesca circa i ruolo autonomo ed attivo degli
> intellettuali intesi come classe-guida e, contemporaneamente, la crisi della
> vocazione cosmopolita dell'intellettuale italiano del Rinascimento.
:::

:::

### [Traktatistika], tipologija, modeli

::: alts
> Razcvet obsežne teoretske produkcije v renesančni dobi je povezan s temelji, na
> katerih temelji klasicistična hipoteza. Prevzem vloge intelektualca je za
> umetnika 15\. in 16\. stoletja pomenil ne le uveljavljanje novega osebnega
> dostojanstva, temveč tudi in predvsem to, da je v umetnosti prepoznal gonilno in
> aktivno vrednost znotraj novih razredov na oblasti, ki so v ideološke programe -- 
> na posebnem področju vizualne komunikacije -- prevajali najbolj napredne
> državljanske zahteve. Zato ima traktat več nalog. Na idealni ravni je glavni
> kanal dialoga z zgodovino in antiko ter zagotavlja prenosljivost in dovršenost
> izkušnje. Na jezikovni ravni opredeljuje kodeks, ki se lahko odziva na
> univerzalne in kozmopolitske naloge nove umetnosti. Na ravni proizvodnih odnosov
> sankcionira novo delitev dela, saj racionalizacija metode projektiranja ustreza
> globoki revoluciji na gradbišču, v organizaciji njegovih delavcev, v abstraktni
> teoriji, tipološki analizi, zgodovinsko-arheološki analizi, praktičnih
> standardih projektiranja in tehnoloških kompendijih, v spisih "romantikov 15\.
> stoletja" -- Filarete in Francesco Colonna -- in v kodeksih antikvarjev -- od Masa
> Finigucerra do Ciriaca da Ancona -- antika že šteje kot čista priložnost za
> mitske evokacije: Filaretijeva Sforzinda niha med dostojanstvom metodološkega
> modela in zabavnim eskapizmom (Schlosser; J. R. Spencer, Filarete's Treatease on
> Architecture, New Haven, 1965, 2 zvezka; A. Bruschi, Orientamenti di gusto e
> indicazioni di teoria in alcuni disegni del '400, "Quad. dell'Ist. di Stor.
> dell'Archit.", 1967, II). [@tafuri1969rinascimento, 229]

::: {lang=it}
> Il fiorire di una vasta produzione teorica nell'età del Rinascimento è
> connesso alle basi stesse su cui poggia l'ipotesi classicista. Assumere il
> ruolo dell'intellettuale, significa, per l'artista del '400 e del '500, non
> solo rivendicare una nuova dignità personale, ma anche, e principalmente,
> riconoscere nell'arte un valore propulsivo e attivo in seno alle nuove classi
> al potere, traducendone in programmi ideologici -- nel settore specifico delle
> comunicazioni visive -- le più progressive istanze civili. La trattatistica ha
> quindi più compiti da svolgere. Sul piano ideale essa è il tramite principale
> di colloquio con la storia e l'antichità e assicura la trasmissibilità e la
> perfettibilità delle esperienze. Sul piano linguistico essa definisce un
> codice, capace di rispondere ai compiti universali e cosmopoliti della nuova
> arte. Sul piano dei rapporti di produzione sancisce la nuova divisione del
> lavoro, dato che alla razionalizzazione del metodo di progettazione
> corrisponde una profonda rivoluzione nel cantiere, nell'organizzazione delle
> sue maestranze, nei e si sovrappongono teoria astratta, analisi tipologiche,
> analisi storico-archeologica, norme pratiche di progettazione e compendi
> tecnologici, negli scritti dei "romantici del '400" -- il Filarete e Francesco
> Colonna -- e nei codici degli antiquari -- da Maso Finigucerra a Ciriaco da
> Ancona -- l'antico vale già come pura occasione di mitiche evocazioni: la
> Sforzinda filaretiana oscilla fra la dignità di un modello metodologico e la
> divertita evasione (Schlosser; J. R. Spencer, *Filarete's Treatease on
> Architecture*, New Haven, 1965, 2 voll.; A. Bruschi, *Orientamenti di gusto e
> indicazioni di teoria in alcuni disegni del '400*, "Quad. dell'Ist. di Stor.
> dell'Archit.", 1967, II).
:::

:::

::: alts
> Spričo Albertijevega univerzalizma dobijo spisi Francesca di Giorgia veliko
> bolj empiričen značaj. Celo antropomorfizem in pogovor s klasicizmom sta odeta
> v eksperimentalno tesnobo, ki jo jasno filtrira poetika, pozorna na merjenje
> arhitekturnih modelov in tipov po merilu nepristranskega empirizma. Medtem ko
> je Francesco di Giorgio težil k zamenjavi osebnega "manifesta" z univerzalnim
> kodeksom in je Leonardo v svojem asistematizmu postavil prvenstvo empirizma na
> vrh metode, ki je bila tako daleč od vitruvijevske kazuistike, da je bila
> opredeljena kot "protiklasicistična", se je v krogih, ki so se ukvarjali z
> nasprotovanjem politični in kulturni prevladi Firenc, uveljavila alternativna
> hipoteza, filološka analiza Vitruvijevega besedila. [@tafuri1969rinascimento,
> 229-230]

::: {lang=it}
> Di fronte all'universalismo albertiano gli scritti di Francesco di Giorgio
> assumono un carattere assai più empirico. Persino l'antropomorfismo e il
> colloquio con la Classicità si rivestono di un'ansia sperimentale, chiaramente
> filtrata attraverso una poetica attenta a misurare modelli e tipi
> architettonici sul metro di una spregiudicata empiria. Mentre Francesco di
> Giorgio tende a sostituire il "manifesto" personale al codice universale, e
> Leonardo, nella sua asistematicità, pone il primato dell'empiria al vertice di
> un metodo che prescinde tanto dalla casistica vitruviana da poter essere stato
> definito "anticlassico", negli ambienti che ai preoccupano di opporre al
> predominio politico e culturale di Firenze un'ipotesi alternativa, prende
> piede l'analisi filologica del testo di Vitruvio.
:::

:::

::: alts

> Vitruvijeva filologija se je rodila v Rimu in Milanu -- mestih, ki sta bili v
> 15\. stoletju oddaljeni od civilnega in družbenega gibanja florentinskega
> humanizma. 1486 je Sulpicio da Veroli napisal aeditio princeps De
> architectura, 1521 pa je Cesare Cesariano izdal prvi italijanski prevod,
> opremljen z ilustracijami. Vitruvijske študije od konca 15\. do konca 16\.
> stoletja so tako tvorile samostojno teoretično smer. V komentarjih in grafikah
> v zvezkih Fra Gioconda (1511), Caporalija (1536), Barbaro (1556), Bertanija
> (1558), Rusconija (1590) ter v francoskih besedilih Martina (1547) in
> Philandra (1550) se je ohranila želja, da bi z arheološkimi in filološkimi
> sredstvi dosegli strogo določen kodeks, ki ga je iz dneva v dan spodkopavala
> oblikovalska izkušnja. Toda tudi arheološka obnova antike, ki je bila izbrana
> kot druga in popolnejša Narava, je kmalu zašla v krizo. Ko je Claudio Tolomei
> leta 1540 ustanovil Vitruvijevo akademijo z nalogo, da dokončno razreši
> protislovja obskurnega latinskega besedila in tista, ki so izhajala iz
> primerjave z monumentalnimi ostanki, sta postali jasni polivalentnost samega
> klasicizma ter nevzdržnost njegove redukcije in zgodovinskega modela. Zato se
> odpreta dve nasprotujoči si poti, ki ju je mogoče povzeti le na področju
> oblikovanja: na eni strani institucionalizacija oblikovalskih kodeksov, na
> drugi strani pa radovedno in nemirno raziskovanje meja "herezije", ki jih ti
> isti kodeksi dopuščajo. Prva pot vodi na področju italijanske kulture do
> Vignolove knjige Regola delli cinque ordini (1562), druga pa do Serliovega
> modeliranja in prek njega do Du Cerceauja, de Vriesa, Cattanea, Montana in
> Giorgia Vasarija mlajšega. Z drugimi besedami, medtem ko se na eni strani
> abstraktno in ahistorično fiksirajo leksikalne prvine klasicizma, pri čemer se
> razčlenjujejo njegove skladenjske zakonitosti, da bi bile na voljo za
> neskončno število aplikacij (z drugimi besedami, zadnji napor je
> univerzalizacija arhitekturnega "jezika"), se na drugi strani ta problem
> odloži in se osredotoča na notranje skladenjske artikulacije, pri čemer se
> vztrajno išče nove figurativne in prostorske modele ter kompleksne načine
> njihovega kombiniranja. Norma in herezija; pomembno je, da sta na mednarodni
> ravni sprejeti in sintetizirani obe poti. Ne glede na to, da ima evropski
> vitruvianizem -- spomnimo se na prevode Riviusa (1548), Bluma (1550) in
> zbornik Shute (1563) -- svojo lastno pot in je daleč od arheološkega
> filologizma, ki oživlja besedila Barbare ali Bertanija, je njegov prispevek
> podoben. Za nemške ali flamske arhitekte so arhitekturni redi, kot je dobro
> ugotovil Forssman, ne toliko racionalne strukture kot magični elementi (E.
> Forssman, Säule und Ornament, cit.). V nordijski normi konflikt med normo in
> herezijo v bistvu ostaja, vendar je konfiguriran paradoksalno: norma ni
> Vignolova modularnost, temveč predmet nenehnega ponovnega izumljanja, model,
> nestabilen v svoji konfiguraciji, vendar prisoten in delujoč kot idealna vrsta
> reference. De Vries, Dietterlin in Sambin s svojimi deli dokazujejo, kar je
> bilo rečeno. Pomembno pa je poudariti, kako se v nordijskih obravnavah teorija
> in grafična ponazoritev dopolnjujeta. Kar je v Italiji razcepljeno -- na eni
> strani vitruvijski kanoni, na drugi Serliovi modeli -, je v evropskih
> besedilih začasno ponovno združeno. Ti zapleteni fermenti vodijo do odnosa med
> oblikovanjem in traktati, ki se vsi rešujejo na področju čistih podob.
> Vignola, ko govori o modulu kot arbitrarnem merilu, izrecno povzdiguje njegovo
> antiintelektualistično vrednost. Z njim bo "vsak povprečen intelekt ... lahko
> vse razumel na prvi pogled, brez večjega truda z branjem, in ga ustrezno
> uporabil" (Regola delli cinque ordini, 1562, str. 2). Smo natanko na področju
> antiintelektualistične tipologije, kakršna se -- v nasprotnem smislu --
> pojavlja v Serlijevem delu. Intelektualizem, ki v zvezkih Albertija, Luce
> Paciolija ali Piera della Francesca oblikuje perspektivno in modularno
> teorijo, je v Vignolijevem priročniku zreduciran na čisto podporo
> poenostavljeni operativni praksi. Arheološka erudicija, ezoterični simbolizem
> in eksperimentalizem, ki so podlaga za tipološke raziskave Bramanteja ali
> Peruzzija, so v Serliovih knjigah zreducirani na empirično kombinacijo podob.
> Vendar je prav ta antiintelektualizem Serliov največji prispevek. Z njim
> manieristična kultura odkrije nove stopnje svobode pri reševanju svojih
> problemov in ni naključje, da bo Serlijev empirizem postal eden od najbolj
> veljavnih instrumentov, ki povezujejo italijansko in evropsko ter celo
> neevropsko kulturo. Poleg tega so antiintelektualistični fermenti raztreseni
> po vsej arhitekturni misli 16\. stoletja in pričajo o njegovi udeležbi v
> omejenem, a globokem gibanju radikalnega spodbijanja globoko zakoreninjenih
> mitov in konvencij, ki je v samem območju renesančne kulture pripravilo
> temelje za kulturno revolucijo nove znanstvene misli. Če je Serlio iskal
> posrednike med konvencijami, erudicijo in empirijo, je Alvise Cornaro v svojem
> diletantizmu brez oklevanja razglašal prednost empirije in hvalil "vedno bolj
> častno lepo, a popolnoma udobno fabbrico kot lepo in neudobno" (G. Fiocco,
> Alvise Cornaro and his treatises on architecture, "Atti dell'Acc. Naz. dei
> Lineei", Rim, 1952, VIII, zv. IV, fasc. 3; G. Fiocco, Alvise Cornara e le sue
> opere, Benetke, 1966). Kdor v resnici posreduje med intelektualizmom,
> historicizmom in empirizmom, je, tudi kot teoretik, Palladio. Poleg tega je
> bilo upravičeno ugotovljeno, da Palladiovo kulturno poreklo leži na dveh
> področjih, ki si le navidezno konkurirata: univerzalistični in eruditski
> akademizem G. G. Trissina in Cornarov empirizem sta dva obraza aristokratske
> ideologije, ki je botrovala zemljiški in gosposki invenciji, ki je sprožila
> proces intenzivne zemljiške kapitalizacije v Benečiji 16\. stoletja (I. Puppi,
> Appunti su villa Badoer di Fratta Polesine, "Memorie dell'Acc. patavina di SS.
> LL. AA.", 1966, zv. LXXVIII, str. 47 in naslednje). Sinteza A. Palladia je v
> njegovih Quattro libri (1570) prevedena na edini možni način za subjektivno
> dejanje, ki se želi postaviti za vzor ravnanja: v "manifestu", utemeljenem na
> osebnih izkušnjah z oblikovanjem. Norma in herezija nista več v nasprotju:
> dejstvo, da bodo Palladijeve izbire več kot dve stoletji veljale za
> nadzgodovinske modele, je posledica nove dostopnosti klasicističnega leksikona
> in novega pomena, ki so ga na tem področju dobile tipološke raziskave (E.
> Forssman, Palladio's Lehrgebäude, Stockholm 1963; J. S. Ackerman, Palladio,
> Harmondsworth 1966; L. Benevolo, Storia dell'architettura del Rinascimento,
> cit. delo). [@tafuri1969rinascimento, 230]

::: {lang=it}

> A Roma e a Milano -- città lontane, per tutto il '400, dal moto civile e
> sociale dell'Umanesimo fiorentino -- nasce la filologia vitruviana; nel 1486
> Sulpicio da Veroli redige la sua aeditio princeps del *De architectura* e nel
> 1521 Cesare Cesariano ne dà la prima traduzione italiana, corredata da
> illustrazioni. Gli studi vitruviani dalla fine del sec. XV alla fine del XVI
> costituiscono così un indipendente filone teorico. Nei commenti e nelle
> incisioni dei volumi di fra' Giocondo (1511), del Caporali (1536), del Barbaro
> (1556), del Bertani (1558), del Rusconi (1590), e nei testi francesi del
> Martin (1547), e del Philander (1550), vive l'ansia di raggiungere, per via
> archeologica e filologica, quel codice rigorosamente definito che l'esperienza
> di progettazione metteva in crisi giorno per giorno. Ma anche il recupero
> archeologico dell'antichità, eletta a seconda e più perfetta Natura, entra ben
> presto in crisi. Quando nel 1540 Claudio Tolomei fonda l'Accademia vitruviana,
> con il compito di dirimere definitivamente sia le contraddizioni dell'oscuro
> testo latino, che quelle sorte dal suo confronto con i resti monumentali, è
> ormai chiara la polivalenza della Classicità stessa, e l'insostenibilità ci
> una sua riduzione e modello storico. Si aprono quindi due vie opposte,
> sintetizzabili solo nell'ambito della progettazione: da un lato
> l'istituzionalizzazione dei codici di progettazione, dall'altro l'esplorazione
> curiosa cd inquieta dei margini di "eresia" concessa da quegli stessi codici.
> La prima via conduce, nell'ambito dello cultura italiana, alla *Regola delli
> cinque ordini del Vignola* (1562); la seconda al modellismo del Serlio, e, per
> suo tramite, del Du Cerceau, del de Vries, del Cattaneo, del Montano, di
> Giorgio Vasari junior. In altre parole, mentre da un lato si fissano in modo
> astorico ed astratto gli elementi lessicali del Classicismo, smembrandone le
> leggi sintattiche al fine di renderlo disponibile per una gamma infinita di
> applicazioni (si fa l'ultimo sforzo, in altre parole, di universalizzare la
> "langue" architettonica), dall'altro si mette fra parentesi tale problema e si
> punta al contrario sulle interne articolazioni sintattiche, in uno ostinata
> ricerca di modelli figurativi e spaziali inediti, e di complessi modi di
> combinazione. Norma ed eresia, dunque; ed è significativo che sul piano
> internazionale entrambe queste vie vengano recepite e sintetizzate. Per quanto
> il vitruvianesimo europeo -- ricordiamo le traduzioni del Rivius (1548), del
> Blum (1550) e il volume dello Shute (1563) -- abbia un percorso tutto proprio
> e ben lontano dal filologismo archeologico che anima i testi del Barbaro o del
> Bertani, il suo contributo è analogo. Per gli architetti tedeschi o fiamminghi
> gli ordini architettonici sono, come ha ben notato il Forssman, non tanto
> strutture razionali, quanto elementi magici (E. Forssman, *Säule und
> Ornament*, cit.). Nella normativa nordica, in sostanza, permane il conflitto
> fra norma ed eresia, ma si configura in termini paradossali: la norma non è la
> modularità vignoliana, ma un oggetto di continua rinvenzione, un modello
> instabile nella sua configurazione, eppure presente e operante come tipo
> ideale di riferimento. I volumi del de Vries, del Dietterlin, del Sambin
> provano quanto si è detto. Ed è importante far risaltare come, nella
> trattatistica nordica, teoria ed esemplificazione grafica siano fra loro
> complementari. Ciò che in Italia si presenta scisso -- da un lato i canoni
> vitruviani, dall'altro i modelli del Serlio -- si riunifica, provvisoriamente,
> nei testi europei. Questi complessi fermenti conducono ad una relazione fra
> progettazione e trattatistica tutta risolta nell'ambito delle pure immagini.
> Il Vignola, nel parlare del modulo come misura arbitraria, ne esalta
> esplicitamente il valore antiintellettualistico. Per suo tramite "ogni
> mediocre ingegno... potrà in un'occhiata sola, senza gran fastidio di leggere,
> comprendere il tutto, et opportunamente servirsene" (*Regola delli cinque
> ordini*, 1562, p. 2). Siamo esattamente nell'ambito di una tipologia
> antintellettualistica, così come -- nelle sua opposta accezione -- avviene
> nell'opera del Serlio. L'intellettualismo che informa la teoria prospettica e
> modulare, nei volumi dell'Alberti, di Luca Pacioli o di Piero della Francesca,
> è ridotto a puro sostegno di una semplificata prassi operativa nel manuale
> vignolesco. L'erudizione archeologica, l'esoterico simbolismo, lo
> sperimentalismo, che informano la ricerca tipologica di un Bramante o di un
> Peruzzi, si riducono nei libri del Serlio ad empirica combinazione di
> immagini. È però proprio tale antintellettualismo l'apporto maggiore dato dal
> Serlio. Per suo tramite la cultura manierista scopre nuovi gradi di libertà
> nella soluzione dei suoi problemi, e non è casuale che l'empirismo serliano
> diverrà uno dei più validi strumenti di collegamento fra cultura italiana e
> cultura europea, e persino extraeuropea. Fermenti antintellettualistici, del
> resto, sono sparsi in tutto il pensiero architettonico del '500, a
> testimoniare della sua partecipazione al limitato ma profondo moto di radicale
> contestazione dei miti e delle convenzioni radicate che prepara, nell'ambito
> stesso della cultura rinascimentale, le basi per la rivoluzione culturale del
> nuovo pensiero scientifico. Se il Serlio cerca mediazioni fra convenzioni,
> erudizione ed empirismo, Alvise Cornaro, nel suo dilettantismo, non esita a
> proclamare il primato dell'empiria, lodando "sempre più lo fabbrica
> honestamente bella, ma perfettamente commoda, che la bellissima e incommoda"
> (G. Fiocco, *Alvise Cornaro e i suoi trattati sull'architettura*, "Atti
> dell'Acc. Naz. dei Lineei", Roma, 1952, VIII, vol. IV, fasc. 3; G. Fiocco,
> *Alvise Cornara e le sue opere*, Venezia, 1966). Chi, in realtà, media
> intellettualismo, storicismo ed empiria è, anche come teorico, il Palladio. È
> stato del resto giustamente osservato che le origini culturali del Palladio
> affondano in due ambiti solo apparentemente in competizione fra loro:
> l'accademismo universalistico ed erudito di G. G. Trissino e l'empirismo del
> Cornaro sono i due volti di un'ideologìa aristocratica che dà vita
> all'involuzione fondiario e signorile che nel Veneto cinquecentesco mette in
> moto un processo di intensa capitalizzazione fondiaria (I. Puppi, *Appunti su
> villa Badoer di Fratta Polesine*, "Memorie dell'Acc. patavina di SS. LL. AA.",
> 1966, vol. LXXVIII, p. 47 sgg.). La sintesi di A. Palladio, si traduce, nei
> suoi *Quattro libri* (1570), nell'unico modo possibile per un atto soggettivo
> che voglia porsi come modello di comportamento: in un "manifesto", fondato
> sull'esperienza personale di progettazione. Norma ed eresia non sono più in
> contrasto: il fatto che le scelte palladiane assumeranno il valore di modelli
> soprastorici per più di due secoli è dovuto alla nuova disponibilità del
> lessico classicista e al nuovo significato dato, in questo ambito, alla
> ricerca tipologica (E. Forssman, *Palladio's Lehrgebäude*, Stockholm, 1963; J.
> S. Ackerman, *Palladio*, Harmondsworth, 1966; L. Benevolo, *Storia
> dell'architettura del Rinascimento*, cit.). [230]
:::

:::

::: alts

> Razdelitev teoretske misli na različne smeri je omogočila intenzivno literarno
> produkcijo o umetnosti, ki si je skušala povrniti intelektualni prestiž, ki
> sta ga ogrozila abstraktna specializacija vitruvijancev in Serliov empirizem.
> Opozoriti je treba, da je pri Varchiju in Lomazzu prisotna stalna polemika
> proti kodificiranim kanonom in Serlijevim razkritjem: proti empirizmu stojijo
> naporne teoretske konstrukcije V. Danti (Il primo libro delle perfette
> proporzioni, Firence, 1567), B. Varchii (Due lezioni ..., Firence, 1549), G.
> P. Lomazzo (Trattato dell'arte della pittura, Milano, 1584), F. Zuccari
> (L'idea de' Pittori, Scultorî et Architetti, Torino, 1607). Aristotelizem,
> platonizem, michelangelizem in protireformacijski moralizem so vir informacij
> za ta besedila: na razkroj humanističnih gotovosti odgovarja zavračanje
> empirizma in poskus umestitve novih gotovosti v metafizično abstrakcijo (M.
> Rosci, Mannerism and Academicism in the Critical Thought of the 1500s, "Acme",
> 1956, št. 1; M. Tafuri, L'idea di architettura nella letteratura teorica del
> Manierismo, "B. del Centro A. Palladio", IX, 1967, str. 369-82).
> [@tafuri1969rinascimento, 230]

::: {lang=it}

> La scissione del pensiero teorico in distinti filoni permette un'intensa
> produzione letteraria sulle arti, che tenta di recuperare il prestigio
> intellettuale compromesso dallo specialismo astratto dei vitruviani e
> dall'empirismo del Serlio. È da notare che nel Varchi e nel Lomazzo è costante
> la polemica contro i canoni codificati e le divulgazioni serliane: contro
> l'empirismo si ergono le faticose costruzioni teoriche di V. Danti (*Il primo
> libro delle perfette proporzioni*, Firenze, 1567), B. Varchii (*Due
> lezioni...*, Firenze, 1549), G. P. Lomazzo (*Trattato dell'arte della
> pittura*, Milano, 1584), F. Zuccari (*L'idea de' Pittori, Scultorî et
> Architetti*, Torino, 1607). Aristotelismo, platonismo, michelangiolismo,
> moralismo controriformistico informano tali testi: alla dissoluzione delle
> certezze umanistiche si risponde con il ripudio dell'empiria e con un
> tentativo di riporre sicurezze nuove nell'astrazione metafisica (M. Rosci,
> *Manierismo e accademismo nel pensiero critico del '500*, "Acme", 1956, n. 1;
> M. Tafuri, *L'idea di architettura nella letteratura teorica del Manierismo*,
> "B. del Centro A. Palladio", IX, 1967, pp. 369--82). [230]
:::

:::

## O Brunelleschijevem prispevku h kupoli na Santa Maria del Fiore

::: {lang=en}

> The technical "miracle" of the dome of Santa Maria del Fiore has distracted
> critics not a little from the significance which that long and strenuous
> constructive labour holds in the art of Brunelleschi. Since it is known that
> Filippo had originally planned to make the dome in the form of a hemisphere,
> and that only on second thoughts did he decide to carry out the scheme laid
> down in Arnolfo's model, the problem of the dome would seem to be reduced to a
> mere question of technique: the method of vaulting it without scaffolding.
> 
> Was it really technically impossible to realize Arnolfo's plan by the usual
> means? One may easily believe that, in those first decades of the
> Quattrocento, no artist would have dared to build vaulting on so vast a scale;
> it is indeed highly probable that throughout the Trecento, when decoration
> took precedence of construction, there may have been a falling-off in
> constructive skill. But it is impossible to believe that Arnolfo can have
> planned, and his successors raised as far as the drum, a building which the
> technical resources of the time did not permit them to roof over.
> 
> What is more, Brunelleschi never even thought of using the traditional
> technique. From the outset he had in mind the idea of building the dome
> without scaffolding; he might give up the form he had first envisaged, but he
> would not give up his method of construction. Only a mistaken estimate of
> Brunelleschi's "classicism" has induced the belief that the spherical vault
> represented a formal ideal, later sacrificed to contingent needs. When we
> remember that the method of vaulting the dome without scaffolding had been
> deduced from the Roman circular domes, the terms of the question are reversed:
> the most reasonable hypothesis is that Filippo had thought first of a
> semi-circular vault because it was from such models that he had evolved his
> system, and that he returned later to Arnolfo's plan when he had become
> persuaded that the system might equally well be applied to domes with ribs and
> pointed arches. This method, which the conclusive researches of Sampaolesi
> have shown to be of Roman origin, consists in walling the dome with courses of
> bricks disposed in a herring-bone pattern. Brunelleschi's formal ideal did not
> end in the pattern of the pointed arch or of the single span: it was the ideal
> of a form capable of sustaining itself throughout the process of its own
> growth, of producing the force that sustains it, of disposing itself in space
> by virtue of its own interior structural coherence and vitality, by its
> natural proportionality, like that of "bones and members."
> 
> The herring-bone method of construction is applied in Santa Maria del Fiore,
> on a much larger scale than it is in any of the ancient models, that is to
> say, to the measurements of the drum already constructed. The problem set by
> Brunelleschi consisted therefore in reducing a gothic *dimension* to
> *proportion* through the principle of self-support, that is of the autonomy of
> the form in space. Thus the double vault of the dome finds a justification not
> only practical but figurative (in the actual words of Filippo "so that it may
> appear more enlarged and splendid"): the artist feels the need for
> establishing an exact relation between the form of the dome and the various
> properties of space that are summed up in it. In the interior the curvature of
> the surfaces of the octagon, sums up and co-ordinates the various spatial
> trends of the naves and the presbytery, as into a common horizon; on the
> exterior the ribs mark the limit or the juncture between the masses of the
> building and the circumambient space. If the effect of the dome is spatial,
> the process which leads to the definition of space is a constructive process.
> But this constructive labour differs from the mediaeval *mechanica* because
> its acts are no longer repeated by tradition, but determined by reason: the
> coherence of these acts must there be referred to a rational principle.
> Manetti says that in Rome Brunelleschi "saw the ancients' methods of building
> and their symmetry; and it seemed to him that he saw there very clearly a
> certain order, as of bones and members." It is not a question of the generic
> anthropomorphism that recurs, following on the traces of Vitruvius, in the
> treatise writers of the Renaissance: it is a question of rational
> discrimination between the elements that bear and the elements that are borne,
> and of their distribution according to order, that is according to symmetry
> and proportion. [@argan1946thearchitecture, 105-107]

:::

## Arhitektura, znanost, tehnologija

:::: alts

> Teme novih znanosti se v vsem tem razbijanju, rekomponiranju, kršenju pridobljenih zakonov perspektive in vizualnih kodov vsekakor izražajo le metaforično.
> Toda odkritje možnosti obravnave prostora kot pravega "laboratorija" avantgardni arhitekturi 16. stoletja omogoča, da vnaprej raziskuje policentrični formalni univerzum, ki naj bi ga uresničila "znanstvena reforma": tako zelo, da je postulat, ki je umetnosti pripisoval neposredno spoznavno vrednost, razjedla do te mere, da je postal zastarel.
> Manieristična kritika je tudi na tem področju prej destruktivna kot propozicionalna: prav na tem področju mora zgodovinska presoja o njej najti svoje parametre.
>
> In čeprav ni naš namen nakazati "zaključka" obravnave, ki smo jo namenoma načrtovali kot povsem odprto, je treba reči, da je relativna homogenost obdobja, ki smo ga doslej povzeli, prav v tej zgodovinski nalogi, ki jo prevzame arhitektura humanizma: v afirmaciji racionalne in znanstvene vrednosti umetniškega dela, da bi bolje -- in brez možnosti pritožbe -- porušili "progresivno" funkcijo same umetnosti.
>
> To je rušenje, ki se seveda zgodi kljub volji protagonistov humanizma, vendar ostaja, da nedvoumno priča o globoki samokritični poklicanosti evropskega racionalizma.
[@tafuri1972larchitettura, 353-354]

::: {lang=it}

> I temi delle nuove scienze sono certo espressi solo metaforicamente in tutto quello scomporre, ricomporre, violentare le leggi prospettiche e i codici visivi acquisiti.
> Ma lo scoprire la possibilità di trattare lo spazio come un vero e proprio 'laboratorio' permette, all'architettura di avanguardia del '500, di esplorare in anticipo l'universo formale policentrico che la "riforma scientifica" renderà realtà: tanto da corrodere, fino a renderlo inattuale, il postulato che aveva attribuito all'arte un valore immediatamente conoscitivo.
> Il criticismo manierista, anche in questo settore, è distruttivo più che propositivo: è in tale ambito che il giudizio storico su di esso deve trovare i propri parametri.
>
> E malgrado non sia nostra intenzione indicare una 'conclusione' per una trattazione che volutamente abbiamo progettato come del tutto aperta, va pur detto che la relativa omogeneità del periodo che abbiamo fino ad ora sintetizzato risiede proprio in questo compito storico che l'architettura dell'Umanesimo si assume: l'affermazione del valore razionale e scientifico dell'operare artistico per meglio demolire -- e senza possibilità di appello -- la funzione 'progressiva' dell'arte stessa.
>
> Una demolizione che avviene, certo, malgrado le volontà dei protagonisti dell'Umanesimo, ma che rimane a testimoniare inequivocabilmente la profonda vocazione autocritica del razionalismo europeo.

:::

:::

---
lang: sl
references:
- type: book
  id: tafuri1972larchitettura
  author:
  author:
  - family: Tafuri
    given: Manfredo
  title: "L'architettura dell'umanesimo"
  publisher-place: Bari
  publisher: Laterza
  issued: 1972
  language: it
- type: entry-dictionary
  id: tafuri1969rinascimento
  author:
  - family: Tafuri
    given: Manfredo
  title: "Rinascimento"
  editor:
  - family: Portoghesi
    given: Paolo
  container-title: "Dizionario enciclopedico di architettura e urbanistica 5: Posnik-Sipario"
  publisher-place: Rim
  publisher: Istituto editoriale romano
  issued: 1969
  page: 173-232
  language: it
- type: article-journal
  id: argan1946thearchitecture
  author:
  - family: Argan
    given: Giulio Carlo
  title: "The architecture of Brunelleschi and the origins of perspective theory in the fifteenth century"
  title-short: "The architecture of Brunelleschi"
  container-title: "Journal of the Warburg and Courtauld institutes"
  volume: 9
  issued: 1946
  page: 96-121
  language: en
# vim: spelllang=sl,en,it
...
