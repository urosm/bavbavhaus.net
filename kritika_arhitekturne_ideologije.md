---
title: "H kritiki arhitekturne ideologije"
description: |
  Naiven prevod in komentarji k Tafurijevem članku ["Per una critica dell'ideologia archittetonica"]{lang=it}.
  [@tafuri1969peruna, [op. a.]]
...

- Prvi prispevek Manfreda Tafurija v poznooperaistični reviji *Contropiano*, "Per una critica dell'ideologia architettonica" ("H kritiki arhitekturne ideologije").
  [@tafuri1969peruna]
- Uporabljamo izraz poznooperaistično, saj revija nastopi po klasičnooperaistični reviji *Classe operaia*, a ne sledi politično-teoretskemu razvoju, ki ga običajno označujemo s postoperaizmom.
- Prispevek lahko razumemo kot izraziteje poznooperaistično nadaljevanje ali dopolnjevanje *Teorie e storia dell'architettura* (*Teorije in zgodovina arhitekture*).
  [@tafuri1968teorie; angleški prevod @tafuri1980theories]
- Če je predmet *Teorij in zgodovine* "dialektika avantgard" -- razvoj kritičnih, a konstruktivnih vlog, ki jih arhitekturna institucija privzema znotraj buržoaznokulturne perspektive -- od italijanskega quattrocenta do povojnega modernizma, pri "Kritiki arhitekturne ideologije" zgodovinski razvoj arhitekturne insitucije neposredneje napoveduje uresničitev "plana kapitala", tj. realne avtonomije kapitala, in preseganje ideološke funkcije arhitekture. 

ta zgodovinska shema  razvoj arhitekturne ideologije od
napovedovanja do uresničitve "plana kapitala", tj. realne avtonomije kapitala na
področju prostora
posredovanju 

V strožje marksovskih kategorijah, ki so eksplicitna
izhodišča revije, je predmet obravnave *razredna funkcija* arhitekture.


Ključno zgodovinsko prizorišče specifičnega odnosa arhitektura--tehnika--družba je obdobje arhitekturnega modernizma.
Za arhitekturnega zgodovinarja Manfreda Tafurija arhitekti *quattrocenta* nakazujejo na dinamiko, ki se bo z industrijsko revolucijo šele popolnoma razvila, [gl. @tafuri1972larchitettura; @tafuri1980theories] in predstavljajo model zgodovinskim avantgardam in arhitekturnim modernističnim gibanjem 20. stoletja.
[gl. @tafuri1969peruna; kasneje razširjeno v @tafuri1985projekt]
Te se v svojih poskusih kritičnega a produktivnega vstopa v politične in gospodarske programe naprednih institucij industrializiranega kapitalizma vedno nanašajo na nekaj še bolj neposredno vključenega v ekonomske procese.
Tako kot se umetniške avantgarde napotijo v arhitekturo in *design*,

> tako se je planiranje, ki so ga razglašale arhitekturne in urbanistične teorije, nanašalo na nekaj izven sebe: na splošno prestrukturiranje produkcije in konsumpcije; z drugimi besedami na *Plan kapitala*.

Kjer pa bosta arhitektura in urbanizem, nadaljuje, objekta, ne subjekta, Plana. [@tafuri1969peruna, 57]

> Prav tako *design*, kljub svojemu realizmu, prinaša neizpolnjene zahteve, in -- v zagonu, ki ga je podal organizaciji podjetij in produkcije -- vsebuje margine utopije.
> Plan, ki ga opredelijo vodilna arhitekturna gibanja od formulacije *Plan Voisin* Le Corbusierja (1925) in od stabilizacije Bauhausa (okoli 1921) ter dalje, vsebuje to protislovje: izhajajoč iz gradbenega sektorja arhitekturna kultura odkriva, da je izpolnitev zastavljenih ciljev možna le s povezovanjem tega sektorja z reorganizacijo mesta; toda to pomeni, da tako kot so jih izpostavljene zahteve zgodovinskih avantgard napotile v sektor vizualnih komunikacij, ki je še bolj neposredno vključen v ekonomske procese -- arhitektura in *design* --, tako se je planiranje, ki so ga razglašale arhitekturne in urbanistične teorije, nanašalo na nekaj izven sebe: na splošno prestrukturiranje produkcije in konsumpcije; z drugimi besedami na *Plan kapitala*.
[@tafuri1969peruna, 57]

> V tem smislu arhitektura -- z izhajanjem iz same sebe -- posreduje realizem in utopijo.
> Utopija je v trdovratno vztrajnem prikrivanju, da je ideologija planiranja v gradbeništvu lahko uresničena le, če pokaže, da pravi Plan lahko privzame formo onkraj nje; celo, da bosta enkrat, ko vstopi v horizont splošne reorganizacije produkcije, arhitektura in urbanizem objekta, ne subjekta Plana.
[@tafuri1969peruna, 57]

> Arhitekturna kultura med 1920 in 1930 ni bila pripravljena sprejeti teh posledic.
> Kar ji je jasno, je njena lastna "politična" naloga.
> Arhitektura -- beri: načrtovanje in planska reorganizacija gradbeništva ter mesta kot produktivnega organizma -- rajši kot Revolucija: Le Corbusier jasno razglasi ti dve alternativi, ki implicitno krožita v spisih tako Mondriana kot Gropiusa.
[@tafuri1969peruna, 57]

> Medtem pa se z začetkom prav v politično najbolj angažiranih krogih -- v *Novembergruppe*, reviji "G", berlinskem *Ring* -- arhitekturna ideologija tehnično natančno določi: s tem, ko z lucidno objektivnostjo sprejme vse sklepe glede "smrti umetnosti" ter čisto "tehnične" funkcije intelektualca, ki jo apokaliptično razglasijo avantgarde, srednjeevropski *Neue Sachlickheit* prilagodi taisto metodo projektiranja idealizirani strukturi tekočega traku.
> Figure in metode industrijskega dela vstopijo v organizacijo projekta in se odražajo v predlogih konsumpcije objekta.
[@tafuri1969peruna, 57-58]

Primer arhitekturnih poskusov nemške klasične socialdemokracije, ki ga poda, priča o popolni "tehnizaciji" arhitekture in drastičnih omejitvah možnosti delovanja: podobe in metode industrijskega dela vstopijo v organizacijo projekta in sama metoda projektiranja je prilagojena idealizirani (in že zastareli) strukturi tekočega traku.
[@tafuri1969peruna, 58]
V skrajnem primeru urbanista Hilberseimerja, ki odčarano sprejme nove naloge zvezne organizacije racionalizacije od gradbišča do urbane strukture, arhitekturni objekt kot "izjema" v razmerju do mesta popolnoma izgine.
[@tafuri1969peruna, 60]

Tudi alternativa, ki jo predstavlja Le Corbusier, ne predstavlja izhoda.
Vrhunec njegovega "apela industrialcem", ki se začne z razdelavo projekta *Dom-Ino*, je *Plan Obus* za Alžir, ki ga je razvijal od leta 1931 dalje, megastruktura nad staro kazbo, ki je "tako na ideološki kot na formalni ravni še nepresežena".
[@tafuri1969peruna, 66]
V tem projektu je Le Corbusierjeva dilema medvojnega kapitalizma -- arhitektura ali revolucija -- razrešena.
Obratno od nemških arhitektov, ki členijo in racionalizirajo osnovno celico, ki določa nadaljnje strukture -- od taylorizacije gradbišča, gospodinjstva, stanovanja, četrti, mesta -- Le Corbusier oblikuje urbano strukturo kot organsko celoto, znotraj katere je predvidena možnost vstavljanja "ekscentričnih in eklektičnih elementov", tako da občinstvu omogoči "razdelavo lastnega 'slabega okusa'".
[@tafuri1969peruna, 69]

Tej arhitekturni gesti Tafuri pripiše tudi poseben pomen v razmerju do gospodarstva.
Svoboda stanovanjskih celic upošteva zahtevo po nenehni tehnološki revoluciji in hitri konsumpciji, moment potrošnje napravi v produktivni moment celotnega cikla kapitala:

> Stanovanjsko celico, teoretično potrošljivo v kratkem času, je mogoče zamenjati ob vsaki spremembi individualnih potreb -- ob vsaki spremembi potreb, ki jih povzroči prenova stanovanjskih modelov in *standards*, ki jih narekuje produkcija.
[@tafuri1969peruna, 69]

Čeprav gre za produktiven arhitekturni predlog v razmerju do potreb kapitalistične reorganizacije mesta, je -- za razliko od arhitektov klasične socialdemokracije -- predlagan v izolaciji od državnih ali mestnih oblasti, z lastnimi sredstvi.
Vse ekonomske cilje *Plana Obus* je na tej točki že moč najti v keynesijanskih anticikličnih politikah, kar pomeni, da je arhitekt kot predlagatelj projekta odvečna figura.

Na eni strani *projekt* -- raztopitev arhitekturnega objekta v spremenljivke širših ekonomskih procesov -- na drugi *utopija* -- neuslišano podajanje forme tem procesom: v obeh primerih gre za izgubo funkcije arhitektov kot aktivnih ideologov.
Za Tafurija so to gabariti krize, ki jo zaznava arhitekturna kultura povojne Evrope, ki jo tudi neposredno naslavlja.
[za vlogo tafurijanske kritike modernističnega gibanja v razmerju do njemu sodobnih arhitekturnih neoavantgard in humanizmov gl. @corna2016thinking; @galimberti2022images]

***

::: alts

> Oddaljiti tesnobo z razumevanjem in ponotranjenjem vzrokov: to se zdi, da je
> eden poglavitnih etičnih imperativov buržoazne umetnosti. Malo pomembno je, če
> bo konflikte, protislovja in raztrganine, ki tesnobo proizvajajo, vpil splošen
> mehanizem sposoben začasne razrešitve teh neskladij, ali če bo katarza
> dosežena skozi kontemplativno sublimacijo. "Najstvo" buržoaznega intelektualca
> se, kakor koli, prepozna v imperativnem pomenu, ki ga privzema njegovo
> "družbeno" poslanstvo: med avantgardami kapitala in intelektualnimi
> avantgardami obstaja nekakšno tiho soglasje, tako tiho, da že sam poskus
> njegove razsvetlitve privabi zbor ogorčenih protestov. Posredniška funkcija
> kulture je lastne značilnosti opredelila v ideoloških terminih do te mere, da
> je v svoji preudarnosti dosegla -- onkraj vsake individualne dobronamernosti
> -- lastnim produktom naložiti forme upora in protesta: in toliko višje kot je
> sublimacija konfliktov na ravni forme, toliko bolj ostajajo strukture, ki to
> sublimacijo potrjujejo in krepijo, zakrite.

::: {lang=it}

> Allontanare l'angoscia comprendendone e introiettandone le cause: questo
> sembra essere uno dei principali imperativi etici dell'arte borghese. Poco
> importa se i conflitti, le contraddizioni, le lacerazioni che generano
> l'angoscia verranno assorbite in un meccanismo complessivo capace di comporre
> provvisoriamente quei dissidi, o se la catarsi verrà raggiunta attraverso la
> sublimazione contemplativa. Il "dover essere" dell'intellettuale borghese si
> riconosce comunque nel valore imperativo che assume la sua missione "sociale":
> fra le avanguardie del capitale e le avanguardie intellettuali esiste una
> sorta di tacita intesa, tale che al solo tentare di portarla alla luce si
> solleva un coro di indignate proteste. Tanto la funzione mediatrice della
> cultura ha identificato i propri connotati in termini ideologici, che la sua
> astuzia giunge -- al di là di tutte le buone fedi individuali -- ad imporre
> forme di contestazione e protesta ai propri prodotti: e quanto più la
> sublimazione dei conflitti è alta sul piano della forma, tanto più rimangono
> nascoste le strutture che quella sublimazione conferma e convalida.

:::

:::

"Oddaljiti tesnobo z razumevanjem in ponotranjenjem njenih vzrokov: to se zdi
eden poglavitnih etičnih imperativov buržoazne umetnosti." [@tafuri1969peruna,
31] S tem je povzeta dinamika intelektualnih poklicev v kapitalističnem razvoju,
ki je izhodišče tako prispevka kot revije nasploh: intelektualno delo posreduje
konflikte in protislovja, ki jih ta razvoj poraja -- govora je razrednih
konfliktih, tu bi opomnili spet na prispevke Panzierija in Trontija -- v njihovi
družbeno sprejemljivi, mistificirani obliki. V tem smislu obstaja med
intelektualnimi avantgardami in naprednim kapitalom "tiho soglasje", ki ga
ideološke konotacije revolucije in reforme, ki so se zgodovinsko nalagale tem
gibanjem, zakrivajo. In ravno v zgodovinski točki kjer del teh ideoloških
funkcij izginja, je videz neposrednega konflikta med zasebno intelektualno
zavestjo in kapitalom, torej videz, da imajo intelektualni produkti objektivne
vrednote, ločene in nasprotne od kapitala, največji. Takšno mistificirano
pojmovanje družbenega konflikta pa ne more zapopasti vseh konstruktivnih vlog,
ki jih je intelektualno delo znotraj kapitalističnega razvoja privzemalo, in
položaja, v katerem se je potem samo znašlo. Razsvetliti to tiho soglasje med
kapitalom in arhitekturo, meni Tafuri, pomeni poskusiti razsvetliti, zakaj mora
eden zgodovinsko najbolj funkcionalnih predlogov reorganizacije sodobnega
kapitala doživljati "najbolj ponižujoča razočaranja". [@tafuri1969peruna, 31]

::: alts

> Pristopiti k temi arhitekturne ideologije iz tega stališča, pomeni poskusiti
> razsvetliti kako to, da je eden najbolj funkcionalnih predlogov reorganizacije
> sodobnega kapitala moral doživeti najbolj ponižujoča razočaranja, do tolikšne
> mere, da se lahko še danes predstavlja kot objektivna vrednota onkraj vsake
> razredne konotacije, ali pravzaprav kot alternativen moment, kot teren
> neposrednega spopada med intelektualci in kapitalom.

::: {lang=it}

> Affrontare il tema dell'ideologia architettonica da questo punto di vista,
> significa tentare di mettere in luce come mai una delle proposte più
> funzionali alla riorganizzazione del capitale contemporaneo abbia dovuto
> subire le più umilianti frustrazioni, tanto da poter essere presentata
> tutt'oggi come valore oggettivo al di là di ogni connotato di classe, o
> addirittura come momento alternativo, come terreno di scontro diretto fra
> intellettuali e capitale.

:::

:::

Obravnavati ta razvoj arhitekture iz stališča kritike arhitekturne ideologije,
da zaobjema celoten cikel moderne arhitekture, narekuje začetek zgodovinske
obravnave v obdobju, "ki je priča tesni povezavi med buržoazno ideologijo in
intelektualnimi pričakovanji": razsvetljenstvo. [@tafuri1969peruna, 32] To
obdobje je priča formiranju arhitekturnih ideologij v razmerju do mesta --
formacija arhitekta kot ideologa "družbenega" in opredelitev polja poseganja v
urbano -- in formiranju protislovij, ki še vedno spremljajo potek sodobne
umetnosti -- kompleksen odnos vlog, ki jih arhitektura vzpostavlja do svojega
"občinstva" in do svoje dejavnosti, ter kompleksen odnos vlog med arhitekturnim
objektom in urbano strukturo. 

::: alts

> Takoj moramo povedati, da smatramo, da ne gre za naključje, da se je velik del
> nedavnih kulturnih hipotez znotraj arhitekturnih razprav obrnil k potrtemu
> preverjanju samih izvorov moderne umetnosti. Vzeto kot kazalec in pričanje
> zadrege, ki je skrbno umaknjena sama vase, ima vse bolj splošno zanimanje
> arhitekturne kulture za razsvetljenstvo za nas -- onkraj mistificiranih
> načinov na katere se razlaga -- natančen pomen. Z vrnitvijo k svojim izvorom,
> ki so pravilno prepoznani v obdobju, ki je priča tesni povezavi med buržoazno
> ideologijo in intelektualnimi pričakovanji, se celoten cikel moderne
> arhitekture zaobjame v enotni strukturi.

::: {lang=it}

> Dobbiamo subito dire che non riteniamo casuale che tanta parte delle recenti
> ipotesi culturali interne al dibattito architettonico si rivolgano ad
> un'accorato riesame delle origini stesse dell'arte moderna. Assunto come
> indice e testimonianza di un disagio accuratamente ripiegato su sè stesso,
> l'interesse sempre più generalizzato della cultura architettonica per
> l'Illuminismo ha per noi -- al di là dei modi mistificati in cui esso si
> esplica -- un significato preciso. Tornando alle proprie origini,
> correttamente identificate nel periodo che vede la stretta connessione fra
> ideologie borghesi e anticipazioni intellettuali, si stringe in una struttura
> unitaria l'intero ciclo dell'architettura moderna.

:::

:::

::: alts

> Sprejeti to napotilo dopušča globalno upoštevanje formiranja arhitekturnih
> ideologij in predvsem njihovih implikacij v razmerju do mesta.

::: {lang=it}

> Accettare tale indicazione permette di considerare globalmente il formarsi
> delle ideologie architettoniche, ed in particolare le loro implicazioni in
> merito alla città.

:::

:::

::: alts

> Po drugi strani pa sistematična raziskava razsvetljenske razprave omogoča
> doumeti, na njihovi čisto ideološki ravni, večji del protislovij, ki v
> različnih formah spremljajo potek sodobne umetnosti.

::: {lang=it}

> D'altra parte l'esplorazione sistematica del dibattito illuminista dà modo di
> cogliere, al loro livello ideologico puro, gran parte delle contraddizioni che
> in diverse forme accompagnano il percorso dell'arte contemporanea.

:::

:::

## [Pustolovščine razuma: naturalizem in mesto v dobi razsvetljenstva]

::: alts

> Formacija arhitekta kot ideologa "družbenega", opredelitev ustreznega polja
> poseganja v urbano fenomenologijo, prepričevalna vloga forme pri soočenju z
> občinstvom in samokritična pri soočenju s svojim lastnim raziskovanjem,
> dialektika -- na ravni formalne preiskave -- med vlogo arhitekturnega
> "objekta" in vlogo urbane organizacije: na kateri ravni in s kakšno zavestjo
> postanejo te abstraktne konstante konkretne znotraj razsvetljenskih tokov?

::: {lang=it}

> Formazione dell'architetto come ideologo del "sociale", individuazione del
> campo adeguato di intervento nella fenomenologia urbana, ruolo persuasivo
> della forma nei confronti del pubblico e autocritico nei confronti della
> propria stessa ricerca, dialettica -- al livello dell'indagine formale -- fra
> ruolo dell'"oggetto" architettonico e ruolo dell'organizzazione urbana: queste
> astratte costanti dei moderni mezzi di comunicazione visiva a quale livello e
> con quale coscienza divengono concrete all'interno delle correnti illuministe?

:::

:::

::: alts

> Ko Laugier, leta 1765, razglasi svoje teorije o oblikovanju mesta in uradno
> začne s teoretskim raziskovanjem razsvetljenske arhitekture, njegove besede
> izdajo dvojno namero: na eni strani gre za potrebo po redukciji samega mesta
> na naraven pojav, na drugi za potrebo po preseganju vsake apriorne zamisli
> urbanega reda skozi raztezanje mestnega tkiva v formalne razsežnosti povezane
> z estetiko Pitoresknega.

::: {lang=it}

> Quando il Laugier, nel 1764, enuncia le sue teorie sul disegno della città,
> aprendo ufficialmente la ricerca teorica del l'architettura illuminista, le
> sue parole tradiscono una doppia influenza: da un lato è l'istanza di ridurre
> la città stessa a fenomeno naturale, dall'altro quella di superare ogni idea a
> priori di ordinamento urbano, tramite l'estensione al tessuto cittadino di
> dimensioni formali legate all'estetica del Pittoresco.

:::

:::

::: alts

> V svojih *Observations* piše Laugier: "Quiconque sçait bien dessiner un parc
> traçera sans peine le plan en conformité duquel une Ville doit être bâtie
> relativement à son etendue et à sa situation. Il faut des places, des
> carrefours, des rues. Il faut de la regularité et de la bizzarrerie, des
> rapports et des oppositions, des accidents qui varient le tableau, un grand
> ordre dans les détails, de la confusion, du fracas, du tumulte dans
> l'ensemble." ["Kdorkoli zna dobro narisati park, bo brez težave začrtal
> skladen načrt, po katerem je treba zgraditi mesto, glede na njegovo lego in
> okoliščine. Potrebni so trgi, križišča in ceste. Potrebne so pravilnost in
> nenavadnost, razmerja in nasprotja, naključja, ki spreminjajo prizorišče,
> velik red v podrobnostih, zmeda, hrušč in trušč v celoti."][^1]

::: {lang=it}

> "Quiconque sçait bien dessiner un parc -- scrive il Laugier nelle sue
> *Observations*[1], -- traçera sans peine le plan en conformité duquel une
> Ville doit être bâtie relativement à son etendue et à sa situation. Il faut
> des places, des carrefours, des rues. Il faut de la regularité et de la
> bizzarrerie, des rapports et des oppositions, des accidents qui varient le
> tableau, un grand ordre dans les détails, de la confusion, du fracas, du
> tumulte dans l'ensemble".

:::

:::

[^1]: M. A. Laugier, *Observations sur l'architecture*, La Hayer, 1765, str.
    312--313.

<!--

[^1]: M. A. Laugier, *Observations sur l'architecture*, La Hayer, 1765, pp.
    312--313.

-->

::: alts

> Formalna realnost mesta 18\. stoletja je z Laugierjevimi besedami prodorno
> zajeta. Nič več arhetipskih shem reda, temveč sprejetje antiperspektivnega
> značaja urbanega prostora; in tudi nanašanje na vrt dobi svoj nov pomen:
> raznovrstnost narave, ki je poklicana, da vstopi kot del urbane strukture,
> osmisli tolažilni, retorični in didaktični naturalizem, ki je v celotnem loku
> od 17\. do srede 18\. stoletja prevladoval v naključni pripovednosti baročnih
> sistematizacij.

::: {lang=it}

> La realtà formale della città settecentesca è colta, dalle parole del Laugier,
> in modo penetrante. Non più schemi archetipi di ordine, ma accettazione del
> carattere antiprospettico dello spazio urbano; ed anche il riferimento al
> giardino ha un suo significato nuovo: la varietà della natura che viene
> chiamata ad entrare a far parte della struttura urbana, fa ragione del
> naturalismo consolatorio, oratorio e didascalico che per tutto l'arco che va
> dal '600 alla metà del '700 aveva dominato l'episodica narratività delle
> sistemazioni barocche.

:::

:::

::: alts

> V tem smislu poziv k naturalizmu hkrati pomeni ponoven klic k izvirni čistosti
> akta oblikovanja okolja in razumevanje *antiorganskega* značaja par
> excellence, ki pripada mestu. Vendar gre še za nekaj več. Redukcija mesta na
> naraven pojav seveda ustreza estetiki *pitoresknega*, ki jo je angleški
> empirizem uvedel že v zgodnjih desetletjih 18\. stoletja in ki ga bo Cozens,
> leta 1759, izjemno bogato in dosledno teoretiziral.

::: {lang=it}

> A tale stregua l'appello al naturalismo significa contemporaneamente richiamo
> alla purezza originaria dell'atto di configurazione dell'ambiente, e
> comprensione del carattere *antiorganico* per eccellenza che è proprio della
> città. Ma vi è anche di più. La riduzione della città a fenomeno naturale
> risponde certo all'estetica del *Pittoresco* che l'empirismo inglese aveva
> introdotto sin dai primi decenni del XVIII secolo, e che riceverà dal Cozens,
> nel 1759, una teorizzazione estremamente ricca e conseguente.

:::

:::

::: alts

> Ne vemo, koliko je na Laugierjevo idejo mesta vplivala Cozensova teorija
> madeža. Jasno pa je, da imajo urbane iznajdbe francoskega opata in
> krajinarstvo angleškega slikarja skupno metodo, utemeljeno na selekciji kot
> instrumentu kritičnega posega v "naravno" realnost.[^2]

::: {lang=it}

> Non sappiamo quanto sull'idea di città del Laugier possa aver influito la
> teoria della macchia del Cozens. Ciò che è certo è che l'invenzione urbana
> dell'abate francese e il paesaggismo del pittore inglese hanno in comune un
> metodo fondato sulla selezione come strumento per un intervento critico sulla
> realtà "naturale"[2].

:::

:::

[^2]: Alexander Cozens, *A new method of assisting the invention drawing
    original compositions of landscape*, London, 1786\. Pomembno je ovrednotiti
    pomen Popejevih besed, ki jih Cozens citira na začetku svojega traktata:
    "Those Rules which are discovered, not devised / are Nature still, but
    Nature methodized: / Nature, like Monarchy, is but restrained / by the same
    Laws which first herself ordained". (Gl. G. C. Argan, *La pittura
    dell'Illuminismo in Inghilterra da Reynolds a Constable*, Bulzoni, Roma,
    1965, str. 153 in dalje). Civilna vrednota pripisana Naravi -- subjekt in
    objekt etično-pedagoškega dejanja -- se razkriva kot nadomestek
    tradicionalnim principom avtoritete, ki sta jih racionalizem in senzualizem
    rušila.

<!--

[^2]: Alexander Cozens, *A new method of assisting the invention drawing
    original compositions of landscape*, London, 1786\. È importante valutare il
    significato che assumono le parole del Pope, citate dal Cozens nell'iniziare
    il suo trattato: "Those Rules which are discovered, not devised / are Nature
    still, but Nature methodized: / Nature, like Monarchy, is but restrained /
    by the same Laws which first herself ordained". (Cfr. G. C. Argan, *La
    pittura dell'Illuminismo in Inghilterra da Reynolds a Constable*, Bulzoni,
    Roma, 1965, pp. 153 e sgg.). Il valore civile attribuito alla Natura --
    soggeto e oggeto di azione etico-pedagogica -- si rivela come il sostituto
    dei tradizionali principi di autorità che razionalismo e sensismo stavano
    demolendo.

-->

::: alts

> Zdaj, če vzamemo za samoumevno, da mesto za teoretike 18\. stoletja pripada
> istemu formalnemu področju kot slikarstvo, pomeni, da selektivnost in
> kriticizem v urbanizem vpeljujeta fragmentizem, ki na isto raven vrednot
> postavlja ne samo Naravo in Razum, ampak tudi naravni fragment in urbani
> fragment.

::: {lang=it}

> Ora, dando per scontato che la città, per i teorici del '700, insista sulla
> medesima area formale della pittura, selettività e criticismo significano
> l'introduzione, nell'urbanistica, di un frammentismo che ponga sul medesimo
> piano di valori non solo Natura e Ragione, ma anche frammento naturale e
> frammento urbano.

:::

:::

::: alts

> Mesto, kot človekovo delo, *teži* k naravnem stanju, kakor krajina, in mora
> skozi kritično selekcijo, ki jo vrši slikar, pridobiti pečat družbene
> moralnosti.

::: {lang=it}

> La città, in quanto opera dell'uomo, *tende* ad una condizione naturale, come
> il paesaggio, attraverso la selettività critica operata dal pittore, deve
> ricevere il suggello di una moralità sociale.

:::

:::

::: alts

> Pomenljivo je, da, medtem ko Laugier, kakor angleški teoretiki
> razsvetljenstva, akutno dojema umeten značaj urbanega jezika, niti Ledoux niti
> Boullée, še bolj inovativna v svojih delih, ne želita opustiti mitičnega in
> abstraktnega spoštovanja Narave in njene organskosti. V tem smislu je
> Boulléejeva polemika proti akutnim pričakovanjem Perraulta glede umetnega
> značaja arhitekturnega jezika zelo povedna.

::: {lang=it}

> Ed è significativo che mentre il Laugier coglie acutamente, come i teorici
> inglesi dell'Illuminismo, il carattere artificiale del linguaggio urbano, né
> il Ledoux né il Boullée, tanto più innovatori nelle loro opere, vogliono
> abbandonare una considerazione mitica e astratta della Natura e della sua
> organicità. La polemica del Boullée contro le acute anticipazioni del Perrault
> circa il carattere artificiale del linguaggio architettonico è altamente
> indicativa al riguardo.

:::

:::

::: alts

> Morebiti, da je Laugierjevo *mesto kot gozd* zgolj imelo za zgled različne
> prostorske sekvence, ki se pojavijo načrtu Pariza, ki ga je sestavil Patte z
> združevanjem novih kraljevih trgov v enotno podobo. Omejili se bomo torej na
> beleženje teoretskih namenov vsebovanih v Laugierjevem odlomku, ki so toliko
> bolj simptomatični, če se le spomnimo, da se je na njih naslonil Le Corbusier,
> ko je začrtal teoretska načela svojega *Ville radieuse*.[^3]

::: {lang=it}

> Può darsi che la *città come foresta* del Laugier non avesse come modello che
> le variate sequenze di spazi che appaiono nella pianta di Parigi redatta dal
> Patte raccogliendo in un unico quadro di insieme, i progetti delle nuove
> piazze reali. Ci limiteremo quindi a registrare le intuizioni teoriche
> contenute nel passo del Laugier, tanto più sintomatiche qualora si rammenti
> che sarà Le Corbusier ad appoggiarsi ad esse nel delineare i principi teorici
> della sua *Ville radieuse*[3].

:::

:::

[^3]: Gl. Le Corbusier, *Urbanisme*, Esprit Nouveau, Crès, Paris, 1925. It.
    prevod *Urbanistica*, il Saggiatore, Milano, 1967, str. 81 in dalje.

<!--

[^3]: Cfr. Le Corbusier, *Urbanisme*, coll. Esprit Nouveau, Crès, Paris, 1925\.
    Trad. it. *Urbanistica*, il Saggiatore, Milano, 1967, pp. 81 e sgg.

-->

::: alts

> Kaj pomeni, na ideološki ravni, enačiti mesto z *naravnim* objektom? Na eni
> strani to predpostavko preseva sublimacija fiziokratskih teorij: mesto se ne
> bere kot struktura, ki z lastnimi mehanizmi akumulacije določa preoblikovanje
> procesov izkoriščanja zemlje in agrarne ter zemljiške rente. Kot pojav, ki ga
> je možno asimilirati v "naraven" proces, ahistoričen, ker je univerzalen, se
> osvobodi od vsake strukturne obravnave: formalni "naturalizem" v prvotnem
> momentu služi za zagovor *objektivne* nujnosti procesov, ki jih je v gibanje
> spravila predrevolucionarna buržoazija; v drugotnem za utrditev in zaščito
> doseženih pridobitev pred vsakim nadaljnjim preoblikovanjem.

::: {lang=it}

> Cosa significa, sul piano ideologico, assimilare la città ad un oggetto
> *naturale*? Da un lato, in tale assunto traspare una sublimazione delle teorie
> fisiocratiche: la città non è letta come struttura che determina, con i propri
> meccanismi di accumulazione, la trasformazione dei processi di sfruttamento
> del suolo e delle rendite agricole e fondiarie. In quanto fenomeno
> assimilabile ad un processo "naturale", astorico perché universale, essa viene
> svincolata da ogni considerazione di natura strutturale: il "naturalismo"
> formale serve in un primo momento per persuadere circa la necessità
> *oggettiva* dei processi messi in moto dalla borghesia prerivoluzionaria; in
> un secondo momento per consolidare e proteggere da ogni ulteriore
> trasformazione le conquiste acquisite.

:::

:::

::: alts

> Na drugi strani ta naturalizem izpelje svojo funkcijo tako, da umetniški
> dejavnosti zagotovi ideološko vlogo v strogem pomenu. Ni naključje, da je,
> prav v trenutku, ko buržoazna ekonomija s tem, ko "vrednotam" poda vsebine, ki
> so neposredno merljive z merili, ki jih narekujejo nove metode produkcije in
> menjave, začne odkrivati in utemeljevati lastne kategorije delovanja in
> razsojanja, kriza starih sistemov "vrednot" takoj zakrita z zatekanjem k novim
> sublimacijam, ki so s sklicevanjem na univerzalnost Narave umetno napravljene
> v objektivne.

::: {lang=it}

> Dall'altro lato quel naturalismo svolge la propria funzione nell'assicurare
> all'attività artistica un ruolo ideologico in senso stretto. Non è causale che
> proprio nel momento in cui l'economia borghese inizia a scoprire e a fondare
> le proprie categorie di azione e giudizio, dando ai "valori" contenuti
> direttamente misurabili con i metri dettati dai nuovi metodi di produzione e
> di scambio, la crisi degli antichi sistemi di "valori" venga subito coperta da
> un ricorso a nuove sublimazioni, rese artificialmente oggettive attraverso
> l'appello all'universalità della Natura.

:::

:::

::: alts

> Zaradi tega se morata Razum in Narava zdaj združiti. Razsvetljenski
> racionalizem ne uspe prevzeti nase vse odgovornosti za operacije, ki se
> izvajajo, in smatra kot nujno izogniti se neposrednemu soočenju z lastnimi
> postavkami.

::: {lang=it}

> È per questo che Ragione e Natura debbono ora unificarsi. Il razionalismo
> illuminista non riesce ad assumere su di sé l'intera responsabilità delle
> operazioni che sta compiendo e ritiene necessario evitare un confronto diretto
> con le proprie stesse premesse.

:::

:::

::: alts

> Jasno je, da ta ideološka odeja, tekom celega 18\. in prva desetletja 19\.
> stoletja, izkorišča protislovja *ancien régime*. Urbani kapitalizem v
> formiranju in ekonomske strukture, ki temeljijo na predkapitalističnem
> izkoriščanju zemlje, že trčijo med seboj: povedno je, da teoretiki mest ne
> zaznajo teh protislovij, temveč se raje zamotijo z njihovim zakrivanjem, ali,
> bolje, z njihovim razreševanjem, z izničevanjem mesta v velikem morju Narave
> in z osredotočanjem vse svoje pozornosti v nadzidavne vidike samega mesta.

::: {lang=it}

> È chiaro che tale copertura ideologica fa leva, per tutto il '700 e i primi
> decenni dell''800, sulle contraddizioni dell'*ancien régime*. Capitalismo
> urbano in formazione e strutture economiche basate sullo sfruttamento
> precapitalista del suolo cozzano già fra di loro: è indicativo che i teorici
> della città non evidenzino tale contraddizione, ma si preoccupino piuttosto di
> coprirla, o, meglio, di risolverla, annullando la città nel gran mare della
> Natura e concentrando per intero la loro attenzione sugli aspetti
> sovrastrutturali della città stessa.

:::

:::

::: alts

> Urbani naturalizem, uvrstitev *pitoresknega* v mesto in v arhitekturo,
> uvrednotenje krajine v umetniški ideologiji služijo zanikanju zdaj že očitne
> dihotomije med urbano in podeželsko realnostjo, služijo prepričevanju, da ne
> obstaja nikakršen preskok med uvrednotenjem narave in uvrednotenjem mesta kot
> stroja za produkcijo novih form ekonomske akumulacije.

::: {lang=it}

> Naturalismo urbano, inserimento del *Pittoresco* nella città e
> nell'architettura, valorizzazione del paesaggio nell'ideologia artistica,
> servono a negare la dicotomia ormai palese fra realtà urbana e campagna,
> servono a persuadere che non esiste alcun salto fra valorizzazione della
> natura e valorizzazione della città come macchina produttrice di nuove forme
> di accumulazione economica.

:::

:::

::: alts

> Retorični in arkadijski naturalizem kulture sedemnajstega stoletja se zdaj
> nadomesti z drugače prepričljivim naturalizmom.

::: {lang=it}

> Al naturalismo oratorio e arcadico della cultura seicentesca si sostituisce
> ora un naturalismo diversamente persuasivo.

:::

:::

::: alts

> Zato je pomembno podčrtati, da je namerna abstraktnost razsvetljenskih teorij
> mesta v prvem momentu služila za uničenje projektantskih in razvojnih shem
> baročnega mesta, in v drugem momentu za izogibanje, raje kot za pogojevanje,
> formaciji novih in doslednih razvojnih shem.

::: {lang=it}

> È però importante sottolineare che la voluta astrattezza delle teorie
> illuministe sulla città serva in un primo momento a distruggere gli schemi di
> progettazione e di sviluppo della città barocca, e in un secondo momento ad
> evitare, piuttosto che a condizionare, la formazione di nuovi e coerenti
> schemi di sviluppo.

:::

:::

::: alts

> Arhitekturna kultura 18\. in 19\. stoletja, na način, ki je zagotovo anomalija
> v razmerju do splošnih smeri razsvetljenskega kriticizma, razvije pretežno
> destruktivno vlogo. Kar se tudi kaže. Brez zrelih temeljev produkcijskih
> tehnik, ki bi bile primerne za nove pogoje buržoazne ideologije in ekonomskega
> liberalizma, je arhitektura primorana osredotočiti lastno samokritično delo na
> dve smeri:

::: {lang=it}

> In modo sicuramente anomalo rispetto alle linee generali del criticismo
> illuminista, la cultura architettonica svolge quindi nel '700 e nell''800 un
> ruolo prevalentemente distruttivo. E si spiega. Non essendo a disposizione di
> un substrato già maturo di tecniche di produzione adeguate alle nuove
> condizioni dell'ideologia borghese e del liberalismo economico, l'architettura
> è obbligata a concentrare il proprio lavorio autocritico in due direzioni:

:::

:::

::: alts

> a\) na povzdigovanje, s polemičnim namenom, vsega kar lahko nadene
> antievropski pomen. Piranesijev fragmentizem je posledica odkritja te nove
> buržoazne znanosti, ki je kritična zgodovina, in paradoksalno tudi, kritika
> kritike. Vsa moda nanašanja na gotsko, kitajsko, indijsko arhitekturo ter
> romantični naturalizem krajinskih vrtov v katerih so zatopljene neironične
> šale eksotičnih paviljonov in lažnih ruševin, se idealno povezuje z vzdušjem
> Montesquievih *Lettres persanes* in Voltairjevega *Ingénu* ter z Leibnizovim
> jedkim protizahodnjaštvom. Da bi integrirali racionalizem in kriticizem, se
> evropski miti postavljajo nasproti vsemu kar lahko, z izpodbijanjem, potrdi
> njihovo veljavnost. V angleškem krajinskem vrtu se izničuje zgodovinska
> perspektiva. Ne gre toliko za beg v pravljično, ki se mu sledi z zbiranjem
> tempeljčkov, paviljonov, jam v katerih se zdi, da so se tam srečale najbolj
> različna pričevanja človeške zgodovine. Bolj gre za vprašanje, ki ga zastavlja
> Brownov, Kentov, Woodov "pitoreskno", ali Lequeujeva "groza": z instrumenti
> arhitekture, ki se je zdaj že odpovedala formiranju "objektov", da bi postala
> tehnika organizacije vnaprej formiranih materialov, zahtevajo preveritev izven
> arhitekture.

::: {lang=it}

> a\) -- nell'esaltare, a scopo polemico, tutto ciò che può assumere un
> significato antieuropeo. Il frammentismo di Piranesi è conseguenza della
> scoperta di quella nuova scienza borghese che è la critica storica, ma è
> anche, e paradossalmente, critica della critica. Tutta la moda dei riferimenti
> all'architettura gotica, cinese, indù, e il naturalismo romantico dei giardini
> paesistici in cui si immergono gli scherzi senza ironia dei padiglioni esotici
> e delle false rovine, si lega idealmente al clima delle *Lettres persanes* di
> Montesquieu, dell'*Ingénu* di Voltaire, del caustico antioccidentalismo di
> Leibniz. Per rendere integrale razionalismo e criticismo, i miti europei
> vengono messi a confronto con tutto ciò che può, contestandoli, conformarne la
> validità. Nel giardino paesistico all'inglese si consuma l'annullamento della
> prospettiva storica. Non è tanto l'evasione nel fiabesco che viene perseguita
> con l'aggregazione di tempietti, padiglioni, grotte, in cui sembra si siano
> date convegno le più disparate testimonianze della storia dell'umanità. È
> piuttosto una domanda che il "Pittoresco" del Brown, del Kent, dei Wood, o
> l'"orrido" del Lequeu pongono: con gli strumenti di un'architettura che ha già
> rinunciato a formare "oggetti", per divenire tecnica di organizzazione di
> materiali preformati, essi chiedono una verifica fatta dal l'esterno
> dell'architettura.

:::

:::

::: alts

> Z vso distanco, tipično za velike razsvetljenske kritike, te arhitekti začnejo
> sistematično in usodno avtopsijo arhitekture in vseh njenih konvencij.

::: {lang=it}

> Con tutto il distacco tipico dei grandi critici dell'Illuminismo, quegli
> architetti iniziano una sistematica e fatale autopsia dell'architettura e di
> tutte le sue convenzioni.

:::

:::

::: alts

> b\) Drugič, čeprav s postavljanjem lastnih formalnih vlog znotraj področja
> mesta v oklepaj, arhitektura predstavi alternativo nihilistični perspektivi,
> ki je jasno opazna za haluciniranimi fantazijami Lequeua, Bélangera,
> Piranesija.

::: {lang=it}

> b\) -- In secondo luogo, pur mettendo fra parentesi il proprio ruolo formale
> nell'ambito della città, l'architettura presenta un'alternativa alla
> prospettiva nichilista chiaramente avvertibile dietro le allucinate fantasie
> di un Lequeu, di un Bélanger, di un Piranesi.

:::

:::

::: alts

> Z zavračanjem simbolične vloge, vsaj v tradicionalnem smislu, arhitektura --
> da bi se izognila uničenju same sebe -- odkrije lastno znanstveno poslanstvo.
> Na eni strani lahko postane instrument družbenega ravnovesja; in potem se mora
> čelno spopasti -- kot sta se Durant in Dubut -- z vprašanjem tipov. Na drugi
> lahko postane znanost senzacij: in v to smer silijo Ledoux in, bolj
> sistematično, Le Camus de Meézières. Tipologija in *architecture parlante*,
> torej: spet teme, ki jih je Piranesi soočil med seboj in ki, rajši kot da
> vodijo k rešitvam, celo 19\. stoletje poudarjajo notranjo krizo arhitekturne
> kulture.

::: {lang=it}

> Rinunciando ad un ruolo simbolico, almeno in senso tradizionale,
> l'architettura -- per evitare di distruggere se stessa -- scopre la propria
> vocazione scientifica. Da un lato essa può divenire strumento di equilibrio
> sociale; ed allora dovrà affrontare frontalmente -- cosa che faranno il Durand
> e il Dubut -- la questione dei tipi. Dall'altro può divenire scienza delle
> sensazioni: ed in tale direzione spingeranno il Ledoux e, con più
> sistematicità, il Le Camus de Meézières. Tipologia e *architecture parlante*,
> dunque: sono ancora i temi fatti scontrare fra loro dal Piranesi e che,
> piuttosto che condurre verso soluzioni, accentueranno, per tutto l''800, la
> crisi interna alla cultura architettonica.

:::

:::

::: alts

> Zdaj arhitektura sprejme napraviti svoje delo "politično". Kot politični
> agentje so arhitekti morali prevzeti nase nalogo nenehne iznajdbe naprednih
> rešitev na najbolj splošnih ravneh. V ta namen postane vloga ideologije
> odločilna.

::: {lang=it}

> Ora l'architettura accetta di rendere "politico" il proprio operato. In quanto
> agenti politici, gli architetti debbono assumersi il compito dell'invenzione
> continua di soluzioni avanzate ai livelli più generalizzabili. Il ruolo
> dell'ideologia, a tale scopo, diviene determinante.

:::

:::

::: alts

> Utopizem, ki ga je moderno zgodovinopisje želelo prepoznati v delih
> arhitekturnega razsvetljenstva, je treba torej natančno definirati v svojih
> pristnih pomenih. V realnosti evropski arhitekturni predlogi 18\. stoletja
> niso zajemali ničesar neuresničljivega, niti ni naključje, da vso obsežno
> teoretiziranje *philosophes* arhitekture ne vsebuje nobene socialne utopije,
> ki bi podpirala urbani reformizem, ki je predpostavljen na čisto formalni
> ravni.

::: {lang=it}

> L'utopismo che la storiografia moderna ha voluto riconoscere nelle opere
> dell'Illuminismo architettonico, va dunque esattamente definito nei suoi
> autentici significati. In realtà le proposte architettoniche del '700 europeo
> non hanno nulla di irrealizzabile, né è causale che tutta la vasta
> teorizzazione dei *philosophes* dell'architettura non contenga alcuna utopia
> sociale a sostegno del riformismo urbano preconizzato al puro livello formale.

:::

:::

::: alts

> Sam uvod v geslo *Architecture*, ki ga je Quatrèmere de Quincy napisal za
> drugo izdajo velike *Encyclopédie*, je mojstrovina realizma, pa čeprav v
> abstraktnih terminih v katerih se izraža.

::: {lang=it}

> La stessa introduzione alla voce *Architecture* scritta dal Quatremère de
> Quincy per la seconda edizione della grande *Encyclopédie*, è un capolavoro di
> realismo, pur nei termini astratti in cui si esprime.

:::

:::

::: alts

> Quatremère piše: "Entre tous le arts ces enfants du plaisir et de la
> nécessité, que l'homme c'est associés, pour l'aider à supporter les peines de
> la vie et à transmettre sa mémoire aux générations futures, ou ne saurait nier
> que l'*architecture* ne doit tenir un rang des plus distingués. A ne
> l'envisager que sous le point de vue de l'utilité, elle l'emporte sur tous les
> arts. Elle entretient la salubrité dans les villes, elle veille à la santé des
> hommes, elle asstire leur propriétés, elle ne travaille que pour la streté, le
> repos, et le bon ordre de la vie civile." ["Ne bi mogli zanikati, da je izmed
> vseh umetnosti, teh otrok ugodja in nujnosti, ki si jih je usvojil človek, da
> mu pomagajo prestajati življenjske težave in prenašati svoj spomin na
> prihodnje rodove, *arhitektura* obdržala najbolj izbran položaj. Tudi če je ne
> motrimo zgolj s stališča koristnosti, zmaga nad vsemi umetnostmi. Vzdržuje
> zdrave razmere v mestih, bdi nad zdravjem ljudi, jim zagotavlja lastnino in se
> trudi predvsem za varnost in za dobro ureditev državljanskega življenja."][^4]

::: {lang=it}

> "Entre tous le arts -- scrive il Quatremère[4] -- ces enfants du plaisir et de
> la nécessité, que l'homme c'est associés, pour l'aider à supporter les peines
> de la vie et à transmettre sa mémoire aux générations futures, ou ne saurait
> nier que l'*architecture* ne doit tenir un rang des plus distingués. A ne
> l'envisager que sous le point de vue de l'utilité, elle l'emporte sur tous les
> arts. Elle entretient la salubrité dans les villes, elle veille à la santé des
> hommes, elle asstire leur propriétés, elle ne travaille que pour la streté, le
> repos, et le bon ordre de la vie civi

:::

:::

[^4]: M. Quatremère de Quincy, geslo *Architecture*, v: *Encyclopédie
    méthodique* itd. zv. I, str. 109.

<!--

[^4]: M. Quatremère de Quincy, voce *Architecture*, in: *Encyclopédie
    méthodique*, ecc. T. I, p. 109.

-->

::: alts

> Razsvetljenskemu realizmu se ne odpovejo niti arhitekture sanje velikanskih
> razsežnosti Bouléeja ali štipendistov Akademije. Dimenzijsko poveličevanje,
> geometrijska izčiščenost, bahav primitivizem, ki vzpostavljajo konstante teh
> projektov, privzamejo konkreten pomen, če jih beremo v luči v kateri želijo
> biti brani: ne toliko neuresničljive sanje, kot pa eksperimentalni modeli za
> novo metodo projektiranja.

::: {lang=it}

> Il realismo illuminista non è smentito neppure dai sogni architettonici a
> scala gigantesca di un Boullée o dei pensionati dell'Académie. L'esaltazione
> dimensionale, la depurazione geometrica, l'ostentato primitivismo, che
> costituiscono le costanti di quei progetti, assumono un significato concreto
> se letti alla luce di ciò che essi vogliono essere: non tanto sogni
> irrealizzabili, quanto modelli sperimentali di un nuovo metodo di
> progettazione.

:::

:::

::: alts

> Od Ledouxovega ali Lequeujevega razuzdanega simbolizma do geometrijskega molka
> Durandovih tipologij: proces, ki mu sledi arhitektura razsvetljenstva je v
> skladu z novo ideološko vlogo, ki jo je privzela. Arhitektura se mora, da bi
> postala del struktur buržoaznega mesta, redimenzionirati ter se raztopiti v
> uniformnost, ki jo zagotavljajo vnaprej vzpostavljene tipologije.

::: {lang=it}

> Dal simbolismo sfrenato di Leoux o di Lequeu al silenzio geometrico della
> tipologia del Durand: il processo seguito dall'architettura dell'Illuminismo è
> conseguente al nuovo ruolo ideologico da essa assunto. L'architettura deve
> ridimensionare se stessa nell'entrare a far parte delle strutture della città
> borghese, dissolvendosi nell'uniformità assicurata da tipologie precostituite.

:::

:::

::: alts

> Toda ta razkroj se ne zgodi brez posledic. Piranesi bo tisti nosilec limit
> teoretskih intuicij Laugierja: njegova dvoumna ponovna oživitev *Iconographia
> Campi Martii* je grafični spomenik odprtju poznobaročne kulture
> revolucionarnim zahtevam, kot je njegov *Parere sull'architettura* njihov
> najbolj napet literarni zagovor.[^5]

::: {lang=it}

> Ma tale dissoluzione non avviene senza conseguenze. Chi porta al limite le
> intuizioni teoriche del Laugier è il Piranesi: la sua ambigua rievocazione
> dell'*Iconographia Campi Martii* è il monumento grafico del dischiudersi della
> cultura tardobarocca alle istanze delle ideologie rivoluzionarie, come il suo
> *Parere sull'architettura* ne è la più tesa testimonianza letteraria[5].

:::

:::

[^5]: Gl. G. B. Piranesi, *Iconographia Campi Martii*, (1761--'62); Id. *Parere
    su l'architettura*, allegato alle *Osservazioni*, ecc., Roma, 1765; Werner
    Körte, *G. B. Piranesi als praktischer Architekt*, in: "Zeitschrift für
    Kunstgeschichte", II, 1933; R. Wittkower, *Piranesi's Parere su
    l'architettura*, in: "Journal of the Warburg Institute", 1938, III, str. 2.

<!--

[^5]: Cfr. G. B. Piranesi, *Iconographia Campi Martii*, (1761--'62); Id. *Parere
    su l'architettura*, allegato alle *Osservazioni*, ecc., Roma, 1765; Werner
    Körte, *G. B. Piranesi als praktischer Architekt*, in: "Zeitschrift für
    Kunstgeschichte", II, 1933; R. Wittkower, *Piranesi's Parere su
    l'architettura*, in: "Journal of the Warburg Institute", 1938, III, p. 2.

-->

::: alts

> V piranesijskem Campomazio ni več nobene zvestobe poznobaročnim načelom
> *raznolikosti*. Ker rimska antika ni samo referenčna vrednota obremenjena z
> ideološkimi nostalgijami in revolucionarnimi pričakovanji, temveč tudi mit za
> izpodbiti, je vsaka forma klasicističnih izpeljank obravnavana kot zgolj
> fragment, kot deformiran simbol, kot halucinirana raztrganina trpečega "reda".

::: {lang=it}

> Nel Campomarzio piranesiano non v'è più alcuna fedeltà al principio
> tardobarocco di *varietà*. Poiché l'antichità romana non è solo un valore di
> riferimento carico di nostalgie ideologiche e di attese rivoluzionarie, ma
> anche un mito da contestare, ogni forma di classicistica derivazione è
> trattata come mero frammento, come deformato simbolo, come allucinato lacerato
> di un "ordine" in macerazione.

:::

:::

::: alts

> Red v detajlu torej ne privede do enostavnega *tumulte dans l'ensemble*, ampak
> do pošastnega vznikanja simbolov, ki so izropani pomenov. Piranesijski *gozd*,
> kakor sadistične atmosfere njegovih *Carceri*, dokaže, da ni samo "spanec
> razuma", ki poraja pošasti, temveč da tudi "bedenje razuma" lahko vodi do
> deformacije: četudi je cilj v katerega meri Sublimno.

::: {lang=it}

> L'ordine nel dettaglio non porta quindi al semplice *tumulte dans l'ensemble*,
> bensì ad un mostruoso pullulare di simboli privati di significato. La
> *foresta* piranesiana, come le sadiche atmosfere delle sue *Carceri*, dimostra
> che non è solo il "sonno della ragione" a provocare mostri, ma che anche "la
> veglia della ragione" può condurre al deforme: anche se l'obiettivo su cui
> essa punta è il Sublime.

:::

:::

::: alts

> Kriticizmu piranesijskega *Camporazio* se lahko pripiše preroška vrednota. Zdi
> se, da, najbolj napredna točka figurativnega razsvetljenstva v njem, s srčnim
> zanosom opozarja o neposredni nevarnosti dokončne izgube organskosti forme:
> ideal Totalitete in univerzalnosti je zdaj vstopil v krizo.

::: {lang=it}

> Si può attribuire un valore profetico al criticismo del *Campomarzio*
> piranesiano. In esso la punta più avanzata dell'Illuminismo figurativo sembra
> avvertire con accorata enfasi del pericolo immanente alla perdita definitiva
> dell'organicità della forma: è l'ideale della Totalità e dell'universalità che
> è ormai entrato in crisi.

:::

:::

::: alts

> Arhitektura se lahko tudi prisili vzdrževati popolnost, ki jo ohranja pred
> popolnim razkrojem. Toda ta napor je izničila združitev arhitekturnih *kosov*
> v mesto. V njem so ti fragmenti neusmiljeno vpiti in izropani vsake
> avtonomije; ne glede na njihovo vztrajno željo privzeti členjene in
> sestavljene oblike. V ikonografiji Campi Martii smo priča epskemu prikazu
> arhitekture med bitko, ki jo bije proti sami sebi: tipologija je potrjena kot
> zahtevek višjega reda, toda oblikovanje posameznih tipov teži k uničenju
> samega koncepta tipologije; zgodovina je vpoklicana kot imanentna "vrednota",
> toda paradoksalno zavračanje arheološke realnosti postavlja v dvom njen
> civilni potencial; formalno izumljanje se zdi, da izraža lasten primat, toda
> obsedeno ponavljanje izumov reducira celoten urbani organizem na neke vrste
> velikanski "nekoristni stroj".

::: {lang=it}

> L'architettura può anche sforzarsi di mantenere una compiutezza che la
> preservi dalla totale dissoluzione. Ma quello sforzo è vanificato
> dall'assemblaggio dei *pezzi* architettonici nella città. È in essa che quei
> frammenti vengono spietatamente assorbiti e privati di ogni autonomia; né a
> qualcosa vale il loro ostinarsi a voler assumere articolate e composite
> configurazioni. Nell'Iconographia Campi Martii si assiste ad una
> rappresentazione epica della battaglia ingaggiata dall'architettura contro se
> stessa: la tipologia viene affermata come istanza di ordinamento superiore, ma
> la configurazione dei singoli tipi tende a distruggere il concetto medesimo di
> tipologia; la storia viene invocata come immanente "valore", ma il paradossale
> rifiuto della realtà archeologica ne pone in dubbio il potenziale civile;
> l'invenzione formale sembra enunciare il proprio primato, ma l'ossessivo
> reiterarsi dalle invenzioni riduce l'intero organismo urbano ad una sorta di
> gigantesca "macchina inutile".

:::

:::

::: alts

> Zdi se, da racionalizem odkrije lastno iracionalnost. Arhitekturno
> "razmišljanje", v želji po vpitju vseh svojih protislovij, za lasten temelj
> postavi tehniko *šoka*. Posamezni arhitekturni fragmenti trčijo med seboj,
> brezbrižni celo do spopada, in se kopičijo ter tako dokazujejo neuporabnost
> inventivnega napora, ki je bil vzpostavljen pri določanje njihove forme.

::: {lang=it}

> Il razionalismo sembra scoprire la propria irrazionalità. Nel voler assorbire
> tutte le proprie contraddizioni, il "ragionamento" architettonico pone a
> proprio fondamento la tecnica dello *choc*. I singoli frammenti architettonici
> si urtano fra loro, indifferenti persino allo scontro, e si accumulano
> dimostrando l'inutilità dello sforzo inventivo messo in opera per definirne la
> forma.

:::

:::

::: alts

> Mesto ostaja neznanka -- glede na to da piranesijski Campomarzio nikogar več
> ne vara glede kvalitete svojih eksperimentalnih razdelav skritih za arheološko
> masko -- niti akt projektiranja ni sposoben določiti novih konstant reda. Samo
> en aksiom izvira iz tega ogromnega *bricolage*: iracionalno in racionalno se
> morata prenehati izključevati. Piranesi si ne lasti instrumentov za prevajanje
> dialektike protislovij v formo: omejiti se mora na vzneseno razglašanje, da je
> nov velik problem to ravnovesje nasprotij, ki ima v mestu svoj odrejen kraj,
> drugače tvega uničenje samega pojma arhitekture.

::: {lang=it}

> La città permane un'incognita -- dato che il Campomarzio piranesiano non
> inganna nessuno circa la propria qualità di elaborazione sperimentale nascosta
> dietro la maschera archeologica --, né l'atto di progettazione è capace di
> definire nuove costanti di ordine. V'è solo un assioma che scaturisce da tale
> colossale *bricolage*: irrazionale e razionale debbono smettere di escludersi
> a vicenda. Piranesi non possiede gli strumenti per tradurre in forma la
> dialettica delle contraddizioni: deve limitarsi quindi ad enunciare,
> enfaticamente, che il nuovo, grande problema, è quello dell'equilibrio degli
> opposti che ha nella città il suo luogo deputato, pena la distruzione del
> concetto stesso di architettura.

:::

:::

::: alts

> V bistvu je boj med arhitekturo in mestom tisto, kar v Piranesijevem
> Campomarzio privzame epski ton. V njem, "dialektika razsvetljenstva" doseže
> nepresežen potencial, toda, hkrati, idealno napetost, tako nasilno, da je
> sodobniki ne morejo razbrati kot take. Prianesijski *presežek* -- kakor iz
> drugih vidikov, *presežki* svobodomiselne literature razsvetljenske dobe --
> kot tak postane razodetje resnice: razvoji arhitekturne in urbanistične
> kulture razsvetljenstva jo hitijo zakriti.

::: {lang=it}

> È in sostanza la lotta fra architettura e città che assume una tonalità epica
> nel Camporazio del Piranesi. In esso la "dialettica dell'Illuminismo"
> raggiunge un insuperato potenziale, ma, insieme, una tensione ideale così
> violenta da non poter essere raccolta come tale dai contemporanei. L'*eccesso*
> piranesiano -- come, sotto altri aspetti, gli *eccessi* della letteratura
> libertina dell'età dei lumi -- diviene in quanto tale rivelazione di una
> verità: gli sviluppi della cultura architettonica e urbanistica
> dell'Illuminismo si affretteranno a coprirla.

:::

:::

::: alts

> Kakorkoli, urbani fragmentizem, ki ga na ideološki ravni uvede Laugier, se z
> eklektičnim teoretiziranjem Milizije vrne k življenju.

::: {lang=it}

> Purtuttavia il frammentismo urbano introdotto al livello ideologico dal
> Laugier, torna a farsi vivo nell'eclettica teorizzazione del Milizia.

:::

:::

::: alts

> V svojih *Principi* ta piše: "Mesto je kot gozd, zatorej je razporeditev mesta
> takšna kot ta od parka. Potrebni so trgi, križišča, prostorne in ravne ulice.
> Toda to ni dovolj; potrebno je, da je plan oblikovan z okusom in živahnostjo,
> da se hkrati najde red in bizarnost, Evritmija in raznolikost: tu so ulice
> razporejene zvezdasto, tam v gosjo nogo, na eni strani v ribjo kost, na drugi
> pahljačasto, dalje vzporedno, povsod s trouličnimi in štiriuličnimi križišči,
> na različnih mestih z mnogimi trgi različnih oblik, velikosti in okrasov."[^6]

::: {lang=it}

> "Una città è come una foresta -- scrive questi nei suoi *Principî*[6] -- onde
> la distribuzione di una città è come quella di un parco. Ci vogliono piazze,
> capo croci, strade in quantità spaziose, e dritte. Ma questo non basta;
> bisogna che il piano ne sia disegnato con gusto, e con brio, affinché, vi si
> trovi insieme ordine, e bizzarria, Euritmia, e varietà: qui le strade si
> partano a stella, colà a zampa d'oca, da una parte a spica, dall'altra a
> ventaglio, più lungi parallele, da per tutto Trivj e quadrivj, in diverse
> posizioni con una moltitudine di piazze di figura, grandezza e di decorazione
> tutte differen

:::

:::

[^6]: Fr. Milizia, *Principî di architettura civile*, Bassano, 1813, zv. II,
    str. 26--27.

<!--

[^6]: Fr. Milizia, *Principî di architettura civile*, Bassano, 1813, vol. II,
    pp. 26--27.

-->

::: alts

> Nihče ne more spregledati vpliva prefinjenega senzualizma v naslednjem
> predlogu Milizie: "tisti, ki ne zna spreminjati naših užitkov, nam nikoli ne
> bo dal užitka. Na kratko, naj bo raznolika slika neskončnih naključij; velik
> red v detajlih; zmeda, trušč in hrup v celoti."[^7]

::: {lang=it}

> Non v'è chi non veda l'influsso di un raffinato sensismo nella proposizione
> successiva del Milizia: "chi non sa variare i nostri piaceri, non ci darà mai
> piacere. Vuol essere insomma un quadro variato da infiniti accidenti; un
> grand'ordine nei dettagli; confusione fracasso e tumulto nell'insieme"[7].

:::

:::

[^7]: Prav tam, str. 28.

::: alts

> Milizia nadaljuje: "Plan mesta naj bo razporejen na način, da se veličastnost
> celote deli na neskončno prelepih podrobnosti, vse tako različne, da nikoli ne
> najdeš enakega objekta, in ko prehodiš od ene strani k drugi, najdeš v vsaki
> četrti nekaj novega, nekaj edinstvenega, nekaj presenetljivega. Vladati mora
> red, toda vladati mora nekakšni zmedi ... in iz množice pravilnih delov mora
> izhajati nekakšna ideja o nepravilnosti in kaosu celote, ki tako ustreza
> velikim Mestom."[^8]

::: {lang=it}

> "La pianta della città -- prosegue il Milizia[8] -- va distribuita in maniera
> che la magnificenza del totale sia suddivisa in una infinità di bellezze
> particolari, tutte sì differenti, che non si riscontrino giammai gli stessi
> oggetti, e che percorrendola da un capo all'altro si trovi in ciascun
> quartiere qualche cosa di nuovo, di singolare, di sorprendente. Deve regnarvi
> l'ordine, ma fra una specie di confusione ... e da una moltitudine di parti
> regolari deve risultare nel tutto una certa idea di irregolarità e di caos,
> che tanto conviene alle Città grandi".

:::

:::

[^8]: Prav tam, str. 29.

::: alts

> Red in kaos, pravilnost in nepravilnost, organskost in neorganskost. Tu smo
> daleč stran od poznobaročne *enotnosti v raznolikosti*, ki je v Shaftesburški
> misli privzela mistične poudarke.

::: {lang=it}

> Ordine e caos, regolarità ed irregolarità, organicità e disorganicità. Siamo
> ben lontani, qui, da quel precetto tardobarocco di *unità nella varietà*, che
> nel pensiero dello Shaftesbury aveva assunto intonazioni mistiche.

:::

:::

Tafuri bo sledil razsvetljenskim arhitekturnim tokovom 18\. stoletja, začenši z
Laugierjevo razpravo, ki naj bi imela dvojno namero: na eni strani reducirati
mesto na naraven pojav, na drugi povzdigniti obravnavo mesta na isto formalno
področje kot slikarstvo. Na ideološki ravni enačenje mesta z *naravnim* objektom
pomeni asimilacijo urbanih procesov -- to je predrevolucionarnih buržoaznih
procesov akumulacije, ločenih od izkoriščanja zemlje -- v naraven, ahistoričen in
univerzalen proces, ki je tako "osvobojen vsake strukturne obravnave" in
prikazan kot "objektivno nujen". Estetika pitoresknega pa umetniški dejavnosti
na področju urbanega zagotavlja ideološko vlogo pri poseganju v to mesto, ki,
četudi smatrano za naravno, mora preiti skozi sito umetnikove kritične selekcije
in pridobiti "pečat družbene moralnosti". Arhitekturna urbana ideologija na tej
točki deluje kot "ideološka odeja" nad protislovji *ancien régime*, kjer že
trčita urbani kapitalizem v formiranju in predkapitalistično izkoriščanje
zemlje: urbani naturalizem prepričuje, da ni nobenega preskoka med uvrednotenjem
narave in uvrednotenjem mesta. A to soočenje Narave in Razuma pravzaprav pomeni,
da razsvetljenski racionalizem še ne more prevzeti vse odgovornosti za
strukturne spremembe v mestih. Ker še nima razvitih orodij in metod, da bi
konstruktivno posegel v ta razvoj -- lahko ga samo ideološko zakriva --, ima
predvsem destruktivno, kritično vlogo do dosedanjih (baročnih) disciplinskih
parametrov in konvencij (In v tej točki Tafuri pripisuje Piranesiju, oziroma
njegovim grafikam, "izum" buržoazne znanosti kritike vsebovane v praksi ali
*kritičnega delovanja*). Na eni strani to pomeni odpoved formiranju objektov, da
bi postala tehnika organizacije vnaprej formiranih materialov. Kar disciplini
nalaga dvoumen -- v razmerju do njene avtonomije -- položaj, ko se morajo
arhitekturni instrumenti preverjati izven arhitekture, in postavi oblikovno
vlogo arhitekture znotraj mesta v oklepaj. Zato mora arhitektura svoje
tradicionalne simbolne vloge, da bi se izognila uničenju same sebe, zamenjati za
znanstveno poslanstvo: na eni strani lahko postane instrument družbenega
ravnovesja (vprašanje tipov), na drugi znanost senzacij (*architecture
parlante*). S tem arhitektura sprejme "politično" nalogo in iz te naloge sledi
imperativ nenehnih iznajdb rešitev, ki so uporabne na najsplošnejših ravneh. Na
tej točki Tafuri obrobno polemizira z sodobnim zgodovinopisjem o razsvetljenstvu
(in opozarjamo, če ni očitno, gre za posredno polemiko s sodobnim
zgodovinopisjem o modernizmu), ki temu pripisuje utopičen značaj. Nasprotno
trdi, da razsvetljenstvo pravzaprav ne vsebuje nobenih utopij, ampak da gre za
popoln realizem, ki, tudi ko zasleduje geometrijsko izčiščenost ali ko
dimenzijsko poveličuje, preverja uresničljivost modelov za nove metode
projektiranja, ki bi bili primerni za vstop arhitekture v strukture buržoaznega
mesta, to pa bo pomenilo razkroj arhitekture -- v razmerju do mesta -- v
"uniformnost, ki jo zagotavljajo vnaprej vzpostavljene tipologije".
[@tafuri1969peruna, 38] Posledice tega razkroja, nadaljuje, do skrajnosti
predvidi Piranesi v svojem *Campomarzio*, ki naj bi bil "grafični spomenik"
poznobaročne kulture v soočenju z revolucionarnimi zahtevami razsvetljenske: z
združitvijo arhitekturnih kosov, ki so zdaj obravnavani kot zgolj fragment enega
reda, v mesto so ti izropani vsake avtonomije, četudi na ravni posameznega
arhitektura sledi popolnosti; oblikovanje posameznih tipov je v nasprotju s
samim pojmom tipologije; vpoklicana vrednota zgodovine zavrača arheološko
realnost; in imperativ oblikovne invencije, ki se zdi, da izraža lasten primat,
terja obsedeno ponavljanje izumov in reducira celoten urbani organizem na neke
vrste velikanski "nekoristni stroj". [@tafuri1969peruna, 39] S tem se zdi, da
racionalizem odkrije lastno iracionalnost, in novo arhitekturno načelo soočeno z
mestom, ki ostaja neznanka, terja, da se morata iracionalno in racionalno
prenehati izključevati:

::: alts

> Nadzor neorganske realnosti, da bi ukrepali zoper to neorganskost, ne zaradi
> predrugačenja strukture, temveč, da bi iz nje izluščili kompleksen nabor
> sočasnih pomenov: to je zahteva, ki jo spisi Laugierja, Piranesija, Milizije
> in -- kasneje ter z zmernimi toni -- Quatremère de Quincy uvedejo v
> arhitekturno razpravo.

::: {lang=it}

> Il controllo di una realtà disorganica, da attuare agendo su quella
> disorganicità non per mutarne la struttura, bensì per far emergere da essa una
> complessa rosa di compresenti significati: questa è l'istanza che gli scritti
> del Laugier, del Piranesi, del Milizia, e -- più tardi e con toni moderati --
> del Quatremère de Quincy, introducono nel dibattito architettonico.

:::

:::

::: alts

> Toda proti tem hipotezam takoj vzniknejo zahteve tradicionalnega rigorizma. V
> komentarju Milizijevih Principî Giovanni Antolini ne pozabi izstreliti svojih
> puščic proti intuicijam apulijskega teoretika z zagovarjanjem vitruvijanske
> avtoritete in idealne ponazoritve Galianija; proti poveličevanju empirike in
> *pitoresknega* Wooda ter Palmerja v Bathu, edinburškem *Crescents*, planu za
> Milano iz leta 1803 se dvignejo racionalistični rigorizem murattijanskega
> Barija, novega Peterburga, novega Helsinki, novega Turku.

::: {lang=it}

> Ma subito si ergono contro tali ipotesi le istanze di un rigorismo
> tradizionale. Giovanni Antolini nel commentare i Principî del Milizia non
> manca di lanciare i suoi strali contro le intuizioni del teorico pugliese
> difendendo l'autorità vitruviana e l'esemplificazione ideale fattane dal
> Galiani; contro l'esaltazione dell'empiria e del *pittoresco* dei Wood e di
> Palmer a Bath, dei *Crescents* di Edimburgo, del piano del 1803 per Milano, si
> erge il rigorismo razionalista della Bari murattiana, della nuova Pietroburgo,
> della nuova Helsinki, della nuova Turku.

:::

:::

::: alts

> Na koncu naše analize je posebej zanimiva idealna opozicija, ki posreduje med
> Antolinijem in člani komisije napoleonskega plana za Milano.

::: {lang=it}

> Ai fini della nostra analisi ha uno speciale interesse l'opposizione ideale
> che intercorre fra l'Antolini e i membri della commissione del piano
> napoleonico per Milano.

:::

:::

::: alts

> Slednji sprejmejo vzpostaviti dialektiko s strukturo mesta kot se je ta
> izoblikovala skozi zgodovino. Le da o njej implicitno podajo sodbo. Kot
> produkt sil in dogodkov, ki so določeni s predsodki, miti, fevdalnimi ali
> protireformacijskimi strukturami, je kompleksno zgodovinsko tkivo lombardskega
> središča za njih nekaj za napraviti racionalno, za razjasniti v svojih
> funkcijah in svoji formi, tudi za vrednotiti, tako da iz spopada med predhodno
> obstoječimi antičnimi -- odrejeni kraji mračnjaštva -- in novimi rušitvami ter
> posegi -- odrejeni kraji *clarté* *lumières* -- izide v civilnem življenju
> očitna in dejavna izbira, ki ustreza jasni in nedvoumni hipotezi glede usode
> in fizične strukture mesta.

::: {lang=it}

> Questi ultimi accettano di impostare una dialettica con la struttura della
> città così come questa si era venuta a configurare nella storia. Solo che
> danno implicitamente un giudizio su di essa. In quanto prodotto di forze ed
> eventi determinati dal pregiudizio, dal mito, dalle strutture feudali o
> controriformiste, il complesso tessuto storico del centro lombardo è per loro
> qualcosa da rendere razionale, da chiarire nelle sue funzioni e nella sua
> forma, da valutare anche, acciocché dallo scontro fra le antiche preesistenze
> -- luoghi deputati dell'oscurantismo -- e i nuovi tagli e interventi -- luoghi
> deputati della *clarté* delle *lumières* -- emerga evidente ed operante nella
> vita civile una scelta cui corrisponda una chiara ed inequivocabile ipotesi
> circa il destino e la struttura fisica della città.

:::

:::

::: alts

> Ni slučaj, da je Antolini med nasprotniki napoleonskega plana. Če je
> napoleonska komisija na nek način sprejela razpravo z zgodovinskim mestom in v
> njegovem tkivu razredčila ideologijo, ki oblikuje posege, Antolini to razpravo
> zavrača. Njegov projekt za Forum Bonaparte je istočasno radikalna alternativa
> zgodovini mesta, simbol obremenjen z absolutnimi ideološkimi vrednotami,
> *urbani* kraj, ki si kot vsevključujoča *prisotnost* zastavi spremembo celotne
> urbane strukture z obnovitvijo komunikativne vloge neodložljive vrednosti za
> arhitekturo.[^9]

::: {lang=it}

> Non è casuale che l'Antolini sia all'opposizione nella vicenda del piano
> napoleonico. Se la commissione napoleonica accetta in qualche modo il
> colloquio con la città storica e diluisce nel suo tessuto l'ideologia che ne
> informa gli interventi, l'Antolini si rifiuta a tale colloquio. Il suo
> progetto per il Foro Bonaparte è, allo stesso tempo, un'alternativa radicale
> alla storia della città, un simbolo carico di valori ideologici assoluti, un
> luogo *urbano* che, come *presenza* totalizzante, si propone il fine di mutare
> l'intera struttura urbana recuperando, per l'architettura, un ruolo
> comunicativo di valore perentorio[9].

:::

:::

[^9]: Gl. G. Antolini, *Descrizione del Foro Bonaparte, presentato coi disegni
    al comitato di governo della Repubblica Cisalpina ...* Milano, 1802; A.
    Rossi, *Il concetto di tradizione nell'architettura neoclassica milanese*,
    v: "Società", 1956, XII, št. 2, str. 474--493; G. Mezzanotte,
    *L'architettura neoclassica in Lombardia*, Esi, Napoli, 1966.

<!--

[^9]: Cfr. G. Antolini, *Descrizione del Foro Bonaparte, presentato coi disegni
    al comitato di governo della Repubblica Cisalpina ...* Milano, 1802; A.
    Rossi, *Il concetto di tradizione nell'architettura neoclassica milanese*,
    in: "Società", 1956, XII, n. 2, pp. 474--493; G. Mezzanotte, *L'architettura
    neoclassica in Lombardia*, Esi, Napoli, 1966.

-->

::: alts

> Antiteza ni slučajna: vključuje celoten razmislek okoli komunikativnih vlog
> mesta. Za Komisijo l. 1803 je protagonistka novih idealnih in funkcionalnih
> sporočil urbana struktura kot taka.

::: {lang=it}

> L'antitesi non è contingente: essa coinvolge l'intera considerazione circa i
> ruoli comunicativi della città. Per la Commissione del 1803 la protagonista
> dei nuovi messaggi ideali e funzionali è la struttura urbana in quanto tale.

:::

:::

::: alts

> Za Antolinija, nasprotno, se prestrukturiranje mesta vrši z vpeljavo urbanega
> kraja zloma, ki je sposoben sevanja vpeljanih vplivov, ki zavračajo vsako
> skrunitev, v mrežo svojih protislovnih vrednot: mesto kot univerzum diskurza
> ali kot sistem komunikacij zanj spet privzame absolutno in neodložljivo
> "sporočilo".

::: {lang=it}

> Per l'Antolini, al contrario, la ristrutturazione della città va compiuta
> introducendo nella trama dei suoi contraddittori valori un luogo urbano
> dirompente, capace di irradiare effetti indotti che rifiutano ogni
> contaminazione: la città come universo di discorso o come sistema di
> comunicazioni si riassume per lui in un "messaggio" assoluto e perentorio.

:::

:::

::: alts

> Tu sta že zarisani *dve poti* moderne umetnosti in arhitekture.

::: {lang=it}

> Ecco già delineate le *due vie* dell'arte e dell'architettura moderne.

:::

:::

::: alts

> To je dialektika, ki je pravzaprav imanentna celotnemu poteku moderne
> umetnosti in se zdi, da med seboj zoperstavlja tiste, ki se, da bi spoznali
> ter privzeli njene vrednote in bedo, skušajo dokopati do drobovja realnosti,
> in tiste, ki želijo seči onkraj realnosti, ki želijo na novo zgraditi novo
> realnost, nove vrednote, nove javne simbole.

::: {lang=it}

> È la dialettica immanente all'intero decorso dell'arte moderna, infatti, che
> sembra opporre fra di loro chi tenta di scavare fin nelle viscere stesse del
> reale per conoscerne ed assumerne valori e miserie, e chi vuole spingersi al
> di là del reale, chi vuole costruire ex novo nuove realtà, nuovi valori, nuovi
> simboli pubblici.

:::

:::

::: alts

> Kar ločuje napoleonsko komisijo od Antolinija je prav ta opozicija, ki ločuje
> Moneta od Cezanna, Muncha od Braqueja, Schittersa od Mondriana, Häringa od
> Gropiusa, Rauschenberga od Vasarelyja.

::: {lang=it}

> Ciò che divide la Commissione napoleonica dall'Antolini, è la stessa
> opposizione che separerà Monet da Cezanne, Munch da Braque, Schwitters da
> Mondrian, Häring da Gropius, Rauschenberg da Vasarely.

:::

:::

::: alts

> Med Laugierjevim "gozdom" in aristokratsko opreznostjo Antolinija je situirana
> tretja pot, usojena, da postane protagonistka novega načina poseganja v in
> nadzorovanja urbane morfologije. L'Enfantov načrt za Washington ali William
> Pennov za Philadelphio uporabljajo, v razmerju do evropskih modelov, nove
> instrumente.

::: {lang=it}

> Fra la "foresta" del Laugier e l'aristocratico riserbo dell'Antolini, si situa
> una terza via destinata a divenire la protagonista di un nuovo modo di
> intervento e controllo della morfologia urbana. Il piano di l'Enfant per
> Washington o di William Penn per Philadelphia agiscono con strumenti nuovi
> rispetto ai modelli europei.

:::

:::

::: alts

> Razmerje, ki veže te pragmatistične sheme razvoja s strukturo vrednot, ki so
> tipične za družbo ZDA, je bilo že dostikrat analizirano, da bi to temo morali
> tu ponovno vzeti v pregled.[^10]

::: {lang=it}

> È stato già e più volte analizzato il rapporto che lega tali pragmatistici
> schemi di sviluppo alla struttura dei valori tipica della società statunitense
> sin dal suo primo affermarsi, per dover qui riprendere l'esame di questo
> tema[10].

:::

:::

[^10]: Gl. L. Benevolo, *Storia dell'architettura moderna*, Laterza, Bari, 1960;
    M. Manieri-Elia, *L'architettura del dopoguerra in U.S.A.*, Cappelli,
    Bologna, 1966\. Manieri o predlogu plana za mesto New York iz leta 1807
    piše: "Na urbanistični ravni se zdi, da puritanska in 'antiarhitekturna'
    drža dobro sovpadata s smislom za jeffersonski svobodnjaški individualizem
    za katerega sistem, kot je zelo razvidno v *Deklaraciji neodvisnosti ...*,
    sestoji iz kar se da malo ovirajoče, funkcionalne podpore: če naj bo oblast
    nič drugega kot prožen in v vsakem trenutku spremenljiv instrument v službi
    neodtujljivih človekovih pravic, je še toliko več razlogov, da mora
    regulacijski načrt dajati maksimalno zagotovilo prožnosti in se mora skrajno
    minimalno upirati produktivnim iniciativam." (Nav. d., str. 64--65). Gl.
    tudi izjemno dokumentacijo o formaciji ameriških mest, v: J. W. Reps, *The
    making of urban America*, Princeton, 1965.

<!--

[^10]: Cfr. L. Benevolo, *Storia dell'architettura moderna*, Laterza, Bari,
    1960; M. Manieri-Elia, *L'architettura del dopoguerra in U.S.A.*, Cappelli,
    Bologna, 1966\. "Alla scala urbanistica -- scrive Manieri a proposito del
    piano per la città di New York del 1807 -- ci sembra che l'atteggiamento
    puritano e 'antiarchitettonico' combaci bene col senso di individualismo
    libertario jeffersoniano, per il quale il sistema, come è evidentissimo
    nella *Dichiarazione d'indipendenza ...*, consiste in un supporto funzionale
    il meno ingombrante possibile: se il governo non deve essere che uno
    strumento elastico e modificabile in ogni momento a servizio degli
    inalienabili diritti umani, a maggior ragione un piano regolatore deve dare
    la massima garanzia di elasticità e deve presentare una minima resistenza
    all'iniziativa produttiva". (Op. cit., pp. 64--65). Cfr. inoltre
    l'eccezionale documentazione sulla formazione delle città americane, in: J.
    W. Reps, *The making of urban America*, Princeton, 1965.

-->

::: alts

> Velika zgodovinska zasluga urbanega oblikovanja, ki ga je ameriški urbanizem
> posvojil od druge polovice 18\. stoletja dalje, je izrazito zavzemanje za
> sile, ki izzivajo morfološke spremembe v mestu, in njihovo obvladovanje s
> pragmatično držo, ki je evropski kulturi popolnoma tuja.

::: {lang=it}

> Mettersi esplicitamente dalla parte delle forze che provocano il mutamento
> morfologico nella città controllandole con un atteggiamento pragmatista del
> tutto estraneo alla cultura europea, è il grande merito storico del disegno
> urbano adottato dall'urbanistica americana sin dalla seconda metà del '700.

:::

:::

::: alts

> Uporaba pravilne mreže prometnih arterij, kot preproste in prožne podpore
> urbani strukturi v kateri naj bo zavarovana nenehna spremenljivost, uresniči
> cilj, ki ga evropska kultura ni uspela doseči. Absolutna svoboda dodeljena
> posameznemu arhitekturnemu fragmentu se tu nahaja natanko v kontekstu, ki s
> strani fragmenta ni formalno pogojen. Ameriško mesto prispe, z ohranjanjem
> rigidnih zakonov, ki mu vladajo kot celoti, do maksimalnega členjenja
> sekundarnih elementov.

::: {lang=it}

> L'uso di una maglia regolare di arterie di scorrimento come semplice e
> flessibile supporto per una struttura urbana di cui si vuole salvaguardare la
> continua mutevolezza, realizza l'obiettivo che la cultura europea non era
> riuscita a raggiungere. L'assoluta libertà concessa al singolo frammento
> architettonico si situa esattamente, qui, in un contesto che non viene
> condizionato formalmente da esso. La città americana giunge ad attribuire il
> massimo di articolazione agli elementi secondari che la configurano,
> mantenendo rigide le leggi che la governano come insieme.

:::

:::

Proti tej zahtevi vzniknejo zahteve tradicionalnega rigorizma, idealno opozicijo
med njimi Tafuri prepozna v opoziciji, tik ob začetku 19\. stoletja, med
Antolinijevim planom za *Foro Bonaparte* in posegi vladne (napoleonske)
arhitekturne komisije. Slednja sprejema "razpravo" z nakopičeno strukturo mesta,
kjer naj "spopad" med novimi in starimi posegi in rušitvami kompleksnemu
zgodovinskemu tkivu poda novo racionalnost in tako preko urejenega kaosa
izkazuje novo civilno vrednost ter nakazuje usodo mesta. Antolini pa to razpravo
zavrača: njegov plan predstavlja radikalno alternativo zgodovini mesta in si
zastavi spremembo urbane strukture v celoti. Obliko teh alternativ Tafuri
prepozna v celotnem poteku moderne umetnosti: na eni strani kritično dejavnost,
ki se skuša dokopati do drobovja realnosti, da bi spoznala ter prevzela njene
vrednote in bedo, na drugi kritična dejavnost, ki želi seči onkraj realnosti,
zgraditi novo realnost z novimi vrednotami in javnimi simboli. Tretja pot, ki
konkretno presega to epizodo in razreši dilemo arhitekturne kulture, se kaže v
pragmatičnih shemah urbanizma novega sveta. Ameriški urbanizem, pravzaprav
neobremenjen s prostorskimi in disciplinskimi zapuščinami *ancien régime*,
omogoča posvajanje novih orodij in enostavnejše zavezništvo med "silami, ki
izzivajo morfološke spremembe v mestu" in arhitekturo. Pravilne prometne mreže
so uporabljene kot podpora urbani strukturi, v kateri je nenehna spremenljivost
predvidena. S tem je uresničen cilj avtonomije in svobode fragmenta znotraj
hkratnih rigidnih zakonov na ravni celote:

::: alts

> Urbanizem in arhitektura sta končno ločeni. Geometrizem plana ne želi najti,
> niti v Washingtonu, niti v Philadelphii in kasneje v New Yorku, arhitekturnega
> soglasja v posameznih formah zgradb. Za razliko od Peterburga ali od Berlina,
> je arhitektura prosta, da raziskuje najrazličnejša in najbolj oddaljena polja
> komunikacije. Urbanemu sistemu je odrejena naloga izražanja stopnje
> nadomestljivosti te figurativne prostosti, ali bolje, naloga zagotavljanja,
> kot njene formalne rigidnosti, stabilne razsežnosti nanašanja. Urbana
> struktura na ta način povzdiguje neverjetno izrazno bogastvo, ki se, še
> posebej od druge polovice 19\. stoletja dalje, nalaga na svobodno mrežo mest
> ZDA: svobodnjaška etika se sreča z pionirskimi miti.

::: {lang=it}

> Urbanistica e architettura sono finalmente scisse. Il geometrismo del piano
> non vuole, a Washington come a Philadelphia e, più tardi, a New York trovare
> una corrispondenza architettonica nelle singole forme degli edifici. A
> differenza di quanto accade a Pietroburgo o a Berlino, l'architettura è libera
> di esplorare i più diversi e distanti campi di comunicazione. Al sistema
> urbano è riservato il compito di esprimere il grado di fungibilità di tale
> libertà figurativa, o, meglio, di assicurare, come la sua rigidità formale,
> una dimensione stabile di riferimento. In tale modo la struttura urbana esalta
> un'incredibile ricchezza espressiva, che, specie dalla seconda metà dell''800
> in poi, si deposita nelle libere maglie delle città statunitensi: l'etica
> liberista si incontra con i miti pionieristici.

:::

:::

## [Forma kot regresivna utopija]

::: alts

> Kar izhaja zdaj iz povzetka analize izkušenj in pričakovanj arhitekturne
> kulture 18\. stoletja, je kriza -- odkrita prav z osveščanjem problema mesta
> kot avtonomnega polja komunikativnih izkušenj -- tradicionalnega koncepta
> *forme*.

::: {lang=it}

> Ciò che per ora risulta dalla sommaria analisi delle esperienze e delle
> anticipazioni della cultura architettonica settecentesca, è la crisi --
> scoperta proprio prendendo coscienza del problema della città come autonomo
> campo di esperienze comunicative -- del tradizionale concetto di *forma*.

:::

:::

::: alts

> Razčlenitev forme in antiorganskost strukture: razsvetljenska arhitektura že
> na začetku dospe do ene kardinalnih točk, na kateri se členi pot sodobne
> umetnosti. Ni vseeno, da je bila intuicija teh novih formalnih vrednot že od
> začetka vezana na probleme novega mesta, ki se je pripravljal postati
> institucionalni kraj moderne buržoazne družbe.

::: {lang=it}

> Disarticolazione della forma e antiorganicità della struttura: già agli inizi
> dell'architettura dell'Illuminismo si giunge a postulare uno dei cardini su
> cui si articolerà il percorso dell'arte contemporanea. Né è indifferente che
> l'intuizione di questi nuovi valori formali fosse legata sin dall'inizio al
> problema della nuova città che si prepara a divenire il luogo istituzionale
> della moderna società borghese.

:::

:::

::: alts

> Toda teoretske zahteve za revizijo formalnih principov ne privedejo toliko k
> resnični in pravi revoluciji pomenov kot pa k akutni krizi vrednot: nove
> razsežnosti, ki jih tekom 19\. stoletja predstavlja problem industrijskih
> mest, ne bodo storile drugega, kot zaostrile te krize v kateri se umetnost
> težko znajde v iskanju ustreznih poti za sledenje razvojem urbane realnosti.

::: {lang=it}

> Ma le istanze teoriche di revisione dei principi formali non conducono tanto
> ad una vera e propria rivoluzione di significati, quanto ad un'acuta crisi di
> valori: le nuove dimensioni presentate dai problemi della città industriale
> nel corso dell''800 non faranno che acuire quella crisi di fronte alla quale
> l'arte si troverà in difficoltà nel trovare le vie adeguate a seguire gli
> sviluppi della realtà urbana.

:::

:::

::: alts

> Na drugi strani se ta zdrobitev organskosti forme osredotoča na arhitekturno
> operacijo, brez ponovnega izhodišča v razsežnost mesta. Ko nas med opazovanjem
> "kosa" arhitekture viktorijanske dobe, prizadene dramatizacija "objekta", ki
> se tam izvaja, se preredko upošteva, da sta lingvistični eklekticizem in
> pluralizem za arhitekte 19\. stoletja predstavljala pravilen odgovor na
> številne razkrojevalne učinke novega vzdušja, ki ga povzroča "*univerzum
> natančnosti*" tehnološke realnosti.

::: {lang=it}

> D'altro lato quella frantumazione dell'organicità della forma si concentra
> nell'operazione architettonica, senza riuscire a trovare uno sbocco nella
> dimensione cittadina. Quando nell'osservare un "pezzo" di architettura di
> epoca vittoriana si è colpiti dall'esasperazione dell'"oggetto", che viene lì
> compiuta, troppo raramente si tiene presente che eclettismo e pluralismo
> linguistico rappresentano, per gli architetti dell''800, la giusta risposta ai
> molteplici stimoli disgregatori indotti dal nuovo ambiente configurato
> dall'"*universo della precisione*" della realtà tecnologica.

:::

:::

::: alts

> Dejstvo, da arhitekti na ta "*univerzum natančnosti*" ne zmorejo odgovoriti z
> ničemer drugim kot z zmedenim "*približkom*" nas ne sme presenetiti. V
> realnosti je urbana struktura ta, in to prav kot register konfliktov, ki so
> prizorišče te zmage tehnološkega napredka, ki nasilno spreminja razsežnosti
> ter privzema oblike odprte strukture v kateri ponovno iskanje točk ravnovesja
> postane utopično.

::: {lang=it}

> Il fatto che gli architetti non sapessero rispondere ancora che con un confuso
> "*press'a poco*" a quell'"*universo della precisione*" non deve stupire. In
> realtà è la struttura urbana, e proprio in quanto registrazione dei conflitti
> che sono il teatro di quella vittoria del progresso tecnologico, che muta
> violentemente di dimensione, configurandosi come aperta struttura in cui
> diviene utopistico ricercare punti di equilibrio.

:::

:::

::: alts

> Toda arhitektura je, vsaj po tradicionalnem pojmovanju, stabilna struktura, ki
> daje formo trajnim vrednotam in utrjuje urbano morfologijo.

::: {lang=it}

> Ma l'architettura, almeno secondo la concezione tradizionale, è una struttura
> stabile, dà forma a valori permanenti, consolida una morfologia urbana.

:::

:::

Iz analize arhitekturne kulture 18\. stoletja, povzema Tafuri, izhaja kriza
tradicionalnega pojma *forme*, ki je odkrita ravno z zavestjo o mestu kot
avtonomnem polju komunikativnih izkušenj. Na eni strani je odziv arhitekturne
kulture 19\. stoletja lingvistični eklekticizem Toda osredotočanje na
arhitekturno operacijo, brez njene preveritve izven zaprtega kroga arhitekturne
razprave, se izkaže za utopizem. Kriza odnosa med arhitekturo in mestom, ki jo
zastavi razsvetljenstvo, se s pojavom industrijskih mest samo še zaostri.
Romantični eklekticizem morda za arhitekte takrat predstavlja pravilen odgovor
na razkrojevalne učinke nove tehnološke realnosti, toda potrdil se bo nov značaj
urbane strukture, ki je prav zmožnost, da privzema oblike odprte strukture, ki
lahko vpije celo predlagano dvoumnost kot vrednoto. Romantični eklekticizem,
pravi, je zgolj tolmač "neusmiljene konkretnosti ublagovljenja človeškega
okolja," ki dokazuje, da noben "subjektiven napor ne more več povrniti za vedno
izgubljene avtentičnosti". [@tafuri1969peruna, 46] Alternativo, čeprav ob
enakih ugotovitvah, predstavljajo predlogi, ki presegajo ta tradicionalni
utopizem bega in predlagajo utopijo, ki je implicirana v realiziranih dejstvih,
utopijo "stvari". Drugačen odziv je torej prekinitev s tradicionalnim
pojmovanjem arhitekture, ki podaja formo trajnim vrednotam:

::: alts

> Tistemu, ki želi razbiti to tradicionalno pojmovanje in arhitekturo povezati z
> usodo mesta, ne preostane drugega kot samo mesto pojmovati kot specifičen kraj
> tehnološke produkcije in kot tehnološki produkt sam, ter s tem redukcija
> arhitekture na enostaven moment produkcijske verige.

::: {lang=it}

> Per chi vuole spezzare tale concezione tradizionale, e legare l'architettura
> al destino della città, non rimane che concepire la città stessa come il luogo
> specifico della produzione tecnologica e prodotto tecnologico essa stessa,
> riducendo l'architettura a semplice momento di una catena produttiva.

:::

:::

::: alts

> In vendar je na nek način piranesijska prerokba buržoaznega mesta kot
> "absurdnega stroja" uresničena v velemestih, ki se v 19\. stoletju
> organizirajo kot primarne strukture kapitalistične ekonomije. 

::: {lang=it}

> Eppure, in qualche modo, la profezia piranesiana della città borghese come
> "macchina assurda" è realizzata nelle metropoli che si organizzano nell''800
> come strutture primarie dell'economia del capitale.

:::

:::

::: alts

> "Coniranje", ki predseduje razvojem teh velemest, se -- sprva -- ne
> obremenjuje z maskiranjem svojega razrednega značaja. Ideologi radikalnega ali
> humanitarnega izvora lahko prav dobro razsvetlijo iracionalnost industrijskega
> mesta, toda pozabijo (ne po naključju), da je ta iracionalnost takšna le za
> opazovalca, ki se slepi, da je *au-dessus de la mêlée*. Humanitarni utopizem
> in radikalni kritiki imajo nepričakovan učinek: napredno buržoazijo prepričajo
> naj ustrezno zastavi temo soglasja med racionalnostjo in iracionalnostjo.

::: {lang=it}

> Lo "zoning" che presiede gli sviluppi di quelle metropoli non si preoccupa --
> in un primo tempo -- di mascherare il proprio carattere di classe. Gli
> ideologi di estrazione radicale o umanitaria possono ben mettere in luce
> l'irrazionalità della città industriale, ma dimenticano (non a caso) che
> quell'irrazionalità è tale solo per un osservatore che voglia illudere se
> stesso di essere *au dessus de la mêlée*. L'utopismo umanitario e le critiche
> radicali hanno un'effetto imprevisto: convincono la borghesia progressista a
> porsi in proprio il tema dell'accordo fra razionalità e irrazionalità.

:::

:::

::: alts

> Glede na vse izrečeno se ta tema kaže kot bistvena formiranju urbane
> ideologije. Ta je med drugim tudi sorodna figurativni kulturi 19\. stoletja
> glede na to, da je odrešenje od dvoumnosti kot kritične vrednote v pravem
> pomenu sam izvor romantičnega eklekticizma: prav te dvoumnosti, ki jo je
> Piranesi privedel do najvišje ravni.

::: {lang=it}

> Per tutto ciò che si è detto questo tema appare intrinseco al formarsi
> dell'ideologia urbana. Esso è inoltre famigliare, preso in astratto, alla
> cultura figurativa ottocentesca, dato che all'origine medesima dell'Eclettismo
> romantico è il riscatto dell'ambiguità come valore critico in senso proprio:
> esattamente quell'ambiguità portata dal Piranesi al suo massimo livello.

:::

:::

::: alts

> Kar Piranesiju omogoča posredovanje grozljive prerokbe mrka svetega s
> primitivno nostalgijo in z begi v dimenzijo *Sublimnega*, je tudi to, kar
> romantičnemu eklekticizmu omogoča biti tolmač neusmiljene konkretnosti
> ublagovljenja človeškega okolja, s pogrezanjem vanj delcev že povsem
> iztrošenih vrednot predstavljenih kot kakršne so, neme, napačne, zvite vase:
> kot v dokaz, da noben subjektiven napor ne more več povrniti za vedno
> izgubljene avtentičnosti.

::: {lang=it}

> Ciò che al Piranesi permette di mediare la terrificante profezia dell'eclissi
> del sacro con nostalgie primitivistiche e fughe nella dimensione del
> *Sublime*, è anche ciò che permette all'Eclettismo romantico di farsi
> interprete della spietata concretezza della mercificazione dell'ambiente
> umano, immergendo in esso parcelle di valori già del tutto consumati e
> presentati così com'essi sono, afoni, falsi, contorti su se stessi: come per
> dimostrare che nessuno sforzo soggettivo può ormai recuperare un'autenticità
> perduta per sempre.

:::

:::

::: alts

> Dvoumnost 19\. stoletja je povsem v razuzdanem razkazovanju lažne zavesti, ki
> s prikazovanjem lastne neavtentičnosti poskuša doseči poslednje etično
> odrešenje. Če je znak in instrument te dvoumnosti zbirateljstvo, je njeno
> specifično polje mesto: v poskusu odrešenja se bo impresionistično slikarstvo
> moralo, ne naključno, podati v opazovalnico, ki je spuščena v urbano
> strukturo, toda distancirana od svojih pomenov s subtilnimi deformacijami leč,
> ki oponašajo objektivno znanstveno distanco.

::: {lang=it}

> L'ambiguità ottocentesca è tutta nell'esibizione sfrenata della falsa
> coscienza che tenta un ultimo riscatto etico esibendo la propria
> inautenticità. Se il collezionismo è il segno e lo strumento di tale
> ambiguità, la città ne è il campo specifico: la pittura impressionista, nel
> tentarne il riscatto, dovrà porsi non casualmente in un osservatorio calato
> nella struttura urbana, ma distanziato dai suoi significati dalle sottili
> deformazioni di lenti che mimano un oggettivo distacco scientifico.

:::

:::

::: alts

> Če se prvi politični odgovori na to situacijo zakoreninijo v odrešitvi tega
> tradicionalnega utopizma za katerega se zdi, da ga je razsvetljenstvo
> odpravilo, pa specifični odgovori metod vizualnih komunikacij predstavijo nov
> tip utopizma: utopijo, ki je implicirana v realiziranih dejstvih, v
> konkretnosti konstruiranih in preverljivih "stvari".

::: {lang=it}

> Se le prime risposte politiche a tale situazione affondano le loro radici nel
> riscatto di quell'utopismo tradizionale di cui l'Illuminismo sembrava aver
> fatto ragione, le risposte specifiche dei metodi di comunicazione visiva
> introducono un nuovo tipo di utopismo: l'utopia implicita nei fatti
> realizzati, nella concretezza delle "cose" costruite e verificabili.

:::

:::

::: alts

> Zaradi tega bo celotna struja političnega utopizma 19\. stoletja s hipotezami
> "modernega gibanja" imela le posredovane -- in to obsežno -- odnose.

::: {lang=it}

> È per questo che l'intero filone dell'utopismo politico ottocentesco avrà
> relazioni solo mediate -- e abbondantemente -- con le ipotesi del "movimento
> moderno".

:::

:::

::: alts

> Namreč, povezave, ki jih sodobno zgodovinopisje običajno vzpostavi med
> utopijami Fouriera, Owna, Cabeta in teoretičnimi modeli Unwina, Geddesa,
> Howarda ali Steina na eni strani ter temi struje Garnier-Le Corbusier na
> drugi, bomo morali obravnavati kot hipoteze za pozorno preveritev in jih po
> vsej verjetnosti prepoznati kot funkcionalne in notranje samim pojavom, ki jih
> želimo z njimi analizirati.[^11]

::: {lang=it}

> Anzi, dovremo considerare i legami istituiti di solito dalla storiografia
> contemporanea fra le utopie di Fourier, Owen, Cabet, ed i modelli teorici di
> Unwin, Geddes, Howard o Stein da un lato, e quelli del filone Garnier-Le
> Corbusier dall'altro, come ipotesi da verificare attentamente, e con ogni
> probabilità da riconoscere come funzionali e interne agli stessi fenomeni che
> con esse si vogliono analizzare[11].

:::

:::

[^11]: Poglavja o utopičnem socializmu in njegovih predlogih urbane
    reorganizacije ne more biti obravnavano z enakimi kriteriji kot formiranje
    ideologij modernega gibanja. Lahko se zgolj pokaže na alternativno vlogo, ki
    jo je romantični utopizem igral v razmerju do teh ideologij; toda njegove
    razvoje, predvsem v anglosaški praksi planiranja, je treba, v analizi, ki
    presega meje pričujočih zapiskov, primerjati z modeli, ki jih je razdelal
    New Deal.

<!--

[^11]: Il capitolo riguardante il socialismo utopistico e le sue proposte di
    riorganizzazione urbana non può essere trattato con criteri omogenei
    rispetto al formarsi delle ideologie del movimento moderno. Si può solo
    accennare al ruolo alternativo che il romanticismo utopistico ha svolto
    rispetto a quelle ideologie; ma i suoi sviluppi, in particolare nella prassi
    della pianificazione anglosassone, vanno messi a confronto con i modelli
    elaborati dal New Deal, in un'analisi che travalica i limiti delle presenti
    note.

-->

::: alts

> Povsem jasno je, da imajo specifični odgovori, ki jih marksistična znanost
> ponuja problemu okoli katerega je utopična misel zavezana neutrudno krožiti,
> dve neposredni posledici za formiranje novih urbanih ideologij:

::: {lang=it}

> È chiaro pertanto che le risposte specifiche offerte dalla scienza marxista al
> problema intorno al quale il pensiero utopista è obbligato a girare senza
> tregua, hanno due conseguenze immediate sul formarsi delle nuove ideologie
> urbane:

:::

:::

::: alts

> a) -- s povrnitvijo splošnih problemov v strogo strukturalno okolje razjasni
> konkreten šah, v katerega se samoobsodi utopija, ter prav tako razkrije
> skrivno voljo do brodoloma, ki je impliciten v samem aktu utopističnih
> hipotez;

::: {lang=it}

> a) -- riconducendo i problemi generali in un ambito rigorosamente strutturale,
> essa rende evidente lo scacco concreto cui l'utopia si autocondanna rivelando
> altresì la volontà segreta di approdare al naufragio implicita nell'atto di
> nascita stesso dell'ipotesi utopistica;

:::

:::

::: alts

> b) -- z izničenjem romantičnih sanj glede enostavnega sovpadanja subjektivnih
> dejanj s potekom družbene usode buržoazni misli razkrije, da je sam koncept
> *usode* izum, ki je povezan z novimi produkcijskimi razmerji: kot sublimacija
> realnih potekov lahko odločno sprejemanje usode -- temelj buržoazne etike --
> odreši od bede in obubožanja, ki ju je ta ista "usoda" vpeljala v vse ravni
> družbenega življenja in predvsem v svojo tipično formo: mesto.

::: {lang=it}

> b) -- annullando il sogno romantico di un'incidenza tout court dell'azione
> soggettiva sul decorso del destino sociale, rivela al pensiero borghese che il
> concetto stesso di *destino* è creazione legata ai nuovi rapporti di
> produzione: in quanto sublimazione di decorsi reali, la virile accettazione
> del destino -- fondamento dell'etica borghese -- può riscattare la miseria e
> l'impoverimento che quello stesso "destino" ha indotto in tutti i livelli
> della vita associata e, principalmente, nella sua forma-tipo: la città.

:::

:::

::: alts

> Konec utopizma in nastanek realizma nista mehanična momenta znotraj procesa
> formacije ideologije "modernega gibanja". Nasprotno, z začetkom četrtega
> desetletja 19\. stoletja, se realistični utopizem in utopični realizem
> prekrivata in nadomeščata; zaton socialne utopije potrjuje vdajo ideologije
> *politiki stvari*, ki jo uresničijo zakoni profita; arhitekturni, umetniški in
> urbani ideologiji preostane utopija forme kot projekt povrnitve človeške
> Totalitete v idealno Sintezo, kot posestvo Nereda skozi Red.

::: {lang=it}

> La fine dell'utopismo e la nascita del realismo non sono momenti meccanici
> all'interno del processo di formazione dell'ideologia del "movimento moderno".
> Anzi, a cominciare dal quarto decennio del XIX secolo, utopismo realistico e
> realismo utopico si accavallano e si compensano; il declino dell'utopia
> sociale sancisce la resa dell'ideologia alla *politica delle cose* realizzata
> dalle leggi del profitto; all'ideologia architettonica, artistica e urbana,
> rimane l'utopia della forma come progetto di recupero della Totalità umana
> nella Sintesi ideale, come possesso del Disordine attraverso l'Ordine.

:::

:::

Opozarjamo na uvodni stavek prispevka: eklekticizem ali tradicionalni utopizem,
bi lahko rekli, vzroke tesnobe "ponotranji", novi politični utopizem pa jih
"razume". Ena posledica tega razumevanja (povrnitve splošnih problemov na
strukturno raven) je prikaz konkretnega "šaha" v katerega se samoobsodi utopija:
možnost arhitekture, da predlaga usodo, ki bo presegla protislovja mesta, se
zanaša na mehanizme istega mesta. In druga: z uničenjem iluzij o sovpadanju
subjektivnih dejanj s potekom "usode" družbe buržoazni misli razkrije, da je
koncept *usode* produkt novih produkcijskih razmerij in da lahko odločno
sprejemanje usode odreši od bede, ki jo je ta ista usoda vpeljala v vse ravni
družbenega delovanja in predvsem v mesto. Arhitekturno pojmovanje mesta kot
produkta, natančneje humanitarne in utopične kritike mesta, torej arhitekturna
obravnava mesta, prepriča napredno buržoazijo naj "ustrezno zastavi temo
soglasja med racionalnostjo in iracionalnostjo". [@tafuri1969peruna, 45]

In tu Tafuri prepozna pogoje, ki sledijo razvoju arhitekture kot ideološkem
instrumentu kapitala:

::: alts

> Arhitektura, v kolikor je neposredno povezana s produktivno realnostjo, torej
> ni zgolj prva, ki s strogo jasnostjo sprejme posledice svojega že izvršenega
> ublagovljenja: izhajajoč iz lastnih specifičnih problemov je moderna
> arhitektura v svoji celoti, še preden mehanizmi in teorije politične ekonomije
> priskrbijo instrumente za izvedbo, sposobna razviti ideološko vzdušje, ki je
> uperjeno k celostni integraciji *designa*, na vseh ravneh delovanja, v
> Projekt, ki je objektivno usmerjen v reorganizacijo produkcije, distribucije
> in konsumpcije v povezavi s kapitalističnim mestom.

::: {lang=it}

> L'architettura, in quanto direttamente legata alla realtà produttiva, non è
> quindi solo la prima ad accettare, con rigorosa lucidità, le conseguenze della
> propria già avvenuta mercificazione: partendo dai propri problemi specifici
> l'architettura moderna, nel suo complesso, è in grado di elaborare, prima
> ancora che i meccanismi e le teorie dell'Economia Politica ne forniscano gli
> strumenti di attuazione, un clima ideologico teso ad integrare compiutamente
> il *design*, a tutti i livelli di intervento, all'interno di un Progetto
> oggettivamente rivolto alla riorganizzazione della produzione, della
> distribuzione e del consumo relativi alla città del capitale.

:::

:::

::: alts

> Analizirati potek modernega gibanja kot ideološkega instrumenta kapitala (od
> okoli 1901, datum projekta *industrijskega mesta* Tonyja Garnierja, do okoli
> 1939, datum ko je njegova kriza preverljiva v vseh sektorjih in na vseh
> ravneh) pomeni slediti zgodovini, ki se členi v tri zaporedne stopnje:

::: {lang=it}

> Analizzare il decorso del movimento moderno in quanto strumento ideologico del
> capitale (dal 1901 circa, data del progetto di *città industriale* di Tony
> Garnier, al 1939 circa, data in cui la sua crisi è verificabile in tutti i
> settori e a tutti i livelli) significa tracciare una storia che si articola in
> tre fasi successive:

:::

:::

Ta razvoj, pravi, poteka od okoli projekta *industrijskega mesta* (1901) Tonyja
Garnierja, do okoli leta 1939, ko je kriza modernega gibanja kot ideološkega
instrumenta že preverljiva v vseh sektorjih in na vseh ravneh. Sledimo lahko
trem stopnjam tega razvoja: prva se zaključuje s formiranjem urbane ideologije
kot preseganja romanticizma; v drugi bomo priča razvoju vlog umetniških
avantgard kot ideoloških projektov "nezadovoljenih potreb", ki bodo izročeni
arhitekturi za njihovo konkretizacijo; ter tretjo, ko arhitekturna ideologija
postane *ideologija Plana*. [@tafuri1969peruna, 48]

::: alts

> a) prva, ki je priča formiranju urbane ideologije kot presegu arhitekturnega
> romanticizma;

::: {lang=it}

> a) una prima, che vede il formarsi dell'ideologia urbana come superamento del
> romanticismo architettonico;

:::

:::

::: alts

> b) druga, ki je priča razvoju vlog umetniških avantgard kot ideoloških
> projektov in kot opredelitev "nezadovoljenih potreb", ki so kot take (kot
> napredni cilji, ki jih slikarstvo, poezija, glasba ali kiparstvo ne morejo
> uresničiti, razen na čisto idealni ravni) izročene arhitekturi in urbanizmu:
> edine zmožne jih konkretizirati;

::: {lang=it}

> b) una seconda, che vede svilupparsi il ruolo delle avanguardie artistiche
> come progetti ideologici e come individuazioni di "bisogni insoddisfatti",
> consegnati come tali (come obiettivi avanzati che la pittura, la poesia, la
> musica o la scultura non possono realizzare che a livello puramente ideale)
> all'architettura e all'urbanistica: le uniche in grado di date loro
> concretizzazione;

:::

:::

::: alts

> c) tretja, kjer arhitekturna ideologija postane *ideologija Plana*: stopnja,
> ki bo sčasoma izpodrinjena in postavljena v krizo, ko se bo, po gospodarski
> krizi 1929, z razdelavo anticikličnih teorij in z mednarodno reorganizacijo
> kapitala, ideološka funkcija arhitekture zdaj zdela odveč, ali pa bo omejena
> na razvoj retrogardnih nalog in obstranske podpore.

::: {lang=it}

> c) una terza, in cui l'ideologia architettonica diviene *ideologia del Piano*:
> fase che viene a sua volta scavalcata e messa in crisi quando, dopo la crisi
> economica del '29, con l'elaborazione delle teorie anticicliche e la
> riorganizzazione internazionale del capitale, la funzione ideologica
> dell'architettura sembra rendersi ormai superflua, o limitata a svolgere
> compiti di retroguardia e sostegno marginale.

:::

:::

::: alts

> Zapiski, ki sledijo si ne prizadevajo izčrpati postavk tega procesa, ampak
> samo nakazati nekatere vogalne kamne tako, da priskrbijo metodološko ogrodje
> za prihodnje razdelave in natančnejše analize.

::: {lang=it}

> Le note che seguono non pretendono di esaurite l'impostazione di tale
> processo, ma solo di segnarne alcuni dei capisaldi, in modo da fornire un
> quadro metodologico a future elaborazioni e più circostanziate analisi.

:::

:::

> *** 

## [Dialektika avantgarde]

::: alts

> Zelo pomembno je poudariti, da med kritiziranjem Engelsove "moralne reakcije"
> pri soočenju z mestno množico, Benjamin svojo opazko izkoristi za to, da
> vpelje temo posplošitve pogojev delavskega razreda na urbano strukturo.[^12]

::: {lang=it}

> È molto importante sottolineare che nel criticare la "reazione morale" di
> Engels nei confronti della folla cittadina, Benjamin usi la sua osservazione
> per introdurre il tema della generalizzazione alla struttura urbana delle
> condizioni del lavoro operaio[12].

:::

:::

[^12]: W. Benjamin, *Schriften*, Suhrkamp Verlag, 1974, zv. 1, str. 620\.
    Benjamin piše: "Za Engelsa je na množici nekaj presenetljivega. To iz njega
    sproži moralno reakcijo. Vlogo igra tudi estetski odziv; hitrost s katero se
    mimoidoči pretakajo drug mimo drugega se ga ne dotakne na prijeten način.
    Čar njegovega opisa je v tem, kako se nepopravljivi kritični habitus
    prepleta s staromodnim tenorjem. Avtor prihaja iz še vedno provincialne
    Nemčije; morda ga skušnjava, da bi se izgubil v toku ljudi, ni nikoli
    dosegla."

<!--

[^12]: W. Benjamin, *Schriften*, Suhrkamp Verlag, 1955\. Trad. it. *Angelus
    Novus*, Einaudi, Torino, 1962\. "La folla -- scrive Benjamin -- ha, in
    Engels, qualcosa che lascia sgomenti. Essa suscita, in lui, una reazione
    morale. A cui si aggiunge una reazione estetica: il ritmo a cui i passanti
    s'incrociano e si oltrepassano lo offende spiacevolmente. Il fascino della
    sua descrizione è proprio nel modo in cui l'incorruttibile abito critico si
    fonde in essa col trono patriarcale. L'autore viene da una Germania ancora
    provinciale; forse la tentazione di perdersi in una marea di uomini non lo
    ha mai sfiorato". Op. cit., p. 98 dell'edizione italiana.

-->

Nemščina: "Für Engels hat die Menge etwas Bestürzendes. Sie löst eine moralische
Reaktion bei ihm aus. Eine ästhetische spielt daneben mit; ihn berührt das
Tempo, in dem die Passanten aneinander vorüberschießen, nicht angenehm. Es macht
den Reiz seiner Schilderung aus, wie sich der unbestechliche kritische Habitus
mit dem altväterischen Tenor in ihr verschränkt. Der Verfasser kommt aus einem
noch provinziellen Deutschland; vielleicht ist die Versuchung, in einem
Menschenstrom sich zu verlieren, an ihn nie herangetreten."

::: alts

> Lahko se ne strinjamo s pristranskostjo s katero Benjamin bere *Položaj
> delavskega razreda v Angliji*. Kar nas zanima je način s katerim prehaja od
> engelsovskega opisovanja množic do obravnave razmerja med Baudelairom in
> množico samo. Med vrednotenjem Engelsovih in Heglovih odzivov kot ostankov
> drže, ki je oddaljena od urbane realnosti v njenih novih kvalitativnih in
> kvantitativnih vidikih, Benjamin zabeleži, da sta spretnost in urnost, s
> katero se pariški *flâneur* premika v množici, postali naravna načina vedenja
> modernega uporabnika velemesta. "Ne glede na to, kako je zatrjeval, da se je
> oddaljil od nje, je ostal obarvan z njo, nanjo ni mogel gledati od zunaj,
> kakor Engels. [...] Množica je Baudelaireju tako notranja, da v njegovem delu
> zaman iščemo njen opis. [...] Baudelaire ne opisuje ne prebivalcev ne mesta.
> To odrekanje mu je omogočilo, da je eno priklical v podobi drugega. Njegova
> množica je vedno množica velemesta; njegov Pariz je vedno prenaseljen. Zato je
> toliko boljši od Barberja, pri katerem zaradi njegove metode opisovanja
> množica in mesto razpadeta. V *Tableaux parisiens* je skoraj povsod mogoče
> zaznati skrivno prisotnost množice."[^13]

::: {lang=it}

> Si può essere in disaccordo sulla parzialità con cui Benjamin legge la
> *Situazione delle classi lavoratrici in Inghilterra*. Ciò che interessa è il
> modo con cui egli passa dalla descrizione engelsiana della massa alle
> considerazioni sulle relazioni fra Baudelaire e la massa stessa. Valutando le
> reazioni di Engels e di Hegel come residui di un atteggiamento di distacco
> dalla realtà urbana nei suoi nuovi aspetti qualitativi e quantitativi,
> Benjamin nota che la facilità e la disinvoltura con cui il *flâneur* parigino
> si muove nella folla sono divenuti modi di comportamento naturali per il
> moderno fruitore della metropoli. "Per quanto grande potesse essere la
> distanza che [questi], per proprio conto pretendeva di assumere di fronte alla
> folla, restava intinto, impregnato da essa, e non poteva, come Engels,
> considerarla dall'esterno. La massa è talmente intrinseca a Baudelaire che si
> cerca invano in lui una descrizione di essa ... Baudelaire non descrive la
> popolazione né la città. E proprio questa rinuncia gli ha permesso di evocare
> l'una nell'immagine dell'altra. La sua folla è sempre quella della metropoli;
> la sua Parigi è sempre sovrapopolata. E ciò lo rende così superiore a Barbier,
> dove -- il procedimento essendo la descrizione -- le masse e la città cadono
> l'una al di fuori dell'altra. Nei *Tableaux parisiens* sì può provare, quasi
> sempre, la presenza segreta di una massa"[13].

:::

:::

Nemščina: "Wie groß auch immer der Abstand sein mochte, den er für seinen Teil
von ihr zu nehmen beanspruchte, er blieb von ihr tingiert, er konnte sie nicht
wie ein Engels von außen ansehen. [...] Die Masse ist Baudelaire derart
innerlich, daß man ihre Schilderung bei ihm vergebens sucht. [...] Baudelaire
schildert weder die Einwohnerschaft noch die Stadt. Dieser Verzicht hat ihn in
den Stand gesetzt, die eine in der Gestalt der anderen heraufzurufen. Seine
Menge ist immer die der Großstadt; sein Paris immer ein übervölkertes. Das ist
es, was ihn Barbier sehr überlegen macht, dem, weil sein Verfahren die
Schilderung ist, die Massen und die Stadt auseinan-derfallen. In den 'Tableaux
parisiens' ist fast überall die heimliche Gegenwart einer Masse nachweisbar."

[^13]: Prav tam, str. 620--622.

<!--

[^13]: *Op. cit.*, p. 99.

-->

::: alts

> Ta prisotnost, ali bolje, ta bistvena lastnost realnih produkcijskih razmerij
> v vedênju "občinstva", ki mesto uporablja tako, da ta nezavedno uporablja
> njega, je prepoznana v isti prisotnosti opazovalca -- kot je Baudelaire --, ki
> je prisiljen spoznati lasten nevzdržen položaj udeleženca v vse bolj
> posplošenem ublagovljenju prav v momentu, ko skozi lastno produkcijo odkrije,
> da je za poeta zdaj edina neizbežna nujnost prostitucija.[^14]

::: {lang=it}

> Tale presenza, tale immanenza, anzi, dei reali rapporti di produzione nel
> comportamento del "pubblico", che usa la città venendo inconsapevolmente usato
> da essa, si identifica nella stessa presenza di un'osservatore -- come
> Baudelaire -- che è costretto a riconoscere la propria insostenibile posizione
> di partecipe di una mercificazione sempre più generalizzata nel momento stesso
> in cui, tramite la propria produzione, scopre che l'unica necessità
> ineluttabile per il poeta è ormai la prostituzionei[14].

:::

:::

[^14]: "S pojavom velikih mest prostitucija pridobi v posest nove skrivnosti.
    Ena od teh je predvsem labirintski značaj mesta samega. Labirint, katerega
    podoba je za flaneurja postala meso in kri, je skozi prostitucijo tako rekoč
    barvito spremenjen." Prav tam, str. 688.

<!--

[^14]: "La prostituzione, col sorgere delle metropoli, entra in possesso di
    nuovi arcani. Uno dei quali è, anzitutto, il carattere labirintico della
    città stessa: l'immagine del labirinto è entrata al *flâneur* nella carne e
    nel sangue. La prostituzione, per così dire, la colora diversamente".
    Ibidem, p. 137.

-->

Nemščina: "Die Prostitution kommt mit der Entstehung der großen Städte in den
Besitz neuer Arkana. Deren eines ist zunächst der labyrinthische Charakter der
Stadt selbst. Das Labyrinth, dessen Bild dem flaneur in Fleisch und Blut
eingegangen ist, erscheint durch die Prostitution gleichsam farbig gerändert."

::: alts

> Baudelairejeva poezija, kot produkcija razstavljena na Svetovnih razstavah,
> ali preoblikovanje urbane morfologije, ki jo v gibanje spravi Haussmann,
> kažejo na zavedanje o nerazrešljivi dialektiki, ki obstaja med enoličnostjo in
> različnostjo. Glede strukture novega buržoaznega mesta se še ne more posebej
> govoriti o napetosti med izjemo in pravilom. Toda lahko se govori o napetosti
> med prisilnim ublagovljenjem objekta in subjektivnimi poskusi povrnitve --
> izmišljene -- avtentičnosti.

::: {lang=it}

> La poesia di Baudelaire, come la produzione messa in mostra nelle Esposizioni
> universali, o la trasformazione della morfologia urbana messa in moto da
> Haussmann, segnano la presa di coscienza della indissolubile dialettica
> esistente fra uniformità e diversità. Specie per la struttura della nuova
> città borghese non si può ancora parlare di tensione fra l'eccezione e la
> regola. Ma si può parlare di tensione fra la obbligata mercificazione
> dell'oggetto e i tentativi soggettivi di recuperarne -- fittiziamente --
> l'autenticità.

:::

:::

::: alts

> Le da zdaj ni podana več nobena druga pot kot ta, ki iskanje avtentičnosti
> reducira na iskanje ekscentričnega. Ni le poet ta, ki mora sprejeti svoje
> stanje pantomimika -- to lahko, mimogrede, razloži zakaj se celotna sodobna
> umetnost, zavestna lastnega mistificiranega značaja, hkrati nosi kot namerni
> "herojski" akt in kot *bluff* --: bolj gre za celotno mesto, objektivno
> strukturirano kot funkcionalen stroj za ekstrakcijo družbene presežne
> vrednosti, ki z lastnimi mehanizmi pogojevanja reproducira realnost
> industrijskega načina produkcije.

::: {lang=it}

> Solo che ora non è più data altra via che ridurre la ricerca dell'autenticità
> alla ricerca dell'eccentrico. Non è solo il poeta a dover accettare la propria
> condizione di mimo -- ciò può spiegare, fra parentesi, perché l'intera arte
> contemporanea si dia, insieme, come atto volutamente "eroico" e come *bluff*,
> consapevole del proprio carattere mistificato --: è piuttosto l'intera città,
> oggettivamente strutturata come macchina funzionale all'estrazione di
> plusvalore sociale, che riproduce, nei propri meccanismi di condizionamento,
> la realtà dei modi di produzione industriale.

:::

:::

::: alts

> Benjamin tesno poveže zaton *izučenosti* in *izkušnje* -- še funkcionalni v
> manufakturi -- med delavstvom z izkušnjo *šoka*, ki je tipična za urbano
> stanje.

::: {lang=it}

> Benjamin lega strettamente il declino dell'*esercizio* e dell'*esperienza* nel
> lavoro operaio -- ancora funzionali nella manifattura -- con l'esperienza
> dello *choc*, tipica della condizione urbana.

:::

:::

::: alts

> Benjamin piše: "Neizučen delavec je tisti, ki je zaradi dresure stroja najbolj
> razvrednoten. Njegovo delo je pred izkušnjo zapečateno. Nad njim je vaja
> izgubila svojo pravico. Kar lunapark doseže s svojimi zibajočimi lonci in
> podobnimi zabavišči, ni nič drugega kot vzorec dresure, ki ji je neizučeni
> delavec izpostavljen v tovarni (vzorec, ki je včasih moral zanj predstavljati
> celoten program; kajti umetnost ekscentrikov, v kateri se je mali človek v
> lunaparku lahko izučil, je cvetela hkrati z brezposelnostjo). Poejevo besedilo
> [Benjamin se nanaša na *Moški množice* v Baudelairovem prevodu] jasno pokaže
> na pravo povezavo med divjostjo in disciplino. Njegovi mimoidoči se obnašajo,
> kot da bi se, prilagojeni avtomatom, lahko izražali zgolj avtomatsko. Njihovo
> vedenje je odziv na šok. 'Ko so jih udarili, so globoko pozdravili tiste, od
> katerih so dobili udarec.'"[^15]

::: {lang=it}

> "L'operaio non specializzato -- egli scrive[15], -- è quello più profondamente
> degradato dal tirocinio della macchina. Il suo lavoro è impermeabile
> all'esperienza. L'esercizio non vi ha più alcun diritto. Ciò che il *lunapark*
> realizza nelle sue gabbie volanti e in altri divertimenti del genere non è che
> un saggio del tirocinio a cui l'operaio non specializzato è sottoposto nella
> fabbrica (un saggio che, a volte, dovette sostituire per lui l'intero
> programma, poiché l'arte dell'eccentrico, in cui l'uomo qualunque poteva
> esercitarsi nei *lunapark*, prosperava nei periodi di disoccupazione). Il
> testo di Poe -- [Benjamin si riferisce all'*Uomo della folla*, tradotto da
> Baudelaire] -- rende evidente il rapporto tra sfrenatezza e disciplina. I suoi
> passanti si comportano come se, adattati ad automi, non potessero più
> esprimersi che in modo automatico. Il loro comportamento è una reazione a
> *chocs*. 'Quando erano urtati, salutavano profondamente quelli da cui avevano
> ricevuto il colp

:::

:::

Nemščina: "Der ungelernte Arbeiter ist der durch die Dressur der Maschine am
tiefsten Entwürdigte. Seine Arbeit ist gegen Erfahrung abgedichtet. An ihr hat
die Übung ihr Recht verloren. Was der Lunapark in seinen Wackeltöpfen und
verwandten Amüsements zustande bringt, ist nichts als eine Kostprobe der
Dressur, der der ungelernte Arbeiter in der Fabrik unterworfen wird (eine
Kostprobe, die ihm zeitweise für das gesamte Programm zu stehen hatte; denn die
Kunst des Exzentriks, in der sich der kleine Mann in den Lunaparks konnte
schulen lassen, stand zugleich mit der Arbeitslosigkeit hoch im Flor). Poes Text
macht den wahren Zusammenhang zwischen Wildheit und Disziplin einsichtig. Seine
Passanten benehmen sich so, als wenn sie, angepaßt an die Automaten, nur noch
automatisch sich äußern könnten. Ihr Verhalten ist eine Reaktion auf Chocks.
'Wenn man sie anstieß, so grüßten sie diejenigen tief, von denen sie ihren Stoß
bekommen hatten.'"

[^15]: Prav tam, str. 632.

<!--

[^15]: Ibidem, pp. 108--109.

-->

::: alts

> Med kodeksom vedenja povezanim z izkušnjo *šoka* in tehniko iger na srečo
> obstaja torej globoka privlačnost. "Ker je vsako dejanje na stroju ločeno od
> prejšnjega, tako kot je *coup* v igri na srečo ločen od zadnjega, je tlaka
> mezdnega delavca na svoj način enakovredna tlaki igralca. Obe deli sta prav
> tako osvobojeni vsebine."[^16]

::: {lang=it}

> Fra il codice di comportamento connesso all'esperienza dello *choc* e la
> tecnica del gioco d'azzardo esiste quindi una profonda affinità. "Ogni
> intervento sulla macchina è altrettanto ermeticamente separato da quello che
> lo ha preceduto quanto un *coup* della partita d'azzardo dal *coup*
> immediatamente precedente; e la schiavitù del salariato fa, in qualche modo,
> *pendant* a quella del giocatore. Il lavoro dell'uno e dell'altro è egualmente
> libero da ogni contenuto"[16].

:::

:::

<!--

Nemščina: "Indem jeder Handgriff an der Maschine gegen den ihm
voraufgegangenen ebenso abgedichtet ist, wie ein coup der Hasardpartie gegen den
jeweils letzten, stellt die Fron des Lohnarbeiters auf ihre Weise ein Pendant zu
der Fron des Spielers. Beider Arbeit ist von Inhalt gleich sehr befreit."

-->

[^16]: Prav tam, str. 633.

<!--

[^16]: Ibidem, p. 110.

-->

::: alts

> Kljub natančnosti svojih opažanj, Benjamin ne poveže -- niti v spisih o
> Baudelaireju, niti v *Umetnini v času, ko jo je mogoče tehnično reproducirati*
> -- tega vdora produkcijskih načinov v strukturo urbane morfologije z odzivom,
> ki ga zgodovinske avantgarde podajo na temo mesta.

::: {lang=it}

> Malgrado l'acutezza delle sue osservazioni, Benjamin non lega -- né nei saggi
> su Baudelaire, né nell'*Opera d'arte nell'epoca della sua riproducibilità
> tecnica* -- tale invasione dei modi di produzione nella struttura della
> morfologia urbana, alla risposta data dalle avanguardie storiche al tema della
> città.

:::

:::

::: alts

> *Pejsaži* in veleblagovnice Pariza so, kakor Razstave, zagotovo kraji v
> katerih množica, s tem ko postane sama sebi spektakel, najde prostorski in
> vizualni instrument za samoizobraževanje iz stališča kapitala. Toda
> ludično-pedagoška izkušnja še vedno prav z osredotočanjem na izjemne
> arhitekturne tipologije nevarno razkriva, skozi celoten potek 19\. stoletja,
> nepopolnost svojih predlogov. Ideologija občinstva pravzaprav ni sama sebi
> namen. Ni nič drugega kot moment ideologije mesta kot produkcijske enote v
> pravem pomenu, in sočasno kot instrumenta koordinacije cikla
> produkcija-distribucija-konsumpcija.

::: {lang=it}

> *Passages* e grandi magazzini di Parigi sono certo, come le Esposizioni, i
> luoghi in cui la folla, divenendo spettacolo a se stessa, trova lo strumento
> spaziale e visivo per un'autoeducazione dal punto di vista del capitale. Ma
> l'esperienza ludico-pedagogica, proprio concentrandosi in tipologie
> architettoniche eccezionali, rivela ancora pericolosamente, per tutto il corso
> dell''800, la parzialità delle sue proposte. L'ideologia del pubblico non è
> infatti fine a se stessa. Essa non è che un momento dell'ideologia della città
> come unità produttiva in senso proprio, e, contemporaneamente, come strumento
> di coordinamento del ciclo produzione-distribuzione-consumo.

:::

:::

::: alts

> Zaradi tega se mora ideologija konsumpcije, daleč od tega, da predstavlja
> izoliran ali zaporeden moment produkcijske organizacije, občinstvu ponuditi
> kot ideologija *pravilne uporabe* mesta. (Na tem mestu velja mimogrede
> spomniti kako je problem vedênja vplival na izkušnje evropskih avantgard in na
> simptomatičen primer Loosa, ki objavi -- leta 1903, po povratku iz Združenih
> držav -- dve številki revije *Das Andere*, ki sta posvečeni dunajski
> buržoaziji predstaviti, s polemičnimi in ironičnimi toni, "moderne" načine
> mestnega vedenja).

::: {lang=it}

> È per questo che l'ideologia del consumo, lungi dal costituite un momento
> isolato o successivo dell'organizzazione produttiva, si deve offrire al
> pubblico come ideologia del *corretto uso* della città. (Può cadere a
> proposito ricordare qui, per inciso, quanto il problema del comportamento
> influenzi le esperienze delle avanguardie europee, e l'esempio sintomatico di
> Loos che pubblica -- nel 1903, al ritorno dagli Stati Uniti -- due numeri
> della rivista *Das Andere*, dedicati ad introdurre, con toni polemici ed
> ironici, "moderni" modi di comportamento cittadino nella borghesia viennese).

:::

:::

::: alts

> Dokler se bo izkušnja množice prevajala -- kot v Baudelaireju -- v trpečo
> zavest udeležbe, bo ta služila posploševanju dejavne realnosti, ne pa
> prispevala k njenemu napredku. Na tej točki, in samo na tej točki, je
> lingvistična revolucija sodobne umetnosti pozvana ponuditi svoj prispevek.

::: {lang=it}

> Fin quando l'esperienza della folla si tradurrà -- come in Baudelaire -- in
> sofferta coscienza di partecipazione, essa servirà a generalizzare una realtà
> operante, ma non a contribuire alla sua avanzata. È a questo punto, e solo a
> questo punto, che la rivoluzione linguistica dell'arte contemporanea è
> chiamata ad offrite il proprio contributo.

:::

:::

Sledi torej obravnava umetniških avantgard v odnosu do velemesta, ki je tudi
nadaljevanje ali razvoj distinktivno tafurijanske teorije o vlogah in
"funkcijah" avantgard, ki mora biti sestavni del kritično-zgodovinske "metode",
da je zgodovinska obravnava (historiziranje) modernizma sploh mogoča. Sprva,
posredno preko Benjaminovih spisih o Baudelairju, uvede pojem velemesta, [Nit
teorije o velemestu seveda seže do v arhitekturi dobro poznanega spisa
@simmel2000metropole; Velemesto bo tudi osrednji pojem Tafurijevih sodelavcev,
gl. @cacciari1993dialectics] ki je prizorišče kapitalističnih procesov družbene
abstrakcije. Kot tako se ne nanaša na konkreten kraj (čeprav velemestno stanje
spremljajo konkretni kapitalistični prostorski procesi), ampak je realna
abstrakcija -- kapitalizmu ustrezna družbena forma tehnično-prostorske
konfiguracije. Eno prvih obravnav življenjskih pogojev velemesta opravi Engels,
ki se še, iz oddaljenega in provincialnega stališča, na nove značilnosti množice
odzove moralistično. Baudelaire pa, ugotavlja Benjamin, ki ga povzema Tafuri,
nanjo ni mogel gledati od zunaj in jasno mu je, da množica pripada velemestu in
velemesto pripada množici, da je idealni in "naravni" tip posameznika iz takšne
množice apatični *flâneur*, ki je "prisiljen spoznati lasten nevzdržen položaj
udeleženca v vse bolj posplošenem ublagovljenju" [@tafuri1969peruna, 49] in
mora "iskanje avtentičnosti reducirati na iskanje ekscentričnosti".
[@tafuri1969peruna, 50] Baudelairejevega flâneurja Benjamin primerja z
delavcem, oziroma velemestno stanje (kapitalistični urbani razvoj) primerja s
kapitalističnim razvojem znotraj tovarne: izginotje vsebine dela in dresura
delavca ob vpeljavi strojev ima svoj analog v velemestnem Lunaparku,
veleblagovnici, igralnici ... njegovi mimoidoči se obnašajo, "kot da bi se,
prilagojeni avtomatom, lahko izražali zgolj avtomatsko". [@tafuri1969peruna,
50] Na tem mestu Tafuri nadaljuje z izvirno povezavo med tem vdorom
produkcijskih načinov v strukturo urbane morfologije in odzivom velemestnih
avantgard. Jasno je, da tu zapolnjuje vrzel med operaistično *družbeno tovarno*
oziroma tezo o posploševanju kapitalističnih odnosov izven tovarne na vse bolj
splošnih ravneh in arhitekturno teorijo (od sociologije in zgodovine urbanega
razvoja do vloge modernističnih gibanj). Pejsaži in veleblagovnice, nadaljuje,
vsekakor so kraji, vizualni in prostorski instrumenti, za "samoizobraževanje
množic iz stališča kapitala", to je instrumenti za subjektivacijo po podobi
kapitala. Toda ideologija občinstva ne more biti sama sebi namen, "ni nič
drugega kot moment ideologije mesta kot produkcijske enote v pravem pomenu in
sočasno kot instrumenta koordinacije cikla produkcija-distribucija-konsumpcija".
[@tafuri1969peruna, 51] In tu arhitekturne tipologije 19\. stoletja razkrivajo
nepopolnost svojih predlogov: dokler izkušnjo množice posredujejo le kot trpečo
udeležbo, prispevajo k posploševanju produkcijske realnosti, ne pa tudi k
njenemu napredku. (Tafurijeva teorija avantgard jim torej ne nalaga le
posredovanja organske enotnosti kapitalističnega cikla
produkcija-distribucija-konsumpcija, ampak teoretizira tudi o njenem
produktivnem vključevanju vanj.) Velemestne avantgarde se bodo torej ponudile
tudi kot ideologije *pravilne uporabe* mesta:

::: alts

> Odvzeti izkušnjo *šoka* od vsakega avtomatizma, na tej izkušnji utemeljiti
> vizualne kodekse in kodekse delovanja izposojene od značilnosti, ki so jih
> kapitalistična velemesta že utrdila -- hitrost preoblikovanja, organizacija in
> sočasnost komunikacij, pospešeni časi uporabe, eklekticizem --, reducirati
> strukturo umetniške izkušnje na čisti objekt (jasna metafora objekta-blaga),
> soudeležiti občinstvo, ki je poenoteno v deklarirano medrazredno, in *zatorej*
> protiburžoazno, ideologijo: to so naloge, ki so jih avantgarde 20\. stoletja,
> v celoti, privzele kot svoje.

::: {lang=it}

> Sottrarre l'esperienza dello *choc* ad ogni automatismo, fondare su
> quell'esperienza codici visivi e di azione mutuati dalle caratteristiche già
> consolidate della metropoli capitalista -- velocità dei tempi di
> trasformazione, organizzazione e simultaneità di comunicazioni, tempi
> accelerati d'uso, eclettismo --, ridurre al puro oggetto (metafora palese
> dell'oggetto-merce) la struttura dell'esperienza artistica, coinvolgere il
> pubblico, unificato in una dichiarata ideologia interclassista e *perciò*
> antiborghese: questi sono i compiti che, nel loro insieme, vengono assunti in
> proprio dalle avanguardie del '900.

:::

:::

::: alts

> Ponavljamo: v celoti, onkraj razlikovanja med konstruktivizmom in protestno
> umetnostjo. Kubizem, futurizem, dada, De Stijl. Zgodovinske avantgarde
> nastajajo in si sledijo po zakonu, ki je tipičen za industrijsko produkcijo:
> njeno bistvo je nenehna tehnična revolucija. Za vse avantgarde -- ne samo
> slikarske -- je zakon montaže temeljnega pomena. In ker montirani objekti
> pripadajo realnemu svetu, platno postane nevtralno polje na katerega se
> projicira *izkušnja šoka*, ki se trpi v mestu. Reči bolje, zdaj ni problem
> naučiti "trpeti" ta *šok*, ampak ga vpiti ter ponotranjiti kot neizogibno
> stanje bivanja.

::: {lang=it}

> Lo ripetiamo: nel loro insieme, al di là di ogni distinzione fra
> costruttivismo e arte di protesta. Cubismo, Futurismo, Dada, De Stijl. Le
> avanguardie storiche sorgono e si succedono seguendo la legge tipica della
> produzione industriale: la continua rivoluzione tecnica ne è l'essenza. Per
> tutte le avanguardie -- e non solo pittoriche -- la legge del montaggio è
> fondamentale. E poiché gli oggetti montati appartengono al mondo reale, il
> quadro diviene il campo neutro in cui si proietta l'*esperienza dello choc*
> subita nella città. Anzi, ora il problema è di insegnare a non "subire" quello
> *choc*, ma di assorbirlo, di introiettarlo come inevitabile condizione di
> esistenza.

:::

:::

::: alts

> Zakoni produkcije tako vstopajo kot del novega univerzuma konvencij, ki so
> izrecno postavljene kot "naravne". To je razlog zakaj si avantgarde ne
> zastavljajo problema približevanja občinstvu. Bolje rečeno, to je problem, ki
> ga sploh ni bilo mogoče postaviti: ker niso počele nič drugega kot
> interpretirale nekaj nujnega in univerzalnega, lahko avantgarde zelo dobro
> sprejmejo začasno nepriljubljenost, dobro vedoč, da je njihov prelom s
> preteklostjo pogoj, ki utemeljuje vrednost njihovih modelov delovanja.

::: {lang=it}

> Le leggi della produzione entrano così a far parte di un nuovo universo di
> convenzioni, poste esplicitamente come "naturali". È qui la ragione per cui le
> avanguardie non si pongono il problema di un avvicinamento al pubblico. Anzi,
> questo è un problema che non si può neppure porre: non facendo che
> interpretare qualcosa di necessario e universale, le avanguardie possono
> benissimo accettare una provvisoria impopolarità, ben sapendo che la loro
> rottura con il passato è la condizione che fonda il loro valore di modelli per
> l'azione.

:::

:::

::: alts

> Umetnost kot model delovanja: veliko poglavitno vodilo umetniške vstaje
> moderne buržoazije, toda istočasno absolut iz katerega nastajajo nova in
> nerazrešljiva protislovja. Življenje in umetnost, razkrita kot antitetična,
> morata skleniti poiskati, ali instrumente posredovanja -- in tako celotna
> umetniška produkcija sprejme problemskost kot nov etični horizont --, ali
> načine prehajanja umetnosti v življenje, četudi za ceno uresničitve
> hegeljanske prerokbe o smrti umetnosti.

::: {lang=it}

> Arte come modello di azione: il grande principio-guida della riscossa
> artistica della borghesia moderna, ma nello stesso tempo l'assoluto da cui
> nascono nuove e insopprimibili contraddizioni. Vita e arte, rivelatesi
> antitetiche, debbono indurre a ricercare o strumenti di mediazione -- ed ecco
> l'intera produzione artistica accettare la problematicità come nuovo orizzonte
> etico --, o i modi di passaggio dell'arte alla vita, anche se ciò deve costare
> l'avverarsi della profezia hegeliana della morte dell'arte.

:::

:::

::: alts

> Tu se bolj konkretno razkrivajo vezi, ki povezujejo veliko tradicijo buržoazne
> umetnosti v enotno celoto. Začetno sklicevanje na Piranesija, istočasno kot
> teoretika in kritika *ne več univerzalističnega ter ne še buržoaznega* stanja
> umetnosti, je zdaj upravičeno s svojo razsvetljevalno funkcijo: kriticizem,
> problemskost, projektiranje, to so pilastri na katerih temelji tradicija tega
> "modernega gibanja", ki nedvoumno ima, kot projekt modeliranja "buržoaznega
> človeka" kot absolutnega "tipa", lastno notranjo koherentnost. (Četudi ne gre
> za koherentnost, ki bi jo trenutno zgodovinopisje priznavalo.)

::: {lang=it}

> È qui che si rivelano più concretamente i legami che stringono in un tutto
> unico la grande tradizione dell'arte borghese. Il riferimento iniziale a
> Piranesi, come teorico e critico, allo stesso tempo, delle condizioni di
> un'arte *non più universalizzante e non ancora borghese*, sì giustifica ora
> per la sua funzione illuminatrice: criticismo, problematicità, progettismo,
> questi sono i pilastri su cui si fonda la tradizione di quel "movimento
> moderno" che, in quanto progetto di modellazione dell'"uomo borghese" come
> "tipo" assoluto, ha una sua indubbia coerenza interna. (Anche se non si tratta
> della coerenza riconosciuta dalla storiografia corrente).

:::

:::

::: alts

> Tako Piranesijev *Campomarzio* kot Picassova *Dame au violon* sta "projekta";
> čeprav prvi organizira arhitekturno razsežnost in drugi organizira človeško
> vedênje. Oba uporabljata tehniko *šoka*, četudi piranesijska grafika uporablja
> vnaprej formirane zgodovinske materiale, in Picassova slika umetne materiale,
> kakor bo to, kasneje in bolj strogo, počel Duchamp. Oba odkrijeta realnost
> univerzuma-stroja, četudi ga urbani projekt 18\. stoletja napravi abstraktnega
> in se nanj odzove z grozo, Picassovo platno pa deluje povsem znotraj njega.

::: {lang=it}

> Sia il *Campomarzio* di Piranesi che la *Dame au violon* di Picasso sono dei
> "progetti"; anche se il primo organizza una dimensione architettonica, e la
> seconda organizza un comportamento umano. Entrambi usano la tecnica dello
> *choc*, anche se il grafico piranesiano usa materiali storici preformati, e il
> quadro di Picasso materiali artificiali, come, con più rigore più tardi, farà
> Duchamp. Entrambi scoprono la realtà di un universo-macchina, anche se il
> progetto urbano settecentesco lo rende astratto e reagisce con terrore a tale
> scoperta, e la tela picassiana agisce completamente all'interno di esso.

:::

:::

::: alts

> Toda, kar je še bolj pomembno, skozi presežek resnice, ki je dosežena z
> globoko kritičnimi instrumenti formalne razdelave, tako Piranesi kot Picasso
> realnost, ki se še vedno lahko smatra kot povsem partikularna, napravita
> "univerzalno". Toda "projekt" prirojen kubistični sliki sega onkraj same
> slike. *Ready-made objects*, ki jih leta 1912 Braque in Picasso uvedeta ter
> Duchamp kodificira kot nove instrumente komunikacije, potrjujejo
> samozadostnost realnosti in dokončno zavračanje, s strani realnosti same,
> vsake reprezentacije. Slikar lahko zgolj analizira to realnost. Njegovo
> domnevno gospostvo nad formo ni drugega kot zakrivanje nečesa, kar še noče
> biti sprejeto kot tako: da je forma zdaj tista, ki prevladuje nad slikarjem.

::: {lang=it}

> Ma, ciò che è più importante, sia Piranesi che Picasso rendono "universale",
> tramite l'eccesso di verità raggiunto con gli strumenti di un'elaborazione
> formale profondamente critica, una realtà che poteva ancora essere considerata
> del tutto particolare. Il "progetto" insito nel quadro cubista va però al di
> là del quadro stesso. I *ready-mades objects* introdotti dal 1912 da Braque e
> Picasso, e codificati come nuovi strumenti di comunicazione da Duchamp,
> sanciscono l'autosufficienza della realtà, e il ripudio definitivo, da parte
> della realtà stessa, di ogni rappresentazione. Il pittore può solo analizzare
> tale realtà. Il suo preteso dominio della forma non è che copertura di
> qualcosa che non vuole ancora essere accettato come tale: che è ormai la forma
> a dominare il pittore.

:::

:::

::: alts

> Le da je s "formo" zdaj treba razumeti logiko subjektivnih reakcij na
> objektivni univerzum produkcije. Kubizem, v svoji celoti, teži k definiranju
> zakonov teh reakcij: simptomatično je, da se celotna njegova zgodba začne pri
> subjektu in konča z njegovim popolnim zavračanjem. (Tega se bo z
> zaskrbljenostjo dobro zavedal Apollinaire). Kar zadeva "projekt", ki ga
> kubizem želi realizirati, je to vedênje. Toda njegov antinaturalizem ne
> vsebuje nobenega prepričljivega elementa za soočenje z občinstvom: nekdo se
> lahko prepriča le, če smatra, da je objekt prepričevanja zunanji in nadrejen
> tistemu, kateremu je naslovljen. Kubizem pa, nasprotno, namerava dokazati
> realnost "nove narave", ki jo je ustvaril kapital, njen nujni in univerzalen
> značaj, sovpadanje nujnosti in svobode v njej.

::: {lang=it}

> Solo che ora per "forma" bisogna intendere la logica delle reazioni soggettive
> all'universo oggettivo della produzione. Il Cubismo, nel suo complesso, tende
> a definire le leggi di tali reazioni: è sintomatico che tutta la sua vicenda
> parta dal soggetto e sfoci nel più assoluto ripudio di esso. (Ben lo
> avvertirà, e con inquietitudine, Apollinaire). In quanto "progetto" ciò che il
> Cubismo vuole realizzare è un comportamento. Il suo antinaturalismo non
> contiene però alcun elemento persuasivo nei confronti del pubblico: si
> persuade qualcuno solo se si ritiene che l'oggetto della persuasione sia
> esterno e sovrapposto a chi ci si indirizza. Il Cubismo intende invece
> dimostrare la realtà della "nuova natura" creata dal capitale, il suo
> carattere necessario e universale, la coincidenza, in esso, di necessità e
> libertà.

:::

:::

::: alts

> Zato platna Braqueja, Picassa in, še bolj, Grisa, za podajanje absolutne forme
> univerzumu diskurza *civilisation machiniste* uporabljajo tehniko montaže.
> Primitivizem in antihistoricizem sta posledici, in ne vzroka, njihovih
> temeljnih odločitev.

::: {lang=it}

> Per questo la tela di Braque, di Picasso, e, ancor più, di Gris, adopera la
> tecnica del montaggio per dare forma assoluta all'universo di discorso della
> *civilisation machiniste*. Primitivismo e antistoricismo sono conseguenze e
> non cause delle loro scelte fondamentali.

:::

:::

::: alts

> Kot tehniki analize totalizirajočega univerzuma, sta tako Kubizem kot De Stijl
> izrecni vabili k akciji: glede njunih umetniških produktov bi lahko pisali o
> *fetišizaciji umetniškega objekta in njegovi skrivnosti*.

::: {lang=it}

> In quanto tecniche di analisi di un'universo totalizzante, sia il Cubismo che
> il De Stijl sono espliciti inviti all'azione: per i loro prodotti artistici si
> potrebbe benissimo scrivere della *feticizzazione dell'oggetto artistico e del
> suo arcano*.

:::

:::

::: alts

> Občinstvo je treba izzvati: le na ta način ga je možno aktivno umestiti v
> *univerzum natančnosti*, ki mu vladajo zakoni produkcije. Pasivnost
> Baudelairejevega *flâneurja* mora biti premagana in pretvorjena v dejavno
> soudeležbo v urbanem dogajanju. Mesto je objekt o katerem ne govorijo ne
> kubistična platna, ne futuristične "klofute", ne nihilizem dada, temveč je --
> ravno ker se nenehno predpostavlja -- referenčna vrednota h kateri težijo
> avantgarde. Mondrian bo imel pogum mesto "nominirati" kot poslednji objekt h
> kateremu teži neoplastična kompozicija: toda prisiljen bo spoznati, da bo
> enkrat, ko bo prevedeno v urbano strukturo, slikarstvo -- zdaj reducirano na
> čisti model vedênja -- moralo umreti.

::: {lang=it}

> Il pubblico deve essere provocato: solo in tal modo esso può essere inserito
> attivamente nell'*universo della precisione* dominato dalle leggi della
> produzione. La passività del *flâneur* di Baudelaire deve essere vinta, e
> tradotta in fattiva compartecipazione alla scena urbana. È la città l'oggetto
> di cui né le tele cubiste né gli "schiaffi" futuristi, né il nichilismo Dada
> parlano, ma che -- proprio perché continuamente presupposto -- è il valore di
> riferimento cui le avanguardie tendono. Mondrian avrà il coraggio di
> "nominare" la città come l'oggetto finale cui tende la composizione
> neoplastica: ma sarà costretto a riconoscere che una volta tradotta in
> struttura urbana, la pittura -- ormai ridotta a puro modello di comportamento
> -- dovrà morire.

:::

:::

::: alts

> Baudelaire odkrije, da lahko prav poetovi poskusi osvoboditi se svojih
> objektivnih pogojev poudarijo ublagovljenje pesniškega produkta: prostitucija
> umetnika sledi trenutku njegove najvišje humane iskrenosti. De Stijl in, še
> bolj, dada odkrijeta, da za samomor umetnosti obstajata dve poti: tiha
> utopitev v strukture mesta z idealizacijo protislovij, ali nasilno uvajanje,
> prav tako idealiziranega, iracionalnega v strukture umetniške komunikacije, ki
> je izpeljana iz istega mesta.

::: {lang=it}

> Baudelaire scopre che la mercificazione del prodotto poetico può essere
> accentuata proprio dal tentativo del poeta a svincolarsi dalle sue condizioni
> oggettive: la prostituzione dell'artista consegue al massimo momento della sua
> sincerità umana. De Stijl e, ancor più, Dada, scoprono che esistono due vie
> per il suicidio dell'arte: il silenzioso immergersi nelle strutture della
> città, idealizzandone le contraddizioni, o la violenta immissione nelle
> strutture della comunicazione artistica dell'irrazionale -- anch'esso
> idealizzato -- mutuato dalla stessa città.

:::

:::

::: alts

> *De Stijl* postane metoda formalnega nadzora produkcije, dada želi
> apokaliptično razglasiti absurdnost, ki je imanentna v njej. Toda nihilistična
> kritika, ki jo formulira dada, na koncu postane instrument nadzora
> projektiranja: ne smemo se pustiti presenetiti, ko najdemo -- celo v
> filološkem smislu -- številne stične točke med najbolj "konstruktivnimi" in
> najbolj destruktivnimi gibanji 20\. stoletja.

::: {lang=it}

> *De Stijl* diviene metodo di controllo formale della produzione, Dada vuole
> enunciare apocalitticamente l'assurdo immanente in essa. Eppure la critica
> nichilista formulata da Dada finisce per divenire strumento di controllo per
> la progettazione: non ci si dovrà meravigliare incontrando -- anche in sede
> filologica -- molti punti di tangenza fra i movimenti più "costruttivi" del
> '900 e quello più distruttivo.

:::

:::

::: alts

> Kaj je krut razkroj lingvističnega materiala in antiprojektiranje dada, če ne,
> kljub vsemu, sublimacija avtomatizma in ublagovljenja "vrednot", ki so zdaj
> razpršene na vseh ravneh obstoja naprednega kapitalizma? De Stijl in Bauhaus
> -- prvi na faktičen, drugi na eklektičen način -- predstavita *ideologijo
> plana* v design, ki je vse globlje povezan z mestom kot produkcijsko bazo:
> dada z absurdom dokaže, ne da bi jo imenoval, potrebo po planu.

::: {lang=it}

> La scomposizione efferata del materiale linguistico e l'antiprogettismo di
> Dada cosa sono se non sublimazione, malgrado tutto, dell'automatismo e della
> mercificazione dei "valori", ormai diffusi a tutti i livelli dell'esistenza,
> dall'avanzata capitalista? De Stijl e Bauhaus -- il primo in modo fazioso, il
> secondo in modo eclettico -- introducono l'*ideologia del piano* in un design
> legato sempre più profondamente alla città come struttura produttiva: Dada
> dimostra per assurdo, senza nominarla, l'esigenza del piano.

:::

:::

::: alts

> Toda gre za še nekaj več. Vse zgodovinske avantgarde delujejo z usvajanjem
> modela delovanja političnih strank. Dada in nadrealizem vsekakor lahko vidimo
> kot posamezne izraze anarhičnega duha, toda De Stijl in Bauhaus se ne
> obotavljata izrecno predstaviti kot globalni alternativi političnim praksam.
> Alternativi, naj se ve, ki privzameta vse značilnosti etične izbire.

::: {lang=it}

> Ma v'è di più. Tutte le avanguardie storiche agiscono adottando come modello
> l'azione dei partiti politici. Dada e Surrealismo possono certo essere visti
> come espressioni particolari dello spirito anarchico, ma il De Stijl e il
> Bauhaus non si peritano di porsi esplicitamente come alternative globali alla
> prassi politica. Alternativa, si noti, che assume tutti i caratteri di una
> scelta etica.

:::

:::

::: alts

> De Stijl nasproti Kaosu, empiričnemu, vsakdanjemu postavi princip Forme. In
> Forma je ta, ki upošteva tisto, kar konkretno napravi realnost brezoblično,
> kaotično, osiromašeno: horizont industrijske produkcije, ki duhovno siromaši
> svet, je oddaljen kot vrednota v pravem pomenu, ki je nato, skozi njeno
> sublimacijo, postopno preoblikovana v novo vrednoto. Neoplastična razgradnja
> elementarnih form ustreza odkritju, da "novega bogastva" duha ni več iskati
> izven "nove revščine", ki jo zajema strojna civilizacija; ponovna sestava teh
> elementarnih form sublimira strojni univerzum ter dokazuje, da zdaj ni več
> mogoča nikakršna forma ponovnega osvajanja totalitete (tako biti kot
> umetnosti), ki ne bi bila odvisna od problemskosti forme same.

::: {lang=it}

> De Stijl oppone al Caos, all'empirico, al quotidiano, il principio della
> Forma. Ed è una Forma che tiene conto di ciò che concretamente rende informe,
> caotica, impoverita, la realtà: l'orizzonte della produzione industriale, che
> impoverisce spiritualmente il mondo, è allontanato come valore in senso
> proprio, ma trasformato successivamente in valore nuovo tramite la sua
> sublimazione. La scomposizione neoplastica delle forme elementari corrisposte
> alla scoperta che la "nuova ricchezza" dello Spirito non può più essere
> cercata al di fuori della "nuova povertà" sussunta dalla civiltà meccanica; la
> ricomposizione disarticolata di quelle forme elementari sublima l'universo
> meccanico, dimostrando che non si dà più, ormai, forma alcuna di riconquista
> della totalità (dell'essere come dell' arte) che non dipenda dalla
> problematicità della forma stessa.

:::

:::

::: alts

> Namesto tega se dada potaplja v Kaos. Z njegovo reprezentacijo potrjuje
> njegovo realnost, z njegovo ironizacijo postavljajo zahtevo, ki jo razkrivajo
> kot neizpolnjeno: prav ta nadzor nad Brezobličnim, ki so ga De Stijl, vsi
> konstruktivistični tokovi Evrope, in že formalistična estetika 19\. stoletja
> -- od *Sichbarkeita* dalje -- postavili kot novo fronto za vizualne
> komunikacije. Nič presenetljivega torej, če se Anarhija dada in Red De Stijla
> v teoretskem polju srečata in stečeta v reviji *Mécano* ter v operativnem
> polju v razdelavi instrumentov nove sintakse.

::: {lang=it}

> Dada affonda invece nel Caos. Rappresentandolo ne conferma la realtà,
> ironizzando su di esso pone un'esigenza di cui denuncia l'inadempimento:
> proprio quel controllo dell'Informe che il De Stijl, tutte le correnti
> costruttive europee, e già nell''800 le estetiche formaliste -- della
> *Sichtbarkeit* in poi -- avevano posto come nuova frontiera per le
> comunicazioni visive. Nessuna meraviglia, quindi, che l'Anarchia di Dada e
> l'Ordine di De Stijl si incontrino e confluiscano nella rivista *Mécano* in
> sede teorica, e nell'elaborazione degli strumenti di una nuova sintassi in
> sede operativa.

:::

:::

::: alts

> Zgodovinske avantgarde Kaos in Red razglasijo kot "vrednoti" v pravem pomenu
> novega mesta kapitala.

::: {lang=it}

> Caos e Ordine sono sanciti dalle avanguardie storiche, come i "valori" in
> senso proprio della nuova città del capitale.

:::

:::

::: alts

> Jasno, Kaos je danost in Red je cilj. Toda Forma se od zdaj naprej ne išče več
> onkraj Kaosa, temveč v njegovi notranjosti: Red je ta, ki Kaosu ponudi pomen
> ter ga prevede v vrednoto, v "svobodo". Brezobličje mesta v konsumpciji
> pogojeni z zakoni profita je bilo z izluščenjem vsake progresivne valence iz
> njegove notranjosti odrešeno: Plan, ki je poklican izvršiti te majevtične
> operacije, je instrument, ki ga avantgarde jasno opredelijo ter takoj zatem
> spoznajo, da mu niso kos podati konkretne forme.

::: {lang=it}

> Certo, il Caos è un dato, e l'Ordine è un obiettivo. Ma la Forma da ora in poi
> non va cercata al di là del Caos, bensì al suo interno: è l'Ordine che offre
> significato al Caos e lo traduce in valore, in "libertà". L'informe della
> città a consumi condizionati dalle leggi del profitto, va riscattato estraendo
> dal suo interno tutte le valenze progressive: il Piano, che è chiamato a
> compiere tale operazione maieutica, è lo strumento che le avanguardie
> individuano con chiarezza, scoprendo subito dopo di non essere in grado di
> dare ad esso forma concreta.

:::

:::

::: alts

> Na tej točki lahko arhitektura vstopi v polje ter vpije in preseže vse zahteve
> zgodovinskih avantgard: tako, da jih postavi v krizo, saj je edina zmožna v
> celoti podati realne odgovore na potrebe, ki jih postavijo kubizem, futurizem,
> dada, De Stijl, vsi konstruktivizmi ter produktivizmi.

::: {lang=it}

> È a questo punto che l'architettura può entrare in campo assorbendo e
> superando tutte le istanze delle avanguardie storiche: mettendole in crisi,
> anche, dato che è essa l'unica in grado di dare risposte reali alle esigenze
> poste dal Cubismo, dal Futurismo, da Dada, dal De Stijl, da tutti i
> Costruttivismi e Produttivismi.

:::

:::

In to vlogo pripisuje zgodovinskim avantgardam *v celoti*, brez bistvenega
razlikovanja med njimi, pravi celo, da je njihova številčnost odraz značaja
industrijske produkcije nenehne tehnične revolucije. Avantgarde posredujejo
zakone produkcije v sfero doživljanja: predlagajo model vedênja, kjer je
velemestni *šok* ponotranjen in ga posredujejo kot neizogibno, torej nujno,
naravno in univerzalno stanje bivanja. In ker tolmačijo zgolj nekaj nujnega,
lahko sprejmejo začasno nepriljubljenost in problematičnost do ostale umetnosti:
na tem sploh utemeljujejo vrednost svojega početja. Spet pa, kot sta ti dve
"poti" vzpostavljeni že v razsvetljenstvu, iz tega nastajajo "nova nerazrešljiva
protislovja": antitetična življenje in umetnost (kar se nas tiče: velemesto in
arhitektura) morata poiskati ali instrumente posredovanja med njima ali način
prehajanja enega v drugo. Na tej točki je obrobno predlagan kriterij
koherentnosti celotnega modernega gibanja -- od Piranesija dalje -- kot projekt
modeliranja "buržoaznega človeka" kot idealnega tipa: tako *Campomarzio* kot
Picassov *Violon* sta "projekta", vsak na svojem področju, ki s kritičnimi
instrumenti formalne razdelave razkrivata globoko resnico in to, kar bi se še
lahko zdelo partikularno, napravita univerzalno. Toda z uvedbo in kodifikacijo
*readymade* objektov kot sredstev komunikacije je jasno, da je realnost zdaj
samozadostna, da ne potrebuje reprezentacije, ter da domnevno slikarjevo
gospostvo nad formo ni drugega kot zakrivanje, da je forma zdaj tista, ki
prevladuje nad slikarjem. S tem da je zdaj kot "formo", dodaja, treba razumeti
"logiko subjektivnih reakcij na objektivni univerzum produkcije".
[@tafuri1969peruna, 54] Kubizem poskuša opredeliti zakone teh reakcij, želi
realizirati "projekt" vedênja, toda na notranje protisloven način, ki sicer
izhaja iz subjekta a se zaključi z njegovim popolnim zavračanjem: kot "projekt"
naslavlja občinstvo, mu narekuje aktivacijo, a hkrati namerava dokazati nujni in
univerzalni značaj "nove narave" kapitala ter sovpadanje nujnosti in svobode v
njej. Avantgardni projekti vedênja, ki težijo k aktivni umestitvi občinstva in k
preseganju pasivnosti in apatičnosti flâneurja, ki ga poraja velemestno stanje,
so ravno v točki kjer ne sprejemajo negativnosti velemesta pozitiven predlog k
njegovemu napredku (k napredku organskosti cikla
produkcije--distribucije--konsumpcije). Ne neposredno, kot njihov objekt, temveč
kot referenčna vrednota, ki se nenehno predpostavlja. In "pozitiven predlog"
avantgard bo izpostavljena potreba po planu -- De Stijl nasproti kaosu postavi
princip forme, dada to potrebo izpostavlja *via negativa*, z ironizacijo kaosa
--, ki jo avantgarde lahko opredelijo, a mu niso kos podati ustrezne forme. Na
tej točki lahko vstopi arhitektura in na "neizpolnjeno potrebo", ki so jo
prepoznale avantgarde, odgovori z Bauhausom in *designom*:

::: alts

> Bauhaus, kot komora za dekantacijo avantgard, je določil to zgodovinsko
> nalogo: izbrati vse prispevke avantgard ter jih postaviti v preizkus pred
> potrebe realnosti industrijske produkcije. *Design*, metoda organizacije
> produkcije še prej kot metoda konfiguracije objektov, osmisli utopične
> ostanke, ki so vpisani v poetiko avantgard. Zdaj se ideologija ne nalaga na
> operacije -- konkretne ker so povezane z realnimi produkcijskimi cikli --,
> ampak je notranja samim operacijam.

::: {lang=it}

> Il Bauhaus, come camera di decantazione delle avanguardie, ha appunto questo
> compito storico: quello di selezionare tutti gli apporti delle avanguardie,
> mettendoli alla prova di fronte alle esigenze della realtà della produzione
> industriale. Il *design*, metodo di organizzazione della produzione prima
> ancora che metodo di configurazione di oggetti, fa ragione dei residui
> utopistici insiti nelle poetiche delle avanguardie. L'ideologia, ora, non si
> sovrappone alle operazioni -- concrete perché connesse ai reali cicli di
> produzione -- ma è interna alle operazioni stesse.

:::

:::

::: alts

> Prav tako *design*, kljub svojemu realizmu, prinaša neizpolnjene zahteve, in
> -- v zagonu, ki ga je podal organizaciji podjetij in produkcije -- vsebuje
> margine utopije (toda, zdaj gre za utopijo, ki je funkcionalna za cilje
> nameravane reorganizacije produkcije). Plan, ki ga opredelijo vodilna
> arhitekturna gibanja -- termin avantgarde ni več ustrezen -- od formulacije
> *Plan Voisin* Le Corbusierja (1925) in od stabilizacije Bauhausa (okoli 1921)
> ter dalje, vsebuje to protislovje: izhajajoč iz gradbenega sektorja
> arhitekturna kultura odkriva, da je izpolnitev zastavljenih ciljev možna le s
> povezovanjem tega sektorja z reorganizacijo mesta; toda to pomeni, da tako kot
> so jih izpostavljene zahteve zgodovinskih avantgard napotile v sektor
> vizualnih komunikacij, ki je še bolj neposredno vključen v ekonomske procese
> -- arhitektura in *design* --, tako se je planiranje, ki so ga razglašale
> arhitekturne in urbanistične teorije, nanašalo na nekaj izven sebe: na splošno
> prestrukturiranje produkcije in konsumpcije; z drugimi besedami na *Plan
> kapitala*. V tem smislu arhitektura -- z izhajanjem iz same sebe -- posreduje
> realizem in utopijo. Utopija je v trdovratno vztrajnem prikrivanju, da je
> ideologija planiranja v gradbeništvu lahko uresničena le, če pokaže, da pravi
> Plan lahko privzame formo onkraj nje; celo, da bosta enkrat, ko vstopi v
> horizont splošne reorganizacije produkcije, arhitektura in urbanizem objekta,
> ne subjekta Plana.

::: {lang=it}

> Anche il *design*, malgrado il suo realismo, pone esigenze insoddisfatte, e --
> nello scatto che impone all'organizzazione delle imprese e all'organizzazione
> della produzione -- contiene un margine di utopia (ma si tratta, ora, di
> un'utopia funzionale agli obiettivi di riorganizzazione della produzione, che
> si intende raggiungere). Il Piano individuato dai movimenti architettonici di
> punta -- il termine avanguardia non è ora più adeguato -- dalla formulazione
> del *Plan Voisin* di Le Corbusier (1925) e dalla stabilizzazione del Bauhaus
> (1921 circa) in poi, contiene questa contraddizione: partendo dal settore
> della produzione edilizia la cultura architettonica scopre che è solo legando
> quel settore alla riorganizzazione della città che gli obiettivi prefissati
> possono trovare soddisfazione; ma ciò equivale a dire che come le esigenze
> denunciate dalle avanguardie storiche rimandavano al settore delle
> comunicazioni visive più direttamente inserito nei processi economici --
> l'architettura e il *design* -- così la pianificazione enunciata dalle teorie
> architettoniche e urbanistiche rimanda ad altro da sé: ad una ristrutturazione
> della produzione e del consumo in generale; in altre parole ad un *Piano del
> capitale*. In tal senso l'architettura media -- partendo da se stessa --
> realismo ed utopia. L'utopia è nell'ostinarsi a nascondere che l'ideologia
> della pianificazione può realizzarsi nella produzione edilizia solo indicando
> che è al di là di essa che il vero Piano può prendere forma; anzi, che una
> volta entrato nell'orizzonte della riorganizzazione della produzione in
> generale, l'architettura e l'urbanistica saranno oggetti, e non soggetti, del
> Piano.

:::

:::

Toda *design* prav tako postavlja neizpolnjene zahteve in torej vsebuje
vsaj margine utopije, toda ta je zdaj funkcionalna za cilje
reorganizacije produkcije. In zastavljeni cilji tega *plana*, ugotovi
arhitekturna kultura, so uresničljivi le ob povezovanju gradbenega
sektorja s splošnejšo reorganizacijo mest. Torej gre za stopnjevanje
"dialektike avantgarde": tako kot so zgodovinske avantgarde razglasile
zahteve, ki so se nanašale na sektor vizualnih komunikacij, ki je
najbolj neposredno umeščen v ekonomske procese (arhitektura in
*design*). Tako se mora planiranje, ki ga razglašata arhitekturne in
urbanistične teorije, nanašati še bolj neposredno na nekaj izven sebe:
na *plan kapitala*. Utopija arhitekture je v zaupanju, da bosta
arhitektura in urbanizem z vstopom v "horizont splošne reorganizacije
produkcije" subjekt, ne objekt tega plana. Teh posledic ni bila
pripravljena sprejeti, svojo nalogo razume politično: arhitektura -- ki
tu, pravi Tafuri, že označuje planiranje in plansko reorganizacijo mesta
-- rajši kot revolucija. Toda s tem je arhitektura že na voljo
oblikovanju po podobi kapitala. Ravno v politično najbolj angažiranih
krogih se metoda projektiranja prilagodi idealizirani strukturi tekočega
traku: posamezna razrešitev delcev tekočega traku (standardnih
elementov) in njihova formalna raztopitev v montažo, s tem pa tudi
revolucija estetske izkušnje, to je premik kritične pozornosti od
objekta k *procesu*. [@tafuri1969peruna, 58]

<!-- TODO: to je narobe/nejasno -->

::: alts

> Arhitekturna kultura med 1920 in 1930 ni bila pripravljena sprejeti teh
> posledic. Kar ji je jasno, je njena lastna "politična" naloga. Arhitektura --
> beri: načrtovanje in planska reorganizacija gradbeništva ter mesta kot
> produktivnega organizma -- rajši kot Revolucija: Le Corbusier jasno razglasi
> ti dve alternativi, ki implicitno krožita v spisih tako Mondriana kot
> Gropiusa.

::: {lang=it}

> La cultura architettonica fra il '20 e il '30 non è pronta ad accettare tali
> conseguenze. Ciò che ha chiaro è il proprio compito "politico". L'architettura
> -- leggi: la programmazione e la riorganizzazione pianificata della produzione
> edilizia e della città come organismo produttivo -- piuttosto che la
> Rivoluzione: Le Corbusier enuncia chiaramente questa alternativa, che circola
> implicitamente negli scritti di un Mondrian come di un Gropius.

:::

:::

::: alts

> Medtem pa se z začetkom prav v politično najbolj angažiranih krogih -- v
> *Novembergruppe*, reviji "G", berlinskem *Ring* -- arhitekturna ideologija
> tehnično natančno določi: s tem, ko z lucidno objektivnostjo sprejme vse
> sklepe glede "smrti umetnosti" ter čisto "tehnične" funkcije intelektualca, ki
> jo apokaliptično razglasijo avantgarde, srednjeevropski *Neue Sachlickheit*
> prilagodi taisto metodo projektiranja idealizirani strukturi tekočega traku.
> Podobe in metode industrijskega dela vstopijo v organizacijo projekta in se
> odražajo v predlogih konsumpcije objekta.

::: {lang=it}

> Nel frattempo, e proprio partendo dai circoli più politicamente impegnati --
> dal *Novembergruppe*, alla rivista "G", al *Ring* berlinese -- l'ideologia
> architettonica si precisa tecnicamente: accettando con lucida oggettività
> tutte le conclusioni sulla "morte dell'arte" e sulla funzione puramente
> "tecnica" dell'intellettuale enunciate apocalitticamente dalle avanguardie, la
> *Neue Sachlickheit* mitteleuropa, adegua lo stesso metodo di progettazione
> alla struttura, idealizzata, della catena di montaggio. Le figure e i metodi
> del lavoro industriale entrano nell'organizzazione del progetto e si
> riflettono nelle proposte di consumo dell'oggetto.

:::

:::

::: alts

> Od standardiziranega elementa, do celice, do posameznega bloka, do
> *Siedlunga*, do mesta; medvojna arhitekturna kultura z izjemno jasnostjo in
> doslednostjo vzpostavlja ta Tekoči trak. Vsak "delec" traku je sam zase v
> celoti razrešen in teži k izginotju oziroma k formalni razredčitvi v montaži.

::: {lang=it}

> Dall'elemento standardizzato, alla cellula, al blocco singolo, alla
> *Siedlung*, alla città; la cultura architettonica fra le due guerre imposta
> con eccezionale chiarezza e coerenza questa Catena di montaggio. Ogni "pezzo"
> della catena è in sé compiutamente risolto e tende a sparire o meglio a
> diluirsi formalmente nel montaggio.

:::

:::

::: alts

> Iz vsega tega celo izhaja revolucioniranje same estetske izkušnje. Zdaj niso
> več *objekti* ti, ki se predstavljajo presoji, ampak *proces*, ki naj se živi
> in uporablja kot tak. Uporabnik, pozvan, da dopolni "odprte" prostore Miesa
> van der Rohe ali Gropusa, je osrednji element tega procesa. Arhitektura, s
> pozivanjem občinstva, da sodeluje v projektiranju, saj nove forme niso več
> absolutno individualistične, ampak predlogi organizacije kolektivnega
> življenja -- Gropiusova *integrirana arhitektura* --, doseže preskok v
> ideologiji občinstva. Morrisove sanje romantičnega socializma -- umetnost vseh
> za vse -- privzamejo ideološko formo znotraj železnih zakonov profitne
> mehanike. Tudi s tega vidika je mesto končna točka soočenja za preveritev
> teoretskih hipotez.

::: {lang=it}

> Anzi, da tutto ciò risulta rivoluzionata la stessa esperienza estetica. Non
> più *oggetti* si presentano ora al giudizio, ma un *processo*, da vivere e
> fruite in quanto tale. Il fruitore, chiamato a completare gli "aperti" spazi
> di Mies van der Rohe o di Gropius è l'elemento centrale di tale processo.
> L'architettura, chiamando il pubblico a compartecipare alla progettazione,
> dato che le nuove forme non sono più assoluti individualistici ma proposte di
> organizzazione della vita collettiva -- l'*architettura integrata* di Gropius
> --, fa compiere all'ideologia del pubblico un salto in avanti. Il sogno del
> socialismo romantico di Morris -- un'arte fatta da tutti per tutti -- prende
> forma ideologica all'interno delle ferree leggi della meccanica del profitto.
> Anche sotto questo aspetto è la città il termine ultimo di confronto per la
> verifica dell'ipotesi teorica.

:::

:::

> ***

## ["Radikalna" arhitektura in mesto]

::: alts

> Hilberseimer piše: "Velemestna arhitektura je bistveno odvisna od razrešitve
> dveh faktorjev: posamezne celice prostora in celotnega mestnega organizma.
> Prostor kot sestavni del hiše, zajete v uličnem bloku, (hišo) določa v njeni
> pojavni obliki, postane oblikovalni faktor mestne naprave, kar je pravi cilj
> arhitekture. Obratno bo konstruktivno oblikovanje mestnega načrta dobilo
> bistven vpliv na načrtovanje stanovanja in hiše."[^17]

::: {lang=it}

> "L'architettura della grande città -- scrive Hilberseimer[17], -- dipende
> essenzialmente dalla soluzione data a due fattori: la cellula elementare e il
> complesso dell'organismo urbano. Il singolo vano come elemento costitutivo
> dell'abitazione ne determinerà l'aspetto, e poiché le abitazioni formano a
> loro volta gli isolati, il vano diverrà un fattore della configurazione
> urbana, ciò che rappresenta il vero scopo dell'architettura; reciprocamente la
> struttura planimetrica della città avrà una sostanziale influenza sulla
> progettazione dell'abitazione e del va

:::

:::

<!--[preveri original]-->

[^17]: Ludwig Hilberseimer, *Großstadtarchitektur*, Jul. Hoffmann Verlag,
    Stuttgart, 1927\. Ital. prev. v: G. Grassi, *Introduzione* all'ed. it. di
    *Entfaltung einer Planungsidee*, Ullstein Bauwelt Fundamente, Berlin, 1963
    (*Un'idea di piano*, Marsilio, Padova, 1967, str. 12--13).

<!--

[^17]: Ludwig Hilberseimer, *Großstadtarchitektur*, Jul. Hoffmann Verlag,
    Stuttgart, 1927\. Trad. in: G. Grassi, *Introduzione* all'ed. it. di
    *Entfaltung einer Planungsidee*, Ullstein Bauwelt Fundamente, Berlin, 1963
    (*Un'idea di piano*, Marsilio, Padova, 1967, pp. 12--13).

-->

::: alts

> Veliko mesto je tako resnična in prava enotnost. Če beremo avtorja onkraj
> njegovih lastnih namenov, lahko njegove trditve drugače prevedemo tako:
> celotno moderno mesto je v svoji strukturi postalo ogromen "družben stroj".
> Hilberseimer izbere -- kot skoraj vsi nemški teoretiki od 20\. do 30\. let
> 20\. stoletja -- ta vidik urbane ekonomije, da bi njegove sestavine izolirano
> analiziral in ločeno razrešil. Kar piše o razmerjih med celico in urbanim
> organizmom je torej, zaradi jasnosti razlage in bistva na katero so problemi
> reducirani, za zgled. Celica ni le prvi element neprekinjene produkcijske
> verige, ki se v mestu zaključi, ampak tudi element, ki pogojuje dinamiko
> gradbene agregacije. Njena vrednost kot *tip* omogoča njeno analizo in njeno
> rešitev v abstrakciji: v tem smislu gradbena celica predstavlja temeljno
> strukturo produkcijskega programa iz katerega je vsaka nadaljnja tipološka
> sestavina izključena. Gradbena enota zdaj ni več "objekt". Je samo kraj kjer
> osnovna montaža posameznih celic privzame fizično formo. Kot elementi, ki jih
> lahko reproduciramo v neskončnost, konceptualno utelešajo primarne strukture
> produkcijske verige, ki presega antična koncepta "kraja" ali "prostora".
> Skladno z lastnimi predpostavkami Hilberseimer kot drugi termin svojega
> teorema postavi celotni mestni organizem: uskladitev celice določa koordinate
> projektiranja skupnega urbanizma; struktura mesta se bo lahko deformirala,
> narekovala zakone montaže, tipologijo celice.[^18]

::: {lang=it}

> La grande città è quindi una vera e propria unità. Leggendo l'autore al di là
> delle sue stesse intenzioni possiamo tradurre le sue affermazioni in
> quest'altra: è l'intera città moderna che, nella sua struttura, diviene
> un'enorme "macchina sociale". Hilberseimer seleziona -- come quasi tutti i
> teorici tedeschi degli anni '20--'30 -- quest'ultimo aspetto dell'economia
> urbana, isolandolo per analizzarne e risolverne separatamente le componenti.
> Quanto egli scrive sulle relazioni fra la cellula e l'organismo urbano è
> quindi esemplare, per la lucidità dell'esposizione e l'essenzialità cui i
> problemi vengono ridotti. La cellula non è solo l'elemento primo della
> continua catena di produzione che si conclude nella città, ma anche l'elemento
> condizionante la dinamica delle aggregazioni edilizie. Il suo valore di *tipo*
> permette una sua analisi ed una sua soluzione in astratto: la cellula
> edilizia, in tale accezione, rappresenta la struttura di base di un programma
> produttivo da cui è esclusa ogni ulteriore componente tipologica. L'unità
> edilizia non è più, ora, un "oggetto". È solo il luogo in cui assume forma
> fisica un montaggio elementare delle singole cellule. In quanto elementi
> riproducibili all'infinito, queste incarnano concettualmente le strutture
> prime di una catena di produzione che prescinde dall'antico concetto di
> "luogo" o di "spazio". Coerentemente ai propri assunti, Hilberseimer pone come
> secondo termine del suo teorema l'intero organismo cittadino: la conformazione
> della cellula predispone le coordinate di progettazione dell'insieme urbano;
> la struttura della città potrà deformare, dettandone le leggi di montaggio, la
> tipologia della cellula[18].

:::

:::

[^18]: Od tod shema "vertikalnega mesta", ki se, po Grassi (*nav. d.*, str. 10),
    postavlja kot teoretska alternativa "mestu za tri milijone stanovalcev", ki
    jo je predstavil Le Corbusier leta 1922 v Salon d'Automne. Velja še
    opozoriti da -- kljub distancirani strogosti Hilberseimerja, ni slučajno
    član *Novembergruppe* v letu 1929 in vseh "radikalnih" intelektualnih skupin
    kasneje -- ga bo samokritika po selitvi v ZDA približala s komunitarnimi in
    naturalističnimi miti, ki ne bodo med zadnjimi ideološkimi sestavinami New
    Deala.

<!--

[^18]: Da ciò lo schema di "città verticale" che, secondo il Grassi, (*op.
    cit.*, p. 10), si pone come alternativa teorica della "città per tre milioni
    di abitanti" presentata da Le Corbusier nel 1922 al Salon d'Automne. Va
    ancora notato che -- malgrado il distaccato rigore di Hilberseimer, non
    casualmente membro del *Novembergruppe* nel '19, e di tutti i gruppi
    intellettuali "radicali" poi -- l'autocritica compiuta poco dopo il
    trasferimento negli U.S.A. lo avvicinerà ai miti comunitari e naturalistici
    che non saranno fra gli ultimi ingredienti ideologici del New Deal.

-->

::: alts

> V železnem členjenju produkcijskega plana izgine specifična arhitekturna
> dimenzija, vsaj v njenem tradicionalnem smislu. Arhitekturni objekt se, kot
> "izjemen" v razmerju do homogenosti mesta, popolnoma raztopi.

::: {lang=it}

> Nella ferrea articolazione del piano di produzione sparisce la dimensione
> specifica dell'architettura, almeno nella sua accezione tradizionale. In
> quanto "eccezionale" rispetto all'omogeneità della città, l'oggetto
> architettonico si è completamente dissolto.

:::

:::

::: alts

> Zdaj gre za to, piše Hilberseimer, "da se velike množice s potlačenjem
> mnogovrstnosti oblikuje po nekem občem zakonu [...]: obči primer, zakon sta
> slavljena in povzdigovana, izjema je, nasprotno, potisnjena ob stran, niansa
> izbrisana, vlada mera, ki prisili kaos, da postane oblika: logična, nedvoumna
> matematična oblika."[^19]

::: {lang=it}

> Dovendo "plasmare grandi masse secondo una legge generale, dominando la
> molteplicità -- scrive Hilberseimer[19] -- ... il caso generale, la legge,
> vengono esaltati e messi in evidenza, mentre l'eccezione viene messa da parte,
> la sfumatura si cancella, regna la misura, che costringe il caos a diventare
> forma, forma logica, univoca, matemati

:::

:::

<!--[preveri original]-->

[^19]: L. Hilberseimer. *nav. d.*, str. 21.

<!--

[^19]: L. Hilberseimer. *op. cit.*, p. 21.

-->

::: alts

> In še enkrat: "potreba, da se heterogeno in pogosto ogromno materialno maso
> oblikuje po oblikovnem zakonu, ki velja za vsak element enako, zahteva
> redukcijo arhitektonske forme na najpičlejše, najnujnejše, najobčejše:
> omejitev na geometrično kubično formo, ki predstavlja temeljne elemente vsake
> arhitekture."[^20]

::: {lang=it}

> Ed ancora: "l'esigenza di plasmare una massa eterogenea e spesso gigantesca di
> materiali secondo una legge formale ugualmente valida per ogni elemento,
> comporta una riduzione della forma architettonica alla sua esigenza più
> sobria, più necessaria, più generale: una riduzione cioè alle forme
> geometriche cubiche, che rappresentano gli elementi fondamentali di ogni
> architettura"[20].

:::

:::

<!--[preveri original]-->

[^20]: Prav tam.

::: alts

> Tu ne gre za puristični "manifest", temveč za logično izpeljavo neoporečnih
> posledic iz hipotez, ki vztrajno ostajajo znotraj laboratorijskih
> konceptualnih razdelav. Ker ne ponuja "modelov" za projektiranje, temveč
> vzpostavlja na najbolj abstraktni možni ravni, saj je ta najbolj splošna,
> koordinate in razsežnosti samega projektiranja, Hilberseimer razkriva, bolj
> kot v istih letih to počno Gropius, Mies ali Bruno Taut, h katerim novim
> nalogam faza kapitalistične reorganizacije Evrope poziva arhitekte.

::: {lang=it}

> Non è, questo, un "manifesto" purista, bensì la logica deduzione di
> conseguenze ineccepibili da ipotesi che rimangono ostinatamente all'interno di
> un'elaborazione concettuale da laboratorio. Non offrendo "modelli" per la
> progettazione, ma impostando al livello più astratto possibile, perché più
> generale, le coordinate e le dimensioni della progettazione stessa,
> Hilberseimer rivela, più di quanto non facciano negli stessi anni Gropius,
> Mies o Bruno Taut, quali sono i compiti nuovi cui la fase di riorganizzazione
> capitalistica dell'Europa chiama gli architetti.

:::

:::

In s to integracijo projektiranja v termine tekočega traku se začne epizoda
najnaprednejših upoštevanih arhitekturnih predlogov: medvojni reformizem
evropske stanovanjske arhitekture. Njihov najbolj trezen predstavnik je
Hilberseimer, ki se zaveda realnosti velemesta: v njegovih spisih lahko
prepoznamo, pravi Tafuri, priznanje, da je velemesto postalo ogromen "družben
stroj", in zastavitev problema velemestne arhitekture, ki mora predvsem --
izolirano in v abstrakciji -- razrešiti probleme posameznih prostorskih členov,
ki si sledijo od celice (sobe) do urbanega organizma v celoti (mesta). Gre za
razrešitev (četudi s premestitvijo) problematičnega odnosa med arhitekturnim
objektom in urbano strukturo, ki spremlja arhitekturno kulturo od
razsvetljenstva dalje. S takšno zastavitvijo se sicer specifična arhitekturna
dimenzija (vsaj v tradicionalnem smislu) popolnoma raztopi v mestu, omogoča pa
pojmovanje mesta kot enotnega objekta in torej ga lahko arhitekturno pojmuje kot
sklenjeno polje poseganja. Hilberseimerjeva velemestna arhitektura zahteva
oblikovanje občih zakonov, ki lahko strukturirajo posamezne člene in odnose med
njimi, to pa zahteva oblikovanje enotnih oblikovnih zakonov, "redukcijo
arhitektonske forme na najpičlejše, najnujnejše, najobčejše". [Hilberseimer
nav\. po @tafuri1969peruna, 60] Tu, opozarja Tafuri, ne gre za puristični
manifest, temveč izražanje na najabstraktnejši ravni vse koordinate in
razsežnosti projektiranja, ki sploh ustreza novim nalogam faze kapitalistične
reorganizacije:

::: alts

> Pred posodobitvami produkcijskih tehnik in ekspanziji ter racionalizaciji trga
> je arhitekt kot proizvajalec "objektov" zdaj neustrezna figura. Zdaj ne gre
> več za podajanje forme posameznim elementom mestnega tkiva, niti, kvečjemu,
> preprostim prototipom. Ko je mesto enkrat prepoznano kot realna enotnost
> produkcijskega cikla, je za arhitekta edina primerna naloga *organizator* tega
> cikla. Če pripeljemo to trditev do njene skrajnosti, je dejavnost razdelave
> "organizacijskih modelov", od katere se Hilberseimer ne želi ločiti, edina v
> kateri se popolnoma odraža nujna potreba po taylorizaciji gradbeništva in nova
> naloga tehnika, ki je vanjo vključen na najvišji ravni.

::: {lang=it}

> Di fronte all'aggiornamento delle tecniche di produzione e all'espansione e
> razionalizzazione del mercato, l'architetto produttore di "oggetti" è ormai
> figura inadeguata. Ora non si tratta più di dare forma a singoli elementi del
> tessuto cittadino, né, al limite, a semplici prototipi. Individuando nella
> città l'unità reale del ciclo di produzione, l'unico compito adeguato per
> l'architetto è quello dell'*organizzatore* di quel ciclo. Portando la
> proposizione all'estremo, l'attività di elaboratore di "modelli organizzativi"
> da cui Hilberseimer tiene a non staccarsi, è l'unica in cui si rispecchino
> completamente la necessità della taylorizzazione della produzione edilizia e
> il nuovo compito del tecnico in essa integrato al livello massimo.

:::

:::

::: alts

> Na podlagi te drže se Hilberseimer lahko izogne vpletanju v "krizo objekta",
> ki so jo tesnobno razglašali arhitekti kot sta Loos ali Taut. Za
> Hilberseimerja "objekt" ne vstopi v krizo zgolj zato, ker je ta iz njegovega
> obzorja razmisleka že izginil. Edini imperativ, ki izhaja je tisti, ki ga
> narekujejo zakoni organizacije: v tem je bila pravilno videna največja
> vrednost Hilberseimerjevega prispevka.

::: {lang=it}

> È partendo da tale atteggiamento che Hilberseimer può evitare di involversi
> nella "crisi dell'oggetto" enunciata in modo ansioso da architetti come Loos o
> Taut. Per Hilberseimer l'"oggetto" non entra in crisi solo perché è già
> sparito dal suo orizzonte di considerazioni. L'unico imperativo emergente è
> quello dettato dalle leggi dell'organizzazione: in ciò, correttamente, è stato
> visto il valore maggiore del contributo di Hilberseimer.

:::

:::

::: alts

> Kar pa po drugi strani ni bilo razumljeno, je Hilberseimerjeva popolna odpoved
> branja arhitekture kot instrumenta spoznanja. Celo Mies van der Rohe je glede
> te teme razdvojen. Pri berlinskih hišah na *Afrikanishce Strasse* je
> Hilberseimerjevim stališčem blizu, negotov pa pri postavitvi
> *Weissenhofsiedlung* v Stuttgartu, pri projektu ukrivljenega nebotičnika iz
> stekla in železa, pri spomeniku Karlu Liebknechtu in Rosi Luxemburg, pri
> projektu stanovanj iz leta 1935 in nenazadnje tudi pri hiši Tugendhat, kjer
> raziskuje katere obrobne povrnitve refleksivne drže so arhitekturi še
> dovoljene.

::: {lang=it}

> Ciò che non si è invece colto, è la rinuncia completa fatta dallo stesso
> Hilberseimer a leggere nell'architettura uno strumento di conoscenza. Persino
> Mies van der Rohe, su tale tema, è scisso. Assai vicino alla posizione di
> Hilberseimer nelle case berlinesi sull'*Afrikanische Strasse* ed incerto
> nell'impostazione del *Weissenhofsiedlung* di Stoccarda, nel progetto per il
> grattacielo curvilineo in vetro e ferro, nel monumento a Karl Liebknecht e
> Rosa Luxemburg, nel progetto per abitazioni del '35, ed in fondo anche nella
> casa Tugendhat, egli esplora quali margini di recupero di un atteggiamento
> riflesso siano ancora consentiti all'architettura.

:::

:::

::: alts

> Ne zanima nas slediti notranjim členitvam dialektike, ki se na teh osnovah
> vleče skozi celotno moderno gibanje. Bolj nam gre za poudarek, da dobršen del
> protislovij in ovir s katerimi se ta sooča izhaja iz poskusa ločevanja
> tehničnih predlogov in spoznavnih ciljev.

::: {lang=it}

> Non ci interessa seguire nelle sue interne articolazioni la dialettica che, su
> tali basi, serpeggia nell'intero movimento moderno. Va piuttosto sottolineato
> che buona parte delle contraddizioni e degli ostacoli che questo incontra di
> fronte a sé, nascono dal tentativo di scindere proposizione tecniche e fini
> conoscitivi.

:::

:::

::: alts

> Frankfurt, ki ga je načrtoval Ernst May, Berlin, s katerim je upravljal Martin
> Wagner, Hamburg Fritza Schumacherja, Amsterdam Corja van Eesterena so
> najpomembnejša poglavja v zgodovini modernega urbanizma. Toda poleg oaz reda,
> ki so *Siedlungi* -- resnične in prave *izgrajene utopije* na obrobju urbane
> realnosti, ki jo bolj malo pogojuje --, zgodovinska mesta še naprej kopičijo
> in množijo svoja protislovja. In pretežno so to protislovja, ki se bodo kmalu
> izkazala kot bolj odločilna od instrumentov, ki jih je arhitekturna kultura
> razvila pri poskusu njihovega nadzora.

::: {lang=it}

> La Francoforte pianificata da Ernst May, la Berlino amministrata da Martin
> Wagner, l'Amburgo di Fritz Schumacher, la Amsterdam di Cor van Eesteren, sono
> i capitoli più importanti della storia dell'urbanistica moderna. Ma accanto
> alle oasi di ordine delle *Siedlungen* -- vere e proprie *utopie costruite*,
> ai margini di una realtà urbana da esse ben poco condizionata -- le città
> storiche continuano ad accumulare e moltiplicare le loro contraddizioni. È
> sono in gran parte contraddizioni che ben presto appaiono più vitali degli
> strumenti elaborati dalla cultura architettonica nel tentativo di
> controllarle.

:::

:::

::: alts

> Arhitektura ekspresionizma bo tista, ki bo sprejela dvoumno vitalnost teh
> protislovij. Dunajskim *Hofom* in Poelzigovim ali Mendelsohnovim javnim
> zgradbam so nove metode posegov, ki so jih razvila avantgardna gibanja,
> vsekakor tuje. Toda kljub temu se zdi, da te izkušnje, ki na marsikateri način
> odklanjajo ponujeno umestitev v nova obzorja, ki jih je odkrila umetnosti, ki
> sprejema možnost svoje "tehnične reprodukcije" kot sredstva za vplivanje na
> človeško vedenje, privzemajo kritično vrednoto, in to prav v soočenju z
> razvoji modernih industrijskih mest.

::: {lang=it}

> È l'architettura dell'Espressionismo a recepire l'ambigua vitalità di quelle
> contraddizioni. Le *Hòfe* di Vienna e gli edifici pubblici di Poelzig o di
> Mendelsohn sono certo estranei alle nuove metodologie di intervento elaborate
> dai movimenti di avanguardia. Ma malgrado tali esperienze rifiutino in più
> modi di porsi al di dentro dei nuovi orizzonti scoperti dall'arte che accetta
> la propria "riproducibilità tecnica" come mezzo per incidere sul comportamento
> umano, esse sembrano assumere un valore critico, e proprio nei confronti degli
> sviluppi delle moderne città industriali.

:::

:::

::: alts

> Dela kot so Poelzigov Shauspiltheater v Berlinu, Chilehaus ali druga hamburška
> dela Fritza Hogerja, berlinske tovarne Hansa Hertleina ali Paulusa vsekakor
> niso gradila nove urbane realnosti, temveč komentirala, z vračanjem k
> formalnim dramatizacijam polnim patosa, protislovja delujoče realnosti.

::: {lang=it}

> Opere come lo Schauspiltheater a Berlino di Poelzig, la Chilehaus o le altre
> opere amburghesi di Fritz Hoger, le fabbriche berlinesi di Hans Hertlein o dei
> Paulus, non costruiscono certo una nuova realtà urbana, ma commentano,
> ricorrendo ad esasperazioni formali colme di pathos, le contraddizioni della
> realtà operante.

:::

:::

::: alts

> Dva pola ekspresionizma in *Neue Sachlickheit* sta ponovno simbolizirala
> razcep, ki je imanenten dialektiki evropske kulture.

::: {lang=it}

> I due poli dell'Espressionismo e della *Neue Sachlickheit* simbolizzano di
> nuovo la scissione immanente nella dialettica della cultura europea.

:::

:::

::: alts

> Med uničenjem *objekta* in njegovo nadomestitvijo s *procesom*, ki ga je treba
> doživljati kot takega, kar je povzročila umetniška revolucija *Bauhausa* in
> konstruktivističnih tokov, ter dramatizacijo *objekta*, ki je lasten
> razcepljenim toda dvoumnim eklekticizmom ekspresionistov, učinkovit dialog ni
> bil možen.

::: {lang=it}

> Fra la distruzione dell'*oggetto* e la sostituzione ad esso di un *processo*
> da vivere come tale, operata dalla rivoluzione artistica attuata dal *Bauhaus*
> e dalle correnti costruttivistiche, e l'esasperazione dell'*oggetto* propria
> dei laceranti ma ambigui eclettismi espressionisti, non v'è effettivo dialogo
> critico.

:::

:::

Toda protislovja in omejitve, ki jih pri Hilberseimerjevih in ostalih predlogih
napredne evropske arhitekturne kulture ne smemo spregledati, izhajajo iz poskusa
ločevanja "tehničnih predlogov od spoznavnih ciljev". [@tafuri1969peruna, 61]
Mayev Frankfurt, Wagnerjev Berlin, van Eesterenjev Amsterdam, Schumacherjev
Hamburg vsekakor so "realizirane utopije" na obrobjih urbane realnosti, a izven
teh enklav se protislovja zgodovinskih mest še naprej kopičijo in kmalu
postanejo odločilnejša od instrumentov, ki jih je arhitekturna kultura razvila
pri poskusu njihovega nadzora. Vzporednice z italijansko povojno stanovanjsko
gradnjo so očitne, jasna pa je tudi prisotnost Panzierijeve kritike tehnologije.
Arhitekturi, pojmovani kot čisto tehnični predlog poseganja, so reformistična
gibanja pripisovala *objektivno* naprednost, brez sklicevanja na subjektivne
pogoje. Tehnologija in racionalizacija sta torej pojmovani kot razredno
nezaznamovani, tehnični predlog se lahko poda sam na sebi in preživi sam na
sebi, neodvisno od razrednih razmerij, zgolj s sklicevanjem na lastno
objektivnost. Alternativo temu takrat predstavlja arhitektura ekspresionizma, ki
protislovja vsekakor komentira, do njih zavzema kritično stališče in se pri
svojih objektih vrača k formalni dramatizaciji, a vendar, opozarja Tafuri:

::: alts

> Vendar naj nas videz ne zavede. Gre za dialektiko med intelektualci, ki za
> produkcijski sistem na poti k reorganizaciji svoj ideološki potencial
> reducirajo na instrumentalizacijo naprednih programov, in intelektualci, ki
> izkoriščajo zaostalosti evropskega kapitalizma. Häringov ali Mendelsohnov
> subjektivizem v tem smislu gotovo privzame kritičen pomen v razmerju do
> Hilberseimerjevega ali Gropiusovega taylorizma; toda objektivno gre za kritiko
> opravljeno iz retrogardne pozicije, torej nezmožne, zaradi njene narave,
> predlagati globalnih alternativ.

::: {lang=it}

> Ma non lasciamoci confondere dalle apparenze. Si tratta di una dialettica fra
> intellettuali che riducono il proprio potenziale ideologico alla
> strumentazione di programmi avanzati per un sistema produttivo in via di
> riorganizzazione, e intellettuali che lavorano sfruttando le arretratezze del
> capitalismo europeo. Il soggettivismo di Häring o Mendelsohn, in tal senso,
> assume certo un significato critico rispetto al taylorismo di Hilberseimer o
> di Gropius; ma oggettivamente si tratta di una critica fatta da posizioni di
> retroguardia, incapaci quindi, per loro natura, di proporre alternative
> globali.

:::

:::

::: alts

> Mendelsohnova samoreklamna arhitektura je stvaritev prepričljivih "monumentov"
> v službi trgovskega kapitala; Häringov intimizem nagovarja poznoromantične
> tendence nemške buržoazije. A vendar, tisti, ki dialektiko arhitekture 20\.
> stoletja predstavljajo kot enoten cikel niso povsem v zmoti, četudi je njihovo
> stališče popolnoma znotraj tega cikla.

::: {lang=it}

> L'architettura autopubblicitaria di Mendelsohn è creazione di "monumenti"
> persuasivi al servizio del capitale commerciale; l'intimismo di Häring fa leva
> sulle tendenze tardoromantiche della borghesia tedesca. Eppure, chi presenta
> la dialettica dell'architettura del '900 come ciclo unitario non ha tutti i
> torti, anche se il suo punto di vista è tutto interno a quel ciclo.

:::

:::

::: alts

> Zavračanje protislovja, kot predpostavka objektivnosti in racionalizacije
> načrtovanja, razkriva pristranskost njegove trditve prav v trenutku najvišjega
> stika s strukturami politične moči. Srednjeevropska izkušnja
> socialdemokratskih arhitektov ima za svoj pogoj izpolnitev poenotenja upravne
> moči in intelektualnih predlogov: v tem smislu ni slučaj, da May, Wagner ali
> Taut privzemajo politične naloge upravljanja socialdemokratskih mest. Če je
> celotno mesto zdaj to, ki privzame strukturo industrijskega stroja, bodo v
> njem morali najti rešitve za probleme različnih kategorij: primarno med vsemi,
> ki izhajajo iz konflikta med zemljiško rento, ki s svojimi parazitskimi
> mehanizmi blokira ekspanzijo in posodabljanje gradbenega trga ter preprečuje
> njegovo tehnološko revolucioniranje, in potrebo po organizaciji mesta-stroja
> na globalen način, tako da mu podajo stimulativno in prepričevalno vlogo pri
> soočenju s njegovimi lastnimi funkcijami.

::: {lang=it}

> Il rifiuto della contraddizione, come premessa di oggettività e
> razionalizzazione della programmazione, rivela la parzialità del suo assunto
> proprio nel momento di massima tangenza con le strutture del potere politico.
> L'esperienza mitteleuropea degli architetti socialdemocratici ha come propria
> condizione da rispettare l'unificazione di potere amministrativo e proposta
> intellettuale: in tal senso, che May, Wagner o Taut assumano cariche politiche
> nell'amministrazione delle città socialdemocratiche non è casuale. Se è
> l'intera città ad assumere ora la struttura di una macchina industriale, in
> essa dovranno trovare soluzione diverse categorie di problemi: primo fra tutti
> quello derivante dal conflitto fra rendita fondiaria di posizione, che con i
> suoi meccanismi parassitari blocca l'espansione e l'aggiornamento del mercato
> edilizio e ne impedisce il rivoluzionamento tecnologico, e necessità di
> organizzare in modo globale la macchina-città, facendole assumere un ruolo
> stimolante e persuasivo nei confronti delle proprie stesse funzioni.

:::

:::

::: alts

> Arhitekturni predlog, urbani model, ki se na njem členi, njegove ekonomske in
> tehnološke postavke -- javno lastništvo zemlje in sredstev industrializacije
> gradbeništva, ki so dimenzionirani na produkcijskih ciklih, načrtovanih v
> urbanem okolju -- se neločljivo povežejo med seboj. Arhitekturna znanost se v
> celoti vključi v ideologijo plana in same formalne odločitve niso nič drugega
> kot spremenljivke odvisne od nje. Vsa Mayeva dela v Frankfurtu se lahko berejo
> kot izraz te konkretne "politizacije" arhitekture na najvišji ravni.
> Industrializacija gradbišča pripada minimalni enoti produkcije opredeljene v
> Siedlungu, v njegovi notranjosti se primarni element industrijskega cikla
> pritrdi na servisno jedro (*Frankfurtska kuhinja*), dimenzioniranje
> *Siedlungov* in njihova dislokacija v mestu omogočajo mestne politike na
> terenih, s katerimi neposredno upravlja mesto: fleksibilnost formalnega modela
> Siedlunga na tej točki postane element, ki poda kulturni pečat, ki politične
> cilje, ki jih arhitektura v celoti privzame, napravi "realne".

::: {lang=it}

> La proposta architettonica, il modello urbano che su di essa si articola, le
> premesse economiche e tecnologiche che essa sottende -- proprietà pubblica del
> suolo ed impianti di industrializzazione edilizia dimensionati sui cicli di
> produzione programmati nell'ambito urbano -- si connettono indissolubilmente
> fra loro. La scienza architettonica si integra totalmente nell'ideologia del
> piano, e le stesse scelte formali non sono che variabili dipendenti da essa.
> Tutta l'opera di May a Francoforte può essere letta come espressione al
> massimo livello di tale concreta "politicizzazione" dell'architettura.
> L'industrializzazione del cantiere aderisce all'unità minima di produzione
> individuata nella Siedlung, al suo interno l'elemento primario del ciclo
> industriale si impernia sul nucleo dei servizi (la *Frankfurter Kuche*), il
> dimensionamento delle *Siedlungen* e la loro dislocazione nella città sono
> permessi dalla politica comunale sui terreni direttamente gestita dal comune:
> la flessibilità del modello formale della Siedlung diviene, a questo punto,
> elemento che dà il suggello culturale, che rende "reale" gli obiettivi
> politici assunti in pieno dall'architettura.

:::

:::

::: alts

> Nacistična propaganda bo za Frankfurtske četrti govorila o *izgrajenem
> socializmu*: mi jih moramo brati kot realizirana socialdemokracija. Toda
> bodimo pozorni: sovpadanje politične in intelektualne oblasti ima nalogo
> čistega posredovanja med bazo in nadzidavo. Kar se jasno odraža v sami
> strukturi mesta: zaprta ekonomija Siedlunga se zrcali v razdrobitvi posega, ki
> protislovja v mestu, ki jih ne gre nadzorovati in prestrukturirati v organski
> sistem, pušča nedotaknjena.

::: {lang=it}

> La propaganda nazista parlerà di *socialismo costruito* per i quartieri di
> Francoforte: noi li dobbiamo leggere come socialdemocrazia realizzata. Ma si
> noti: la coincidenza di autorità politica e intellettuale ha un compito di
> pura mediazione fra strutture e sovrastrutture. Ciò si riflette chiaramente
> nella struttura stessa della città: l'economia chiusa della Siedlung si
> rispecchia nella frantumazione dell'intervento, che lascia intatte le
> contraddizioni di una città che non viene controllata e ristrutturata in
> quanto organico sistema.

:::

:::

::: alts

> Utopizem srednjeevropske arhitekturne kulture med 1920 in 1930 je prav v tem:
> v zaupljivem razmerju, ki je vzpostavljeno med intelektualci levice,
> naprednimi sektorji kapitala in političnimi upravami. Sektorske rešitve v tem
> razmerju medtem, ko sebe težijo predstavljati kot najvišje posplošeni modeli
> -- politika javne lastnine in razlastitvena politika, tehnološki eksperimetni,
> formalna razdelava tipologije Siedlunga --, razkrivajo svojo omejeno
> učinkovitost, ko jih preverimo z dejstvi.

::: {lang=it}

> L'utopismo della cultura architettonica mitteleuropea fra il '20 e il '30
> consiste proprio in questo: nel rapporto fiduciario istituito fra
> intellettuali di sinistra, settori avanzati del capitale e amministrazioni
> politiche. Le soluzioni di settore, in tale rapporto, mentre tendono a
> presentarsi come modelli altamente generalizzati -- politica demaniale e di
> espropri, sperimentazioni tecnologiche, elaborazione formale della tipologia
> della Siedlung -- rivelano la loro limitata efficienza alla prova dei fatti.

:::

:::

::: alts

> Mayev Frankfurt, kakor Machlerjev in Wagnerjev Berlin, vsekakor teži k
> reprodukciji podjetniškega modela na družbeni ravni, da bi mestu podalo
> "figuro" produkcijskega stroja, da bi realiziralo v urbani strukturi in
> distribucijskem in konsumpcijskem mehanizmu videz splošne proletarizacije.
> (Medrazrednost srednjeevropskih urbanističnih predlogov je cilj, ki se na
> teoretski ravni nenehno predlaga.)

::: {lang=it}

> La Francoforte di May, come la Berlino di Machler e Wagner, tende certo a
> riprodurre a livello sociale il modello aziendale, a far assumere alla città
> la "figura" della macchina produttiva, a realizzare nella struttura urbana e
> nel meccanismo di distribuzione e consumo, l'apparenza della proletarizzazione
> generale. (L'interclassismo delle proposte urbanistiche mitteleutopee è
> l'obbiettivo continuamente proposto a livello teorico).

:::

:::

::: alts

> Toda enotnosti urbane podobe, formalne metafore predlagane "nove sinteze",
> vsem berljiv znak vzhajajočega kolektivnega gospostva nad naravo in nad
> produkcijskimi sredstvi omejenimi na okolje nove "humane" utopije nemški in
> nizozemski intelektualci ne uresničijo. Tesno integrirani v natančne politike
> plana na urbani in regionalni ravni razdelajo modele posplošljivih posegov:
> model Siedlunga to dokazuje. Toda te teoretske konstante v mestu reproducirajo
> razpadajočo formo paleotehničnega tekočega traku: mesto ostaja skupek delov,
> funkcionalno združenih na minimalni ravni, in tudi znotraj enega "delca" --
> delavske četrti -- se poenotenje metod kmalu razkrije kot negotov instrument.

::: {lang=it}

> Ma l'unità della immagine urbana, metafora formale della "nuova sintesi"
> proposta, segno leggibile da tutti dell'esaltante dominio collettivo sulla
> natura e sui mezzi di produzione costretti nell'ambito di una nuova utopia
> "umana", non è realizzata dagli intellettuali tedeschi e olandesi.
> Strettamente integrati in precise politiche di piano a livello urbano e
> regionale, essi elaborano modelli di intervento generalizzabili: il modello
> della Siedlung ne è la prova. Ma tale costante teorica riproduce nella città
> la forma disgregata della catena di montaggio paleotecnica: la città rimane un
> aggregato di parti, unificate funzionalmente al livello minimo, e anche
> all'interno del singolo "pezzo" -- il quartiere operaio -- l'unificazione dei
> metodi si rivela ben presto strumento aleatorio.

:::

:::

::: alts

> Kriza, na specifičnem terenu arhitekture, eksplodira 1930 v berlinskem
> Siemensstadtu. Neverjetno je kako sodobno zgodovinopisje v slavnem berlinskem
> Siedlungu, ki ga je načrtoval Scharoun, še ni prepoznalo zgodovinskega
> vozlišča v katerem je jasen eden hujših prelomov v globini "modernega
> gibanja".

::: {lang=it}

> La crisi, sul terreno specifico dell'architettura, esplode nel 1930, nella
> Siemensstadt di Berlino. È incredibile come la storiografia contemporanea non
> abbia ancora riconosciuto nella famosa Siedlung berlinese pianificata da
> Scharoun, il nodo storico nel quale si evidenzia una delle più gravi fratture
> in seno al "movimento moderno".

:::

:::

::: alts

> Predpostavka metodološke enotnosti *designa* v svojih različnih dimenzijskih
> merilih v Siemensstadtu razkriva lasten utopističen značaj. V podporo urbanemu
> oblikovanju, ki se je morda upravičeno želelo sklicevati na ironične
> deformacije Kleeja, Bartninga, Gropiusa, Scharouna, Häringa, Forbata,
> dokazujejo, da razvodenitev arhitekturnega objekta v formativnem procesu
> celote trči s protislovji samega modernega gibanja. Proti Gropiusu in
> Bartningu, ki ostajata zvesta zamisli *Siedlunga* kot *tekočega traku*, se
> postavljajo aluzivne ironije Scharouna in izzivalen organicizem Häringa. Če
> se, z uporabo dobro znanega Benjaminovega termina, v ideologiji *Siedlunga*
> dovrši "uničenje avre" tradicionalno povezane z arhitekturnim "kosom",
> nasprotno Scharounovi in Häringovi "objekti" težijo k povrnitvi neke "avre",
> četudi pogojene z novimi produkcijskimi načini in novimi formalnimi
> strukturami.

::: {lang=it}

> Il postulato dell'unità metodologica del *design* nelle sue diverse scale
> dimensionali rivela nella Siemensstadt il proprio carattere utopistico. Sul
> supporto di un disegno urbano che si è voluto, forse giustamente, riferire
> alle ironiche deformazioni di un Klee, Bartning, Gropius, Scharoun, Häring,
> Forbat, dimostrano che la diluizione dell'oggetto architettonico nel processo
> formativo dell'insieme cozza contro le contraddizioni dello stesso movimento
> moderno. Contro Gropius e Bartning che rimangono fedeli alla concezione della
> *Siedlung* come *catena di montaggio*, si pongono le allusive ironie di
> Scharoun e l'ostentato organicismo di Häring. Se, per usare il ben noto
> termine di Benjamin, nell'ideologia della *Siedlung* si consuma la
> "distruzione dell'aura" tradizionalmente connessa al "pezzo" architettonico,
> gli "oggetti" di Scharoun e di Häring tendono al contrario al recupero di
> un'"aura", anche se condizionata dai nuovi modi di produzione e da nuove
> strutture formali.

:::

:::

::: alts

> Konec koncev je epizoda Siemensstadt zgolj najbolj odmevna. Če vzamemo primer
> Amsterdama, ki ga je med 1930 in 1940 planiral Cor van Eesteren, ideal
> evropskih konstruktivističnih gibanj, to je oživeti *mesto tendenc*, dokončno
> vstopi v krizo.

::: {lang=it}

> Del resto l'episodio della Siemensstadt è solo il più clamoroso. Qualora si
> accettui il caso dell'Amsterdam pianificata da Cor van Eesteren, fra il '30 e
> il '40 l'ideale dei movimenti costruttivisti europei, quello cioè di dar vita
> ad una *città di tendenza*, entra decisamente in crisi.

:::

:::

Kljub očitni polarnosti predlogov, pa je na eni točki res točno govoriti o tem
obdobju kot "enotnem ciklu". Pogoj srednjeevropske socialdemokratske arhitekture
je poenotenje upravne moči in intelektualnih predlogov in, nadaljuje, zatorej ni
slučaj, da May, Wagner in Taut prevzemajo tudi politične funkcije upravljanja
socialdemokratskih mest. S tem pa se razširi problemsko polje, ki ga projekti
*Siedlungov* še izključujejo: zdaj mora celo mesto privzeti strukturo tekočega
traku in se soočiti s tistimi elementi, ki ekspanzijo in posodabljanje
zemljiškega trga ter industrializacijo in racionalizacijo gradbišč -- vse to so,
opominjamo, pogoji "projekta" -- blokirajo, predvsem z zasebno lastnino zemlje.
Arhitekturni predlog kot urbani model, ki zajema vse ekonomske in tehnološke
cikle mesta ter povezuje vse njihove člene, je že v celoti vključen v ideologijo
plana. Na tej točki je vpeljana tudi formalna fleksibilnost znotraj
*Siedlungov*, da tem političnim ciljem poda "kulturni pečat" in jih napravi
"realne". [@tafuri1969peruna, 63] Predlogi socialdemokratske srednjeevropske
arhitekture temeljijo na zavezništvu med intelektualci levice, naprednimi
sektorji kapitala in lokalnimi upravami in preko reprodukcije modela podjetja na
ravni družbe v sferi distribucije in konsumpcije želijo realizirati videz
splošne proletarizacije. Politični zaključki ideologije medrazrednosti, ki bodo
vsekakor tudi svojevrsten konec modernega gibanja, so dobro poznani in na tem
mestu naslavljajo tudi italijanske povojne medrazredne nacionalne politične in
arhitekturne projekte. Specifično na polju arhitekture pa predpostavke
metodološke enotnosti *designa* v različnih merilih izkažejo protislovnost
"ideologije Siedlunga": na ravni celote zapuščina Bauhausa in konstruktivizma
vsekakor dovršuje "uničenje avre", nasprotno pa povabljeni ekspresionistični
objekti znotraj njih težijo ravno k povrnitvi neke "avre". Sploh pa razviti
instrumenti srednjeevropske arhitekture z njihovo minimalno funkcionalno
povezavo arhitekturnih členov v "tekoči trak" težko ohranjajo svojo aktualnost v
napredujočih integracijskih potrebah kapitalističnega razvoja. Ne le, da
zasledujejo racionalnost po podobi kapitala, zasledujejo -- in tu Tafuri razvije
Panzierijevo kritiko racionalizacije do nove ravni -- zastarelo in idealizirano
idejo racionalizacije:

::: alts

> Negotovost, polifunkcionalnost, mnogovrstnost in dezorganiziranost v vseh
> protislovnih vidikih privzetih v globini modernega mesta kapitala, ostajajo
> zunaj analitične racionalizacije, ki jo zasleduje srednjeevropska arhitektura.

::: {lang=it}

> L'improbabilità, la polifunzionalità, la molteplicità e la disorganicità, in
> tutti gli aspetti contraddittori assunti nel seno della moderna città del
> capitale, rimangono all'esterno della razionalizzazione analitica perseguita
> dall'architettura mitteleuropea.

:::

:::

## [Kriza utopije: Le Corbusier v Alžiru]

::: alts

> Vpiti to mnogovrstnost, posredovati negotovost z gotovostjo načrta,
> nadomestiti organskost in dezorganskost z zaostritvijo dialektike, dokazati,
> da najvišja raven produkcijskega načrtovanja sovpada z najvišjo
> "produktivnostjo duha": pri orisovanju teh ciljev z lucidnostjo, ki na
> področju *progresivne* evropske kulture nima para, se Le Corbusier zaveda
> trojne fronte na katerih se mora moderna arhitektura boriti. Če je arhitektura
> zdaj sinonim za organizacijo produkcije, potem tudi drži, da sta odločilna
> dejavnika cikla, poleg same produkcije, tudi distribucija in konsumpcija.
> *Arhitekt je organizator*, ne oblikovalec objektov: to ni zgolj Le
> Corbusierjev slogan, ampak imperativ, ki povezuje intelektualno prizadevanje
> in *civilisation machiniste*. Kot avantgarda te *civilisation*, mora arhitekt,
> pri predhajanju in določanju planov (četudi samo sektorskih) členiti svoje
> delovanje na tri tire: na *appel aux industriels* in ponujanje tipologije
> proizvodnemu podjetju; na iskanje *oblasti*, ki je sposobna posredovati med
> gradbenim in urbanističnim planiranjem ter programi civilne reorganizacije, na
> politični ravni z ustanovitvijo CIAM; na izrabo členitve forme na njeni
> najvišji ravni, da se občinstvo napravi v aktiven subjekt konsumpcije.

::: {lang=it}

> Assorbire quelle molteplicità, mediare l'improbabile con la certezza del
> piano, compensare organicità e disorganicità acutizzandone la dialettica,
> dimostrare che il massimo livello di programmazione produttiva coincide con il
> massimo di "produttività dello spirito": nel delineare tali obiettivi con una
> lucidità che non ha paragoni nell'ambito della cultura *progressista* europea,
> Le Corbusier è cosciente del triplice fronte su cui l'architettura moderna
> deve combattere. Se architettura è ora sinonimo di organizzazione della
> produzione, è anche vero che fattori determinanti del ciclo, oltre la
> produzione stessa, sono la distribuzione e il consumo. L'*architetto è un
> organizzatore*, non un disegnatore di oggetti: non è uno slogan, questo di Le
> Corbusier, ma l'imperativo che connette iniziativa intellettuale e
> *civilisation machiniste*. In quanto avanguardia di quella *civilisation*,
> l'architetto, nel precederne e determinarne i piani, (e sia pur di settore),
> deve articolare la sua azione su tre binari: l'*appel aux industriels* e la
> tipologia sono le offerte fatte all'impresa produttiva; la ricerca di
> un'*autorità* capace di mediare la pianificazione edilizia e urbanistica con
> programmi di riorganizzazione civile è perseguita a livello politico con
> l'istituzione dei CIAM; l'articolazione della forma al suo massimo livello è
> sfruttata al fine di rendere il pubblico soggetto attivo del consumo.

:::

:::

::: alts

> Natančneje; forma privzame nalogo nenaravni univerzum tehnološke natančnosti
> napraviti *avtentičen in naraven*. In ker ta univerzum teži naravo celovito
> podjarmiti v neprekinjen in zapleten proces preoblikovanja, postane za Le
> Corbusierja celoten antropogeografski potek subjekt na katerem vztraja
> reorganizacija cikla gradbene produkcije.

::: {lang=it}

> Più esattamente; la forma si assume il compito di rendere *autentico e
> naturale* l'universo innaturale della precisazione tecnologica. E poiché
> quell'universo tende ad assoggettare integralmente la natura in un processo
> continuo e coinvolgente di trasformazione, è l'intero passaggio
> antropogeografico a divenire, per Le Corbusier, il soggetto su cui insiste il
> riorganizzato ciclo della produzione edilizia.

:::

:::

::: alts

> Toda Le Corbusier odkrije tudi, da finančna previdnost, podjetniški
> individualizem in vztrajnost arhaičnih mehanizmov rent, kot je zemljiška
> renta, nevarno ovirajo *civilisation*, produktivni izraz in usposobljenost ter
> "humani" donos te ekspanzije.

::: {lang=it}

> Ma Le Corbusier scopre anche che prudenza finanziaria, individualismo
> dell'impresa, permanenza di meccanismi di rendita arcaici come la rendita
> fondiaria, ostacolano pericolosamente *civilisation*, espressione e
> qualificazione produttive, rendimento "umano" di quell'espansione.

:::

:::

::: alts

> Tipološke opredelitve celice *Dom-ino*, tipologija *Immeuble-villa*, *Ville
> pour trois millions d'habitant*, *Plan Voisin* za Pariz: od 1919 do 1929 Le
> Corbusierjev *recherche patiente* opredeli merila in posamezna orodja
> poseganja, eksperimentira z delnimi realizacijami -- vzete kot laboratorijske
> preveritve -- splošnih hipotez, preseže nemške modele "racionalizma", sluti
> pravilno razsežnost znotraj katere je treba umestiti urbani problem.

::: {lang=it}

> Individuazione tipologica della cellula *Dom-ino*, tipologia
> dell'*Immeuble-villa*, *Ville pour trois millions d'habitant*, *Plans Voisin*
> per Parigi: dal 1919 al 1929 la *recherche patiente* di Le Corbusier individua
> scale e strumenti particolari di intervento, sperimenta in realizzazioni
> parziali -- assunte come laboratorio di verifica -- le ipotesi generali,
> supera i modelli del "razionalismo" tedesco, intuendo la corretta dimensione
> cui il problema urbano va posto.

:::

:::

::: alts

> Od 1929 do 1931, s plani za Montevideo, Buenos Aires, São Paulo, Rio in končno
> z izkušnjo plana *Obus* za Alžir Le Corbusier formulira najnaprednejšo
> teoretsko hipotezo modernega urbanizma, ki je tako na ideološki kot na
> formalni ravni še nepresežena.

::: {lang=it}

> Dal '29 al '31, con i piani per Montevideo, per Buenos Aires, per S. Paulo,
> per Rio, e con la finale esperienza del piano *Obus* per Algeri, Le Corbusier
> formula l'ipotesi teorica più elevata dall'urbanistica moderna, ancora
> insuperata sia al livello ideologico che formale.

:::

:::

::: alts

> V nasprotju z realizacijami Tauta, Maya ali Gropiusa, Le Corbusier prekine
> zvezno zaporedje arhitektura-četrt-mesto: urbana struktura kot taka, kot
> fizična in funkcionalna enota, je skladišče nove lestvice vrednot in
> razsežnost v kateri je treba iskati pomene njenih komunikacij je razsežnost
> same krajine.

::: {lang=it}

> Al contrario di quanto realizzato da Taut, May o Gropius, Le Corbusier spezza
> la seguenza continua architettura-quartiere-città: la struttura urbana come
> tale, in quanto unità fisica e funzionale, è depositaria di una nuova scala di
> valori, e la dimensione alla quale va cercato il significato delle sue
> comunicazioni è quella stessa del paesaggio.

:::

:::

::: alts

> V Alžiru so antična kazba, hribi Fort-l'Empereur in obalni zaliv privzeti kot
> grobi materiali za ponovno uporabo, pravi pravcati *ready-made objects* v
> gigantskem merilu, ki jim nova struktura, ki jih pogojuje, z rušitvijo
> prvotnih problemov ponuja prej neobstoječo enotnost. Toda najvišji pogojenosti
> mora odgovarjati najvišja svoboda in fleksibilnost. Ekonomska predpostavka
> celotne operacije je zelo jasna: plan *Obus* se ne omejuje na vprašanje novega
> "statuta območja", ki s preseganjem paleokapitalistične anarhije zemljiške
> akumulacije napravi celotno zemljo na voljo enotni in organski reorganizaciji
> tega, kar bi tako postalo urbani sistem v pravem pomenu.

::: {lang=it}

> Ad Algeri l'antica Casbah, le colline di Fort-l'Empereur, l'insenatura
> costiera, sono assunti come materiali bruti da riutilizzare, veri e propri
> *ready-mades objects* a scala gigantesca, ai quali la nuova struttura che li
> condiziona offre un'unità prima inesistente, sconvolgente i significati
> originari. Ma al massimo di condizionamento deve corrispondere un massimo di
> libertà e flessibilità. La premessa economica dell'intera operazione è
> chiarissima: il piano *Obus* non si limita a chiedere un nuovo "statuto del
> terreno" che, vincendo l'anarchia paleocapitalistica dell'accumulazione
> fondiaria, renda disponibile l'intero suolo alla riorganizzazione unitaria e
> organica di ciò che diviene in tal modo un sistema urbano in senso proprio.

:::

:::

::: alts

> Industrijski objekt ne predpostavlja nobene enoznačne postavitve v prostor. V
> osnovi serijske produkcije je radikalno preseganje vsakega hierarhičnega
> prostora. Tehnološki univerzum ne upošteva nobenega *tukaj* in *tam*; celotno
> človeško okolje -- čisto topološko polje -- je naravni sedež njegovih operacij
> (dobro napovedano s strani kubističnih, futurističnih ali elementarističnih
> raziskav). Popolna razpoložljivost zemlje za reorganizacijo mesta ni več
> dovolj: celoten tridimenzionalen prostor mora zdaj postati razpoložljiv za
> oblikovanje po načrtovani tehnologizaciji. In jasno je, da se mora -- v
> globini mestne enote -- razlikovati dve merili posega: dva cikla produkcije in
> konsumpcije.

::: {lang=it}

> L'oggetto industriale non presuppone alcuna collocazione univoca nello spazio.
> Alla base della produzione in serie è il superamento radicale di ogni spazio
> gerarchico. L'universo tecnologico ignora il *qui* e il *là*; l'intero
> ambiente umano -- puro campo topologico -- è la sede naturale delle sue
> operazioni (ben lo avevano intuito le ricerche cubiste, futuriste o
> elementariste). La piena disponibilità del suolo, nella riorganizzazione della
> città, non è più sufficiente: è l'intero spazio tridimensionale che deve ora
> divenire disponibile per essere informato da una programmata
> tecnologizzazione. Ed è chiaro che si dovranno distinguere -- in seno
> all'unità città -- due scale di intervento: due cicli di produzione e consumo.

:::

:::

::: alts

> Prestrukturiranje celotnega urbanega in krajinskega prostora odgovarja potrebi
> po celostni racionalizaciji organizacije mestnega *stroja*: v tem obsegu
> morajo tehnološke strukture in komunikacijska omrežja biti sposobna
> vzpostaviti enotno "podobo" v kateri antinaturalizem *terreins artificiels*,
> ki se predpostavlja na različnih višinah, ter izjemnost cestnega omrežja --
> avtocesta, ki teče po zadnjem nadstropju kačastega bloka, ki je namenjen
> delavskim stanovanjem -- pridobijo simbolno vrednost. Svoboda stanovanjskih
> blokov Fort-l'Empereur vpije emblematične valence nadrealistične avantgarde;
> ukrivljene zgradbe -- na enak način kot svobodne forme v notranjosti Ville
> Savoye ali ironični *assemblages* podstrešja Bestegui na Champs Elysées -- so
> ogromni objekti, ki posnemajo abstrakten in sublimiran "ples
> protislovij".[^21]

::: {lang=it}

> La ristrutturazione dell'intero spazio urbano e paesistico risponde
> all'esigenza di razionalizzare l'organizzazione complessiva della *macchina*
> cittadina: a questa scala strutture tecnologiche e reti di comunicazione
> debbono essere in grado di costituire una "immagine" unitaria, in cui
> l'antinaturalismo dei *terreins artificiels* predisposti a varie altezze, e
> l'eccezionalità della rete viaria -- l'autostrada che scorre all'ultimo piano
> del blocco serpentinato destinato alle residenze operaie -- acquisti un valore
> simbolico. La libertà dei blocchi residenziali di Fort-l'Empereur assorbono le
> valenze emblematiche dell'avanguardia surrealista; gli edifici ricurvi -- allo
> stesso modo delle forme libere all'interno della villa Savoye o degli ironici
> *assemblages* dell'attico Bestegui agli Champs Elysées -- sono enormi oggetti
> che mimano un'astratta e sublimata "danza delle contraddizioni"[21].

:::

:::

[^21]: Gre za risbe *Poème de l'angle droit* (ed. Verve, Paris, 1955), ki ga je
    Le Corbusier pripisal intelektualnemu popotovanju čez labirint: kot za
    Kleeja, kateremu blizu je grafični okus risb, Red ni totaliteta, ki bi bila
    zunanja človeški aktivnosti, ki ga ustvarja. Bolj kot je *recherche* sintez
    obogatena z negotovostjo spomina, s problematično napetostjo, celo z
    usmeritvami, ki so nasprotne končnim ciljem, bolj je ta dosežena v polnosti
    pristnega *izkustva*. Tudi za Le Corbusierja je absolut forme izpolnitev
    realizacije stalne zmage nad negotovostjo prihodnosti, in sicer s sprejetjem
    problematične drže kot edinega zagotovila kolektivne odrešitve.

<!--

[^21]: Sono i disegni del *Poème de l'angle droit* (ed. Verve, Paris, 1955), che
    spiegano il significato attribuito da Le Corbusier al percorso intellettuale
    compiuto attraverso il labirinto: come per Klee, al cui gusto grafico quei
    disegni sono molto vicini, l'Ordine non è una totalità esterna all'attività
    umana che lo crea. Quanto più la *recherche* della sintesi è arricchita
    dalle incertezze della memoria, dalla tensione problematica, persino da
    direzioni in contrasto con la meta finale, questa viene raggiunta nella
    pienezza di un'*esperienza* autentica. Anche per Le Corbusier l'assoluto
    della forma è compiuta realizzazione di una costante vittoria
    sull'incertezza del futuro, tramite l'assunzione dell'atteggiamento
    problematico come unica garanzia di salvezza collettiva.

-->

::: alts

> Kar izhaja, tudi na ravni urbane strukture, ki je končno razrešena v organsko
> enoto, je pozitivnost protislovij, uskladitev problematičnega in racionalnega
> ter "herojska" kompozicija nasilnih napetosti. Skozi strukturo podob, in *samo
> skozi njo*, se kraljestvo nuje združi s kraljestvom svobode: četudi je prva
> prepoznana v strogosti plana in druga v povrnitvi, v njeno notranjost, višje
> človeške zavesti.

::: {lang=it}

> Anche al livello della struttura urbana, finalmente risolta in organica unità,
> ciò che emerge è la positività delle contraddizioni, la conciliazione del
> problematico e del razionale, la composizione "eroica" di violente tensioni.
> Attraverso la struttura dell'immagine, e *solo attraverso di essa*, il regno
> della necessità si fonde con il regno della libertà: anche se il primo è
> identificato nel rigore del piano e il secondo nel recupero, al suo interno,
> di una più alta conoscenza umana.

:::

:::

::: alts

> Tudi Le Corbusier uporablja tehniko *šoka*: *objects à réaction poètique* so
> zdaj med seboj povezani v organsko dialektiko. Njihovi dinamiki, formalni in
> funkcionalni, je nemogoče ubežati: na vseh ravneh uporabe in branja, Le
> Corbusierjev Alžir vsiljuje popolno soudeležbo občinstva. Toda pozor:
> občinstvo je tu pogojeno s kritično, refleksivno, intelektualno participacijo.
> "Brezbrižno branje" urbanih podob bi dejansko vodilo v prikrito prepričevanje:
> ni rečeno, da Le Corbusier ni predvidel niti takega sekundarnega učinka kot
> nujnega momenta posredne spodbude.[^22]

::: {lang=it}

> Anche Le Corbusier usa la tecnica dello *choc*: gli *objects à réaction
> poètique* sono però ora connessi fra loro in una dialettica organicità. Alla
> loro dinamica, formale e funzionale, è impossibile sfuggire: a tutti i livelli
> di fruizione e lettura, l'Algeri di Le Corbusier impone un coinvolgimento
> totale del pubblico. Ma si noti: il pubblico è qui condizionato ad una
> partecipazione critica, riflessa, intellettuale. Una "lettura disattenta"
> delle immagini urbane condurrebbe infatti ad una persuasione occulta: né è
> detto che Le Corbusier non prevedesse anche tale effetto secondario come
> momento necessario di stimolo indiretto[22].

:::

:::

[^22]: Med številnimi literarnimi pričevanji Le Corbusierja, kjer je
    posredovanje arhitekture kot instrumenta integracije razreda eksplicitno
    postavljeno v prvi plan, je še posebej značilno pričevanje o nizozemski
    tovarni *Van Nelle*. Le Corbusier piše: "L'usine des tabacs *Van Nelle* de
    Rotterdam, création des temps modernes, efface sa signification désespérante
    au mot de 'proletaire'. Cette dérivation du sentiment de proprieté égoiste
    vers un sentiment d'action collective, nous conduit à ce phénomène heureux
    de l'*intervention personnelle* en chaque point de l'entreprise humaine. Le
    travail demeure tel dans sa matérialité, mais l'ésprit l'éclaire. Je le
    répète, tout est dans ce mot: *preuve d'amour*.

    C'est là que par une administration autre, il faut conduire, épurer, et
    amplifier l'événement contemporain; dites-nous ce que nous sommes, à quoi
    nous pouvons servir, pourquoiì nous travaillons. Donnez-nous des plans,
    montrez-nous les plans, expliquez-nous les plans. *Rendez-nous solidaires*
    [...] Si vous nous montrez les plans et nous les expliquez, il n'y aura plus
    ni caste possédente, ni prolétariat sans espoir. Il y aura une societé
    croyante et agissante. A l'heure actuelle des plus strictes
    rationalisations, c'est de conscience qu'il s'agit".

    ["Tobačna tovarna Van Nelle de Rotterdam, stvaritev moderne dobe, zabrisuje
    brezupni pomen besede 'proletarec'. Ta izpeljava sebičnega čuta za lastnino
    nasproti čutu za kolektivno dejavnost nas popelje k srečnemu pojavu osebnega
    posega, ki je vseskozi človekovo dejanje. Delo samo počiva v materialnosti,
    vendar ga razsvetljuje duh. Ponavljam, vse tiči v besedi: dokaz ljubezni.

    Gre za to, da je treba začeti z drugačno delitvijo, enako tudi prečistiti in
    izdelati sodobni doživljaj; povejmo, kaj smo, čemu lahko služimo in zakaj
    delamo. Dajte nam načrte, pokažite nam načrte, razložite nam načrte.
    Napravite nas solidarne [...]. Če nam pokažete načrte in nam jih razložite,
    ne bo več lastniške kaste niti proletariata, ki nima upanja. Bo pa zato
    upajoča in delujoča družba. Ob sedanji uri najstrožjih racionalizacij gre za
    zavest."] Le Corbusier, *Spectacle de la vie moderne*, in: *La ville
    radieuse*, Vincent et Fréal e C., Paris, 1933, str. 177.

<!--

[^22]: Tra le molte testimonianze letterarie di Le Corbusier, in cui
    l'intervento dell'architettura come strumento di integrazione della classe è
    posto esplicitamente in primo piano, quella relativa alla fabbrica olandese
    Van Nelle è particolarmente indicativa. "L'usine des tabacs *Van Nelle* de
    Rotterdam -- scrive Le Corbusier --, création des temps modernes, efface sa
    signification désespérante au mot de 'proletaire'. Cette dérivation du
    sentiment de proprieté égoiste vers un sentiment d'action collective, nous
    conduit à ce phénomène heureux de l'*intervention personnelle* en chaque
    point de l'entreprise humaine. Le travail demeure tel dans sa matérialité,
    mais l'ésprit l'éclaire. Je le répète, tout est dans ce mot: *preuve
    d'amour*.

    C'est là que par une administration autre, il faut conduire, épurer, et
    amplifier l'événement contemporain; dites-nous ce que nous sommes, à quoi
    nous pouvons servir, pourquoiì nous travaillons. Donnez-nous des plans,
    montrez-nous les plans, expliquez-nous les plans. *Rendez-nous solidaires*
    ... Si vous nous montrez les plans et nous les expliquez, il n'y aura plus
    ni caste possédente, ni prolétariat sans espoir. Il y aura une societé
    croyante et agissante. A l'heure actuelle des plus strictes
    rationalisations, c'est de conscience qu'il s'agit". Le Corbusier,
    *Spectacle de la vie moderne*, in: *La ville radieuse*, Vincent et Fréal e
    C., Paris, 1933, p. 177.

-->

::: alts

> "Oddaljiti tesnobo s ponotranjenjem vzrokov": toda, Le Corbusierjev predlog se
> ne omejuje na to. Na ravni minimalne produkcije -- raven posamezne
> stanovanjske celice -- je tema, ki jo je treba obravnavati, povrnitev
> maksimalne fleksibilnosti, nadomestljivosti in možnosti hitre konsumpcije. V
> mrežah velikih struktur, vzpostavljenih s strani prekrivajočih *terreins
> artificiels*, je vstavljanju predhodno oblikovanih stanovanjskih elementov
> zagotovljena največja svoboda. V razmerju do občinstva to pomeni vabilo naj se
> to napravi v aktivnega projektanta mesta: Le Corbusier v demonstracijski skici
> na koncu doseže predvideti možnost vstavljanja ekscentričnih in eklektičnih
> elementov v mrežo fiksnih struktur. "Svoboda" zagotovljena občinstvu se mora
> potisniti tako daleč, da dovoli samemu občinstvu -- proletariatu doma v
> serpentinah, ki se vijejo pred morjem, in buržoaziji na hribih Fort-l'Empereur
> -- razdelavo lastnega "slabega okusa". Arhitektura kot pedagoški akt in
> instrument integracije, torej.

::: {lang=it}

> "Allontanare l'angoscia introiettandone le cause": non si riduce a questo,
> però, la proposta di Le Corbusier. Al livello della produzione minima --
> quella della singola cellula residenziale -- il tema da affrontare è il
> recupero della massima flessibilità, intercambiabilità, possibilità di rapido
> consumo. Nelle maglie delle grandi strutture, costituite dai *terreins
> artificiels* sovrapposti, è concessa la più ampia libertà di inserimento di
> elementi residenziali preformati. Rispetto al pubblico ciò significa invito a
> farsi progettista attivo della città: Le Corbusier, in uno schizzo
> dimostrativo, giunge fino a prevedere la possibilità di inserimento di
> elementi eccentrici ed eclettici nelle maglie delle strutture fisse. La
> "libertà" concessa al pubblico deve spingersi tanto in là da permettere al
> pubblico stesso -- al proletariato nel caso della serpentina che si snoda al
> cospetto del mare e all'alta borghesia sulle colline di Fort-l'Empereur --
> l'esplicazione del suo "cattivo gusto". L'architettura come atto pedagogico e
> strumento di integrazione collettiva, dunque.

:::

:::

::: alts

> Toda v odnosu do industrije ta svoboda privzame še pomembnejše pomene. Le
> Corbusier ne izkrastilizira minimalne produkcijske enote v funkcionalne
> standardne elemente, kakor May v svoji *Frankfurter Kuche*. V merilu
> posameznega objekta je treba upoštevati zahteve po nenehni tehnološki
> revoluciji, po *stylingu*, po hitri konsumpciji, ki jih narekuje aktivni
> kapitalizem v ekspanziji. Stanovanjsko celico, teoretično potrošljivo v
> kratkem času, je mogoče zamenjati ob vsaki spremembi individualnih potreb --
> ob vsaki spremembi potreb, ki jih povzroči prenova stanovanjskih modelov in
> *standards*, ki jih narekuje produkcija.[^23] Pomen projekta postane zelo
> jasen.

::: {lang=it}

> Ma rispetto all'industria quella libertà assume significati ancora maggiori.
> Le Corbusier non cristallizza l'unità produttiva minima in elementi funzionali
> standard, come May nella sua *Frankfurter Kuche*. Alla scala dell'oggetto
> singolo bisogna tener conto delle esigenze della continua rivoluzione
> tecnologica, dello *styling*, del rapido consumo, dettate da un capitalismo
> attivo, in espansione. La cellula residenziale, teoricamente consumabile in
> tempi brevi, può essere sostituita ad ogni mutamento delle esigenze
> individuali -- ad ogni mutamento delle esigenze indotte dal rinnovamento dei
> modelli e degli *standards* residenziali dettati dalla produzione[23]. Il
> significato del progetto diviene chiariss

:::

:::

[^23]: Lahko bi, na podlagi teh premislekov, nasprotovali Banhamovi tezi, ki
    kritizira, s stališča notranjega tehnološkemu razvoju, tipološko statičnost
    mojstrov "modernega gibanja". "In opting for stabilised types or norms,
    architects opted for the pauses when the normal process of technology were
    interrupted, those process of changes and renovation that, as far we can
    see, can only be halted by abandoning technology as we know it today, and
    bringing both research and mass-production to a stop". (R. Banham, *Theory
    and Design in the First Machine Age*, The Architectural Press, London, 1962,
    str. 325). Morda je odveč poudarjati, da vsa arhitekturna
    znanstvena-fantastika, ki se je razmahnila od 60\. let do danes z
    odrešitvijo razsežnosti "podobe" tehnoloških procesov, je -- v primerjavi z
    Le Corbusierjevim planom *Obus* -- nazadnjaška na najbolj obupen način.

<!--

[^23]: Si potrebbe, sulla base di queste considerazioni, controbattere la tesi
    del Banham, che critica da un punto di vista interno allo sviluppo
    tecnologico la staticità tipologica dei maestri del "movimento moderno". "In
    opting for stabilised types or norms, -- egli scrive -- architects opted for
    the pauses when the normal process of technology were interrupted, those
    process of changes and renovation that, as far we can see, can only be
    halted by abandoning technology as we know it today, and bringing both
    research and mass-production to a stop". (R. Banham, *Theory and Design in
    the First Machine Age*, The Architectural Press, London, 1962, p. 325). È
    forse superfluo rilevare che tutta la fantascienza architettonica che ha
    prolificato dal '60 ad oggi riscattando la dimensione "di immagine" dei
    processi tecnologici, è -- rispetto al piano *Obus* di Le Corbusier --
    arretrata nel più sconsolante dei modi.

-->

Vsa ta protislovja bo, nadaljuje Tafuri, razrešil, oziroma vsaj naslovil Le
Corbusier med svojim *recherche patiente* (1919--1929) in izkušnjami predlogov
planov, nazadnje s planom *Obus* (1929--1931). In lotil se jih bo, trdi, z
lucidnostjo, ki na področju *progresivne* evropske kulture nima para. "Arhitekt
kot organizator" zanj ni samo slogan, temveč imperativ, ki povezuje
intelektualno prizadevanje z najvišje opredeljeno idejo družbe, *civilisation
machiniste*. Kot avantgarda te ideje mora členiti svoje delo na tri tire: na eni
ravni iskanje produktivnega zavezništva z industrijo; na drugi iskanje
avtoritete, ki lahko gradbeno in urbanistično planiranje posreduje kot civilno
reorganizacijo; in tretje na ravni interpelacije arhitekturnega občinstva, da se
napravi v aktiven subjekt konsumpcije. Do neke mere posamezno že prepoznani
nujni vzvodi, a za Le Corbusierja ti predhodijo samim določilom plana in raven
na kateri naj bodo sintetizirani je celoten antropogeografski potek družbe.
Projekt na katerem Tafuri zagovarja Le Corbusierjevo "najnaprednejšo teoretsko
hipotezo modernega urbanizma" je plan *Obus* za Alžir, ki obstoječe kazbatsko
tkivo, zaliv in hribovje jemlje za *readymades* v gigantskem merilu in jih
poveže, pravzaprav preseže, z novo enotno urbano strukturo. Le Corbusier prekine
gropiusevsko zaporedje arhitektura-četrt-mesto in predlaga fizično in
funkcionalno enoto, ki odnose vzpostavlja na geografski ravni mesto-teren. Ob
maksimalni pogojenosti (ki jo uveljavlja s formalno zaključenostjo) predvideva
maksimalno fleksibilnost in tako celoten tridimenzionalen prostor urbane
strukture postane razpoložljiv in, kot posplošljiv model, celotna zemlja.
Protislovja, ki smo jim sledili pri zgodovinskih avantgardah, -- napetosti med
"kosom" in strukturo, funkcijo in simbolom, racionalnim in problematičnim -- so
tu pozitivno izrabljena, vzroki tesnobe so dejansko ponotranjeni. Toda plan
*Obus* se ne omejuje le pomiritev arhitekturne zavesti, temveč dejansko
predstavlja tudi napreden predlog v razmerju do kapitalističnega razvoja.
Povrnitev maksimalne fleksibilnosti na raven najmanjše celice -- delavska
stanovanja v serpentinah -- pomeni vabilo občinstvu, da se napravi v aktivnega
projektanta mesta, predvidi celo vstavljanje ekscentričnih in eklektičnih
struktur v mrežo fiksnih struktur: svoboda občinstva se mora zagotoviti do te
mere, da mu omogoča razdelavo lastnega "slabega okusa" in torej maksimalno
integracijo. In v odnosu do industrije je ta svoboda še pomembnejša: Le
Corbusier ne gre, kakor May, do funkcionalnih elementov kuhinje, temveč se
ustavi pri stanovanju in tako upošteva zahtevo po nenehni tehnološki revoluciji
in hitri konsumpciji, ki jo narekuje kapitalizem. Stanovanje je tako teoretično
potrošljivo v hitrem času in mogoče ga je zamenjati ob vsaki spremembi
(individualnih ali industrijskih) potreb. Gre za projekt integracije --
občinstva, arhitekta, industrije -- do skrajnih ravni:

::: alts

> Subjekt urbane reorganizacije je občinstvo, ki je nagovorjeno in napravljeno
> kritično sodelovati v svoji kreativni vlogi: industrijska avantgarda,
> "oblast", potrošniki so, s teoretično homogenimi funkcijami, soudeleženi v
> zagnanem in "povzdignjenem" procesu nenehnega razvoja in preoblikovanja. Od
> produkcijske realnosti, do podob, do uporabe podob, ves urbani stroj potiska
> "družbene" potenciale *civilisation machiniste* do konca skrajnosti njenih
> implicitnih možnosti.

::: {lang=it}

> Il soggetto della riorganizzazione urbana è un pubblico sollecitato e reso
> criticamente partecipe del suo ruolo creativo: l'avanguardia industriale,
> l'"autorità", i fruitori, sono coinvolti con funzioni teoricamente omogenee
> nell'impetuoso ed "esaltante" processo di continuo sviluppo e trasformazione.
> Dalla realtà produttiva, all'immagine, all'uso dell'immagine, l'intera
> macchina urbana spinge fino all'estremo delle sue implicite possibilità il
> potenziale "sociale" della *civilisation machiniste*.

:::

:::

::: alts

> Zdaj moramo odgovoriti na vprašanje: kako to, da projekt za Alžir, kasnejši
> načrti za evropska in afriška mesta in celo Le Corbusierjevi minimalni
> predlogi ostanejo mrtva črka? Ali ne gre morda za protislovje med tem kar je
> bilo izrečeno -- da so v teh predlogih najnaprednejše in formalno
> najrazvitejše hipoteze buržoazne kulture na polju *designa* in urbanizma -- in
> neuspehi, ki jim je Le Corbusier priča v prvi osebi?

::: {lang=it}

> Dobbiamo ora rispondere alla domanda: come mai il progetto per Algeri, i
> successivi piani per le città europee ed africane, e persino le proposte di
> minima avanzate da Le Corbusier rimangono lettera morta? Non v'è forse
> contraddizione fra quanto si è detto -- che, cioè, in quelle proposte va letto
> a tutt'oggi l'ipotesi più avanzata e formalmente elevata della cultura
> borghese, nel campo del *design* e dell'urbanistica -- e i fallimenti vissuti
> in prima persona da Le Corbusier?

:::

:::

::: alts

> Na to vprašanje se lahko odgovori na več načinov, vsi so veljavni in
> komplementarni med seboj. Najprej je treba opozoriti, da Le Corbusier deluje
> kot "intelektualec" v strogem pomenu. Ne povezuje se -- kot Taut, May ali
> Wagner -- z lokalnimi in državnimi oblastmi: njegove hipoteze pripadajo
> posebni realnosti (jasno, orografija in zgodovinska razslojenost Alžira sta
> izjemi in forma projekta, ki jo upošteva, je neponovljiva), toda metodo, ki ga
> vodi, je možno posplošiti. Od posameznega k univerzalnemu: natanko nasprotno
> metodi, ki ji sledijo intelektualci weimarske socialdemokracije. Ni slučaj, da
> Le Corbusier v Alžiru več kot štiri leta dela brez provizije ali plačila. On
> je "iznašel" svojo zaposlitev, jo posplošil in je pripravljen osebno plačevati
> svojo aktivno in predlagalno vlogo.

::: {lang=it}

> Si possono dare molte risposte a tale interrogativo, tutte valide e
> complementari fra loro. Va anzitutto notato che Le Corbusier opera come
> "intellettuale" in senso stretto. Non si lega -- come Taut, May o Wagner -- a
> poteri locali e statali: le sue ipotesi partono da realtà particolari (certo,
> l'orografia e la stratificazione storica di Algeri sono delle eccezioni e la
> forma del progetto che di esse tiene conto è irripetibile), ma il metodo che
> le guida è ampiamente generalizzabile. Dal particolare all'universale:
> l'esatto contrario del metodo seguito dagli intellettuali della
> socialdemocrazia di Weimar. Né è causale che Le Corbusier lavori, ad Algeri,
> per più di quattro anni senza incarico o compensi. Egli "inventa" la sua
> committenza, la generalizza, è disposto a pagar di persona il proprio ruolo
> attivo e propositivo.

:::

:::

Na vprašanje, odgovarja, kako to, da projekt za Alžir ni nikoli uresničen, se
lahko odgovori na več načinov. Najprej, Le Corbusier se ne povezuje z lokalnimi
in državnimi oblastmi. Dalje, njegova metoda je obratna tej weimarske
socialdemokracije: projekt razdeluje od posameznega k univerzalnemu in je zato
kot metoda posplošljiva in privzema značilnosti eksperimentov, ki se ne morejo v
celoti prevesti v realnost. Njihova posplošljivost pa je na tej točki že v
nasprotju z zaostalimi strukturami, ki jih naslavlja:

::: alts

> Zaradi tega imajo vsi njegovi modeli značilnosti laboratorijskih
> eksperimentov: in ne more se zgoditi, da bi laboratorijski model lahko
> *tout-court* prevedli v realnost. Toda to ni dovolj. Možnost posplošitve
> hipotez je v nasprotju z zaostalimi strukturami, ki jih želi stimulirati: če
> so zahteve po revolucioniziranju arhitekture v skladu z najnaprednejšimi
> nalogami ekonomske in tehnološke realnosti še nezmožne jim dati koherentne in
> organske forme, ni čudno, da se realizem hipotez predstavlja kot utopija.

::: {lang=it}

> Ciò fa si che i suoi modelli assumano tutti i caratteri di esperimenti di
> laboratorio: e non si dà il caso che un modello di laboratorio si traduca
> *tout-court* in realtà. Ma non basta. La generalizzabilità dell'ipotesi
> scontra con le strutture arretrate che essa vuole stimolare: se le esigenze
> sono quelle di un rivoluzionamento dell'architettura in sintonia con i compiti
> più avanzati di una realtà economica e tecnologica ancora incapace di dare
> loro coerente e organica forma, nessuna meraviglia che il realismo delle
> ipotesi venga assunto come utopia.

:::

:::

::: alts

> Vendar se brodoloma v Alžiru -- in bolj splošno, Le Corbusierjevega "neuspeha"
> -- ne bere pravilno, če ga ne beremo kot pojav mednarodne krize moderne
> arhitekture.

::: {lang=it}

> Ma non si legge correttamente il naufragio di Algeri -- e, più in generale, il
> "fallimento" di Le Corbusier -- se non lo si lega al fenomeno della crisi
> internazionale dell'architettura moderna.

:::

:::

"Brodolom" Alžira je zato potrebno brati kot pojav mednarodne krize
moderne arhitekture.

> ***

## [Kapitalistični razvoj se sooči z ideologijo]

::: alts

> Zanimivo je preiskati, kako je sodobno zgodovinopisje poskušalo pojasniti
> krizo moderne arhitekture, katere začetek postavlja v leta okoli 1930 in
> katere nadaljevanje še do danes je splošno sprejeto. Evropski fašizmi na eni,
> stalinizem na drugi strani: tem političnim zapletom se pripisuje skoraj vsa
> "krivda" začetka krize. Hkrati pa se sistematično zanemarja pojav novega
> odločilnega protagonista na svetovnem prizorišču še posebej po veliki
> ekonomski krizi leta 1929: mednarodne reorganizacije kapitala in uveljavitev
> sistemov anticikličnega planiranja.

::: {lang=it}

> È interessante esaminare come la storiografia corrente abbia tentato di
> spiegare la crisi dell'architettura moderna, la cui data di inizio vien fatta
> cadere negli anni intorno al '30 e la cui accentuazione fino ad oggi è
> comunemente accettata. Fascismi europei da un lato, stalinismo dall'altro: a
> tali involuzioni politiche vengono attribuite pressoché tutte le "colpe"
> iniziali della crisi. Nello stesso tempo viene sistematicamente ignorata
> l'introduzione, a livello mondiale, e proprio dopo la grande crisi economica
> del '29, di un nuovo decisivo protagonista: la riorganizzazione internazionale
> del capitale e l'affermazione dei sistemi di pianificazione anticiclica.

:::

:::

::: alts

> Pomenljivo je, da lahko skoraj vse cilje z ekonomskega področja, kot so
> formulirani v Keynesovi *General Theory*, ponovno najdemo kot čisto ideologijo
> v poetični osnovi moderne arhitekture. "Osvoboditi se strahu pred prihodnostjo
> s fiksiranjem te prihodnosti kot sedanjost": temelj keynesijanskega
> intervencionizma je enak temelju poetike moderne umetnosti. Toda tudi, in to
> prav v političnem smislu, je v temelju Le Corbusierjevih urbanističnih teorij:
> Keynes se sprijazni in dogovori s "stranko katastrofe" in teži k povrnitvi
> grožnje z njeno asimilacijo na vedno nove ravni;[^24] Le Corbusier vzame na
> znanje razredno realnost modernega mesta in prenese njegove konflikte na višjo
> raven, vzpostavi najnaprednejše predloge integracije občinstva, ki je
> soudeleženo kot upravljavec in aktivni potrošnik, v urbani mehanizem razvoja,
> ki je zdaj napravljen v organsko "človeškega".

::: {lang=it}

> È significativo che quasi tutti gli obiettivi formulati in sede economica
> dalla *General Theory* del Keynes possano essere ritrovati, come ideologia
> pura, alla base delle poetiche dell'architettura moderna. "Liberarsi dalla
> paura del futuro fissando quel futuro come presente": il fondamento
> dell'interventismo keynesiano è il medesimo delle poetiche dell'arte moderna.
> Ma, e proprio in senso politico, è alla base delle teorie urbanistiche di Le
> Corbusier: Keynes fa i conti con il "partito della catastrofe", e tende a
> recuperarne la minaccia, assorbendola a sempre nuovi livelli[24]; Le Corbusier
> prende atto della realtà di classe della città moderna e ne sposta i conflitti
> al livello più alto, mettendo in atto la più elevata proposta di integrazione
> del pubblico, coinvolto, come operatore e come consumatore attivo, nel
> meccanismo urbano di sviluppo, reso, ora, organicamente "uma

:::

:::

<!-- obstaja slovenski prevod-->

[^24]: Gl. A. Negri, *La teoria capitalista dello stato nel '29: John M.
    Keynes*, v: "Contropiano", 1968, št. 1, str. 3 in dalje.

<!--

[^24]: Cfr. A. Negri, *La teoria capitalista dello stato nel '29: John M.
    Keynes*, in: "Contropiano", 1968, n. 1, pp. 3 e sgg.

-->

::: alts

> Zdaj se potrjuje naša začetna hipoteza. Arhitektura kot *ideologija Plana* je
> premagana z *realnostjo Plana*, ko ta preseže raven utopije in postane delujoč
> mehanizem.

::: {lang=it}

> Si conferma ora la nostra ipotesi iniziale. L'architettura come *ideologia del
> Piano* è travolta dalla *realtà del Piano*, una volta che questo dal livello
> dell'utopia sia divenuto meccanismo operante.

:::

:::

Tu Tafuriju pripisujemo najizvirnejše in najproduktivnejše historiziranje krize
modernizma. Vse cilje arhitekturne ideologije plana prepozna tudi v Keynesovi
*Splošni teoriji*, [@keynes2006splosna] ki odločno informira mednarodno
reorganizacijo kapitala in vzpostavitev anticikličnih sistemov po gospodarski
krizi 1929. Temelje keynesijanskega intervencionizma prepozna v temelju poetike
moderne umetnosti in Le Corbusierjevih urbanističnih teorij: razredni kompromis
-- med delom in kapitalom -- v obliki subvencioniranja in spodbujanja potrošnje
ter s tem prenos konfliktov na višjo raven, ki je kosumpcija. [Na tem mestu se
Tafuri sklicuje na razredno analizo keynesijanizma prav tako objavljeno v
*Contropiano* in prevedeno tudi v slovenščino, gl. @negri1984delavci] Tako v
keynesijanizmu kot pri Le Corbusierju je strukturna negativnost kapitalizma
(stopnjevanje razrednih konfliktov in "posledični" dvig mezd) napravljena v
njegovo pozitivno spodbudo. Potrošnja je napravljena v aktivni faktor
produkcije. In v tej uresničitvi plana je premagana arhitekturna ideologija
plana, ki zdaj nima več funkcije:

::: alts

> Kriza moderne arhitekture se začne natanko v trenutku, ko njen naravni
> naslovnik -- veliki industrijski kapital -- njeno temeljno ideologijo naredi
> za svojo ter postavi na stran nadzidave. Od tega trenutka dalje arhitekturna
> ideologija izčrpa lastne naloge: njena vztrajna želja videti realizacijo
> lastnih hipotez postane, ali sredstvo za premagovanje zaostale realnosti, ali
> nadležna motnja.

::: {lang=it}

> La crisi dell'architettura moderna inizia nel momento preciso in cui il suo
> destinatario naturale -- il grande capitale industriale -- ne fa propria
> l'ideologia di fondo, mettendone da parte le sovrastrutture. Da quel momento
> in poi l'ideologia architettonica ha esaurito i propri compiti: il suo
> ostinarsi a voler vedere realizzate le proprie ipotesi diviene o molla per il
> superamento di realtà arretrate, o fastidioso disturbo.

:::

:::

::: alts

> V tej luči lahko beremo nazadovanja in tesnobne borbe modernega gibanja od
> leta 1935 do danes: splošnejše zahteve po racionalizaciji mest in teritorijev
> ostajajo neizpolnjene in nadalje učinkujejo kot posredne pobude za realizacijo
> delnih ciljev, ki jih sproti določa sistem.

::: {lang=it}

> In tale chiave si possono leggere le involuzioni e l'angoscioso dibattersi del
> movimento moderno dal '35 circa ad oggi: le istanze più generali di
> razionalizzazione delle città e dei territori rimangono inevase, continuando
> ad agite come stimolo indiretto per le realizzazioni compatibili con gli
> obiettivi parziali fissati via via dal sistema.

:::

:::

::: alts

> Na tej točki se zgodi nekaj, kar se na prvi pogled zdi nerazložljivo.
> Ideologija forme dozdevno opusti svoje realistično poslanstvo, da bi se
> obrnila k drugemu polu, ki je neločljivo povezan z dialektiko buržoazne
> kulture: brez opustitve "utopije projekta", odrešitev od procesov, ki so
> konkretno obšli raven ideologije, išče v povrnitvi kaosa, v kontemplaciji
> tiste tesnobe, ki jo je konstruktivizem dozdevno za vedno izkoreninil, v
> sublimaciji Nereda. 

::: {lang=it}

> È a questo punto che interviene qualcosa di inspiegabile, a prima vista.
> L'ideologia della forma sembra abbandonare la propria vocazione realistica,
> per ripiegare sul secondo polo insito nella dialettica propositiva della
> cultura borghese: senza abbandonare "l'utopia del progetto", il riscatto
> contro i processi che concretamente hanno scavalcato il livello dell'ideologia
> viene cercato nel recupero del Caos, nella contemplazione di quell'angoscia
> che il Costruttivismo sembrava aver debellato per sempre, nella sublimazione
> del Disordine.

:::

:::

::: alts

> Po tem, ko prispe do nesporne *impasse* notranjih protislovij kapitalističnega
> razvoja, se arhitekturna ideologija odpove svoji spodbudni vlogi v razmerju do
> produkcijskih struktur in se maskira z dvoumnimi *slogans*, ki oporekajo
> "tehnološki civilizaciji".

::: {lang=it}

> Giunti ad un'*impasse* innegabile delle contraddizioni interne dello sviluppo
> capitalista, l'ideologia architettonica rinuncia a svolgere il proprio ruolo
> propulsivo nei confronti delle strutture di produzione, mascherandosi dietro
> ambigui *slogans* contestativi della "civiltà tecnologica".

:::

:::

::: alts

> Nesposobna analizirati dejanske vzroke krize *designa* in z vso svojo
> pozornostjo osredotočena na interne probleme *designa samega*, sodobna kritika
> kopiči simptomatične ideološke iznajdbe v poskus ponujanja novih vsebin za
> zavezništvo med tehnikami vizualne komunikacije in industrijske produkcije. Ni
> naključje, da opredeljeno področje za odrešitev tega zavezništva -- zdaj
> zahtevano z zvenom dvoumnega "neohumanizma", ki ima, v razmerju do *Neue
> Sachlickheit* 20\. let 20\. stoletja, resno pomanjkljivost mistifikacije svoje
> posredniške vloge med Utopijo in Produkcijo -- vztraja prav na *podobi mesta*.

::: {lang=it}

> Incapace di analizzate le cause effettive della crisi del *design*, e
> concentrando tutta l'attenzione sui problemi interni al *design stesso*, la
> critica contemporanea va accumulando sintomatiche invenzioni ideologiche nel
> tentativo di offrite nuova sostanza all'alleanza fra tecniche di comunicazione
> visiva e produzione industriale. Né è causale che il campo individuato per il
> riscatto di tale alleanza -- postulata ora con inflessioni di ambiguo
> "neoumanesimo", che, rispetto alla *Neue Sachlickheit* degli anni '20, ha il
> grave demerito di mistificare il proprio ruolo di mediatrice fra Utopia e
> Produzione -- insista precisamente sull'*immagine della città*.

:::

:::

::: alts

> Mesto kot nadzidava torej. Bolje rečeno, umetnost je zdaj poklicana mestu
> podati obličje nadzidave. Pop art, op art, analize urbane *imageability*,
> *esthétique prospective* se usmerijo k naslednjemu cilju: prikriti, z
> razrešitvijo v raznovrstnih *podobah*, protislovja sodobnega mesta ter s tem
> simbolno povzdigniti tiste formalne kompleksnosti, ki, brane z ustreznimi
> parametri, niso nič drugega kot eksplozija nerazrešenih razhajanj, ki uhajajo
> planu naprednega kapitala. Povrnitev koncepta *umetnosti* je tako funkcionalno
> v tej novi vlogi zakrivanja. Prav tam kjer se *industrial design* postavi na
> avantgardo tehnološke produkcije in pogojuje njeno kakovost z namenom
> povečanja konsumpcije, se *pop art*, s ponovno uporabo ostankov, zavržkov te
> produkcije, postavi na njeno retrogardo. Toda to ustreza ravno dvojni zahtevi,
> ki je zdaj obrnjena k tehnikam vizualne komunikacije. Umetnost, ki se odpove
> postaviti se na avantgardo produkcijskih ciklov, dokaže, da proces
> konsumpcije, onkraj verbalnih oporekanj, teži k neskončnosti, da tudi
> *zavržki*, sublimirani v neuporabne ali nihilistične objekte, lahko privzamejo
> novo *uporabno vrednost*, ponovno vstopijo, četudi zgolj skozi zadnja vrata, v
> cikel produkcije-konsumpcije.

::: {lang=it}

> Città come sovrastruttura dunque. Anzi, l'arte è ora chiamata a dare un volto
> sovrastrutturale alla città. Pop art, op art, analisi sull'*imageability*
> urbana, *esthétique prospective*, convergono in questo obiettivo: dissimulare,
> risolvendole in *immagini* polivalenti, le contraddizioni della città
> contemporanea, esaltando figurativamente quella complessità formale che, letta
> con parametri adeguati, altro non è se non l'esplosione dei dissidi insanabili
> che sfuggono al piano del capitale avanzato. Il recupero del concetto di
> *arte* è quindi funzionale a tale nuovo ruolo di copertura. È vero che laddove
> l'*industrial design* si pone alla testa della produzione tecnologica
> condizionandone la qualità in vista di un'espansione dei consumi, la *pop
> art*, riutilizzando i residui, i rifiuti di quella produzione, si pone alla
> sua retroguardia. Ma ciò corrisponde esattamente alla duplice richiesta ora
> rivolta alle tecniche di comunicazione visiva. L'arte che rinuncia a porsi
> all'avanguardia dei cicli di produzione, dimostra, al di là delle
> contestazioni verbali, che il processo di consumo tende all'infinito, che
> anche i *rifiuti* possono assumere, sublimati in oggetti inutili o
> nichilistici, un nuovo *valore d'uso*, rientrando, sia pure per la porta di
> servizio, nel ciclo produzione-consumo.

:::

:::

::: alts

> Toda ta retrogarda je tudi znak odpovedi -- morda le začasne --
> kapitalističnega plana od popolne razrešitve protislovij mesta, od
> preoblikovanja mesta v popolnoma organiziran stroj, brez odpadkov arhaičnega
> značaja ali splošnih disfunkcionalnosti.

::: {lang=it}

> Ma è anche indice, tale retroguardia, della rinuncia -- forse solo provvisoria
> -- del piano capitalista a risolvere compiutamente le contraddizioni della
> città, a trasformare la città in macchina totalmente organizzata, senza
> sprechi di carattere arcaico o disfunzioni generalizzate.

:::

:::

::: alts

> V tej fazi je dejansko treba delovati za prepričevanje občinstva, da so
> protislovja, neravnovesja in kaos, tipični za sodobna mesta, neizbežni:
> namreč, da ta kaos vsebuje neraziskano bogastvo, neomejene možnosti uporabe,
> ludične vrednote, ki naj se predlagajo kot novi družbeni fetiši.

::: {lang=it}

> In tale fase, infatti, bisogna agire per persuadere il pubblico che
> contraddizioni, squilibri, caoticità, tipici della città contemporanea, sono
> inevitabili: anzi, che tale caos contiene in sé ricchezze inesplorate,
> possibilità illimitate da utilizzare, valori ludici da proporre come nuovi
> feticci sociali.

:::

:::

::: alts

> *Carnaby Street* in novi utopizem sta torej različna vidika istega fenomena.
> Arhitekturni in supertehnološki utopizem, ponovno odkritje *igre* kot pogoja
> soudeležbe občinstva, prerokbe "estetske družbe", pozivi k ustanavljanju
> *primata domišljije*: to so predlogi nove urbane ideologije.[^25]

::: {lang=it}

> *Carnaby Street* e nuovo utopismo sono dunque aspetti diversi del medesimo
> fenomeno. Utopismo architettonico e supertecnologico, riscoperta del *gioco*
> come condizione di coinvolgimento del pubblico, profezie di "società
> estetiche", inviti all'istituzione di un *primato dell'immaginazione*: tali le
> proposizioni delle nuove ideologie urbane[25].

:::

:::

[^25]: Gl. besedila, ki jih je treba razumeti kot simptome fenomena: G. C.
    Argan, *Relazione introduttiva al convegno sulle "strutture ambientali"*,
    Rimini, settembre, 1968; L. Quaroni, *La Torre di Babele*, Matsilio, Padova,
    1967; M. Ragon, *Les visionnaires de l'architecture*, Paris, 1965; A.
    Boatto, *Pop art in U.S.A.*, Lerici, Milano, 1967; F. Menna, *Profezza di
    una società estetica*, Lerici, Milano, 1968\. Bilo bi odveč opozoriti, da je
    primerjava teh besedil povsem nepovezana s premisleki glede njihove notranje
    rigoroznosti in kakovosti posameznega prispevka.

<!--

[^25]: Cfr., come testi da assumere come sintomi del fenomeno: G. C. Argan,
    *Relazione introduttiva al convegno sulle "strutture ambientali"*, Rimini,
    settembre, 1968; L. Quaroni, *La Torre di Babele*, Matsilio, Padova, 1967;
    M. Ragon, *Les visionnaires de l'architecture*, Paris, 1965; A. Boatto, *Pop
    art in U.S.A.*, Lerici, Milano, 1967; F. Menna, *Profezza di una società
    estetica*, Lerici, Milano, 1968\. Dovrebbe essere superfluo avvertire che
    l'accostamento di tali testi prescinde del tutto da considerazioni relative
    al loro intrinseco rigore e dalla qualità del loro apporto.

-->

*Carnaby street* tu predstavlja 

::: alts

> Obstaja zgledno besedilo v katerem so vsi pozivi, naj umetniška dejavnost
> namesto operativne prevzame novo prepričevalno vlogo, združeni in medsebojno
> uravnoteženi.

::: {lang=it}

> Esiste un testo esemplare in cui tutte le sollecitazioni rivolte all'attività
> artistica ad assumere un nuovo ruolo persuasivo invece che operativo, si
> trovano sintetizzate e compensate fra loro.

:::

:::

::: alts

> In pomenljivo je da *Livre blanc de l'art total* Pierra Restanyja, na katero
> se sklicujemo, izrecno postavlja na tapeto vse teme, ki izhajajo iz
> zaskrbljene ugotovitve o izrabi ciljev, ki se jim je do danes sledilo. Z
> naslednjim izidom: da "novi" predlogi odrešitve umetnosti z različnimi
> besedami privzemajo prav iste pomene kot predlogi zgodovinskih avantgard, brez
> jasnosti ali samozaupanja, ki so jih te lahko upravičeno razkazovale.

::: {lang=it}

> Ed è significativo che il *Livre blanc de l'art total* di Pierre Restany, cui
> ci riferiamo, ponga esplicitamente sul tappeto tutti i temi che nascono dalla
> preoccupata constatazione dell'usura degli obiettivi fino ad ora perseguiti.
> Con un risultato: che le "nuove" proposte di riscatto dell'arte assumono, con
> parole diverse, proprio i medesimi connotati delle proposizioni delle
> avanguardie storiche, senza la chiarezza o la fiducia in se stesse che quelle
> potevano a buon diritto ostentare.

:::

:::

::: alts

> Restany piše: "La metamorphose des languages n'est que le reflet des
> changements structurels de la societé. La technologie, en réduisant de façon
> croissante la décalage entre l'art (synthèse des nouveaux languages) et la
> nature (la realité moderne, technique, et urbaine) joue le rôle déterminant
> d'une catalyse suffisante et nécessaire." ["Preobrazba jezika ni nič drugega
> kot odsev strukturalne spremembe družbe. Tehnologija, ki vedno bolj krči
> razkorak med umetnostjo (sintezo novih jezikov) in naravo (moderno, tehnično
> in urbano stvarnostjo), igra odločilno vlogo v zadostni in nujni
> katalizi."][^26]

::: {lang=it}

> "La metamorphose des languages n'est que le reflet des changements structurels
> de la societé -- scrive Restany[26], -- La technologie, en réduisant de fagon
> croissante la décalage entre l'art (synthèse des nouveaux languages) et la
> nature (la realité moderne, technique, et urbaine) joue le ròle déterminant
> d'une catalyse suffisante et nécessai

:::

:::

[^26]: P. Restany -- *Le livre blanc de l'art totale; pour une esthetique
    prospective*, v: "Domus", 1968, št. 269, str. 50.

<!--

[^26]: P. Restany -- *Le livre blanc de l'art totale; pour une esthetique
    prospective*, in: "Domus", 1968, n. 269, p. 50.

-->

::: alts

> "Outre ses immenses possibilités et ses ouvertures illimitées, la technologie
> témoigne de la flexibilité indespensable en période de transition: elle permet
> à l'artiste conscient d'agir non plus sur les effets formels de la
> communication, mais sur ses termes-mémes, l'imagination humaine. *La
> technologie contemporaine permet enfin è l'imagination de prendre le pouvoir*:
> liberée de toute entrave normative, de tout problème de réalisation ou de
> production, l'imagination créatrice peut s'identifier à la conscience
> planétaire. *L'esthétique prospective est le véhicule de la plus grande
> espérance de l'homme: sa libération collective.* La socialisation de l'art
> traduit la convergence des forces de création et de production sur un objectif
> de synthèse dinamique, la métamorphose technique: c'est à travers cette
> restructuration que l'homme et le réel trouvent leur vrai visage moderne,
> qu'ils redeviennent *naturels*, toute aliénation dépassée." ["Tehnologija
> priča skozi neizmerne možnosti in neomenjene predigre o nepogrešljivi
> prilagodljivosti v prehodnem obdobju: zavestnemu umetniku dovoljuje, da ne
> deluje več na oblikovne učinke komunikacije, pač pa na same njene izraze, na
> človeško predstavo. *Moderna tehnologija končno dovoljuje predstavi, da pride
> na oblast*: ustvarjalna domišljija, ki je osvobojena vsake uravnavajoče
> prepreke, vsakega problema realizacije ali proizvodnje, se lahko poistoveti s
> planetarno zavestjo. *Bodoča estetika je nosilka največjega človekovega upa,
> kolektivne osvoboditve.* Socializacija umetnosti vpelje sovpadanje
> ustvarjalnih in proizvajalnih sil k cilju, ki ga predstavlja dinamična sinteza
> in tehnična preobrazba; prek te ponovne strukturacije najdeta človek in
> stvarnost svoj resnični moderni izraz, ki ju znova napravi naravna, brez
> zastarele odtujitve."][^27]

::: {lang=it}

> "Qutre ses immenses possibilités et ses ouvertures illimitées, la technologie
> témoigne de la flexibilité indespensable en période de transition: elle permet
> à l'artiste conscient d'agir non plus sur les effets formels de la
> communication, mais sur ses termes-mémes, l'imagination humaine. *La
> technologie contemporaine permet enfin è l'imagination de prendre le pouvoir*:
> liberée de toute entrave normative, de tout problème de réalisation ou de
> production, l'imagination créatrice peut s'identifier à la conscience
> planétaire. *L'esthétique prospective est le véhicule de la plus grande
> espérance de l'homme: sa libération collective.* La socialisation de l'art
> traduit la convergence des forces de création et de production sur un objectif
> de synthèse dinamique, la métamorphose technique: c'est à travers cette
> restructuration que l'homme et le réel trouvent leur vrai visage moderne,
> qu'ils redeviennent *naturels*, toute aliénation dépassée"[27].

:::

:::

[^27]: Prav tam. Poudarek naš.

In v tej luči Tafuri bere nazadovanja in involucijo modernega gibanja od
1935 dalje. Na eni strani lahko spremljamo nadaljnje zahteve po
racionalizaciji mest, ki ostajajo neizpolnjene, saj se ta problem zdaj
naslavlja na višji ravni. Na drugi, kot odziv, pa opustitev
realističnega poslanstva in potopitev nazaj v kontemplacijo Nereda in
privzemanje dvoumnih sloganov, ki izzivajo "tehnološko civilizacijo". Po
odpovedi iskanju vzrokov neuspehov pri poseganju v mesto se umetnost
(ali arhitekturna kultura) osredotoča na iskanje novih vsebin, ki bi jih
ponudila za nova zavezništva z industrijo. Te Tafuri prepozna v obratu k
podobam -- govora je o pojavih *op* in *pop arta*, *industrial design*,
jasno pa tudi o *Podoba mesta* [gl. @lynch2011podoba] --, ki z
metodološkimi predlogi in analizami raznovrstnih podob realna
protislovja mesta, ki še vedno uhajajo planu, simbolno povzdignejo na
formalno kompleksnost, katerih obravnava kot gradivo je omejena na raven
nadzidave, torej brez izhodišč v njihove materialne pogoje. Poleg
povratka v demonstracijo neizbežnosti neravnovesja in kaosa, je tem
pripisana tudi neomejena možnost uporabe in neodkrit potencial igre. Na
tej podlagi se oblikuje tudi nova urbana ideologija, ki spet, v odnosu
do občinstva, privzame prepričevalno vlogo. Konkreten primer teorije
neoavantgard, ki ga obravnava, je *Livre blanc de l'art total* Pierrja
Restanyja, vzorec, ki ga prepozna, pa je vsekakor opazen tudi
situacionistični kritiki mesta (preko sklica na francoski "maj"), ki
ima še danes zagotovljeno romantično stojišče v kritični umetniški
dejavnosti v razmerju do mesta:

::: alts

> Krog se sklene. Markuzejanska mitologija je izkoriščena za dokazovanje, da je
> možno doseči neko nedoločeno "kolektivno svobodo" znotraj trenutnih
> produkcijskih razmerij in ne z njihovim spodkopavanjem. Dovolj je zgolj
> "podružbiti umetnost" in jo postaviti na čelo tehnološkega "napredka": ni
> pomembno, če celoten cikel moderne umetnosti dokazuje -- včeraj morda
> razumljivo, danes zgolj zaostalo -- utopičnost teh predlogov. Za to postane
> dopustno celo vpijanje najbolj dvoumnih *slogans* francoskega "maja".
> *Imagination au povoir* odobrava dogovor med protestništvom in
> konzeravtivizmom, med simbolično metaforo in produkcijskimi procesi, med begom
> in *realpolitik*.

::: {lang=it}

> Il circolo si chiude. La mitologia marcusiana è sfruttata per dimostrare che è
> all'interno degli attuali rapporti di produzione e non con il loro
> sovvertimento che è possibile guadagnare una non meglio precisata "libertà
> collettiva". Basta "socializzare l'arte" e porla alla testa del "progresso"
> tecnologico: e poco importa se tutto il ciclo dell'arte moderna dimostra
> l'utopismo -- ieri forse spiegabile, oggi solo arretrato -- di tale
> proposizione. Per questo diviene persino lecito assorbire anche il più ambiguo
> degli *slogans* del "maggio" francese. L'*imagination au pouvoir* sancisce il
> concordato fra contestazione e conservazione, fra metafora simbolica e
> processi produttivi, fra evasione e *realpolitik*.

:::

:::

::: alts

> In to ni vse. S ponovno utrditvijo posredniške vloge umetnosti se lahko celo
> vrnemo k pripisovanju *naturalističnih* konotacij umetnosti, ki ji jih je
> pripisovala razsvetljenska kultura. Avantgardna kritika tako razkriva svojo
> nalogo kot ideološki instrument današnje kritične faze kapitalističnega
> univerzuma. Do te mere, da jo je nenatančno še vedno nazivati "kritika", saj
> je njena funkcionalnost v tem smislu povsem očitna: zmeda in dvoumnost, ki jih
> pridiga v imenu umetnosti -- instrumentalno usvajanje vseh zaključkov
> semantične analize -- nista nič drugega kot sublimirane metafore in
> dvoumnosti, ki oblikujejo strukture trenutnega mesta. Zavračanje kritike, da
> se umesti znotraj kroženja projektiranje--produkcija--konsumpcija je zato
> simptomatično.

::: {lang=it}

> E non basta. Con il riaffermare il ruolo mediatore dell'arte si può persino
> tornare ad assegnarle i connotati *naturalistici* che la cultura illuminista
> le aveva attribuito. La critica di avanguardia rivela così il proprio compito
> di strumento ideologico della presente fase critica dell'universo capitalista.
> Tanto che è inesatto chiamarla ancora "critica", dato che la sua funzionalità,
> in tal senso, è del tutto palese: la confusione e l'ambiguità che essa predica
> per l'arte -- assumendo strumentalmente tutte le conclusioni delle analisi
> semantiche -- non sono che le metafore sublimate della crisi e dell'ambiguità
> che informano le strutture della città attuale. Il rifiuto della critica a
> porsi al di fuori del circolo progettazione-produzione-consumo è quindi
> sintomatico.

:::

:::

Kritika torej postane operativna, umeščena znotraj produkcijskih ciklov njegova
protislovja prestavlja na vedno naprednejše ravni, ki so tam izolirano
kontemplirane v mejah domišljije (in predvsem lobanje).

::: alts

> Restany nadaljuje: "La méthode critique doit concourir à la généralisation de
> l'esthétique: dépassement de l'oeuvre et production multiple; distinction
> fondamentale entre les deux ordres complémentaire de la création et de la
> production, systématitation de la rechetche opérationelle et de la coopération
> technique dans tous les domaines de l'expérimentation de synthèse;
> structuration psyco-sensorielle de la notion de jeu et de spectacle;
> organisation de l'espace ambient en vue de la communication de masse;
> insertion de l'environnement individuel dans l'espace collectif du bien-étre
> urbain." ["Kritična metoda mora tekmovati z generalizacijo estetike: s
> prehitevanjem dela in pomnoženo proizvodnjo; temeljno razlikovanje med
> komplementarnim določilom ustvarjanja in proizvodnjo, sistematizacijo
> operacionalnega razlikovanja in sodelovanja tehnike na vseh področjih
> eksperimentiranja s sintezo; psiho-senzorno strukturacijo pojma igre in
> prizora, organizacijo prostorskega okolja, ustrezno komunikacijo celote;
> uvrstitvijo posameznega okolja v skupni prostor blagostanja."][^28]

::: {lang=it}

> "La méthode critique -- prosegue Restany[28], -- doit concourir à la
> généralisation de l'esthétique: dépassement de l'oeuvre et production
> multiple; distinction fondamentale entre les deux ordres complémentaire de la
> création et de la production, systématitation de la rechetche opérationelle et
> de la coopération technique dans tous les domaines de l'expérimentation de
> synthèse; structuration psyco-sensorielle de la notion de jeu et de spectacle;
> organisation de l'espace ambient en vue de la communication de masse;
> insertion de l'environnement individuel dans l'espace collectif du bien-étre
> urba

:::

:::

[^28]: Prav tam. Jasno je da Restanyjevo besedilo uporabljamo samo kot primer
    mitologije, ki je izjemno razširjena med protagonisti neoavantgard: po drugi
    strani pa lahko številne naše trditve veljajo tudi za globlje
    "disciplinarne" poskuse odrešitve skozi utopijo.

<!--

[^28]: Ibidem. È chiaro che usiamo il testo di Restang solo come
    esemplificazione di una mitologia estremamente diffusa presso i protagonisti
    della neoavanguardie: e d'altra parte molte delle nostre asserzioni possono
    valere anche per ben più profondi tentativi "disciplinari" di riscatto
    attraverso l'utopia.

-->

::: alts

> Kritika mora zdaj delovati znotraj produkcijskih ciklov, delovati mora, tako
> da postane *operativna*, kot spodbuda za prestavljanje Plana na vedno
> naprednejše ravni.

::: {lang=it}

> La critica deve quindi operare all'interno dei cicli di produzione, deve,
> diventando *operativa*, fungere da stimolo per spostare il Piano su livelli
> sempre più avanzati.

:::

:::

::: alts

> Kaj je zares novega v odnosu do predlogov zgodovinskih avantgard? Ne bi bilo
> težko, s tehnično analizo, dokazati, da so onkraj ponovnega ideološkega zagona
> prostori za novosti zelo reducirani. Vrh tega, v podobnih predlogih v razmerju
> do koherentnosti zgodovinskih avantgard -- ko izvzamemo markuzijansko utopijo
> o odrešitvi prihodnosti skozi Veliko zavrnitev, ki jo opravi domišljija --
> vsekakor nekaj manjka.

::: {lang=it}

> Cosa c'è di realmente nuovo rispetto alle proposte delle avanguardie storiche?
> Non sarebbe difficile dimostrare, mediante una analisi tecnica, che, al di là
> del rilancio ideologico, i margini di novità sono estremamente ridotti. Anzi,
> in proposte simili -- una volta messa da parte l'utopia marcusiana di un
> riscatto della dimensione futura attraverso il Grande Rifiuto compiuto
> dall'immaginazione -- rispetto alla coerenza delle avanguardie storiche v'è
> senz'altro qualcosa di meno.

:::

:::

::: alts

> Kako si pravzaprav razložiti vso to vztrajanje na zapravljanju forme in na
> povrnitvi posebne razsežnosti umetniških tem, v luči potreb po vse večji
> integraciji formalne razdelave v produkcijski cikel?

::: {lang=it}

> Come si spiega infatti tutto questo insistere sullo spreco della forma e il
> ricupero di una dimensione specifica dei temi artistici, alla luce della
> necessità di una crescente integrazione dell'elaborazione formale nel ciclo
> della produzione?

:::

:::

::: alts

> Nesporno se soočamo z dvema sočasnima pojavoma. Na eni strani umestitev
> gradbeništva znotraj občih planov nadaljuje z omejevanjem funkcionalnosti
> ideološke vloge arhitekture. Na drugi je videti, kot da ekonomska in družbena
> protislovja, ki na področju urbanih in teritorialnih struktur vedno bolj
> pogosto eksplodirajo, ukazujejo zaustavitev Plana kapitala. Do zdaj kaže, da v
> soočenju s temo racionalizacije urbane ureditve -- osrednja in odločilna tema
> -- kapital ni uspešno našel ustreznih sil in instrumentov za izpolnitev nalog,
> ki jih je arhitekturna ideologija modernega gibanja pravilno pokazala.

::: {lang=it}

> È innegabile che ci si trova di fronte a due fenomeni concomitanti. Da un lato
> la collocazione della produzione edilizia all'interno dei piani complessivi
> continua a ridurre la funzionalità del ruolo ideologico dell'architettura.
> Dall'altro lato le contraddizioni economiche e sociali che esplodono in modo
> sempre più accelerato nell'ambito delle strutture urbane e territoriali,
> sembrano imporre al Piano del capitale una battuta d'arresto. Di fronte al
> tema della razionalizzazione dell'assetto urbano -- tema centrale e
> determinante -- il capitale mostra per ora di non riuscite a trovare al suo
> interno le forze e gli strumenti adeguati per adempiere ai compiti
> correttamente indicati dalle ideologie architettoniche del movimento moderno.

:::

:::

::: alts

> To prisili intelektualno opozicijo in celo razredne organizacije, ki so do
> danes sprejele boriti se za razrešitev teh težav in protislovij, k zatekanju v
> aktivno funkcijo -- spodbujanja, kritike, boja. Ostrina boja za urbanistične
> zakone (tako v Italiji kot v ZDA), za reorganizacijo gradbenega sektorja, za
> *urban renewal* je lahko mnoge zavedla, da bitka za planiranje lahko
> predstavlja neposredni moment razrednega boja. 

::: {lang=it}

> Ciò obbliga a ricorrere alla funzione attiva -- di stimolo, di critica, di
> lotta -- delle opposizioni intellettuali e persino delle organizzazioni di
> classe, che fino ad oggi hanno accettato di combattere per risolvere tali
> difficoltà e contraddizioni. La durezza della lotta per le leggi urbanistiche
> (in Italia come negli U.S.A.), per la riorganizzazione del settore edilizio,
> per l'*urban renewal*, ha potuto illudere molti che la battaglia per la
> pianificazione potesse costituite un momento, addirittura, della lotta di
> classe.

:::

:::

::: alts

> Odkritje njihovega zatona kot aktivni ideologi, spoznavanje ogromnih
> tehnoloških možnosti racionalizacije mesta in teritorija skupaj z
> vsakodnevnimi ugotovitvami o njihovi brezkoristnosti, zastaranje specifičnih
> metod projektiranja še preden so njihove hipoteze lahko preverjene v realnosti
> za arhitekte tvorijo tesnobno vzdušje, ki omogoča, da na obzorju vidimo zelo
> konkretno ozadje, zastrašujoče kakor največje zlo: proletarizacija arhitekta
> in njegova umestitev, zdaj brez poznohumanističnih zadržkov, v planske načrte
> produkcije.

::: {lang=it}

> Per gli architetti, la scoperta del loro declino come ideologi attivi, la
> constatazione delle enormi possibilità tecnologiche di razionalizzazione delle
> città e dei territori unita alla quotidiana constatazione del loro spreco,
> l'invecchiamento dei metodi specifici di progettazione prima ancora di poterne
> verificare nella realtà le ipotesi, generano un clima ansioso, che lascia
> intravvedere all'orizzonte uno sfondo molto concreto e temuto come il peggiore
> dei mali: la proletarizzazione dell'architetto e il suo inserimento, senza più
> remore tardoumanistiche, nei programmi pianificati della produzione.

:::

:::

::: alts

> Da ta nova profesionalna situacija -- že realnost v deželah naprednega
> kapitalizma kot so ZDA ali socializiranega kapitala kot je ZSSR -- arhitekte
> straši in da se je izogibajo z najbolj nevrotičnimi ideološkimi in oblikovnimi
> izigravanji, je samo pokazatelj politične zaostalosti te skupine
> intelektualcev.

::: {lang=it}

> Che tale nuova situazione professionale -- già reale in paesi a capitalismo
> avanzato come gli U.S.A. o a capitale socializzato come l'U.R.S.S. -- venga
> temuta dagli architetti e scongiurata con i contorcimenti formali ed
> ideologici più nevrotici, è solo l'indice dell'arretratezza politica di tale
> gruppo intellettuale.

:::

:::

Splošna tafurijanska "shema" arhitekturnega avtobiografskega napovedovanja
lastne usode ravno preko pozitivnih predlogov izvzemanja iz te usode se zdi na
primeru postmoderne rekvalifikacije dodatno dopolnjena:

::: alts

> Po tem, ko so ideološko predvideli železni zakon Plana, se arhitekti, nezmožni
> zgodovinskega branja opravljene poti, upirajo skrajnim posledicam procesov, ki
> so jih sami pomagali sprožiti. In, kar je še huje, patetično poskušajo oživeti
> "etiko" moderne arhitekture in ji -- verbalno -- naložiti politične naloge
> primerne le za začasno pomiritev abstraktnega in neupravičenega besa.

::: {lang=it}

> Dopo aver anticipato ideologicamente la ferrea legge del Piano, gli
> architetti, incapaci di leggere storicamente il percorso compiuto, si
> ribellano alle estreme conseguenze dei processi che essi hanno contribuito ad
> innescare. E, ciò che è peggio, tentano patetici rilanci "etici"
> dell'architettura moderna, assegnandole -- verbalmente -- compiti politici
> adatti solo a calmare provvisoriamente astratti guanto ingiustificati furori.

:::

:::

Če je vstop arhitekturnega projekta v razsežnost kapitalističnega plana
označeval objektivno ublagovljenje arhitekture, je nadaljnje estetsko
"raziskovanje" tega ublagovljenja in spremljajoče iskanje novih "zavezništev"
onkraj industrijskega kapitala napovedovalo prekarizacijo arhitekturne
dejavnosti. Specifični razvoj slovenske arhitekturne prekarizacije, v kolikor je
postmoderni obrat k trgu moral potekati s samostojnim političnim delovanjem v
samoupravnih organizacijah, ni zgolj lokalna "zanimivost", ampak postane eden
jasnejših primerov globalnosti sprememb, ki jih je od arhitekture kot
institucije terjala postmoderna reorganizacija kapitala. 

::: alts

> Zaradi tega se je potrebno prepričati: da celoten cikel moderne arhitekture in
> novih sistemov vizualne komunikacije nastaja, se razvija in vstopa v krizo kot
> grandiozen poskus -- zadnji s strani buržoazne likovne kulture -- razrešitve
> na ideološki ravni, ki je toliko bolj zahrbtna, ko je znotraj konkretnih
> operacij, neravnovesij, protislovij in zaostalosti, ki so tipične za
> kapitalistično reorganizacijo svetovne trgovine.

::: {lang=it}

> Perché di questo è necessario convincersi: che l'intero ciclo
> dell'architettura moderna e dei nuovi sistemi di comunicazione visiva nascono,
> si sviluppano ed entrano in crisi, come un grandioso tentativo -- l'ultimo
> della cultura figurativa borghese -- per risolvere, sul piano di un'ideologia
> tanto più insidiosa quanto più tutta interna alle operazioni concrete, calata
> nei reali cicli di produzione, squilibri, contraddizioni e ritardi, tipici
> della riorganizzazione capitalista del mercato mondiale.

:::

:::

::: alts

> V tem smislu si red in nered nehata nasprotovati. Če beremo njihove realne
> zgodovinske pomene, med konstruktivizmom in "protestno umetnostjo", med
> racionalizacijo gradbeništva in neformalnim subjektivizmom ali *pop* ironijo,
> med kapitalističnim planom in urbanim kaosom, med ideologijo planiranja in
> poetiko objekta ni protislovja.

::: {lang=it}

> Ordine e disordine, in tale accezione, finiscono di opporsi fra loro. Letti
> nei loro reali significati storici non v'è contraddizione fra costruttivismo e
> "arte di protesta", fra razionalizzazione della produzione edilizia e
> soggettivismo informale o ironia *pop*, fra piano capitalista e caos urbano,
> fra ideologia della pianificazione e poetica dell'oggetto.

:::

:::

::: alts

> *Usoda* kapitalistične družbe v tem pogledu nikakor ni tuja *projektu*.
> Ideologija projekta je povsem bistvena za integracijo modernega kapitalizma v
> vse baze in nadzidave človeškega obstoja, prav toliko kot je bistvena iluzija
> možnosti nasprotovanja temu *projektu* z instrumenti *alternativnega*
> projektiranja, ali z radikalnim "antiprojektiranjem".

::: {lang=it}

> Il *destino* della società capitalista, a tale stregua, non è affatto estraneo
> al *progetto*. L'ideologia del progetto è tanto essenziale all'integrazione
> del capitalismo moderno in tutte le strutture e sovrastrutture dell'esistenza
> umana, quanto lo è l'illusione di potersi opporre a quel *progetto* con gli
> strumenti di una progettazione *diversa*, o di un'"antiprogettazione"
> radicale.

:::

:::

Neumorno iskanje odvodov v simbolne in marginalne politične akcije (ter njihova
takojšnja funkcionalizacija), spoznavanje ogromnih tehnoloških zmožnosti
racionalizacije (skupaj z vsakodnevnimi ugotovitvami o njihovi brezkoristnosti)
in zastaranje metod projektiranja še preden so njihove hipoteze lahko
preverjene, pravi, tvorijo tesnobno vzdušje, ki na obzorju nakazuje zastrašujoč
prizor proletarizacije arhitekturnega dela. Da se, zaradi nezmožnosti
zgodovinske in materialne analize "opravljene poti", posledicam procesov, ki so
jih "sami sprožili", upirajo s poskusi rekuperacije "etike" moderne arhitekture
in rekuperacije idealiziranega poklicnega dostojanstva, kaže na "politično
zaostalost te skupine intelektualcev". [@tafuri1969peruna, 77] Iz stališča
opravljene analize si zgodovinski pomeni mnogovrstnih poskusov -- konstruktivizem
in "protestna umetnost", racionalizacija in subjektivizem, plan in kaos -- končno
ne nasprotujejo: oba pola sta bistvena za kapitalistični razvoj. Tako projekt
kot utopija (iluzija možnosti nasprotovanja projektu z alternativnim
projektiranjem), sta "bistvena za integracijo kapitalizma v vse baze in
nadzidave človeškega obstoja". Na tej analizi zgodovinskega poteka in izteka
avantgard sloni slavni zaključek:

::: alts

> Lahko bi se reklo, da za arhitekturo in planning obstajajo številne marginalne
> in retrogardne naloge. Nas rajši zanima kako to, da je marksistično usmerjena
> kultura, doslej s skrajno skrbnostjo in vztrajnostjo, ki je vredna boljšega
> cilja, po krivem zanikala ali zakrivala to preprosto resnico: da, kot ne more
> obstajati razredna politična ekonomija, ampak zgolj razredna kritika politične
> ekonomije, tako ne moremo utemeljiti razredne estetike, umetnosti,
> arhitekture, ampak zgolj razredno kritiko estetike, umetnosti, arhitekture,
> mesta.

::: {lang=it}

> Può anche darsi che esistano molti compiti marginali e di retroguardia per
> l'architettura e il planning. A noi interessa piuttosto chiederci come mai
> sino ad ora la cultura di intonazione marxista abbia, con estrema cura e con
> un'ostinazione degna di miglior causa, negato o coperto colpevolmente questa
> semplice verità: che, come non può esistere un'Economia Politica di classe, ma
> solo una critica di classe all'Economia politica, così non può fondarsi
> un'Estetica, un'arte, un'architettura di classe, ma solo una critica di classe
> all'Estetica, all'arte, all'architettura, alla città.

:::

:::

Del odlomka verjetno predstavlja najširše reproduciran del prispevka, navajamo
ga pa v celoti, saj razkriva usmeritve kritike ideologije, ki jih želimo
poudariti. Da ne gre za "poetiko odpovedi" in "smrt arhitekture" smo že
poudarili, a ob vseh stališčih "*contro*", ki so se utemeljevali od *Teorij in
zgodovine*, samih izhodišč revije in do obravnavanega spisa, so točke dvoumnih
branj povsem na voljo. Tu ne moremo mimo tega, da je že občinstvo tega
političnega sporočila zelo omejeno, kaj šele teža, ki si jo sam pripisuje. S tem
ne mislimo, da je trivialno, temveč predlaga strateško usmeritev tistim, ki si
izhodišča za razumevanje objektivnega stanja v veliki meri že delijo -- recimo
temu marksistično usmerjena kultura. Do te, bi lahko rekli, Tafuri zavzema
"sektaško" stališče, kot to ob drugi priložnosti tudi sam pojmuje, izhaja pa iz
Fortinijevega vabila, da je v kritičnih časih to edina pot do novih enotnosti.
["Pridevniki, s katerimi lahko komentirali te moje izjave, že obstajajo in
sektaški je eden od njih. Menim, da smo dolgo trpeli zaradi pomanjkanja
„sektaštva“, to je drže do ločitve, ki je edini način za novo enotnost," v
@fortini2016cunning, 128] Do svojega arhitekturnega občinstva, do "arhitektov
levice", pa bi vseeno zagovarjali, da zavzema nekakšno spodbujevalno stališče.
Že zaključek *Teorij in zgodovine* je možna posledica dramatizacije protislovij
spodbuda in izziv arhitekturi. Vsekakor gre za namero, ki jo upravičeno tudi tu
prepoznamo: možnost odčarane kritike, ki jo, tako ali drugače, arhitekturna
praksa "vzame na znanje" in, tako ali drugače, prevede v delovanje. Seveda
izhaja, da mora vsako takšno dejanje ohranjati utopičnost, ampak to nikoli ni
izključevalo realnih učinkov arhitekture na svet.

::: alts

> Dosledna marksistična kritika ideologije arhitekture in urbanizma ne more
> drugega kot demistificirati slučajno, zgodovinsko, nikakor objektivno ali
> univerzalno, realnost, ki se skriva za enotnimi kategorijami umetnost,
> arhitektura, mesto.

::: {lang=it}

> Una coerente critica marxista dell'ideologia architettonica e urbanistica non
> può che demistificare le realtà contingenti, storiche, niente affatto
> oggettive o universali, che si celano dietro le categorie unificanti dei
> termini arte, architettura, città.

:::

:::

::: alts

> S privzemanjem svoje zgodovinske vloge in razredne kritike nase mora kritika
> arhitekture postati kritika urbane ideologije in se mora na vsak način
> izogibati vstopanju v "progresiven" dialog s tehnikami racionalizacije
> protislovij kapitala.

::: {lang=it}

> Assumendo il proprio ruolo storico ed oggettivo di critica di classe, la
> critica dell'architettura deve divenire critica dell'ideologia urbana,
> evitando in tutti i modi di entrare in colloqui "progressivi" con le tecniche
> di razionalizzazione delle contraddizione del capitale.

:::

:::

::: alts

> Med intelektualnimi iluzijami, ki jih je treba najprej poraziti, je ta, ki je,
> zgolj z vrednoto podobe, usmerjena v anticipacijo pogojev nekakšne arhitekture
> za "osvobojeno družbo". Kdor predlaga takšen slogan, če postavimo na stran
> njihov boleč utopizem, se izogiba vprašanju, ali ta cilj lahko dosežemo brez
> lingvistične, metodološke, strukturne revolucije, ki presega preprosto
> subjektivno voljo ali preprosto posodobitev sintakse.

::: {lang=it}

> E fra le illusioni intellettuali da frustrare la prima è quella che tende ad
> anticipare, con il solo valore dell'immagine, le condizioni di un'architettura
> "per una società liberata". Chi propone un tale slogan evita di chiedersi se,
> anche messo da parte il suo palese utopismo, tale obiettivo sia perseguibile
> senza una rivoluzione linguistica, metodologica e strutturale, la cui portata
> va ben al di là della semplice volontà soggettiva o del semplice aggiornamento
> di una sintassi.

:::

:::

Četudi so dalje omenjene "intelektualne iluzije" ["Med intelektualnimi
iluzijami, ki jih je treba najprej poraziti, je ta, ki je, zgolj z vrednoto
podobe, usmerjena v anticipacijo pogojev nekakšne arhitekture za „osvobojeno
družbo“. Kdor predlaga takšen slogan, če postavimo na stran njihov boleč
utopizem, se izogiba vprašanju, ali ta cilj lahko dosežemo brez lingvistične,
metodološke, strukturne revolucije, ki presega preprosto subjektivno voljo ali
preprosto posodobitev sintakse," v @tafuri1969peruna, 78] in "pogoji razrednega
boja" ["Refleksija o arhitekturi kot kritika konkretne ideologije, ki jo
„izvaja“ arhitektura sama, ne more drugega, kot iti dlje in doseči specifično
politično dimenzijo v kateri je sistematična destrukcija mitologij, ki
vzdržujejo razvoje, le eden od ciljev: in samo prihodnji pogoji razrednega boja
bodo dali vedeti, če je to kar si zastavljamo avantgardna ali retrogardna
naloga," v @tafuri1969peruna, 79] danes v veliki meri neprepoznavne
"dnevnopolitične" kategorije, menimo, da imajo za arhitekturno teorijo -- kot
kategorije razreda, produkcijskih odnosov, kapitalističnega razvoja in
"dialektike" intelektualnih poklicev, ki se ravno z uveljavljanjem svoje
avtonomije določajo do njih -- tudi "dolgotrajnejše" posledice, namreč da so
predlog ene kritično-zgodovinske ali analitične prakse.

::: alts

> Moderna arhitektura je začrtala poti svoje usode, ko je postala nosilka
> idealov napredka in racionalizacije, ki so delavskemu razredu tuji, ali pa so
> mu blizu le v socialdemokratski perspektivi. Lahko se spomnimo zgodovinske
> neizogibnosti tega pojava, toda ko je enkrat prepoznan kot tak ni več možno
> skriti dokončne realnosti, ki tesnobne odločitve arhitektov "levice" napravi
> brezkoristne.

::: {lang=it}

> L'architettura moderna ha segnato le vie del proprio destino facendosi
> portatrice di ideali di progresso e razionalizzazione cui la classe operaia è
> estranea, o da cui è investita solo in una prospettiva socialdemocratica. Si
> potrà riconoscere l'inevitabilità storica di tale fenomeno, ma una volta
> riconosciuto come tale non è più possibile nascondere la realtà ultima che
> rende inutilmente angosciose le scelte degli architetti "di sinistra".

:::

:::

::: alts

> Brezkoristno tesnobne, ker je brezkoristno boriti se znotraj brezizhodnih
> kapsul. Kriza moderne arhitekture ni posledica "utrujenosti" ali "propada":
> bolj gre za krizo funkcije ideologije arhitekture. "Padec" moderne umetnosti
> je zadnje pričanje buržoazne dvoumnosti, razpete med "pozitivnimi" cilji
> posredovanja med protislovji ter neusmiljenim raziskovanjem svojega
> objektivnega ublagovljenja. Nobenega "odrešenja" ni več mogoče najti znotraj
> nje: niti z nemirnim tavanjem v "labirintih" podob, tako raznovrstnih, da so
> končno neme, niti z zapiranjem v mračno tišino geometrij, zadovoljnih z lastno
> popolnostjo.

::: {lang=it}

> Inutilmente angosciose perché è inutile dibattersi all'interno di capsule
> prive di uscite. La crisi dell'architettura moderna non consegue da
> "stanchezze" o "dilapidazioni": è piuttosto crisi della funzione ideologica
> dell'architettura. La "caduta" dell'arte moderna è l'ultima testimonianza
> dell'ambiguità borghese, tesa fra obiettivi "positivi", di mediazione fra le
> contraddizioni, e la spietata autoesplorazione della propria merceficazione
> oggettiva. Nessuna "salvezza" è più rinvenibile al suo interno: né aggirandosi
> inquieti in "labirinti" di immagini talmente polivalenti da risultare mute, né
> chiudendosi nello scontroso silenzio di geometrie paghe della propria
> perfezione.

:::

:::

::: alts

> Zaradi tega ni mogoče predlagati arhitekturnih "protiprostorov": raziskovanje
> alternativ znotraj struktur, ki pogojujejo mistificiran značaj projektiranja,
> je žalostno protislovje v terminih.

::: {lang=it}

> È per questo che non è dato proporre "controspazi" architettonici: la ricerca
> di un'alternativa al di dentro delle strutture che condizionano il carattere
> mistificato della progettazione è una palese contraddizione in termini.

:::

:::

::: alts

> Refleksija o arhitekturi kot kritika konkretne ideologije, ki jo "izvaja"
> arhitektura sama, ne more drugega, kot iti dlje in doseči specifično politično
> dimenzijo v kateri je sistematična destrukcija mitologij, ki vzdržujejo
> razvoje, le eden od ciljev: in samo prihodnji pogoji razrednega boja bodo dali
> vedeti, če je to kar si zastavljamo avantgardna ali retrogardna naloga.

::: {lang=it}

> La riflessione sull'architettura, in quanto critica della ideologia concreta,
> "realizzata" dall'architettura stessa, non può che andare oltre, e raggiungere
> una dimensione specificamente politica, di cui la distruzione sistematica
> delle mitologie che ne sostengono gli sviluppi non è che uno degli obbiettivi:
> e solo le condizioni future della lotta di classe daranno modo di sapere se
> questo che ci prefiggiamo è compito di avanguardia o di retroguardia.

:::

:::

---
lang: sl
references:
- type: article-journal
  id: tafuri1969peruna
  author:
  - family: Tafuri
    given: Manfredo
  title: "Per una critica dell'ideologia archittetonica"
  title-short: "Per una critica dell'ideologia archittetonica"
  container-title: "Contropiano: materiali marxisti"
  issue: 1
  issued: 1969
  page: 31-79
  language: it
- type: book
  id: tafuri1968teorie
  author:
  - family: Tafuri
    given: Manfredo
  title: "Teorie e storia dell'architettura"
  publisher-place: Bari
  publisher: Laterza
  issued: 1968
  language: it
- type: book
  id: tafuri1980theories
  author:
  - family: Tafuri
    given: Manfredo
  title: "Theories and history of architecture"
  translator:
  - family: Verrecchia
    given: Giorgio
  publisher-place: London
  publisher: Granada
  issued: 1980
  language: en
# vim: spelllang=sl,it
...
