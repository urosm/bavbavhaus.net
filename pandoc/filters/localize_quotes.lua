SCRIPT_NAME = "localize_quotes.lua"
os.setlocale("C")

QUOTEMARKS_LANGMAP = {
  de = {'„', '“', '‚', '‘' },
  en = {'“', '”', '‘', '’' },
  it = {'«', '»', '“', '”' },
  sl = {'»', '«', '„', '“' },
}

QUOTEMARKS = nil

return {
  { Meta = function (meta)
    local lang = pandoc.utils.stringify(meta["lang"] or "en")
    QUOTEMARKS = QUOTEMARKS_LANGMAP[lang] -- @todo what about xx-XX?
  end },
  { Quoted = function (quoted)
    if not QUOTEMARKS then return end
    if quoted.quotetype == "DoubleQuote" then
      quoted.content:insert(1, QUOTEMARKS[1])
      quoted.content:insert(   QUOTEMARKS[2])
      return quoted.content
    end
    if quoted.quotetype == "SingleQuote" then
      quoted.content:insert(1, QUOTEMARKS[3])
      quoted.content:insert(   QUOTEMARKS[4])
      return quoted.content
    end
  end }
}
