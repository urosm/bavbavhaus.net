---
title: |
  Planiranje metropole: odnos teorija-praksa-tehnika v socialni politiki
  socialdemokracije med drugo in tretjo internacionalo
author: Uroš Mikanovič
date: 2023
abstract: |
  Področje zidave je v prvi tretjini dvajsetega stoletja bilo
  privilegirano področje planskih ideologij.  Neproduktivnost podedovane
  geografije predindustrijskega in špekulativnega mesta, koncentracija
  primanjkljaja delavskih bivališč, temu odgovarjajoča politična,
  moralna in higienska nepokorščina proletariata ter tehnološka
  zaostalost gradbene industrije kapitalističnemu razvoju predstavljajo
  meje, ki jih ne more enostavno spontano preseči.  Avantgarde
  modernističnega gibanja razdelajo arhitekturne in urbanistične
  programe, ki na obči ravni razrešujejo te probleme ter jih ponudijo
  industrialcem in lokalnim upravam.  Na predavanju bo predstavljena
  epizoda socialdemokratskega upravljanja mest v obdobju Weimarske
  Nemčije in Rdečega Dunaja, ko razreševanje protislovij
  kapitalističnega mesta prvič postane poseben cilj organiziranega
  delavskega gibanja.  Ogledali si bomo razvoj in zaključek poskusov
  integracije urbanističnega in gospodarskega planiranja v pogojih
  lokalno omejenih političnih uspehov nemške in avstrijske
  socialdemokracije; poskusov, ki so še danes mitološka referenca
  levičarskih programov stanovanjske preskrbe.
lang: sl
...

# Planiranje metropole: odnos teorija-praksa-tehnika v socialni politiki socialdemokracije med drugo in tretjo internacionalo

<!-- O čem govorimo?  -->

Socialdemokratski arhitekturni in urbanistični poskusi v nemško govoreči
srednji Evropi zasedajo posebno mesto v sodobni arhitekturni kulturi.
Zaradi njihovega neposrednega političnega konteksta kot osrednje
sredstvo socialne politike nemške socialdemokratske stranke v weimarskem
obdobju (<!-- TODO: letnica -->) ali -- v nekoliko drugačni obliki,
oziroma pod drugačnimi pogoji -- avstrijske socialdemokratske stranke v
obdobju tako imenovanega Rdečega Dunaja (<!-- TODO: letnica -->) pa sta
ti arhitekturni in urbanistični epizodi tudi redni referenci sodobnih
evropskih socialističnih gibanj.  Posebna pozornost, ali medvojnemu
evropskemu arhitekturnemu modernizmu ali medvojnemu reformizmu, je
zgodovinsko legitimna.  Gre za obdobje profesionalizacije avantgardnih
(arhitekturnih) umetniških gibanj in njihovega vstopa v nove gospodarske
in politične institucije.  To dejansko pomeni ustanovitev arhitekture v
obliki, ki je dobršen del 20\. stoletja disciplini omogočala
relativno avtonomen razvoj novih vlog in polj za uresničevanje lastnih
programov.  In hkrati gre za obdobje, ko zastopniki organiziranega
delavskega gibanja prvič razdelajo politične prakse in instrumente boja
z zemljiško rento, zadružnega upravljanja socialne stanovanjske gradnje
in tehnološke prenove gradbenega obrata.  S tem množična stanovanjska
gradnja postane poseben cilj delavskega gibanja in še danes ostaja
programski temelj evropskih levih strank.  Kar pa sodobna angažirana
arhitekturna kultura pri sklicevanju na evropsko modernistično tradicijo
spregleda, so dejanski pogoji enotnosti političnih, gospodarskih in
urbanističnih ciljev socialdemokratske arhitekture ter dejanska
parcialnost projektov, ki so bili pod temi cilji izvedeni.  

Pri osvetljevanju temeljnega trenutka socialdemokratskega upravljanja
mest torej ne gre za enostavno ugotavljanje politične angažiranosti
nemške arhitekturne avantgarde, temveč za razkrivanje ideoloških
predpostavk ter strateških odločitev tako arhitekturne inteligence kot
klasične socialdemokracije, ki so takšno sodelovanje omogočale in
narekovale, ter dejanske rezultate ter učinke tega sodelovanja, ki so,
za razliko od danes, takrat že bili tema tako tehničnih kot političnih
polemik.

<!-- Tafurijeva teorija avantgarde in kontekst kritike medvojne
socialdemokracije -->

Pri obravnavi se bomo zanašali na dela italijanskega arhitekturnega
zgodovinarja Manfreda Tafurija, ki je s sodelavci v desetletju
1970--1980 arhitekturo Weimarske republike in Rdečega Dunaja analiziral
v sklopu širše raziskave koncepta arhitekturne avantgarde [kritiko
(umetniške, intelektualne) avantgarde, kot zgodovinski pojav buržoazne
kulture z začetki v renesansi, prvič razdela v @tafuri1968teorie;
angleški prevod @tafuri1980theories; bolj osredotočena analiza vloge
avantgard in modernističnega gibanja znotraj kapitalističnega razvoja v
@tafuri1973progetto; slovenski prevod @tafuri1985projekt; zaključek
cikla "kritike avantgarde", kjer so v posameznih poglavjih, med drugim,
natančneje razdelana napredna arhitekturna in urbanistična gibanja treh
ekonomskih programov, v @tafuri1980sfera; angleški prevod
@tafuri1987sphere].  Po Tafuriju morajo umetniške avantgarde zaradi
svoje nepopolne negacije lastne tradicije, kot buržoazna inteligenca,
prehoditi krožno pot, na kateri si v svoji revolucionarni fazi
nakopičijo ideološko prtljago, ki jih po doseženi "umetniški revoluciji"
pusti razorožene in nepripravljene [@tafuri1980theories, 1-3].
Natančneje, če govorimo o obdobju med vojnama, arhitekti in arhitektke
svoje interese kot družbena skupina na ideološki ravni vežejo na takrat
napredne politične in gospodarske programe.  Evropski arhitekti in
arhitektke svoje umetniške in tehniške programe opredelijo v terminih
organiziranega delavskega gibanja in tako cilje razrednega boja
opredelijo v povsem tehničnih ukrepih.  Intervencijski modeli, ki jih je
modernistična arhitektura pod temi cilji razvila, so imeli vprašljivo
korist izven ozkega parlamentarnega obzorja nemške socialdemokracije.
Področja, ki jih je modernistična arhitektura naslavljala --
neproduktivnost podedovanega stanja v prostoru (predkapitalistični
zemljiški odnosi ter vzorci zidave), večanje in koncentracija
stanovanjskega primanjkljaja, temu odgovarjajoča politična, moralna in
higienska nevarnost urbanega proletariata ter tehnološka zaostalost
gradbene industrije --, so predstavljala predvsem zgornje meje
reprodukcije kapitala [gl\. @tafuri1985projekt, 69 in dalje].  Za
Tafurija je pri modernistični arhitekturi, urbanizmu in oblikovanju
ključno razumeti njihovo vlogo pri substitucionizmu, ki je razredni boj
proletariata zamenjal za tehnološki napredek.  Pri tem naj v zadnji
instanci ne bi šlo za nič drugega kot integracijo delavskega boja v
*kapitalistični razvoj* kot njegov konstruktivni dejavnik.  Zanj je
zgodovina modernistične arhitekture zgodovina preoblikovanja sredstev
distribucije in potrošnje, vseh aspektov človeškega obstoja, po podobi
kapitala.  Na podlagi tega kapital razdela tudi lastne anticiklične
instrumente za obvladovanje svojih kriz ter s tem napravi posredovanja
napredne inteligence odvečno.  Vodilna vloga arhitekture na področju
mesta je s tem presežena, svojo novo neaktualnost, h kateri je sama
prispevala, pa čuti kot posebno krizo:

> Odkritje njihovega zatona kot dejavnih ideologov, ugotovitve o
> ogromnih tehnoloških možnostih, ki se lahko uporabijo pri
> racionalizaciji mesta in teritorija, ki vsakodnevno spodbujajo njihove
> napore, zastarevanje posebnih metod projektiranja, še preden se lahko
> preverijo v dejanskosti, porajajo za arhitekte tesnobno klimo, ki na
> obzorju ponuja vpogled na zelo konkretno ozadje, največjo nesrečo, ki
> se je bojijo: zaton "profesionalnosti" arhitekta in njegovo uvrstitev,
> brez poznoromantičnih zadržkov, v programe, v katerih je ideološka
> vloga arhitekture kar najmanjša [@tafuri1985projekt, 112].

Tafuri svojo kritiko prvič razdela v tekom šestdesetih letih 20\.
stoletja, v dobi neoavantgard in povojnih arhitekturnih humanizmov, ki
so želeli funkcije modernistične arhitekture obnoviti v navezavi na nove
protagoniste povojne Evrope oziroma znotraj socialne države.  Zaradi
omejujočih zaključkov glede usod zgodovinskih avantgard in dvoma v
sodobne arhitekturne aktivizme sicer priznanega arhitekturnega
zgodovinarja znotraj arhitekturne kulture spremljajo zadržki glede
njegovega kulturnega pesimizma ter zgodovinskega ali ekonomskega
determinizma [za razgrnitev problema, ki ga tafurijanska diagnoza
predstavlja arhitekturi, ter resen poskus njenega preseganja gl\.
@ceferin2016niti, 57-75].  Toda impulz takšne historizacije
modernistične arhitekture ne izhaja iz stroge problematike strokovne ali
umetniške avtonomije arhitekture, temveč iz razprave o razmerju med
delavskim gibanjem in kapitalističnim planiranjem [gl\.
@corna2016thinking, pogl\. 1].  Njen zgodovinski kontekst je, kot smo
omenili, Evropa, oziroma, Italija po letih povojne obnove, ko je v
središču politične razprave nadzorovan ekonomski razvoj in je nujnost
kapitalističnega planiranja, tudi zaradi zahtev ZDA glede porabe
sredstev iz Programa za obnovo Evrope, samoumevna.  Pobudo in sredstva
določanja značaja dolgoročnega razvojnega načrta je obvladovala takratna
italijanska krščanska demokracija.  Kritika nestrankarske levice, zbrane
okoli revije *Classe operaia* in kasneje okoli revije *Contropiano* [ta
revija je izvorni kontekst spisov, ki bodo kasneje razširjeni v
kanonične učbenike arhitekturne zgodovine, gl\. @tafuri1969peruna;
@tafuri1970lavoro; @tafuri1971socialdemocrazia;
@tafuri1971austromarxismo] je bila, da sta takšne koordinate planiranja
nekritično sprejeli tudi italijanski socialistična in komunistična
stranka.  Pragmatični obrati in taktike iskanja "italijanske poti k
socializmu", ki so jih dodatno okrepili tudi notranja in zunanja
politika Sovjetske zveze sredi 50\. let, naj bi teoretske predpostavke
strank uradnih predstavnic delavskega gibanja o racionalnosti
modernizacije in vloge države v gospodarstvu vse bolj zbližale s
keynesijansko politiko italijanske krščanske demokracije [gl\.
@wright2002storming, 5-15].

Tem teoretskim predpostavkam ni sledil določen del italijanskih delavcev in,
kar je za nas zanimivo, določen del italijanske tehnične inteligence.

Pri tej razdelavi kritike kapitalističnega planiranja je zanimiva
zastopanost arhitektov in sorodnih profesionalcev, ki delujejo na
področju umeščanja v prostor in prostorskega planiranja.  To je mogoče
pripisati vlogi, ki so jo znotraj institucij kapitalističnega planiranja
imeli, in razočaranju z rezultati tega planiranja [za različne vloge, ki
jih je arhitekturna inteligenca imela znotraj kapitalističnega
planiranja ter kako se je na podlagi teh razvilo tudi tafurijansko
arhitekturno zgodovinopisje in kritika gl\. @galimberti2022images].  Na
eni strani kot tehnični kader, ki jih je zaposloval napredni tovarnar
Olivetti tudi pri načrtovanju humanega tovarniškega mesta.  Na drugi
kot javni uslužbenci pri načrtovanju in izvajanju stanovanjskih
programov ali programov urbanizacije juga države.
Ravno stanovanjsko vprašanje, kot socialna politika in dejavnost
gradbene industrije, je spodkopavalo teoretske predpostavke o
racionalnosti kapitalističnega planiranja in reformizmu kot zadovoljivi
in vzdržni socialistični strategiji.

Tafurijevo pozornost do arhitekturne epizode socialdemokracije v času
Weimarske republike in Rdečega Dunaja je treba gledati tudi v tej luči,
kot političnoteoretsko intervencijo v svoj strokovni in politični milje,
ki je optimizem svoje volje trošil na repu buržoazne ideologije.  To je
na omejujočem horizontu pojmovanja socialistične politike kot socialne
politike in družbenega napredka kot kapitalističnega razvoja.

Ker je, tudi zaradi okoljske krize in zaostrovanja mednarodnih trgovskih
odnosov, vprašanje industrijske politike z dolgoročnimi cilji spet
legitimno vprašanje znotraj zahodnih buržoaznih strank in ker tako
angažirana arhitektura kot sodobna levica v to potencialno obnovo
projekta modernizacije projicirata svoje politične in strokovne cilje,
se nam zdi vredno vrniti se k Tafurijevi analizi Weimarske in Dunajske
socialdemokracije.  Pogledali bomo ideološke predpostavke nemških
umetniških avantgard in arhitekturnega modernističnega gibanja, ki so
vzdrževale poroko med arhitekturno inteligenco in socialno politiko.
Dalje bomo pogledali razvoj orodij takšne politike v pogojih po
stabilizaciji weimarske ekonomije.  Pogledali bomo učinkovanje teh
orodij na primeru arhitektov osredotočenih na socialno gradnjo v Berlinu
in Frankfurtu ter njihovo razočarano samokritiko.  Obdobje Rdečega
Dunaja bomo, zaradi političnih in arhitekturnih specifik, obravnavali
ločeno.

## Delo brez kapitala: od razprave o svetih in podružbljanju do ekonomske demokracije

<!-- *Arbeitsrat* in *Sozialisierung* -->

Na tem mestu ne moremo slediti vsem razlikam v držah, ki so jih različne
nemške umetniške avantgarde od začetka stoletja do po koncu prve
svetovne vojne zavzemale do vprašanja revolucije in konkretnih
političnih dejanj po njej [glede tega gl\. @tafuri1987ussrberlin; prav
tako gl\. @fowkes2023theshock].  Za vse pa velja zvezanost z izkušnjo
metropole, to je z mestom industrializiranega kapitalizma:  

> Osvoboditi doživljanje šoka od vsakega avtomatizma, utemeljiti na tej
> izkušnji vizualne kodekse in kodekse delovanja, ki so jih posodile že
> utrjene značilnosti kapitalistične metropole -- hitro spreminjanje,
> organizacija in sočasnost komunikacij, pospešena izraba, eklekticizem
> -- reducirati strukturo umetniške izkušnje na čisti predmet (boleča
> metafora predmeta--blaga), vključiti občinstvo, ki je združeno v
> deklarirani medrazredni in *zato* protiburžoazni ideologiji: to so
> naloge, ki so jih v celoti kot svoje prevzele avantgarde v 20\. st\.
> 
> Ponavljamo: v njihovi celoti, ne glede na vse razlike med
> konstruktivizmom in protestno umetnostjo.  Kubizem, futurizem, dada,
> De Stijl [@tafuri1985projekt, 54].

Presnavljanje iracionalnosti kapitalistične produkcije kot se izraža v
urbanem prostoru in naslavljanje politično nejasno opredeljenega
občinstva je po Tafuriju skupna tema vseh umetniških avantgard in
njihova temeljna značilnost.

Nujnost in možnost politične konkretizacije njihovega delovanja
spodbudita revoluciji v Rusiji in Novembrska revolucija v Nemčiji.  Ob
katastrofi vojne so nemški intelektualci hitro prevzeli perspektivo
"novega sveta", ki jo je ustvaril pojav ljudskih množic kot političnega
protagonista, kot nov teren delovanja, kjer bo mogoče obnoviti pomen
intelektualnega dela [@tafuri1987ussrberlin, 121].  Sklicevanje na
Novembrsko revolucijo je neposredno -- vizualni umetniki in arhitekti
ustanovijo na primer *Novembergruppe* in po vzoru vojaških in delavskih
svetov tudi umetniški, *Arbeitsrat für Kunst* (AfK) --, a njihovi
programi ostajajo politično generični (v manifestu *Novemberguppe* tako
lahko beremo, da se ne opredeljuje niti kot razred, niti kot stranka,
temveč kot človeška bitja, ki želijo s svojim delom prispevati k
skupnemu dobremu) in so konkretneje razdelani v tistih točkah, kjer
pričakujejo tesnejše sodelovanje znotraj institucij [gl\. na primer
manifeste in programe *Novembergruppe*, *Bauhausa* in *Arbeitsrat für
Kunst* v @benson2002between, 204-208].

Navezovanje avantgardnih skupin na politiko sicer ni enoznačno za vse
struje, a po političnih porazih manjšinske SPD, KPD in gibanja svetov se
krha njihova transgresivna drža.  Tako se na primer *AfK* leta 1919 po
volilnem neuspehu manjšinske SPD, s katero so bili povezani, distancira
od politike.  Soustanovitelj *AfK*, arhitekt Bruno Taut odstopi in
njegovo mesto prevzame Walter Gropius, ki prepriča socialdemokratsko
vlado v Sachsen-Weimar, da ustanovi šolo uporabnih umetnosti, ki bo
znana kot *Bauhaus* [@fowkes2023theshock, 68-69].  Uresničevanje
programov umetniških avantgard se bo moralo zgoditi v navezavi z
večinsko SPD.

Pomembno vlogo v tej smeri bo imel arhitekt in urbanist Martin Wagner,
ki se leta 1919 včlani v SPD in se vključi v razpravo o svetih in
podružbljanju.  "Kot urbanist in arhitekt, ki se odziva na ekonomske
implikacije vsake pobude v odnosu do preoblikovanja fizičnega reda, je
bil Wagner eden vodilnih znotraj nemške arhitekturne kulture, ki se je
eksperimentalno boril za dokončno 'politizacijo tehnike'."
[@tafuri1987sozialpolitik, 200]  Kot mestni arhitekt leta 1919 projektira 500
stanovanj Grosssiedlung Lindenhof, kjer poskuša s standardizacijo
gradnje odpraviti pavze in neproduktivne stroške.  S svojim besedilom
"Die Sozialisierung der Baubetriebe" neposredno vstopi v razpravo o
delavskih svetih s popolnim planom podružbljanja gradbene industrije
[Angleški prevod v @wagner1987thesocialization].

V uvodu svoj prispevek predstavi kot praktično dopolnilo doslej zgolj
teoretskih načrtov iz Erfurtskega programa in iz Kautskijevega
*Richtlinien für ein sozialistisches Aktionsprogramm*.  Pravzaprav začne
s kritiko socialdemokracije, da v vseh teh letih ni pripravila
praktičnega načrta za podružbljanje gradbenega sektorja, da ni čas za
besede, temveč so potrebna dejanja za rešitev tehnično-organizacijskih
problemov, preden Nemčijo doleti kaos iz Rusije
[@wagner1987thesocialization, 235-236].  Cilj socialdemokratskih
političnih posegov na področje gradbene industrije je seveda zagotovitev
socialne stanovanjske gradnje.  Značilnosti gradbene industrije pa
predstavljajo poseben organizacijski problem: gradnja se izvaja na
mestu; je specializirana in različne vključene obrti niso konsolidirane;
novogradnja je sezonska; gradbeni delavci so mobilna delovna sila;
deluje po naročilu in ne more kopičiti zalog; omejena in usmerjena je na
lokalni trg [@wagner1987thesocialization, 247].  Wagner problematiko
vidi skozi prizmo usklajevanja racionalizacije delovnih procesov,
delavske participacije in akumulacije kapitala.

Subvencioniranje zasebnih izvajalcev je -- poleg očitnih problemov, ki
jih predstavlja stavkovni val in val ustanavljanja delavskih svetov --
nevzdržno, saj se pri značilni strukturno nizki organski sestavi
gradbenega kapitala pritiski na plače v večji meri prenesejo v stroške
gradnje, ki bi jim potem morali slediti dodatni javni izdatki.
[@wagner1987thesocialization]

Prav tako je za Wagnerja neustrezna tudi nacionalizacija ali
komunalizacija gradbenega sektorja.  Nacionalizacija je primerna, pravi,
samo za industrije, kjer sta proizvodnja in prodaja dosegle maksimalno
mero zveznosti in racionalizacije, da bi lahko prenesli birokratizacijo
teh procesov.  Prav tako pa za takšno upravljanje nemški delavci, po
Wagnerju, še niso pripravljeni. [@wagner1987thesocialization, 237-238]
Zanj je nujno sistem zasebnih odgovornosti, zmogljivosti in odločitev
posameznih obratov zaščititi pred birokratizacijo, ki bi jo
"parlamentarizacija" upravljanja preko nacionalizacije prinesla.

Za Wagnerja je največ potenciala v Kautskijevem najbolj dvoumnem
predlogu ustanavljanja združb gradbenih delavcev.  Zanimivo je, da
se Wagner, da bi bolje razdelal to idejo, spusti v 
zgodovino srednjeveških gradbenih delavnic, *Bauhütten*.  Tu poudari, da
je šlo za sodelovanje na podlagi delovne etike, ki so jo sodobna
industrijska razmerja spodkopala, kar spodbuja konflikt med delavci in
upravo.

Wagnernerjev predlog previdnega podružbljanja gradbene industrije
ohranja konkurenco med posameznimi gradbenimi obrati in prosto določanje
višine mezd.  Primerna organizacijska forma posameznega podružbljenega
gradbenega obrata je, pravi, podobna delniški družbi.  Sestavljajo jo
uprava sestavljena iz arhitektov, tehnikov, upravljavcev, ki projektira
zgradbe in vodijo projekte.  Lokalni svet sestavljen iz predstavnikov
zaposlenih na gradbišču in uprave.  Ta skrbi za organizacijo na
gradbišču.  Predsedstvo, ki skrbi za letne proračune, določa plače in
kosovne mezde, odpušča in zaposluje ...  Sindikat, ki združuje funkcije,
ki so pred podružbljanjem v domeni sindikata, združenja delodajalcev,
javnih servisov.  Ta organ je zadolžen za arbitražo in za deljenje
presežkov med deležnike.

Zanimivo je, da je v tej razdelani organizacijski shemi predvidenih več
vzvodov za nadzorovanje oziroma pomirjanje zahtev članstva oziroma
delavcev.  Edini predviden vzvod delavskih zahtev je predviden v
lokalnem svetu, ki je podrejen sindikatu in ni stalen organ.  Zanimivo
je tudi, da je predviden in vgrajen (zaradi značilnosti gradbene
industrije) dvotirni sistem delavcev: stalnih in začasnih.  Pravice
začasnih delavcev so bolj omejene, ker nimajo dolgoročnega interesa v
firmi.  Delež presežkov, ki pripada začasnim delavcem, bi se, predvidi
Wagner, lahko plačeval prek davkov socialni blagajni.

Ti mehanizmi, od omejevanja potencialnih terjatev s strani delavcev, do
konkurence med posameznimi podružbljenimi obrati, naj bi zagotavljali
ustrezno cenovno dostopnost izgrajenih stanovanj.  Pod Wagnerjevimi
pogoji naj bi bilo zagotovljeno, da bi se gradnja racionalizirala, plače
pa bi ostale dovolj nizko, da ne bi višale najemnin izgrajenih
stanovanj.  Podružbljanje kot ga zagovarja Wagner, pravi Tafuri, je bolj
instrument, zaupan sindikalnemu menedžmentu, da upravlja z delovno silo,
kot pa da upravlja s cilji produkcije na strani delavskega razreda.
[@tafuri1987sozialpolitik, 201] Sklicevanje na bratstvo in delovno etiko
v srednjeveških *Bauhütte* je ideološko dopolnilo temu.

Wagner leta 1919 ustanovi prvi berlinski Soziale Bauhütte. Leta 1920
ustanovi združenje socialnih gradbenih zadrug (*Verband sozialer
Baubetriebe*).  To so praktični koraki k uresničevanju njegove sheme.

Od populizma umetniških avantgard, njihovega generičnega poziva za
poenotenje umetnosti in ljudstva, zmede glede menedžerskih vlog v
delovnem procesu, do Wagnerjevega predpostavke etičnega zavezništva med
tehnično inteligenco in delavci, Tafuri označi za nostalgijo po delu
brez kapitala, ki odgovarja tudi tisti taktiki socialdemokracije, ki se
je izogibala neposredni konfrontaciji z nemškim kapitalom in se zapletla v
ohranjanje buržoazne demokracije.

Po letu 1923, po stabilizaciji ekonomije, se spopad med delom in
kapitalom prenese na spopad med "socialnim" sektorjem, ki ga upravljata
sindikalna konfederacija ADGB in SPD, ter industrijo.

Reforma nemške centralne banke, davčna reforma in vstop ameriškega
kapitala, ki so del tako imenovanega *Dawesovega plana*, da se umiri
inflacijo, krizo okupacije Porurja ter zagotovi nemško sposobnost
plačevanja reparacij okrepi nemške industrijske kartele.  Posredno pa
zagotovi tudi finance za izvajanje weimarske stanovanjske politike.
Pravi temelj weimarske socialne politike je bil namreč nadzor
stanovanjskega trga s programom državnih posojil gradbenim podjetjem in
zadrugam.  Zakon o državnih posojilih neprofitnim gradbenim družbam
(*Gemeinnützige Baugesellschaften*) je sprejel že pruski deželni zbor
leta 1918, tak zakon je bil sprejet tudi na federalni ravni oktobra
istega leta -- torej še pred revolucijo.  Toda do leta 1924 je edina
politika, ki dejansko odgovarja na stanovanjsko vprašanje, omejitev
rasti najemnin v času inflacije.  Šele februarja 1924 nov nepremičninski
davek (*Hauszinssteuer*), ki je ciljal na lastnike nepremičnin, ki jim
je inflacija odplačala dolgove -- zagotovi dovolj sredstev, da se
izvajanje zakona iz leta 1918 lahko začne. 

Pred tem se morajo obstoječe *Bauhütte* nekoliko reorganizirati, saj so
do sredstev stanovanjskega programa upravičene le, če se znebijo
  delavskega razpolaganja z dobičkom.

ADGB ustanovi DEWOG, ki se v stanovanjsko preskrbo vključi kot
demokratičen naročnik, lastnik in upravljavec stanovanj.  Kapital mu
zagotavljajo vključene potrošniške zadruge in sindikati.  Deluje na
državni ravni, ima 11 hčerinskih družb, ki delujejo v posameznih mestih.

Delavsko gibanje, po zakonu o financiranju javnih stanovanj in
reorganizaciji zadružnih struktur, upravlja z dobršnim delom
stanovanjskega sektorja in uživa skoraj monopol.  Na drugi strani, na
primer dobavitelji gradbenega materiala, ostajajo zasebna podjetja.
Pravzaprav vsi sektorji, ki so odvisni od gradbene industrije in
stanovanjske preskrbe -- kot je promet --, ampak niso podružbljeni, se
lahko pospešeno razvijajo na tej podlagi in v veliki meri cene izdelkov
in storitev teh industrij posredno tudi določajo "socialnost"
weimarske stanovanjske politike.

Nov okvir, ki ga vzpostavlja DEWOG -- kompaktno upravljanje in
potegovanje za dozdevno predvidljive in stalne finančne tokove -- organe
delavskega razreda dokončno spremenijo iz političnih organizacij v nabor
storitev.  Prav tako se mora zdaj razviti stroga tehnika izvajanja teh
storitev.  Arhitekti kot so Wagner, Ernst May in Bruno Taut so
poklicani, da izpeljejo to novo tehnično-intelektualno vlogo in v letih
1924--1929 razvijejo najznačilnejši arhitekturni izraz
socialdemokracije.

## Novi Frankfurt

SPD župan Landmann bo leta 1925 poklical arhitekta Ernsta Maya, da
prevzame tehnično-organizacijsko vlogo v novih načrtovalskih strukturah,
ki so jih spodbudile nove možnosti javnega financiranja.

Specifične tehnike, ki jih ti arhitekti razvijejo se ne tičejo zgolj
organizacije gradbenega obrata, organizacije gradbišča, racionalizacije
delovnega procesa gradnje -- kot je to takrat domet Wagnerjevih
predlogov -- ampak se tičejo same standardizacije razporeditev, dimenzij
in zasnove bivalnih prostorov.  Izgradnjo spremljajo publikacije in tudi
filmi o pravilni uporabi stanovanj in opreme, o obnašanju novega
človeka.

Ampak vse te nove tehnike, tako imenovani *Existenzminimum*, poskus
določitve minimalnih uporabnih in sprejemljivih dimenzij stanovanjskih
prostorov, in prefabrikacija se vseeno izkažejo za nezadostna orodja, ko
se cene gradbenih materialov lahko prosto določajo.

Stroški gradnje in oskrbovanja *Praunheima*, kot še kakšnega projekta,
so previsoki, da bi bili dostopni nižjim slojem.  Ob določitvi
minimalnih standardov se mora zagnati tudi program podstandardne
gradnje, da se zagotovi predvidoma začasne kapacitete za tiste, ki jim
standard socialdemokratskih socialnih stanovanj ni dostopen.  V merilu
celotne stanovanjske krize industrializirajoče Nemčije in Frankfurta to
sicer ni znaten pojav, ampak zgolj za demonstracijo protislovja v
socialdemokratskih programih: nove tehnike gradnje privedejo tudi do
nove skupine brezdomnih, to je bivših obrtnikov vezanih na gradbeno
dejavnost, ki so postali tehnološki višek.
[@tafuri1987sphere, 211-213]

Ta *Existenzminimum* bo kasneje kmalu deležen še največ samokritike s
strani modernističnih arhitektov. Ne nujno zato, ker bi bil surovo
določen prenizko. Ampak, ker ni niti finančno zares utemeljen saj
stroški izgradnje ne rastejo linearno ob večanju kvadrature.

Ampak kritika in razočaranje nad eksperimenti razvijejo že tisti, ki so
jo pomagali oblikovati.

Hilberseimer, profesor na Bauhausu, bo napisal teoretsko kritiko taktike
umeščanja naselij na obrobje mest in taktiko izoliranih uresničenih
enklav »demokratične ekonomije«, da naivno obravnavajo »metropolo« kot
nekaj, čemur se enostavno lahko izogneš. Teoretizira o metropoli kot
produktivni sili, ki ne pozna centralizacije ali decentralizacije, ker
je enakomerno zajela ves teritorij. Zanesljivi urbanistični posegi v
takšno realnost so mogoči le skozi centralno planiranje, ki presega
nacionalne meje.[@tafuri1987sphere, 219]

Ta kritika se je potrjevala tudi v realnosti. Siedlungi so brez nenehnih
javnih izdatkov ostajali nerentabilni, generirali pa so razvoj
storitvenega sektorja v starih jedrih, ki je višal dohodke iz rent v
mestih, dodatno centraliziral populacijo in tako dalje, se pravi
ustvarjal stanovanjsko krizo na novo.

Tudi Wagner bo 1931 izstopil iz SPD in spisal dolgo kritiko socialdemokratske
zemljiške politike. Krivdo bo sicer pripisal povsem osebnim napakam
posameznikov. Tudi bo razočaran ugotavljal, da so mestna uprave doslej
bile le »nočni čuvaj potratnemu sistemu«, da je urbanistično načrtovanje
v Nemčiji farsa, da je zaenkrat UN možno le v Sovjetski zvezi, saj nam je
Rusija pokazala, da urbanistično načrtovanje ne more biti nič drugega
kot gospodarsko planiranje.[@tafuri1987sphere, 224-225]

Razpad sistema bo sovpadal s poročilom Ernsta Maya iz Sovjetske zveze.
Skupaj s celotno frankfurtsko ekipo, tako imenovano *Mayevo brigado*, je
bil povabljen za dobo treh let v Sovjetsko zvezo, da pripravi krovne
urbanistične načrte za 20 mest. Do konca prvega leta, na primer, je imel
nalogo izvesti stanovanja, mesto za 700.000 ljudi.

Weimarski arhitekti so na podlagi zapoznele ugotovitve iz lastne
izkušnje cenili nekaj, kar je bilo v Sovjetski zvezi samoumevno:
prostorska politika, načrtovanje prostorskega razvoja ne more biti
osnovano samo na socialnih servisih, ampak zahteva tudi planiranje
industrije, narekovanja produkcije, produkcijskih ciljev.

Še ena prednost sovjetske situacije je nacionalizacija zemlje, weimarske
mestne oblasti so zemljo kupovale od lastnikov (kar je ali vodilo v
slabo prostorsko politiko ali negospodarnost).

## Kontinuitete Rdečega Dunaja

Enotna obravnava Weimarja in Dunaja glede na arhitekturne razlike med
njima se ne zdi neposredno upravičena.  Skupaj jih obravnavamo, ker
imata enotno ideološko podlago, razlike pa izhajajo iz različnih usod
nemških in avstrijskih revolucionarnih gibanj tik po vojni.  Razlike so
torej kontingentne.  Zaključiti z Rdečim Dunajem je pomembno prav tako
zato, ker bolj neposredna kontinuiteta današnje dunajske stanovanjske
politike z obdobjem Rdečega Dunaja informira sodobne mitologije
stanovanjskih socialnih politik.  Skupaj jih obravnavamo, ker lahko tako
dobimo bolj celostno sliko odnosa med tehniko in politiko ter hkrati
naslovimo cel blok socialdemokratskih mitologij.  

Ideološke predpostavke Weimarske republike in Rdečega Dunaja so si na
osnovni ravni zelo podobne.  Pri obojih se tekom dogajanj po prvi
svetovni vojni kažejo kot zadržek do boljševizma in znotraj tega zadržka
kot previdno manevriranje znotraj buržoazne demokracije.  Prav tako si
delijo zmedo glede menedžerskih in delavskih vlog v svetih, socializem
razumejo kot delovno etiko [povzemamo po @tafuri1971austromarxismo].

Razlika je v situaciji v kateri se znajdejo SPÖ oziroma Dunaj v razmerju
do ostale države in Nemčije.

Dunaj kot mesto je bilo ob koncu vojne odrezano od zaledja, do katerega
je sploh razvil svojo industrijsko funkcijo.  Že v času imperija je
velik del funkcij Dunaja bil simbolen, reprezentančen.  Z izgubo zaledja
in z ustanovitvijo republike Dunaj postane skoraj dvojno nefukcionalen.
Upanje avstromarksistov, da pride do združitve Avstrije in Nemčije se ne
uresniči in tako ostane mesto izolirano znotraj lastne države, odrezano
od mreže socialdemokratskih lokalnih uprav v Nemčiji.

Zanimivo je kako se takšna izolacija kaže v tehnikah, ki stanovanjski
arhitekturi Rdečega Dunaja dajejo izraz.  Vojna zajame Dunaj ravno v
času obsežne špekulativne prenove in širitve mesta ob sprostitvi
zemljišč, ki jih je nekoč zasedalo obzidje.  Pri strukturi brezposelnih
po vojni gre tako predvsem za gradbene delavce.  Stanovanjski program
Rdečega Dunaja zato načrtno zavira tehnološki napredek gradbene
industrije in vpliva na arhitekturni stil, tudi v etične namene, tako,
da ta zaposli čim večji del manualnega dela.  Ob istih ideoloških
predpostavkah klasične socialdemokracije tako dobimo arhitekturno
alternativo visokemu modernizmu Weimarja.  Tipologija, ki jo razvije
Rdeči Dunaj je tako dokaj regresivna, gre za obodne stavbne bloke, masivne konstrukcije iz opeke.  

Zaradi izolacije in zapuščine predvojne špekulacije je tehnična
inteligenca, ki je na voljo, da prevzame tehnični vidik naloge
socialdemokratskega programa, sestavljena predvsem iz konzervativnejših
arhitektov, ki projekte obravnavajo kot formalni problem.  Ker SPÖ tako
ali tako ne more ponuditi konkretnejših rezultatov kot simbolnih
priznanj, sodelovanje med nemodernističnimi arhitekti in simbolno
politiko samoupravljanja reprodukcije delavcev privede do nekakšnih
ekspresivnih delavskih palač, ki pa, kot stavbni bloki, ostajajo
izolirani od mesta, kot otoki realiziranih regresivnih utopij [tehnično
regresivnost Dunajskega stanovanjskega modela kritizira tudi Martin
Wagner na gradbeni konferenci na Dunaju, njegovo poročilo je v
@tafuri1980vienna, 228-231].

Preoblikovanje SPÖ v sistem storitev se v primerjavi z weimarsko SPD zdi
bolj izsiljena pozicija.

\*\*\*

Kar je nenazadnje kritika površnih sklicevanj na tradicije
socialdemokratskega upravljanja in načrtovanja mest ne more ponuditi
zaključka, ki bi bil politično neposredno uporaben.  Lahko pa v
razmislek izpostavi točke kjer je primerjava sodobnih reformističnih
prizadevanj na področju mesta s to tradicijo zdaj smiselna.

Najprej je treba omeniti, da *osnovna* oblika javne stanovanjske
preskrbe, ki prevladuje kot legalna in legitimna politična možnost v
sodobnih evropskih liberalnih demokracijah -- naj se ta izvaja kot
javna subvencija na mestu potrošnje, ali kot javno financiranje gradnje --
ne predstavlja kvalitativne razlike s politično ali tehnično inovacijo
socialdemokratskega upravljanja mest.  Tako sta ji skupni tudi
protislovja, ki izhajajo iz omejevanja razrednega interesa na socialno
politiko ob nedotaknjenih razmerjih v produkciji.  Finančna nevzdržnost
socialnih programov ob nedotaknjenem trgu vodi v postopno prelaganje
stroškov na potencialne upravičence -- tako je na primer zgodovina dunajske
stanovanjske politike po drugi svetovni vojni zgodovina uvajanja
instrumentov financiranja iz lastnih sredstev upravičencev ali
aktivacije zasebnega kapitala, prav tako pa v splošnem ta trend viden v
redefiniciji neprofitne najemnine v stroškovno najemnino, ki naj pokrije
storitev gradbenika kot kapitalističnega obrata.

Ne moremo enostavno destilirati pozitivne zapuščine nemške in avstrijske
klasične socialdemokracije.  Ne moremo pa mimo dejstva, da rezultati
stanovanjskih in arhitekturnih programov klasične socialdemokracije v
primerjavi z današnjimi skromnimi razdelavami socialnih programov
predstavljajo razumen bazen mitologij za sodobno levico.  Kompleksno
součinkovanje uradnega delavskega gibanja med drugo in tretjo
internacionalo, ki je imelo konkretno povezavo z zahtevami ene strate
delavskega razreda in tehnične inteligence, in objektivnimi zahtevami
nemškega kolektivnega kapitala je rezultiralo v "sistemu kompromisov",
kjer vsaj nekakšno delovanje je bilo mogoče.

Temeljna razlika med stanovanjskim vprašanjem med vojnama in tekom
povojne obnove ter danes je, da se je stanovanjsko vprašanje med vojnama
in po drugi svetovni vojni izražalo predvsem kot stanovanjski
primanjkljaj.  Danes ta primanjkljaj ni tako izrazit.  Če je
socialdemokratski stanovanjski program propadal na izogibanju poseganja
v razmerja v industriji in zemljiški lastnini, so danes, ob manku
objektivnih teženj kapitala po množični stanovanjski gradnji, toliko
pomembnejša.  Nekoliko cinično^[a vendar gre za klasični nauk, gl\.
Engels, "O stanovanjskem vprašanju"] bi lahko sodobno enostavno
sklicevanje na zapuščino klasične socialdemokracije stanovanjskem
vprašanju odsvetovali na podlagi tega, da stanovanjskega vprašanje ne
odpre niti "na staro", kaj šele na novo.

---
# vim: spelllang=sl spell
---

