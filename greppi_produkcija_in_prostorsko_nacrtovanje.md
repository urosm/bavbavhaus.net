---
title: "Claudio Greppi in Alberto Pedrolli, Produkcija in načrtovanje prostora"
...

<!-- 1\. Alcuni recenti sviluppi della pianificazione territoriale in
Italia meritano di essere analizzati per documentare le tendenze del
capitale nel campo delle localizzazioni (produttive e residenziali) e
dei collegamenti (trasporti). La fase che attraversiamo sembra essere
quella dei preparativi per l'assestamento dello sviluppo capitalistico
nel territorio, cioè per l'integrazione fabbrica-territorio. -->

> 1\. Nekateri nedavni razvoji prostorskega načrtovanja v Italiji
> terjajo analizo, da bi dokumentirali težnje kapitala na polju
> prostorskih razporeditev (produkcijskih ter rezidenčnih) ter povezav
> (promet). Zdi se, da smo sredi pripravljalne faze utrjevanja
> kapitalističnega razvoja na ozemlju, torej sredi integracije
> tovarne-ozemlja.

<!-- Questi preparativi sì svolgono principalmente in due direzioni: a)
perfezionamento del meccanismo di pianificazione; b) ricerca della
disponibilità delle aree. -->

> Te priprave se odvijajo predvsem v dveh smereh: a) v izpopolnjevanje
> mehanizmov načrtovanja; b) v iskanje razpoložljivih zemljišč.

<!-- La cultura e la tecnica urbanistica subiscono contemporaneamente
una profonda trasformazione adeguandosi di fatto al grado di sviluppo
del capitalismo, cioè alle possibilità contingenti, storiche, di un suo
intervento per l'organizzazione del territorio. -->

> Kultura in urbanistična tehnika trpita (hkrati) pod temeljitim
> preoblikovanjem in prilagajanjem na dejstvo stopnje kapitalističnega
> razvoja, torej na zgodovinsko pogojene možnosti njegovega poseganja v
> organizacijo ozemlja. 

<!-- Dalla fase in cui l'urbanistica è utilizzata come strumento
puramente negativo di frantumazione della classe operaia, si passa alla
fase in cui l'urbanistica e la pianificazione territoriale in genere
diventano tecniche per l'integrazione del territorio nel piano del
capitale. -->

> Od faze, v kateri se urbanizem uporablja kot čisto negativen
> instrument za razbijanje delavskega razreda, preidemo v fazo, v kateri
> urbanizem in ozemeljsko načrtovanje na splošno postaneta tehniki za
> integracijo ozemlja v plan kapitala.

<!-- Nel primo caso, mentre rimangono inascoltati il grandi progetti di
sistemazione globale della città perché estranei al grado di sviluppo
delle forze produttive del capitale, incontrano vivo successo i miti
della "città-giardino", dell'abitazione individuale, del "vicinato",
della comunità di quartiere. L'urbanistica è nata infatti col preciso
compito di rendere più facile la repressione delle tendenze
rivoluzionarie, da quando il barone Haussmann trasformò la struttura
medioevale di Parigi — troppo barricadiera — in un complesso funzionale
alle esigenze di manovra di un grosso esercito poliziesco, fino alla
odierna impostazione del problema della casa in funzione disgregatrice
dei quartieri operai ormai ridotti ad agglomerati semirurali (si veda ad
esempio l'involuzione subita dal concetto di quartiere dal primo
razionalismo fino all'INA-Casa). -->

> V prvem primeru, medtem ko veliki projekti globalne ureditve mesta
> ostajajo neuslišani, ker so tuji stopnji razvoja produktivnih sil
> kapitala, so miti o "vrtnem mestu", individualni stanovanjski gradnji,
> "soseski", sosedski skupnosti doživeli velik uspeh. Dejansko se je
> urbanizem rodil prav z nalogo, da olajša zatiranje revolucionarnih
> teženj, od časa, ko je baron Haussmann srednjeveško strukturo Pariza
> -- preveč zabarikadirano -- preoblikoval v kompleks, funkcionalen za
> potrebe manevriranja velike policijske vojske, do današnjega pristopa
> k stanovanjskemu problemu kot razdiralni funkciji delavskih sosesk, ki
> so se zreducirale na polkmečke aglomeracije (glej na primer involucijo
> koncepta soseske od zgodnjega racionalizma do projekta INA-Casa).

<!-- Nel secondo caso, cioè nella fase del capitalista collettivo, la
pianificazione territoriale si rende necessaria per risolvere i rapporti
tra città e territorio, tra industria e agricoltura, o almeno per
equilibrare le disfunzioni interne al processo di circolazione. Cosi al
concetto ormai sorpassato di sviluppo urbano per nuclei satelliti
autosufficienti, ancorato a una visione statica, tradizionale dei
rapporti città-campagna, si sostituisce il concetto di *città-regione*
come struttura comprensoriale che organizza la totalità del territorio
per una sua maggiore funzionalità produttiva. Dietro questa concezione
rimane sempre, come prima, il tentativo di integrare la forza-lavoro nel
piano di sviluppo, questa volta non solo attraverso la repressione ma
anche attraverso i canali delle istituzioni democratiche e delle
"battaglie" dei partiti di massa. -->

> V drugem primeru, tj\. v fazi kolektivnega kapitalizma, postane
> teritorialno načrtovanje nujno za rešitev razmerij med mestom in
> ozemljem, med industrijo in kmetijstvom ali vsaj za uravnoteženje
> notranjih disfunkcij procesa cirkulacije. Tako je zastareli koncept
> razvoja mest s samozadostnimi satelitskimi središči, zasidran v
> statični, tradicionalni viziji odnosov med mestom in podeželjem,
> zamenjal koncept *mestne regije* kot okrožne strukture, ki organizira
> celotno ozemlje organizira bolj produkcijsko funkcionalno. V ozadju te
> zasnove ostaja, tako kot prej, poskus vključevanja delovne sile v
> razvojni plan, tokrat ne le z represijo, temveč tudi prek
> demokratičnih institucij in "bitk" množičnih strank.

<!-- L'insieme infrastrutturale che collega l'intero comprensorio e i
comprensori tra loro, razionalizza i rapporti delle forze produttive in
modo analogo alle tecniche di organizzazione aziendale. Al pari di
queste anche la tecnica urbanistica segue il destino che il capitale le
prepara, quando la utilizza e quando no, quando ne fa un pilastro
dell'interesse generale e quando opera tranquillamente le proprie scelte
scavalcando i piani e scandalizzando gli urbanisti. -->

> Infrastrukturni sklop, ki povezuje celotno okrožje in okrožja,
> racionalizira razmerja med produktivnimi silami na podoben način kot
> tehnike organizacije podjetij. Tako kot te tehnike tudi tehnike
> urbanističnega načrtovanja sledijo usodi, ki jim jo pripravlja
> kapital, ko jih uporablja in ko ne, ko jih naredi za steber splošnega
> interesa in ko se mirno odloča po svoje, mimo načrtov in s škandalom
> nad urbanisti.

<!-- Solo considerando la "città-regione" come proiezione del sistema di
fabbrica sull'intero territorio è possibile rovesciarne la visione
tecnicistica di forma spaziale aperta, dinamica, esigenza oggettiva di
una società progredita e "libera dalle tecniche alienanti della società
classista" (come scrivono alcuni giovani teorici). Del sistema di
fabbrica la "città-regione" conserva tutta la sostanza burocratica, che
vanifica sistematicamente i tentativi di razionalizzazione del
territorio: è bene tener presente che i piani risultano sempre sfasati
rispetto alla mobilità crescente imposta dal capitale a tutti i fattori
della produzione; ed è anche per questo che accanto al tecnicismo, forma
mistificata della scienza che prescinde dalla struttura di classe della
società capitalistica, troviamo sempre l'utopia, l'incapacità operativa:
sono questi i due poli entro cui si svolge attualmente l'attività
critica e operativa degli architetti-urbanisti. -->

> Le z obravnavo "mestne regije" kot projekcije tovarniškega sistema na
> celotno ozemlje je mogoče ovreči njeno tehnicistično vizijo odprte,
> dinamične prostorske oblike, ki je objektivna zahteva napredne družbe,
> "osvobojene od odtujitvenih tehnik razredne družbe" (kot pišejo
> nekateri mladi teoretiki). "Mestna regija" ohranja vse birokratske
> značilnosti tovarniškega sistema, ki sistematično onemogoča poskuse
> racionalizacije ozemlja: Dobro se je zavedati, da so načrti vedno
> izven faze naraščajoče mobilnosti, ki jo kapital vsiljuje vsem
> proizvodnim dejavnikom, in tudi zato poleg tehnike, mistificirane
> oblike znanosti, ki ne upošteva razredne strukture kapitalistične
> družbe, vedno najdemo utopijo, operativno nesposobnost: to sta dva
> pola, znotraj katerih se trenutno izvaja kritična in operativna
> dejavnost arhitektov-urbanistov.

<!-- 2\. La prima operazione che si sta compiendo in questa fase di
preparazione è la ricerca della disponibilità di aree. Mentre per le
localizzazioni industriali il problema è facilmente risolvibile, per
l'abitazione occorrono al capitale strumenti giuridici nuovi. Infatti
nel primo caso sì osserva che una industria può ancora spostarsi come
vuole trovando sempre terreno a buon mercato se non gratuito e
facilitazioni di ogni genere da parte delle pubbliche amministrazioni. È
cosi che si realizza il decentramento, in verità senza grandi sacrifici.
Non è il problema del costo delle aree che indirizza l'attenzione del
capitale verso questo o quel terreno, ma tutto un altro tipo di
considerazioni che rientrano nei criteri di pianificazione del
territorio. -->

> 2\. Prvi postopek, ki se izvaja v tej fazi priprave, je iskanje
> razpoložljivih zemljišč. Medtem ko je za industrijske lokacije ta
> problem enostavno rešljiv, pa za stanovanjsko gradnjo kapital
> potrebuje nove pravne instrumente. V prvem primeru je dejansko mogoče
> opaziti, da se lahko industrija še vedno giblje po svojih željah in
> vedno najde poceni, če ne celo brezplačno, zemljišče in vse vrste
> olajšav s strani javne uprave. Tako se doseže decentralizacija, v
> resnici brez velikih žrtev. Ne gre za problem cene zemljišča, ki
> usmerja pozornost kapitala na to ali ono zemljišče, temveč za vrsto
> drugih vidikov, ki spadajo v merila teritorialnega načrtovanja.

<!-- Invece le abitazioni, in particolare le abitazioni operaie, sono
ancora localizzate dove possono, cioè dove trovano condizioni di mercato
che lo permettono. Questa situazione rispecchia la presente fase di
integrazione tra città e campagna, caratterizzata dall'afflusso
giornaliero di masse operaie dalla provincia nelle città e
dall'elefantiasi di tutto il sistema dei trasporti. Il costo dei terreni
edificabili nelle città è una delle cause più dirette della mancanza di
coordinamento delle localizzazioni. Avrà quindi notevoli conseguenze
l'applicazione della nuova legge "per la formazione di piani per
l'edilizia economica e popolare" (n. 167, aprile 1962). Questa si
propone di bloccare le aree di espansione delle maggiori città per
consentire una loro utilizzazione (sia da parte di privati che di enti
pubblici) a costi non eccessivi. Anche se non tutte le intenzioni della
legge appaiono realizzabili (in particolare la possibilità per i comuni
di espropriare il 50 % di queste aree al prezzo di mercato di due anni
fa) la legge è interessante perché esprime due preoccupazioni: a) quella
di agevolare l'attuale sviluppo fondato sulla piccola industria
edilizia; b) quella di preparare il terreno al futuro intervento su
larga scala dell'industria edilizia più evoluta. -->

> Po drugi strani pa se stanovanja, zlasti delavska, še vedno nameščajo,
> kjer je to mogoče, tj. kjer to omogočajo tržni pogoji. Ta položaj
> odraža sedanjo fazo povezovanja med mestom in podeželjem, za katero
> sta značilna vsakodnevni pritok množic delavcev iz provinc v mesta in
> elefantiaza celotnega prometnega sistema. Stroški stavbnih zemljišč v
> mestih so eden od najbolj neposrednih vzrokov za neusklajenost
> lokacij. Uporaba novega zakona "o oblikovanju načrtov za gospodarska
> in ljudska stanovanja" (št. 167, april 1962) bo zato imela precejšnje
> posledice. Cilj tega zakona je zamrzniti območja širitve večjih mest,
> da bi jih lahko uporabljali (zasebni in javni organi) po razumnih
> cenah. Čeprav se vsi nameni zakona ne zdijo izvedljivi (zlasti
> možnost, da občine razlastijo 50 % teh površin po tržni ceni izpred
> dveh let), je zakon zanimiv, ker izraža dva interesa: a) olajšati
> sedanji razvoj, ki temelji na mali gradbeni industriji; b) pripraviti
> teren za prihodnje obsežne posege bolj razvite gradbene industrije.

<!-- Metà delle aree di espansione della città dovrebbero essere
espropriate dal Comune, per essere ricedute a basso prezzo e dotate dei
servizi di urbanizzazione alle piccole imprese edilizie; l'altra metà è
riservata all'intervento degli enti pubblici. Dal momento che questi
coprono attualmente il 7 % appena dell'industria edilizia, è chiaro che
queste aree sono per ora accantonate in vista della trasformazione
tecnologica dell'industria edilizia. -->

> Polovico območij širitve mesta naj bi občina razlastila, jih po nizki
> ceni predala malim gradbenim podjetjem in jih opremila z
> urbanističnimi storitvami, druga polovica pa je rezervirana za
> posredovanje javnih organov. Ker trenutno pokrivajo le 7 % gradbene
> industrije, je jasno, da so ta področja za zdaj rezervirana za
> tehnološko preoblikovanje gradbene industrije.

<!-- Questa è una delle vie che il capitalismo italiano sta seguendo per
assicurarsi la disponibilità delle aree urbane: essa è al tempo stesso
una premessa per la nuova fase di integrazione tra industria e
agricoltura fondata sul controllo globale del territorio e di tutte le
localizzazioni (tanto dell'industria che della forza-lavoro). -->

> To je eden od načinov, s katerimi italijanski kapitalizem zagotavlja
> razpoložljivost mestnih območij, hkrati pa je to pogoj za novo fazo
> povezovanja med industrijo in kmetijstvom, ki temelji na globalnem
> nadzoru ozemlja in vseh lokacij (tako industrije kot delovne sile).

<!-- È da notare che la legge n\. 167 non è frutto di "battaglie
democratiche", ma dell'iniziativa diretta del ministro Sullo: è giunta
inaspettatamente, e in un primo tempo nessuno ne capiva il significato.
Anche per questo fatto si può interpretarla come esigenza della grande
industria edilizia, senza neppure la mediazione di una politica
riformista. Del resto la disponibilità di aree edificabili non avrebbe
senso se non fosse seguita da un grosso salto tecnologico nella
produzione delle abitazioni: questo sembra possibile oggi, data la
grande disponibilità di capitale fornita dalla nazionalizzazione
dell'industria elettrica e data anche la necessità di fronteggiare
l'intervento massiccio dei metodi di prefabbricazione francesi (già
introdotti, per esempio, a Milano per il piano di edilizia popolare).
-->

> Opozoriti je treba, da zakon št\. 167 ni bil rezultat "demokratičnih
> bitk", temveč neposredne pobude ministra Sulla: prišel je
> nepričakovano in sprva nihče ni razumel njegovega pomena. To je še en
> razlog, zakaj ga je mogoče razlagati kot zahtevo velike gradbene
> industrije brez posredovanja reformistične politike. Po drugi strani
> pa razpoložljivost gradbenih površin ne bi bila smiselna, če ji ne bi
> sledil velik tehnološki preskok v proizvodnji stanovanj: to se zdi
> danes mogoče zaradi velike razpoložljivosti kapitala, ki ga je
> zagotovila nacionalizacija elektroenergetske industrije, in tudi
> zaradi potrebe po obvladovanju obsežnega posega francoskih montažnih
> metod (ki so jih na primer že uvedli v Milanu v okviru načrta za
> socialna stanovanja).

<!-- 3\. Contemporaneamente all'esigenza di assicurarsi la disponibilità
delle aree urbane per la grande industria edilizia, il capitale sembra
essersi posta quella di configurare un nuovo meccanismo di
pianificazione territoriale. Il livello di pianificazione più importante
diviene quello *comprensoriale*, cioè quello intermedio tra il piano
regionale e il piano regolatore comunale. Nella legislazione attuale
esiste solo il piano intercomunale, reso assai difficoltoso
nell'attuazione dal necessario consenso di tutti i Comuni interessati.
Nella nuova legge urbanistica (elaborata dal ministro Sullo e
successivamente presentata alla Camera dal gruppo comunista) si affronta
invece decisamente il problema dei comprensori, la cui dimensione non è
più legata alle unità amministrative e può efficacemente seguire le
esigenze di assestamento dello sviluppo produttivo. Il comprensorio è lo
strumento più interessante proposto dalla nuova legge, la quale per il
resto (rapporti con la programmazione economica, disciplina
dell'esproprio, ecc.) non è altro che l'adeguamento della legislazione
italiana a quella degli altri paesi capitalistici. Anche se l'iniziativa
del gruppo parlamentare comunista ritarderà sicuramente l'approvazione
della legge Sullo (ora Sullo-Natoli), l'ipotesi di una suddivisione di
tutto il territorio nazionale in comprensori non è avveniristica, perché
di essa il capitale tiene già conto oggi quando affronta il problema
delle nuove localizzazioni. -->

> 3\. Zdi se, da si je kapital hkrati s potrebo po zagotavljanju
> razpoložljivosti mestnih območij za veliko gradbeno industrijo zadal
> nalogo oblikovati nov mehanizem teritorialnega načrtovanja.
> Najpomembnejša raven načrtovanja je raven *okrožja*, tj. vmesna raven
> med regionalnim načrtom in občinskim prostorskim načrtom. V veljavni
> zakonodaji je na voljo le medobčinski načrt, ki ga je zelo težko
> izvajati zaradi potrebnega soglasja vseh zadevnih občin. Novi zakon o
> prostorskem načrtovanju (ki ga je pripravil minister Sullo, nato pa ga
> je poslanski zbornici predstavila komunistična skupina) pa odločno
> rešuje problem okrožij, katerih velikost ni več vezana na upravne
> enote in ki lahko učinkovito sledijo potrebam proizvodnega razvoja.
> Okrožje je najzanimivejši instrument, ki ga predlaga novi zakon, pri
> ostalih (odnosi z gospodarskim načrtovanjem, predpisi o razlastitvi
> itd.) pa gre zgolj za prilagajanje italijanske zakonodaje zakonodaji
> drugih kapitalističnih držav. Čeprav bo pobuda komunistične poslanske
> skupine zagotovo odložila sprejetje zakona Sullo (zdaj Sullo-Natoli),
> hipoteza o razdelitvi celotnega nacionalnega ozemlja na okrožja ni
> futuristična, saj jo kapital že danes upošteva pri reševanju problema
> novih lokalizacij.

<!-- La tecnica soccorre infatti questa tendenza del capitale con tutta
l'ideologia della "città-regione", che trasforma in obiettivi
democratici l'assestamento del capitale nel territorio e la soluzione
razionale dei problemi delle abitazioni e dei trasporti. D'altra parte è
lo sviluppo stesso del capitale che esclude la possibilità di un
assestamento definitivo e di una soluzione razionale: per cui forse
l'unica soluzione perfetta da un punto di vista capitalistico è quella
delle *caravan-towns* americane, con le abitazioni operaie montate su
ruote e disponibili in ogni momento in ogni punto del territorio. Al
contrario sembra destinato al fallimento qualsiasi tentativo di
soluzione "statica" dei rapporti industria-abitazione, come quello delle
*news-towns* inglesi, dove l'integrazione fabbrica-territorio si è
rivelata illusoria e il problema dei trasporti si è riproposto daccapo
nei medesimi termini. -->

> Tehnologija to težnjo kapitala podpira s celotno ideologijo "mestne
> regije", ki naselitev kapitala na ozemlju ter racionalno reševanje
> stanovanjskih in prometnih problemov spreminja v demokratične cilje.
> Po drugi strani pa je razvoj kapitala tisti, ki izključuje možnost
> dokončne naselitve in racionalne rešitve, zato je morda edina popolna
> rešitev s kapitalističnega vidika rešitev ameriških *caravan-towns* z
> delavskimi stanovanji na kolesih, ki so kadar koli na voljo na katerem
> koli delu ozemlja. Nasprotno se zdi, da je vsak poskus "statične"
> rešitve razmerja med industrijo in stanovanji obsojen na neuspeh, kot
> na primer v angleških *news-towns,* kjer se je povezovanje tovarne in
> ozemlja izkazalo za iluzorno in se je prometni problem ponovno pojavil
> pod istimi pogoji.

<!-- Analogamente sembrerebbe destinato al fallimento in Italia un piano
di integrazione fabbrica-territorio (fondato sul decentramento
industriale e sulle industrie-filtro nelle zone agricole), se ad esso
non corrispondesse un meccanismo di controllo dei serbatoi di
forza-lavoro in tutto il territorio, accanto all'eliminazione
dell'antagonismo tra città e campagna e allo sviluppo capitalistico
dell'agricoltura. -->

> Podobno se zdi, da bi bil načrt integracije tovarn in ozemelj (ki bi
> temeljil na industrijski decentralizaciji in filtrirni industriji na
> kmetijskih območjih) v Italiji obsojen na neuspeh, če ne bi ustrezal
> mehanizmu za nadzor virov delovne sile na celotnem ozemlju, hkrati z
> odpravo antagonizma med mestom in podeželjem ter kapitalističnim
> razvojem kmetijstva.

<!-- La pianificazione territoriale viene in aiuto offrendo, attraverso
il dimensionamento dei comprensori, la possibilità di determinare i
successivi tempi di sviluppo delle localizzazioni industriali, in
rapporto alle riserve di forza-lavoro, e lo sviluppo dell'agricoltura
fondato sul ciclo produttivo della azienda capitalistica. -->

> Prostorsko načrtovanje priskoči na pomoč tako, da z določitvijo
> velikosti okrožij omogoči, da se določi nadaljnji čas razvoja
> industrijskih lokacij glede na rezerve delovne sile in razvoj
> kmetijstva na podlagi proizvodnega cikla kapitalističnega podjetja.

<!-- La dimensione dei comprensori è importante per due ragioni: prima
di tutto perché è funzionale all'attuale livello di sviluppo; la regione
è troppo grande e il Comune è troppo piccolo, e tutti e due
corrispondono soltanto a una geografia amministrativa e non produttiva.
Attraverso il comprensorio si cerca quindi di superare 1 pericoli di una
rigidità eccessiva del meccanismo di pianificazione, consentendo un
certo margine di autonomia ai livelli più bassi di pianificazione
(comunale) nel quadro di un controllo diretto del territorio da un punto
di vista più ravvicinato di quello regionale. -->

> Velikost okrožij je pomembna iz dveh razlogov: prvič, ker je
> funkcionalna glede na sedanjo raven razvoja; regija je prevelika,
> občina pa premajhna, obe pa ustrezata le upravni in ne proizvodni
> geografiji. Z okrožjem se torej poskuša premagati nevarnost prevelike
> togosti mehanizma načrtovanja, pri čemer se najnižjim ravnem
> načrtovanja (občinskim) omogoči določena mera avtonomije v okviru
> neposrednega nadzora ozemlja z vidika, ki je bližji regionalnemu.

<!-- In secondo luogo la dimensione può essere direttamente dettata dal
raggio di influenza di un'industria pilota o dell'azienda agricola
capitalistica. Gli esempi si possono trovare facilmente nel caso dei
poli di sviluppo del sud e delle aziende zootecniche il cui ciclo
produttivo si estende dalle zone di pascolo alle industrie di
trasformazione. -->

> Drugič, velikost je lahko neposredno odvisna od polmera vpliva pilotne
> industrije ali kapitalistične kmetije. Primere lahko zlahka najdemo v
> primeru južnih razvojnih poljan in živinorejskih kmetij, katerih
> proizvodni cikel se razteza od pašnikov do predelovalne industrije.

<!-- In questi casi il rapporto tra piano territoriale e piano economico
è immediato. Già adesso certi tentativi di pianificazione intercomunale
in zone prevalentemente agricole (per esempio il piano del Trentino)
hanno il carattere di piani economici veri e propri, anche se non ancora
del tutto operativi. -->

> V teh primerih je povezava med prostorskim in gospodarskim načrtom
> neposredna. Nekateri poskusi medobčinskega načrtovanja na pretežno
> kmetijskih območjih (npr\. načrt Trentino) imajo že danes značaj
> pravih gospodarskih načrtov, čeprav še ne delujejo v celoti.

<!-- Ma anche nel caso di piani puramente territoriali si ritrovano le
caratteristiche attuali e future dell'assestamento geografico del
capitale, e quindi indirettamente alcune linee di tendenza che servono a
chiarire — in particolare — i nuovi rapporti tra industria e agricoltura
e tra abitazione e produzione. -->

> Toda tudi v primeru povsem teritorialnih načrtov je mogoče najti
> sedanje in prihodnje značilnosti geografske naselitve kapitala in s
> tem posredno nekatere smernice, ki služijo za pojasnitev -- zlasti --
> novih razmerij med industrijo in kmetijstvom ter med stanovanji in
> proizvodnjo.

<!-- 4\. Dalle osservazioni fatte derivano alcune possibilità di lavoro.
Prima di tutto la conoscenza dei metodi di integrazione del territorio
da parte del capitale allarga il campo di verifica delle ipotesi di
sviluppo formulate a livello dell'analisi di fabbrica. In questo caso si
tratta di togliere di mezzo i miti tecnicistici (la "città-regione") che
presentano la fabbrica puramente come fatto oggettivo su cui gravita un
certo numero di lavoratori e che provoca una certa quantità di traffico.
L'analisi non può partire dalla geografia per risalire al capitale, al
contrario è solo attraverso l'analisi della produzione che si possono
comprendere le cause di certe trasformazioni dell'assetto territoriale;
il problema allora non è più quello di intervenire "democraticamente"
nelle trasformazioni (cioè al livello della circolazione o del consumo):
queste perdono il carattere di "obiettivi" autonomi, mentre restano
strumenti teorici dell'azione politica che contrappone la classe operaia
al capitale al livello della produzione. -->

> 4\. Iz zgornjih ugotovitev izhajajo številne delovne možnosti.
> Poznavanje metod vključevanja ozemlja s kapitalom najprej razširi
> polje preverjanja razvojnih hipotez, oblikovanih na ravni tovarniške
> analize. V tem primeru se je treba znebiti tehničnih mitov ("mestna
> regija"), ki predstavljajo tovarno zgolj kot objektivno dejstvo, na
> katerem se zadržuje določeno število delavcev in ki povzroča določen
> promet. Nasprotno, le z analizo proizvodnje je mogoče razumeti vzroke
> za določene transformacije teritorialnega reda; problem ni več
> "demokratično" poseganje v transformacije (tj\. na ravni obtoka ali
> potrošnje): te izgubijo svoj značaj avtonomnih "ciljev" in ostanejo
> teoretični instrumenti političnega delovanja, ki na ravni proizvodnje
> nasprotujejo delavskemu razredu in kapitalu.

<!-- Il che non esclude che considerando il territorio come proiezione
della fabbrica non si ritrovino alcune possibilità reali di intervento,
alcune scelte alternative. Per esempio, mentre è del tutto illusorio il
"contenuto antimonopolistico" di un piano territoriale (a meno che non
sì voglia scoprirlo nella capacità di dare impulso allo sviluppo
produttivo), la soluzione pratica di certi problemi può essere
utilizzata per assorbire o al contrario per stimolare l'azione di
classe. Per esempio non è affatto indifferente che le case per i
braccianti vengano costruite sui poderi, o in piccoli nuclei, oppure in
vaste concentrazioni operaie: nel primo caso — che si presenta per la
quasi totalità dei braccianti toscani ex-mezzadri — il padrone mantiene
un controllo diretto dell'operaio singolo e una possibilità illimitata
di super-sfruttamento (produzione di plusvalore assoluto); nel secondo
(il caso dei villaggi rurali tanto cari alle ideologie cattoliche) si
mantiene una serie di valori e di miti che ostacola decisamente lo
sforzo di ricomposizione unitaria della classe operaia agricola; questo
sforzo trova invece un impulso dalla rivendicazione di condizioni di
abitazione pari a quelle operaie, perché attraverso di essa si manifesta
una volontà di autonomia e di deciso distacco dalla condizione del
contadino. La disgregazione della classe operaia agricola, infatti, è
legata alle preesistenze contadine una delle quali è l'abitazione
(isolata o nel villaggio rurale). -->

> To pa ne izključuje dejstva, da obstajajo nekatere realne možnosti za
> poseg, nekatere alternativne izbire, če upoštevamo ozemlje kot
> projekcijo tovarne. Medtem ko je na primer "antimonopolna vsebina"
> teritorialnega načrta povsem iluzorna (razen če jo želimo odkriti v
> zmožnosti dajanja spodbude proizvodnemu razvoju), lahko praktično
> reševanje nekaterih problemov služi za absorbiranje ali, nasprotno, za
> spodbujanje razrednega delovanja. Na primer, nikakor ni nepomembno,
> ali so hiše za delavce zgrajene na kmetijah, v majhnih jedrih ali v
> velikih koncentracijah delavcev: v prvem primeru -- ki je značilen za
> skoraj vse toskanske nekdanje najemniške delavce -- gospodar ohrani
> neposreden nadzor nad posameznim delavcem in neomejeno možnost
> superizkoriščanja (proizvodnja absolutne presežne vrednosti); V drugem
> primeru (podeželske vasi, ki so bile tako drage katoliški ideologiji)
> se je ohranila vrsta vrednot in mitov, ki so odločilno ovirali
> prizadevanja za ponovno sestavo kmečkega delavskega razreda kot enote;
> ta prizadevanja je po drugi strani spodbujala zahteva po stanovanjskih
> pogojih, enakih tistim, ki jih imajo delavci, saj se je prek nje
> kazala želja po samostojnosti in odločilni ločitvi od kmečkega stanja.
> Razpad kmetijskega delavskega razreda je dejansko povezan s kmečkimi
> predejavnostmi, med katerimi je stanovanje (osamljeno ali v podeželski
> vasi).

<!-- Un parallelo con la situazione operaia nell'industria non è
pensabile; nessun industriale nemmeno ai tempi di Engels ha mai preteso
di tenere gli operai a dormire con le loro famiglie dentro la fabbrica,
cosa che invece ha un senso nel caso dell'azienda agricola. Però anche
per le abitazioni operaie esiste la possibilità di scegliere una
tipologia edilizia piuttosto che un'altra: oggi si può riconoscere che
gli unici esperimenti di urbanistica veramente rivoluzionari sono stati
quelli dei quartieri-fortezza per gli operai costruiti sulla scia del
movimento rivoluzionario europeo degli anni venti. Alle cannonate che
piegarono la resistenza operaia nel Karl Marx Hof di Vienna la borghesia
sostituisce in seguito le armi della penetrazione di valori privatistici
piccolo borghesi e la disgregazione delle abitazioni operaie. La
risposta non può essere cercata altro che nella riaffermazione della
abitazione collettiva come strumento di resistenza operaia contro la
disgregazione.-->

> Vzporednica s položajem delavcev v industriji je nepredstavljiva;
> noben industrialec, niti v Engelsovem času, ni nikoli trdil, da
> delavci spijo s svojimi družinami v tovarni, kar je smiselno v primeru
> kmetije. Toda tudi pri delavskih stanovanjih obstaja možnost izbire
> ene vrste stavbe pred drugo: danes lahko ugotovimo, da so bili edini
> resnično revolucionarni poskusi na področju urbanizma delavske
> trdnjave, zgrajene v okviru evropskega revolucionarnega gibanja v
> dvajsetih letih 20\. stoletja. Buržoazija nato topove, ki so v
> dunajskem dvorcu Karla Marxa zadušili delavski upor, zamenja z
> orožjem, ki ga predstavlja prodiranje malomeščanskih zasebnih vrednot
> in uničevanje delavskih stanovanj. Odgovor lahko najdemo le v ponovni
> potrditvi kolektivnih stanovanj kot instrumenta delavskega upora proti
> dezintegraciji.

<!-- In questo senso si può anche parlare di "obiettivi intermedi" e di
scelte alternative: sempre però soltanto in relazione al reale movimento
di ricomposizione della classe nella fabbrica e nella azienda
capitalistica senza il quale non solo quegli obbiettivi non hanno più
senso perché sono delle pure astrazioni, ma rischiano concretamente di
essere utilizzati per deviare la presa di coscienza della classe. Ci
sembra cioè necessario indicare sempre, accanto a queste possibili
scelte, anche la utilizzazione che ne può essere fatta o in funzione
dell'integrazione della forza-lavoro nel capitale o in funzione della
ricomposizione della classe: il contenuto politico sta nella
utilizzazione e non negli obiettivi di per se stessi. Il collegamento
tra fabbrica e abitazione può essere un elemento di maggiore coscienza o
al contrario di mistificazione. -->

> V tem smislu lahko govorimo tudi o "vmesnih ciljih" in alternativnih
> izbirah: vedno pa le v odnosu do dejanskega gibanja rekompozicije
> razreda v tovarni in kapitalističnem podjetju, brez katerega ti cilji
> nimajo dosti smisla, saj so čista abstrakcija, ampak obstaja realna
> nevarnost, da bodo uporabljeni za preusmerjanje zavesti razreda. Z
> drugimi besedami, zdi se nam, da je treba ob teh možnih izbirah vedno
> navesti, kako jih je mogoče uporabiti bodisi v funkciji vključevanja
> delovne sile v kapital bodisi v funkciji rekompozicije razreda:
> politična vsebina je v uporabi in ne v samih ciljih. Povezava med
> tovarno in domom je lahko element večje ozaveščenosti ali, nasprotno,
> mistifikacije.

<!-- Infine si può osservare che queste proposte di lavoro comprendono
anche una possibilità di impiego culturale degli architetti al di fuori
della professione integrata nel capitale. Attraverso la partecipazione
al movimento di classe questi trovano il loro campo di ricerca:
l'architettura dal punto di vista operaio, la progettazione delle nuove
Karl Marx Hof in cui la struttura dell'abitazione collettiva è lo
strumento di battaglia che la classe operaia e la cultura rivoluzionaria
oppongono alle mistificazioni borghesi, cosi come a Vienna opponevano
larghe muraglie e solide barricate ai cannoni borghesi.-->

> Nazadnje lahko opazimo, da ti predlogi delovanja vključujejo tudi
> možnost kulturne zaposlitve arhitektov zunaj stroke, ki so vključeni v
> kapital. S sodelovanjem v razrednem gibanju najdejo svoje raziskovalno
> področje: arhitekturo z delavskega vidika, oblikovanje novega Karla
> Marxa Hofa, v katerem je struktura kolektivnih stanovanj orodje boja,
> ki ga delavski razred in revolucionarna kultura nasprotujeta
> meščanskim mistifikacijam, tako kot so na Dunaju meščanskim topovom
> nasprotovali velikim zidovom in trdnim barikadam.

---
references:
- type: article-journal
  id: greppi1963produzione
  author:
  - family: Greppi
    given: Claudio
  - family: Pedrolli
    given: Alberto
  title: "Produzione e programmazione territoriale"
  container-title: "Quaderni rossi"
  volume: 3
  issued: 1963
  page: 94-101
  language: it
...
