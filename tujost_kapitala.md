---
title: "Tujost kapitala"
description: |
  Zapiski okoli *tujosti* kapitala.
...

::: {lang=en}

> Capital is always searching for ways to overcome barriers to valorization.
> The circulation of money, for instance, allows capital to overcome the temporal and spatial barriers inherent in direct exchange.
> Automation, or what Marx called the "automatic mechanism" enables the overcoming of a different barrier; it helps "reduce to a minimum the resistance offered by man, that obstinate yet elastic natural barrier".
> Even if we do not go so far as to consider the possibility of a capitalism without humans, the notion of synthetic automation should remind us that capital is not "exclusively a reorganisation of human production" but also a trajectory towards a "radically new, alien way of production".
> The needs of capital are alien to humans and only coincide with them accidentally.
> In a striking and horrific passage, Tony Smith puts it admirably: capital is a "higher-order alien power operating at the level of society as a whole.
> It systematically selects for human ends compatible with its end, 'the self-valorization of value', and systematically represses all human ends that are not compatible with this non-human end".
> At this point, we can only speculate on what new operations of selection and repression might be provided to capital by more mature forms of synthetic automation.
[@steinhoff2021automation, 235]

:::

***

Za Krašovca je prehod kapitala od nadomeščanja motoričnih in mehaničnih človeških funkcij do nadomeščanja intelektualnih funkcij ključen.
Dokler stroji nadomeščajo zgolj silo, trend tega nadomeščanja nadzira človek, četudi posledic tega nima popolnoma pod nadzorom.
Ko pa se človeka lahko nadomesti tudi s tako imenovanimi mislečimi stroji -- sistemi, ki lahko sami načrtujejo in prilagajajo svojo dejavnost -- konkurenčno določen razvoj razvoj kapitala postane popolnoma *ambivalenten* do človeka.

> Tehnološka evolucija je presegla biološke omejitve človeških možganov, kar pomeni tudi omejitve človeškega intelekta.
> Na tej točki človeštvo ne postaja odvečno le v socialnem pomenu, temveč tudi v pomenu možnosti nadomeščanja človeške delovne sile v kapitalističnem produkcijskem procesu z mislečimi stroji.
> Stroji industrijske revolucije so bili le prožni, ne pa tudi (avtonomno) inteligentni, mogoče jih je bilo hitro prilagajati, spreminjati, "hekati" ali nadomeščati z novimi, bolj zmogljivimi, niso pa mogli svojih dejavnosti načrtovati, izvajati in prilagajati sami.
> Človeško biološko omejenost so presegli le na področju motorike, ne pa tudi intelekta, medtem ko so današnji stroji čedalje bolj zmožni tudi avtonomnih intelektualnih funkcij, kar pomeni, da morda predstavljajo zametke prožne in inteligentne delovne sile, ki bo sčasoma nadomestila človeško.
[@krasovec2021tujost, 82-83]

Na tej točki, pravi, je za razumevanja delovanja kapitala ključno razumevanje njegove *realne avtonomije*.
Potencialno samospreminjanje kapitala preseže shemo človeški intelekt--materializacija v strojnem sistemu, preseže humanistično teorijo odtujitve, ki poteka na relaciji predikat subjekta--materializacija v objektu.
Realna subsumpcija tako ni več proces prisvajanja človeka po kapitalu, temveč "konkurenčno določena realna avtonomija kapitala".
[@krasovec2021tujost, 83-84]
Na tej podlagi Krašovec za politiko, ideologijo, diskurz pravi, da so stvar starega reda.
Ideološko opredeljevanje intelektualnih disciplin do tehnologije postane brezpredmetno, saj niso presežene zgolj določene ideološke vloge intelektualcev, temveč je presežena sama ideološka funkcija kot taka.


Pri Krašovcu prav tako ideologijo, ki predstavlja intelektualne družbene
skupine, nadomesti računalniška koda:

> Pisanje računalniške kode ni le druga oblika pisanja, v kateri namesto črk nastopajo številke oziroma matematični znaki, temveč je tudi usmerjeno drugam -- ni pisanje neposredno za druge ljudi, temveč pisanje za stroje oziroma tehnološko okolje.
> Medtem ko klasično pisanje s pomočjo primitivne kognitivne tehnologije pisave pošilja sporočila od enih do drugih biomožganov, pomeni programiranje komunikacijo med biomožgani programerja in računalniki kot tehnomožgani.
> Tehnomožgani izvajajo večino današnjih splošnih družbenih kognitivnih in intelektualnih operacij ter pretežno komunicirajo med seboj, kar povzroča večino tesnob, povezanih z novimi mediji in tehnologijami, ki še dodatno eskalirajo z razvojem umetne inteligence, saj ta marginalizira intelektualni *input* biomožganov in, ker je njen način delovanja hiter, kompleksen in tuj, zaradi svoje netransparentnosti deluje zastrašujoče.
[@krasovec2021tujost, 171]


---
lang: sl
references:
- type: book
  id: steinhoff2021automation
  author:
  - family: Steinhoff
    given: James
  title: "Automation and autonomy: labour, capital and machines in the artificial intelligence industry"
  title-short: "Automation and autonomy"
  publisher-place: Cham
  publisher: Palgrave Macmillan
  issued: 2021
  language: en
- type: book
  id: krasovec2021tujost
  author:
  - family: Krašovec
    given: Primož
  title: "Tujost kapitala"
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2021
  language: sl
...
