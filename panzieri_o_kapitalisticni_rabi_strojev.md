---
title: "Raniero Panzieri, O kapitalistični rabi strojev v poznem kapitalizmu"
...

<!-- Com'è noto, la cooperazione semplice si presenta, secondo Marx,
storicamente all'inizio del processo di sviluppo del modo di produzione
capitalistico. Ma questa *figura semplice* della cooperazione è soltanto
una *forma particolare* della cooperazione in quanto *forma
fondamentale* della produzione capitalistica.[^1] "La forma
capitalistica presuppone fin da principio l'operaio salariato libero, il
quale vende al capitale la sua forza-lavoro". Ma l'operaio, in quanto
proprietario e venditore della sua forza-lavoro, entra in rapporto con
il capitale soltanto come *singolo*. La cooperazione, il rapporto
reciproco degli operai "comincia soltanto nel processo lavorativo, ma
nel processo lavorativo hanno già cessato d'appartenere a se stessi.
Entrandovi, sono incorporati nel capitale. Come cooperanti, come membri
di un organismo operante, sono essi stessi soltanto un modo particolare
di esistenza del capitale. Dunque, la forza produttiva sviluppata
dall'operaio come *operaio sociale è forza produttiva del capitale*. *La
forza produttiva sociale* del lavoro si sviluppa gratuitamente appena
gli operai vengono posti in certe condizioni; e il capitale li pone in
quelle condizioni. Siccome la *forza produttiva sociale del lavoro* non
costa nulla al capitale, perché d'altra parte non viene sviluppata
dall'operaio *prima* che il suo stesso lavoro appartenga al capitale,
essa si presenta come forza produttiva posseduta dal capitale *per
natura*, come una forza produttiva *immanente*."[^2]

[^1]: Karl Marx, *Il Capitale,* l. I, 2, trad. di Delio Cantimori, Roma
    1952, p. 33.

[^2]: *Ivi*, p. 30--31. -->
 
> Ve se, da po Marxu enostavna kooperacija zgodovinsko nastopi na
> začetku procesa razvoja kapitalističnega produkcijskega načina. Toda
> ta *enostavna podoba* kooperacije je le *posebna forma* kooperacije
> kot *temeljne forme* kapitalistične produkcije.[^1] "Kapitalistična
> forma pa že na samem začetku predpostavlja svobodnega mezdnega
> delavca, ki svojo delovno silo prodaja kapitalu." Toda delavec, kot
> lastnik in prodajalec svoje delovne sile, v razmerje s kapitalom stopi
> le kot *posameznik*. Kooperacija, stopanje delavcev v razmerja drug z
> drugimi "se začne šele v delovnem procesu, toda v delovnem procesu so
> že nehali pripadati sami sebi. Z vstopom vanj so inkorporirani v
> kapital. Kot kooperirajoči, kot členi dejavnega organizma, so sami le
> poseben eksistenčni način kapitala. Produktivna sila, ki jo delavec
> razvije kot družbeni delavec, je zato produktivna sila kapitala.
> Družbena produktivna sila dela se razvije brezplačno, brž ko so
> delavci postavljeni v določene okoliščine, v te okoliščine pa jih
> postavi kapital. Ker družbena produktivna sila dela kapitala nič ne
> stane in ker je na drugi strani delavec ne razvije, dokler sámo
> njegovo delo ne pripade kapitalu, se pojavlja kot produktivna sila, ki
> jo ima kapital po naravi, kot njegova imanentna produktivna sila."[^2]

[^1]: Karl Marx, *Kapital*, zv. I, Ljubljana 2012, str. 280.

[^2]: *Prav tam*, str. 278--279.

<!-- Il processo produttivo capitalistico si sviluppa nei suoi vari
stadi storici come processo di sviluppo della divisione del lavoro, e il
luogo fondamentale di questo processo è la fabbrica: la
"contrapposizione delle *potenze intellettuali* del processo materiale
di produzione agli operai, *come proprietà non loro* e come *potere che
li domina*, è un prodotto della divisione del lavoro di tipo
manifatturiero. Questo *processo di scissione* comincia nella
cooperazione semplice, dove il capitalista rappresenta l'unità e la
volontà del corpo lavorativo sociale; si completa nella grande industria
che separa la *scienza*, facendone una potenza produttiva indipendente
dal lavoro, e la costringe a entrare al servizio del capitale".[^3]
 
[^3]: Karl Marx, *Il Capitale*, cit., p. 61--62. -->

> Kapitalistični produkcijski proces se v svojih različnih zgodovinskih
> fazah razvija kot proces razvoja delitve dela, in temeljno mesto tega
> procesa je tovarna: "To, da se delavcem duhovne potence materialnega
> produkcijskega procesa postavljajo nasproti kot tuja lastnina in moč,
> ki jih obvladuje, je produkt manufakturne delitve dela. Ta proces
> ločevanja se začne v enostavni kooperaciji, v kateri kapitalist
> nasproti posamičnim delavcem zastopa enotnost in voljo družbenega
> delovnega telesa. Razvija se v manufakturi, ki delavca pohabi v
> delnega delavca. Dopolni se v veliki industriji, ki znanost kot
> samostojno produkcijsko potenco loči od dela in prisili k službi
> kapitalu."[^3]

[^3]: *Prav tam*, str. 301.

<!-- Lo sviluppo della tecnologia avviene interamente all'interno di
questo processo capitalistico. Per quanto il lavoro sia parcellizzato, a
fondamento della manifattura è ancora l'abilità artigiana, "e poiché il
meccanismo complessivo che funziona in essa non possiede una ossatura
*oggettiva* indipendente dai lavoratori stessi, il capitale lotta
continuamente con l'insubordinazione degli operai". La manifattura ha
dunque una *base tecnica ristretta*, che entra in contraddizione "coi
bisogni di produzione da essa stessa creati".[^4] L'introduzione delle
macchine su vasta scala segna il passaggio dalla manifattura alla grande
industria. Questo passaggio si presenta da un lato come superamento
della "ragione tecnica dell'annessione dell'operaio ad una funzione
parziale per tutta la vita e dall'altra cadono i limiti che quello
stesso principio ancora imponeva al dominio del capitale".[^5]

[^4]: *Ivi*, p. 69.
 
[^5]: *Ivi*, p. 70. -->

> Razvoj tehnologije se v celoti odvija znotraj tega kapitalističnega
> procesa. Čeprav je delo parcelirano, je podlaga manufakturi še vedno
> obrtna izučenost, "in ker celotni mehanizem, ki funkcionira v njej,
> nima objektivnega skeleta, neodvisnega od samih delavcev, se kapital
> stalno spopada z nepokorščino delavcev." Manufaktura ima torej *ozko
> tehnično podlago*, ki vstopi v protislovje "s produkcijskimi
> potrebami, ki jih je sama ustvarila."[^4] Uvajanje strojev v velikem
> obsegu pomeni prehod iz manufakture v veliko industrijo. Ta prehod se
> na eni strani razume kot preseganje "tehničnih razlogov za dosmrtno
> priklenitev delavca na delno funkcijo. Na drugi strani pa padejo
> omejitve, ki jih je to načelo še vedno nalagalo gospostvu
> kapitala."[^5]

[^4]: *Prav tam*, str. 306.

[^5]: *Prav tam*, str. 307.

<!-- La tecnologia incorporata nel sistema capitalistico insieme
distrugge "il vecchio sistema della divisione del lavoro" e lo consolida
"*sistematicamente* quale mezzo di sfruttamento della forza-lavoro in
una forma ancor più schifosa. Dalla specialità di tutta una vita,
consistente nel maneggiare uno strumento parziale, si genera la
specialità di tutta una vita, consistente nel servire una macchina
parziale. Così, non solo si diminuiscono notevolmente le spese
necessarie alla riproduzione dell'operaio, ma allo stesso tempo si
completa la sua assoluta dipendenza dall'insieme della fabbrica, quindi
dal capitalista".[^6]

[^6]: *Ivi*, p. 128. -->

> Tehnologija, inkorporirana v kapitalistični sistem, hkrati uničije
> "stari sistem delitve dela" in ga "sistematično reproducira in utrjuje
> kot sredstvo za eksploatacijo delovne sile v še gnusnejši obliki. Iz
> dosmrtne specializiranosti za vodenje kakega delnega orodja nastane
> dosmrtna specializiranost za služenje kakemu delnemu stroju. Tako se
> ne samo pomembno zmanjšajo stroški, potrebni za reprodukcijo delavca,
> ampak se hkrati tudi dopolni njegova nemočna odvisnost od tovarniške
> celote, torej od kapitalista."[^6]

[^6]: *Prav tam*, str. 348.

<!-- Lo stesso progresso tecnologico si presenta quindi come modo di
esistenza del capitale, come suo sviluppo. "La stessa facilità del
lavoro diventa un mezzo di tortura, giacché la macchina non libera dal
lavoro l'operaio, ma toglie il contenuto al suo lavoro. È fenomeno
comune a tutta la produzione capitalistica in quanto non è soltanto
*processo lavorativo* ma anche processo di valorizzazione del capitale,
che non è l'operaio ad adoperare la condizione del lavoro ma, viceversa,
la condizione del lavoro ad adoperare l'operaio; ma questo
capovolgimento viene ad avere soltanto con le macchine una realtà
*tecnicamente evidente*. Mediante la sua trasformazione in macchina
automatica, il mezzo di lavoro si contrappone all'operaio durante lo
stesso processo lavorativo *quale capitale*, quale lavoro morto che
domina e succhia la forza-lavoro vivente".[^7]
 
[^7]: Karl Marx, *Il Capitale*, cit., p. 129. -->
 
> Tako se sam tehnološki napredek predstavlja kot način eksistence
> kapitala, kot njegov razvoj. "Celo olajšanje dela postane sredstvo
> torture, saj stroj ne osvobodi delavca dela, ampak njegovo delo
> vsebine. Vsej kapitalistični produkciji, kolikor je ne le delovni
> proces, temveč hkrati proces uvrednotevanja kapitala, je skupno, da ne
> uporablja delavec delovnega pogoja, pač pa, obratno, delovni pogoj
> uporablja delavca, a šele s stroji dobi ta sprevrnitev tehnično
> otipljivo dejanskost. S to preobrazbo v avtomat stopi delovno sredstvo
> med samim delovnim procesom delavcu nasproti kot kapital, kot mrtvo
> delo, ki obvladuje in izčrpava živo delovno silo."[^7]

[^7]: *Prav tam*, str. 349.

<!-- La fabbrica automatica stabilisce *potenzialmente* il dominio da parte
dei produttori associati sul processo lavorativo. Ma nella applicazione
capitalistica del macchinario, nel moderno sistema di fabbrica "l'automa
stesso è il soggetto, e gli operai sono *coordinati* ai suoi organi
incoscienti solo quali organi coscienti e insieme a quelli sono
subordinati a quella forza motrice centrale".[^8] Si può dunque
stabilire, tra l'altro: 1) che l'uso capitalistico delle macchine non è,
per così dire, la semplice distorsione o deviazione da uno sviluppo
"oggettivo" in se stesso razionale, ma esso determina lo sviluppo
tecnologico; 2) che "la scienza, le immani forze naturali e il lavoro
sociale di massa... sono incarnati nel sistema delle macchine e... con
esso costituiscono il potere del *"padrone"*".[^9] Dunque, di fronte
all'operaio individuale "svuotato", lo sviluppo tecnologico si manifesta
come sviluppo del capitalismo: "come *capitale* e in quanto tale la
macchina automatica ha consapevolezza e volontà nel capitalista".[^10]
Nel "cervello (del padrone) il macchinario e il *suo monopolio* del
medesimo sono inseparabilmente uniti".[^11]

[^8]: *Ivi*, p. 125. 

[^9]: *Ivi*, p. 129.

[^10]: *Ivi*, p. 107.
 
[^11]: *Ivi*, p. 129. -->

> Avtomatska tovarna vzpostavlja *potencialno* gospostvo producentov
> združenih v delovnem procesu. Toda s kapitalistično uporabo strojev, v
> sodobnem tovarniškem sistemu, "pa je sam avtomat subjekt, delavci pa
> so kot organi z zavestjo le dodeljeni njegovim organom, ki so brez
> zavesti, in skupaj z njimi podrejeni osrednji gonilni sili."[^8] Tako
> lahko med drugim ugotovimo: 1) da kapitalistična uporaba strojev ni
> zgolj enostavno izkrivljanje ali odklon od "objektivnega" razvoja, ki
> bi bil sam po sebi racionalen, temveč, da ta določa tehnološki razvoj;
> 2) da "so znanost, neznanske naravne sile in družbeno množično delo
> utelešeni v strojnem sistemu in skupaj z njim tvorijo moč
> "gospodarja"."[^9] Tako se tehnološki razvoj pred posameznim
> "izpraznjenim" delavcem kaže kot razvoj kapitalizma: "kot kapital, in
> kot takšen ima avtomat zavest in voljo v kapitalistu".[^10] V
> "možganih gospodarja so stroji in njegov monopol nad njimi neločljivo
> zraščeni".[^11]

[^8]: *Prav tam*, str. 347.

[^9]: *Prav tam*, str. 349.

[^10]: *Prav tam*, str. 334.

[^11]: *Prav tam*, str. 349.

<!-- Il processo d'industrializzazione, via via che si impadronisce di
stadi sempre più avanzati di progresso tecnologico, coincide con
l'incessante aumento dell'*autorità* del capitalista. Col crescere del
volume dei mezzi di produzione, contrapposti all'operaio, cresce la
necessità di un controllo assoluto da parte del capitalista. Il *piano*
del capitalista è la figura ideale con cui agli operai salariati si
contrappone "la connessione tra i loro lavori" -- "*praticamente*, il
*piano* è l'*autorità* del capitalista, potenza d'una volontà
estranea".[^12] Dunque strettamente connesso allo sviluppo dell'uso
capitalistico delle macchine è lo sviluppo della programmazione
capitalistica. Allo sviluppo della cooperazione, del processo lavorativo
sociale, corrisponde, nella direzione capitalistica, lo sviluppo del
piano come *dispotismo*. Nella fabbrica il capitale afferma in misura
via via crescente il suo potere "come privato legislatore". Il suo
dispotismo è la sua pianificazione, "caricatura capitalistica della
regolazione sociale del processo lavorativo".[^13]
 
[^12]: Karl Marx, *Il Capitale*, cit., p. 28--29. 

[^13]: *Ivi*, p. 131. -->

> Proces industrializacije, preko katerega se postopoma uveljavljajo vse
> naprednejše stopnje tehnološkega napredka, sovpada z vztrajnim
> povečevanje kapitalistove *avtoritete*. Z rastjo obsega produkcijskih
> sredstev, nasproti delavcu, raste kapitalistova potreba po absolutnem
> nadzoru. Mezdnim delavcem povezanost njihovih del nasproti stopa
> idejno kot kapitalistov *plan*, "praktično pa kot kapitalistova
> avtoriteta, kot moč tuje volje".[^12] Tako torej tesno je razvoj
> kapitalističnega načrtovanja povezan z razvojem kapitalistične uporabe
> strojev. Razvoju kooperacije, družbenemu delovnemu procesu, ustreza,
> pod kapitalističnim vodenjem, razvoj plana kot *despotizma*. V tovarni
> kapital v vedno večji meri uveljavlja svojo moč "kot z zasebnim
> zakonom". Njegov despotizem je njegovo načrtovanje, "le kapitalistična
> karikatura družbene ureditve delovnega procesa".[^13]

[^12]: *Prav tam*, str. 277.

[^13]: *Prav tam*, str. 350.

<!-- # Le trasformazioni tecniche e organizzative del capitalismo e le
interpretazioni oggettivistiche  -->

> # Tehnične in organizacijske transformacije kapitalizma ter njihove objektivistične interpretacije

<!-- L'analisi di Marx sulla divisione del lavoro nel sistema della
grande industria a direzione capitalistica si presenta come una valida
metodologia per confutare le varie ideologie "oggettivistiche"
rifiorenti sul terreno del progresso tecnologico (specialmente in
rapporto alla fase dell'automazione). Lo sviluppo *capitalistico* della
tecnologia comporta, attraverso le diverse fasi della razionalizzazione,
di forme sempre più raffinate di integrazione ecc., un aumento crescente
del controllo capitalistico. Il fattore fondamentale di questo processo
è il crescente aumento del capitale costante rispetto al capitale
variabile. Nel capitalismo contemporaneo, come è noto, la pianificazione
capitalistica si amplia smisuratamente con il passaggio a forme
monopolistiche e oligopolistiche, che implicano il progressivo
estendersi della pianificazione dalla fabbrica al mercato, all'area
sociale esterna. -->

> Marxova analiza delitve dela znotraj sistema kapitalistično vodene
> velike industrije se kaže kot primerna metodologija za izpodbijanje
> raznih "objektivističnih" ideologij, ki vznikajo na terenu
> tehnološkega napredka (zlasti teh v zvezi s fazo avtomatizacije).
> *Kapitalistični* razvoj tehnologije vključuje, skozi različne faze
> racionalizacije vse bolj izpopolnjenih oblik integracije itd., vse
> večji kapitalistični nadzor. Temeljni dejavnik tega procesa je vse
> večji delež konstantnega kapitala v razmerju do variabilnega. V
> sodobnem kapitalizmu je znano, da se kapitalistično načrtovanje
> brezmerno širi s postopnim prehodom na monopolistične in
> oligopolistične forme, ki vključujejo postopno širitev načrtovanja iz
> tovarne na trg, na širše družbeno področje.

<!-- Nessun "oggettivo", occulto fattore, insito negli aspetti di
sviluppo tecnologico o di programmazione nella società capitalistica di
oggi, esiste, tale da garantire l'"automatica" trasformazione o il
"necessario" rovesciamento dei rapporti esistenti. Le nuove "basi
tecniche" via via raggiunte nella produzione costituiscono per il
capitalismo nuove possibilità di *consolidamento* del suo potere. Ciò
non significa, naturalmente, che non si accrescano nel contempo le
possibilità di rovesciamento del sistema. Ma queste possibilità
coincidono con il valore totalmente eversivo che, di fronte
all'"ossatura oggettiva" sempre più indipendente del meccanismo
capitalistico, tende ad assumere "l'insubordinazione operaia".  -->

> Ni nobenega "objektivnega", skritega dejavnika, prirojenega
> tehnološkemu razvoju ali načrtovanju današnjih kapitalističnih družb,
> ki bi zagotavljal "avtomatsko" transformacijo ali "nujno" odpravo
> obstoječih odnosov. Nove "tehnične podlage", ki jih postopoma dosegajo
> v produkciji, kapitalizmu predstavljajo nove možnosti *konsolidacije*
> njegove moči. Kar seveda ne pomeni, da hkrati ne povečujejo možnosti
> odprave sistema. Toda te možnosti sovpadajo s povsem subverzivnim
> predznakom, ki ga "nepokorni delavci" pridobivajo spričo "objektivnega
> skeleta", ki je vse bolj neodvisen od kapitalističnega mehanizma.

<!-- Le ideologie "oggettivistiche", "economicistiche" presentano
quindi, ovviamente, gli aspetti più interessanti intorno ai problemi
dello sviluppo tecnologico e della organizzazione aziendale. Non ci
riferiamo qui naturalmente alle ideologie neocapitalistiche, ma a
posizioni espresse all'interno del movimento operaio e della sua
problematica teorica.  -->

> "Objektivistične" in "ekonomistične" ideologije tako, očitno,
> predstavljajo najbolj zanimive vidike v zvezi s problemi tehnološkega
> razvoja in organizacije podjetja. Pri tem seveda nimamo v mislih
> neokapitalističnih ideologij, temveč stališča, ki se izražajo znotraj
> delavskega gibanja, in njegove teoretične problematike.

<!-- Contro le vecchie cristallizzazioni ideologiche nell'azione
sindacale, il processo di rinnovamento del sindacato di classe in questi
anni si sviluppa innanzi tutto intorno al riconoscimento delle "nuove
realtà" del capitalismo contemporaneo. Ma l'attenzione giustamente
rivolta alle modificazioni che accompagnano l'attuale fase tecnologica
ed economica è, in tutta una serie di posizioni e di ricerche, distorta
in una rappresentazione di esse in forma "pura", idealizzata, spogliata
delle concrete connessioni con gli elementi generali e determinanti (di
potere) dell'organizzazione capitalistica.[^14] La razionalizzazione,
con la sua parcellizzazione estrema del lavoro, il suo "svuotamento" del
lavoro operaio, è considerata come una fase di passaggio, "dolorosa" ma
necessaria e transitoria allo stadio che "ricompone in senso unitario i
lavori parcellari". Ambiguamente viene riconosciuto che la diminuzione
dell'applicazione del lavoro vivo nella produzione e l'aumento
corrispondente del capitale costante sospingono verso una ininterrotta
continuità del ciclo così come "crescono ulteriormente i legami di
interdipendenza interna ed esterna: come all'interno di una unità
produttiva il singolo posto di lavoro e il singolo lavoratore non
possono essere considerati che come parte di un insieme organicamente
collegato, così, verso l'esterno, ogni singola unità produttiva e il suo
comportamento hanno più stretti legami di interdipendenza con tutto il
corpo economico".[^15] 

[^14]: Riteniamo utile riferirci ai primi documenti della "svolta"
sindacale, sulla cui base ancor oggi continua a svilupparsi il
dibattito: *I lavoratori e il progresso tecnico. Atti del Convegno
tenuto all'Istituto "Antonio Gramsci" in Roma, nei giorni 29-30 giugno e
1° luglio 1956, sul tema: "Le trasformazioni tecniche e organizzative e
le modificazioni del rapporto di lavoro nelle fabbriche italiane"*;
Silvio Leonardi, *Progresso tecnico e rapporti di lavoro*, Torino 1957.
Prendiamo come riferimento fondamentale l'opera di Leonardi, che amplia
e approfondisce la relazione svolta dal medesimo al Convegno
dell'Istituto Gramsci. Per gli sviluppi più recenti della discussione,
cfr. le relazioni e gli interventi al recente Congresso sul progresso
tecnologico e la società italiana, cit. più sotto. Cfr. anche, in questo
"Quaderno", la rassegna di Dino De Palma. In questi appunti prescindiamo
da ogni riferimento alla vasta letteratura (sia d'ispirazione
neocapitalistica sia marxista) sui temi accennati, intendendo solo
richiamarci al dibattito in corso nel nostro movimento sindacale. 

[^15]: Cfr. Leonardi, cit., p. 93; cfr. anche p. 35, 46, 55--59. -->

> V nasprotju s starimi ideološkimi kristalizacijami sindikalnega
> delovanja se je proces prenove razrednega sindikata v teh letih
> razvijal predvsem okoli prepoznanja "nove realnosti" sodobnega
> kapitalizma. Toda pozornost, ki se jo upravičeno namenja spremembam,
> ki spremljajo trenutno tehnološko in ekonomsko fazo, je, v številnih
> stališčih in raziskavah, izkrivljena v reprezentacijo "čistih" form,
> idealiziranih, slečenih konkretnih povezav s splošnimi in odločilnimi
> elementi (moči) kapitalistične organizacije.[^14] Racionalizacija, s
> svojo skrajno parcelacijo dela, s svojo "izpraznitvijo" delavčevega
> dela, se obravnava kot prehodna faza, kot "boleč", a nujen prehod v
> stadij, ki "parcelirano delo ponovno združi v enotnem smislu". Dvoumno
> se priznava, da pojemanje uporabe živega dela v produkciji in ustrezno
> povečanje konstantnega kapitala sili v smer neprekinjenega
> nadaljevanja cikla, saj se "notranja in zunanja soodvisnost še
> povečujeta: kakor posameznega delovnega mesta in posameznega delavca
> znotraj produkcijske enote ni možno obravnavati drugače kot del
> organsko povezane celote, tako sta navzven vsaka posamezna
> produkcijska enota in njeno delovanje tesno povezana s celotnim
> gospodarskim korpusom".[^15]

[^14]: Menimo, da je koristno omeniti prve dokumente sindikalnega
"obrata", na podlagi katerih se razprava razvija še danes: *I lavoratori
e il progresso tecnico. Atti del Convegno tenuto all'Istituto "Antonio
Gramsci" in Roma, nei giorni 29--30 giugno e 1° luglio 1956, sul tema:
"Le trasformazioni tecniche e organizzative e le modificazioni del
rapporto di lavoro nelle fabbriche italiane"*; Silvio Leonardi,
*Progresso tecnico e rapporti di lavoro*, Torino 1957. Leonardijevo delo
jemljemo kot temeljno referenco, ki razširja in poglablja poročilo, ki
ga je podal na konferenci Istituto Gramsci. Za novejše razvoje razprave
gl. poročila in govore na nedavnem Conresso sul progresso tecnologico e
la società italiana, ki so navedeni v nadaljevanju. Gl. tudi, v tem
"Zvezku", recenzijo Dina De Palme. V teh zapisih ne upoštevamo vse
obsežne literaturo (tako neokapitalistično kot marksistično) na to temo,
temveč se sklicujemo samo na razpravo, ki poteka v našem sindikalnem
gibanju.


[^15]: Gl. Leonardi, nav. d., str. 93; gl. tudi str. 35, 46, 55--59.

<!-- Aspetti caratteristici nuovi assunti dall'organizzazione
capitalistica vengono così scambiati come stadi di sviluppo di una
oggettiva "razionalità". Così, ad esempio, viene sottolineata la
funzione positiva, "razionale" dell'MTM, in quanto "attraverso i tempi,
il tecnico è obbligato a studiare i metodi"![^16] E ancora: l'enorme
valore di rottura che nella grande azienda moderna "con una produzione
programmata e realizzata a flusso continuo", assume "la non
corrispondenza di un operaio, di un gruppo di operai, a quanto viene
loro richiesto in base alle previsioni fatte nel programma di produzione
aziendale"[^17] è assolutamente dimenticato per mettere invece in luce
l'esigenza (naturalmente "razionale") "del cosiddetto rapporto "morale"
tra imprenditori e lavoratori, che è condizione e scopo delle cosiddette
"relazioni umane", appunto perché unicamente sulla sua base si può
stabilire la collaborazione": infatti, "a una produzione integrata deve
corrispondere una integrazione del lavoratore nell'azienda, e questa
integrazione deve essere volontaria, poiché nessuna costrizione o
disciplina può ottenere la rinuncia, da parte di uomini, alla libertà,
per esempio, di produrre un giorno un po' di meno e un altro un po' di
più", ecc. ecc.[^18] Dimodoché le "ragioni di esaurimento di questo
movimento (delle "relazioni umane") potranno consistere
nell'assorbimento della parte valida della sua tematica": certo, i
sindacati devono intervenire "a rompere dannose forme di aziendalismo
strettamente legate alle "relazioni umane" stesse"![^19] Dunque, la
*sostanza* dei processi di integrazione viene accettata, riconoscendo in
essi una intrinseca necessità, che scaturirebbe fatalmente dal carattere
della produzione "moderna". Semplicemente, viene richiamata l'esigenza
di correggere alcune "distorsioni" che l'uso capitalistico introdurrebbe
in questi procedimenti. La stessa organizzazione "funzionale" della
produzione viene vista in questo quadro soltanto nella sua forma
tecnologicamente "sublimata", addirittura come un salto oltre la
gerarchizzazione propria delle fasi precedenti di meccanizzazione. Non
si sospetta neppure che il capitalismo possa servirsi delle nuove "basi
tecniche" offerte dal passaggio dagli stadi precedenti a quello di
meccanizzazione spinta (e all'automazione), per perpetuare e consolidare
la struttura *autoritaria* dell'organizzazione della fabbrica; infatti,
ci si rappresenta tutto il processo dell'industrializzazione come
dominato dalla fatalità "tecnologica" che conduce alla liberazione
"dell'uomo dalle limitazioni impostegli dall'ambiente e dalle
possibilità fisiche". La "razionalizzazione amministrativa", la crescita
enorme di funzioni di "organizzazione verso l'esterno", vengono
ugualmente colte in una forma "tecnica", "pura": il rapporto tra questi
sviluppi e i processi e le contraddizioni del capitalismo contemporaneo
(la sua ricerca di mezzi sempre più complessi per realizzare e imporre
la *sua* pianificazione), ossia la concreta realtà storica nella quale
il movimento operaio si trova a vivere e a combattere, l'odierno "uso
capitalistico" delle macchine e dell'organizzazione -- vengono
completamente ignorati a vantaggio di una rappresentazione
tecnologico-idilliaca.

[^16]: *Ivi*, p. 48.

[^17]: *Ivi*, p. 50. "Un semplice ritardo, un'assenza, o anche solo una
diminuita produzione di un solo operaio, possono riflettersi su tutta
una linea di macchine", ecc. ecc. (p. 50 sgg.). 

[^18]: *Ivi*, p. 50--51.
 
[^19]: *Ivi*, p. 52. -->

> Novi značilni vidiki, ki jih privzema kapitalistična organizacija, se
> tako zamenjujejo za stadije razvoja objektivne "racionalnosti". Tako
> se, na primer, poudarja pozitivna funkcija, "racionalnost"
> *Methods-time-measurementa*, saj je "tehnik, skozi čas, dolžan
> preučevati metode"![^16] In še enkrat: ogromna prelomna vrednost tega,
> da veliko moderno podjetje "z načrtovano produkcijo realizirano v
> neprekinjenem toku", predpostavlja "nestrinjanje delavca, skupine
> delavcev, s tem kar se od njih zahteva na podlagi programskih napovedi
> podjetja"[^17] je popolnoma pozabljena, da bi namesto nje razsvetlili
> zahtevo (seveda "racionalno") "tako imenovanega "moralnega" razmerja
> med zaposlovalci in delavci, ki je pogoj in namen tako imenovanih
> "človeških razmerij", ravno ker se zgolj na njihovi podlagi lahko
> vzpostavi sodelovanje": dejansko "mora integrirani produkciji
> odgovarjati integracija delavca v podjetje, in ta integracija mora
> biti prostovoljna, saj nobena prisila ali disciplina ne more od ljudi
> pridobiti odpovedi svobodi, da, na primer, en dan proizvede malo manj
> in drug dan malo več" itd.[^18] Tako da "so razlogi za izčrpavanje
> tega gibanja ["človeških razmerij"] lahko v vpijanju veljavnega dela
> njegove tematike": seveda, sindikati morajo posredovati "da bi
> prekinili s škodljivimi oblikami podjetništva, ki so tesno povezane s
> "človeškimi razmerji" samimi"![^19] Tako, *substanca* procesov
> integracije je sprejeta, v njih je prepoznana notranja nujnost, ki naj
> bi neizogibno izhajala iz značaja "moderne" produkcije. Treba je zgolj
> opozoriti na potrebo, da se nekatere "distorzije", ki jih je v te
> postopke vnesla kapitalistična raba, popravi. Sama "funkcionalna"
> organizacija produkcije je znotraj tega okvira videti le v njeni
> tehnološko "sublimirani" formi, celo kot skok onkraj hierarhizacije,
> ki je značilna za prejšnje faze mehanizacije. Niti se ne posumi, da
> lahko kapitalizem nove "tehnične osnove", ki jih ponuja prehod iz
> prejšnjih stadijev k napredni mehanizaciji (in avtomatizaciji),
> uporabi za perpetuacijo in konsolidacijo *avtoritarne* strukture
> tovarniške organizacije; pravzaprav se celoten proces
> industrializacije predstavlja kot gospostvo "tehnološke" usode, ki
> vodi k osvoboditvi "človeka od omejitev, ki mu jih je naložilo okolje
> in fizične možnosti". "Administrativna racionalizacija", ogromna rast
> "zunanjih organizacijskih funkcij", so prav tako zajete v "tehnični",
> "čisti" formi: razmerje med temi razvoji in procesi ter protislovji
> sodobnega kapitalizma (njegovo iskanje vedno bolj zapletenih sredstev
> za realizacijo in uveljavljanje *svojega* načrtovanja) -- konkretna
> zgodovinska realnost v kateri delavsko gibanja živi in se bori,
> današnja "kapitalistična uporaba" strojev in organizacija -- sta
> popolnoma prezrta v korist tehnološko-idilične predstave.

[^16]: *Prav tam*, str. 48.

[^17]: *Prav tam*, str. 50. "Preprost zaostanek, odsotnost ali celo
zmanjšanje produkcije za enega samega delavca se lahko odraža na celotni
liniji strojev", itd. (str. 50 in nasl.).

[^18]: *Prav tam*, str. 50--51.

[^19]: *Prav tam*, str. 52.

<!-- Particolarmente gravi sono le deformazioni che riguardano il
carattere della prestazione di lavoro nella fabbrica moderna,
conseguenti a una considerazione "oggettiva" delle nuove forme
tecnologico-organizzative. Si tende a riconoscere la scomparsa della
parcellizzazione delle funzioni e lo stabilirsi di nuove mansioni a
carattere unitario, che sarebbero qualificate da responsabilità,
capacità di decisione, molteplicità di preparazione tecnica.[^20] Lo
sviluppo delle tecniche e delle funzioni connesse al *management* viene
isolato dal concreto contesto sociale in cui si produce, cioè dal
crescente accentramento del potere capitalistico, e perciò considerato
come il supporto di nuove categorie di lavoratori (i tecnici, gli
"intellettuali della produzione"), che "naturalmente" porterebbero, come
diretto riflesso delle loro nuove professionalità, la soluzione delle
contraddizioni "tra caratteri ed esigenze delle forze produttive e
rapporti di produzione".[^21] Il contrasto tra forze produttive e
rapporti di produzione compare qui come "non corrispondenza" tecnica:
"ad esempio, nel caso che nella scelta della migliore combinazione di
determinati fattori produttivi, ormai conseguibile con metodi sempre più
obbiettivamente validi, essi (i lavoratori di nuovo tipo) siano
costretti a scartare le soluzioni obbiettivamente più valide per
rispettare limiti posti da interessi privati".[^22] Ed è certo, da
questo punto di vista, che "la falce e il martello... oggi possono
essere un simbolo del lavoro umano solo dal punto di vista ideale"![^23]
Tutto ciò, naturalmente, ha un diretto riflesso sulla concezione della
lotta operaia, sulla rappresentazione dei protagonisti stessi di questa
lotta. La realtà delle lotte attuali indica una convergenza dei vari
"livelli" di lavoratori determinati dall'organizzazione attuale della
grande fabbrica[^24] verso richieste *gestionali*. S'intende che questo
è un processo che avviene sulla base di fattori oggettivi, rappresentati
appunto dalla diversa "collocazione" dei lavoratori nel processo
produttivo, dal diverso tipo di rapporti con la produzione e con
l'organizzazione, ecc. ecc. Ma l'elemento specifico di questo processo
di "ricomposizione unitaria" non può cogliersi se sfugge o si rifiuta il
nesso tra l'elemento "tecnologico" e quello organizzativo-politico (di
potere) nel processo produttivo capitalistico. Il livello *di classe* si
esprime non come progresso ma come rottura, non come "rivelazione"
dell'occulta razionalità insita nel moderno processo produttivo ma come
costruzione di una razionalità radicalmente nuova e contrapposta alla
razionalità praticata dal capitalismo. Ciò che caratterizza gli attuali
processi di acquisizione di coscienza di classe negli operai della
grande fabbrica (quelli, ad esempio, esaminati in questo "Quaderno")
"non (è) soltanto la esigenza primaria di espansione della personalità
nel lavoro, ma una esigenza motivata strutturalmente di gestire il
potere politico ed economico dell'impresa e attraverso ad essa della
società".[^25]

[^20]: Leonardi cit., pp. 55--56. 

[^21]: *Ivi*, p. 82. Sulla "alienazione totale" degli "intellettuali
della produzione" v. invece le osservazioni veramente puntuali e acute
di Pino Tagliazucchi nell'articolo *Aspetti della condizione
impiegatizia nell'industria moderna*, in "Sindacato moderno", n. 1,
febbraio-marzo 1961, p. 53 sgg. 

[^22]: Leonardi cit., pp. 81--82. 

[^23]: *Ivi*, p. 67. 

[^24]: Vedi in questo "Quaderno", la relazione di Alquati.
 
[^25]: *Ivi*. -->

> Posebej resna so izkrivljanja, posledica "objektivne" obravnave nove
> tehnološko-organizacijske forme, ki se nanašajo na značaj opravljanja
> dela v moderni tovarni. Pojavlja se težnja, da bi razdrobljenost
> izginila ter se vzpostavile nove naloge enotnega značaja, za katere bi
> bili značilni odgovornost, sposobnost odločanja in številne tehnične
> priprave.[^20] Razvoj tehnik in funkcij povezanih z *menedžmentom* je
> ločen od konkretnega družbenega konteksta v katerem se proizvaja, tj.
> od vse večje koncentracije kapitalistične moči, in se zato obravnava
> kot podpora novim kategorijam delavcev (tehniki, "intelektualci
> produkcije"), ki naj bi seveda prinesli, kot neposreden odraz njihove
> nove strokovnosti, rešitev protislovij "med značilnostmi in potrebami
> produktivnih sil ter produkcijskih razmerij".[^21] Nasprotje med
> produktivnimi silami in produkcijskimi razmerji se tu pojavlja kot
> tehnična "neskladnost": "na primer, v primeru ko med izbiro najboljše
> kombinacije določenih produktivnih faktorjev, ki jih je zdaj možno
> določiti z vse bolj objektivno veljavnimi metodami, so [novi tipi
> delavcev] prisiljeni zavreči objektivno primernejše rešitve, da bi
> upoštevali omejitve, ki jih določajo zasebni interesi".[^22] S tega
> stališča je jasno, da "sta srp in kladivo [...] danes lahko simbol
> človeškega dela je z idealističnega stališča"![^23] Vse to, seveda, se
> neposredno odraža v pojmovanju delavskega boja v predstavi samih
> protagonistov tega boja. Realnost aktualnih bojev kaže, da se različne
> "ravni" delavcev, ki jih določa aktualna organizacija velike
> tovarne[^24], usmerja k *vodstvenim* zahtevam. Treba je razumeti, da
> gre za proces, ki poteka na podlagi objektivnih faktorjev, ki jih
> predstavljajo prav različni "položaji" delavcev v produkcijskem
> procesu, različni tipi razmerij do produkcije, do organizacije itd.
> Toda specifičnega elementa tega procesa "enotne rekompozicije" ni
> mogoče razumeti, če spregledamo ali če zavrnemo povezavo med
> "tehnološkim" elementom in organizacijsko-političnim elementom (moči)
> v kapitalističnem produkcijskem procesu. *Razredna* raven se ne izraža
> kot napredek ampak kot prelom, ne kot "razkritje" okultne
> racionalnosti vgrajene v modernem produkcijskem procesu, temveč kot
> konstrukcija radikalno nove racionalnosti, ki nasprotuje
> racionalnosti, ki jo prakticira kapitalizem. To kar je značilno za
> aktualne procese pridobivanja razredne zavesti med delavci velikih
> tovarn (na primer teh, ki jih obravnavamo v tem "Quaderno") "ni samo
> primarna potreba po uresničevanju svoje osebnosti med delom, ampak
> strukturno motivirana potreba po upravljanju politične in gospodarske
> moči podjetja in skozi njo družbe".[^25]

[^20]: Leonardi, nav. d., str. 55--56.

[^21]: *Prav tam*, str. 82. O "popolni odtujitvi" "intelektualcev
proizvodnje" glej zelo natančna in ostra opažanja Pina Tagliazucchija v
članku *Aspetti della condizione impiegatizia nell'industria moderna*, v
"Sindacato moderno", št. 1, februar-marec 1961, str. 53 in naslednje.

[^22]: Leonardi, nav. d., str. 81--82.

[^23]: *Prav tam*, str. 67.

[^24]: Glej v tem "Zvezku", Alquatijevo poročilo.

[^25]: *Prav tam*.

<!-- Perciò i fattori, cui sopra si accennava, di caratterizzazione
"oggettiva" dei diversi strati di lavoratori nel processo produttivo
hanno certamente un significato nella formazione di una presa di
coscienza "collettiva", da parte dei lavoratori, delle implicazioni
politiche del fatto produttivo. Ma questi fattori si rapportano alla
formazione di una forza unitaria di rottura che tende a investire in
tutti i suoi aspetti l'attuale realtà
tecnologico-organizzativa-proprietaria della fabbrica capitalistica.
-->

> Zato so zgoraj navedeni faktorji, ki "objektivno" označujejo različne
> sloje delavcev v produkcijskem procesu, zagotovo pomembni pri
> oblikovanju "kolektivne" zavesti delavcev o političnih implikacijah
> produktivne dejanskosti. Toda te faktorji se nanašajo na oblikovanje
> enotne prelomne sile, ki teži v vseh svojih aspektih napasti trenutno
> tehnološko-organizacijsko-lastniško realnost kapitalistične tovarne.

<!-- # Integrazione ed equilibrio del sistema -->

> # Integracija in ravnovesje sistema

<!-- È ovvio che la convalida piena dei processi di razionalizzazione
(considerati come insieme delle tecniche produttive elaborate
nell'ambito del capitalismo) dimentica che è precisamente il
"dispotismo" capitalistico che assume la forma della razionalità
tecnologica. Nell'uso capitalistico, non solo le macchine, ma anche i
"metodi", le tecniche organizzative, ecc. sono incorporati nel capitale,
si contrappongono agli operai come capitale: come "razionalità"
estranea. La "pianificazione" capitalistica presuppone la pianificazione
del lavoro vivo, e quanto più essa si sforza di presentarsi come un
sistema chiuso, perfettamente razionale di regole, tanto più essa è
astratta e parziale, pronta per essere utilizzata in una organizzazione
soltanto di tipo gerarchico. Non la "razionalità", ma il *controllo*,
non la programmazione tecnica ma il progetto di potere dei produttori
associati possono assicurare un rapporto adeguato con i processi
tecno-economici globali. -->

> Očitno je, da se s polnim potrjevanjem procesov racionalizacije (če
> jih obravnavamo kot skupek produkcijskih tehnik, razvitih v okviru
> kapitalizma) pozablja, da je ravno kapitalistični "despotizem" tisti,
> ki privzame formo tehnološke racionalnosti. S kapitalistično uporabo
> so, ne le stroji, ampak tudi "metode", organizacijske tehnike itd.
> inkorporirane v kapital, delavcu se zoperstavljajo kot kapital: kot
> tuja "racionalnost". Kapitalistično "načrtovanje" predpostavlja
> načrtovanje živega dela, in toliko bolj se trudi predstaviti kot
> zaprt, popolnoma racionalnih sistem pravil, toliko bolj je abstrakten
> in parcialen, primeren za uporabo le v hierarhičnih organizacijah. Ne
> "racionalnost", temveč *nadzor*, ne tehnično načrtovanje, temveč
> projekt moči združenih producentov lahko zagotovi ustrezen odnos z
> globalnimi tehnično-ekonomskimi procesi.

<!-- In effetti, nell'ambito di una considerazione "tecnica",
pseudoscientifica dei nuovi problemi e delle nuove contraddizioni che
insorgono nell'azienda capitalistica odierna, è possibile trovare
soluzioni via via più "avanzate" dei nuovi squilibri senza toccare la
sostanza dell'alienazione, garantendo anzi il mantenimento
dell'equilibrio del sistema. In effetti, le ideologie sociologiche e
organizzative del capitalismo contemporaneo presentano varie fasi, dal
taylorismo al fordismo fino allo sviluppo delle tecniche integrative,
*human engineering*, relazioni umane, regolazione delle comunicazioni,
ecc.[^26], appunto nel tentativo, sempre più complesso e raffinato, di
adeguare la pianificazione del lavoro vivo agli stadi via via raggiunti,
attraverso il continuo accrescimento del capitale costante, dalle
esigenze di programmazione produttiva.[^27] In questo quadro, è evidente
che tendono ad assumere sempre più importanza le tecniche di
"informazione", destinate a neutralizzare la protesta operaia
immediatamente insorgente dal carattere "totale" che assumono i processi
di alienazione nella grande fabbrica razionalizzata. Naturalmente,
l'analisi concreta si trova di fronte a situazioni anche profondamente
diverse tra loro, sotto questo profilo, in rapporto a una quantità non
trascurabile di fattori particolari (disparità nello sviluppo
tecnologico, orientamenti soggettivi diversi nella direzione
capitalistica, ecc. ecc.); ma il punto che qui ci preme di sottolineare
è che nell'uso delle tecniche "informative", come manipolazione
dell'atteggiamento operaio, il capitalismo ha vasti, indefinibili
margini di "concessione" (e meglio si direbbe di "stabilizzazione"). Non
è determinabile il limite oltre il quale l'"informazione" circa i
processi produttivi globali cessa di essere un fattore di
stabilizzazione per il potere del capitale. Ciò che è certo è che le
tecniche di informazione tendono a restituire, nella situazione più
complessa dell'azienda capitalistica contemporanea, quella "attrattiva"
(soddisfazione) del lavoro di cui già parlava il *Manifesto*.[^28] 

[^26]: Cfr. Nora Mitrani, *Ambiguité de la technocratie*, in "Cahiers
internationaux de sociologie", vol. XXX, 1961, p. 111.

[^27]: Franco Momigliano ha notato giustamente che "la fabbrica moderna
non solo esclude sempre più gli operai dalla cosciente partecipazione al
momento stesso di elaborazione del piano razionale produttivo, al
processo globale di produzione, ma richiede agli operai, subordinati
alla nuova razionalità, di impersonare contemporaneamente il momento
"antirazionale", quello corrispondente alla filosofia dello
"arrangiarsi", del vecchio empirismo. In tal modo la stessa resistenza
operaia risulta, paradossalmente, razionalmente sfruttata". Cfr. *Il
Sindacato nella fabbrica moderna*, in "Passato e Presente" n. 15,
maggio-giugno 1960, p. 20--21. 

[^28]: "Il lavoro dei proletari, con l'estendersi dell'uso delle
macchine e con la divisione del lavoro, ha perduto ogni carattere di
indipendenza e quindi ogni attrattiva per l'operaio. Questi diventa un
semplice accessorio della macchina". -->

> Dejansko je znotraj okvira "tehnične", psevdoznanstvene obravnave
> novih problemov in novih protislovij, ki se pojavljajo v današnjem
> kapitalističnem podjetju, možno najti vse bolj "napredne" rešitve za
> nova neravnovesja, ne da bi se dotaknili bistva odtujenosti, temveč da
> bi zagotovili ravnovesje sistema. Sociološke in organizacijske
> ideologije sodobnega kapitalizma prikazujejo različne faze, od
> taylorizma do fordizma pa do razvoja integracijskih tehnik, *human
> engineeringa*, človeških razmerij, regulacija komunikacij itd.[^26],
> prav v vse bolj zapletenem in prefinjenem poskusu, da bi prilagodile
> načrtovanje živega dela stadijem, ki jih z nenehno rastjo konstantnega
> kapitala dosegajo potrebe produktivnega načrtovanja.[^27] Očitno je,
> da znotraj tega okvira vse večji pomen dobivajo "informacijske"
> tehnike, usmerjene k nevtralizaciji delavskega upora, ki neposredno
> izhaja iz "totalnega" značaja procesov odtujitve v veliki
> racionalizirani tovarni. Seveda se konkretna analiza znajde tudi pred
> situacijami, ki se med sabo, zaradi številnih nezanemarljivih
> faktorjev (razlike v tehnološkem razvoju, subjektivne usmeritve
> kapitalističnega vodstva itd.), lahko močno razlikujejo; vendar točka,
> ki jo želimo poudariti je, da ima kapitalizem pri uporabi
> "informacijskih" tehnik za manipulacijo delavske drže velike,
> nedoločljive margine "koncesij" (ali bolje rečeno, "stabilizacij").
> Meja onkraj katere "informacije" o globalnih produkcijskih procesih
> prenehajo biti stabilizacijski faktor kapitalistične moči ni
> določljiva. Gotovo pa je, da informacijske tehnike težijo, v bolj
> kompleksnih situacijah sodobnih kapitalističnih podjetij, k obnovitvi
> te "privlačnosti" (zadovoljstva) dela o kateri je govoril že
> *Manifest*.[^28]

[^26]: Gl. Nora Mitrani, *Ambiguité de la technocratie*, v: "Cahiers
internationaux de sociologie", letnik XXX, 1961, str. 111.

[^27]: Franco Momigliano je pravilno ugotovil|opomnil, da "moderna
tovarna ne le vedno bolj izključuje delavce iz zavestne udeležbe v samem
trenutku razdelave racionalnega plana produkcije, v globalnem
produkcijskem procesu, temveč od delavcev, podrejenih novi
racionalnosti, zahteva, da hkrati poosebljajo "antiracionalni" moment,
ki ustreza filozofiji "prilagajanja", staremu empirizmu. Tako se sam
delavski upor, paradoksalno, racionalno izgorišča". Gl. *Il Sindicato
nella fabbrica moderna*, in "Passato e Presente" št. 15, maj-junij 1960,
str. 20--21-

[^28]: "Delo proletarcev je z razširjanjem strojev in z delitvijo dela
izgubilo sleherno samostojnost in s tem ves mik za delavca. Le-ta
postane zgolj pritiklina stroja".

<!-- str 103 -->

<!-- L'estendersi delle tecniche di informazione e del loro campo di
applicazione, così come l'estendersi della sfera di decisioni
tecniche,[^29] rientrano perfettamente nella "caricatura" capitalistica
della regolazione sociale della produzione. Occorre dunque sottolineare
che la "consapevolezza produttiva" non opera il rovesciamento del
sistema, che la partecipazione dei lavoratori al "piano funzionale" del
capitalismo, di per sé, è fattore di integrazione, di alienazione, per
così dire, ai limiti estremi del sistema. È bensì vero che si ha qui,
con lo sviluppo dei "fattori di stabilizzazione" nel neocapitalismo, una
premessa di natura tale, per l'azione operaia, da rendere immediatamente
necessario il rovesciamento totale dell'ordine capitalistico. La lotta
operaia si presenta perciò come necessità di contrapposizione globale al
piano capitalistico, dove fattore fondamentale è la consapevolezza,
diciamo pure dialettica, dell'unità dei due momenti "tecnico" e
"dispotico" nell'attuale organizzazione produttiva. Rispetto alla
"razionalità" tecnologica, il rapporto ad essa dell'azione
rivoluzionaria è di "comprenderla", ma non per riconoscerla ed
esaltarla, bensì per sottometterla a un nuovo uso: all'uso socialista
delle macchine.[^30]

[^29]: Sulle esigenze di partecipazione "democratica" degli operai per
una amministrazione capitalistica più razionale, cfr. il libro molto
importante di Seymour Melman, *Decision Making and Productivity*,
Oxford, 1958.

[^30]: Gli sviluppi più recenti della ricerca economica e tecnica
nell'Unione Sovietica presentano un carattere ambiguo. Mentre la
rivendicazione del momento autonomo della ricerca ha senza dubbio un
significato di contrasto e di rottura rispetto alle forme più rozze di
volontarismo nella pianificazione di tipo staliniano, lo sviluppo di
processi "razionali", indipendentemente dal controllo sociale della
produzione, sembra piuttosto rappresentare (quanto già oggi? e quanto
come possibilità futura?) la premessa e il supporto per nuovi sviluppi
dei vecchi processi di burocratizzazione. È tuttavia importante non
perdere di vista il tratto distintivo della pianificazione sovietica
rispetto al piano capitalistico. L'elemento autoritario, dispotico
dell'organizzazione produttiva nasce nel seno dei rapporti capitalistici
e *sopravvive* nelle economie pianificate di tipo burocratico. Le
burocrazie nel loro rapporto verso la classe operaia non possono
soltanto appellarsi alla razionalità oggettiva, ma debbono richiamarsi
alla classe operaia stessa. La caduta dell'elemento fondamentale,
dell'elemento proprietario, toglie all'organizzazione burocratica, per
così dire, il suo fondamento proprio. Onde, in URSS e nelle Democrazie
popolari, le contraddizioni si presentano diversamente e il dispotismo
presenta un carattere precario e non organico. Ciò che non significa,
naturalmente, che le sue manifestazioni non possano assumere aspetti
altrettanto crudi di quelli delle società capitalistiche. Cfr. le
osservazioni fondamentali di Rodolfo Morandi negli scritti: *Analisi
dell'economia regolata* (1942) e *Criteri organizzativi dell'economia
collettiva* (1944), ristampati in *Lotta di popolo*, Torino 1958.
L'esclusione dell'elemento proprietario e la considerazione a sé
dell'elemento autoritario-burocratico o della alienazione tecnica (o di
entrambi) sono, com'è noto, al centro di una ormai sterminata
letteratura ideologica neo-capitalistica e neo-riformistica. All'analisi
di queste ideologie sarà dedicato uno dei nostri "Quaderni". -->

> Širitev informacijskih tehnik in njihovega polja uporabe ter prav tako
> širitev sfere tehničnega odločanja[^29] se popolnoma ujemata s
> kapitalistično "karikaturo" družbene regulacije produkcije. Zato je
> potrebno poudariti, da "produktivna zavest" ne privede do odprave
> sistema, da je sodelovanje delavcev pri "funkcionalnem planu"
> kapitalizma, samo po sebi, faktor integracije, tako rekoč odtujitve,
> na skrajnih mejah sistema. Res je, da imamo z razvojem
> "stabilizacijskih faktorjev" neokapitalizma takšne pogoje za delavsko
> akcijo, da je takojšnja popolna odprava kapitalističnega reda nujna.
> Delavski boj se zato predstavlja kot nujnost globalnega
> zoperstavljanja kapitalističnemu planu, kjer sta temeljna faktorja
> zavest o, recimo temu dialektičnem, ujemanju "tehničnega" in
> "despotskega" momenta v sedanji produktivni organizaciji. Kar zadeva
> tehnološko "racionalnost", je odnos revolucionarnega delovanja do nje
> v tem, da jo "dojame", toda ne zato, da bi jo priznaval in
> poveličeval, temveč zato, da bi jo podvrgel novi uporabi:
> socialistični uporabi strojev.[^30]

[^29]: O zahtevi po "demokratičnem" sodelovanju delavcev za bolj
racionalno kapitalistično upravo glej zelo pomembno knjigo Seymourja
Melmana, *Decision Making and Productivity*, Oxford, 1958.

[^30]: Najnovejši razvoj ekonomskih in tehničnih raziskav v Sovjetski
    zvezi je dvoument. Mentem ko zagovor avtonomnega raziskovalnega
    momenta nedvoumno ima pomen kontrasta in preloma v odnosu do grobih
    oblik volontarizma v stalinističnem tipu načrtovanja, razvoj
    "racionalnih" procesov, neodvisno od družbenega nadzora produkcije,
    se prej zdi (koliko že danes? in koliko kot možna prihodnost?) kot
    predpostavka in podpora novim razvojem starih birokratskih procesov.
    Vsekakor pa je pomembno ne spregledati bistvene razlike med
    sovjetskim in kapitalističnim načrtovanjem. Avtoritarni, despotski
    element organizacije produkcijske se rodi v kapitalističnih odnosih
    in *preživi* v birokratskih planskih gospodarstvih. Birokracija v
    svojem odnosu|razmerju do delavskega razreda se ne more sklicevati
    na objektivno racionalnost, ampak mora vedno priklicati|se sklicati
    na sam delavski razred. Upad|padec|propad temeljnega elementa,
    lastniškega elementa, odvzame birokratski organizaciji, tako rekoč,
    lasten temelj. Tako se v ZSSR in v ljudskih demokracijah kažejo
    drugače, despotizem pa ima negotov in neorganski značaj. Kar seveda
    ne pomeni, da njegove manifestacije|manifestacije njega ne moreje
    privzeti prav tako surovega aspekta|vidika kot ga imajo
    kapitalistične družbe. Glej temeljne opazke Rodolfa Morandija v
    spisih: *Analisi dell'economia regolata* (1942) in *Criteri
    organizzativi dell'economia collettiva* (1944), ponatis v *Lotta di
    popolo*, Torino 1958. Izključevanje|izključitev lastniškega elementa
    in upoštevanje avtoritarno-birokratskega elementa ali tehnične
    odtujitve (ali obeh) sta, kot vemo, v središču neskončnih ideoloških
    literature neokapitalistične in reformistične. Eden od naših
    "Zvezkov" bo posvečen analizi teh ideologij.

<!-- # I salari e la schiavitù politica -->

> # Mezde in politična sužnost
 
<!-- Dacché, con l'organizzazione moderna della produzione, aumentano
"teoricamente" per la classe operaia le possibilità di controllare e
dirigere la produzione, ma "praticamente", attraverso il sempre più
rigido accentramento delle decisioni di potere, si esaspera
l'alienazione, la lotta operaia, *ogni* lotta operaia tende a proporre
la rottura *politica* del sistema. E agente di questa rottura non è il
confronto tra esigenze "razionali" implicite nelle nuove tecniche e loro
utilizzazione capitalistica, ma la contrapposizione di una collettività
operaia che reclama la subordinazione dei processi produttivi alle forze
sociali. Non c'è continuità da affermare, attraverso il salto
rivoluzionario, nell'ordine dello sviluppo tecno-economico: l'azione
operaia mette in discussione i fondamenti del sistema e tutte le sue
ripercussioni e aspetti, a ogni livello. -->

> Ker "teoretično" z moderno organizacijo produkcije za delavski razred
> rastejo možnosti nadzora in vodenja produkcije, "praktično" pa se
> skozi vedno bolj rigidno centralizacijo moči veča odtujenost, delavski
> boj, *vsak* delavski boj, teži predlagati *politični* zlom sistema. A
> dejavnik tega zloma ni soočenje med "racionalnimi" zahtevami, ki
> izhajajo iz novih tehnik in njihovih kapitalističnih rab, ampak
> zoperstavljanje delavskega kolektiva, ki terja podreditev
> produkcijskih procesov družbenim silam. V redu tehnično-ekonomskega
> razvoja ni kontinuitete, ki bi jo revolucionarni skok potrdil:
> delavska akcija pod vprašaj postavlja temelje sistema in vse njegove
> posledice ter vidike, na vseh ravneh.

<!-- Al processo capitalistico è, ovviamente, connaturato il progresso
tecnologico, la "successione sempre più rapida di invenzioni e di
scoperte, (un) rendimento del lavoro umano che aumenta di giorno in
giorno in misura inaudita".[^31]

[^31]: Cfr. Friedrich Engels, *Introduzione a Lavoro salariato e
capitale* di Karl Marx, Roma 1949, p. 19.  -->

> Kapitalističnemu procesu je, seveda, prirejen tehnološki napredek,
> "čedalje hitreje druga drugo spodrivajoče iznajdbe in odkritja, ta v
> doslej neverjetni meri dan za dnem stopnjujoča se produktivnost
> človeškega dela".[^31]  

[^31]: Gl. Friedrich Engels, *Uvod k "Mezdnemu delu in kapitalu"*, v
"Izbrana dela", zv. 2, Ljubljana 1976, str. 672.

<!-- Ma mentre Engels fa scaturire da questo processo "la scissione
della società in una piccola classe smisuratamente ricca e in una grande
classe di salariati nullatenenti", Marx prevede l'aumento non soltanto
del salario nominale ma anche di quello reale: "se... con il rapido
aumento del capitale aumentano le entrate dell'operaio, nello stesso
tempo però si approfondisce l'abisso sociale che separa l'operaio dal
capitalista, aumenta il potere del capitale sul lavoro, la dipendenza
del lavoro dal capitale".[^32] Perciò quanto più è rapido l'aumento del
capitale altrettanto migliora la situazione *materiale* della classe
operaia. E quanto più il salario è legato all'aumento del capitale,
tanto più diretto è il mutevole rapporto di dipendenza del lavoro dal
capitale. Ossia, nella misura in cui migliora la situazione materiale
dell'operaio, peggiora la sua situazione sociale, si approfondisce
"l'abisso sociale che lo separa dal capitalista".[^33]
 
[^32]: *Ivi*, p. 52--53. 

[^33]: *Ivi*, p. 53. -->

> Toda medtem ko Engels meni, da ta proces vodi v "razcepitev družbe v
> majhen, čezmerno bogat razred in v velik razred mezdnih delavcev, ki
> nima ničesar", Marx predvideva povečanje ne le nominalnih, temveč tudi
> realnih mezd: "če se torej delavčev dohodek z naglim naraščanjem
> kapitala povečuje, tedaj se hkrati širi tudi družbeni prepad, ki
> ločuje delavca od kapitalista, tedaj se hkrati povečuje moč kapitala
> nad delom, odvisnost dela od kapitala".[^32] Torej toliko hitrejša kot
> je rast kapitala, toliko boljša je *materialna* situacija delavskega
> razreda. In toliko bolj so mezde povezane z večanjem kapitala, toliko
> bolj neposredna je odvisnost dela od kapitala. To pomeni, v tolikšni
> meri kot se izboljšuje materialna situacija delavca, v tolikšni je
> slabša njegova družbena situacija, poglablja se "družbeni prepad, ki
> ločuje delavca od kapitalista".[^33]

[^32]: *Prav tam*, str. 697.

[^33]: *Prav tam*, str. 697.

<!-- In questo rapporto immediato tra salario e capitale, "la condizione
più favorevole per il lavoro salariato è un aumento più rapido possibile
del capitale produttivo": cioè, "quanto più rapidamente la classe
operaia accresce e ingrossa la forza che le è nemica, la ricchezza che
le è estranea e la domina, tanto più favorevoli sono le condizioni in
cui le è permesso di lavorare a un nuovo accrescimento della ricchezza
borghese, a un aumento del potere del capitale, contenta di forgiare
essa stessa le catene dorate con le quali la borghesia la trascina
dietro di sé".[^34]

[^34]: *Ivi*, p. 53. -->

> V tem neposrednem razmerju med mezdami in kapitalom je "najugodnejši
> pogoj za mezdno delo kar najhitrejše naraščanje produktivnega
> kapitala": to pomeni, "čim hitreje pomnožuje in povečuje delavski
> razred sovražno si silo, tuje bogastvo, ki mu zapoveduje, v tem
> ugodnejših pogojih mu je dovoljeno delati znova za pomnožitev
> meščanskega bogastva, za povečanje moči kapitala, vsemu zadovoljnemu,
> da sam sebi kuje zlate verige, s katerimi ga buržoazija vleče za
> seboj."[^34]

[^34]: *Prav tam*, str. 697.

<!-- Del resto, lo stesso Engels riconoscerà (nella "Critica al
programma di Erfurt") che "il sistema del lavoro salariato è un sistema
di schiavitù, e di una schiavitù che diviene sempre più dura nella
misura in cui si sviluppano le forze produttive sociali del lavoro,
*tanto se l'operaio è pagato meglio, quanto se è pagato peggio*"
(corsivo nostro). Lenin sottolinea questo aspetto come ovvio nel
marxismo: "La concezione dell'accumulazione elaborata dai classici è
stata accolta nella teoria di Marx, la quale ammette che quanto più
rapidamente aumenta la ricchezza, tanto più concretamente si sviluppano
le forze produttive e la socializzazione del lavoro, tanto migliore è la
situazione dell'operaio, nella misura almeno in cui può essere migliore
nell'attuale sistema dell'economia sociale".[^35] Il progressivo aumento
dell'"abisso sociale" tra operai e capitalisti è anche espresso da Marx
nella forma del *salario relativo* e della sua diminuzione. Ma è
evidente che questo concetto implica l'elemento di coscienza politica,
appunto la consapevolezza che al miglioramento delle condizioni
materiali, all'aumento del salario nominale e reale, corrisponde
l'aggravarsi della "dipendenza politica". La cosiddetta inevitabilità
del passaggio al socialismo non è nell'ordine del conflitto materiale
ma, sulla base stessa dello sviluppo *economico* del capitalismo, in
rapporto alla "intollerabilità" del divario sociale, che può
manifestarsi soltanto come presa di coscienza politica. Ma per ciò
stesso il rovesciamento operaio del sistema è negazione dell'intera
organizzazione in cui si esprime lo sviluppo capitalistico, e in primo
luogo della tecnologia in quanto legata alla produttività.

[^35]: Lenin, *Caratteristiche del romanticismo economico*, in *Opere*,
vol. II, trad. it., Roma 1955, p. 136.  -->

> Poleg tega bi Engels sam priznal, da "je torej sistem mezdnega dela
> sistem sužnosti, in sicer sužnosti, ki postaja toliko hujša, kolikor
> bolj se razvijajo družbene produktivne sile dela, *pa naj dobi delavec
> boljše ali slabše plačilo*" (poudarek naš). Lenin poudarja, da je ta
> vidik znotraj marksizma očiten: "pojmovanje akumulacije, ki so ga
> razdelali klasiki in ki ga je sprejela Marxova teorija, priznava, da
> toliko bolj konkreten razvoj produktivnih sil in socializacija dela,
> toliko boljši je položaj delavcev, vsaj do mere, ki je v trenutnem
> sistemu družbene ekonomije mogoča".[^35] Postopno večanje "družbenega
> prepada" med delavci in kapitalisti Marx izrazi tudi v obliki
> *relativne mezde* in njenega zmanjševanja. Toda očitno je, da ta pojem
> vključuje element politične zavesti, natančneje zavedanja, da
> izboljšanju materialnih pogojev, povečanju nominalne in realne mezde,
> ustreza zaostritev "politične odvisnosti". Tako imenovana neizogibnost
> prehoda v socializem ni na ravni materialnega konflikta, temveč na
> sami osnovi *ekonomskega* razvoja kapitalizma v povezavi z
> "nevzdržnostjo" družbene vrzeli, ki se lahko izrazi le kot politična
> zavest. Prav zato je delavska odprava sistema negacija celotne
> organizacije, v kateri se odraža kapitalistični razvoj, ter predvsem
> tehnologije, ki je povezana s produktivnostjo.
 
[^35]: Lenin, *Caratteristiche del romanticismo economico*, v *Opere*,
vol. II, it. prev., Rim 1955, str. 136. 

<!-- gotakritik str 39-40 -->

<!-- La rottura, il superamento del meccanismo salario-produttività non
può quindi porsi come rivendicazione "generale" di aumento del livello
dei salari. E' evidente che l'azione tendente a superare le
sperequazioni salariali costituisce un aspetto del superamento di quel
rapporto. Di per sé, non garantisce in nessun modo la rottura del
sistema, ma soltanto "catene più dorate" per tutta la classe operaia.
Solamente investendo le radici dei processi di alienazione, individuando
la crescente "dipendenza politica" dal capitale, è possibile configurare
un'azione di classe veramente generale.[^36]

[^36]: Cfr. l'attuale dibattito su "Politica ed economia", con articoli
di Garavini, Tatò, Napoleoni, ecc. (nn. dal novembre 1960). -->

> Zato zloma, preseganja mezdno-produktivističnega mehanizma ni mogoče
> postaviti kot "splošne" zahteve za višanje mezd. Jasno je, da akcija
> za odpravo neenakosti v mezdah predstavlja enega izmed aspektov
> preseganja tega razmerja. Sama po sebi pa na noben način ne zagotavlja
> zloma sistema, temveč samo "bolj pozlačene verige" za ves delavski
> razred. Zgolj z napadom na korenine odtujitvenih procesov, z
> določitvijo naraščajoče "politične odvisnosti" od kapitala je mogoče
> oblikovati resnično splošno razredno akcijo.[^36]

[^36]: Glej aktualno razpravo v "Politica ed economia" v člankih
Garavinija, Tatòja, Napoleonija itd. (št. od novembra 1960 dalje).

<!-- In altre parole, la forza eversiva della classe operaia, la sua
capacità rivoluzionaria si presenta (potenzialmente) più forte
precisamente nei "punti in sviluppo" del capitalismo, laddove il
rapporto schiacciante del capitale costante sul lavoro vivente, con la
razionalità in quello incorporata, pone immediatamente alla classe
operaia la questione della sua schiavitù politica. Peraltro, la
crescente dipendenza dei processi sociali "esterni" globali dal piano
capitalistico, quale innanzi tutto si manifesta a livello aziendale, è,
per così dire, nella logica elementare dello sviluppo capitalistico. È
noto che Marx ha più volte insistito su tale sempre più estesa
proliferazione delle radici del potere capitalistico: al limite, la
divisione del lavoro nella fabbrica tende a coincidere con la divisione
sociale del lavoro -- ciò che, naturalmente, è da intendersi in modo non
grettamente economicistico.  -->

> Z drugimi besedami, subverzivna moč delavskega razreda, njegova
> revolucionarna sposobnost je (potencialno) najmočnejša prav v "točkah
> razvoja" kapitalizma, kjer prevladujoče razmerje konstantnega kapitala
> nad živim delom, z racionalizacijo, ki jo uteleša, delavski razred
> neposredno postavlja pred vprašanje njegove politične sužnosti. Poleg
> tega je naraščajoča odvisnost "zunanjih" globalnih družbenih procesov
> od kapitalističnega plana, ki se kaže predvsem na ravni podjetja, tako
> rekoč v osnovni logiki kapitalističnega razvoja. Znano je, da je Marx
> večkrat vztrajal na tem vedno širšem razraščanju korenin
> kapitalistične moči: delitev dela v tovarni, v skrajnem primeru,
> sovpada z družbeno delitvijo dela -- ki je, seveda, ne smemo razumeti
> na ozko ekonomističen način.

<!-- # Consumi e tempo libero  -->

> # Potrošnja in prosti čas

<!-- L'"oggettivismo" accetta la "razionalità" capitalistica a livello
aziendale, svaluta la lotta entro le strutture e i punti in sviluppo,
tende a sottolineare, invece, il valore dell'azione nella sfera esterna,
dei salari e dei consumi; di qui conseguono, con la ricerca di una
"dialettica" a più alto livello, entro l'ambito del sistema, tra
capitale e lavoro, la sopravvalutazione dell'azione a livello statale,
la distinzione-separazione tra momento sindacale e momento politico,
ecc. ecc. Così, anche nel dibattito più serio e "aggiornato" (che oggi
in Italia si svolge soprattutto nell'ambito del sindacato di classe), si
finisce per trovare, in forme più critiche e moderne, una conferma
semplicemente alle vecchie impostazioni "democratiche" della lotta
operaia. Tutto il travaglio di ricerca e di adeguamento dell'azione
sindacale ai modi di sviluppo del capitalismo corre il rischio di
sfociare in una convalida di vecchie posizioni, arricchite di un nuovo
contenuto ma in forma mistificata. Così "si arriva a qualificare
l'azione autonoma delle grandi masse solo a posteriori delle scelte
padronali e mai a priori".[^37]

[^37]: Cfr. Ruggero Spesso, *Il potere contrattuale dei lavoratori e la
"razionalizzazione" del monopolio*, in "Politica ed economia", novembre
1960, p. 10. Una considerazione a parte meriterebbero le posizioni
espresse da Franco Momigliano. Egli giustamente richiama che la
considerazione degli "strumenti dell'organizzazione e della
razionalizzazione del mondo moderno" deve costituire per il Sindacato la
premessa "per ricercare le condizioni di una competizione efficiente e
di una capacità egemone della classe operaia" (articolo cit., p. 2029).
E più volte ha insistito sulla esigenza che, per questa via, la classe
operaia riconquisti di fronte al capitale una vera e completa autonomia.
Ma non si comprende come egli possa conciliare queste tesi ed esigenze
con la conferma dello "specifico terreno istituzionale del Sindacato",
con il conseguente rifiuto a riconoscere alla stessa azione sindacale il
carattere di una crescente tensione di rottura rispetto al sistema (cfr.
F. Momigliano, *Struttura delle retribuzioni e funzioni del Sindacato*,
in "Problemi del socialismo", giugno 1961, p. 633). V. anche dello
stesso Momigliano: *Una tematica sindacale moderna*, in "Passato e
Presente", n. 13, gennaio-febbraio 1960; la Relazione al Congresso sul
Progresso tecnologico e la società italiana (Milano, giugno 1960) sul
tema: "Lavoratori e sindacati di fronte alle trasformazioni del processo
produttivo nell'industria italiana". -->

> "Objektivizem" sprejema kapitalistično "racionalnost" na ravni
> podjetja, razvrednoti boj znotraj struktur in razvojnih točk, ter
> namesto tega poudarja vrednost delovanja v zunanji sferi, v sferi mezd
> in konsumpcije; od tod izhaja -- z raziskovanjem "dialektike" na višji
> ravni, v okviru sistema, med kapitalom in delom -- precenjevanje
> delovanja na državni ravni, razlikovanje-ločevanje med sindikalnim in
> političnim momentom itd. Tako se tudi v najresnejših in "najbolj
> današnjih" razpravah na koncu v bolj kritičnih in modernih oblikah
> pojavi zgolj potrditev starih "demokratičnih" pristopov k delavskemu
> boju. Vse raziskovalno delo in prilagajanje sindikalnega delovanja
> načinom kapitalističnega razvoja tvegajo potrditev starih stališč
> obogatenih z novo vsebino, a v mistificirani obliki. Tako je
> "avtonomno delovanje širokih množic možno zgolj naknadno odločitvam
> gospodarjev in nikoli pred njimi".[^37]

[^37]: Glej Ruggero Spesso, *Il potere contrattuale dei lavoratori e la
"razionalizzazione" del monopolio*, v "Politica ed economia", november
1960, str. 10. Posebej je treba obravnavati stališča, ki jih je izrazil
Franco Momigliano. Upravičeno poudarja, da mora biti
upoštevanje|obravnava "instrumentov organizacije in racionalizacije
modernega sveta" za sindikat predstavljati izhodišče za "iskanje pogojev
učinkovite konkurence in hegemonske sposobnosti delavskega razreda"
(zgoraj navedeni članek, str. 2029). Večkrat je poudaril, da mora
delavski razred, tako, ponovno pridobiti pravo in popolno avtonomijo
pred kapitalom. Vendar se ne razume kako je te teze in zahteve možno
uskladiti s potrditjivo "specifičnega institucionalnega terena
sindikata", s posledično zavrnitvijo|zavračanjem prepoznati isto
sindikalno akcijo kot naraščanje napetosti preloma s sistemom (gl. F.
Momigliano, *Struttura delle retribuzioni e funzioni del sindicato*, v
"Problemi del socialismo", junij 1961, str. 633). Gl. tudi, prav tako
Momigliano: *Una tematica sindicale moderna*, v "Passato e Presente",
št. 13, januar-februar 1960; la Relazione al Congresso sul Progresso
tecnologico e la società italiana (Milano, junij 1960) na temo:
"Lavoratori e sindicati di fronte alla trasformazioni del processo
porduttivo nell'industria italiana".

<!-- Mentre i processi intrinseci all'accumulazione capitalistica
divengono sempre più determinanti globalmente, all'"interno" e
all'"esterno", a livello aziendale e a livello sociale generale, le
varie posizioni rifiorenti anche all'interno del movimento operaio dalla
matrice keynesiana, si presentano come vere e proprie ideologie,
riflesso degli sviluppi neocapitalistici. Contro di esse vale ancora,
anzi più fortemente, l'avvertimento di Marx: "La sfera della
circolazione, ossia dello *scambio* delle merci, nella quale si attua la
*vendita* e la *compera* della *forza di lavoro*, è di fatto un vero
*Eden dei diritti innati dell'uomo*". Non per nulla si contrappongono ai
consumi "imposti" dal capitalismo i consumi "onesti", che dovrebbe
proporre la classe operaia, e l'aumento generale dei salari, cioè la
conferma della schiavitù capitalistica, è presentato come "istanza" del
lavoratore in quanto "persona umana", che rivendica (entro il sistema!)
il riconoscimento e l'affermazione della sua "dignità".[^38]
 
[^38]: Cfr. Antonio Tatò, *Ordinare la struttura della retribuzione
    secondo la logica e i fini del sindacato*, in "Politica e economia",
    febbraio-marzo 1961, pp. 11--23.

    La crescente incidenza sociale immediata della sfera della
    produzione è, com'è noto, sottolineata in tutta la ricerca marxista.
    Come altri autori, Paul M. Sweezy ne *La teoria dello sviluppo
    capitalistico* (tr. it., Torino 1951) ne ha dato, sotto vari
    aspetti, una dimostrazione ancor oggi valida (v. soprattutto p. 307
    sgg., 350 sgg., ecc). 

    Sweezy richiama questo passo di Rosa Luxemburg in *Riforma e
    rivoluzione*: "Il "controllo sociale"... nulla ha a che fare con la
    limitazione della proprietà capitalistica, ma al contrario riguarda
    la sua protezione. Ovvero, per parlare in termini economici, esso
    non costituisce un attacco allo sfruttamento capitalistico, ma
    piuttosto una normalizzazione e regolarizzazione di esso". (Sweezy
    cit., p. 319; cfr. *Capitale*, vol. I, cap. X, par. 6, a proposito
    della legislazione inglese sulla limitazione della giornata di
    lavoro). -->

> Medtem ko postajajo procesi, značilni za kapitalistično akumulacijo,
> vse bolj globalno veljavni, tako "znotraj" kot "zunaj", tako na ravni
> podjetja kot na splošni družbeni ravni, se različna stališča, ki so
> ponovno vzniknila celo znotraj keynesijanskega delavskega gibanja,
> predstavljajo kot prave pravcate ideologije, ki odsevajo
> neokapitalistični razvoj. Zoper njih še vedno, celo bolj, velja Marxov
> opomin: "Sfera cirkulacije ali blagovne menjave, v mejah katere se
> gibljeta nakup in prodaja delovne sile, je dejansko resnični raj
> prirojenih človekovih pravic." Ne zaman se potrošnji, ki jo "vsiljuje"
> kapitalizem, zoperstavlja "poštena" potrošnja, ki jo predlaga delavski
> razred, in splošno višanje mezd, torej potrditev politične sužnosti,
> pa se predstavlja kot "primer" delavca, ki kot "človeške oseba"
> zahteva (znotraj sistema!) priznanje in potrditev svojega
> "dostojanstva".[^38]

[^38]: Glej Antonio Tatò, *Ordinare la struttura della retribuzione
    secondo la logica e i fini del sindacato*, v "Politica e economia",
    februar-marec 1961, str. 11--23.

    V vseh marksističnih raziskavah se poudarja vse večji neposredni
    družbeni vpliv sfere produkcije. Tako kot drugi avtorji, Paul M.
    Sweezy v *The Theory of Capitalist Development* (London 1962), z
    različnih vidikov prikazal kar velja še danes (gl. zlasti str. 307
    in naslednje, 350 in naslednje, itd. v italijanskem prevodu, Torino
    1951).

    Sweezy opozarja na odlomek iz knjige Rose Luxemburg *Reforma in
    revolucija*: "'Družbeni nadzor' ... nima nič opraviti z omejevanjem
    kapitalistične lastnine, temveč gre, nasprotno, za njeno zaščito.
    Ali, v ekonomskem smislu, ne gre za napad na kapitalistično
    izkoriščanje, temveč prej za normalizacijo in regulacijo tega
    izkoriščanja". (Sweezy nav. d., str. 249; gl. tudi *Kapital*, zv. 1,
    poglavje 8, podpoglavje 6. v zvezi z angleško zakonodajo o
    omejevanju delovnega dne).

<!-- La stessa rivendicazione di "bisogni essenziali" (la cultura, la
salute) contro la scala dei consumi imposta dal capitalismo (o dal
neocapitalismo) non ha senso -- come ha giustamente rilevato Spesso --
al di fuori di un rifiuto della razionalizzazione capitalistica e di una
richiesta operaia di controllo e gestionale nella sfera della
produzione.[^39]

[^39]: Cfr. Spesso, cit.: "Auspicare... maggiori consumi culturali non
    ha senso se poi non si possa considerare come fattibile la
    utilizzazione di questa cultura da parte dell'individuo proprio
    nella sua attività creativa e cioè per eccellenza nel processo
    lavorativo... Gli stessi consumi di un individuo sono del tutto
    condizionati dalla sua posizione nell'attività produttiva... I
    "bisogni essenziali" (la cultura, la salute) nascono, si precisano,
    si affermano nel rifiuto delle *work rules*, nella presa di
    coscienza *operaia* del significato e del ruolo del lavoro", (pp.
    9--10).
 
    La rappresentazione dell'alienazione nel neocapitalismo come
    alienazione del consumatore è nello stesso tempo una delle ideologie
    correnti più ridicole e più diffuse. -->

> Sama zahteva po "osnovnih potrebah" (kultura, zdravje) zoper
> konsumpcijo, ki jo vsiljuje kapitalizem (ali neokapitalizem), nima
> smisla -- kot je pravilno izpostavil Spesso -- brez zavrnitve
> kapitalistične racionalizacije in brez delavske zahteve po nadzoru in
> upravljanju s sfero produkcije.[^39]

[^39]: Gl. Spesso, nav. d.: "Upanje na ... večjo kulturno konsumpcijo
    nima smisla, če te kulture ne moremo smatrati kot uporabne za
    posameznike prav v njihovi kreativni dejavnosti, in to je prav v
    delovnem procesu ... Sama konsumpcija posameznika je v celoti
    odvisna od njegovega položaja v produkcijski dejavnosti ... "osnovne
    potrebe" (kultura, zdravje) nastajajo in se potrjujejo v zavračanju
    *work rules*, v ozaveščanju|pridobivanju *delavske* zavesti glede
    vloge dela", (str. 9--10).

    Reprezentacija odtujitve v neokapitalizmu kot odtujitve potrošnika
    je hkrati ena najbolj neumnih in razširjenih ideologij dandanes.

<!-- E' significativo che posizioni "revisionistiche" richiamino,
deformandola, la concezione marxiana del tempo libero, del suo rapporto
con la giornata lavorativa e della sua collocazione nella prospettiva di
una società comunista. Si tende, cioè, sulla base di una interpretazione
"economistica", a identificare, nel pensiero di Marx, la libertà
comunista con l'espansione del tempo libero sulla base di una crescente
pianificazione "oggettiva" e razionalizzatrice dei processi
produttivi.[^40]

[^40]: Cfr. Paul Cardan, *Capitalismo e socialismo*, in "Quaderni di
    unità proletaria", n. 3 (cicl.). Occorre tuttavia sottolineare che
    tale interpretazione è richiamata in Cardan per esprimere, in
    polemica con il marxismo, un punto di vista rivoluzionario. -->

> Pomenljivo je, da "revizionistična" stališča, z izkrivljanjem,
> spominjajo na Marxovo pojmovanje prostega časa, njegovo razmerje do
> delovnega dne in njegov položaj znotraj perspektive komunistične
> družbe. Obstaja težnja, torej, na podlagi "ekonomistične"
> interpretacije Marxove misli komunistično svobodo enačiti z
> razširitvijo prostega časa na podlagi vse bolj "objektivnega" in
> racionalnega načrtovanja produktivnih procesov.[^40]

[^40]: Glej Paul Cardan, *Capitalismo e socialismo*, v: "Quaderni di
    unità proletaria", št. 3. Vseeno je treba poudariti, da se Cardan na
    to razlago sklicuje, da bi v polemiki z marksizmom, izrazil
    revolucionarno stališče.

<!-- In effetti, per Marx, il tempo libero "per la libera attività
mentale e sociale degli individui" non coincide affatto semplicemente
con la riduzione della "giornata lavorativa". Presuppone la
trasformazione radicale delle condizioni del lavoro umano, l'abolizione
del lavoro salariato, la "regolazione sociale del processo lavorativo".
Presuppone, cioè, l'integrale rovesciamento del rapporto capitalistico
tra dispotismo e razionalità, per la formazione di una società
amministrata da liberi produttori, nella quale -- con l'abolizione della
produzione per la produzione -- la programmazione, il piano, la
razionalità, la tecnologia siano sottoposti al permanente controllo
delle forze sociali, e il lavoro possa così (e soltanto per questa via)
diventare il "primo bisogno" dell'uomo. Il superamento della divisione
del lavoro, in quanto meta del processo sociale, della lotta di classe,
non significa un salto nel "regno del tempo libero", ma la conquista del
dominio delle forze sociali sulla sfera della produzione. Lo "sviluppo
completo" dell'uomo, delle sue capacità fisiche e intellettuali (che
tanti critici "umanisti" della "società industriale" amano richiamare)
compare come una mistificazione se si rappresenta come "godimento di
tempo libero", come astratta "versatilità", ecc. indipendentemente dal
rapporto dell'uomo col processo produttivo, dalla riappropriazione del
prodotto e del contenuto del lavoro da parte del lavoratore, in una
società di liberi produttori associati.[^41]

[^41]: La rappresentazione della società comunista come una società di
"abbondanza" di beni (anche se non soltanto materiali) e di "tempo
libero" è comunemente diffusa nelle ideologie sovietiche e risulta
ovviamente dalla negazione di una effettiva regolazione sociale del
processo lavorativo. Le illusioni "tecnologiche" intervengono oggi a
soccorrere queste ideologie. Per Strumilin, ad esempio, "le funzioni
direttive dei processi di produzione" si identificano con il controllo
"tecnico", con il "più elevato contenuto intellettuale" del lavoro reso
possibile dallo "sviluppo della tecnica con i suoi miracolosi meccanismi
automatici e le macchine elettroniche "pensanti"" (cfr. *Sulla via del
comunismo*, Mosca 1959). E così l'automazione permetterà di realizzare
una società realmente "affluente", di consumatori di "tempo libero"!
(Cfr. sopra, nota 30). Come esempio di tipica deformazione dei testi di
Marx su questo punto, cfr. Georges Friedman, *Dove va il lavoro umano?*,
trad. it., Milano 1955, p. 333 sgg., dove la riappropriazione del
prodotto e del contenuto del lavoro stesso da parte dell'operaio è
identificata con il "controllo psico-fisiologico del lavoro"! -->

> Dejansko, za Marxa, prosti čas "osvojen za svobodno, duhovno in
> družbeno dejavnost individuov" nikakor|sploh ne sovpada enostavno z
> redukcijo|krajšanjem "delovnega dne". Predpostavlja radikalno
> preoblikovanje pogojev človeškega dela, odpravo mezdnega dela,
> "družbeno ureditev delovnega procesa". Predpostavlja, torej,
> integralno|popolno odpravo kapitalističnega razmerja med despotizmom
> in racionalnostjo, za oblikovanje|formacijo družbe, s katero
> upravljajo svobodni proizvajalci, v kateri so -- z odpravo produkcije
> zaradi produkcije -- načrtovanje, plan, racionalnost, tehnologija
> podvrženi stalnim nadzorom družbenih sil, in delo lahko tako (in
> le|zgolj tako) postane človekova "prva življenska potreba". Preseganje
> delitve dela , kot cilj družbenega procesa, razrednega boja, ne pomeni
> preskoka|skoka v "kraljestvo prostega časa", ampak|temveč osvojitev
> prevlade družbenih sil nad sfero produkcije. "Celostni razvoj"
> človeka, njegovih fizičnih in duhovnih sposobnosit (na katere se
> toliko "humanističnih" kritikov "industrijske družbe" radi sklicujejo)
> izpade|se zdi kot mistifikacija, če je predstavljeno kot "uživanje
> prostega časa", kot abstraktna "vsestranskost" itd., neodvisna od
> razmerja|odnosa človeka do produkcijskega procesa, do prilaščanja
> produktov in vsebine dela s strani delavca, v družbi asociacije
> svobodnih producentov.[^41]

[^41]: Prikaz komunistične družbe kot družbe "izobilja" dobrin (četudi
    ne samo materialnih) in "prostega časa" je pogost v sovjetskih
    ideologijah in je očitno posledica zanikanja učinkovitega družbenega
    urejanja delovnega procesa. Tem ideologijam danes pripomorejo
    "tehnološke" iluzije. Za Strumilin, na primer, "vodstvene funkcije
    produkcijskega procesa" identificira|enači s "tehničnim" nadzorom, z
    "višjo intelektualno vsebino" dela, ki ga omogoča "tehnični razvoj s
    svojimi čudežnimi avtomatskimi mehanizmi in elektronskimi
    "mislečimi" stroji" (gl. *Sulla via del comunismo*, Moskva 1959). In
    tako avtomatizacija dopušča realizacijo resnično "bogate" družbe
    potrošnikov "prostega časa"! (Gl. zgoraj, opomba 30). Kot primer
    tipičnega izkrivljanja Marxovih besedil, gl. Georges Friedman, *Dove
    va il lavoro umano?*, it. prev., Milano 1955, str. 333 in nadaljnje,
    kjer reapropriacija produkta in vsebina samega dela s strani delavca
    je prepoznana|enačena s "psihično-fiziološkim nadzorom dela"!

<!-- # Il controllo operaio in una prospettiva rivoluzionaria -->

> # Revolucionarna perspektiva delavskega nadzora

<!-- Le "nuove" rivendicazioni operaie, che caratterizzano le lotte
sindacali (prese in esame in questo stesso "Quaderno") non recano
immediatamente un contenuto politico rivoluzionario né implicano uno
sviluppo automatico nello stesso senso. Tuttavia, il loro significato
non può neppure essere limitato a un valore di "adeguamento" ai moderni
processi tecnologici e organizzativi nella fabbrica moderna, presupposto
di una "sistemazione" dei rapporti di lavoro in generale a più alto
livello. Esse contengono delle *indicazioni di sviluppo*, che riguardano
la lotta operaia nel suo insieme e nel suo valore politico. Tali
indicazioni non scaturiscono però semplicemente dalla rilevazione e
dalla "somma" di quelle rivendicazioni, per quanto diverse e più
"avanzate" esse possano apparire rispetto agli obiettivi tradizionali.
Contrattazione dei tempi e ritmi di lavoro, degli organici, del rapporto
salario-produttività, ecc., tendono evidentemente a contrastare il
capitale all'interno stesso del meccanismo di accumulazione e a livello
dei suoi "fattori di stabilizzazione". Il fatto che esse avanzino con le
lotte dei nuclei operai nelle aziende più forti e a più alto sviluppo è
la conferma del loro valore di avanguardia, di rottura. Il tentativo di
strumentalizzarle ai fini di una lotta generale semplicemente salariale
è soltanto illusoriamente la ricerca di una nuova, più vasta unità
dell'azione di classe: su questa linea si realizzerebbe in pratica
precisamente ciò che si dichiara di voler evitare, ossia la ricaduta in
situazioni di chiusura aziendalistica necessariamente conseguenti allo
svuotamento dei potenziali elementi di sviluppo politico. La linea
tendenziale oggettivamente rilevabile come valida ipotesi-guida è nel
rafforzamento e nella espansione della esigenza gestionale. Poiché
l'esigenza gestionale si pone non come esigenza meramente di
partecipazione "conoscitiva" ma investe il rapporto concreto
razionalizzazione-gerarchia-potere, essa non si chiude nell'ambito
dell'azienda, si rivolge precisamente contro il "dispotismo" che il
capitale proietta ed esercita sull'intera società e a tutti i suoi
livelli, si esprime come necessità di rovesciamento totale del sistema
attraverso una presa di coscienza globale e una lotta generale della
classe operaia in quanto tale. -->

> "Nove" delavske zahteve, ki označujejo|so značilne za sindikalne boje
> (obravnavane v tem "Zvezku") nimajo takojšnje revolucionarne politične
> vsebine niti ne pomenijo|implicirajo avtomatskega razvoja v
> istem|samem smislu. Vendar njihovega ponema ne moremo niti omejiti na
> vrednoto "prilagajanja" modernim tehnološkim in organizacijskim
> procesom v moderni tovarni, ki predpostavljajo "sustematizacijo"
> delovnih razmerij na splošno a višji ravni. Vsebujejo
> *kazalce|kazalnike razvoja*, ki zadevajo delavski boj v svojem|njegovi
> celoti in njegovi politični vrednosti. Te kazalniki ne izhajajo
> zgolj|čisto enostavno iz povpraševanj|raziskave in "povzetkov"
> zahtevkov, ne glede na to kako drugačni ali "naprednejši" se zdijo v
> primerjavi s tradicionalnimi cilji. Pogajanja o času in ritmu dela, o
> sestavi|kolektivu|številu zaposlenih, razmerju med mezdo in
> produktivnostjo, itd. evidentno|jasno|očitno nasprotujejo kapitalu
> prav v samem mehanizmu akumulacije in na ravni njegovih "faktorjev
> uveljavljanja|stabilizacije". Dejstvo, da jih predlagajo|da
> napredujejo z bojem delavskih celic v najmočnejših najrazvitejših
> podjetij potrjuje njihovo avantgardno, prelomno vrednost. Poskus, da
> bi jih instrumentalizirali|izkoristili za cilje splošnega zgolj
> mezdnega boja je le iluzorno iskanje novih velikih enotnih razrednih
> akcij: na tem mestu|v tej smeri bi se v praksi uresničilo ravno|prav
> to, čemur se želijo izogniti, tj. ponoven padec v podjetniško
> zapiranje, ki (mu) nujno sledi izpraznitev potencialnih elementov
> političnega razvoja. Trend, ki ga je objektivno mogoče zaznati kot
> vodilno hipotezo je krepitev in širitev zahteve po upravljanju. Ker se
> zahteva po upravljanju ne zastavlja kot zgolj zahteva po "kognitivnem"
> sodelovanju|udeležbi, temveč je investirana|usmerjena v konkretno
> razmerje racionalizacija-hierarhija-moč, se ne zapira v okolje
> podjetja, naslavlja prav proti "despotizmu" ki ga kapital projecira in
> izvaja nad vso|celotno družbo in na vseh svojih ravneh, izraža se kot
> nuja popolne odprave sistema skozi|preko globalnega ozaveščanja in
> splošnega boja delavskega razreda kot takega.

<!-- Noi riteniamo che, praticamente e immediatamente, questa linea
possa esprimersi nella rivendicazione del controllo operaio. Tuttavia,
qualche chiarimento è qui necessario. La formula del controllo operaio
può oggi essere giudicata equivoca, assimilabile a una impostazione
"centrista", di attenuazione o di conciliazione delle esigenze
rivoluzionarie proposte dalle lotte con la tradizionale linea
nazional-parlamentare-democratica: in verità, non mancano accenni a una
utilizzazione della formula in questo senso. Velleitaria e ambigua è,
per esempio, l'indicazione del controllo operaio quando si intende con
essa la continuazione o la ripresa della concezione e della esperienza
dei Consigli di gestione. Nel movimento dei Consigli di gestione, una
esigenza autentica di controllo operaio veniva subordinata -- fino
all'annullamento -- all'elemento "collaborazionistico" legato alle
ideologie della ricostruzione nazionale e a una impostazione strumentale
del movimento reale rispetto al piano istituzionale-elettorale. La
stessa ambiguità è rilevabile quando una linea di controllo operaio
viene proposta come alternativa "tollerabile", come "correzione"
all'"estremismo" della prospettiva dell'autogestione operaia. Ora, è
evidente che una formulazione non mistificata del controllo operaio ha
senso soltanto in rapporto a un obbiettivo di rottura rivoluzionaria e a
una prospettiva di autogestione socialista. In questo quadro, il
controllo operaio esprime la necessità di colmare il "salto" attualmente
esistente tra le stesse rivendicazioni operaie più avanzate a livello
sindacale e la prospettiva strategica. Rappresenta dunque, o meglio può
rappresentare, in una versione non mistificata, una linea politica
immediata alternativa a quelle proposte attualmente dai partiti di
classe.  -->

> Menimo|vztrajamo, da, praktično in takoj, ta trend|linija lahko izrazi
> z zahtevo po delavskem nadzoru|upravljanju. Vendar je tu potrebno
> nekaj razjasnitev. Formulo delavskega nadzora je danes lahko soditi
> enako|enoznačno, možno asimilirani v "centristični" pristop ,
> omiljevanju|omilitvi ali spravi|sprijaznjenju revolucionarnih zahtev,
> ki jih predlagajo boji|bitke tradicionalnih
> nacionalnih-parlamentarnih-demokratičnih usmeritev: v resnici ne
> manjka namigov uporabe te formule v tej smeri|smislu. Neuresničljiva
> in dvouma je, na primer, navedba delavskega nadzora, če imamo v mislih
> nadaljevanje ali ponovitev pojmovanja|koncepta in izkušenj upravnih
> svetov [Consigli di gestione]. V gibanju upravnih svetov je bila
> avtentična zahteva po delavskem nadzoru podrejena -- do izničenja --
> "kolaboranionističnemu" elementu, vezanemu|povezaneu z ideologijo
> nacionalne obnove|prenove in ustanitev|ustalitev (instrumentalizacija)
> realnega gibanja v institucionalno-elektoralni plan. Enako dvoumnost
> je mogoče zaznati, ko se smer|stališče delavskega nadzora predlaga kot
> "sprejemljiva" alternativa, kot "korekcija|popravek" "ekstremizma"
> perspektivi delavskega samoupravljanja. Zdaj je jasno, da formulacija,
> ki ne bi bila mistificirana, delavskega nadzora ima smisel le|zgolj v
> povezavi s ciljem revolucionarnega preloma in s perspektivo
> socialističnega samoupravljanja. V tem okviru, delavski nadzor izraža
> nujnost premostitve "vrzeli|skoka", ki trenutno obstaja
> najnaprednejšimi delavskimi zahtevami na ravni sindikatov in strateško
> perspektivo. Zato predstavlja, ali bolj rečeno, lahko predstavlja v
> nemistificirani različici, politično usmeritev, ki je neposredno|takoj
> alternativa tistim, ki jih predlagajo trenutne|aktualne razredne
> stranke.

<!-- È evidente che qui la linea del controllo operaio è prospettata
come fattore di accelerazione dei tempi della lotta generale di classe:
strumento politico per realizzare tempi "ravvicinati" per rotture
rivoluzionarie. Ben lungi dal potersi rappresentare come "surrogato"
della conquista del potere politico, il controllo operaio costituirebbe
una fase di *massima* pressione sul potere capitalistico (in quanto
minaccia esplicitamente portata alle radici del sistema). Il controllo
operaio, dunque, deve essere visto come preparazione di situazioni di
"dualismo di potere" in rapporto alla conquista politica totale. -->

> Jasno je da tu linija|stališče smer delavskega nadzora je predvidena
> kot dejavnik pospešitve časa splošnega razrednega boja: politični
> instrument za realizacijo "bližnjih" časov|bližanje časov
> revolucionarnih prelomov. Zdaleč da bi lahko predstavljal kot
> "nadomestek" osvojitve politične moči, delavski nadzor lahko
> konstituira|vzpostavi|predstavlja fazo *maksimalnega* pritiska na
> kapitalistično moč|vlado (kot izrecna grožnja koreninam sistema).
> Delavski nadzor torej moramo videti kot pripravo situacija "dvojne
> moči" v razmerju do popolne politične osvojitve.

<!-- È inutile insistere sui motivi che portano a proporre il controllo
operaio come proposta politica generale e attuale. Ciò che veramente
importa è che la polemica contro le formule non sia un alibi per
sfuggire al problema politico generale imposto dalle lotte operaie, e
che concretamente si lavori a ricostruire, sulla base di queste lotte,
una prospettiva politica nuova che garantisca dallo scadimento
"sindacale" dell'azione operaia e dal suo riassorbimento nello sviluppo
capitalistico. -->

> Nesmiselno|brezkoristno je vztrajati pri motivih|razlogih, ki vodijo
> do predloga delavskega nadzora kot splošnega in trenutnega|aktualnega
> političnega predloga. Kar je zares pomembno je, da polemika proti
> formulam ni alibi za beg pred splošnim političnim problemom, ki ga
> vsiljuje|narekuje delavski boj, in ki konkretno delamo na ponovni
> vzpostavitvi, na osnovi|podlagi teh bojev, nove politične perspektive
> ki zagotavlja|preprečuje degradacijo delavskega delovanja in njegovo
> ponovno vpitje v kapitalistični razvoj.

---
resources:
########################################################################
# vim: spelllang=sl,it
...
