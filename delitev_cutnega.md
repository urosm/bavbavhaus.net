---
title: "Delitev čutnega"
description: |
  Zapiski okoli Rancièrjevih estetskih konceptov.
...

## *Nerazumevanje*

*politična filozofija*

: refleksija o skupnosti in njenem smotru, o zakonu in njegovem temelju


Estetske avantgarde stremijo k estetskim revolucijam, se pravi k transformiranju
načina čutenja in izkustva sveta, merijo na "redistribucijo čutnega" (Rancière),
da bi vzpostavile načine, v katerih sistemi delitev določajo dele, preskrbijo
pomene in določajo razmerja med stvarmi vsakdanjega sveta.

## Nelagodje v estetiki

Rancière ugovarja "teorijam distinkcije" -- to je kritikom estetike, ali kot
ideologije, ali kot nepotrebne filozofije -- s preprosto tezo:

> [Z]meda, ki jo razgaljajo v imenu misli, ki postavi vsako reč na njeno pravo
> mesto, je dejansko vozlišče, v katerem se vzpostavijo misli, prakse in afekti,
> hkrati pa so oropani svojega polja ali svojega "pravega" objekta. Če je
> "estetika" ime neke zmede, je ta "zmeda" dejansko tisto, kar nam pomaga
> identificirati objekte, načine izkušanja in forme umetnostne misli, ki naj bi
> jih izolirali, da bi jo razkrinkali. Razvezati vozel, da bi prakse umetnosti
> ali stetske afekte toliko bolje razločili v njihovi singularnosti, morebiti ne
> pomeni drugega kot zapisati se usodi, da popolnoma spregledamo njihovo
> singularnost. [@ranciere2012nelagodje, 32]

### Rancière vs. kritika ideologije

Rancierjeva metoda enakosti je zaznamovana s prelomom z althusserijansko idejo

naloge intelektualcev 
"znanosti" o izkoriščanju, ki se zoperstavlja 
. S prelomom s tisto idejo, ki zoperstavlja na eni
strani znanstveno delo intelektualcev ter na drugi lažno izkoriščanih.

### Metoda enakosti

> Na Francoskem smo vojaki in državljani. Biti državljan, vzrok spet za ponos!
> Za reveže to pomeni, da vzdržujejo in ohranjajo bogataše na oblasti in v
> brezdelju. Delati morajo tam pred veličastno enakostjo zakonov, ki tako
> bogatinom, kakor revežem enako prepovedujejo spati pod mostovi, beračiti po
> ulicah in krasti kruh. To je ena izmed dobrin Revolucije. [Choulette v
> @france1962rdeca, 77]

## "Je čas emancipacije minil?"

Namesto, da se vpraša, ali je emancipacija ideja preteklosti, Rancière vprašanje
zastavi kot 
Rancière izpostavi teološkos

> Čeprav se predstavlja kot kritičen in radikalen, je diskurz o koncu in o
> "post" zgolj določen način izrekanja konfiguracije sveta, ki jo danes
> narekujejo dominantne sile. Pripada teoretskemu arzenalu, ki naj bi obstoječi
> red potrdil kot editi možni svet. Toda to ni res, da živimo v svetu v katerem
> so izginile stare strukture, konflikti in verjetja. Tako imenovana "velika
> pripoved" modernosti ni bila odpravljena -- njeni elementi so bili
> reciklirani. Kar se je zgodilo, ni proces erozije oblasti, konfliktov in
> verjetij, ki se izteka v sredinsko stanje mirne vladavine in ideološkega
> konsenza, temveč aktivni poskus konstruiranja reda dominacije, ki se bo
> sposoben ubraniti vsakršnega odpora in opraviti z vsakršno alternativo, saj se
> predstavlja kot samoumeven in neizbežen. Naš čas torej ni "čas-potem", ampak
> "proti-čas". Evolucija, ki smo ji bili priča zadnja tri desetletja, je strogo
> rečeno intelektualna protirevolucija. [@ranciere2012je, 134]

Za Rancièrja je govor o "post" apologetski govor, ki zgolj potrjuje obstoječi
red. 


> Koncepti, argumenti in verjetja, ki naj bi služili kritiki kapitalističnega
> izkoriščanja in državne dominacije ter oborožili tiste, ki se borijo proti
> njima, so čedalje bolj postavljeni v službo prav nasprotnega, torej podpore
> obstoječemu redu dominacije, prikazu nemožnosti izmikanja njegovemu prijemu in
> dokazovanju, da ga upor naredi le še močnejšega. [@ranciere2012je, 134]

Ta obrat, pravi, naj bi si izposodil tri velike teme iz marksističnega arzenala.

Prva je *ekonomska nujnost*, oziroma enačenje ekonomske in zgodovinske nujnosti,
oziroma marksistični determinizem. 


***

Kako Rancixre poveže politično filozofijo in estetiko? Natančneje na primeru
arhitekture. Vprašanje je torej kako je lahko arhitektura prizorišče
političnofilozofskega preverjanja enakosti.

[enakost inteligenc s stroji? kaj ce nova realnost dinamicnega odnosa med
podatki in (algoritmi) predstavlja sposobnost, ki jo ranc. zagovarja kot
clovesko zmoznost]


---
lang: sl
references:
# La mésentente (1995)
- type: book
  id: ranciere2005nerazumevanje
  author:
  - family: Rancière
    given: Jacques
  title: "Nerazumevanje"
  translator:
  - family: Šumič-Riha
    given: Jelica
  publisher-place: Ljubljana
  publisher: Filozofski inštitut ZRC SAZU
  issued: 2005
  language: sl
# Malaise dans l'esthétique (2004)
- type: book
  id: ranciere2012nelagodje
  author:
  - family: Rancière
    given: Jacques
  title: "Nelagodje v estetiki"
  translator:
  - family: Jenko
    given: Marko
  - family: Benčin
    given: Rok
  publisher-place: Ljubljana
  publisher: ZRC
  issued: 2012
  language: sl
# Aisthesis (2011)
- type: book
  id: ranciere2015aisthesis
  author:
  - family: Rancière
    given: Jacques
  title: "Aisthesis: prizori iz estetskega režima umetnosti"
  title-short: "Aisthesis"
  translator:
  - family: Dular
    given: Sonja
  publisher-place: Ljubljana
  publisher: Maska
  issued: 2015
  language: sl
- type: article-journal
  id: ranciere2012je
  author:
  - family: Rancière
    given: Jacques
  title: "Je čas emancipacije minil?"
  translator:
  - family: Benčin
    given: Rok
  container-title: "Filozofski vestnik"
  volume: XXXIII
  issue: 1
  issued: 2012
  page: 133-145
  language: sl
- type: book
  id: france1962rdeca
  author:
  - family: France
    given: Anatole
  title: "Rdeča lilija"
  translator:
  - family: Dobida
    given: Karel
  publisher-place: Ljubljana
  publisher: Državna založba Slovenije
  issued: 1962
  language: sl
# vim: spelllang=sl
...
