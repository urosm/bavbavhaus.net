---
title: "Fetiš kapitala"
...

- Fetiški značaj blaga in njegova skrivnost [@marx2012kapital1, 57-66]
- Trinitarna formula [@marx1973kapital3, 907-926]

---
lang: sl
references:
- type: book
  id: marx2012kapital1
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 1
  publisher-place: Ljubljana
  publisher: Sophia
  issued: 2012
  language: sl
- type: book
  id: marx1973kapital3
  author:
  - family: Marx
    given: Karl
  title: "Kapital: kritika politične ekonomije"
  title-short: "Kapital"
  volume: 3
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1973
  language: sl
...
