---
title: "Friedrich Engels, Ludwig Feuerbach in konec klasične nemške filozofije"
...

Engels o Marxovih enajstih tezah o Feuerbachu:

> To so beležke, ki bi jih bilo treba pozneje izdelati, napisane v
> naglici, nikakor ne namenjene za tisk, toda neprecenljive vrednosti
> kot prvi dokument, v katerem je genialna kal novega svetovnega nazora
> [@engels1945ludwig, 4].

Kako odpraviti neko filozofijo:

> Heglova šola se je razkrojila, toda Heglove filozofije ni nihče
> kritično obvlada. Strauss in Bauer sta pobrala iz nje vsak po eno
> stran in sta ju polemično obrnila drugo proti drugi. Feuerbach je
> sistem razbil in ga meni nič tebi nič zavrgel. Toda z neko filozofijo
> še nisi opravil, če jo kratkomalo proglasiš za napačno. In tako
> ogromnega dela, kakor je Heglova filozofija, ki je tako silno vplivala
> na duhovni razvoj vsega naroda, nisi mogel odstraniti na ta način, da
> si ga meni nič tebi nič ignoriral. Treba jo je bilo "odpraviti" v
> njenem lastnem smislu, to se pravi v tem smislu, da si kritično uničil
> njeno obliko, toda rešil novo vsebino, ki jo je ta filozofija
> ustvarila [@engels1945ludwig, 14].



> Vse, kar spravlja ljudi v gibanje, mora skoz njihovo glavo; toda zelo
> je odvisno od okoliščin, kakšno obliko bo dobilo v njihovi glavi.
> Delavci se nikakor niso pomirili s kapitalističnim strojnim obratom,
> odkar ne razbijajo več, kakor še leta 1848 ob Renu, strojev kratko in
> malo na drobne kose [@engels1945ludwig, 42].


Fetiš kapitala: D -- D' [gl. kapital3 440]

> Boj zatiranega razreda proti vladajočemu postane nujno političen boj,
> je to boj predvsem proti političnemu gospostvu tega razreda; zavest o
> zvezi tega političnega boja z njegovo ekonomsko osnovo pa otopeva in
> se lahko povsem izgubi. Kjer pa je sami udeleženci tega boja ne izgube
> popolnoma, pa jo skoraj vedno izgube zgodovinopisci ... TODO
> [@engels1945ludwig, 46]


Strogatsky tezko je biti bog

---
lang: sl
references:
########################################################################
- type: book
  id: engels1945ludwig
  author:
  - family: Engels
    given: Friedrich
  title: "Ludwig Feuerbach in konec klasične nemške filozofije"
  title-short: "Ludwig Feuerbach"
  publisher-place: Ljubljana
  publisher: Cankarjeva založba
  issued: 1945
  language: sl
# vim: spelllang=sl
...
